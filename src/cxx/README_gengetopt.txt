The files
src/cxx/applications/*/cmdline.ggo.cmake 
and
src/cxx/tools/*/cmdline.ggo.cmake 

can be used to define command line options for the program in that subdirectory. 

The first program being ported to gengetopt is primeGEM. Take a look in the files

src/cxx/applications/primeGEM/cmdline.ggo.cmake
src/cxx/applications/primeGEM/troyo_gengetopt.cc

to get inspiration.


Documentation about the software gengetopt is found here:
http://www.gnu.org/software/gengetopt/gengetopt.html
