/*
 * File:   ConsensusTree.hh
 * Author: peter9
 *
 * Created on December 7, 2009, 12:25 PM
 *
 * A class representing a consensus tree. A consensus tree is -- as the name
 * implies -- a consensus of several classifications (in this case the
 * classifications are binary phylogenetic gene trees).
 * A consensus tree is not necessarily a binary tree. If a
 * node has more than two children it is called an unresolved node.
 * Every node has two (optional) numbers associated with it:
 * - Posterior probability
 * - Speciation probability
 *
 * Note that it does not make sense to associate neither a posterior probability
 * nor a speciation probability to a non-binary node.
 *
 * Also, a name may be given to a node.
 *
 * IMPLEMENTATION NOTE:
 * Decided to NOT make this a subclass of beep::Tree. First of all, this tree
 * is not necessarily binary and therefore it contains nodes from the class
 * ConsensusNode rather than beep::Node. Secondly, this class do not need
 * many of the member functions of beep::Tree and/or it doesn't make sense
 * to apply some of them to this kind of tree.
 * /Peter
 */

/**
 * Overview of members:
 * public:
        ConsensusTree();
        ConsensusTree(const ConsensusTree& orig);
        virtual ~ConsensusTree();
        ConsensusNode* getRoot();
        void deleteAllNodes();
        void setRoot(ConsensusNode* root);
        friend ostream& operator<<(ostream &o, const ConsensusTree &C);
        bool drawTreeSVG(ostream &o);
        int numberOfLevels();
    private:
        ConsensusNode *m_root;
        static void print_rec(ConsensusNode *node, ostream &o);
        void copy_node_rec(ConsensusNode *oldnode, ConsensusNode *newparent);
        void delete_rec(ConsensusNode *node);
 */


#ifndef _CONSENSUSTREE_HH
#define	_CONSENSUSTREE_HH

#include <iostream>
#include "ConsensusNode.hh"
#include <plotter.h>

namespace beep{


    class ConsensusTree {
    public:
        /**
         * ConsensusTree
         *
         * Basic constructor.
         */
        ConsensusTree();

        /**
         * ConsensusTree
         *
         * Copy constructor.
         *
         * @param orig Tree to copy.
         */
        ConsensusTree(const ConsensusTree& orig);

        /**
         * ~ConsensusTree
         *
         * Destructor.
         */
        virtual ~ConsensusTree();

        /**
         * getRoot
         *
         * Return the root of the tree.
         */
        ConsensusNode* getRoot();

        /**
         * deleteAllNodes
         *
         * Deletes all nodes in the tree.
         */
        void deleteAllNodes();

        /**
         * setRoot
         *
         * Sets the root of the tree.
         * @param root The new root.
         */
        void setRoot(ConsensusNode* root);

        /**
         * operator<<
         *
         * Utility method for printing a consensus tree.
         * @param o An outstream.
         * @param C A consensus tree.
         */
        friend ostream& operator<<(ostream &o, const ConsensusTree &C);
        
        /**
         * drawTreePS
         * 
         * Draws the consensus tree in PS format to the specified ostream.
         * @return Success of operation
         * @param o ostream where tree will be drawn/printed on in PS format.
         */
        bool drawTreeSVG(ostream &o);

        /**
         * numberOfLevels
         *
         * Returns the number of levels in the tree.
         * @return The number of levels in the tree.
         *
         */
        int numberOfLevels();
    private:

        //The root of the tree.
        ConsensusNode *m_root;

        //Helper function for printing a consensus tree.
        static void print_rec(ConsensusNode *node, ostream &o);

        //Helper function for copying a tree.
        void copy_node_rec(ConsensusNode *oldnode, ConsensusNode *newparent);

        //Helper function for deleting a tree.
        void delete_rec(ConsensusNode *node);
    };

};
#endif	/* _CONSENSUSTREE_HH */

