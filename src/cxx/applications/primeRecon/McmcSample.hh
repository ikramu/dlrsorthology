/* 
 * File:   McmcSample.h
 * Author: naurang
 *
 * Created on November 29, 2011, 6:38 PM
 */

#ifndef MCMCSAMPLE_H
#define	MCMCSAMPLE_H

#include <vector>
#include <map>
#include "Tree.hh"
#include "Beep.hh"

namespace beep {

    using namespace std;
    
    class McmcSample {
    public:
        McmcSample();
        McmcSample(const McmcSample& orig);
        virtual ~McmcSample();

        void setTreeStrRep(string _tree);
        string getTreeStrRep();

        void setVvProbs(map<string, double> _map);
        map<string, double>  getVvProbs();

        void setBirthRate(Real _birth);
        void setDeathRate(Real _death);

        void setMeanRate(Real _mean);
        void setVarianceRate(Real _variance);

        string getStringRepresentation();
        
    private:
        string treeStrRep;
        map<string, double> vvProbs;
        Real mean, variance;
        Real birth, death;
    };
};

#endif	/* MCMCSAMPLE_H */

