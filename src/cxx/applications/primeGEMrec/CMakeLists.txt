include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)


# ifeq ($(shell uname),Darwin)
#	MY_LDFLAGS =	-framework accelerate -bind_at_load 


add_executable(${programname_of_this_subdir}
primeGEMrec.cc
${CMAKE_CURRENT_BINARY_DIR}/cmdline.c   ${CMAKE_CURRENT_BINARY_DIR}/cmdline.h 
)


target_link_libraries(${programname_of_this_subdir} prime-phylo)

install(TARGETS ${programname_of_this_subdir} DESTINATION bin)




# troyo_gengetopt.cc is a fork of troyo.cc
# troyo_gengetopt.cc is meant to use gengetopt instead of the function readOptions()



#add_executable(primeGEM_gengetopt
#troyo_gengetopt.cc
#${CMAKE_CURRENT_BINARY_DIR}/cmdline.c   ${CMAKE_CURRENT_BINARY_DIR}/cmdline.h 
#)

#target_link_libraries(primeGEM_gengetopt prime )
#install(TARGETS primeGEM_gengetopt DESTINATION bin)
