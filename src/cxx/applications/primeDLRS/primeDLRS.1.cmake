.\" Comment: Man page for primeDLRS 
.pc
.TH primeDLRS 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
primeDLRS, primeGSRf \- Guest-in-host tree reconciliation tool
.SH SYNOPSIS
.B primeDLRS
[\fIOPTIONS\fR]\fI seqfile hostfile [\fIgsfile\fR]\fR
.SH DESCRIPTION

Guest-in-host tree reconciliation tool, enabling analysis of e.g. guest tree topologies, reconciliation properties and duplication-loss rates. Based on a Bayesian MCMC framework using the underlying GSR model. The old program name, primeGSRf, is still available as a symlink to primeDLRS.

.\" Comment: If we just want a bullet instead of a number .IP \(bu
.IP 1)
the guest tree topology and its divergence times are modelled with a duplication-loss process in accordance with the Gene Evolution Model (GEM).
.IP 2)
sequence evolution is modelled with a user-defined substitution model.
.IP 3)
sequence evolution rate variation over guest tree edges (relaxed molecular clock) are modelled with iid values from a user-selected distribution.
.IP 4)
sequence evolution rate variation over sites (positions) are modelled according to a discretized Gamma distribution.

.PP
The implementation uses a discretization of the host tree to perform its computations.
Please review available options, as you will need to change default settings.
Option \-r may be useful to avoid numeric issues due to scaling.

.I seqfile
is a file with aligned sequences for guest tree leaves. 

.I hostfile
is a PrIME Newick file with host tree incl. divergence times. Leaves must have time 0 and root have time > 0.

.I gsfile
is a tab-delimited file relating guest tree leaves to host tree leaves if info not included in
.I hostfile.

.SH OPTIONS
.TP
.BR \-h ", " \-u ", " \-?
Display help (this text).
.TP
.BR \-o " " \fIFILE\fR
Output filename. Defaults to stderr.
.TP
.BR \-s " " \fIUNSIGNED_INT\fR
Seed for pseudo-random number generator. Defaults to random seed.
.TP
.BR \-i " " \fIUNSIGNED_INT\fR
Number of iterations. Defaults to @conf_value_from_cmake_Iterations@.
.TP
.BR \-t " " \fIUNSIGNED_INT\fR
Thinning, i.e. sample every <value>-th iteration. Defaults to @conf_value_from_cmake_Thinning@.
.TP
.BR \-w " " \fIUNSIGNED_INT\fR
Output diagnostics to stderr every <value>-th sample. Defaults to @conf_value_from_cmake_PrintFactor@.
.TP
.BR \-q 
Do not output diagnostics. Non-quiet by default.
.TP
.BR \-m " " \fRMCMC|PDHC|PD
Execution type (MCMC, posterior density hill-climbing from initial values, or just initial posterior density). Defaults to @conf_value_from_cmake_RunType@.
.TP
.BR \-Sm " " \fRUniformAA|JC69|F81|JTT|UniformCodon|ArveCodon
Substitution model. @conf_value_from_cmake_SubstModel@ by default.
.TP
.BR \-Su " " \fRDNA|AminoAcid|Codon " <Pi=float1 float2 ... floatn> <R=float1 float2 ...float(n*(n-1)/2)>"
User-defined substitution model. The size of Pi and R must fit data type (DNA: n=4, AminoAcid: n=20, Codon: n=62). R is given as a flattened upper triangular matrix. Don't use both option \-Su and \-Sm.
.TP
.BR \-Sn " " \fIUNSIGNED_INT\fR
Number of steps of discretized Gamma-distribution for sequence evolution rate variation over sites. Defaults to @conf_value_from_cmake_SiteRateCats@ (no variation).
.TP
.BR \-Ed " " \fRGamma|InvG|LogN|Uniform
Distribution for iid sequence evolution rate variation over guest tree edges. Defaults to @conf_value_from_cmake_EdgeRateDistr@ (not to confuse with \-Sn).
.TP
.BR \-Ep " " \fIFLOAT\fR " " \fIFLOAT\fR
Initial mean and variance of sequence evolution rate. Defaults to simple rule-of-thumb based on host tree times.
.TP
.BR \-Ef
Fix mean and variance of sequence evolution rate. Non-fixed by default.
.TP
.BR \-Gi " " \fIFILE\fR
Filename with initial guest tree topology.
.TP
.BR \-Gg
Fix initial guest tree topology, i.e. perform no branch-swapping. Non-fixed by default.
.TP
.BR \-Gl
Fix initial guest tree edge lengths (in addition to topology), i.e. fix the edge lengths. Non-fixed by default.
.TP
.BR \-Bp " " \fIFLOAT\fR " " \fIFLOAT\fR
Initial duplication and loss rates. Defaults to @conf_value_from_cmake_BDRates_first@ and @conf_value_from_cmake_BDRates_second@.
.TP
.BR \-Bf
Fix initial duplication and loss rates. Non-fixed by default.
.TP
.BR \-Bt " " \fIFLOAT\fR
Override time span of edge above root in host tree. If the value is <=0, the span will be set to equal the root-to-leaf time. Defaults to value in host tree file. See also option \-Dtt.
.TP
.BR \-Dt " " \fIFLOAT\fR
Approximate discretization timestep. Set to 0 to divide every edge in equally many parts (see \-Di). Defaults to @conf_value_from_cmake_DiscTimestep@. See \-Dtt for edge above root.
.TP
.BR \-Di " " \fIUNSIGNED_INT\fR
Minimum number of parts to slice each edge in. If \-Dt is set to 0, this becomes the exact number of parts. Minimum 2. Defaults to @conf_value_from_cmake_DiscMinIvs@. See \-Dtt for edge above root.
.TP
.BR \-Dtt " " \fIUNSIGNED_INT\fR
Override number of discretization points for edge above root in host tree. By default, irrespective of time span, this is set to the number of points for a (hypothetical) root-to-leaf edge
.TP
.BR \-r
Rescale the host tree so that the root-to-leaf time equals 1.0. All inferred parameters will refer to the new scale. Off by default. Note that discretization parameters are NOT rescaled.
.TP
.BR \-Z
Do not print elapsed wall time and CPU time
.TP
.BR \-W
Do not print the command line
.TP
.BR \-debuginfo
Show misc. info to stderr before iterating. Not shown by default.

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeGEM (1),
.BR showtree (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR treesize (1)

