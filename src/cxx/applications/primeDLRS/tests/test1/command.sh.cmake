#!/bin/sh

datadir="@TESTDATA_DIR@"
out1="@CMAKE_CURRENT_BINARY_DIR@/$1/result.stdout"
out2="@CMAKE_CURRENT_BINARY_DIR@/$1/result.stderr"
expected1="@CMAKE_CURRENT_SOURCE_DIR@/$1/expected.stdout"
expected2="@CMAKE_CURRENT_SOURCE_DIR@/$1/expected.stderr"

@program_exe@ -s 1234567 -q -Z -W -m MCMC -i 100 -t 25 -Sm JTT -Ed Gamma -r -Bt 0.1 -Dt 0.04 -Di 3 ${datadir}/56.pep.align ${datadir}/flies9.estab.mapdp.tree ${datadir}/56.gs > "$out1" 2> "$out2"

if [ $? -ne 0 ]; then
  echo "command returned non-null"
  exit 1
fi

if ! diff "$out1" "$expected1"  >/dev/null 2>&1 ; then 
  echo "$out1" "$expected1" differs;
  exit 1; 
fi

if ! diff "$out2" "$expected2"  >/dev/null 2>&1 ; then 
  echo "$out2" "$expected2" differs
  exit 1
fi

exit 0
