include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)

conf_value_from_cmake(primeDLRS Iterations 1000000) # Used in man-page, C++ code. See macro definition in trunk/cmake/macro.cmake
conf_value_from_cmake(primeDLRS Thinning 100)
conf_value_from_cmake(primeDLRS PrintFactor 1)
conf_value_from_cmake(primeDLRS DiscTimestep 0.05)
conf_value_from_cmake(primeDLRS DiscMinIvs 3)
conf_value_from_cmake(primeDLRS SiteRateCats 1)
conf_value_from_cmake(primeDLRS BDRates_first 1.0)
conf_value_from_cmake(primeDLRS BDRates_second 1.0)
conf_value_from_cmake(primeDLRS RunType MCMC)
conf_value_from_cmake(primeDLRS SubstModel JTT)
conf_value_from_cmake(primeDLRS EdgeRateDistr Gamma)

configure_file(primeDLRS.1.cmake ${tmp_man_pages_dir}/primeDLRS.1 @ONLY)

#configure_file(config_primeDLRS.hh.cmake ${CMAKE_CURRENT_BINARY_DIR}/config_primeDLRS.hh)
include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_executable(${programname_of_this_subdir}
primeDLRS.cc
#test_EdgeDiscBDProbs.cc
)
# set_property(TARGET ${programname_of_this_subdir} PROPERTY LINK_SEARCH_END_STATIC ON )
target_link_libraries(${programname_of_this_subdir} prime-phylo prime-phylo-sfile)

prime_add_tests(${CMAKE_CURRENT_SOURCE_DIR}/tests)

install(TARGETS ${programname_of_this_subdir} DESTINATION bin)


