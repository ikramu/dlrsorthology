package "primeDUP"
version "0.1.0"
purpose "Finds large scale duplications and performs hypothesis testing."

args "--unamed-opts"

option "substitution-model" s "The substitution model used for evolving sequences, default JC69." values="ArveCodon","JC69","JTT","UniformAA","UniformCodon" string default="JC69" optional

option "burn-in-iterations" b "The number of iterations used when burning in the MCMC chains" int default="1000000" optional
option "iterations" i "The number of iterations to perform in the mcmc chains" int default="100000" optional
option "discretization-intervals" d "The number of intervals used to discretize the species tree." int default="8" optional
option "density-function" r "The density functions used to generate edge rates." values="GAMMA","INV","NLOG","UNIFORM"  string default="GAMMA" optional
option "mean" m "The mean of the distribution function used to generate edge rates." float default="1.0" optional
option "variance" v "The variance of the distribution function used to generate edge rates." float default="0.5" optional
option "multiple-tests" t "The number of probability tests performed, i.e. if p = 10, then lsd = 0, 0.1, 0.2, ..., 1" int default="5" optional
option "single-test" p "A single computation of bayes factor is performed with specified probability." float optional
option "log-file" l "This sets the path of the default log file." string typestr="filename" default="log.txt" optional

option "estimation-burn-in-iterations" x "The number of burn in iterations used in the LSD estimation." int default="100000" optional
option "estimation-map-iterations" y "The number of iterations used to make a maximum aposteriori estimation of the gene tree." int default="10000" optional
option "estimation-fixed-iterations" z "The number of iterations performed with the fixed MAP tree in the LSD estiamtion." int default="10000" optional
option "estimation-iterations" e "The number of iterations to perform in the mcmc chain used in LSD estimation, this parameter is important to the accuracy of the estimation." int default="1000" optional
option "estimation-only" o "Perform LSD estimation only, i.e. do not perform model selection." flag off
option "estimation-output" f "If given save estimation output to this file." string typestr="filename" optional

usage "primeDUP [OPTIONS] <species tree> <sequences> <gene species map> [lsd probabilities]"

description "<species tree> is the species tree where a gene tree will evolve."

text "some text here"







