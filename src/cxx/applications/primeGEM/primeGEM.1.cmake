.\" Comment: Man page for primeGEM 
.pc
.TH primeGEM 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
primeGEM \- Guest-in-host tree reconciliation tool
.SH SYNOPSIS
.B primeGEM
[\fIOPTIONS\fR]\fI guest-tree species-tree [\fIgene-species-map\fR]\fR
.SH DESCRIPTION

.I guest-tree
Name of file containing guest tree.

.I species-tree
Species tree in Newick format. with divergence times.

.I gene-species-map
A file that contains lines with a gene name in the first column and species name as found in the species tree in the  second. You can also choose to associate the genes with species in the gene tree.

.SH OPTIONS
.TP
.BR \-h ", " \-u
Display help (this text).
.TP
.BR \-o " " \fIFILE\fR
Output filename. 
.TP
.BR \-i " " \fIINT\fR
Number of iterations.
.TP
.BR \-t " " \fIINT\fR
Thinning
.TP
.BR \-m
Do maximum likelihood. No MCMC.
.TP
.BR \-w " " \fIINT\fR
Write to stderr INT times less often than to stdout.
.TP
.BR \-s " " \fIINT\fR
Seed for pseudo-random number generator. If set to 0 (default), the process id is used as seed.
.TP
.BR \-q 
Do not output diagnostics to stderr.
.TP
.BR \-g
Debug info.
.TP
.BR \-l
Output likelihood. No MCMC.
.TP
.BR \-Gr
reroot gene tree. Default is a fixed root.
.TP
.BR \-Go
Estimate orthology. Default is discard orthology data.
.TP
.BR \-Gs
record speciation probabilities (orthology probabilities will not be recorded)
.TP
.BR \-Bf " " \fIFLOAT\fR " " \fIFLOAT\fR
Fix the birth/death rates to these values
.TP
.BR \-Bp " " \fIFLOAT\fR " " \fIFLOAT\fR
start values of birth/death rate parameters
.TP
.BR \-Bt " " \fIFLOAT\fR
fix 'top time', the time between the first duplication and root of S, to this value.
.TP
.BR \-Bb " " \fIFLOAT\fR
The beta parameter for a prior distribution on species root distance.

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeDLRS (1),
.BR showtree (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR treesize (1)
