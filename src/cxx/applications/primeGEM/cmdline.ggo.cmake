package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "What this program does"
    
args "--unamed-opts"


option "output-file" o "Output file" string typestr="filename" optional
option "number-of-iterations" i "Number of iterations" int default="10000" optional
option "thinning" t "Thinning" int default="10" optional
option "do-maximum-likelihood" m "Do maximum likelihood. No MCMC" flag off
option "factor-stderr-stdout" f "Write to cerr number of times less often to stderr than to stdout" int default="1" optional
option "seed" s "Seed for pseudo-random number generator. If set to 0 (default), the process id is used as seed." int default="0" optional
option "no-diagnostics-to-stderr" q "Do not output diagnostics to stderr" flag off 
option "debug" g "Debug info" flag off 
option "output-likelihood"  l "Output likelihood. No MCMC." flag off

section "Gene tree" sectiondesc="Options related to the gene tree"
option "reroot" r "reroot gene tree. Default is a fixed root. (This parameter was previously named -Gr)" flag off 
option "estimate-orthology" e  "Estimate orthology. Default is discard orthology data. (This parameter was previously named -Go)" flag off 
option "record-speciation-probabilities" p "record speciation probabilities (orthology probabilities will not be recorded) (This parameter was previously named -Gs)" flag off 


section "Birth death process" sectiondesc="Options related to the birth death process"


option "choose-starting-rates" M "use starting rates" flag off
option "fixate-birth-and-death-rate" W "the birth rate and death rates are fixed" flag off 

# "default" values does not work with "multiple" as of gengetop 2.22.3 ( it seems )
option "birth-and-death-rates" d "start parameter for the birth and death rates. The default rates are 1.0,1.0" double typestr="DOUBLE,DOUBLE" multiple(2) optional


# // is the global variable topTime  used in any library? /Erik Sj
# option  "fix-top-time" T "fix 'top time', the time between the first duplication and root of S, to this value.(This parameter was previously named -Bt)" double default="-1" optional

option "beta-parameter" b "The beta parameter for a prior distribution on species root distance. (This parameter was previously named -Bp)" double optional

usage "primeGEM [OPTIONS] <guest tree> <host tree> [<gene-species map>]"

description "<guest tree> is the name of file containing guest tree. <species tree> is the species tree in Newick format (with divergence times). <gene-species map> is optional. This file contains lines with a gene name in the first column and species name as found in the species tree in the second. You can also choose to associate the genes with species in the gene tree. Please see documentation."

text "some text here
"


