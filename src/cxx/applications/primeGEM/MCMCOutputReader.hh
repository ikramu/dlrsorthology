#ifndef MCMCOUTPUTREADER_HH
#define MCMCOUTPUTREADER_HH

#include "Beep.hh"
#include "AnError.hh"

#include <fstream>
#include <string>
#include <map>
#include <stdlib.h>
#include <vector>
#include <sstream>


namespace beep
{
  //! Implements parsing a file containing MCMC output, currently
  //! highly specialized for primeGEM
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  class MCMCOutputReader
  {
  public:
    //----------------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //----------------------------------------------------------------
    MCMCOutputReader(std::string filename, 
		     std::vector<std::string> parameters)
      : file(filename.data()),
	posterior()
    {
      init(parameters);
    }
  
    ~MCMCOutputReader()
    {}

//     MCMCOutputReader(MCMCOutputReader& mor)
//       : file(mor.file),
// 	posterior(mor.posterior)
//     {}

//     MCMCOutputReader& operator=(MCMCOutputReader& mor)
//     {
//       file = mor.file;
//       posterior = mor.posterior;
//     }


    //----------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------
    std::map< std::string, std::vector<Real> >& getPosterior()
    {
      return posterior;
    }

    //  private:
    //----------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------
    void
    init(std::vector<std::string> params)
    {
      using namespace std;

      string line;
      getline(file, line);
      while(line.substr(0,5) != "# L N" && file.eof() == false)
	{
	  getline(file,line);
	}
      
      if(file.eof())
	{
	  throw AnError("No MCMC output found in file!", 1);
	}

      string token;
      stringstream ss(line);
      map< unsigned, string > cols;
      
      unsigned col = 0;
      unsigned nfound = 0;
      vector<Real> dummy;
      while(ss >> token)
	{
	  for(vector<string>::iterator iter = params.begin();
	      iter != params.end(); iter++)
	    {
	      if(token.find(*iter) != std::string::npos) 
		{
		  cols[col] = token;
		  posterior[token] = dummy;
		  nfound++;
		}
	    }
	      col++;
	}
      if(nfound != params.size())
	{
	  throw AnError("MCMCOutReader::init()\n"
			"Could not find all parameters or some"
			" parameter found twice",1);
	}
      getline(file,line);
      while(line.substr(0,1) != "#" && file.eof() == false)
	{
	  stringstream ss(line);
	  unsigned i = 1;
	  while(ss >> token)
	    {
	      if(cols.find(i) != cols.end())
		{
		  posterior[cols[i]].push_back(atof(token.c_str()));
		}
	      i++;
	    }
	  getline(file, line);
	}
    }


    //----------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------
    std::ifstream file;
    std::map< std::string, std::vector<Real> > posterior;
  };

}// end namespace beep

#endif
