#include <map>
#include <fstream>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "EnumerateReconciliationModel.hh"
#include "TreeIO.hh"
#include "GuestTreeModel.hh"
#include "MaxReconciledTreeModel.hh"
#include "LambdaMap.hh"
#include "StrStrMap.hh"

void usage(char* cmsd);
int readOptions(int argc, char **argv);
int nParams = 2;

double topTime           = -1.0;
double birthRate         = 1.0;
double deathRate         = 1.0;

// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  if(argc <= nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = readOptions(argc, argv);
      if(opt + nParams > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	}
      
      //Get tree and Data
      //---------------------------------------------
      string G_filename(argv[opt++]);
      TreeIO io = TreeIO::fromFile(G_filename);
      StrStrMap gs;
      Tree G = io.readBeepTree(0, &gs);//BeepTree(false, false, false, false, 0, 0);
      string S_filename(argv[opt++]);
      io.setSourceFile(S_filename);
      Tree S = io.readHostTree();//BeepTree(true, false, false, true, 0, 0);
      S.setName("S");

      if(gs.size() == 0 && opt < argc)
	{
	  gs = TreeIO::readGeneSpeciesInfo(argv[opt++]);
	}
      else
	{
	  cerr << "No leaf-to-leaf map (gs) given in gene tree\n"
	       << "Please supply additional file with gs map \n";
	  usage(argv[0]);
	}
      
      cout << G << S << gs << endl;

      LambdaMap lambda(G, S, gs);
      if(topTime <= 0)
	{
	  cerr << "Using 'top time' in host tree\n";
	  topTime = S.getTopTime();
	  if(topTime <= 0)
	    {
	      cerr << "No valid top time given in host tree,\n"
		   << "Please use option -Bt\n";
	      usage(argv[0]);
	    }
	}
      BirthDeathProbs bdp(S, birthRate, deathRate, &topTime);
      EnumerateReconciliationModel N(G, gs, bdp);
      cout << N << endl;

      cout << endl <<"N.getNumberOfReconciliations() = " << N.getNumberOfReconciliations() << endl <<endl;
      Probability sum = 0;
      Probability max = 0;
      GuestTreeModel a(N);
      Probability p2 = a.calculateDataProbability();
      unsigned best;

      Probability p = 0;
      for(unsigned i = 0; i < N.getNumberOfReconciliations(); i++)
//        for(unsigned i = 0; i < 4; i++)
	{
	  if(i%1000 == 0)
	    cout << i << endl;
 	  cout << "\n\nReconciliation #" <<i << "\n";
	  N.setGamma(i);

	  cout << N.getGamma().print(false) 
	       << "\n\n";
	  cout << " receives the unique number " 
	       << N.computeGammaID()
	       <<" and has Prob ";
	  p = N.calculateDataProbability().val();
 	  cout << p.val()
	       << endl;

	  cout << "and posterior prob = "
		 << (p/p2).val()
		 << endl; 
	  sum += p;
	  if(p > max)
	    {
	      max = p;
	      best = N.computeGammaID();
	    }
	}
      cout << "\nSum of Probabilities for these recoonciliations is "
	   << sum.val() << " = exp(" << sum << ")\n";
//       GuestTreeModel a(G, gs, bdp);
      cout << "\nThe corresponding prob from GuestTreeModel is " 

	   << p2.val() << " = exp(" << p2 << ")\n"
	   << "\nThe difference is ";
      if(sum > p2)
	cout << (sum-p2).val() << " = exp(" << (sum-p2) << ")\n";
      else
	cout << (p2-sum).val() << " = exp(" << (p2-sum) << ")\n";
      cout<< endl;
      MaxReconciledTreeModel m(N);
      p2 = m.getMLReconciliation();
      cout << "\nMax probabilities of these recoonciliations is "
	   << max.val() << " = exp(" << max << ")\n";
      cout << "\nThe corresponding prob from MaxReconciliationModel is " 
	   << p2.val() << " = exp(" << p2 << ")\n"
	   << "\n\nThe difference is ";
      if(max > p2)
	cout << (max-p2).val() << " = exp(" << (max-p2) << ")\n";
      else
	cout << (p2-max).val() << " = exp(" << (p2-max) << ")\n";
      cout<< endl;
      
    }
  catch(AnError e) 
    {
      e.action();
    }
  catch (exception e) 
    {
      cerr << "Error: "
	   << e.what();
    }
}

void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << "[<options>] <guesttree> <hosttree> [<gs>]"
    << "\n"
    << "Enumerates all reconciliations of <guesttree> to <hosttree> (Beware\n"
    << "could be very many!) Outputs reconciliation and its probability\n"
    << "under the GEM model.\n"
    << "\n"
    << "Parameters:\n"
    << "   <guesttree>/<hosttree>  (string) names of file with guest tree in\n"
    << "                           PrIME or Newick format\n"
    << "   <gs>                    (string, optional) name of file with \n"
    << "                           leaf-to-leaf map from guest to host tree\n"

    << "Options:\n"
    << "  -u, -h          This text.\n"
    << "  -B<option>      Options relating to the bith death process\n"
    << "   -Bp <float> <float>\n"
    << "                  Set BD parameters to these values.\n"
    << "   -Bt <float>    Fix top time to this value\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'p':
		{
		  if (opt + 1 < argc)
		    {		  
		      birthRate = atof(argv[++opt]);
		      if (opt + 1 < argc)
			{		  
			  deathRate = atof(argv[++opt]);
			}
		      else
			{
			  cerr << "Expected birth and death param (float) after -Bp/f\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected birth and death param (float) after -Bp/f\n";
		      usage(argv[0]);
		      exit(1);
		    }
		  break;
		}
	      case 't':
		{
		  if (opt + 1 < argc)
		    {		  
		      topTime = atof(argv[++opt]);
		    }
		  else
		    {
		      cerr << "Expected top time param (float) after -Bt\n";
		      usage(argv[0]);
		      exit(1);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

