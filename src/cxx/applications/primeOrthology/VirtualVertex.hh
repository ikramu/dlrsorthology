/* 
 * File:   VirtualVertex.hh
 * Author: peter9
 *
 * Created on November 30, 2009, 3:53 PM
 *
 * A virtual vertex represent a node in a tree as a partition of the leaves into
 * tree parts. No notion of parent- or child vertices is present.
 * Instead a virtual vertex has 2 parts that are considered to be "below" the
 * vertex and the third part is "above" or "besides" the vertex.
 * The 2 former parts are called descendant parts and the third part is given
 * implicitly by the descendant parts.
 * A virtual vertex V1 is considered to "dominate" another virtual vertex V2 if
 * the union of the descendant parts of V2 is a subset of the union of the
 * descendant parts of V1.
 */


/**
 * Overview of members:
 * public:
        VirtualVertex(int tno_genes);

        VirtualVertex(const VirtualVertex& orig);

        friend bool operator<(const VirtualVertex &lhs, const VirtualVertex &rhs);
        friend bool operator==(const VirtualVertex &lhs, const VirtualVertex &rhs);

        GeneSet getLeftDescendantPart() const {return m_dp_left;}
        GeneSet getRightDescendantPart() const {return m_dp_right;}

        void setLeftDescendantPart(const GeneSet &gs){m_dp_left = gs;}
        void setRightDescendantPart(const GeneSet &gs){m_dp_right = gs;}

        bool dominates(const VirtualVertex &rhs) const;

        virtual ~VirtualVertex();

        friend ostream& operator<<(ostream& o, const VirtualVertex &vv);

    private:
        GeneSet m_dp_left;
        GeneSet m_dp_right;
 * 
 * 
 */

#ifndef _VIRTUALVERTEX_HH
#define	_VIRTUALVERTEX_HH

#include <vector>
#include "GeneSet.hh"
#include <boost/foreach.hpp>
#define foreach BOOST_FOREACH

namespace beep{

    using namespace std;


    class VirtualVertex {

    public:
        /**
         * VirtualVertex
         *
         * Constructor with argument.
         *
         * IMPLEMENTATION NOTE: Maybe the information about total number of
         * genes is unnecessary..
         *
         * @param tno_genes Total number of leaves/genes in the overlaying tree.
         */
        VirtualVertex(int tno_genes);

        /**
         * VirtualVertex
         *
         * Copy constructor.
         *
         * @param orig The VirtualVertex to copy.
         */
        VirtualVertex(const VirtualVertex& orig);

        /**
         * operator <
         *
         * Somewhat arbitrary operator that must be specified to be able to
         * place a VirtualVertex in a map or set.
         */
        friend bool operator<(const VirtualVertex &lhs, const VirtualVertex &rhs);

        /**
         * operator==
         *
         * Descides if two virtual vertices are equal.
         *
         * Two virtual vertices are considered equal if both descendant parts in
         * one virtual vertex have corresponding parts in the other virtual
         * vertex, i.e. we do not really care about "left" or "right"
         * descendant parts.
         */
        friend bool operator==(const VirtualVertex &lhs, const VirtualVertex &rhs);

        /**
         * getLeftDescendantPart
         *
         * Returns the left descendant part of the virtual vertex.
         */
        GeneSet getLeftDescendantPart() const {return m_dp_left;}

        /**
         * getRightDescendantPart
         *
         * Returns the right descendant part of the virtual vertex.
         */
        GeneSet getRightDescendantPart() const {return m_dp_right;}

        /**
         * setLeftDescendantPart
         *
         * Sets the left descendant part of the virtual vertex.
         *
         * @param gs The gene set representing the left descendant part.
         */
        void setLeftDescendantPart(const GeneSet &gs){m_dp_left = gs;}

        /**
         * setRightDescendantPart
         *
         * Sets the right descendant part of the virtual vertex.
         *
         * @param gs The gene set representing the right descendant part.
         */
        void setRightDescendantPart(const GeneSet &gs){m_dp_right = gs;}

        /**
         * dominates
         *
         * Returns true if this object dominates the specified object.
         *
         * A virtual vertex V1 is considered to "dominate" another virtual
         * vertex V2 if the union of the descendant parts of V2 is a subset
         * of the union of the descendant parts of V1.
         *
         * @param rhs A virtual vertex.
         */
        bool dominates(const VirtualVertex &rhs) const;

        string getStrRepresentation();

        /**
         * ~VirtualVertex
         *
         * Destructor.
         */
        virtual ~VirtualVertex();

        /**
         * operator <<
         *
         * Prints a string representation of a VirtualVertex object to the
         * specified outstream.
         */
        friend ostream& operator<<(ostream& o, const VirtualVertex &vv);

    private:
        //Left descendant part.
        GeneSet m_dp_left;

        //Right descendant part.
        GeneSet m_dp_right;
    };



};

#endif	/* _VIRTUALVERTEX_HH */

