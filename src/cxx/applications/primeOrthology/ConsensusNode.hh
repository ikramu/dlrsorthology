/*
 * File:   ConsensusNode.hh
 * Author: peter9
 *
 * A class representing a node in a consensus tree. 
 * Since a consensus tree is not necessarily binary each node has the ability 
 * to maintain a variable number of children.
 * Also, each node is able to hold a posterior probability, a  speciation
 * probability and a name (all optional).
 *
 * Created on December 7, 2009, 12:26 PM
 */


/**
 * Overview of members:
 *
 * public:
        ConsensusNode();
        ConsensusNode(  Probability &post,
                        Probability &spec,
                        string name = "");
        ConsensusNode(const ConsensusNode& orig);
        virtual ~ConsensusNode();

        void addProbabilities(Probability &post, Probability &spec);
        bool hasProbabilities();
        Probability getPosteriorProb();
        Probability getSpeciationProb();

        string getName();
        void setName(string name);

        ConsensusNode* getParent();
        void setParent(ConsensusNode *parent);

        void addChild(ConsensusNode* child);
        ConsensusNode* getChild(int n);
        unsigned int getNumberOfChildren();

        bool isRoot();
        bool isLeaf();

    private:
        Probability m_posterior;
        Probability m_speciation;
        bool m_probabilities_set;
        string m_name;
        ConsensusNode *m_parent;
        vector<ConsensusNode*> m_children;
 *
 */

#ifndef _CONSENSUSNODE_HH
#define	_CONSENSUSNODE_HH

#include "Probability.hh"
#include <vector>
#include <plotter.h>

namespace beep{
    using namespace std;

    class ConsensusNode {
    public:
        /**
         * ConsensusNode
         *
         * Basic constructor.
         */
        ConsensusNode();

        /**
         * ConsensusNode
         *
         * Constructor with optional arguments.
         *
         * @param post Posterior probability
         * @param spec Speciation probability
         * @param name Name of node
         */
        ConsensusNode(  Probability &post,
                        Probability &spec,
                        string name = "");

        /**
         * ConsensusNode
         *
         * Copy constructor.
         *
         * @param orig Node to copy.
         *
         */
        ConsensusNode(const ConsensusNode& orig);

        /**
         * ~ConsensusNode
         *
         * Destructor.
         */
        virtual ~ConsensusNode();

        /**
         * addProbabilities
         *
         * Add probabilities to node.
         *
         * @param post Posterior probability.
         * @param spec Speciation probability.
         */
        void addProbabilities(Probability &post, Probability &spec);

        /**
         * hasProbabilities
         *
         * Returns true if the posterior and speciation probabilities are set.
         */
        bool hasProbabilities();

        /**
         * getPosteriorProbability
         *
         * Returns the posterior probability. Note that if
         * hasProbabilities() == false then this method has undefined results.
         */
        Probability getPosteriorProb();

        /**
         * getSpeciationProbability
         *
         * Returns the speciation probability. Note that if
         * hasProbabilities() == false then this method has undefined results.
         */
        Probability getSpeciationProb();

        /**
         * getName
         *
         * Returns the name of the node.
         */
        string getName();

        /**
         * setName
         *
         * Set the name of the node.
         */
        void setName(string name);

        /**
         * getParent
         *
         * Returns the parent node (if there is one).
         */
        ConsensusNode* getParent();

        /**
         * setParent
         *
         * Set the parent node.
         */
        void setParent(ConsensusNode *parent);

        /**
         * addChild
         *
         * Add a child to the node.
         */
        void addChild(ConsensusNode* child);

        /**
         * getChild
         *
         * Return a child of the node.
         * @param n Id of child. Must be in the range 0..getNumberOfChildren()-1
         */
        ConsensusNode* getChild(int n);

        /**
         * getNumberOfChildren
         *
         * Returns the number of children registered to the node.
         */
        unsigned int getNumberOfChildren();

        /**
         * isRoot
         *
         * Returns true if the node is a root node. (i.e. no parent).
         */
        bool isRoot();

        /**
         * isLeaf
         *
         * Returns true if the node is a leaf (i.e. has no children).
         */
        bool isLeaf();

        /**
         * Returns the level of the node. 0 if the node is a leaf.
         */
        int nodeHeight();

        /**
         * numberOfLeaves
         *
         * Returns the number of leaves in the subtree rooted at this node.
         */
        int numberOfLeaves();

        /*
         * The following member is used for drawing a consensus tree in the class
         * ConsensusTree.
         * I put it here for convenience. Maybe bad design..
         * /Peter
         */


        /**
         * drawSubTree
         * 
         * Draws the subtree using the specified plotter and user coordinates.
         * 
         * @param plotter The plotter to use.
         * @param x0
         * @param y0 (x0, y0) is the lower right corner.
         * @param x1
         * @param y1 (x1, y1) is the upper right corner.
         */
        void drawSubTree(Plotter &plotter, double x0, double y0, double x1, double y1, double xmargin, double ymargin, bool only_speciation = false);

    private:
        //Posterior probability
        Probability m_posterior;

        //Speciation probability
        Probability m_speciation;

        //Flag for probabilities. True if probabilities are set.
        bool m_probabilities_set;

        //Name of node
        string m_name;

        //Parent node
        ConsensusNode *m_parent;

        //Children nodes
        vector<ConsensusNode*> m_children;

        //Height of node
        unsigned int m_lvl;

        //Update node height and number of leaves for ancestors of node
        void update();

        //Drawing coords
        double m_x;
        double m_y;

        /**
         * drawRecursive
         *
         * Recursively updates the drawing coords and draws the subtree. If some leaf below this node
         * has not been given drawing coordinates, then the coordinates will
         * be (-1,-1) for all nodes below and including this node.
         *
         * Returns the minimum and maximum x-coordinates of the leaves of this node.
         * If this node is a leaf, then the x-coordinate is simply returned twice.
         * If some leaf below this node is missing x coordinates then (-1,-1) is returned.
         */
        vector< pair<double,double> > drawRecursive(Plotter &plotter, double lvl_height, double leaf_width, double x0, double y0, int &leaf, double font_size, int longest_name, bool only_speciation=false);

        int findLongestLeafName();


    };

};
#endif	/* _CONSENSUSNODE_HH */

