/*
 * Original.
 * File:   ConsensusNode.cc
 * Author: peter9
 * 
 * Created on December 7, 2009, 12:26 PM
 */

#include <algorithm>



#include "ConsensusNode.hh"
#include <sstream>
#include <iomanip>
#include <cmath>

namespace beep {

    const double NODE_RADIUS = 5.0;
    const double NODE_SPACE = NODE_RADIUS * 3.0;
    const double LINE_WIDTH = 1.0;
    const string NODE_COLOR = "black";
    const string TEXT_COLOR = "black";
    const double LEAF_TEXT_ANGLE = 90.0;
    const double NODE_TEXT_ANGLE = 0.0;
    const string BOX_BG_COLOR = "grey";
    const int BOX_BG_FILLTYPE = 30000;
    const int FILLTYPE_NONE = 0;
    const int FILLTYPE_FULL = 1;

    ConsensusNode::ConsensusNode() :
    m_posterior(),
    m_speciation(),
    m_probabilities_set(false),
    m_name(""),
    m_parent(NULL),
    m_children(),
    m_lvl(0),
    m_x(-1.0),
    m_y(-1.0) {

    }

    ConsensusNode::ConsensusNode(Probability &post, Probability &spec, string name) :
    m_posterior(post),
    m_speciation(spec),
    m_probabilities_set(true),
    m_name(name),
    m_parent(NULL),
    m_children(),
    m_lvl(0),
    m_x(-1.0),
    m_y(-1.0) {

    }

    ConsensusNode::ConsensusNode(const ConsensusNode& orig) :
    m_posterior(orig.m_posterior),
    m_speciation(orig.m_speciation),
    m_probabilities_set(orig.m_probabilities_set),
    m_name(orig.m_name),
    m_parent(orig.m_parent),
    m_children(orig.m_children),
    m_lvl(orig.m_lvl),
    m_x(orig.m_x),
    m_y(orig.m_y) {

    }

    ConsensusNode::~ConsensusNode() {
    }

    void
    ConsensusNode::addProbabilities(Probability &post, Probability &spec) {
        m_posterior = post;
        m_speciation = spec;
        m_probabilities_set = true;
    }

    bool
    ConsensusNode::hasProbabilities() {
        return m_probabilities_set;
    }

    Probability
    ConsensusNode::getPosteriorProb() {
        return m_posterior;
    }

    Probability
    ConsensusNode::getSpeciationProb() {
        return m_speciation;
    }

    string
    ConsensusNode::getName() {
        return m_name;
    }

    void
    ConsensusNode::setName(string name) {
        m_name = name;
    }

    ConsensusNode*
    ConsensusNode::getParent() {
        return m_parent;
    }

    void
    ConsensusNode::update() {
        if (m_parent == NULL) {
            return;
        }
        m_parent->m_lvl = std::max<int>(m_parent->m_lvl, m_lvl + 1);
        m_parent->update();
    }

    void
    ConsensusNode::setParent(ConsensusNode *parent) {
        m_parent = parent;
        update();
    }

    void
    ConsensusNode::addChild(ConsensusNode* child) {
        // Currently no check for duplicate children.
        m_children.push_back(child);
    }

    ConsensusNode*
    ConsensusNode::getChild(int i) {
        // Currently no check for duplicate children.
        return m_children[i];
    }

    unsigned int
    ConsensusNode::getNumberOfChildren() {
        return m_children.size();
    }

    int ConsensusNode::numberOfLeaves() {
        if (isLeaf()) {
            return 1;
        }
        int ret = 0;
        for (unsigned int i = 0; i < getNumberOfChildren(); i++) {
            ret += getChild(i)->numberOfLeaves();
        }
        return ret;
    }

    bool
    ConsensusNode::isRoot() {
        return m_parent == NULL;
    }

    bool
    ConsensusNode::isLeaf() {
        return m_children.empty();
    }

    int
    ConsensusNode::nodeHeight() {
        return m_lvl;
    }

    bool cmp_lvls(ConsensusNode *n1, ConsensusNode *n2) {
        return n1->nodeHeight() > n2->nodeHeight();
    }

    vector< pair<double, double> >
    ConsensusNode::drawRecursive(Plotter &plotter, double lvl_height, double leaf_width, double x0, double y0, int &leaf, double font_size, int longest_name, bool only_speciation) {
        if (isLeaf()) {
            //Set coordinates for node
            m_x = x0 + (double) leaf * leaf_width;
            m_y = y0;

            //Draw node
            plotter.colorname(NODE_COLOR.c_str());
            plotter.filltype(FILLTYPE_FULL);
            plotter.fcircle(m_x, m_y, NODE_RADIUS);

            //Draw extra circle around node to make it stand out
            plotter.filltype(FILLTYPE_NONE);
            plotter.fcircle(m_x, m_y, (4.0 / 2.0) * NODE_RADIUS);

            //Draw box
            plotter.filltype(BOX_BG_FILLTYPE);
            double bw = 1.5 * font_size;
            double bh = (double) longest_name * font_size * 0.70;
            plotter.fmoverel(0.0, -2.0 * NODE_RADIUS);
            plotter.colorname(BOX_BG_COLOR.c_str());
            plotter.fboxrel(-bw / 2.0, -bh, bw / 2.0, 0.0);

            //Draw text
            plotter.fmoverel(0.0, bh / 2.0);
            plotter.textangle(LEAF_TEXT_ANGLE);
            plotter.colorname(TEXT_COLOR.c_str());
            plotter.alabel('r', 'c', getName().c_str());

            //Set min and max
            vector< pair<double, double> > ret(1);
            ret[0].first = m_x;
            ret[0].second = m_x;

            leaf++;
            return ret;
        }


        /*
         * Non-leaf node
         */

        /*
         * Sort children in decreasing order of level in order to draw and 
         * update nodes in that order.
         */
        //std::sort(m_children.begin(), m_children.end(), cmp_lvls);

        /* 
         * Draw each child's subtree and get the minimal and maximal x coordinates
         * below this node
         */
        vector< pair<double, double> > ret(m_lvl + 1, pair<double, double>(-1.0, -1.0));
        for (unsigned int i = 0; i < m_children.size(); i++) {
            //Get child
            ConsensusNode *child = m_children[i];

            //Draw subtree for child
            vector< pair<double, double> > minmax = child->drawRecursive(plotter, lvl_height, leaf_width, x0, y0, leaf, font_size, longest_name, only_speciation);

            /*
             * Update min and max for each level below this node with
             * information from the child's subtree
             */
            for (unsigned int j = 0; j < minmax.size(); j++) {
                //Update min
                if (minmax[j].first < 0) {
                    ret[j].first = -1.0;
                } else {
                    if (ret[j].first < 0) {
                        ret[j].first = minmax[j].first;
                    } else {
                        ret[j].first = std::min<double>(ret[j].first, minmax[j].first);
                    }
                }
                //Update max
                ret[j].second = std::max<double>(ret[j].second, minmax[j].second);
            }
        }

        /* Calculate coordinates for this node */
        m_x = (ret[0].first + ret[0].second) / 2.0;
        m_y = y0 + ((double) m_lvl) * lvl_height;

        /* Set min and max for the current level */
        ret[m_lvl].first = m_x;
        ret[m_lvl].second = m_x;


        /*
         * Draw this node and edges to each child.
         * Also, draw possible probabilities.
         */

        //Draw node
        plotter.colorname(NODE_COLOR.c_str());
        plotter.filltype(FILLTYPE_FULL);
        plotter.fcircle(m_x, m_y, NODE_RADIUS);

        //Draw Edge to each child
        plotter.filltype(FILLTYPE_NONE);
        for (unsigned int i = 0; i < getNumberOfChildren(); i++) {
            ConsensusNode *child = getChild(i);
            bool line_drawn = false;
            if (std::abs(m_x - child->m_x) > 1e-20) {
                /*
                 * Check if some lines from lower levels will be crossed.
                 * If so, draw a bezier curve outside the child.
                 */
                for (unsigned int j = 1; j < m_lvl && !line_drawn; j++) {
                    //(xm, ym) is the possible conflicting node

                    //Check min
                    double xm = ret[j].first;
                    double ym = y0 + j*lvl_height;
                    if (child->m_x < xm && xm < m_x && child->m_y < ym && ym < m_y) {
                        /*
                         * Let
                         * P0 = (x0, y0) be the childs coordinates
                         * P2 = (x1, y1) be this node's coordinates
                         * M = (xm, ym) be the conflicting node's coordinates
                         */

                        /*
                         * Construct a line from the child to this node.
                         * The line is given by (on parametric form):
                         * L(alpha) = P0 + alpha*(P2 - P0)
                         *
                         * If the line crosses the box (xm, x1) x (y0, ym)
                         * then we have a conflict.
                         *
                         * We find a conflict by setting L(alpha)_y = ym, solve
                         * for alpha, then check if L(alpha)_x > mx - C, where C
                         * is some offset constant.
                         *
                         */

                        //Find alpha
                        double alpha = (ym - child->m_y) / (m_y - child->m_y);
                        if (child->m_x + alpha * (m_x - child->m_x) > xm - NODE_SPACE) {
                            /*
                             * We have a collision between lines.
                             * 
                             * We draw a bezier curve that passes through the point
                             * M + C. C is some small offset vector.
                             *
                             * If B(t) is a quadratic bezier curve then we can make
                             * sure that B(t) goes through M+C by setting
                             * B(0.5) = M+C, then solve for the control point P1.
                             */
                            double p1x = 0.5 * (4.0 * (xm - NODE_SPACE) - m_x - child->m_x);
                            double p1y = 0.5 * (4.0 * (ym + NODE_SPACE) - m_y - child->m_y);
                            plotter.fbezier2(child->m_x, child->m_y, p1x, p1y, m_x, m_y);
                            ret[j].first = xm - NODE_SPACE;
                            line_drawn = true;
                        }
                    }
                    //Check max
                    xm = ret[j].second;
                    if (m_x < xm && xm < child->m_x && child->m_y < ym && ym < m_y) {
                        /*
                         * This is symmetric to the case above.
                         */
                        double alpha = (ym - child->m_y) / (m_y - child->m_y);
                        if (child->m_x + alpha * (m_x - child->m_x) < xm + NODE_SPACE) {
                            double p1x = 0.5 * (4.0 * (xm + NODE_SPACE) - m_x - child->m_x);
                            double p1y = 0.5 * (4.0 * (ym + NODE_SPACE) - m_y - child->m_y);
                            plotter.fbezier2(child->m_x, child->m_y, p1x, p1y, m_x, m_y);
                            ret[j].second = xm - NODE_SPACE;
                            line_drawn = true;
                        }
                    }
                }
            }
            if (!line_drawn) {
                plotter.fline(m_x, m_y, child->m_x, child->m_y);
            }
        }

        //Draw possible probabilities.
        if (m_probabilities_set) {

            //Draw extra circle around node to make it stand out
            plotter.filltype(FILLTYPE_NONE);
            plotter.colorname(NODE_COLOR.c_str());
            plotter.fcircle(m_x, m_y, (4.0 / 2.0) * NODE_RADIUS);

            //Draw box
            plotter.filltype(BOX_BG_FILLTYPE);
            plotter.fmove(m_x, m_y - lvl_height / 2.0);
            double bw = 7 * font_size * 0.60;

            double rec_height = 2.5;
            if (only_speciation)
                rec_height = 2.1;
            double bh = rec_height * font_size;
            plotter.colorname(BOX_BG_COLOR.c_str());
            plotter.fboxrel(-bw / 2.0, -bh / 2.0, bw / 2.0, bh / 2.0);

            //Draw probabilities in box
            plotter.fmove(m_x, m_y - lvl_height / 2.0 + font_size);
            plotter.colorname(TEXT_COLOR.c_str());
            plotter.textangle(NODE_TEXT_ANGLE);

            if (!only_speciation) {
                std::ostringstream o(ostringstream::out);
                o << "Po: " << setprecision(2) << getPosteriorProb().val();
                plotter.alabel('c', 'C', o.str().c_str());
            }

            std::ostringstream o2(ostringstream::out);
            o2 << "Sp: " << setprecision(2) << getSpeciationProb().val();
            plotter.fmoverel(0.0, -font_size+10);
            plotter.alabel('c', 't', o2.str().c_str());
        }

        return ret;
    }

    void
    ConsensusNode::drawSubTree(Plotter &plotter,
            double x0,
            double y0,
            double x1,
            double y1,
            double xmargin,
            double ymargin,
            bool only_speciation) {
        /*
         * Specify user coordinate system:
         * (x0,y0) in lower left corner,
         * (x1,y1) in upper right corner.
         */
        plotter.fspace(x0, y0, x1, y1);

        //Set linee thickness in user coordinates.
        plotter.flinewidth(LINE_WIDTH);

        // Clear Plotter's graphics display to get rid of possible junk drawings.
        plotter.erase();

        //Calculate font size
        double font_size = (x1 - x0) / 50.0;
        if (numberOfLeaves() > 24) {
            font_size = (x1 - x0) / ((double) numberOfLeaves()*2.0);
        }
        plotter.ffontsize(font_size);

        //Find height for each level and width between each leaf.
        int lln = findLongestLeafName();
        double lvl_height = (y1 - y0 - 2 * ymargin - (1.0 + (double) lln * 0.75) * font_size) / (double) (m_lvl);
        double leaf_width = (x1 - x0 - 2 * (xmargin + font_size)) / (double) (numberOfLeaves() - 1);

        //Draw tree recursively
        int leaf = 0;
        drawRecursive(plotter, lvl_height, leaf_width, x0 + xmargin + font_size, y0 + ymargin + (double) lln * font_size * 0.75, leaf, font_size, findLongestLeafName(), only_speciation);
    }

    int ConsensusNode::findLongestLeafName() {
        if (isLeaf()) {
            return getName().length();
        }
        int longest_name = 0;
        for (unsigned int i = 0; i < getNumberOfChildren(); i++) {
            longest_name = std::max<int>(getChild(i)->findLongestLeafName(), longest_name);
        }
        return longest_name;
    }

}; //Namespace beep