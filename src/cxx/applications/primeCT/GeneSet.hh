/*
 * File:   GeneSet.hh
 * Author: peter9
 *
 * A GeneSet is a collection of genes. The genes are here 
 * represented by ids only.
 * This class is designed to be memory efficient.
 * Gene ids are taken from a (possibly) larger set of ids. To use this class,
 * the size of the larget set must be specified.~
 * 
 * ~ IMPLEMENTATION NOTE:
 * Maybe this class should be rewritten to just adapt to larger ids as they are 
 * presented to the set for convenience.
 *
 * Created on December 2, 2009, 11:36 AM
 */


/**
 * Overview of members:
 * public:
        GeneSet(unsigned int tno_genes):m_genes(tno_genes, false),m_size(0), m_max_id(0){}
        GeneSet(const GeneSet &cp): m_genes(cp.m_genes), m_size(cp.m_size), m_max_id(cp.m_max_id){}
        void addGene(unsigned int gene_id);
        void addGeneSet(const GeneSet &gs);
        vector<unsigned int> getGeneIDs() const;
        GeneSet getSetDifference(const GeneSet &gs) const;
        GeneSet getSetUnion(const GeneSet &gs) const;
        int size() const {return m_size;}
        bool subsetOf(const GeneSet &rhs) const;
        friend bool operator<(const GeneSet &o1, const GeneSet &o2);
        friend bool operator==(const GeneSet &o1, const GeneSet &o2);
        friend ostream& operator<<(ostream &o, const GeneSet &gs);
    private:
        vector<bool> m_genes;
        unsigned int m_size;
        unsigned int m_max_id;
 */

#ifndef _GENESET_HH
#define	_GENESET_HH

#include "boost/foreach.hpp"
#include <vector>
#define foreach BOOST_FOREACH

namespace beep{

    using namespace std;

    class GeneSet{
    public:
        /**
         * GeneSet
         *
         * Constructor with argument.
         *
         * @param tno_genes Total number of genes in (possibly) larger set of
         *                  ids from which genes in this class are
         *                  representatives of.
         */
        GeneSet(unsigned int tno_genes):m_genes(tno_genes, false),m_size(0), m_max_id(0){}

        /**
         * GeneSet
         *
         * Copy constructor
         *
         * @param cp GeneSet to copy.
         */
        GeneSet(const GeneSet &cp): m_genes(cp.m_genes), m_size(cp.m_size), m_max_id(cp.m_max_id){}

        /**
         * addGene
         *
         * Add a gene to the set.
         *
         * @param gene_id ID of gene to add.
         */
        void addGene(unsigned int gene_id);

        /**
         * addGeneSet
         *
         * Add another set of genes to the set.
         *
         * @param gs Gene set to add
         */
        void addGeneSet(const GeneSet &gs);

        /**
         * getGeneID
         *
         * Returns the set of gene ids stored in the gene set.
         */
        vector<unsigned int> getGeneIDs() const;

        /**
         * getSetDifference
         *
         * Returns the set difference between the gene set and a specified set,
         * i.e. THIS \ gs.
         *
         * @param gs Gene set to subtract.
         */
        GeneSet getSetDifference(const GeneSet &gs) const;

        /**
         * getSetUnion
         *
         * Return the union of this set and a specified set, i.e THIS U gs.
         *
         * @param gs Gene set to unite with.
         */
        GeneSet getSetUnion(const GeneSet &gs) const;

        /**
         * getSize
         *
         * Returns the size of this gene set, i.e. the number of gene ids
         * stored in the set.
         */
        int size() const {return m_size;}

        /**
         * subsetOf
         *
         * Returns true if this set is a subset of a specified gene set, i.e
         * THIS subset of rhs ?
         *
         * @param rhs The (possible) superset.
         */
        bool subsetOf(const GeneSet &rhs) const;

        /**
         * operator <
         *
         * More or less arbitrary order operator. This must be specified to
         * be able to put a gene set (or a virtual vertex, which uses GeneSet)
         * into a set or map in C++.
         */
        friend bool operator<(const GeneSet &o1, const GeneSet &o2);

        /**
         * operator ==
         *
         * Decides if this gene set is equal to another gene set.
         */
        friend bool operator==(const GeneSet &o1, const GeneSet &o2);

        /**
         * operator <<
         *
         * Prints a string representation of this gene set.
         */
        friend ostream& operator<<(ostream &o, const GeneSet &gs);
    private:

        //Bit vector to keep track of genes
        vector<bool> m_genes;

        //Size of gene set, i.e the number of gene ids stored in the set.
        unsigned int m_size;

        //The maximum id stored.
        unsigned int m_max_id;
    };

};
#endif	/* _GENESET_HH */

