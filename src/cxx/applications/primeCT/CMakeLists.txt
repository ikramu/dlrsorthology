include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)

if(Boost_FOUND)

find_library(tmp plot )
if(NOT tmp)
  message(FATAL_ERROR  "libplot not found" )
endif(NOT tmp )

add_executable(${programname_of_this_subdir}
primect.cc
ConsensusTree.cc
ConsensusNode.cc
GeneSet.cc
VirtualVertex.cc
${CMAKE_CURRENT_BINARY_DIR}/cmdline.c   ${CMAKE_CURRENT_BINARY_DIR}/cmdline.h
)


target_link_libraries(${programname_of_this_subdir} prime-phylo prime-phylo-sfile plotter)
install(TARGETS ${programname_of_this_subdir} DESTINATION bin)

else()
  message(STATUS  "Not building primeCT because Boost was not found" )
endif()
