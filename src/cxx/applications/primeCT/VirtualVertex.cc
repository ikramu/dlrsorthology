/* 
 * File:   VirtualVertex.cc
 * Author: peter9
 * 
 * Created on November 30, 2009, 3:53 PM
 */

#include <set>

#include "VirtualVertex.hh"

namespace beep{

VirtualVertex::VirtualVertex(int tno_genes):
m_dp_left(tno_genes),
m_dp_right(tno_genes)
{}

VirtualVertex::VirtualVertex(const VirtualVertex& orig):
m_dp_left(orig.m_dp_left),
m_dp_right(orig.m_dp_right)
{}



bool
operator<(const VirtualVertex &lhs, const VirtualVertex &rhs)
{

    /*Collect gene sets*/
    GeneSet lhs_geneset = lhs.m_dp_left.getSetUnion(lhs.m_dp_right);
    GeneSet rhs_geneset = rhs.m_dp_left.getSetUnion(rhs.m_dp_right);

    /*Compare genesets*/
    if( lhs_geneset < rhs_geneset ){
        return true;
    }
    if( rhs_geneset < lhs_geneset ){
        return false;
    }

    /*
     * Now lhs_geneset == rhs_geneset.
     * Compare left and right descendant parts
     */
    if(lhs == rhs){
        return false;
    }

    /*
     * Here we have the same geneset but the descendant parts differ.
     * We can just compare the right or left parts. Here the left parts
     * are compared.
     */
    return lhs.m_dp_left < rhs.m_dp_left;
}


bool
operator==(const VirtualVertex &lhs, const VirtualVertex &rhs)
{
    /**
     * Two virtual vertices are considered equal if both descendant parts in
     * one virtual vertex have corresponding parts in the other virtual vertex,
     * i.e. we do not really care about "left" or "right" descendant parts.
     */

    //Compare number of descendants
    if( lhs.m_dp_left.size() + lhs.m_dp_right.size() !=
        rhs.m_dp_left.size() + rhs.m_dp_right.size() ){
        return false;
    }

    /*
     * Now both vertices have the same number of descendants.
     * We compare descendant parts.
     */

    //Get descendant parts
    const GeneSet &lhs_left = lhs.m_dp_left;
    const GeneSet &lhs_right = lhs.m_dp_right;
    const GeneSet &rhs_left = rhs.m_dp_left;
    const GeneSet &rhs_right = rhs.m_dp_right;
    
    //Compare "cross-wise"
    if(lhs_left == rhs_left){
        return lhs_right == rhs_right;
    }
    if(lhs_left == rhs_right){
        return lhs_right == rhs_left;
    }

    return false;
}


VirtualVertex::~VirtualVertex() {
}










// *this >= rhs, or rhs subset of *this
bool
VirtualVertex::dominates(const VirtualVertex &rhs) const
{
    //Get gene set unions
    GeneSet lhs_geneset = m_dp_left.getSetUnion(m_dp_right);
    GeneSet rhs_geneset = rhs.m_dp_left.getSetUnion(rhs.m_dp_right);

    return rhs_geneset.subsetOf(lhs_geneset);
}



















ostream&
operator<<(ostream& o, const VirtualVertex &vv)
{
    return o << "[" << vv.m_dp_left << ", " << vv.m_dp_right << "]";
}

}; //Namespace beep