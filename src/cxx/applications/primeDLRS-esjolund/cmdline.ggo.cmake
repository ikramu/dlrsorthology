package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "Guest-in-host tree inference enabling reconciliation analysis using the underlying GSR model"


# This file is not used right now. Read more about how to use this file in 
# src/cxx/README_gengetopt.txt


   
args "--unamed-opts"

option "output-file" o "Output file" string typestr="filename" optional
option "number-of-iterations" i "Number of iterations" int default="1000000" optional
#option "seed" s "Seed for pseudo-random number generator. If set to 0 (default), the process id is used as seed." int default="0" optional
option "seed" s "Seed for pseudo-random number generator. Defaults to random seed" int optional
option "thinning" t "Thinning, i.e. sample every n-th iteration" int default="100" optional

option "print-factor" w "Output diagnostics to stderr every n-th sample" int default="1" optional

option "no-diagnostics" q "Do not output diagnostics" flag off

option "execution-type" m   "Execution type (MCMC, MPIMCMC, posterior density hill-climbing from initial values, or just initial posterior density)"   values="MCMC","MPIMCMC","PDHC","PD"  enum default="MCMC" optional 

option "temperature-factor" T "exponential growth factor for temperatures. Only used together with --execution-type=MPIMCMC. The four temperatures will be 1.0, 1.0*f, 1.0*f*f, 1.0*f*f*f" float default="1.2" optional

option "rescale" r "Rescale the host tree so that the root-to-leaf time equals 1.0. All inferred parameters will refer to the new scale." flag off
option "debuginfo" Q "Show miscellaneous info to stderr before iterating" flag off

option "substitution-model" M   "Substitution model ( The option was previously named -Sm )"   values="UniformAA","JC69","JTT","UniformCodon","ArveCodon"  enum default="JTT" optional 


option "user-defined-substitution-model" u "User-defined substitution model file ( A similar option was previously named -Su )" string typestr="filename" optional


#  -Su <'DNA'/'AminoAcid'/'Codon'> <>
#      . The size of Pi and R must fit data type 
#      (DNA: n=4, AminoAcid: n=20, Codon: n=62).  Don't use both option -Su and -Sm.

option "number-of-steps-of-discretized-gamma-distribution" S "Number of steps of discretized Gamma-distribution for sequence evolution rate variation over sites. Defaults to 1 (no variation). The option was previously named -Sn" int default="1" optional


section "Evolution rate" sectiondesc="Options related to evolution rate"

option "edge-rate-distribution" D "Distribution for iid sequence evolution rate variation over guest tree edges. ( This option was previously named -Ed )"   values="Gamma","InvG","LogN","Uniform"  enum default="Gamma" optional 


# Note, gengetopt can't set default values for multiple(2). We have to handle that in our code instead
option "initial-mean-and-variance-of-sequence-evolution-rate" I "Initial mean and variance of sequence evolution rate. Default is 1.0,1.0.  ( This option was previously named -Ep )" double typestr="FLOAT,FLOAT" multiple(2) optional


option "fix-mean-and-variance-of-evolution-race" F "Fix mean and variance of sequence evolution rate. ( This option was previously named -Ef )" flag off


section "Guest tree topology" sectiondesc="Options related to guest tree topology"


option "initial-guest-tree-topology-file" g "Filename with initial guest tree topology ( This option was previously named -Gi )" string typestr="filename" optional

option "fix-initial-guest-tree-topology" G "Fix initial guest tree topology, i.e. perform no branch-swapping. ( This option was previously named -Gg )" flag off


section "Birth death process" sectiondesc="Options related to the birth death process"

# Note, gengetopt can't set default values for multiple(2). We have to handle that in our code instead



option "fixate-birth-and-death-rate" e "the birth rate and death rates are fixed ( This option was previously named -Bf )" flag off 

# "default" values does not work with "multiple" as of gengetop 2.22.3 ( it seems )
option "birth-and-death-rates" B "start parameter for the birth and death rates. The default rates are 1.0,1.0. ( This option was previously named -Bp )" double typestr="DOUBLE,DOUBLE" multiple(2) optional

option "top-time" x "Override time span of edge above root in host tree. If the value is <=0, the span will be set to equal the root-to-leaf time. Defaults to value in host tree file. This option was previously named -Bt" double optional

section "Time steps" sectiondesc="Options related to time steps" 

option "approximate-discretization-timestep" y "Approximate discretization timestep. Set to 0 to divide every edge in equally many parts. This option was previously named -Dt" double default="0.05" optional

option "minimum-number-of-parts-to-slice-each-edge" k "Minimum number of parts to slice each edge in. If --approximate-discretization-timestep is set to 0, this becomes the exact number of parts. The integer must be 2 or higher. This option was previously named -Di" int default="3" optional



usage "primeDLRS-esjolund [OPTIONS] <seqfile> <hostfile> [<gsfile>]"

description "Model properties:
1) the guest tree topology and its divergence times are modelled with a
   duplication-loss process in accordance with the Gene Evolution Model (GEM).
2) sequence evolution is modelled with a user-defined substitution model.
3) sequence evolution rate variation over guest tree edges (relaxed molecular
   clock) are modelled with iid values from a user-selected distribution.
4) sequence evolution rate variation over sites are modelled according to a
   discretized Gamma distribution with mean 1.
The implementation uses a discretization of the host tree to approximate the
probability of all possible reconciliation realizations for the current parameter
state.

Parameters:
  <seqfile>     (string), file with aligned sequences for guest tree leaves.
  <hostfile>    (string), PrIME Newick file with host tree incl. divergence times.
                          Leaves must have time 0 and root have time > 0.
  <gsfile>      (string), tab-delimited file relating guest tree leaves to host
                          tree leaves if info not included in <hostfile>.

"

text "some text here
"
