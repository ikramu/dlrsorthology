#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

#include "AnError.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "TreeInputOutput.hh"
#include "TreeIOTraits.hh"
#include "StrStrMap.hh"
#include "cmdline.h"

using namespace beep;


void
PrintLeaves(Node *v)
{
  if (v->isLeaf()) {
    cout << v->getName();
  } else {
    PrintLeaves(v->getLeftChild());
    cout << ", ";
    PrintLeaves(v->getRightChild());
  }
}


string 
NodeId(Node *v)
{
  if (v->getName().length() > 0) {
    return v->getName();
  } else {
    ostringstream oss;
    oss << v->getNumber();
    return oss.str();
  }
}


// Print all leave descending from gene nodes in the
// antichain corresponding to x.
// Don't do anything if the set is empty!
void
MakeCut(const GammaMap& gamma, Node *x, string prefix)
{
  SetOfNodes S = gamma.getGamma(x);
  unsigned size = S.size();

  if (size != 0) 
    {
      cout << "Cut " << prefix  << NodeId(x) << ":" << endl;
      //      for (set<Node*>::iterator i = S.getIterator();
      for (unsigned i=0; i<size; i++)
	{
	  PrintLeaves(S[i]);
	  cout << endl;
	}
      cout << "EndCut" << endl;
    } 
}


// x is the root of a (sub-) tree. Cut at x and all interior nodes below x.
void
MakeAllCuts(const GammaMap& gamma, Node *x, string prefix)
{
  if (! x->isLeaf())		// Give up when reaching the leaves
    {
      MakeCut(gamma, x, prefix);
      MakeAllCuts(gamma, x->getLeftChild(), prefix + NodeId(x) + "/" );
      MakeAllCuts(gamma, x->getRightChild(), prefix + NodeId(x) + "/");
    }
}

    
Tree
ReadGuestTree(struct gengetopt_args_info &ai, vector<SetOfNodes> *AC, StrStrMap *gs)
{
  TreeInputOutput io;
  FILE * f;
  f =  fopen(ai.inputs[0], "r");
  if (!f) {
    throw AnError("File not found", ai.inputs[0], 1);
  }

  switch(ai.guest_tree_format_arg)
    {
    case guest_tree_format_arg_primeOrHybrid : 
      io.fromFileStream(f, inputFormatBeepOrHybrid);
      break;
    case guest_tree_format_arg_inputxml :
      io.fromFileStream(f, inputFormatXml);
      break;
    }

  Tree G = io.readGuestTree(AC, gs);

  return G;
}

Tree 
ReadHostTree(struct gengetopt_args_info &ai)
{
  TreeInputOutput io;
  FILE * f = fopen(ai.inputs[1],  "r");
  if (!f) {
    throw AnError("File not found", ai.inputs[0], 1);
  }

  switch(ai.host_tree_format_arg)
    {
    case host_tree_format_arg_primeOrHybrid : 
      io.fromFileStream(f, inputFormatBeepOrHybrid);
      break;
    case host_tree_format_arg_inputxml :
      io.fromFileStream(f, inputFormatXml);
      break;
    }

  Tree H = io.readHostTree();

  return H;
}

int
main (int argc, char **argv) 
{
  
  struct gengetopt_args_info args_info;

  try
    {
      if (cmdline_parser(argc, argv, &args_info) != 0) 
	{
	  exit(1);
	}

      if (args_info.inputs_num != 2)
	{
	  cmdline_parser_print_help();
	  exit(2);
	}

      vector<SetOfNodes> AC(1000); // Unnecessary here
      StrStrMap          gs;

      Tree G = ReadGuestTree(args_info, &AC, &gs);
      Tree S = ReadHostTree(args_info);

      LambdaMap lambda(G, S, gs);
      GammaMap gamma(G, S, lambda, AC);
      Node *x = NULL;

      if (args_info.node_name_given) {
	string nodename = args_info.node_name_arg;
	try {
	  x = S.findLeaf(nodename);
	}
	catch (AnError e) {
	  // Catch the exception that occurs when the name is wrong.
	  // Throw something more appropriate
	  throw AnError("Give node name not found in host tree.", nodename);
	}
	
      } else if (args_info.node_id_given) {
	int cutidx = args_info.node_id_arg;
	if (cutidx > (int) S.getNumberOfNodes()) {
	  throw AnError("Too large node ID!", cutidx);
	} else if (cutidx < 0) {
	  throw AnError("Not a node number", cutidx);
	} else {
	  x = S.getNode(cutidx);
	}      
      }
      
      if (x) {
	// Go cut at given node
	MakeCut(gamma, x, "");
      } else {
	// Cut at all nodes
	MakeAllCuts(gamma, S.getRootNode(), "");
      }
    }
  catch (AnError e) {
    e.action();
  }
  return EXIT_SUCCESS;
}
