#/bin/bash
#
# The main purpose of this file is to show how the test cases are supposed
# be used. All genetrees contains the necessary reconciliation information,
# so there is no  need to call 'reconcile'. 
#


# 2012-04-07 Erik Sjolund tried to run some of the test commands
#
# $ chainsaw r4b sp1		     
# Error:				     
#     TreeIOTraits::enforceGuestTree:  
#     no branch length info in tree    
# $ chainsaw  ex15.gt ex8.st	     
# Error:				     
#     TreeIOTraits::enforceGuestTree:  
#     no branch length info in tree    
# $ chainsaw  ex6.gt ex5.st 	     
# Error:				     
#     TreeIOTraits::enforceGuestTree:  
#     no branch length info in tree    
# $ chainsaw  ex7.gt ex5.st	     
# Error:				     
#     TreeIOTraits::enforceGuestTree:  
#     no branch length info in tree    
#
#
# 2012-04-07 Erik Sjolund moved the test data files to example_data/data_not_in_source_package/chainsaw_testdata
#


echo "--- Test case 1 ---"
chainsaw r4b sp1
primetv -i -f ps r4b sp1 | gv -
echo
echo "--- Test case 2 ---"
chainsaw ex15.gt ex8.st
primetv -i -f ps ex15.gt ex8.st | gv -
echo
echo "--- Test case 3 ---"
chainsaw ex6.gt ex5.st
primetv -i -f ps ex6.gt ex5.st | gv -
echo
echo "--- Test case 4 ---"
chainsaw ex7.gt ex5.st
primetv -i -f ps ex7.gt ex5.st | gv -
