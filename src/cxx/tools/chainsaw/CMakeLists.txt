include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)


add_executable(${programname_of_this_subdir}
chainsaw.cc
${CMAKE_CURRENT_BINARY_DIR}/cmdline.c   ${CMAKE_CURRENT_BINARY_DIR}/cmdline.h 
)
configure_file(chainsaw.1.cmake ${tmp_man_pages_dir}/chainsaw.1 @ONLY)

target_link_libraries(${programname_of_this_subdir} prime-phylo)
install(TARGETS ${programname_of_this_subdir} DESTINATION bin)

