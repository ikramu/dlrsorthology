package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "Cut a gene tree based on a reconciliation."

args "--unamed-opts"

option "guest-tree-format" g "The input format" values="primeOrHybrid","inputxml"  enum default="primeOrHybrid" optional 
option "host-tree-format" h "The input format" values="primeOrHybrid","inputxml"  enum default="primeOrHybrid" optional 

defgroup "Choosing a node"
groupoption "node-name" x "A named species node to cut at" string group="Choosing a node" optional
groupoption "node-id" i "The numerical id of a species node to cut at" int group="Choosing a node" optional

usage "treesize [OPTIONS] <reconciled gene tree> <species tree>"

description "<reconciled gene tree> is the filename of\
the file containing trees in PRIME format.\
Notice that the node id is optional and if left out all species nodes\
will be used recursively."



# This file is not used right now. Read more about how to use this file in 
# src/cxx/README_gengetopt.txt




