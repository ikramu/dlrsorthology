package "prime_s"
version "0.0"
purpose "Finds specificity and sensitivity for orthology estimation using the GSR model."

args "--unamed-opts"

option "silent" s "No diagnostics printed on stdout." flag off
option "output-file" o "Output file for sensitivity and specificity." string typestr="filename" optional

section "MCMC" sectiondesc="Options related to MCMC"
option "iterations" i "Number of iterations in MCMC." int default="1000" optional
option "thinning" j "Thinning, or the number of MCMC iterations to skip before sampling." int default="1" optional
option "timestep" t "Approximate discretization timestep. Set to 0 to divide every edge in equally many parts (see -m)." float default="0.05" optional
option "min-intervals" m "Minimum number of parts to slice each edge in. If -t is set to 0, this becomes the exact number of parts. Minimum 2." int default="3" optional
option "burn-in-iterations" b "The number of burn-in iterations to use for the MCMC-chains. Right now there is no convergence test." int default="2000" optional
option "bdt-burn-in-iterations" d "The number of burn-in iterations to use for the BDT MCMC-chain. Right now there is no convergence test." int default="2000" optional

section "Other" sectiondesc="Other options"
option "substitution-model" u "The substitution model to use." string default="JTT" optional
option "threshold-step" r "The threshold step." float default="0.2" optional
option "nr-samples" n "Number of samples." int default="10" optional

usage "prime_s [OPTIONS] <sequences> <gene tree> <species tree> <gs-map>"

description "Input is supposed to be experimental data.
<sequences> is a file with protein sequences in fasta format with ids consistent with those in <gs-map>.
<gene tree> is the name of a file containing the gene tree.
<species tree> is the name of a file containing the species tree.
<gs-map> is the gene-to-species map."

text "The probability is based on the GSR model. This program follows a test outlined in 'Probabilistic Orthology Analysis' by Sennblad and Lagergren 2009.
500 samples are drawn from the distribution Pr[lambda, my, tau | G, S], where G and S are the input trees. For each sample, a 'true' gene tree is generated which has the same number of leaves as G. For each inner vertex in the 'true' tree we record whether it is a speciation event or a duplication event and then see what the orthology analysis says about the vertex. Specificity and sensitivity are then calculated and stored for a number of thresholds. After the sampling is done the average sensitivity and specificity is calculated and printed to stdout or a specified output file"

