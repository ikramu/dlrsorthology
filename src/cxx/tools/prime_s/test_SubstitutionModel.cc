#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>

#include "CacheSubstitutionModel.hh" //
#include "FastCacheSubstitutionModel.hh" //#include "SubstitutionHandler.hh"
#include "VarRateModel.hh"
#include "ConstRateModel.hh"
#include "EdgeWeightHandler.hh"
#include "MatrixTransitionHandler.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SiteRateHandler.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << "<tree> <data> <alpha> <model> <subclass>\n"
       << "\n"
       << "Parameters:\n"
       << "   <tree>             a string\n"
       << "   <data>             a string\n"
       << "   <alpha>            a double, max 2.0\n"
       << "   <model>            a string, JC69, UniformAA, JTT, \n"
       << "                      UniformCodon, ,ust ,atch datatype\n"
       << "   <subclass>         if 'cache', the CacheSubstitutionModel\n"
       << "                      is used, if 'fast' FastCacheSubstitutionModel\n"
       << "                      is used, else SubstitutionModel is used\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < 5 || argc > 6) 
    {
      usage(argv[0]);
    }

  try
    {
 
      //Get tree and Data
      //---------------------------------------------
      TreeIO treeIF = TreeIO::fromFile(string(argv[1]));
      Tree G = treeIF.readNewickTree();
      cout << G.print(false,false,true,false);
      SequenceData D = SeqIO::readSequences(string(argv[2]));
//       cout << D << endl << D.data4os();
      unsigned ntax = G.getNumberOfLeaves();
      unsigned nchar = D.getNumberOfPositions();

      //Create Q
      //---------------------------------------------
      string arg(argv[4]);
      MatrixTransitionHandler Q = MatrixTransitionHandler::create(arg);
      cout << Q;


      cout << "\nTest av SubstitutionModel\n" 
	   << "-----------------------------\n";
      UniformDensity gd(0, 2.0, true);
      iidRateModel crm(gd, G, G.getLengths());
      //! \todo{ This stupid hack is required because of the antiquated
      //! state of EdgeWeightModel_common has its member variable
      //! edgeRates as an instance rather than as a pointer -- changing 
      //! this requires a whole lot of changes though
      G.setLengths(crm.getWeightVector());
      EdgeWeightHandler ewh(crm); 

      unsigned nCat = 4; 
      UniformDensity ud(0, 10, true);
      ConstRateModel alpha(gd, G, 1.0);
      SiteRateHandler srh(nCat, alpha);
      srh.setAlpha(atof(argv[3]));

      vector<string> partitionList ;
      partitionList.push_back(string("all"));
      SubstitutionModel* SM;
      if(argc == 5)
	{
	  if(string(argv[5]) == "cache")
	    {
	      SM = new CacheSubstitutionModel(D, G, srh, Q, ewh,
					      partitionList);
	    }
	  else if(string(argv[5]) == "fast")
	    {
	      SM = new FastCacheSubstitutionModel(D, G, srh, Q, ewh,
						  partitionList);
	    }
	  else
	    {
	      cerr << argv[5] << "not recognized\n";
	      exit(1);
	    }
	}
      else 
	{
	  SM = new SubstitutionModel(D, G, srh, Q, ewh, partitionList);
	}
      cout << *SM;
      // Perform and time the Likelihood calcualtion
      time_t t0 = time(0);
      clock_t ct0 = clock();

      Probability p;
      for(unsigned i = 0; i < 1; i++) // For time benchmarks
	p = SM->calculateDataProbability();
      
      time_t t1 = time(0);
      clock_t ct1 = clock();
      cout << "lnLikelihood of tree is : "
 	   << p
	   << endl
	   << "Wall time: " << difftime(t1, t0) << " s\n"
	   << endl
	   << "CPU time: " << double(ct1 - ct0)/CLOCKS_PER_SEC << " s\n";

       //Create nexus file for paup if you want to compare with Paup
       //Create a file handle here!!
      ofstream nex("matrix.nex");
      TreeIO treeOF;
      nex << "#NEXUS\n"
	  << "begin data;\n"
	  << "\t dimensions ntax = "<< ntax
	  << " nchar = " << nchar << ";\n"
	  << "format datatype=dna   missing=- equate=\"?=-\" interleave ;\n"
	  << "matrix \n"
	  << D.data4os()
	  << ";\n"
	  << "end;\n"
	  << endl;
     
       nex << "begin trees;\n"
 	   << "tree PAUP1 = "
   	   << TreeIO::writeGuestTree(G)
	   << ";"
	   << "end;\n"
	   << endl;

       nex << "begin paup;\n"
	    << "gettrees storebrlens=yes file=matrix.nex;\n"
	    << "set criterion=likelihood;\n"
	 //	    << "lset nst=6 rmatrix=(1 2 3 3 2 ) basefreq=equal rates=equal userbrlens=yes;\n"
// 	    << "lset nst=1 basefreq=equal"
	   << "lset nst=1 basefreq=equal rates=gamma ncat=" << nCat 
	   << " shape=" << srh.getAlpha()
	   << " userbrlens=yes;\n"
	 //	    << "reconstruct all;\n"
	    << "lscores;\n"
	    << "end;\n"
	    << endl;
       delete SM;
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
//   catch(exception e)
//     {
//       cout <<" error\n"
// 	   << e.what();
//     }
}

