package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "Compute a (new) root for the tree."
    
args "--unamed-opts"

defmode "exhaustive"
defmode "by node index" 
defmode "by outgroup"

option "read-tree-from-stdin"   S  "read tree from STDIN (no <treefile>)" flag off
option "input-format"           f "The input format" values="prime","inputxml"  enum default="prime" optional 

modeoption "exhaustive"         e "Do all possible rootings." flag off mode="exhaustive" 
modeoption "output-file-prefix" o "The stem for filenames" string typestr="file prefix" mode="exhaustive" optional
modeoption "node-index"         n "Reroot tree at edge leading to node <int>" int mode="by node index" optional
modeoption "outgroup"           g "Root tree at smallest subtree containing the leaves listed in a commaseparated arguments" string mode="by outgroup"

usage "reroot [OPTIONS] [<treefile>]"

description "This program reroots the trees in <treefile> on an selected edge.

The default is to output a random rerooting, but you can also choose
to give the edge to put the root on, or an outgroup, or even all
possible rootings.

Output is written to stdout unless the exhaustive rooting is
chosen. Then -o is giving the prefix for filenames to use and the
results are written to <prefix>_<n>.tree, where <n> is the node number
of the head of the edge.

Warning! In the current implementation all time/length info is lost.
"

