.\" Comment: Man page for reroot 
.pc
.TH reroot 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
reroot \- Compute a (new) root for the tree.

.SH SYNOPSIS
.B reroot
[\fIOPTIONS\fR]\fI \fItree-file\fR

.SH DESCRIPTION

This program reroots the trees in <treefile> on an selected edge.

The default is to output a random rerooting, but you can also choose
to give the edge to put the root on, or an outgroup, or even all
possible rootings.

Output is written to stdout unless the exhaustive rooting is
chosen. Then \-o is giving the prefix for filenames to use and the
results are written to <prefix>_<n>.tree, where <n> is the node number
of the head of the edge.

Warning! In the current implementation all time/length info is lost.

.PP

.SH OPTIONS
.TP
.BR \-\-help
Display help
.TP
.BR \-V ", " \-\-version
Print version and exit
.TP
.BR \-S ", " \-\-read\-tree\-from\-stdin 
read tree from STDIN (no <tree-file>) 
.TP
.BR \-f ", " \-\-input\-format " " \fRprime|inputxml
The input format. Default is prime
.TP
.BR \-e ", " \-\-exhaustive
Do all possible rootings
.TP
.BR \-o ", " \-\-output\-file\-prefix " " file-prefix
The stem for filenames
.TP
.BR \-n ", " \-\-node\-index " " \fIINT\fR
Reroot tree at edge leading to node 
.TP
.BR \-g ", " \-\-outgroup " " \fISTRING\fR
Root tree at smallest subtree containing the leaves listed in a commaseparated arguments

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeDLRS (1),
.BR primeGEM (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR showtree (1),
.BR tree2leafnames (1),
.BR treesize (1)
