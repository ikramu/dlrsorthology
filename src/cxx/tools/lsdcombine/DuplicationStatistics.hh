/* 
 * File:   DuplicationStatistics.hh
 * Author: fmattias
 *
 * Created on January 18, 2010, 10:47 AM
 */
#ifndef _DUPLICATIONSTATISTICS_HH
#define	_DUPLICATIONSTATISTICS_HH

#include <map>
#include <stdexcept>

#include "io/LSDEstimatesFormat.hxx"

class DiscPoint;
class DuplicationStatistics;
class DuplicationStatisticsIterator;

/* DiscPoint
 *
 * A class used to store discretization points. To avoid using a map in a map
 * data structure later on, this class contains both the edge id and the
 * discretization point id.
 */
class DiscPoint {
public:
    /**
     * Constructor
     *
     * edge - The id of the edge on which the discretization point resides.
     * discPoint - The id of the disc point.
     *
     * ASSUMPTION: edge > 0 and discPoint > 0.
     */
    DiscPoint(int edge = 0, int discPoint = 0)
    {
        if(edge < 0 || discPoint < 0) {
            throw std::out_of_range("DiscPoint::constructor: Edges and "
                                    "discPoint must be larger than zero");
        }
        
        m_edge = edge;
        m_discPoint = discPoint;
    }

    /**
     * Returns the edge id.
     */
    int getEdge()
    {
        return m_edge;
    }

    /**
     * Returns the id of the disc point.
     */
    int getDiscPoint()
    {
        return m_discPoint;
    }

    /**
     * Sets the time of the disc point.
     */
    void setTime(double time)
    {
        m_time = time;
    }

    /**
     * Returns the time of the disc point.
     */
    double getTime()
    {
        return m_time;
    }

    /**
     * Less than-operator
     *
     * Returns true if this object is less than rightOperand.
     *
     * rightOperand - The object to be compared to.
     */
    bool operator<(const DiscPoint& rightOperand) const
    {
        /* If edges are distinct compare on edge id  */
        if(m_edge != rightOperand.m_edge) {
            return m_edge < rightOperand.m_edge;
        }
        else {
            /* If edges are equal compare on discPoint id */
            return m_discPoint < rightOperand.m_discPoint;
        }
    }
    
private:
    // Id of the edge
    int m_edge;

    // Id of the discretization point
    int m_discPoint;

    // Time of the discretization point
    double m_time;
};

class DuplicationStatistics {
public:
    /* Convenient types */
    typedef std::vector<double> DiscPointData;
    typedef std::map<DiscPoint, DiscPointData> DiscPointMap;

    typedef DuplicationStatisticsIterator iterator;

    /**
     * constructor
     */
    DuplicationStatistics();
    virtual ~DuplicationStatistics();

    /**
     * addDataPoint
     *
     * Adds the data point given by value to the discretization point
     * uniquely identified by edgeId and discPoint.
     *
     * edgeId - Id of the edge on which the discretization point resides.
     * discPoint - Id of the discretization point.
     * time - Time of the discretization point, will only be set the first
     *        time a point is added.
     * value - Any value, usually the expected number of duplications.
     */
    void addDataPoint(int edgeId, int discPoint, double time, double value);

    /**
     * getAverage
     *
     * Returns the average of the data points stored for the discretization
     * point that is uniquely identified by edgeId and discPoint.
     *
     * edgeId - Id of the edge on which the discretization point resides.
     * discPoint - Id of the discretization point.
     *
     * ASSUMPTION: There exists at least one data point.
     */
    double getAverage(int edgeId, int discPoint);

    /**
     * getAverage
     *
     * Returns the standard deviation of the data points stored for the
     * discretization point that is uniquely identified by edgeId and discPoint.
     *
     * edgeId - Id of the edge on which the discretization point resides.
     * discPoint - Id of the discretization point.
     *
     * ASSUMPTION: There exists at least two data points.
     */
    double getStd(int edgeId, int discPoint);

    /**
     * begin
     *
     * Returns an iterator over the edges and discretization points stored
     * in this object. The iterator returns a pair for each discretization
     * point.
     */
    iterator begin();

    /**
     * end
     *
     * Represents the end of the iterator.
     */
    iterator end();

    
    
private:
    // Map from discretization point to data, in this case the data is a
    // vector of points.
    DiscPointMap m_discPointData;
};

class DuplicationStatisticsIterator : public std::iterator< std::forward_iterator_tag, std::pair<int, int> > {
public:
    DuplicationStatisticsIterator();
    
    DuplicationStatisticsIterator(const DuplicationStatistics::DiscPointMap::iterator &discPointMap);

    /**
     * Assignment operator.
     */
    DuplicationStatisticsIterator& operator=(const DuplicationStatisticsIterator& other);

    /**
     * Equal-to operator.
     */
    bool operator==(const DuplicationStatisticsIterator& other) const;

    /**
     * Not-equal-to operator.
     */
    bool operator!=(const DuplicationStatisticsIterator& other) const;

    /**
     * Prefix addition operator.
     */
    DuplicationStatisticsIterator& operator++();

    /**
     * Dereference operator.
     */
    std::pair<int, int> operator*();

    double getTime();

private:
    // The current state of the iterator
    DuplicationStatistics::DiscPointMap::iterator m_current;
 

};

#endif	/* _DUPLICATIONSTATISTICS_HH */

