/* 
 * File:   DuplicationStatistics.cc
 * Author: fmattias
 * 
 * Created on January 18, 2010, 10:47 AM
 */

#include <iostream>
#include <cmath>
#include <stdexcept>

#include "DuplicationStatistics.hh"

using namespace std;

DuplicationStatistics::DuplicationStatistics()
{
}

DuplicationStatistics::~DuplicationStatistics()
{
}

void DuplicationStatistics::addDataPoint(int edgeId, int discPoint, double time, double value)
{
    DiscPoint discPointKey(edgeId, discPoint);

    /* If we haven't seen this point before, add a time to it */
    if(m_discPointData.count(discPointKey) == 0) {
        discPointKey.setTime(time);
    }

    /* Add point to data vector */
    m_discPointData[discPointKey].push_back(value);
}

double DuplicationStatistics::getAverage(int edgeId, int discPoint)
{
    DiscPoint discPointKey(edgeId, discPoint);

    if(m_discPointData[discPointKey].size() <= 0) {
        throw std::domain_error("DuplicationStatistics::getAverage: No data "
                           "available for the specified discretization point.");
    }

    /* Sum the value of each data point */
    double dataSum = 0.0;
    for(unsigned int i = 0; i < m_discPointData[discPointKey].size(); i++) {
        dataSum += m_discPointData[discPointKey][i];
    }

    /* Return the average */
    return dataSum / m_discPointData[discPointKey].size();
}
double DuplicationStatistics::getStd(int edgeId, int discPoint)
{
    DiscPoint discPointKey(edgeId, discPoint);

    if(m_discPointData[discPointKey].size() <= 1) {
        throw std::domain_error("DuplicationStatistics::getStd: Need at least "
                                "two data points to calculate STD.");
    }

    double average = getAverage(edgeId, discPoint);

    /* Compute the square sum (x_i - average)^2 */
    double squareSum = 0.0;
    for(unsigned int i = 0; i < m_discPointData[discPointKey].size(); i++) {
        squareSum += pow(m_discPointData[discPointKey][i] - average, 2.0);
    }

    /* Return the standard deviation */
    return squareSum / (m_discPointData[discPointKey].size() - 1);
}

DuplicationStatisticsIterator DuplicationStatistics::begin()
{
    return DuplicationStatisticsIterator(m_discPointData.begin());
}

DuplicationStatisticsIterator DuplicationStatistics::end()
{
    return DuplicationStatisticsIterator(m_discPointData.end());
}
DuplicationStatisticsIterator::DuplicationStatisticsIterator()
{
    
}

DuplicationStatisticsIterator::DuplicationStatisticsIterator(const DuplicationStatistics::DiscPointMap::iterator &discPointMap_it)
{
    m_current = discPointMap_it;
}

DuplicationStatisticsIterator& DuplicationStatisticsIterator::operator=(const DuplicationStatisticsIterator& other)
{
    if(this != &other) {
        m_current = other.m_current;
    }
    return *this;
}

bool DuplicationStatisticsIterator::operator==(const DuplicationStatisticsIterator& other) const
{
    return m_current == other.m_current;
}

bool DuplicationStatisticsIterator::operator!=(const DuplicationStatisticsIterator& other) const
{
    return !(*this == other);
}

DuplicationStatisticsIterator& DuplicationStatisticsIterator::operator++()
{
    ++m_current;
    return *this;
}

pair<int, int> DuplicationStatisticsIterator::operator*()
{
    DiscPoint current = (*m_current).first;
    return pair<int, int>(current.getEdge(), current.getDiscPoint());
}

double DuplicationStatisticsIterator::getTime()
{
    DiscPoint current = (*m_current).first;
    return current.getTime();
}
