/* A class for plotting 2-dimensional graphs on any plotter.
 *
 * File:   GraphPlotter.hh
 * Author: fmattias
 *
 * Created on January 13, 2010, 1:44 PM
 */

#ifndef _GRAPHPLOTTER_HH
#define	_GRAPHPLOTTER_HH

#include <vector>

#include <plotter.h>

class GraphPlotter {
public:
    /**
     * constructor
     *
     * Constructs a GraphPlotter object that can plot a series of data points
     * in a graph at a specifed position. The data points will be scaled to
     * fit in the graph.
     *
     * width - Width of the graph.
     * height - Height of the graph.
     */
    GraphPlotter(double height, double width);
    GraphPlotter(const GraphPlotter& orig);
    virtual ~GraphPlotter();

    /**
     * addDataPoint
     *
     * Adds a data point to the set of data points for this graph.
     * 
     * x - point on the x-axis.
     * y - point on the y-axis.
     */
    void addDataPoint(double x, double y, double stdY = 0.0);

    /**
     * Plots a graph in the given plotter where the lower left side of the
     * graph starts at (startX, startY).
     *
     * plotter - Plotter where the graph will be plotted.
     * startX - x position of the lower left corner of the graph.
     * startY - y position of the lower left corner of the graph.
     */
    void plotGraph(Plotter &plotter, double startX, double startY);

    void setFontSize(int fontSize);
    void setArrowSize(double arrowSize);
    void setLeftPadding(double leftPadding);
    void setRightPadding(double rightPadding);
    void setName(const std::string &name);

private:
    // Contains the x value of each data point
    std::vector<double> m_xDataPoints;

    // Contains the y value of each data point
    std::vector<double> m_yDataPoints;

    // Contains the standard deviation of each y value
    std::vector<double> m_yStds;

    // Width of the graph
    double m_width;

    // Height of the graph
    double m_height;

    double m_arrowSize;
    int m_fontSize;
    double m_leftPadding;
    double m_rightPadding;

    std::string m_name;

    static const int DEFAULT_FONT_SIZE = 14;
    static const double DEFAULT_ARROW_SIZE = 5.0;
    static const double DEFAULT_LEFT_PADDING = 40.0;
    static const double DEFAULT_RIGHT_PADDING = 40.0;
};

#endif	/* _GRAPHPLOTTER_HH */

