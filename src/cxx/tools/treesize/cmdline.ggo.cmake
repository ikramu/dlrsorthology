package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "Count the leaves of trees."
    
args "--unamed-opts"

option "read-tree-from-stdin" S  "read tree from STDIN (no <treefile>)" flag off
option "input-format" f "The input format" values="beepOrHybrid","inputxml"  enum default="beepOrHybrid" optional 



usage "treesize [OPTIONS] [<treefile>]"

description "<treefile> is the filename of the for the file containing trees in PRIME format. Output is a list of leaf names, one leaf per row. If there are multiple trees in the input file, there will be an empty line separating the leaves."


