#include <iostream>
#include <stdlib.h>

#include "AnError.hh"
//#include "BranchSwapping.hh"
//#include "PRNG.hh"
#include "HybridTreeInputOutput.hh"
#include "HybridTree.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "TreeIOTraits.hh"
#include "cmdline.h"

using namespace std;
using namespace beep;


int
main (int argc, char **argv) 
{
  
  struct gengetopt_args_info args_info;

  try
    {
      if (cmdline_parser(argc, argv, &args_info) != 0) 
	{
	  exit(1);
	}

      if ( args_info.read_tree_from_stdin_flag && args_info.inputs_num != 0 )
	{
	  cerr << "Error: If you specify the flag --read-tree-from-stdin there should not be any unnamed parameters" << endl;
	  exit(1);
	}
      if ( ! args_info.read_tree_from_stdin_flag && args_info.inputs_num != 1 )
	{
	  cerr << "Error: If you don't specify the flag --read-tree-from-stdin there should be one unnamed parameter (the tree filename)" << endl;
	  exit(1);
	}
      
      HybridTreeInputOutput io;
      FILE * f;
      if( args_info.read_tree_from_stdin_flag )
	{ 
	  f = stdin;
	} 
      else
	{ 
	  f=  fopen(args_info.inputs[0],  "r");
	  if (!f) {
	    throw AnError("Could not open tree file.", args_info.inputs[0], 1);
	  }
	}
      switch(args_info.input_format_arg)
	{
	case input_format_arg_beepOrHybrid:
	  {
	    io.fromFileStream(f, inputFormatBeepOrHybrid); 
	    break;
	  }
	case input_format_arg_inputxml: 
	  {
	    io.fromFileStream(f, inputFormatXml);
	    break;
	  }
	}

      TreeIOTraits traits;
      io.checkTagsForTrees(traits);

      if (traits.hasHY())
	{
	  vector<HybridTree> Tvec = io.readAllHybridTrees(traits,0,0);
	  for (unsigned i=0; i<Tvec.size(); i++)
	    {
	      cout << Tvec[i].getNumberOfLeaves() << endl;
	    }
	}
      else
	{
	  vector<Tree> Tvec = io.readAllBeepTrees(traits, 0,0);
	  for (unsigned i=0; i<Tvec.size(); i++)
	    {
	      cout << Tvec[i].getNumberOfLeaves() << endl;
	    }
	}
      
    }      
  catch (AnError e) {
    e.action();
  }
  return EXIT_SUCCESS;
}
