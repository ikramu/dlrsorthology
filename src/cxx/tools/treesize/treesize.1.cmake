.\" Comment: Man page for treesize 
.pc
.TH treesize 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
treesize \- Count the leaves of trees.
.SH SYNOPSIS
.B treesize
[\fIOPTIONS\fR]\fI [\fItreefile\fR]\fR

.SH DESCRIPTION

Count the leaves of trees. Output is a list of leaf names, one leaf per row. If there are multiple trees in the input file, there will be an empty line separating the leaves.

.I treefile
is the filename of the file containing trees in PRIME format. 

.PP

.SH OPTIONS
.TP
.BR \-\-help
Display help (this text).
.TP
.BR \-V ", " \-\-version
Print version and exit
.TP
.BR \-S ", " \-\-read\-tree\-from\-stdin
read tree from STDIN (no <treefile>)
.TP
.BR \-f ", " \-\-input\-format " " \fRbeepOrHybrid|inputxml
The input format. Default is beepOrHybrid

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeDLRS (1),
.BR primeGEM (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR showtree (1)
