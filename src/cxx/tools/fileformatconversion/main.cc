#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <cstring>
#include <libxml/tree.h>

#include "cmdline.h"
using namespace std;

#include "TreeInputOutput.hh"



using namespace beep;

int
main (int argc, char **argv) 
{


  struct gengetopt_args_info args_info;
  FILE * fBeep;
  FILE * fXml;


  if (cmdline_parser(argc, argv, &args_info) != 0) 
   exit(1);


  fBeep = fopen(args_info.beep_filename_arg, "r");


  if (!fBeep) {
    fprintf(stderr, "Could not open tree file '%s' for reading.\n", args_info.beep_filename_arg);
    exit(EXIT_FAILURE);
  }
  fXml = stdout;


    //  assert(fXml);
  TreeInputOutput t;
  t.fromFileStream(fBeep, inputFormatBeepOrHybrid );
  //  t.writeXML(stdout, ! args_info.no_format_xml_flag );

  fclose(fBeep);
  return EXIT_SUCCESS;
}


