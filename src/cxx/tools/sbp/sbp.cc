#include <iostream>

#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "TreeIO.hh"

using namespace std;
using namespace beep;

//
// Global options
//
Real DefaultBirthRate = 0.001;
Real DefaultDeathRate = 0.0012;


void 
usage(char *cmd)
{
  cerr << "Usage: "
       << cmd
       << " [<options>] <time> <int>\n\n"
       << "Species branch probability: Given a branchlength (interpreted as\n"
       << "time) and an integer for the number of children to get over this\n"
       << "time given a single ancestor, what is the probability for this?\n"; 
  
  cerr << "\nOptions:\n\
   -u, -h     This text.\n\
\n\
   Parameters:\n\
   -r <birthrate> <deathrate>\n\
              Start values for birth and death rate. Without this option, a\n\
              rough guess is made regarding good start values.\n\
";
    exit(1);
}








int
main (int argc, char **argv) {
  double BirthRate = DefaultBirthRate;
  double DeathRate = DefaultDeathRate;
  bool MustChooseRates = true;
  int nChildren = -1;
  float branchlength=0.0;

  if (argc < 3) {
    usage(argv[0]);
  }

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') {
    switch (argv[opt][1]) {
    case 'h':
    case 'u':
      usage(argv[0]);
      break;


    case 'r':
      {
	if (++opt < argc) 
	  {
	    BirthRate = atof(argv[opt]);
	    if (++opt < argc) 
	      {
		DeathRate = atof(argv[opt]);
	      }
	    else
	      {
		cerr << "Error: Expected a gene loss rate (death rate)\n";
		usage(argv[0]);
	      }
	    MustChooseRates = false;
	  }
	else
	  {
	    cerr << "Expected gene duplication and loss rates (birth and death rates) for option '-r'!\n";
	    usage(argv[0]);
	  }
      }
      break;

    default:
      cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
      usage(argv[0]);
    }

    opt++;
  }

  sscanf(argv[opt++], "%f", &branchlength);
  sscanf(argv[opt++], "%d", &nChildren);

  try 
    {
      // Phony tree, since BirthDeathProbs requires it
      Tree S = Tree::EmptyTree(branchlength); 
      BirthDeathProbs bdp(S, BirthRate, DeathRate);
      Node* u = S.getRootNode();
      
      cout.precision(10);
      cout << bdp.partialProbOfCopies(*u, nChildren).val()
	   << endl;
    }
  catch (AnError e) 
    {
      e.action();
    }

 
  return EXIT_SUCCESS;
}

