.\" Comment: Man page for tree2leafnames 
.pc
.TH tree2leafnames 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
tree2leafnames \- List the leaves of a tree
.SH SYNOPSIS
.B tree2leafnames
[\fIOPTIONS\fR]\fI [\fItreefile\fR]\fR

.SH DESCRIPTION

List the leaves of a tree

.I treefile
is the filename of the file containing trees in PRIME or XML format. 

.PP

.SH OPTIONS
.TP
.BR \-\-help
Display help (this text).
.TP
.BR \-V ", " \-\-version
Print version and exit
.TP
.BR \-f ", " \-\-input\-format " " \fRprimeOrHybrid|inputxml
The input format. Default is primeOrHybrid

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeDLRS (1),
.BR primeGEM (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR showtree (1),
.BR treesize (1)
