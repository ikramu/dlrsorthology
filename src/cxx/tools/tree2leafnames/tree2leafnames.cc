#include <iostream>
#include <cstdlib>

#include "AnError.hh"
#include "Beep.hh"
#include "Node.hh"
#include "Tree.hh"
#include "HybridTreeInputOutput.hh"
#include "HybridTree.hh"
#include "TreeIOTraits.hh"
#include "cmdline.h"


using namespace std;
using namespace beep;


void
FindLeaves(Node* v) {
  if (v->isLeaf()) {
    cout << v->getName() << "\n";
  } else {
    FindLeaves(v->getLeftChild());
    FindLeaves(v->getRightChild());
  }
}


int
main (int argc, char **argv) 
{
  struct gengetopt_args_info args_info;

  try {
    if (cmdline_parser(argc, argv, &args_info) != 0) 
      {
	exit(1);
      }

    if ( args_info.inputs_num != 1 )
      {
	throw AnError("A file name argument is required.", 1);
      }

    HybridTreeInputOutput io;
    FILE * f = fopen(args_info.inputs[0],  "r");
    if (!f) {
      throw AnError("Could not open tree file.", args_info.inputs[0]);
    }

    switch(args_info.input_format_arg)
      {
      case input_format_arg_primeOrHybrid:
	{
	  io.fromFileStream(f, inputFormatBeepOrHybrid); 
	  break;
	}
      case input_format_arg_inputxml: 
	{
	  io.fromFileStream(f, inputFormatXml);
	  break;
	}
      }
    
    TreeIOTraits traits;
    io.checkTagsForTrees(traits);
    
    if (traits.hasHY())
      {
	vector<HybridTree> Tvec = io.readAllHybridTrees(traits,0,0);
	for (unsigned i=0; i<Tvec.size(); i++)
	  {
	    Node *root = Tvec[i].getRootNode();
	    FindLeaves(root);
	    if (i + 1 != Tvec.size())
	      {
		cout << endl;
	      }
	  }
      }
    else
      {
	vector<Tree> Tvec = io.readAllBeepTrees(traits, 0,0);
	for (unsigned i=0; i<Tvec.size(); i++)
	  {
	    Node *root = Tvec[i].getRootNode();
	    FindLeaves(root);
	    if (i + 1 != Tvec.size())
	      {
		cout << endl;
	      }
	  }
      }
  }
  catch (AnError e) {
    e.action();
  }
  return EXIT_SUCCESS;
}
