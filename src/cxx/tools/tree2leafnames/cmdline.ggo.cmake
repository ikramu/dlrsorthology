package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "List the leaves of a tree."
    
args "--unamed-opts"

option "input-format" f "The input format" values="primeOrHybrid","inputxml"  enum default="primeOrHybrid" optional 


usage "treesize [OPTIONS] [<treefile>]"

description "<treefile> is the filename of the for the file containing trees in PRIME or XML format. "


