#include <iostream>
#include <sys/time.h>
#include <time.h>
#include <Beep.hh>
#include <DLRSOrthoCalculator.hh>

#include "OrthoUtils.hh"

/* Namespaces used */
using namespace std;
using namespace beep;
using namespace boost::algorithm;

int main(int argc, char *argv[]) {

    if (argc != 6) {
        cerr << "\nerror: wrong number of arguments\n" << endl;
        cerr << "usage: ortho_true_trees posterior_file specie_file true_tree" << endl;
        cerr << "posterior_file: DLRS posterior file containing samples from Pr[G,l,\\theta|D,S]" << endl;
        cerr << "burn_in: number of samples to be left as burn in" << endl;
        cerr << "specie_file: specie tree (a.k.a S in above formula)" << endl;
        cerr << "true_tree: true reconciliation for synthetic gene tree made of leaves represented by synthetic sequences (a.k.a D in above)" << endl;
        cerr << "out_file: output file where orthology probabilities will be written" << endl;
        cerr << endl;
        exit(1);
    }

    //string specie_tree = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/mhcLasse.stree";
    //string true_tree = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/g95.true";
    //string posterior_file = "/home/ikramu/PrIME/trunk/src/cxx/tools/ortho_true_vertices/data/g95.all.test";
    timespec ts, ts_after;
    
    
    string posterior_file = argv[1];
    int burn_in_val = atoi(argv[2]);
    string specie_tree = argv[3];
    string true_tree = argv[4];   
    string out_file = argv[5];
    
    OrthoUtils outils(burn_in_val);

    /*
    vector<string> lst;
    lst.push_back("gene_homo_1");
    lst.push_back("gene_mus_1");
    lst.push_back("gene_homo_2");
    lst.push_back("gene_mus_2");
    lst.push_back("gene_mus_3");

    outils.print_vector(lst);
    cout << endl;
    std::sort(lst.begin(), lst.end());
    outils.print_vector(lst);
    */
    
    vector<map<string, double> > ortho_list;

    TreeIO tio = TreeIO::fromFile(specie_tree);
    Tree sptree = tio.readHostTree();
    
    clock_gettime(CLOCK_REALTIME, &ts);
    cout << "Going through the posterior file" << endl;
    outils.get_num_of_samples(posterior_file);
    clock_gettime(CLOCK_REALTIME, &ts_after);
    cout << (ts_after.tv_sec - ts.tv_sec) << " seconds and " << (ts_after.tv_nsec - ts.tv_nsec) << " nano-seconds" << endl;    
    
    //vector<string> leaves = outils.get_leaves_from_true_file(true_tree);
    vector<string> leaves = outils.get_gene_pair_from_true_tree(true_tree);
    outils.set_orthology_map(leaves);
    outils.print_vector(leaves);

    outils.estimate_orthologies(posterior_file, true_tree, specie_tree, leaves, ortho_list);

    cout << "total samples are " << ortho_list.size() << endl;

    map<string, double> norm_ortho = outils.normalize_orthologies(ortho_list);
    outils.write_orthologies(norm_ortho, out_file);
    return 0;
}



