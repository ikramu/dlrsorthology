/* 
 * File:   OrthoUtils.hh
 * Author: ikramu
 *
 * Created on October 22, 2012, 1:19 PM
 */

#ifndef ORTHOUTILS_HH
#define	ORTHOUTILS_HH

#include <iostream>
#include <Beep.hh>
#include <DLRSOrthoCalculator.hh>

// maximum size of characters in mcmc sample
#define SAMPLE_LENGTH 3000

/* Namespaces used */
using namespace std;
using namespace beep;
using namespace boost::algorithm;

class OrthoUtils {
public:
    OrthoUtils();
    OrthoUtils(int b);
    OrthoUtils(const OrthoUtils& orig);
    virtual ~OrthoUtils();

    
    vector<string> get_leaves_from_true_file(string true_tree);
    
    map<string, double> compute_ortho_single(string, string, double,
            double, double, double, vector<string>);
    
    void estimate_orthologies(string, string, string, vector<string>, vector<map<string, double> > &);
    
    double estimate_time_left(long nanoseconds, int count, int total_iterations);
    
    void get_num_of_samples(string posterior_file);
    
    void get_data_from_sample(string, string &, double &, double &, double &, double &);
    
    string get_days_hours_from_seconds(double seconds);
    
    map<string, double> normalize_orthologies(vector<map<string, double> >);
    
    void write_orthologies(map<string, double>, string outfile);
    
    vector<string> get_gene_pair_from_true_tree(string true_file);
    
    void print_map(map<string, double> tmap);
    
    void print_vector(vector<string> vec); 
    
    void set_orthology_map(vector<string> pairs);
    
private:    
    bool not_same_specie(string left_gene, string right_gene);
    string get_specie_from_gene_name(string gene);
    int total_samples;
    int BURN_IN;
    
    map<string, double> orthology_map;

};

#endif	/* ORTHOUTILS_HH */

