package "genGSR"
version "0.1.0"
purpose "Generates sequences and gene trees for the GSR model with given parameters."

args "--unamed-opts"

option "birth-rate" b "The birth rate of the underlying birth-death process." float
option "death-rate" d "The death rate of the underlying birth-death process." float
option "top-time" t "The time of the top edge in the gene tree. If the top time is set to 0.0 then the top edge is not modelled." float default="0.0" optional
option "sequence-length" l "The length of the sequences." int

option "number-of-categories" c "The number of categories, intervals in which the probability is the same, discretized gamma distribution. See Yang (1994)." int default="1" optional
option "alpha" a "The shape parameter for the gamma distribution used to generate site rates." float default="1.0" optional

option "density-function" r "The density functions used to generate edge rates." values="GAMMA","INV","NLOG","UNIFORM"  string default="GAMMA" optional
option "mean" m "The mean of the distribution function used to generate edge rates." float default="1.0" optional
option "variance" v "The variance of the distribution function used to generate edge rates." float default="0.5" optional

option "output-sequence-file" o "The file where the generated sequences are written. If no file is specified then the output is written to stdout." string typestr="filename" optional
option "output-gene-tree-file" g "The file where the generated gene tree is written. If no file is specified then the gene tree is not shown." string typestr="filename" optional
option "output-gs-map-file" u "The file where the map that tells which species each gene belong to is written. If no file is specified then the map is not written." string typestr="filename" optional
option "substitution-model" s "The substitution model used for evolving sequences, default JC69." values="ArveCodon","JC69","JTT","UniformAA","UniformCodon" string default="JC69" optional
option "output-directory" i "The sequences, gene tree and gs is written to the specified directory, the names are seqs, g and gs respectively. Note that this option overrides o, g and u." string typestr="filename" optional

option "max-number-of-tries" x "Sets the maximum number of tries when trying to generate a tree." int default="500" optional
option "min-number-of-leaves" y "Sets the minimum number of allowed leaves" int default="2" optional
option "max-number-of-leaves" z "Sets the maximum number of allowed leaves" int default="500" optional
option "seed" e "Sets the seed for the pseudo random number generator." long optional

option "generate-ancestral" p "If flag is set, ancestral sequences are also generated." flag off

usage "genGSR [OPTIONS] -b <birth rate> -d <death rate> -n <number of sequences> -l <sequence length> <species tree> [lsd probabilities]"

description "<species tree> is the species tree where a gene tree will evolve. <birth rate> is the birth rate of the linear birth-death process generating gene trees, correspondingly for the <death rate>."

text "some text here"



