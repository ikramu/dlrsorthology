#include <fstream>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "BranchSwapping.hh"
#include "GuestTreeModel.hh"
#include "LambdaMap.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"


using namespace beep;
using namespace std;

std::vector<Node*> getDescendentNodes(Node* n);
std::vector<Node*> getDescendentNodeRecursive(Node* n);
void rescale_specie_tree(Tree *S);
bool isObligateDuplication(Node* node);
void populateGsMap(Tree gtree, StrStrMap &map);
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems);
std::vector<std::string> split(const std::string &s, char delim);

int
main(int argc, char** argv) {

    if (argc < 4 || argc > 6) {
        cerr << "usage: test_GuestTreeModel <Guesttree> <HostTree> <birthrate> <deathrate> [<toptime>]\n";
        exit(1);
    }

    //Get tree and Data
    //---------------------------------------------
    string guest(argv[1]);
    TreeIO io = TreeIO::fromFile(guest);
    //StrStrMap gs;
    StrStrMap *gs = new StrStrMap();

    Tree G = io.readBeepTree(0, gs);
    G.setName("G");

    string host(argv[2]);
    TreeIO io2 = TreeIO::fromFile(host);
    Tree S = io2.readHostTree(); // Reads times?
    S.setName("S");
    S.rescale_specie_tree();
    
    cout << S << endl;

    char filename[800] = {0};
    strcat(filename, guest.data());
    strcat(filename, ".gemcomputed");
    ofstream outfile;
    outfile.open(filename);

    string gtname = "";

    size_t pos = guest.find_last_of("/");
    if (pos != std::string::npos)
        gtname.assign(guest.begin() + pos + 1, guest.end());
    cout << "Input gene tree name is " << gtname << endl;

    ostringstream gsfile;
    gsfile << argv[2] << "_" << gtname << ".gs" << endl;
    string gsfilename = gsfile.str();

    // trim any spaces in start or end of the filepath
    gsfilename.erase(gsfilename.find_last_not_of(" \n\r\t") + 1);

    //if (gs.size() == 0) {
    //    gs = TreeIO::readGeneSpeciesInfo(gsfilename);
    //}
    populateGsMap(G, *gs);

    LambdaMap lambda(G, S, *gs);

    Real brate = atof(argv[3]);
    Real drate = atof(argv[4]);
    Real topTime = 1.0; //atof(argv[5]);
    if (argc == 6)
        topTime = atof(argv[5]);
    BirthDeathProbs bdp(S, brate, drate, &topTime);

    GuestTreeModel a(G, *gs, bdp);

    Probability prG = a.calculateDataProbability();
    prG = 0;
    ProbVector prOrtho(G);
    GuestTreeModel g(a);

    prG += g.calculateDataProbability();
    for (unsigned i = 0; i < G.getNumberOfNodes(); i++) {
        Node* u = G.getNode(i);
        if (u->isLeaf() == false) { // && lambda[u]->isLeaf() == false) {
            a.setOrthoNode(u);
            prOrtho[u] += a.calculateDataProbability();
        }
    }

    for (unsigned i = 0; i < G.getNumberOfNodes(); i++) {
        Node* u = G.getNode(i);
        if (!isObligateDuplication(u)) { //&& lambda[u]->isLeaf() == false) {
            vector<Node*> lnodes = getDescendentNodes(u->getLeftChild());
            vector<Node*> rnodes = getDescendentNodes(u->getRightChild());
            ostringstream os;
            os << "[" << lnodes[0]->getName();
            for (unsigned i = 1; i < lnodes.size(); i++)
                os << " " << lnodes[i]->getName();
            os << ",";
            for (unsigned i = 0; i < rnodes.size(); i++)
                os << " " << rnodes[i]->getName();
            os << "]";
            //cout << os.str() << endl;
            outfile << os.str() << "\t" << (prOrtho[u] / prG).val() << endl;
            cout << os.str() << "\t" << (prOrtho[u] / prG).val() << endl;
        }
    }

    outfile.close();
    cout << "Done..." << endl;
    cout << "Computed orthologies are written to " << filename << endl;

    return (0);
}

bool isObligateDuplication(Node* node) {
    if(node->isLeaf())
        return true;
    vector<Node*> leafNodes = getDescendentNodes(node);
    string firstLeaf;
    for (int i = 0; i < leafNodes.size(); i++) {
        std::vector<std::string> elems = split(leafNodes[i]->getName(), '_');
        if (i == 0)
            firstLeaf = elems[1];
        else {
            if (firstLeaf != elems[1])
                return false;
        }
    }    
    return true;
}

std::vector<Node*> getDescendentNodes(Node* n) {
    return getDescendentNodeRecursive(n);
}

std::vector<Node*> getDescendentNodeRecursive(Node* n) {
    std::vector<Node*> nodes; // = new std::vector<Node*>();
    if (n->isLeaf()) {
        nodes.push_back(n);
        return nodes;
    } else {
        std::vector<Node*> lnodes = getDescendentNodeRecursive(n->getLeftChild());
        std::vector<Node*> rnodes = getDescendentNodeRecursive(n->getRightChild());
        lnodes.insert(lnodes.end(), rnodes.begin(), rnodes.end());
        return lnodes;
    }
}

/**
 * 
 * @param gtree Gene Tree
 * @param map Map object to be populated
 */
void populateGsMap(Tree gtree, StrStrMap &map) {
    std::vector<Node*> nodes = gtree.getAllNodes();

    map.clearMap();
    for (int i = 0; i < gtree.getNumberOfNodes(); i++) {
        if (nodes[i]->isLeaf() && nodes[i] != NULL) {
            std::vector<std::string> elems = split(nodes[i]->getName(), '_');
            //cout << nodes[i]->getName() << " , " << elems[1] << endl;
            map.insert(nodes[i]->getName(), elems[1]);
        }
    }
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

/**
 * rescale_specie_tree
 * Rescales a species tree to [0,1].
 *
 * @param S The specie tree
 */
void rescale_specie_tree(Tree *S) {
    Real sc = S->rootToLeafTime();
    beep::RealVector* tms = new beep::RealVector(S->getTimes());
    for (beep::RealVector::iterator it = tms->begin(); it != tms->end(); ++it) {
        (*it) /= sc;
    }
    S->setTopTime(S->getTopTime() / sc);
    S->setTimes(*tms, true);
    cout << "Specie tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
}