#include <iostream>

using namespace std;

#include "AnError.hh"
#include "LambdaMap.hh"
#include "TreeIO.hh"
using namespace beep;

void
usage(char *s)
{
  cerr << "Usage: "
       << s
       << " [options] <reconciled gene tree> <species tree>\n"
       << "\nThis program outputs a table describing the reconciliation embedded in the"
       << "input tree.\n"
       << "\noptions:\n"
       << " -f              output full reconciliation. Default is to output\n"
       << "                 reduced reconciliation\n"
       << endl;
}

int
main (int argc, char **argv) 
{
  

  if (argc < 3 || argc > 5)
    {
      usage(argv[0]);
      exit (1);
    }

  try 
    {
      unsigned opt = 0;
      bool full = false;
      if(argc != 3)
	{
	  if(string(argv[++opt]) != "-f")
	    {
	      usage(argv[0]);
	      exit(1);
	    }
	  full = true;
	}
      
      vector<SetOfNodes> AC(1000); // Unnecessary here
      StrStrMap          gs;
      
      TreeIO io = TreeIO::fromFile(argv[++opt]);
      Tree G = io.readGuestTree(&AC, &gs);
      
      io.setSourceFile(argv[++opt]);
      Tree S = io.readNewickTree();
      
      LambdaMap lambda(G, S, gs);
      GammaMap gamma(G, S, lambda, AC);
      
      cout << gamma.print(full) << endl;
    }
  catch (AnError e) {
    e.action();
  }
  return EXIT_SUCCESS;
}
