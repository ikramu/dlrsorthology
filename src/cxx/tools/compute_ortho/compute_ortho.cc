#include <iostream>
#include <cstdlib>
#include <map>
#include <boost/algorithm/string.hpp>
#include <fstream>

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "GammaDensity.hh"
#include "StrStrMap.hh"
#include "EdgeDiscGSR.hh"
#include "SetOfNodes.hh"
#include "Probability.hh"

/* Namespaces used */
using namespace std;
using namespace beep;
using namespace boost::algorithm;

// function signatures
Tree read_species_tree(string spfile);
void rescale_specie_tree(Tree *S);
void create_disc_tree(Tree &species_tree, EdgeDiscTree **DS);
void computeAndWriteOrthologies(EdgeDiscGSR *gsr, string geneFileName);
Node* find_lca(SetOfNodes &nodes, Tree &T);
std::vector<Node*> getDescendentNodes(Node* n);
std::vector<Node*> getDescendentNodeRecursive(Node* n);
void create_lookup_tables(map<int, string> &ID, map<string, int> &inv_ID, StrStrMap &gs_map);
void calc_speciation_single(EdgeDiscGSR *gsr, map<int, string> &ID, char *outfile);
void printVector(vector<Node*> v);
vector<unsigned> getIdsFromNodes(vector<Node*> lnodes, StrStrMap gsmap);
bool isObligateDuplication(Node *lca, LambdaMap sigma);
void populateGsMap(Tree t, StrStrMap &map);
std::vector<std::string> split(const std::string &s, char delim);
void read_leaves_from_file(string path, vector<string> &msn);
void findNonObligateVertices(string gFilePath, string sFilePath, vector<string> &leaves);

int main(int argc, char *argv[]) {

    if (argc != 7) {
        cout << "Usage: compute_ortho <geneTreeName> <specieTreeName> <mean_rate> <var_rate> <dup_rate> <loss_rate>" << endl;
        exit(EXIT_FAILURE);
    }

    string gg(argv[1]);
    string ss(argv[2]);
    vector<string> ortho_nodes;
    
    Tree species_tree; //The species tree
    StrStrMap *gsMap = new StrStrMap();
    GammaDensity *gamma;
    EdgeDiscBDProbs *bd_probs;
    EdgeDiscTree *DS;
    species_tree = read_species_tree(argv[2]);
    create_disc_tree(species_tree, &DS);
    //cout << species_tree << endl;

    TreeIO tio = TreeIO::fromFile(argv[1]);
    Tree gene_tree = tio.readBeepTree(NULL, gsMap);
    //rescale_specie_tree(&species_tree);

    string gtfullname(argv[1]);
    string gtname = "";

    size_t pos = gtfullname.find_last_of("/");
    if (pos != std::string::npos)
        gtname.assign(gtfullname.begin() + pos + 1, gtfullname.end());
    else 
        gtname = argv[1];
    cout << "Input gene tree name is " << gtname << endl;

    //vector<string> mprSpecNodes;
    //findNonObligateVertices(argv[1], argv[2], mprSpecNodes);
    
    ostringstream gsfile;
    //cout << argv[2] << "_" << gtname << ".gs" << endl;
    gsfile << argv[2] << "_" << gtname << ".gs" << endl;
    string gsfilename = gsfile.str();

    trim(gsfilename);
    cout << gsfilename << endl;

    //*gsMap = tio.readGeneSpeciesInfo(gsfilename);
    //cout << "Before " << gsMap->size() << endl;
    populateGsMap(gene_tree, *gsMap);
    //cout << "After " << gsMap->size() << endl;
    //cout << *gsMap << endl;

    gamma = new GammaDensity(atof(argv[3]), atof(argv[4]));
    bd_probs = new EdgeDiscBDProbs(DS, atof(argv[5]), atof(argv[6]));

    EdgeDiscGSR *gsr;
    gsr = new EdgeDiscGSR(&gene_tree, DS, gsMap, gamma, bd_probs);

    computeAndWriteOrthologies(gsr, argv[1]);
}

/**
 * read_species_tree
 * Read a species tree from file.
 *
 * @param args_info Program arguments
 */
Tree read_species_tree(string spfile) {
    TreeIO tree_reader = TreeIO::fromFile(spfile);
    Tree stree = tree_reader.readHostTree();
    rescale_specie_tree(&stree);
    return stree;
}

/**
 * rescale_specie_tree
 * Rescales a species tree to [0,1].
 *
 * @param S The specie tree
 */
void rescale_specie_tree(Tree *S) {
    Real sc = S->rootToLeafTime();
    beep::RealVector* tms = new beep::RealVector(S->getTimes());
    for (beep::RealVector::iterator it = tms->begin(); it != tms->end(); ++it) {
        (*it) /= sc;
    }
    S->setTopTime(S->getTopTime() / sc);
    S->setTimes(*tms, true);
    cout << "Specie tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
}

void findNonObligateVertices(string gFilePath, string sFilePath, vector<string> &leaves) {
    string dirName = "";
    string geneTree = "";
    //string specieTree = "abca.stree";
    string specieTree = sFilePath;
    string gsFile = "";
    string mprOutFile = "/tmp/mpr.out";
    size_t pos = gFilePath.find_last_of("/");
    if (pos != std::string::npos){
        dirName.assign(gFilePath.begin(), gFilePath.begin() + pos + 1);
        geneTree.assign(gFilePath.begin() + pos + 1, gFilePath.end());
    }
    pos = geneTree.find_last_of(".");
    geneTree.assign(geneTree.begin(), geneTree.begin() + pos);
    pos = geneTree.find_last_of(".");
    geneTree.assign(geneTree.begin(), geneTree.begin() + pos);
    
    pos = sFilePath.find_last_of("/");
    if (pos != std::string::npos){
        //dirName.assign(sFilePath.begin(), gFilePath.begin() + pos + 1);
        specieTree.assign(sFilePath.begin() + pos + 1, sFilePath.end());
    }
    
    gsFile = specieTree + "_" + geneTree + ".tree.gs";
    //gsFile.assign(strcat(gsFile.c_str(),geneTree.c_str()));
    
    string command = "mprOrthology " + dirName + geneTree + ".tree ";
    command += dirName + specieTree + " " + dirName + gsFile + " " + mprOutFile;    
    system(command.c_str());
    
    TreeIO tio = TreeIO::fromFile(mprOutFile);    
    Tree G = tio.readBeepTree(NULL, new StrStrMap());
    vector<Node*> all_nodes = G.getAllNodes();
    
    //read_leaves_from_file(mprOutFile);

    for (unsigned i = 0; i < all_nodes.size(); i++) {
        Node* node = all_nodes.at(i);
        if (node == NULL || node->isLeaf()) {
            continue;
        } else {
            vector<Node*> ch = getDescendentNodes(node);
            string leaves = "";
            for(vector<Node*>::iterator i = ch.begin(); i < ch.end(); i++)
                leaves = leaves + " " + (*i)->getName();
            cout << leaves << endl;
        }
    }
    cout << "MPR tree created" << endl;
}

void read_leaves_from_file(string path, vector<string> &mprSpecNodes) {
    ifstream infile;
    infile.open(path.c_str());
    while (!infile.eof()) {
        string line;
        getline(infile, line);
        size_t pos = line.find_last_of("\t");
        if (pos != std::string::npos)
            line.assign(line.begin()+1, line.begin() + pos-1);
        cout << line << endl;
        mprSpecNodes.push_back(line);
    }
}

/**
 * 
 * @param gtree Gene Tree
 * @param map Map object to be populated
 */
void populateGsMap(Tree gtree, StrStrMap &map) {
    std::vector<Node*> nodes = gtree.getAllNodes();

    map.clearMap();
    for (unsigned int i = 0; i < gtree.getNumberOfNodes(); i++) {
        if (nodes[i]->isLeaf() && nodes[i] != NULL) {
            std::vector<std::string> elems = split(nodes[i]->getName(), '_');
            cout << nodes[i]->getName() << " , " << elems[1] << endl;
            map.insert(nodes[i]->getName(), elems[1]);
        }
    }
}

/**
 * create_disc_tree
 * Creates a discretized species tree from a species tree.
 *
 * @param species_tree The species tree
 * @param DS At return: Pointer to the discretized species tree
 * @param args_info Program arguments
 *
 */
void create_disc_tree(Tree &species_tree, EdgeDiscTree **DS) {
    float timestep; //Timestep for discretized tree
    int min_intervals; //Minimum number of intervals in DS
    EdgeDiscretizer *disc; //Used for creating discretized trees

    //Set timestep and min_interval parameters
    timestep = 0;
    min_intervals = 10;

    //cout << species_tree << endl;

    //Create discretized tree
    if (timestep == 0) {
        disc = new EquiSplitEdgeDiscretizer(min_intervals);
    } else {
        disc = new StepSizeEdgeDiscretizer(timestep, min_intervals);
    }
    *DS = new EdgeDiscTree(species_tree, disc);

    delete disc;
}

void computeAndWriteOrthologies(EdgeDiscGSR *gsr, string geneFileName) {
    Tree G = gsr->getTree();
    StrStrMap gs_map = gsr->getGSMap();
    std::vector<Node*> nodeList = G.getAllNodes();
    char outfile[800] = {0};
    //cout << "gene file name would be " << geneFileName.data() << endl;
    strcat(outfile, geneFileName.data());
    strcat(outfile, ".dlrscomputed");

    map<int, string> ID; //Lookup table for id-gene mapping
    map<string, int> inv_ID; //Lookup table for gene-id mapping

    create_lookup_tables(ID, inv_ID, gs_map);

    cout << "Computing orthology of input file..." << endl;
    calc_speciation_single(gsr, ID, outfile);
    cout << "Done..." << endl;
    cout << "Computed orthologies are written to " << outfile << endl;
}

/**
 * find_lca
 * Finds and returns the lca node of a set of nodes.
 *
 * @param nodes A set of nodes
 * @param T Tree in which the nodes are found.
 *
 */
Node* find_lca(SetOfNodes &nodes, Tree &T) {
    if (nodes.size() > 0) {
        Node *lca = nodes[0];
        for (unsigned int i = 1; i < nodes.size(); i++) {
            lca = T.mostRecentCommonAncestor(lca, nodes[i]);
        }
        return lca;
    }
    return NULL;
}

std::vector<Node*> getDescendentNodes(Node* n) {
    return getDescendentNodeRecursive(n);
}

std::vector<Node*> getDescendentNodeRecursive(Node* n) {
    std::vector<Node*> nodes; // = new std::vector<Node*>();
    if (n->isLeaf()) {
        nodes.push_back(n);
        return nodes;
    } else {
        std::vector<Node*> lnodes = getDescendentNodeRecursive(n->getLeftChild());
        std::vector<Node*> rnodes = getDescendentNodeRecursive(n->getRightChild());
        lnodes.insert(lnodes.end(), rnodes.begin(), rnodes.end());
        return lnodes;
    }
}

void calc_speciation_single(EdgeDiscGSR *gsr,
        map<int, string> &ID, char *filename) {
    Tree G = gsr->getTree();
    Tree S = gsr->getDiscretizedHostTree()->getTree();
    StrStrMap gsMap = gsr->getGSMap();
    ofstream outfile;
    outfile.open(filename);

    cout << G << endl;

    map<int, Node*> gs_node_map;
    for (unsigned int i = 0; i < ID.size(); i++) {
        gs_node_map[i] = S.findLeaf(gsMap.find(ID[i]));
    }

    map<string, double> vvProbs;
    vector<Node*> all_nodes = G.getAllNodes();

    for (unsigned i = 0; i < all_nodes.size(); i++) {
        Node* node = all_nodes.at(i);
        if (node == NULL || node->isLeaf()) {
            continue;
        } else {
            vector<Node*> lnodes = getDescendentNodes(node->getLeftChild());
            vector<Node*> rnodes = getDescendentNodes(node->getRightChild());
            vector<unsigned> lnodes_id = getIdsFromNodes(lnodes, gsMap);
            vector<unsigned> rnodes_id = getIdsFromNodes(rnodes, gsMap);

            SetOfNodes species_leaves;
            //Left part
            for (unsigned i = 0; i < lnodes_id.size(); i++) {
                species_leaves.insert(gs_node_map[lnodes_id[i]]);
            }
            //Right part
            for (unsigned i = 0; i < rnodes_id.size(); i++) {
                species_leaves.insert(gs_node_map[rnodes_id[i]]);
            }

            Node *lca_s = find_lca(species_leaves, S);

            //Create "discretized" speciation node
            EdgeDiscretizer::Point point(lca_s, 0);
            Node *lca_g = node;

            Probability speciation_prob = gsr->getPlacementProbability(lca_g, &point);
            //Probability speciation_prob_alt = gsr->getPlacementProbabilityAlternate(lca_g, &point);

            //cout << "speciation prob is " << speciation_prob;
            //cout << " and alternate is " << speciation_prob_alt << endl;

            if (speciation_prob.val() > 1.0) {
                cout << "Virtual vertex number is " << lca_g->getLeaves() << endl;
                cout << "Compute_Ortho: WARNING, Speciation prob > 1.0." << endl
                        << "speciation_prob: " << speciation_prob.val() << endl;
            }
            //if (!isObligateDuplication(lca_g, gsr->getSigma())) {
            ostringstream os;
            os << "[" << lnodes[0]->getName();
            for (unsigned i = 1; i < lnodes.size(); i++)
                os << " " << lnodes[i]->getName();
            os << ",";
            for (unsigned i = 0; i < rnodes.size(); i++)
                os << " " << rnodes[i]->getName();
            os << "]";
            os << "\t" << speciation_prob.val() << endl;
            cout << os.str();
            outfile << os.str(); // << "\t" << speciation_prob.val() << endl;
            //cout << os.str() << endl;
            //}
        }
    }
    outfile.close();
}

bool isObligateDuplication(Node *lca, LambdaMap sigma) {
    // First, make sure that both the children are leaves
    //if(!(lca->getLeftChild())->isLeaf() || !(lca->getRightChild())->isLeaf())        
    // return false;
    vector<Node*> desc = getDescendentNodes(lca);
    string name = sigma[desc[0]]->getName();
    //cout << "For node: " << lca->getNumber() << desc[0]->getName() ;
    for (unsigned int i = 1; i < desc.size(); i++) {
        //cout << "\t" << desc[i]->getName();
        if (sigma[desc[i]]->getName() != name)
            return false;
    }
    //cout << endl;
    return true;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

/**
 * create_lookup_tables
 * Create an id for each gene in the range 0,..., gs_map.size()-1.
 * Then created lookup tables for gene name to id mapping and vice versa.
 *
 * @param ID Storage of id to gene map upon return.
 * @param inv_ID Storage of gene to id map upon return.
 * @param gs_map Gene to species map
 */
void
create_lookup_tables(map<int, string> &ID,
        map<string, int> &inv_ID,
        StrStrMap &gs_map) {
    int nr_genes; //Total number of genes
    string gene_name; //Gene name

    /*
     * Go through all genes in the gene-species map. Create an
     * id and a record in both lookup-tables for each gene found
     */
    nr_genes = gs_map.size();
    for (int i = 0; i < nr_genes; i++) {
        gene_name = gs_map.getNthItem(i);
        ID.insert(pair<int, string > (i, gene_name));
        inv_ID.insert(pair<string, int>(gene_name, i));
    }
}

void printVector(vector<Node*> v) {
    for (unsigned i = 0; i < v.size(); i++)
        cout << v[i]->getNumber() << endl;
    cout << endl;
}

vector<unsigned> getIdsFromNodes(vector<Node*> nodes, StrStrMap gsmap) {
    vector<unsigned> ids;
    for (unsigned i = 0; i < nodes.size(); i++)
        ids.push_back(gsmap.getIdFromGeneName(nodes[i]->getName()));
    return ids;
}