#include <iostream>

#include "AnError.hh"
#include "BranchSwapping.hh"
#include "PRNG.hh"
#include "TreeIO.hh"

void
usage(char *s)
{
  std::cerr << "Usage: "
       << s
       << " <treefile>"
       << std::endl;
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;  

  if (argc != 2)
    {
      usage(argv[0]);
      exit (1);
    }

  try 
    {
      string filename(argv[1]);
      TreeIO io = TreeIO::fromFile(filename);
      Tree G = io.readGuestTree(NULL, NULL);
      
      BranchSwapping secateur;
      
      for (int i = 0; i < 100; i++)
	{
	  secateur.NNI(G);
	  secateur.reRoot(G);
	}
      
      cout << io.writeGuestTree(G) << endl;
    }
  catch (AnError e) 
    {
      e.action();
    }
  return EXIT_SUCCESS;
}
