#! /bin/bash


# 2012-04-07 Erik Sjolund moved the directory 
# src/cxx/tools/reconcile/testdata to example_data/data_not_in_source_package/reconcile_datafiles/testdata

pushd testdata
for ((i=0; i<10; i++)); do
    echo $i
#    primetv -f ps R$i H G$i"_H".gsmap | gv -geometry 800x600+0+0 - &
    primetv -f ps R$i"b" H G$i"_H".gsmap | gv -geometry 800x600+0+0 - &
    sleep 1
    primetv -c red "#99ccff" "#0066ff" -f ps G$i"c" H G$i"_H".gsmap | gv -geometry 800x600-0-0 - 
done
