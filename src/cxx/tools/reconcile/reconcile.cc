#include <cstdlib>
#include <iostream>
#include <stdlib.h>
#include <cstring>

using namespace std;
//using namespace beep;
#include "AnError.hh"
#include "BranchSwapping.hh"
#include "GammaMap.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "PrimeOptionMap.hh"
#include "Rootalyzer.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"
#include "cmdline.h"
#include "fstream"

using namespace beep;
void populateGsMap(Tree gtree, StrStrMap &map);

// Parameters

string guest_tree_file;
string host_tree_file;
string gs_file;


//---- Main ---------------------------------------
int
main (int argc, char **argv) 
{

  bool gs_available = false;
  struct gengetopt_args_info args_info;
  char *ofilename = "mpr.out";
  ofstream outfile;  

  if (cmdline_parser(argc, argv, &args_info) != 0) 
    exit(1);


  if(args_info.inputs_num < 2  ||  args_info.inputs_num > 4)
    {
      cerr << "error: Wrong number of parameters" << endl;
      exit(1);
     }
      guest_tree_file = args_info.inputs[0];
      host_tree_file = args_info.inputs[1];

    if (  args_info.inputs_num >= 3 ) {
	  gs_file =  args_info.inputs[2];
	  gs_available = true;
    }
    
    if (  args_info.inputs_num == 4 ) {
        outfile.open(args_info.inputs[3]);
    } else
        outfile.open(ofilename);


    // as of gengetopt 2.22.3 you can't specify default values to options with "multiple" keyword ( it seems ) /Erik Sjolund 2009-12-02
Real duplication_weight = 1.0;
Real loss_weight = 1.0;


    if (  args_info.weights_given ) {
      if ( args_info.weights_arg[0] <= 0  || args_info.weights_arg[1] <= 0 ) {
        cerr << "error: Weights are required to be positive" << endl;
        exit(1);
      }
      duplication_weight = ( Real ) args_info.weights_arg[0];
      loss_weight = ( Real )  args_info.weights_arg[0];
    }



  try {
    vector<SetOfNodes> AC(1000); // Unnecessary here
    StrStrMap          gs;

    TreeIO io = TreeIO::fromFile(guest_tree_file);
    Tree G = io.readBeepTree(&AC, &gs);

    io.setSourceFile(host_tree_file);
     Tree S = io.readNewickTree();
     //    Tree S = io.readHostTree();

     if (gs_available)
       {
         //try {
	 gs = TreeIO::readGeneSpeciesInfo(gs_file);
         //cout << gs.size() << endl;
         //} catch(AnError e){
         if(gs.size() == 0) {
           populateGsMap(G, gs);
         }
         //}
       }

     cout << G << endl;
     cout << S << endl;
     cout << gs << endl;
    if(args_info.reroot_arg != reroot_arg_no)
      {        
	Rootalyzer ra(G, S, gs, duplication_weight, loss_weight);
 	BranchSwapping bs;

        if(args_info.reroot_arg == reroot_arg_any)
	  {
	    bs.setRootOn(ra.getCheapestRoot());
	  }
	else
	  {
	    bs.setRootOn(ra.getRandomCheapRoot());
	  }
      }

    LambdaMap lambda(G, S, gs);
    GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda);
    
    std::vector<Node*> all_nodes = G.getAllNodes();
    for (int i = 0; i < all_nodes.size(); i++) {
        if( gamma.isSpeciation(*all_nodes[i]))
            cout << all_nodes[i]->getNumber() << " is a speciation in MPR" << endl;
        else
            cout << all_nodes[i]->getNumber() << " is a DUPLICATION in MPR" << endl;
    }
    
    string mprTree = io.writeGuestTree(G, &gamma);
    cout << mprTree;
    //cout << ';' << endl;
    //cout << "MPR analysis done for " << guest_tree_file << endl;
    
    outfile << mprTree << ";" << endl;
    outfile.close();
  }
  catch (AnError e) {
    e.action();
  }
  return EXIT_SUCCESS;
}

std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> split(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split(s, delim, elems);
}

/**
 * 
 * @param gtree Gene Tree
 * @param map Map object to be populated
 */
void populateGsMap(Tree gtree, StrStrMap &map) {
    std::vector<Node*> nodes = gtree.getAllNodes();

    map.clearMap();
    for (int i = 0; i < gtree.getNumberOfNodes(); i++) {
        if (nodes[i]->isLeaf() && nodes[i] != NULL) {
            std::vector<std::string> elems = split(nodes[i]->getName(), '_');
            //cout << nodes[i]->getName() << " , " << elems[1] << endl;
            map.insert(nodes[i]->getName(), elems[1]);
        }
    }
}


