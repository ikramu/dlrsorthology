#include <cstdlib>
#include <fstream>

#include "AnError.hh"
#include "GammaMap.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"



// Global options
//-------------------------------------------------------------
char* outfile=0;
bool quiet = false;
int nParams = 3;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);

// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  if(argc < nParams + 1) 
    {
      usage(argv[0]);
      std::exit(1);
    }
  try
    {
      // Check for options
      //-----------------------------------
      int opt = readOptions(argc, argv);

      //Get trees and rconciliation
      //---------------------------------------------
      string S_filename(argv[opt++]);
      TreeIO io = TreeIO::fromFile(S_filename);
      Tree S = io.readHostTree();
      S.setName("S");

      string G_filename(argv[opt++]);
      io.setSourceFile(G_filename);
      Tree G = io.readBeepTree(0,0);
      G.setName("G");

      string gamma_filename(argv[opt++]);
      ifstream is(gamma_filename.c_str());

      vector<SetOfNodes> gamma_in(S.getNumberOfNodes());
      while(is.good()) 
	{
 	  string line;
	  //Remove commas from the line
	  getline(is, line);
	  for(unsigned i = 0; i < line.length(); i++) 
	    {
	      if (line[i] == ',') 
		{
		  line[i] = ' ';
		}
	    }
	  istringstream tmp(line);

	  unsigned gene;
	  unsigned species;
	  if(tmp >> species)
	    {
	      while(tmp >> gene)
		{
		  Node* g = G.getNode(gene);
		  gamma_in[species].insert(g);
		}
	    }
	}

     
      
      // Derive gs from gamma_in
      // First we set up a structure for checking that all leaves are mapped
      map<Node*, bool> gLeafIsMapped;
      for(unsigned k = 0; k < G.getNumberOfNodes(); k++)
	{
	  Node* g = G.getNode(k);
	  if(g->isLeaf())
	    gLeafIsMapped[g] = false;
	}

      // Now lets construct gs
      StrStrMap gs;
      for(unsigned i = 0; i < S.getNumberOfNodes(); i++)
	{
	  Node& s = *S.getNode(i);
	  for(unsigned j = 0; j < gamma_in[i].size(); j++)
	    {
	      Node* g = gamma_in[i][j];
	      if(s.isLeaf() &&g->isLeaf())
		{
		  gs.insert(g->getName(), s.getName());
		  gLeafIsMapped[g] = true;
		}
	    }
	}
      for(unsigned k = 0; k < G.getNumberOfNodes(); k++)
	{
	  Node* g = G.getNode(k);
	  if(gLeafIsMapped[g] == false)
	    cerr << "Guest tree leaf " << k << " is not mapped to any species leaf\n";;
	}

      if(!quiet)
	{
	  cout << "Host "
	       << S.print(false,false,false,false)
	       << "\nGuest "
	       << G.print(false,false,false,false)
	       << "\ngene-species leaf map:\n"
	       << "Guest\tHost\n"
	       << gs
	       << "\nReconciliation in tabular format:\n"
	       << "Host\tGuest\n";
	  for(unsigned i = 0; i < S.getNumberOfNodes(); i++)
	    {
	      cout << i << "\t";
	      for(unsigned j = 0; j < gamma_in[i].size(); j++)
		{
		  if(j != 0)
		    cout << ", ";
		  cout << gamma_in[i][j]->getNumber();
		}
	      cout << "\n";
	    }
	  cout << "\n";
	}

	      

      LambdaMap lambda(G, S, gs);
      GammaMap gamma(G, S, lambda, gamma_in);
      if(!quiet)
	{
	  cout << "The reduced reconciliation in tabular format\n"
	       << "Host\tGuest\n"
	       << gamma.print()
	       << "\n";
	}      
      if(outfile)
	{
	  ofstream of(outfile);
	  of << TreeIO::writeGuestTree(G, &gamma) << endl;
	}
      else
	{
	  if(!quiet)
	    cout << "\nReconciliation in PRIME format:\n";
	  cout << TreeIO::writeGuestTree(G, &gamma) << endl;
	}

      return(0);

    }
  catch (AnError e) 
    {
      e.action();
    }
  catch (exception e) 
    {
      cout << "Error: "
	   << e.what();
    }
}


      
void 
usage(char* cmd)
{
  using std::cout;
  cout 
    << "This program reads a host tree, S , a guest tree, G, and a\n"
    << "reconciliation, gamma, between these in tabular format and\n"
    << "outputs the recnciled tree (G, gamma) in PrIME format.\n"
    << "The reconciliation maps nodes of S to sets of nodes in G\n"
    << "by their PrIME ID-numbers. If ID-numbers are not given in the\n"
    << "tree infiles, these are assigned automatically by the program.\n"
    << "This assignment is done in a consistent way by all PrIME programs.\n"
    << "Nevertheless, users will probably benefit from giving ID-numbers in infiles\n"
    << "\n"
    << "Usage: "
    << cmd
    << " [<options>] <host tree> <guest tree> <reconc tab>"
    << "\n"
    << "Parameters:\n"
    << "   <host tree>        Host tree in Newick format. Using PrIME\n"
    << "                      ID-tags for nodes is recommended\n"
    << "   <guest tree>       Guest tree in Newick format. Using PrIME\n"
    << "                      ID-tags for nodes is recommended\n"
    << "   <reconc tab>       Reconciliation in tabular format. Host node\n"
    << "                      ID number in the first column and its gamma\n"
    << "                      map as a comma-separated list of guest node\n"
    << "                      ID-numbersin the second.\n"
    << "/n"
    << "Options:\n"
    << "   -u, -h             This text.\n"
    << "   -q                 Quiet mode.\n"
    << "   -o <filename>      output file\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  usage(argv[0]);
	  break;

	case 'q':
	  quiet = true;
	  break;

	case 'o':
	  if (opt + 1 < argc)
	    {
	      outfile = argv[++opt];
	    }
	  else
	    {
	      cout << "Expected filename after option '-o'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	default:
	  {
	    cout << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  

	
