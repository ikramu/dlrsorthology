/*
 * Original.
 * File:   ConsensusTree.cc
 * Author: peter9
 * 
 * Created on December 7, 2009, 12:25 PM
 */

#include "ConsensusTree.hh"
#include "AnError.hh"

#include <plotter.h>


namespace beep{

ConsensusTree::ConsensusTree():
m_root( NULL )
{
}

void
ConsensusTree::copy_node_rec(ConsensusNode *oldnode, ConsensusNode *newparent)
{
    //Copy node and each children
    ConsensusNode *newnode = new ConsensusNode(*oldnode);

    if( oldnode->isRoot() ){
        m_root = newnode;
    }
    else{
        newparent->addChild(newnode);
        newnode->setParent(newparent);
    }

    if( oldnode->isLeaf() ){
        return;
    }
    else{
        for(unsigned int i = 0; i < oldnode->getNumberOfChildren(); i++){
            copy_node_rec( oldnode->getChild(i), newnode );
        }
    }
}

ConsensusTree::ConsensusTree(const ConsensusTree& orig):
m_root( orig.m_root )
{
    //Copy each node
    if(orig.m_root == NULL){
        m_root = NULL;
        return;
    }
    copy_node_rec(orig.m_root, NULL);
}

ConsensusTree::~ConsensusTree()
{
    deleteAllNodes();
}

ConsensusNode*
ConsensusTree::getRoot()
{
    return m_root;
}

void
ConsensusTree::delete_rec(ConsensusNode *node)
{
    if( !(node->isLeaf()) ){
        //Delete children first, then current node
        for(unsigned int i = 0; i < node->getNumberOfChildren(); i++){
            delete_rec( node->getChild(i) );
        }
    }
    delete node;
}

void
ConsensusTree::deleteAllNodes()
{
    //Free all nodes
    if(m_root == NULL){
        return;
    }
    delete_rec(m_root);
}

void
ConsensusTree::setRoot(ConsensusNode* root)
{
    m_root = root;
}

ostream&
operator<<(ostream &o, const ConsensusTree &C)
{
    //Print tree in a nice way
    ConsensusTree::print_rec(C.m_root, o);
    return o;
}

void
ConsensusTree::print_rec(ConsensusNode *node, ostream &o)
{
    //Add parentheses around subtree
    o << "(";
    //Print subtree
    for(unsigned int i = 0; i < node->getNumberOfChildren(); i++){
        ConsensusNode *child = node->getChild(i);
        if(child->isLeaf()){
            o << child->getName();
        }
        else{
            print_rec(child, o);
        }
        if(i < node->getNumberOfChildren() - 1){
            o << ", ";
        }
    }
    o << ")";
    //Print probabilities if they exist
    if( node->hasProbabilities() ){
        o << ":[";
        o << node->getPosteriorProb().val();
        o << ", ";
        o << node->getSpeciationProb().val();    
        o << "]";
    }

}







int
ConsensusTree::numberOfLevels()
{
    if(m_root == NULL){
        return 0;
    }

    return m_root->nodeHeight() + 1;
}








bool
ConsensusTree::drawTreeSVG(ostream &o, bool only_speciation)
{
    PlotterParams params;
    /* for bigger plots, change this to any of b, c till f */
    params.setplparam("PAGESIZE", (char *)"a");
    //Open Plotter
    SVGPlotter plotter(cin, o, cerr, params);    
    if (plotter.openpl() < 0)
    {
      cerr << "drawTreeSP: Couldn't open PSPlotter. Aborting drawing process." << endl;
      return false;
    }

    /* Draw tree */
    //plotter.parampl("PAGESIZE", "letter");
    m_root->drawSubTree(plotter, 0.0, 0.0, 1500.0, 1500.0, 30.0, 10.0, only_speciation);

    //Close Plotter
    if (plotter.closepl() < 0)      
    {
      cerr << "Couldn't close Plotter\n";
      return false;
    }

    return true;
}







//istream&
//operator>>(istream &is, ConsensusTree &C)
//{
//    //Skip whitespace
//    ws(is);
//
//    //Find tokens
//    //Lexical
//    vector<string> tokens;
//    string line = getline(is);
//    if(line == ""){
//        throw AnError("Not correct Consensus Tree. Read ''. ");
//    }
//    int i = 0;
//    while(i < line.length() && )
//
//}
}; //Namespace beep
