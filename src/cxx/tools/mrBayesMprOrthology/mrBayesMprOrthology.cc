/*
 * mrBayesMprOrthology
 *
 * Computes the orhtology probabilities and creates a consensus tree with speciation probabilities 
 * computed using MPR from gene trees created using MrBayes.
 * This is almost the same code we have used for DLRSOrthology with only the speciation
 * probability computed using MPR and customized formatting for MrBayes file
 *
 * Author: Ikram Ullah (ikramu@kth.se) November/December 2009
 *
 *
 * Acknowledgement: Much of the code in this program has been taken from Prime-CT
 * by Peter Andersson.
 * /Ikram
 */




/*Includes*/

//Boost
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

//Standard libraries
#include <stdlib.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <iosfwd>

//Gengetopt
#include "cmdline.h"

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "SeqIO.hh"
#include "GammaDensity.hh"
#include "SequenceData.hh"
#include "MatrixTransitionHandler.hh"
#include "StrStrMap.hh"
#include "EdgeDiscGSR.hh"
#include "Tokenizer.hh"
#include "Hacks.hh"
#include "TimeEstimator.hh"
#include "RandomTreeGenerator.hh"
#include "CT/EdgeDiscGSRObjects.hh"
#include "Rootalyzer.hh"
#include "BranchSwapping.hh"
#include "GammaMap.hh"
#include "mcmc/PosteriorSampler.hh"

//From primeCT
#include "ConsensusTree.hh"
#include "ConsensusNode.hh"
#include "VirtualVertex.hh"
#include "GeneSet.hh"
#include "McmcSample.hh"


/*Namespaces used*/
using namespace beep;
using namespace std;



/*Definitions and constants*/
// maximum size of characters in mcmc sample
#define SAMPLE_LENGTH 3000
//To be able to use foreach() constructs
#define foreach BOOST_FOREACH
//Flag desciding if diagnostics about the flow of the program is written to stdout
static bool DIAGNOSTICS = true;
//Identities for the input arguments
static const int INPUT_SEQUENCES_SAMPLES_FILE = 0;
static const int INPUT_SPECIES_TREE_FILE = 1;
static const int INPUT_GSMAP_FILE = 2;
//Threshold for majority-rule consensus
static const float posterior_threshold = 0.5f;
//Default MCMC parameters
static const float DEFAULT_BIRTH_RATE = 1.0f;
static const float DEFAULT_DEATH_RATE = 1.0f;
static const float DEFAULT_MEAN = 1.0f;
static const float DEFAULT_VARIANCE = 1.0f;
static const float DEFAULT_TIME_STEP = 0.0f;
static const int DEFAULT_MIN_INTERVALS = 3;
//Convenience typedef
// The first Probability is pair is Posterior Probability
// The second Probability is pair is Speciation Probability
typedef map< VirtualVertex, pair<Probability, Probability> > vvmap;
//NOTE:
//For some reason I cannot find a way to make a pointer to an element
//in a vvmap, this is a workaround. The iterator for vvmap is seen as a
//regular pointer. /Peter
typedef vvmap::iterator vvmapel_pt;





/*Pre-declarations*/
//See each function definition for descriptions.

static void read_cmd_opt(struct gengetopt_args_info &args_info,
        int argc, char *argv[]);

static Tree read_species_tree(struct gengetopt_args_info &args_info);

static void rescale_specie_tree(Tree *S);

static void create_disc_tree(Tree &species_tree,
        EdgeDiscTree **DS, struct gengetopt_args_info &args_info);

static void create_lookup_tables(map<int, string> &D,
        map<string, int> &inv_D,
        StrStrMap &gs_map);

static void read_and_process_samples(EdgeDiscTree &DS,
        StrStrMap &gs_map,
        struct gengetopt_args_info &args_info,
        map<int, string> ID, map<string, int> inv_ID,
        vvmap &vertices,
        vector<vvmapel_pt> &hp_vertices, vector<string> &gene_trees);

static void find_vertices_single(vvmap &vertices,
        Tree &tree,
        map<int, string> &ID,
        map<string, int> &inv_ID);

static void find_vertices_rec(vvmap &vertices,
        Node *node,
        //EdgeDiscGSR *state,
        map<int, string> &D,
        map<string, int> &inv_D,
        GeneSet &part);

static void find_high_post_vertices(vvmap &vertices,
        vector<vvmapel_pt> &hp_vertices,
        float threshold_count);

static Node* find_lca(SetOfNodes &nodes, Tree &T);

void calc_speciation_probs_from_file(vector<vvmapel_pt> &hp_vertices,
        Tree S, map<int, string> &D, StrStrMap &gs_map, int &numStates,
        vector<string> gene_trees, struct gengetopt_args_info &args_info);

void calc_speciation_single(GammaMap gammaMap,
        vector<vvmapel_pt> &hp_vertices, ofstream &spfile,
        map<int, string> &ID, Tree S, map<int, Node*> gs_node_map);

bool independent(Node * lca_a, Node *lca_b);

static void find_and_replace(string &source, const string find, string replace);

static void normalize_hp_prob(vector<vvmapel_pt> &hp_vertices, int num_states);

static void build_consensus_tree(ConsensusTree &ctree,
        vector<vvmapel_pt> &hp_vertices,
        map<int, string> &D);

static void build_consensus_tree_rec(GeneSet &D,
        ConsensusTree &ctree,
        vector<vvmapel_pt> &hp_vertices,
        ConsensusNode *c,
        map<int, string> &ID);

static void find_mvvs(vector<vvmapel_pt> &mvvs, vector<vvmapel_pt> &V);

/**
 * main
 */
int main(int argc, char *argv[]) {

    /*
     * Program variables
     */
    struct gengetopt_args_info args_info; //Used for option parsing
    vector<EdgeDiscGSRObjects*> gsr_objects; //Samples read from file
    vector<EdgeDiscGSR*> states; //GSR states
    TreeIO tree_reader; //Used for reading from files
    Tree species_tree; //The species tree
    EdgeDiscTree *DS; //Discretized species tree
    StrStrMap gs_map; //The gene-species map used
    map<int, string> ID; //Lookup table for id-gene mapping
    map<string, int> inv_ID; //Lookup table for gene-id mapping
    vvmap vertices; //Table of virtual vertices
    vector<vvmapel_pt> hp_vertices; //High posterior vertices
    ConsensusTree ctree; //Consensus tree
    int num_states;
    int header_tokens = 0; // number of tokens in prime output header (fixed/variable tree)
    vector<string> gene_trees;

    /* 
     * Starting comment for test purposes 
     */
    cout << "\t*******************************" << endl;
    cout << "\tWelcome to Orthology Estimator" << endl;
    cout << "\t*******************************" << endl;

    /*
     * Read command-line options
     */
    read_cmd_opt(args_info, argc, argv);

    /*
     * Read species tree
     */
    if (DIAGNOSTICS) {
        cout << "Reading species tree.. ";
    }
    species_tree = read_species_tree(args_info);

    cout << species_tree << endl;
    /*
     * Rescale specie tree to 0-1
     */
    rescale_specie_tree(&species_tree);
    TreeIOTraits iot;
    iot.setET(true);
    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    /*
     * Create discretized tree
     */
    if (DIAGNOSTICS) {
        cout << "Creating discretized tree.. " << endl;
    }
    create_disc_tree(species_tree, &DS, args_info);
    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    /*
     * Read gene-species map
     */
    if (DIAGNOSTICS) {
        cout << "Reading gene-species map.. ";
    }
    cout << "\nspecie file name is " << args_info.inputs[INPUT_GSMAP_FILE] << endl;
    gs_map = tree_reader.readGeneSpeciesInfo(args_info.inputs[INPUT_GSMAP_FILE]);


    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    /*
     * Find vertices in trees and gather data for posterior and
     * speciation probability calculations
     */

    //Give each gene an id in the range 0, 1, ..., gs_map.size()-1
    //and create lookup-tables for gene-id and id-gene.
    if (DIAGNOSTICS) {
        cout << "Creating lookup tables.. ";
    }
    create_lookup_tables(ID, inv_ID, gs_map);
    if (DIAGNOSTICS) {
        cout << "lookup tables created!" << endl;
    }

    if (args_info.mcmc_flag) {
        /* This part is yet to be implemented */
    } else {

        /*
         * Read in samples from file
         */
        if (DIAGNOSTICS) {
            cout << "Reading samples.. " << endl;
        }

        int before = gene_trees.size();
        read_and_process_samples(*DS, gs_map, args_info, ID, inv_ID, vertices, hp_vertices, gene_trees);
        cout << "gene_trees before call before is " << before << " and after is " << gene_trees.size() << endl;

        if (DIAGNOSTICS) {
            cout << "done!" << endl;
        }

        /*
         * calculate speciation probability
         */
        if (DIAGNOSTICS) {
            cout << "\nCalculating speciation probability.. " << endl;
        }

        calc_speciation_probs_from_file(hp_vertices, species_tree, ID, gs_map, num_states, gene_trees, args_info);
    }

    if (DIAGNOSTICS) {
        cout << "done calculating speciation probs!" << endl;
    }

    cout << "Number of states are: " << num_states << endl;
    cout << "Number of hp_vertices are: " << hp_vertices.size() << endl;
    normalize_hp_prob(hp_vertices, num_states);

    foreach(vvmapel_pt it, hp_vertices) {
        cout << it->first << "\t" << it->second.first.val() << "\t";
        cout << it->second.second.val() << endl;
    }

    /*
     * write the speciation probabilities for each virtual vertex to file
     */
    ofstream spfile(args_info.virtual_vertex_file_arg);
    spfile << "VirtualVertex\tPosterior Probability\tSpeciation Probability" << endl;

    foreach(vvmapel_pt it, hp_vertices) {
        spfile << it->first << "\t" << it->second.first.val() << "\t" << it->second.second.val() << endl;
    }
    spfile.close();

    /*
     * Create consensus tree
     */
    if (DIAGNOSTICS) {
        cout << "Building consensus tree..  ";
    }
    for (int i = 0; i < hp_vertices.size(); i++) {
        vvmapel_pt it = hp_vertices[i];
        cout << it->first << "\t" << it->second.first.val() << "\t" << it->second.second.val() << endl;
    }
    std::map<int, std::string>::iterator iter;
    for (iter = ID.begin(); iter != ID.end(); iter++) {
        cout << iter->first << "\t" << iter->second << endl;
    }
    build_consensus_tree(ctree, hp_vertices, ID);
    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    /*
     * Print consensus tree
     */
    if (DIAGNOSTICS) {
        cout << "Printing consensus tree..  " << endl;
    }
    if (args_info.output_file_given) {
        ofstream file(args_info.output_file_arg);
        file << ctree;
        file.close();
    } else {
        cout << ctree << endl;
    }
    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    cout << "checking fixed node case..." << endl;
    //Draw tree to file
    cout << "Drawing tree and saving to file " << args_info.ctree_svg_file_arg;

    ofstream file(args_info.ctree_svg_file_arg);

    // if you want to output just speciation probability in the output consensus
    // tree, then put the flag to true, else to false (both posterior and 
    // speciation probability)
    ctree.drawTreeSVG(file, false);
    file.close();
    cout << "  done!" << endl;

    cout << "End of program..." << endl;

}

/**
 * read_cmd_opt
 *
 * Deal with typical command-line option parsing using gengetopt.
 *
 * @param args_info On return, contains all option and input info.
 * @param argc NO input parameters (same as for main)
 * @param argv Input arguments (same as for main)
 */
void
read_cmd_opt(struct gengetopt_args_info &args_info,
        int argc,
        char *argv[]) {
    /*Parse options and input*/
    if (cmdline_parser(argc, argv, &args_info) != 0) {
        exit(EXIT_FAILURE);
    }

    /*Check for correct number of inputs*/
    if (args_info.inputs_num != 3) {
        cerr << gengetopt_args_info_usage << endl
                << "Type '" << argv[0] << " -h' for help" << endl;
        exit(EXIT_FAILURE);
    }

    /*Set diagnostics printout*/
    DIAGNOSTICS = !(args_info.silent_flag);
}

/**
 * read_species_tree
 *
 * Read a species tree from file.
 *
 * @param args_info Program arguments
 */
Tree
read_species_tree(struct gengetopt_args_info &args_info) {
    //cout << "\nspecie file name is " << args_info.inputs[INPUT_SPECIES_TREE_FILE] << endl;
    TreeIO tree_reader = TreeIO::fromFile(args_info.inputs[INPUT_SPECIES_TREE_FILE]);
    return tree_reader.readHostTree();
}

/**
 * rescale_specie_tree
 *
 * Rescales a species tree to [0,1].
 *
 * @param S The specie tree
 */
void
rescale_specie_tree(Tree *S) {
    Real sc = S->rootToLeafTime();
    RealVector* tms = new RealVector(S->getTimes());
    for (RealVector::iterator it = tms->begin(); it != tms->end(); ++it) {
        (*it) /= sc;
    }
    S->setTopTime(S->getTopTime() / sc);
    S->setTimes(*tms, true);
    cout << "Specie tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
}

/**
 * create_disc_tree
 *
 * Creates a discretized species tree from a species tree.
 *
 * @param species_tree The species tree
 * @param DS At return: Pointer to the discretized species tree
 * @param args_info Program arguments
 *
 */
void
create_disc_tree(Tree &species_tree,
        EdgeDiscTree **DS,
        struct gengetopt_args_info &args_info) {
    float timestep; //Timestep for discretized tree
    int min_intervals; //Minimum number of intervals in DS
    EdgeDiscretizer *disc; //Used for creating discretized trees

    //Set timestep and min_interval parameters
    timestep = args_info.timestep_arg;
    min_intervals = args_info.min_intervals_arg;

    //Create discretized tree
    if (timestep == 0) {
        disc = new EquiSplitEdgeDiscretizer(min_intervals);
    } else {
        disc = new StepSizeEdgeDiscretizer(timestep, min_intervals);
    }
    *DS = new EdgeDiscTree(species_tree, disc);

    delete disc;
}

/**
 * create_lookup_tables
 *
 * Create an id for each gene in the range 0,..., gs_map.size()-1.
 * Then created lookup tables for gene name to id mapping and vice versa.
 *
 * @param ID Storage of id to gene map upon return.
 * @param inv_ID Storage of gene to id map upon return.
 * @param gs_map Gene to species map
 */
void
create_lookup_tables(map<int, string> &ID,
        map<string, int> &inv_ID,
        StrStrMap &gs_map) {
    int nr_genes; //Total number of genes
    string gene_name; //Gene name

    /*
     * Go through all genes in the gene-species map. Create an
     * id and a record in both lookup-tables for each gene found
     */
    //cout << gs_map << endl;
    vector<string> gene_names;
    nr_genes = gs_map.size();

    for (int i = 0; i < nr_genes; i++) {
        gene_names.push_back(gs_map.getNthItem(i));
    }
    std::sort(gene_names.begin(), gene_names.end());

    for (int i = 0; i < gene_names.size(); i++) {
        //for (int i = 0; i < nr_genes; i++) {
        //gene_name = gs_map.getNthItem(i);
        gene_name = gene_names.at(i);
        cout << i << " = " << gene_name << endl;
        ID.insert(pair<int, string > (i, gene_name));
        inv_ID.insert(pair<string, int>(gene_name, i));
    }
}

/**
 * read_and_process_samples
 *
 * Read all samples from a prime output file.
 *
 * @param samples Storage for sample objects
 * @param DS Discretized species tree needed to create sample objects
 * @param args_info Input parameter including the filenames for gene/species
 *                  tree files.
 */
void
read_and_process_samples(EdgeDiscTree &DS,
        StrStrMap &gs_map,
        struct gengetopt_args_info &args_info,
        map<int, string> ID, map<string, int> inv_ID,
        vvmap &vertices,
        vector<vvmapel_pt> &hp_vertices, vector<string> &gene_trees) {
    ifstream sample_file; //File object for samples
    string curr_line; //Current line when reading samples            

    Tree G; //Gene tree

    int burn_in = 1;
    string header = "";
    bool endOfHeader = false;

    /*Read samples and create states*/
    sample_file.open(args_info.inputs[INPUT_SEQUENCES_SAMPLES_FILE]);
    if (sample_file.is_open()) {
        try {
            while (!sample_file.eof()) {
                //Skip lines of comments
                if ((sample_file.peek() == '#') && !endOfHeader) {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');

                    /*
                    getline(sample_file, header);
                    Tokenizer tok(";");
                    tok.setString(header);
                    hl = 0;
                    while (tok.hasMoreTokens()) {
                        tok.getNextToken();
                        hl++;
                    }
                     */
                    continue;
                } else if ((sample_file.peek() == '#') && endOfHeader) {
                    // don't do anything, this is the summary at 
                    // the end of prime output.
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    continue;
                }
                // only take the samples after burn-in                
                cout << "sample number: " << burn_in << endl;
                if (burn_in <= args_info.burn_in_arg) {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    burn_in++;
                    continue;
                }

                // set the endOfHeader to true, if not done already
                if (!endOfHeader)
                    endOfHeader = true;

                //Get next line from file
                curr_line = "";
                getline(sample_file, curr_line);

                //Skip empty lines
                if (curr_line == "") {
                    cout << "line is empty..." << endl;
                    continue;
                }


                TreeIO tree_reader = TreeIO::fromString(curr_line);
                //cout << curr_line << endl;
                G = tree_reader.readNewickTree();
                //cout << G << endl;
                //cout << G << endl;
                Real duplication_weight = 1.0;
                Real loss_weight = 1.0;
                Tree S = DS.getTree();
                Rootalyzer ra(G, S, gs_map, duplication_weight, loss_weight);
                BranchSwapping bs;
                /* pick a root at random amongst the equally cheap alternatives. */
                bs.setRootOn(ra.getCheapestRoot());

                //LambdaMap lambda(G, DS.getTree(), gs_map);
                //GammaMap gamma = GammaMap::MostParsimonious(G, DS.getTree(), lambda);
                //cout << G << endl;
                cout << "read gene tree from sample no: " << (burn_in - args_info.burn_in_arg) << endl;

                burn_in++;

                find_vertices_single(vertices, G, ID, inv_ID);
                /* add gene tree G to the vector */
                gene_trees.push_back(tree_reader.writeGuestTree(G));
                //cout << tree_reader.writeGuestTree(G, &gamma) << endl;
            }
        } catch (exception &ex) {
            cout << ex.what() << endl;
        }
        sample_file.close();
        int numStates = (burn_in - 1) - args_info.burn_in_arg;
        cout << "Total samples in hp_vertices are " << numStates << endl;
        cout << "Number of hp_vertices before normalization are " << vertices.size() << endl;
        find_high_post_vertices(vertices, hp_vertices, numStates * posterior_threshold);
        cout << "Number of hp_vertices AFTER normalization are " << hp_vertices.size() << endl;
    } else {
        cerr << "Could not open file: " << sample_file << endl;
        exit(EXIT_FAILURE);
    }
}

/*
 * Find all virtual vertices in the state and update counts.
 * The procedure is recursive and starts at the
 * root of the current gene tree.
 * @param vertices virtual vertex map
 * @param state EdgeDiscGSR object containing all the information
 * @param ID id to leaf name map
 * @param inv_ID leaf name to id map
 */

void
find_vertices_single(vvmap &vertices,
        Tree &tree,
        //EdgeDiscGSR* state,
        map<int, string> &ID,
        map<string, int> &inv_ID) {


    GeneSet g(0);
    //find_vertices_rec(vertices, state->getTree().getRootNode(), state, ID, inv_ID, g);
    find_vertices_rec(vertices, tree.getRootNode(), ID, inv_ID, g);
}

/**
 * find_vertices_rec
 *
 * Finds all virtual vertices in one GSR state recursively.
 *
 * @param vertices Storage for the set of virtual vertices upon return, including the count, for each
 *                  vertex, of how many states it was found in.
 * @param node Current node.
 * @param state The GSR state to consider.
 * @param ID id to gene name map.
 * @param inv_D gene name to id map.
 * @param gs_map gene to species map
 * @param part Gene set to add found leaves to.
 */
void
find_vertices_rec(vvmap &vertices,
        Node *node,
        //EdgeDiscGSR *state,
        map<int, string> &ID,
        map<string, int> &inv_ID,
        GeneSet &part) {
    /*Error check*/
    if (node == NULL) {
        throw AnError("Programming error in find_vertices_rec: node == NULL");
    }

    /*
     * If we have reached a leaf, add gene to part of parent vertex.
     */
    if (node->isLeaf()) {
        part.addGene(inv_ID[node->getName()]);
        return;
    }

    /*
     * Non-leaf node.
     * The general idea is to create a new virtual vertex and construct its
     * descendant parts recursively in each subtree of the current node.
     * Then we check if the virtual vertex has been found before. If there is an
     * old record of the virtual vertex, then we update it by:
     * - increasing the count of the virtual vertex.
     *
     * Otherwise we enter the newly found virtual vertex with count 1 to 
     * the set of virtual vertices.
     *
     * Also we add all genes found recursively to the descendant part of the
     * parent of the current node.
     */

    //Create new virtual vertex.
    VirtualVertex vv(ID.size());

    //Create descendant parts and construct them recursively.
    GeneSet left(ID.size()), right(ID.size());
    //find_vertices_rec(vertices, node->getLeftChild(), state, ID, inv_ID, left);
    //find_vertices_rec(vertices, node->getRightChild(), state, ID, inv_ID, right);
    find_vertices_rec(vertices, node->getLeftChild(), ID, inv_ID, left);
    find_vertices_rec(vertices, node->getRightChild(), ID, inv_ID, right);

    //Add descendant parts to the virtual vertex.
    /* due to duplicate entries like (a, b c) (b c, a), I am modifying the code
     * to add left descendent as the one with less
     */

    /* IMPORTANT 
     * I have found that existing setup of VirtualVertex doesn't know that 
     * if, for instance, v1=[1 4, 5] and v2=[5, 1 4] then v1 == v2.
     * For the time being, I am adding the condition that the left descendent 
     * of VirtualVertex will contain geneset with greater number of leaves than 
     * its right sibling (in other words, it would always be [1 4, 5] in above case
     * Ikram Ullah (Aug 22, 2013)
     */
    if (left.size() >= right.size()) {
        vv.setLeftDescendantPart(left);
        vv.setRightDescendantPart(right);
    } else {
        vv.setLeftDescendantPart(right);
        vv.setRightDescendantPart(left);
    }

    //Add genes found to the descendant part of the parent to the current node.
    if (!(node->isRoot())) {
        part.addGeneSet(left);
        part.addGeneSet(right);
    }

    //Update virtual vertex set
    /*
     * IMPLEMENTATION NOTE:
     * If vv does not exist in vertices, then the statement vertices[vv]
     * adds vv and the count and cumulative speciation probability
     * both defaults to 0. See documentation for std::map for more info.
     */
    vertices[vv].first += 1.0;
}

/**
 * find_high_posterior_vertices
 *
 * Finds all virtual vertices with a posterior probability over some threshold.
 *
 * @param vertices A set of virtual vertices to sift. Assumes count has already been calculated.
 * @param hp_vertices Storage for the found vertices upon return.
 * @param threshold_count Posterior threshold. Note that this is a number reperesenting a count
 *                          of states a vertex must have occured in to qualify as a high posterior
 *                          vertex. It is NOT a probability.
 */
void
find_high_post_vertices(vvmap &vertices,
        vector<vvmap::iterator> &hp_vertices,
        float threshold_count) {
    for (vvmap::iterator it = vertices.begin(); it != vertices.end(); it++) {
        cout << it->first << " \t" << it->second.first.val() << "," << it->second.second.val() << endl;
        if (it->second.first.val() > threshold_count) {
            hp_vertices.push_back(it);
        }
    }
}

/**
 * find_lca
 *
 * Finds and returns the lca node of a set of nodes.
 *
 * @param nodes A vector of nodes
 * @param T Tree in which the nodes are found.
 *
 */
Node*
find_lca(SetOfNodes &nodes, Tree &T) {
    Node *lca = nodes[0];
    for (unsigned int i = 1; i < nodes.size(); i++) {
        lca = T.mostRecentCommonAncestor(lca, nodes[i]);
    }
    return lca;
}

/**
 * calc_speciation_probs_from_file
 *
 * Read all trees from a prime output file, and calculate the speciation probability.
 *
 * @param samples Storage for sample objects
 * @param DS Discretized species tree needed to create sample objects
 * @param args_info Input parameter including the filenames for gene/species
 *                  tree files.
 *        
 */
void calc_speciation_probs_from_file(vector<vvmapel_pt> &hp_vertices,
        Tree S, map<int, string> &D, StrStrMap &gs_map, int &numStates,
        vector<string> gene_trees, struct gengetopt_args_info &args_info) {

    map<int, Node*> gs_node_map;
    bool first_state = true;

    cout << "#################################################################" << endl;
    cout << "Starting to calculate speciation probabilities from states now..." << endl;
    cout << "#################################################################" << endl;

    ofstream spfile(args_info.gsr_spec_output_file_arg);

    spfile << "GeneTree(tree);\t";

    foreach(vvmapel_pt it, hp_vertices) {
        ostringstream os;
        os << it->first.getLeftDescendantPart();
        string ldp = os.str();

        // reset the stream
        os.str("");

        os << it->first.getRightDescendantPart();
        string rdp = os.str();

        find_and_replace(ldp, " ", ".");
        find_and_replace(rdp, " ", ".");
        spfile << "vv_" << ldp << "_" << rdp << "(logfloat);\t";
    }

    for (int i = 0; i < gene_trees.size(); ++i) {
        TreeIO tree_reader = TreeIO::fromString(gene_trees[i]);
        Tree G = tree_reader.readNewickTree();
        //cout << G << endl;        
        LambdaMap lambda(G, S, gs_map);
        GammaMap gammaMap = GammaMap::MostParsimonious(G, S, lambda);

        if (first_state) {
            cout << "constructing gs_node_map now: ";
            //Create gene_id to species-leaf-node map                    
            for (unsigned int i = 0; i < D.size(); i++) {
                gs_node_map[i] = S.findLeaf(gs_map.find(D[i]));
            }
            first_state = false;
            cout << "done..." << endl;
        }

        try {
            calc_speciation_single(gammaMap, hp_vertices, spfile, D, S, gs_node_map);
            cout << "computed orthology probability for sample no: " << (i + 1) << endl;
        } catch (exception & ex) {
            cout << "ERROR probably runtime: " << ex.what() << endl;
        }

    }
    numStates = gene_trees.size();
    cout << "No of states are " << numStates << endl;
    spfile.close();
}

void printVector(Tree &G, map<int, string> &ID, vector<unsigned> v) {
    for (vector<unsigned>::iterator i = v.begin(); i != v.end(); i++)
        cout << "for ID " << *i << ", gene is : " << G.findLeaf(ID[*i])->getName() << endl;
}

void calc_speciation_single(GammaMap gammaMap,
        vector<vvmapel_pt> &hp_vertices, ofstream &spfile,
        map<int, string> &ID, Tree S, map<int, Node*> gs_node_map) {

    Tree G = gammaMap.getG(); //state->getTree();

    map<string, double> vvProbs;

    foreach(vvmapel_pt it, hp_vertices) {
        vector<unsigned int> left_gene_ids = it->first.getLeftDescendantPart().getGeneIDs();

        SetOfNodes left_nodes, right_nodes;
        vector<unsigned int> right_gene_ids = it->first.getRightDescendantPart().getGeneIDs();

        foreach(int i, left_gene_ids) {
            left_nodes.insert(G.findLeaf(ID[i]));
        }
        Node *lca_left = find_lca(left_nodes, G);

        foreach(int i, right_gene_ids) {
            right_nodes.insert(G.findLeaf(ID[i]));
        }
        Node *lca_right = find_lca(right_nodes, G);

        /* Here we check the first condition as per the equation 4.6
         * on page 15 in thesis. If its 1, we proceed to find
         * o(v_alpha, v_beta) discussed in last paragraph of page 14.
         */
        //Check for independence
        if (independent(lca_left, lca_right)) {

            /* find the MPR speciation for LCA of left and right genes of G */
            Node *lca_g = G.mostRecentCommonAncestor(lca_left, lca_right);
            Probability speciation_prob(0.0);
            if (gammaMap.isSpeciation(*lca_g)) {
                speciation_prob = 1.0;
                //cout << it->first << ":" << lca_g->getNumber() << " is a speciation" << endl;
                //cout << lca_g->getNumber() << " is a speciation" << endl;
            }


            //Store probability
            it->second.second += speciation_prob;
        }
    }
}

/**
 * independent
 *
 * Decide if a pair of nodes are independent.
 *
 * @param lca_a Node 1
 * @param lca_b Node 2
 */
bool
independent(Node * lca_a, Node *lca_b) {
    return !(lca_a->dominates(*lca_b) || lca_b->dominates(*lca_a));
}

/*
 * replaces a substring with another given substring
 * 
 * @param source source string
 * @param find string to be find and replaced
 * @param replace string replacing "find"
 */
void find_and_replace(string &source, const string find, string replace) {
    size_t j;
    for (; (j = source.find(find)) != string::npos;) {
        source.replace(j, find.length(), replace);
    }
}

/*
 * normalizes high posterior probabilities
 * 
 * @param hp_vertices high posterior vertices
 * @param num_states number of states
 */
void normalize_hp_prob(vector<vvmapel_pt> &hp_vertices, int num_states) {
    //Normalize the probabilities by dividing with the number of states

    foreach(vvmapel_pt it, hp_vertices) {

        it->second.first /= static_cast<Real> (num_states);

        /* NOTE: Due to fixed tree case, sometime the error for posterior
         * is given, so changing below to 1.1 (temporary fix)
         */
        if (it->second.first.val() > 1.1) {
            cout << "ERROR: Posterior: " << it->second.first.val() << endl;
            cout << "nr_states: " << num_states << endl;
            cout << "Vertex: " << it->first << endl;
        }
        it->second.second /= static_cast<Real> (num_states);
        if (it->second.second.val() > 1.0) {
            cout << "ERROR: Orthology: " << it->second.second.val() << endl;
            cout << "nr_states: " << num_states << endl;
            cout << "Vertex: " << it->first << endl;
        }
    }
}

/**
 * build_consensus_tree
 *
 * Constructs a majority rule consensus tree.
 *
 * @param ctree Storage for the final consensus tree.
 * @param hp_vertices A consistent set of virtual vertices with probabilities.
 * @param ID id to gene name map.
 *
 */
void
build_consensus_tree(ConsensusTree &ctree,
        vector<vvmapel_pt> &hp_vertices,
        map<int, string> &ID) {
    /*Create root node*/
    ConsensusNode* root = new ConsensusNode();
    ctree.setRoot(root);

    /*Create gene set with all genes*/
    GeneSet D = GeneSet(ID.size());
    for (unsigned int i = 0; i < ID.size(); i++) {
        cout << ID.at(i) << endl;
        D.addGene(i);
    }

    /*Create tree recursively*/
    build_consensus_tree_rec(D, ctree, hp_vertices, root, ID);
}

/**
 * printV
 *
 * Prints a vector of virtual vertices.
 *
 * @param V Vector of virtual vertices.
 * @param name String describing the name of the set of vertices.
 * @author peter9
 */
void
printV(vector<vvmapel_pt> &V, string name) {
    cout << name << ": " << endl;

    foreach(vvmapel_pt it, V) {
        cout << it->first << endl;
    }
}

/**
 * build_consensus_tree_rec
 *
 * Constructs a consensus tree recursively. Used by build_consensus_tree.
 *
 * @param D Gene set for this level.
 * @param ctree Storage of final consensus tree.
 * @param V Current set of virtual vertices.
 * @param c Current node.
 * @param ID id to gene name map.
 * @author peter9
 *
 */
void
build_consensus_tree_rec(GeneSet &D,
        ConsensusTree &ctree,
        vector<vvmapel_pt> &V,
        ConsensusNode *c,
        map<int, string> &ID) {
    /*
     * Error check
     */
    assert(D.size() > 0);
    if (D.size() < 1) {
        throw AnError("Programming error in build_consensus_tree_rec: D is empty.");
    }

    /* 
     * Base cases
     */
    if (D.size() == 1) {
        //If c is the only node in the tree, stop and return.
        if (c->isRoot()) {
            return;
        }
        //Otherwise, we have encountered a leaf that must be added to the tree.

        //Change c to be a leaf
        vector<unsigned int> ids = D.getGeneIDs();
        c->setName(ID[ ids[0] ]);
        return;
    }
    if (V.size() == 0) {
        //Create new nodes for all remaining genes in D and set them as
        //children to the current node
        vector<unsigned int> ids = D.getGeneIDs();

        foreach(unsigned int id, ids) {
            //Create node for gene
            ConsensusNode *leaf = new ConsensusNode();
            leaf->setName(ID[id]);
            //Create edge from c
            leaf->setParent(c);
            c->addChild(leaf);
        }
        return;
    }


    /*
     * Recursion step
     */

    //Find MVVs.
    vector<vvmapel_pt> mvvs;
    find_mvvs(mvvs, V);

    //Create combined gene set of the MVVs to check for left-out genes in D
    GeneSet Dprime(ID.size());

    foreach(vvmapel_pt pt, mvvs) {
        Dprime.addGeneSet(pt->first.getLeftDescendantPart());
        Dprime.addGeneSet(pt->first.getRightDescendantPart());
    }
    /*Error check*/
    if (D.size() < Dprime.size()) {
        throw AnError("Programming error in build_consensus_tree_rec. |D|<|Dprime|.");
    }
    //Check for left-out genes
    if (Dprime.size() < D.size()) {
        /*
         * Not every gene is covered by a virtual vertex.
         * We create an edge to every left-out gene from c then make a recursive
         * call with the other genes.
         */

        //Get left-out genes and create leaves for them.
        vector<unsigned int> ids = D.getSetDifference(Dprime).getGeneIDs();

        foreach(unsigned int id, ids) {
            //Create node for gene
            ConsensusNode *leaf = new ConsensusNode();
            leaf->setName(ID[id]);
            //Create edge
            leaf->setParent(c);
            c->addChild(leaf);
        }

        /*
         * Create a new node for all genes in Dprime and make a recursive call
         * with Dprime as the gene set.
         */
        //Create node for gene
        ConsensusNode *node = new ConsensusNode();
        //Create edge from c
        node->setParent(c);
        c->addChild(node);
        //Recursive call
        build_consensus_tree_rec(Dprime, ctree, V, node, ID);
        return;
    }


    /*
     * Now we now that |Dprime| == |D| and we do not have to consider
     * Dprime anymore.
     */

    //Create new virtual vertex sets for recursive calls. One pair for each MVV.

    foreach(vvmapel_pt mvv, mvvs) {
        //Create parent node for MVV, i.e. the parent of the MVVs children sets
        ConsensusNode *pnode;
        if (mvvs.size() == 1) {
            pnode = c;
        } else {
            pnode = new ConsensusNode();
            pnode->setParent(c);
            c->addChild(pnode);
        }

        //An MVV is always a high posterior vertex so add probabilities
        pnode->addProbabilities(mvv->second.first, mvv->second.second);

        //Create new children for each part of the MVV
        ConsensusNode *left = new ConsensusNode();
        ConsensusNode *right = new ConsensusNode();
        left->setParent(pnode);
        right->setParent(pnode);
        if (right->isLeaf() && left->isLeaf()) {
            if (left->getName().compare(right->getName()) < 0) {
                pnode->addChild(right);
                pnode->addChild(left);
            } else {
                pnode->addChild(left);
                pnode->addChild(right);
            }
        } else if (right->getNumberOfChildren() >= left->getNumberOfChildren()) {
            pnode->addChild(right);
            pnode->addChild(left);
        } else {
            pnode->addChild(left);
            pnode->addChild(right);
        }

        //Create new vertex sets for each part
        vector<vvmapel_pt> V_L, V_R;
        GeneSet mvv_left = mvv->first.getLeftDescendantPart();
        GeneSet mvv_right = mvv->first.getRightDescendantPart();
        //Place all virtual vertices in V that are dominated by the MVV
        //in the correct subset

        foreach(vvmapel_pt vv, V) {
            GeneSet uni = vv->first.getLeftDescendantPart().getSetUnion(
                    vv->first.getRightDescendantPart());
            if (uni.subsetOf(mvv_left)) {
                V_L.push_back(vv);
            } else if (uni.subsetOf(mvv_right)) {
                V_R.push_back(vv);
            }
        }
        //Make recursive calls
        build_consensus_tree_rec(mvv_left, ctree, V_L, left, ID);
        build_consensus_tree_rec(mvv_right, ctree, V_R, right, ID);
    }

}

/**
 * find_mvvs
 *
 * Finds all MVVs in a set of virtual vertices.
 *
 * @param mvvs Storage for found MVVs. Assumed to be intitially empty.
 * @param V A set of virtual vertices.
 * @author peter9
 */
void
find_mvvs(vector<vvmapel_pt> &mvvs, vector<vvmapel_pt> &V) {
    /*Error check*/
    if (V.size() <= 0) {
        throw AnError("Error in find_mvvs: Set of virtual vertices empty.");
    }

    /*If there is only one element in V, it must be the only MVV.*/
    if (V.size() == 1) {
        mvvs.push_back(V[0]);
        return;
    }

    /*
     * Here V.size >= 2.
     * We maintain a bitvector stating which virtual vertices we have found a
     * "larger" element for, i.e. which vertices that are dominated by another
     * vertex in V. The analogy is that we "kill off" smaller vertices until we
     * only have a conquering vertex left. If we no longer can find any new dominators
     * but there are more than 1 element left alive, there are several conquerers,
     * which are mutually incomparable.
     */
    vector<bool> dead(V.size(), false);
    vvmapel_pt max;
    vvmapel_pt cur;
    unsigned int max_index, cur_index;
    bool end = false;
    /*
     * Perform several runs of max-finding until we have considered all possibilities.
     * Since some elements are incomparable, we may have more that one maximum.
     */
    while (!end) {
        //Find starting point, i.e. first vertex that is still alive.
        end = true;
        for (max_index = 0; max_index < V.size(); max_index++) {
            if (dead[max_index]) {
                continue;
            }
            end = false;
            max = V[max_index];
            dead[max_index] = true;
            break;
        }
        if (end) {
            break;
        }

        //Find maximum
        if (max_index < V.size()) {
            for (cur_index = 0; cur_index < V.size(); cur_index++) {
                //Skip dead vertices
                if (dead[cur_index]) {
                    continue;
                }
                cur = V[cur_index];
                if (max->first.dominates(cur->first)) {
                    /*
                     * If current maximum dominates the current vertex, then
                     * we "kill" the current vertex.
                     */
                    dead[cur_index] = true;
                } else if (cur->first.dominates(max->first)) {
                    /*
                     * Similarly, if the current vertex dominates the maximum vertex
                     * Then the maximum is no longer needed in any run, so we kill
                     * it.
                     * In this case we must also restart the current run since
                     * the new maximum may dominate a vertex that the previous
                     * maximum was not comparable with, and thus may still
                     * be alive.
                     */
                    max = cur;
                    max_index = cur_index;
                    dead[cur_index] = true;
                    cur_index = 0; //Restart
                }
                /*
                 * Else, the vertices are incomparable. We simply continue in this
                 * case.
                 */
            }
            /*Maximum found, add it to set of MVVs.*/
            mvvs.push_back(max);
        }
    }
}