#include "Rootalyzer.hh"

#include "AnError.hh"
#include "PRNG.hh"

namespace beep
{
  using namespace std;

  Rootalyzer::Rootalyzer(Tree &G, 	// Tree needing a root analysis
			 Tree &S,       // Species tree that G "lives" in
			 StrStrMap gs,	// Map from genes to species
			 Real dup_weight,	// 
			 Real loss_weight	// 
			 )
    : G(G), S(S), lambda(G, S, gs),
      cost(G),
      n_dups_below(G),
      n_losses_below(G),
      dup_weight(dup_weight),
      loss_weight(loss_weight),
      minimal_cost_nodes(),
      minimal_cost(Real_limits::max())
  {
    if (dup_weight<0.0 || loss_weight < 0.0)
      {
	throw AnError("Weights are required to be non-negative.");
      }

    // Check other roots
    Node *root = G.getRootNode();
    countDuplications(root);

    if (root->isLeaf()) 
      {
	PROGRAMMING_ERROR("Forgot to implement case for single-node trees.");
	return;			// We will have to do something more!
      }
    Node *left = root->getLeftChild();
    Node *right = root->getRightChild();

    if (! left->isLeaf()) 
      {
	TryRootBelow(left, lambda[right], n_dups_below[right]); // Try roots in left subtree
      }
    if (! right->isLeaf())
      {
	TryRootBelow(right, lambda[left], n_dups_below[left]); // and roots in the right subtree
      }

    // Record the cost at the current root and its children.
    // This is a bit redundant, bit avoids special cases.
    // Both children of the root are of course also 
    // equivalent to the current root: they are endpoints of the single
    // edge no which the root is placed.
    unsigned root_dups = n_dups_below[root];
    cost[root] = root_dups; // Cost if current root is chosen
    cost[left] = root_dups; 
    cost[right] = root_dups;

    // Check whether current node is good or really good.
    if (root_dups * dup_weight == minimal_cost) 
      {
	minimal_cost_nodes.push_back(root);
      }
    else if (root_dups * dup_weight < minimal_cost)
      {
	minimal_cost = minimal_cost;
	minimal_cost_nodes.clear();
	minimal_cost_nodes.push_back(root);
      }
  }


  // Start investigating in the tree rooted at v. Note that 
  // we do not want to consider the root on (root,v), because that 
  // is the present state!
  // 
  // We are coming from (T1, (T2,T3)v) in the Newick notation, 
  // "v" is a label, and are considering (T2, (T1, T3)) or (T3, (T1, T2)).
  void 
  Rootalyzer::TryRootBelow(Node *v, 
			   Node *T1_lambda,
			   unsigned other_dups)
  {
    Node *left = v->getLeftChild();
    Node *right = v->getRightChild();

    // Try (T2, (T1, T3)), and recursively other roots in T2
    TryRootOnEdge(left, v, right, T1_lambda, other_dups);

    // Try (T3, (T1, T2)), and recursively other roots in T3
    TryRootOnEdge(right, v, left, T1_lambda, other_dups);
  }

  //
  // Evaluate putting a root on the arc from parent to v. 
  // Basic scenario: input tree has structure like (T1, (T2, T3))), 
  // but (T2, T3) may also be deep down in the tree, in which case 
  // T1 represents "everything else" of the tree.
  // We now test (T2, (T1, T3)) and options inside T2.
  //
  void
  Rootalyzer::TryRootOnEdge(Node *T2_root, 
			    Node *parent, // root of (T2, T3)
			    Node *T3_root,
			    Node *T1_lambda,
			    unsigned T1_dups)
  {
    // What are the properties of (T1, T3)?
    Node *T3_lambda = lambda[T3_root];
    Node *T1T3_lambda = S.mostRecentCommonAncestor(T3_lambda, T1_lambda);
    unsigned T1T3_dups = 
      T1_dups 
      + n_dups_below[T3_root] 
      + impliesDuplication(T1T3_lambda, T1_lambda, T3_lambda);

    // The lambda of a potential new root
    Node *T2_lambda = lambda[T2_root];
    Node *lambda = S.mostRecentCommonAncestor(T2_lambda, T1T3_lambda);

    if (!T2_root->isLeaf())
      {
	// Evaluate putting the root below T2
	Node *left = T2_root->getLeftChild();
	Node *right = T2_root->getRightChild();

	TryRootOnEdge(left, T2_root, right, T1T3_lambda, T1T3_dups);
	TryRootOnEdge(right, T2_root, left, T1T3_lambda, T1T3_dups);
      }

    unsigned n_dups = 
      n_dups_below[T2_root] 
      + T1T3_dups 
      + impliesDuplication(lambda, T2_lambda, T1T3_lambda);
				
    cost[T2_root] = n_dups;
    Real cost = dup_weight * n_dups;

    if (cost == minimal_cost)
      {
	minimal_cost_nodes.push_back(T2_root);
      }
    else if (cost < minimal_cost)
      {
	minimal_cost = cost;
	minimal_cost_nodes.clear();
	minimal_cost_nodes.push_back(T2_root);
      }
  }




  // 
  unsigned 
  Rootalyzer::countDuplications(Node *v)
  {
    if (v->isLeaf()) 
      {
	n_dups_below[v] = 0;
	return 0;
      }
    else 
      {
	Node *left = v->getLeftChild();
	Node *right = v->getRightChild();
	
	Node *vl = lambda[v];

	unsigned count = 
	  impliesDuplication(vl, lambda[left], lambda[right])
	  + countDuplications(left) 
	  + countDuplications(right);

	n_dups_below[v] = count;
	return count;
      }	    
  }


  
  
  // Is u a duplication? Look at its lambda and compare with 
  // its children v and w
  unsigned 
  Rootalyzer::impliesDuplication(Node *ulambda, Node *vlambda, Node *wlambda)
  {
    if (ulambda == vlambda || ulambda == wlambda) {
      return 1;
    } else {
      return 0;
    }
  }


  // How many duplications does the edge leading into u imply?
  unsigned
  Rootalyzer::impliedLosses(Node *u)
  {
    Node *x = lambda[u];
    Node *y = lambda[u->getParent()];

    if (x == y) 
      {
	return 0;
      }
    else
      {
	unsigned c = 0;
	while (x != y) 
	  {
	    c++;
	    x = x->getParent();
	  }
	
	//	This is not correct. There are cases when we should return c.
	return c-1;
      }
  }


  // Returns a node v to put the root on. It is chosen arbitrarily
  // amongst the equivalent alternatives. However, if the current
  // root is amongst the minimally cheap, then it is chosen.
  Node* 
  Rootalyzer::getCheapestRoot()
  {            
    return minimal_cost_nodes.back(); // By convention, the root should be here if at all.
  }



  // Pick a root at random amongst the equally cheap alternatives.
  Node *
  Rootalyzer::getRandomCheapRoot()
  {
    PRNG prng;
    int i = minimal_cost_nodes.size();
    return minimal_cost_nodes[prng.genrand_modulo(i)];
  }




  void 
  Rootalyzer::print()
  {
    Node *r = G.getRootNode();
    print(r);
  }
  void 
  Rootalyzer::print(Node *v)
  {
    cout << v->getName() << "\t" << v->getNumber() << "\t" << cost[v] << endl;
    if (! v->isLeaf()) 
      {
	print(v->getLeftChild());
	print(v->getRightChild());
      }
    
  }


}


