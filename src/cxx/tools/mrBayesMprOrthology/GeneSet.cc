/*
 * File:   GeneSet.cc
 * Author: peter9
 * 
 * Created on December 2, 2009, 11:36 AM
 */

#include "GeneSet.hh"
#include <assert.h>

namespace beep{

bool
operator<(const GeneSet &o1, const GeneSet &o2)
{
    /*
     * Go through bit vectors o1.m_genes and o2.m_genes
     * from MSB to LSB.
     * If we come across a case where the bit in one vector is true
     * and the corresponding bit in the other vector is false,
     * then we can decide which set is larger.
     * Think "Binary number comparison".
     */
    if(o1.m_max_id < o2.m_max_id){
        return true;
    }
    if(o1.m_max_id > o2.m_max_id){
        return false;
    }

    //Now o1.m_max_id == o2.m_max_id
    for(int i = o1.m_max_id-1; i >= 0; i--){

        if(o1.m_genes[i] && !o2.m_genes[i]){
            return false;
        }

        if(!o1.m_genes[i] && o2.m_genes[i]){
            return true;
        }
    }
    return false;
}



bool
operator==(const GeneSet &o1, const GeneSet &o2)
{
    //Check cardinality
    if(o1.m_size != o2.m_size){
        return false;
    }
    else if(o1.m_max_id != o2.m_max_id){
        return false;
    }
    /*
     * Go through bit vectors o1.m_genes and o2.m_genes.
     * If we come across a case where the bit in one vector is different
     * from the corresponding bit in the other vector then return.
     * If all bits are equal, return true.
     */
    for(unsigned int i = 0; i <= o1.m_max_id ; i++){
        bool a = o1.m_genes[i];
        bool b = o2.m_genes[i];
        //If a XOR b is true, then the vectors are not equal in this position
        if( (a||b) && !(a && b) ){
            return false;
        }
    }
    return true;
}








bool 
GeneSet::subsetOf(const GeneSet &rhs) const
{
    //Fast easy checks
    if(rhs.m_size < m_size) return false;
    if(rhs.m_max_id < m_max_id) return false;

    //Check each position. For each 'true' value in bit vector for this
    //object, check if the corresponding bit in rhs is 'true'.
    for(unsigned int i = 0; i <= m_max_id ; i++){
        if(m_genes[i]){
            if( !rhs.m_genes[i] ){
                return false;
            }
        }
    }
    return true;
}















//Correspond to *this \ gs.
GeneSet
GeneSet::getSetDifference(const GeneSet &gs) const
{
    GeneSet diff( m_genes.size() );

    for(unsigned int i = 0; i < m_genes.size(); i++){
        if( m_genes[i] ){
            if ( !gs.m_genes[i] ){
                diff.addGene(i);
            }
        }
    }

    return diff;
}









GeneSet 
GeneSet::getSetUnion(const GeneSet &gs) const
{
    //Make copy of this element and add gs
    GeneSet ret = *this;
    ret.addGeneSet(gs);
    return ret;
}










vector<unsigned int>
GeneSet::getGeneIDs() const
{
    //Create set of ids
    vector<unsigned int> ids(m_size);

    unsigned int id = 0;
    int i = 0;
    foreach(bool b, m_genes){
        if(b){
            ids[i++] = id;
        }
        id++;
    }
    return ids;
}

















void
GeneSet::addGeneSet(const GeneSet &gs)
{
    /*Update bitvector and size of gene set*/

    //Check for self assignment
    if(this != &gs){
        //Compare all possible positions in the bit vectors
        for(unsigned int i = 0; i < m_genes.size(); i++){
            if( !m_genes[i] ){
                if( gs.m_genes[i] ){
                    //Found new gene
                    m_size++;
                    m_genes[i] = true;
                }
            }
        }
    }
    //Update maximum element
    if(gs.m_max_id > m_max_id){
        m_max_id = gs.m_max_id;
    }
}















void
GeneSet::addGene(unsigned int gene_id)
{
    assert(gene_id < m_genes.size()); //Should not be an assert..
    //Check for already existing gene
    if( !m_genes[gene_id] ){
        m_genes[gene_id] = true;
        m_size++;
    }
    //Check for new maximum
    if(gene_id > m_max_id){
        m_max_id = gene_id;
    }
}














ostream&
operator<<(ostream &o, const GeneSet &gs)
{
    unsigned int i;
    for(i = 0; i < gs.m_max_id; i++){
        if(gs.m_genes[i]){
            o << i << " ";
        }
    }
    o << i;
    return o;
}


}; //Namespace beep