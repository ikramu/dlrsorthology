#include "PhyloDag_tmp.hh"

#include "AnError.hh"
#include <boost/graph/adjacency_list.hpp>
#include <iostream>


void usage(char* cmd);

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 1)
    {
      usage(argv[0]);
    }

  try
    {
      PhyloDAG_AL T;
      T.setName("Kalle");
      T.addVertex(7);
      T.addVertex(6);
      T.addVertex(5);
      T.addVertex(4);
      T.addVertex(3);
      cerr << "PhyloDAG " << T.getName() << " has " << T.getNVertices() << " vertices and " << T.getNEdges() << " edges. " << endl;

      cerr << T << endl;

      cerr << endl << T.getVertex(7) << endl;
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
  catch(exception e)
    {
      cout <<" error\n"
	   << e.what();
    }
}


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
    ;
    exit(0);
}

