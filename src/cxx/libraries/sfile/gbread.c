
#include <stdio.h>

int yydebug;

#include "sfile.h"
#include "gb.tab.h"
 
int
main(int argc, char **argv) {
  seq *sequence;
  int n, opt;
  sfile *sf = NULL;

  if (argc > 1) {
    opt = 1;
    if (!strcmp(argv[opt], "-d")) {
      yydebug = 1;
      opt++;
    } else {
      yydebug = 0;
    }

    sf = seq_open(argv[opt], "r");
    if (argc > 2) {
      fprintf(stderr, "gbread: Warning, only reading first file. (%s)\n", argv[opt]);
    }
  } else {
    sf = seq_file2sfile(stdin);
  }

  sequence = seq_read_all(sf, &n); 
/*   sequence = seq_read(sf, 1, &n); */
  seq_close(sf);
  fprintf(stderr, "All %d sequences read.\n", n);
  while (sequence) {
      seq_add_comment(sequence, "Passed through testing program gbread.");
/*       seq_set_definition(sequence, "Test sequence"); */
      seq_print(sequence);
      sequence = sequence->next;
  }
  return 0;
}

