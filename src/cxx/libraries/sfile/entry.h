
/*   entry.h
     Data structure for GenBank files
*/


#ifndef entry_H
#define entry_H

#define LISTSTR_NAME_LEN 255

#define AUTHOR_LNAME_LEN 50
#define AUTHOR_FNAME_LEN 2
#define AUTHOR_MNAME_LEN 2
#define AUTHOR_SUFFIX_LEN 4

#define QUALVAL_NAME_LEN 20

#define REF_TITLE_LEN 255
#define REF_VOLUME_LEN 5
#define REF_CIT_ADDRESS_LEN 255
#define REF_PUBSTAT_LEN 30
#define REF_PBNAME_LEN 255
#define REF_PBTYPE_LEN 25
#define REF_ADDEPT_LEN 40
#define REF_ADNAME_LEN 60
#define REF_ADCITY_LEN 31
#define REF_ANNOTQUAN_LEN 20
#define REF_ANNOTQUAL_LEN 20
#define REF_SBMEDIUM_LEN 30
#define REF_SBDATE_LEN 30
#define REF_SBPLACE_LEN 30
#define REF_PATNUM_LEN 30
#define REF_PATDATE_LEN 30
#define REF_PATCO_LEN 255

#define FEAT_KEY_LEN 50
#define FEAT_LABEL_LEN 255
#define FEAT_MISC_LEN 10
#define FEAT_LOC_LEN 255

#define ENTRY_MOLTYPE_LEN 20
#define ENTRY_TOPOLOGY_LEN 11
#define ENTRY_ENDIV_LEN 3
#define ENTRY_DISTDATE_LEN 14
#define ENTRY_DEF_LEN 250
#define ENTRY_ACC_LEN 10
#define ENTRY_SOURCE_LEN 250
#define ENTRY_GENUSSPEC_LEN 250
#define ENTRY_ORIGIN_LEN 250


struct liststr {
   char                name[LISTSTR_NAME_LEN+1];
   struct liststr     *next;
};

struct author {
   char                lname[AUTHOR_LNAME_LEN+1];
   char                fname[AUTHOR_FNAME_LEN+1];
   char                mname[AUTHOR_MNAME_LEN+1];
   char                suffix[AUTHOR_SUFFIX_LEN+1];
   struct author      *next;
};

struct qualval {
   char                name[QUALVAL_NAME_LEN+1];
   char               *value;
   struct qualval     *next;
};

struct seqel {
   int                 start;
   int                 end;
   struct seqel       *next;
};

struct reference {
   char                title[REF_TITLE_LEN+1];
   char                volume[REF_VOLUME_LEN+1];
   int                 year;
   int                 pgst;
   int                 pgend;
   char                cit_address[REF_CIT_ADDRESS_LEN+1];
   char                pubstat[REF_PUBSTAT_LEN+1];   /*  Unpublished, In Press */
   char                pbname[REF_PBNAME_LEN+1];   /*  Name of publication   */
   char                pbtype[REF_PBTYPE_LEN+1];    /*  Thesis, Book, Journal */
   char                addept[REF_ADDEPT_LEN+1];
   char                adname[REF_ADNAME_LEN+1];
   char                adcity[REF_ADCITY_LEN+1];
   char                annotquan[REF_ANNOTQUAN_LEN+1];
   char                annotqual[REF_ANNOTQUAL_LEN+1];
   char                sbmedium[REF_SBMEDIUM_LEN+1];
   char                sbdate[REF_SBDATE_LEN+1];
   char                sbplace[REF_SBPLACE_LEN+1];
   char                patnum[REF_PATNUM_LEN+1];
   char                patdate[REF_PATDATE_LEN+1];
   char                patco[REF_PATCO_LEN+1];
   struct seqel       *seqels;
   struct author      *editors;
   struct author      *authors;
   struct reference   *next;
};

struct featocc {
   char                featkey[FEAT_KEY_LEN+1];
   char                label[FEAT_LABEL_LEN+1];
   char                database[FEAT_MISC_LEN+1];
   char                accnum[FEAT_MISC_LEN+1];
   char                ltstart[FEAT_MISC_LEN+1];
   char                ltend[FEAT_MISC_LEN+1];
   char                rtstart[FEAT_MISC_LEN+1];
   char                rtend[FEAT_MISC_LEN+1];
   int                 iscomp;
   char                location[FEAT_LOC_LEN+1];
   struct featocc     *spans;
   struct qualval     *qualvals;
   struct featocc     *next;
};

struct comment {
   char               *text;
   struct comment     *next;
};


struct entry {
   char                *locus;
   int                 seqlen;
   char                moltype[ENTRY_MOLTYPE_LEN+1];
   char                topology[ENTRY_TOPOLOGY_LEN+1];
   char                endiv[ENTRY_ENDIV_LEN+1];
   char                distdate[ENTRY_DISTDATE_LEN+1];
   char                definition[ENTRY_DEF_LEN+1];
   char                accession[ENTRY_ACC_LEN+1];
   int                 segment;
   int                 segs;
   char                source[ENTRY_SOURCE_LEN+1];
   char                genusspec[ENTRY_GENUSSPEC_LEN+1];
   char                origin[ENTRY_ORIGIN_LEN+1];
   char               *sequence;
   struct liststr     *secaccs;
   struct liststr     *keywords;
   struct liststr     *taxes;
   struct reference   *references;
   struct comment     *comments;
   struct featocc     *featoccs;
   struct entry       *next;
};

struct entry       *new_entry();
struct featocc     *new_featocc();
struct reference   *new_reference();
struct author      *new_author();
struct seqel       *new_seqel();
struct comment     *new_comment();
struct qualval     *new_qualval();
struct liststr     *new_liststr();
struct liststr     *new_keyword();
struct liststr     *new_taxlev();
struct liststr     *new_secacc();

struct featocc     *add_span();
struct featocc     *add_featspan();


#endif   /* entry_H */

