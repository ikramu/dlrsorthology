%{
  /* C declarations */
#include <assert.h>
#include <stdio.h>
#include <string.h>

#include "gb.tab.h"
#include "sfile.h"

#ifdef DEBUG
#define MSG(S) {fprintf(stderr, "flex: %s\n", S);}
#else
#define MSG(S)
#endif

// Lintian reports shlib-calls-exit
// Replacing the exit call with an abort call is discussed here
// http://stackoverflow.com/questions/13364139/lintian-reports-shlib-calls-exit-from-flex-generated-source-code-when-building-a
// If you have better ideas of how to fix this, please share them with the PrIME developers.

#define exit(x) abort()

static unsigned yyline = 1;

char *stripped(char *string);
void current_string(char *s);
void current_number(char *s);
char *darwin_stripped_token(char *s, int offset);
int yywrap();
struct entry* get_seq_list();

#define AVLUSA(a)	/*printf("%s\n", a);*/


%}

DIGIT	[0-9]
WORD	[^ \t\n\>]+
IGSEQ	[^ \t\n0-9]+
WHITE   [ \t]
BP	[bB][pP]
DNA     [aAcCgGtT]
DNAambig     [aAcCgGtTmMrRwWsSyYkKvVhHdDbBnNxX]
RNA     [aAcCgGuU]
RNAambig     [aAcCgGuUmMrRwWsSyYkKvVhHdDbBnNxX]
AMINOACIDS [aArRnNdDcCqQeEgGhHiIlLkKmMfFpPsStTwWyYvVxX]
INDELS  [-+~_*.]

%option stack

%s origin locus definition accession comment
%s embl embl_locus embl_def embl_acc embl_seq
%s fasta_locus fasta_comment
%x fasta_seq
%s darwin_element
%s ig ig_seq
%s phylip_interleaved phylip_iseq1 phylip_iseq2

%p 3000
%n 1000
%%

^LOCUS			{ BEGIN(locus); return LOCUS; }
^{WHITE}*DEFINITION	{ BEGIN(definition); return DEFINITION; }
^{WHITE}*ACCESSION	{ BEGIN(accession); return ACCESSION; }
^{WHITE}*COMMENT	{ BEGIN(comment); return COMMENT; }

^ORIGIN(.)*   	{ BEGIN(origin); return ORIGIN; }

^{WHITE}+{DIGIT}+{WHITE}+{DIGIT}+{WHITE}*$	{ BEGIN(phylip_interleaved);
						  MSG("Phylip interleaved");
						  return PHYLIP_INTERLEAVED; }
^">"{WHITE}*		{ BEGIN(fasta_locus); MSG("FASTA_START"); return FASTA_START; }

"<"[^\n>]">"	{ yy_push_state(darwin_element);
		  current_string(darwin_stripped_token(yytext, 1)); 
		  return DARWIN_TOKEN;
		}
";".*\n         { BEGIN(ig); yyline++; }





<locus>{
{BP}		{ BEGIN(INITIAL);         AVLUSA("BP");  return BASEPAIRS; }
{DIGIT}+	{ current_number(yytext); AVLUSA("NR");  return NUMBER;}
{WORD}		{ current_string(yytext); AVLUSA("ORD"); return NAME; }
[ \t]*		{ return WHITESPACE; }
}

<origin>^"//"		{ BEGIN(INITIAL); return END_OF_ENTRY; }
<origin>^[ \t]*{DIGIT}+	{ current_number(yytext); return NUMBER; }
<origin>[ \t]*[^ \t\n]+	{ current_string(stripped(yytext)); 
			  return SEQUENCE_PART; 
			}
<origin>"\n"		{ yyline++; }

<definition>[^\n]*      { current_string(stripped(yytext)); 
			  BEGIN(INITIAL);
			  return TEXT;
			}
<accession>[^\n]*	{ current_string(stripped(yytext));
			  BEGIN(INITIAL);
			  return TEXT;
			}

<comment>{WHITE}+[^\n]*	{ current_string(stripped(yytext));
			  return TEXT;
			}
<comment>.		{ BEGIN(INITIAL); yyless(0); }




<fasta_locus>{WORD}	{ BEGIN(fasta_comment); current_string(stripped(yytext)); return TEXT; }
<fasta_comment>{
[^\n]*		{ current_string(stripped(yytext)); return TEXT; }
"\n"		{ BEGIN(fasta_seq); yyline++; return FASTA_SEQ_START; }
}

<fasta_seq>{
^">"{WHITE}*   { BEGIN(fasta_locus); return FASTA_START;}
{WORD}	       { current_string(stripped(yytext)); yyline++; return TEXT; }
{WHITE}*       {}
"\n"           { yyline++; }
}



<darwin_element>{
"</"[^\n>]">"	{ yy_pop_state();
		  current_string(darwin_stripped_token(yytext, 2)); 
		  return DARWIN_END_TOKEN;
		}
"<"[^\n]">"	{ yy_push_state(darwin_element);
		  current_string(darwin_stripped_token(yytext, 1)); 
		  return DARWIN_TOKEN;
		}
"\n"		{ yyline++; return DARWIN_EOL; }
.		{ current_string(yytext); 
		  return DARWIN_ELEMENT;
		}
} /* Darwin */


<ig>{
{WORD}.*	{ current_string(strtok(yytext, " \t")); BEGIN(ig_seq); return IG_START; }
}

<ig_seq>{
{IGSEQ}	{ current_string(stripped(yytext)); 
	  return IG_SEQ_PART; }
[12]	{ BEGIN(INITIAL); return IG_END_OF_ENTRY; }
}



"\n"		{ yyline++; }
.		{
		  ;		/* Eat the garbage */
		}

%%

/***
 ***   Help code
 ***/
unsigned
linenumber() 
{
  return yyline;
}

int
yywrap() {
  return 1;
}

/* stripped returns a pointer to the non-white position in s and
 * ends the string (with a '\0') after last non-white character.
 */

char *
stripped(char *s) 
{
  char *first, *last;
  
  first = s;
  while ((*first == ' ' || *first == '\t') && *first != '\0') {
    first++;
  }
  last = first;
  while (*last != '\0' && *last != ' ' && *last != '\t') {
    last++;
  }
  last = '\0';

  return first;
}

void
current_string(char *s)
{
  yylval.str = strdup(s);
}


void
current_number(char *s)
{
  sscanf(s, "%d", &yylval.integer);
}


char *
darwin_stripped_token(char *s, int offset) {
    int len;

    len = strlen(s);
    assert (s[len-2] != '>');
    s[len-2] = '\0';
    return &s[offset];
}

/*  
    seq_file2sfile
    In: An open file f
    Out: An sfile structure wrapping f. If no memory is
         available, the return value is NULL. 
 */
sfile * 
seq_file2sfile(FILE *f) {
    sfile *sf;
    
    sf = (sfile *) malloc(sizeof(sfile));
    if (sf == NULL) {
	return NULL;
    } else {
	sf->file = f;
	sf->flex_buffer_state = yy_create_buffer(f, YY_BUF_SIZE);
	sf->flex_start_condition = INITIAL;
	return sf;
    } 
}
    

void seq_close(sfile *sf) {
    if (sf != NULL) {
	if (sf->file != NULL) {
	    fclose(sf->file);
	    yy_delete_buffer((YY_BUFFER_STATE) sf->flex_buffer_state);
	    free(sf);
	} else {
	    WARNING("Programming error: Tried closing NULL file!");
	}
    } else {
	WARNING("Programming error: Tried closing NULL sequence file!");
    }
}

/* Read one or several GanBank entries from stdin. Returns
 * a list of entry structs.
 */

seq *
seq_read(sfile *sf, int n, int *ndone) 
{
    YY_BUFFER_STATE previous_buffer;
    int code;				/* return value */

    *ndone = 0;
    if (n > 0) {
	if (sf != NULL) {
	    /* Set the flex reader to the right context and input buffer */
	    previous_buffer = YY_CURRENT_BUFFER;
	    yy_switch_to_buffer((YY_BUFFER_STATE) sf->flex_buffer_state);
	    yy_push_state(sf->flex_start_condition); /* "state" is used confusingly in the flex docs*/
	    sf->entry_list = NULL;
	    
	    /* Ready to parse! */
	    if (yyparse() == 0) {
	      sf->entry_list = get_seq_list();
	    } else {
	      return NULL;
	    }

	    /* Reset reader and return sequence(s)*/
            sf->flex_start_condition =  yy_top_state();/* Saved until next time */
	    yy_pop_state();
	    if (previous_buffer != (YY_BUFFER_STATE) 0) {
		yy_switch_to_buffer(previous_buffer); /* Reset buffer for safety. */
	    }
            *ndone = seq_entry_list_length(sf->entry_list);
	    return sf->entry_list;
	} else {
	    WARNING("Progamming error: Tried to read from NULL sequence-file.");
	    return NULL;
	}
    } else {
	WARNING("Asked for less than zero (0) sequences.");
	return NULL;
    }

}

#ifdef BITROT			/* Can I resurrect this? */
/* Read one or several GanBank entries from stdin. Returns
 * a list of entry structs.
 */

seq *
seq_read(sfile *sf, int n, int *ndone) 
{
    YY_BUFFER_STATE previous_buffer;
/*     struct parser_control parserparams; */
    seq *last;
    int code;				/* return value */

    if (n > 0) {
	if (sf != NULL) {
	    /* Set the flex reader to the right context and input buffer */
	    previous_buffer = YY_CURRENT_BUFFER;
	    yy_switch_to_buffer((YY_BUFFER_STATE) sf->flex_buffer_state);
	    yy_push_state(sf->flex_start_condition); /* "state" is used confusingly in the flex docs*/
	    sf->entry_list = NULL;
	    
	    /* Ready to parse! */
/*  	    parserparams.entry_list = NULL; */
/* 	    parserparams.n_entries = 0; */
/* 	    parserparams.n_wanted_entries = n; */
/* 	    code = yyparse((void *) &parserparams); */
#ifdef OLD_CODE
	    if (code == 0) {
		sf->entry_list = parserparams.entry_list;
		*ndone = 1;
		for (last = sf->entry_list; n > *ndone; *ndone = *ndone + 1, last = last->next) {
		    code = yyparse((void *) &parserparams);
		    if (code == 1) {
			break;
		    } else {
			last->next = parserparams.entry_list;
		    }
		}
	    }
#endif
/* 	    sf->entry_list = parserparams.entry_list; */
/* 	    *ndone = parserparams.n_entries; */

	    /* Reset reader and return sequence(s)*/
	    (YY_BUFFER_STATE) sf->flex_start_condition = (YY_BUFFER_STATE) yy_top_state();/* Saved until next time */
	    yy_pop_state();
	    if (previous_buffer != (YY_BUFFER_STATE) 0) {
		yy_switch_to_buffer(previous_buffer); /* Reset buffer for safety. */
	    }
	    return sf->entry_list;
	} else {
	    WARNING("Progamming error: Tried to read from NULL sequence-file.");
	    return NULL;
	}
    } else {
	WARNING("Asked for less than zero (0) sequences.");
	return NULL;
    }
}

#endif /* BITROT */


/*******************************************************************
*
* Some parsing is ripped out for simplicity while changing code
* structure in gb.y and seqreader.l.
*
^ID{WHITE}+		{ BEGIN(embl_locus); return EMBL_LOCUS; }


<embl_locus>[^ \t\n]+	{ BEGIN(embl);
			  current_string(stripped(yytext)); 
			  return NAME; 
			}


<embl>{
"\n"		{ yyline++; }
^DE{WHITE}+	{ BEGIN(embl_def); return EMBL_DEFINITION; }
^AC{WHITE}+	{ BEGIN(embl_acc); return EMBL_ACCESSION; }
^SQ{WHITE}+	{ BEGIN(embl_seq); return EMBL_SEQUENCE_HEAD; }
}

<embl_def>[^\n]*      { current_string(stripped(yytext)); 
			BEGIN(embl);
			return TEXT;
		      }
<embl_acc>[^\n]*      {	current_string(stripped(yytext));
			BEGIN(embl);
			return TEXT;
		      }
<embl_seq>{
{DIGIT}+	{ current_number(yytext); return NUMBER; }
{WHITE}*{BP}{WHITE}*\n	{ return BASEPAIRS; }
"//"	{ BEGIN(INITIAL); return EMBL_END_OF_ENTRY; }
[ \t]*[^ \t\n]+	{ current_string(stripped(yytext)); 
			  	  return EMBL_SEQUENCE_PART; 
      			}
"\n"	{ yyline++; return EMBL_EOL; }
}



<phylip_interleaved>{
^{WORD}{1,9}/{WHITE}+   { current_string(stripped(yytext)); BEGIN(phylip_iseq); return TEXT; }
^{WORD}{10}             { current_string(stripped(yytext)); BEGIN(phylip_iseq); return TEXT; }
{WHITE}*"\n"		{ /o Blank line o/ return PHYLIP_INTERLEAVED_DIVIDER; }
}

<phylip_iseq1>{
{WHITE}+   { /o Just some alignment extra padding. Discard. o/ }
{WORD}+    { current_string(stripped(yytext)); return TEXT; }
"\n"	   { BEGIN(phylip_interleaved); }
}

<phylip_iseq2>{
{WHITE}+   { /o Just some alignment extra padding. Discard. o/ }
{DIGIT}+   { /o Just some alignment extra info. Discard. o/ }
{WORD}+    { current_string(stripped(yytext)); return TEXT; }
"\n"	   { }
}
*/
