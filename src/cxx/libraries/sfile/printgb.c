#include <string.h>
#include <stdio.h>

#include <entry.h>
/* #include <misc.h> */

#include "printgb.h"

int
print_simple_gb_entry (ent, file)
    struct entry *ent;
    struct print *file;
{
   char *top;

   while (ent)
     {
     if (ent->sequence && ent->seqlen != strlen (ent->sequence))
        {
        fprintf (stderr, "Sequence is not the same length as the seqlen.\n");
        }

     top = !strcmp(ent->topology, "Circular") ? ent->topology : "";
     set_left_margin (file, BEGIN);
     ff_printf(file, "LOCUS");
     set_left_margin (file, FFLEFT);
     ff_printf(file, "%-10s%7d bp %-7s  %-10s%-3s       %-11s",
         ent->locus, ent->seqlen, ent->moltype, top, ent->endiv, ent->distdate);
     ff_newline(file);

     set_left_margin (file, BEGIN);
     ff_printf(file, "DEFINITION");
     set_left_margin (file, FFLEFT);
     ff_printf(file, "%s", ent->definition);
     ff_newline(file);

     set_left_margin (file, BEGIN);
     ff_printf(file, "ACCESSION");
     set_left_margin (file, FFLEFT);
     ff_printf(file, "%s", ent->accession);
     if (ent->secaccs)
        ff_printf(file, " ");
     print_list (ent->secaccs, file, " ", "");

     set_left_margin (file, BEGIN);
     ff_printf(file, "KEYWORDS");
     set_left_margin (file, FFLEFT);
     add_hard_list (ent->keywords);
     print_list (ent->keywords, file, "; ", ".");
     remove_hard_list (ent->keywords);

     if (ent->segment)
        {
        set_left_margin (file, BEGIN);
        ff_printf(file, "SEGMENT");
        set_left_margin (file, FFLEFT);
        ff_printf(file, "%d of %d", ent->segment, ent->segs);
        ff_newline(file);
        }

/*      set_left_margin (file, BEGIN); */
/*      ff_printf(file, "SOURCE"); */
/*      set_left_margin (file, FFLEFT); */
/*      ff_printf(file, "%s", ent->source); */
/*      ff_newline(file); */

/*      set_left_margin (file, ORGANISM); */
/*      ff_printf(file, "ORGANISM"); */
/*      set_left_margin (file, FFLEFT); */
/*      ff_printf(file, "%s", ent->genusspec); */
/*      ff_newline(file); */
/*      add_hard_list (ent->taxes); */
/*      print_list (ent->taxes, file, "; ", "."); */
/*      remove_hard_list (ent->taxes); */

/*      print_reference (ent->references, file); */
/*      print_comment (ent->comments, file); */
/*      print_feature (ent->featoccs, file); */
/*      print_basecount (file, ent->sequence); */

     set_left_margin (file, BEGIN);
     ff_printf(file, "ORIGIN");
     set_left_margin (file, FFLEFT);
     ff_printf(file, "%s", ent->origin);

     print_sequence (file, ent->sequence);
     ff_printf(file, "//");
     ff_newline(file);

     ent = ent->next;
     }

   return (0);
}
