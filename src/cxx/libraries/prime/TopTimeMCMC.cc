#include "TopTimeMCMC.hh"

#include "AnError.hh"
#include "MCMCObject.hh"
#include "Node.hh"

#include <cassert>
#include <sstream>
#include <cmath>
#include <cassert>

// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
namespace beep
{

  using namespace std;

  //-------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //-------------------------------------------------------------
  // This now handles single leaved species trees. Note, however, that
  // when S is single-leaved, rootTime is NOT perturbed in MCMC!!
  //-------------------------------------------------------------
  TopTimeMCMC::TopTimeMCMC(MCMCModel& prior, Tree& S, Real beta)
    : StdMCMCModel(prior, (S.getRootNode()->isLeaf()?0:1), S.getName() + "_TopTime"),
      time(S.getTopTime()),
      beta(beta),
      max(S.rootToLeafTime()),
      oldRootTime(time),
      estimateTopTime((S.getRootNode()->isLeaf()?false:true)),
      suggestion_variance(0.001 * S.rootToLeafTime())
  {
    if(time <= 0)
      {
	if(S.getRootNode()->isLeaf())
	  {
 	    time = 1.0; // Arbitrary set time to 1.0
	  }
	else
	  {
	    // Use info in tree to get a reasonably scaled value
	    time = S.rootToLeafTime(); 
	  }
	oldRootTime = time; 
      }
    assert(time > 0);
    if(beta <= 0)
      {
	throw AnError("TopTimeMCMC::Beta must be positive",1);
      }
    assert(beta > 0);
  }

  TopTimeMCMC::~TopTimeMCMC()
  {
  }



  //-------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------
  void 
  TopTimeMCMC::fixTopTime()
  {
    estimateTopTime = false;
    if(n_params == 1)
      {
	n_params--;
	updateParamIdx();
      }  
  }

  Real& 
  TopTimeMCMC::getTopTime()
  {
    assert(time>0);
    return time;
  }

  void 
  TopTimeMCMC::setTopTime(Real t)
  {
    time = t;
  }


  MCMCObject
  TopTimeMCMC::suggestOwnState()
  {
    MCMCObject MOb(1.0,1.0);
    oldRootTime = time;

    time = perturbLogNormal(oldRootTime, suggestion_variance, 
			    0, -max * std::log(Real_limits::min()) / 5.0,
			    MOb.propRatio);
   
    MOb.stateProb = updateDataProbability();
    return MOb;
  }

  void 
  TopTimeMCMC::commitOwnState()
  {
//     oldRootTime = S.getRootNode()->getTime(); //not needed????!!
  }

  void
  TopTimeMCMC::discardOwnState()
  {
    time = oldRootTime;
  }


  string
  TopTimeMCMC::ownStrRep() const
  {
    ostringstream oss;
    if(n_params !=0)
      {
	oss << time
	    << "; ";
      }
    return oss.str();
  }

  string
  TopTimeMCMC::ownHeader() const
  {
    ostringstream oss;
    if(n_params !=0)
      {
	oss << "S_rootTime(float); "
	  ;
      }
    return oss.str();
  }

  Probability
  TopTimeMCMC::updateDataProbability()
  {
    assert(beta > 0);
    // The density function for the exponential distribution
    return 1.0/beta * std::exp(-time/beta);
  }


  //-------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const TopTimeMCMC& A)
  {
    return o << A.print();
  }

  string 
  TopTimeMCMC::print() const
  {
    ostringstream oss;
    oss << "TopTimeMCMC: The time before the species tree root is ";
    if(estimateTopTime)
      oss << "perturbed \n";
    else
      oss << "fixed \n";
    oss << "and is modeled by an exponential distribution with mean "
	<< beta
	<< "\n"
	<< StdMCMCModel::print()
      ;
    return oss.str();
  }
  


}//end namespace beep
