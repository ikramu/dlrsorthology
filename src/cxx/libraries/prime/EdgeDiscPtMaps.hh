#ifndef EDGEDISCPTMAPS_HH
#define EDGEDISCPTMAPS_HH

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "GenericMatrix.hh"
#include "Node.hh"
#include "Probability.hh"
#include "Tree.hh"
#include "TreeDiscretizers.hh"

namespace beep
{

// Forward declarations to avoid cyclic dependency.
class EdgeDiscTree;
template <class T>
class EdgeDiscPtMapIterator;
template <class T>
class EdgeDiscPtKeyIterator;

/********************************************************************
 * Map for storing data for each point of a discretized tree.
 * A new map is created by using an EgdeDiscTree as a template
 * (the latter is in fact also a subclassed map).
 * 
 * If the discretization/topology on which the map is based
 * changes, the map must be "rediscretized".
 * 
 * The map includes functionality for caching and restoring values.
 * No bounds checking is made upon element retrieval.
 * 
 * @author Joel Sj�strand.
 ********************************************************************/
template<typename T>
class EdgeDiscPtMap
{
	
protected:
	
	/**
	 * Protected constructor used for creating "baseline" point maps,
	 * i.e. original discretizations.
	 */
	EdgeDiscPtMap(Tree& S);
	
public:
	
	/**
	 * Public constructor. Creates a map filled with default elements with
	 * the same structure as an an original tree discretization.
	 * @param DS an original discretization acting as template.
	 * @param defaultVal default value for all points in the new map.
	 */
	EdgeDiscPtMap(EdgeDiscTree* DS, const T& defaultVal);
	
	/**
	 * Copy-constructor. Cached values and validity flag are also copied.
	 */
	EdgeDiscPtMap(const EdgeDiscPtMap& ptMap);
	
	/**
	 * Destructor.
	 */
	virtual ~EdgeDiscPtMap();
	
	/**
	 * Assignment operator. Cached values and validity flag are also copied.
	 */
	EdgeDiscPtMap& operator=(const EdgeDiscPtMap& ptMap);
	
	/**
	 * Returns the value at a certain point.
	 * @param pt the discretization point.
	 * @return the value at the point.
	 */
	inline T& operator() (const EdgeDiscretizer::Point& pt)
	{
		return (m_vals[pt.first][pt.second]);
	}
	
	/**
	 * Returns the value at a certain point.
	 * @param pt the discretization point.
	 * @return the value at the point.
	 */
	inline const T& operator() (const EdgeDiscretizer::Point& pt) const
	{
		return (m_vals[pt.first][pt.second]);
	}
	
	/**
	 * Returns the value at the point corresponding to a certain node.
	 * @param node the node.
	 * @return the value at the point corresponding to the node.
	 */
	inline T& operator() (const Node* node)
	{
		return (m_vals[node][0]);
	}
	
	/**
	 * Returns the value at the point corresponding to a certain node.
	 * @param node the node.
	 * @return the value at the point corresponding to the node.
	 */
	inline const T& operator() (const Node* node) const
	{
		return (m_vals[node][0]);
	}
	
	/**
	 * Returns the tree which the map refers to.
	 * @return the tree.
	 */
	Tree& getTree() const;

	/**
	 * Returns the number of points of an edge (NOT intervals!).
	 * For an empty top edge, there is only one, while for a
	 * non empty top edge the point of the tip is also included.
	 * @param node the lower end of the edge.
	 * @return the number of points.
	 */
	inline unsigned getNoOfPts(const Node* node) const
	{
		return (m_vals[node].size());
	}
	
	/**
	 * Returns the topmost value, i.e. the value at
	 * tip of the root's edge.
	 * @return the topmost value.
	 */
	T& getTopmost();
	
	/**
	 * Returns the topmost value, i.e. the value at
	 * tip of the root's edge.
	 * @return the topmost value.
	 */
	const T& getTopmost() const;
	
	/**
	 * Returns the topmost point of the discretization.
	 * @param node the node.
	 * @return the topmost point.
	 */
	EdgeDiscretizer::Point getTopmostPt() const;

	/**
	 * Returns an iterator starting at one of the points associated
	 * with this map. The iterator will iterate over all points, not
	 * just a specific node-to-root path.
	 * @return all-encompassing iterator start.
	 */
	EdgeDiscPtKeyIterator<T> beginKey();

	/**
	 * Returns an iterator that represents the end of a point iteration.
	 * This is used in conjunction with the iterator returned by beginKey()
	 * to know when the iteration has finished.
	 * @return all-encompassing iterator end.
	 */
	EdgeDiscPtKeyIterator<T> endKey();

	/**
	 * Returns an iterator starting at the point corresponding to a node.
	 * Iterator moves in the leaf-to-root direction when incremented.
	 * @param node the starting point.
	 * @return an iterator at the point corresponding to the node.
	 */
	EdgeDiscPtMapIterator<T> begin(const Node* node);
	
	/**
	 * Returns an iterator initially placed on a specified point.
	 * Iterator moves in the leaf-to-root direction when incremented.
	 * @param pt the starting point.
	 * @return an iterator at the point.
	 */
	EdgeDiscPtMapIterator<T> begin(const EdgeDiscretizer::Point& pt);
	
	/**
	 * Returns an iterator corresponding to the topmost point in
	 * the map, NOT BEYOND the topmost! See also 'endPlus()'.
	 * @return an iterator at the topmost point.
	 */
	EdgeDiscPtMapIterator<T> end();
	
	/**
	 * Works similar to 'end()' method, but returns an
	 * iterator PAST the very tip of the root edge.
	 * @return an iterator just above the topmost point.
	 */
	EdgeDiscPtMapIterator<T> endPlus();
	
	/**
	 * Returns an iterator corresponding to the upper end of an edge,
	 * i.e. placed on the edge's upper node. In case of the "top edge",
	 * this corresponds instead to the topmost point (i.e. the tip of that edge).
	 * See also 'endPlus(node)'.
	 * @param node the lower end of the edge.
	 * @return the iterator to the upper end.
	 */
	EdgeDiscPtMapIterator<T> end(const Node* node);
	
	/**
	 * Works similar to the 'end(node)', but for the top edge,
	 * returns an iterator PAST the very tip of the root edge.
	 * @return an iterator at the upper end of the edge,
	 *         or just above the end in case of the top edge.
	 */
	EdgeDiscPtMapIterator<T> endPlus(const Node* node);
	
	/**
	 * Reinitializes the data holders. Use this when the
	 * underlying discretization map has changed structure.
	 * @param defaultVal the value to be set.
	 */
	virtual void rediscretize(const T& defaultVal);

	/**
	 * Resets all values in entire map to the specified value
	 * without changing the underlying structure.
	 * @param defaultVal the value to be set.
	 */
	virtual void reset(const T& defaultVal);
	
	/**
	 * Saves all current values in a cache. Cached values can be
	 * restored with a call to 'restoreCache()', or invalidated
	 * via call to 'invalidateCache()'.
	 */
	virtual void cache();
	
	/**
	 * Partial cache that saves all points from a node and along the path upwards
	 * in the tree. Typically used when only parts of the map
	 * are used. A symmetrical restoration via can be achieved by calling
	 * 'restoreCache(node)'.
	 * @param node the lowest point on the path.
	 */
	virtual void cachePath(const Node* node);
	
	/**
	 * Restores cached values. Has no effect if
	 * 'invalidateCache()' has been invoked since call to 'cache()'.
	 */
	virtual void restoreCache();
	
	/**
	 * Restores cached values along the path from a node and upwards in the tree.
	 * Has no effect if 'invalidateCache()' has been invoked since caching.
	 * Entire cache is invalidated afterwards, so one cannot perform consecutive
	 * calls to this method.
	 * @param node the lower node of the path.
	 */
	virtual void restoreCachePath(const Node* node);
	
	/**
	 * Disables last made cache.
	 */
	virtual void invalidateCache();
	
	/**
     * Returns a string representation of the map.
     * @return a string representation.
     */
    std::string print() const;
    
    /**
     * Returns a string representation of the map for
     * the path from a lower edge to the root.
     * @param node the lower node of the path.
     * @return a string representation.
     */
    std::string printPath(const Node* node) const;
    
    /**
     * normalize the node placement values
     * @param node the lower node of the path
     */
    T normalizeToProbabilities(const Node *node);
    
    /**
     * Prints the map.
     */
	friend std::ostream& operator<<(std::ostream &o, const EdgeDiscPtMap& ptMap)
    {
    	return o << ptMap.print();
    }
	
protected:
	
	/** Discretized tree on which map is based. */
	EdgeDiscTree* m_DS;
	
	/** Values for all points, one vector per edge. */
	BeepVector< std::vector<T> > m_vals;
	
	/** Cached values. */
	BeepVector< std::vector<T> > m_cache;
	
	/** Indicates cache validity. */
	bool m_cacheIsValid;
};



/********************************************************************
 * 2-D map for storing data for each pair of points of an EdgeDiscPtMap.
 * One may choose to create a map which can hold a value for all (ptx,pty),
 * or only pairs where ptx>=pty in the partial order (where pty is the descendant).
 * 
 * A new map is created by copying the structure from a discretized tree.
 * 
 * If the discretization/topology on which the map is based
 * changes, the map must be "rediscretized".
 * 
 * The map includes functionality for caching and restoring values. No bounds
 * checking is made upon element retrieval.
 * 
 * @author Joel Sj�strand.
 ********************************************************************/
template<typename T>
class EdgeDiscPtPtMap
{
	
public:
	
	/**
	 * Public constructor. Creates a map filled with default elements.
	 * @param DS a discretized tree.
	 * @param defaultVal default value for all points in the new map.
	 * @param subtreeOnly if true, only stores point-to-point values for an
	 *        edge to itself and edges at its subtree below. More efficient
	 *        in case only "partial order pairs" are considered.
	 */
	EdgeDiscPtPtMap(EdgeDiscTree& DS, const T& defaultVal, bool subtreeOnly);
	
	/**
	 * Copy-constructor. Cached values and validity flag are also copied.
	 * @param ptPtMap map to copy.
	 */
	EdgeDiscPtPtMap(const EdgeDiscPtPtMap& ptPtMap);
	
	/**
	 * Destructor.
	 */
	~EdgeDiscPtPtMap();
	
	/**
	 * Assignment operator. Cached values and validity flag are also copied.
	 */
	EdgeDiscPtPtMap& operator=(const EdgeDiscPtPtMap& ptPtMap);
	
	/**
	 * Returns the point-to-point value for (x,y).
	 * @param x the first point.
	 * @param y the second point.
	 * @return the value.
	 */
    inline T& operator() (const EdgeDiscretizer::Point& x, const EdgeDiscretizer::Point& y)
    {
    	std::vector<T>& v = m_vals(x.first->getNumber(),
				   y.first->getNumber());
    	return v[x.second * m_noOfPts[y.first] + y.second];
    }
    
    /**
	 * Returns the point-to-point value for (x,y).
	 * @param x the first point.
	 * @param y the second point.
	 * @return the value.
	 */
    inline const T& operator() (const EdgeDiscretizer::Point& x, const EdgeDiscretizer::Point& y) const
    {
    	const std::vector<T>& v = m_vals(x.first->getNumber(), y.first->getNumber());
    	return v[x.second * m_noOfPts[y.first] + y.second];
    }
    
	/**
	 * Creates the entire discretization anew. Use this when the
	 * underlying discretization map has changed structure.
	 * @param defaultVal the value to be set.
	 */
	void rediscretize(const T& defaultVal);
    
	/**
	 * Resets all values in entire map to the specified value without
	 * changing the underlying structure.
	 * @param defaultVal the value to be set.
	 */
	void reset(const T& defaultVal);
	
	/**
	 * Saves all current values in a cache. Cached values can be
	 * restored with a call to 'restoreCache()', or invalidated
	 * via call to 'invalidateCache()'.
	 */
	void cache();
	
	/**
	 * Restores all cached values. Has no effect if
	 * 'invalidateCache()' has been invoked since call to 'cache()'.
	 */
	void restoreCache();
	
	/**
	 * Disables last made cache.
	 */
	void invalidateCache();
	
private:
	
	/** Discretized tree on which map is based. */
	EdgeDiscTree& m_DS;
	
	/**
	 * If true, stores only values for pairs (ptx,pty) for 
	 * which there is a path between their corresponding edges.
	 */
	bool m_subtreeOnly;

	/** Number of points (not intervals) for each edge. */
	BeepVector<unsigned> m_noOfPts;
	
	/**
	 * Point values stored thus: outer matrix corresponds to 
	 * edge-to-edge, inner vector is a concatenated matrix 
	 * holding point-to-point values for the two edges.
	 */
public:
	GenericMatrix< std::vector<T> > m_vals;
	
	/** Cached values. */
	GenericMatrix< std::vector<T> > m_cache;
		
	/** Indicates cache validity. */
	bool m_cacheIsValid;
	
};


// Typedefs. Make sure to 'templatize' them in source file.
typedef EdgeDiscPtMap<Real> RealEdgeDiscPtMap;
typedef EdgeDiscPtMap<Probability> ProbabilityEdgeDiscPtMap;
typedef EdgeDiscPtPtMap<Real> RealEdgeDiscPtPtMap;
typedef EdgeDiscPtPtMap<Probability> ProbabilityEdgeDiscPtPtMap;

typedef EdgeDiscPtMapIterator<Real> RealEdgeDiscPtMapIterator;
typedef EdgeDiscPtMapIterator<Probability> ProbabilityEdgeDiscPtMapIterator;

} // end namespace beep

#endif // EDGEDISCPTMAPS_HH
