#ifndef EPOCHBDTMCMC_HH
#define EPOCHBDTMCMC_HH

#include <string>
#include <iostream>

#include "EpochBDTProbs.hh"
#include "StdMCMCModel.hh"

namespace beep
{

/**
 * MCMC model for altering birth, death and lateral transfer rates
 * for a discretized epoch tree (see documentation for EpochBDTProbs
 * for more info).
 * 
 * In each iteration, one of the birth, death and transfer rates are
 * perturbed with equal chance. The proposed value is chosen according
 * to a lognormal distribution with a suggestion variance around the old
 * value.
 * 
 * Caching is used so that data structures of the underlying
 * object (holding the rates) are stored when suggestOwnState()
 * is invoked, and these are restored in case of a discardOwnState().
 * 
 * If one of the rates is initialized to 0, it will not be perturbed.
 * 
 * @author Joel Sjöstrand.
 */
class EpochBDTMCMC : public StdMCMCModel
{

public:

	/**
	 * Constructor.
	 * @param prior the prior in the MCMC-chain.
	 * @param BDTProbs the birth-death-transfer rates and
	 * related probabilities.
	 * @param suggestRatio the suggestion ratio.
	 */
	EpochBDTMCMC(MCMCModel& prior, EpochBDTProbs& BDTProbs,
			const Real& suggestRatio);
	
	/**
	 * Destructor.
	 */
	virtual ~EpochBDTMCMC();

	/**
	 * Fixes rates to their current values.
	 */
	void fixRates();

	/**
	 * Perturbs the rates with equal probability.
	 * @return the MCMC object associated with the state.
	 */
	MCMCObject suggestOwnState();
	
	/**
	 * Commits the state. Resets the perturbation status of 
	 * the object holding the rate parameters.
	 */
	void commitOwnState();
	
	/**
	 * Restores previous rates.
	 */
	void discardOwnState();
	
	/**
	 * Returns a string with parameter values in accordance with MCMC model guidelines.
	 * @return the current parameter values in a string.
	 */
	std::string ownStrRep() const;
	
	/**
	 * Returns a string with rate parameter names in accordance
	 * with MCMC model guidelines.
	 * @return the parameter names in a string.
	 */
	std::string ownHeader() const;
	
	/**
	 * Returns more detailed acceptance info.
	 * @return detailed acceptance info.
	 */
	virtual std::string getAcceptanceInfo() const;

	/**
	 * No data probability of its own, returns 1.0.
	 * @return 1.
	 */
	Probability updateDataProbability();
	
	/**
	 * Friend helper for printing the object. Uses member method print() for the task.
	 * @param o the ostream reference.
	 * @param obj the object to print.
	 * @return the appended ostream reference.
	 */
	friend std::ostream& operator<<(std::ostream& o, const EpochBDTMCMC& obj);

	/**
	 * Returns an information string for the object.
	 * @return the info string.
	 */
	std::string print() const;

private:

	/**
	 * Update borders used for determining which parameter to perturb.
	 */
	void updateBorders();
	
public:

private:
	
	/** The current rates and precomputed probs. */
	EpochBDTProbs& m_BDTProbs;
	
	/** Set to 1 for each of rates to keep fixed. */
	std::vector<bool> m_fixRates;

	/** Convenience borders for which param to perturb. */
	std::pair<Real,Real> m_rndBorders;
	
	/**
	 * The suggestion function uses a normal distribution around the current
	 * value. For the proposal ratios to function right, we need to keep the
	 * variance to the distribution constant. This is a problem since we do not
	 * know the location/scale of the distribution ahead of time. As a fix, we
	 * store a variance initiated from the start values of rates.
	 */
	//Real m_birthSuggestionVar;
	//Real m_deathSuggestionVar;
	//Real m_transferSuggestionVar;
	
	unsigned m_which;                             /*< Flag for curr. perturbed param. */
	std::pair<unsigned,unsigned> m_bAccPropCnt;   /*< Birth acc. and tot. prop. count. */
	std::pair<unsigned,unsigned> m_dAccPropCnt;   /*< Death acc. and tot. prop. count. */
	std::pair<unsigned,unsigned> m_tAccPropCnt;   /*< Transfer acc. and tot. prop. count. */

};

}//end namespace beep

#endif /*EPOCHBDTMCMC_HH*/

