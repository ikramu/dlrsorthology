#ifndef RECONCILIATIONTREEGENERATOR_HH
#define RECONCILIATIONTREEGENERATOR_HH

#include <iostream>
#include <string>
#include <vector>

#include "Beep.hh"
#include "BeepVector.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "Node.hh"
#include "PRNG.hh"  
#include "StrStrMap.hh"  
#include "Tree.hh"  


namespace beep
{

  //----------------------------------------------------------------------
  //! Author: Bengt Sennblad
  //! �the MCMC-club, Stockholm Bioinformatics Center, 2002
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  //
  //! Class ReconciliationTreeGenerator
  //! This class contains functions for generating a tree, most often a 
  //! species tree, according to the Birth-death model specified by 
  //! attribute BD.
  //
  //----------------------------------------------------------------------
  class ReconciliationTreeGenerator
  {
  public:
    //----------------------------------------------------------------------
    //
    //Constructors and Destructors and Assignment
    //
    //----------------------------------------------------------------------
    ReconciliationTreeGenerator(BirthDeathProbs& BDP, 
				const std::string &prefix = "");
    ReconciliationTreeGenerator(ReconciliationTreeGenerator &TG);
    ~ReconciliationTreeGenerator(); 

    ReconciliationTreeGenerator 
    & operator=(const ReconciliationTreeGenerator &TG);


    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    //! \name Tree generation interface
    //----------------------------------------------------------------------
    //@{

    //! generateGammaTree(leaves, generateEdgeTimes)
    //! Generates a guest tree with a specified size of the top slice
    //! typically this is used when you want to generate a "unreconciled"
    //! tree, i.e., the host tree has only a single vertex, with a specified
    //! number of leaves; or when you want a tree with no top slice (nleaves=1).
    //! Note that gamma is not a GammaMap. This is because GammaMap needs a 
    //! full tree as input. View gamma as the working sketch
    //----------------------- -----------------------------------------------
    void generateGammaTree(const unsigned& nleaves, 
			   const bool& generateRootTime = true);

    //! generateGammaTree(generateEdgeTimes)
    //! Generates a guest tree where the size of the top slice is drawn from 
    //! the birth death process
    //----------------------------------------------------------------------
    void generateGammaTree(const bool& generateRootTime = true);
    //@}

    //! name export of trees, gamma, etc.
    //----------------------------------------------------------------------
    //@{
    Tree getStree();
    Tree exportGtree();
    Tree& getGtree();
    GammaMap exportGamma();
    StrStrMap exportGS() const;
    //@}


    //---------------------------------------------------------------------
    //! \name I/O
    //---------------------------------------------------------------------
    //@{
    friend std::ostream& operator<<(std::ostream &o, 
				    const ReconciliationTreeGenerator& tsm);
    //! output of gs
    //----------------------------------------------------------------------
    std::string gs4os();
    //@}

    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------
    
    //! \name generate helpers
    //----------------------------------------------------------------------
    //@{    
    Node* generateSlice(unsigned nLeaves, Node& sn);
    Node* generateSubtree(Node& sn);
    //@}

    //! growTree() used by simulateTree 
    //----------------------------------------------------------------------
    Node* growTree(std::vector<Node*>& leaves);

    //! updateTrueGamma
    //! reads simGamma and converts to GammaMap
    //---------------------------------------------------------------------
    void createTrueGamma(GammaMap& tmpGamma) const;

    //! print function for parameters used in operator<<
    //--------------------------------------------------------------------
    const std::string print() const;

  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //! \todo{make these pointers}
    //----------------------------------------------------------------------
    Tree& S;               //!< This is always taken from BDP
    BirthDeathProbs& BDP;  //!< The probabilites are taken from here 
    PRNG& R;               //!< Random number generator

    Tree G;                //!< The generated tree
    StrStrMap gs;
    std::vector<SetOfNodes> gamma; //!< This could possibly be a BeepVector

    std::string leaf_prefix; //!< So that we can add info from commandline to gene names
  };

}//end namespace beep
#endif
