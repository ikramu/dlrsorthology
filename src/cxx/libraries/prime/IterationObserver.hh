#ifndef _ITERATIONOBSERVER_HH
#define	_ITERATIONOBSERVER_HH

#include "SubstitutionMCMC.hh"
#include <string>

class MCMCModel;

namespace beep {
 

  class IterationObserver {
  public: 
    IterationObserver( unsigned int iter_total ) { m_iter_total = iter_total; return; };
    virtual    ~IterationObserver(){};
    unsigned int iterations() { return m_iter_total; };
    virtual bool afterEachStep(MCMCModel & model, unsigned int iterationsPerformed, bool stateWasChanged, std::string & stdoutStr, std::string & stderrStr ) = 0;

  protected: 
    unsigned int m_iter_total;
  };
}

#endif	/* _ITERATIONOBSERVER_HH */


