#ifndef STDMCMCMODEL_HH
#define STDMCMCMODEL_HH

#include <iostream>
#include <string>

#include "MCMCModel.hh"
#include "Probability.hh"



namespace beep
{
  // Forward declarations


  //---------------------------------------------------------------
  //
  // class StdMCMCModel
  //! Baseclass that provides common implementation code 
  //! for the a subclass of"MCMCModel-class family", which uses 
  //! MCMC to intergrate over parameters.
  //!
  //! An instance of the StdMCMCModel class will inherit from a 
  //! ProbabilityModel class for likelihood calculations. 
  //! It also contains a MCMCModel class for calculation of any 
  //! additional prior probs that might exists; give a DummyMCMC 
  //! in constructor if no such prior occur. These two classes 
  //! contain the parameters fro StdMCMCModel to perturb.
  //! Typically, an instance class constructor looks like:
  //! InstanceMCMC::InstanceMCMC(MCMCModel& prior, ProbabilityModel& like)
  //!     : StdMCMCModel(prior, 3, 0.4),
  //!       ProbabilityModel(like),
  //!       ...
  //!
  //! The main task for this class is to decide on when, on 
  //! suggestNewState()-calls, to pass this call on to the nested 
  //! prior classes and when to perturb the parameter of the 
  //! ProbabilityModel baseclass.
  //! The suggestRatio is a parameter that allows the user to affect
  //! how often a local parameter or a prior parameter should 
  //! be updated (default is a uniform probability over all local
  //! and prior states):
  //! suggestRatio = pr{a}/pr{b}, where a = the average local 
  //! parameter a is perturbed, b = the average prior parameter b 
  //! is perturbed. Setting suggestratio to 2.0 will perturb a local
  //! parameter twice as often as a prior parameter. The attribute
  //! numParams is the number of local parameters. If a local parameter 
  //! is to be perturbed, the pure virtual' function perturbState() 
  //! is called.
  //!
  //! This class currently inherits from MCMCModel, it is possible
  //! that it might eventually replace MCMCModel.
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  //
  //---------------------------------------------------------------
  class StdMCMCModel : public MCMCModel
  {
  public:

    //! Tuning constants for certain proposal methods. Smaller values
    //! implies more bold proposals (i.e. larger suggestion variance).
    enum SuggestionAudacity
      {
	FIVE_PCT, TWENTYFIVE_PCT, FIFTY_PCT, SEVENTYFIVE_PCT, NINETYFIVE_PCT
      };

    //---------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    // 
    //---------------------------------------------------------------

    //! Constructor.
    //! suggestRatio = pr{a}/pr{b}, a = a parameter of 'this' is perturbed, 
    //! b = a prior parameter is pertubed. paramIdxRatio adjusts for 
    //! unequal number of like and prior parameters.
    //---------------------------------------------------------------
    StdMCMCModel(MCMCModel& prior, const unsigned& n_params,
		 const Real& suggestRatio = 1.0);
    StdMCMCModel(MCMCModel& prior, const unsigned& n_params,
		 const std::string& name, const Real& suggestRatio = 1.0);
    StdMCMCModel(const StdMCMCModel& A);
    virtual ~StdMCMCModel();
    StdMCMCModel& operator=(const StdMCMCModel& A);

    //! Deprecated! don't use! will be phased out! for back-compatibility only!
    //----------------------------------------------------------------------
    StdMCMCModel(PRNG& R_in, MCMCModel& prior_in, 
		 const unsigned n_params, const Real& suggestRatio) :
      MCMCModel(),prior(&prior_in),n_params(n_params), stateProb(1.0),
      old_stateProb(stateProb), suggestRatio(suggestRatio),
      paramIdxRatio(n_params==0?0:1/(1+(prior_in.nParams()*suggestRatio/n_params*(1.0-suggestRatio)))),
      paramIdx(0)
    {
      std::cerr << "StdMCMCModel: Deprecated constructor - correct your code or perish\n";
    };

    //---------------------------------------------------------------
    //
    // Standardized interface from MCMCModel
    // You should never really need to change this!
    // 
    //---------------------------------------------------------------
    MCMCObject suggestNewState();
    MCMCObject suggestNewState(unsigned x);
    Probability initStateProb();
    Probability currentStateProb();
    void commitNewState();
    void commitNewState(unsigned x);
    void discardNewState();
    void discardNewState(unsigned x);
    std::string strRepresentation() const;
    std::string strHeader() const;
    // TODO: if prior is guaranteed to take a StdMCMCModel, this function 
    // could be moved to StdMCMCModel (only used there) /bens
    //----------------------------------------------------------------------
    unsigned nParams() const;
    void setName(const std::string& name_in);
    std::string getName();
	
    //----------------------------------------------------------------------
    //
    // Internal interface
    //
    // For example (do = suggest/ commit / discard):
    //     virtual doState()
    //     {
    //       Real Idx = paramIdx / paramIdxRatio;
    //       if(Idx < x)
    // 	       do x();
    //       else if(Idx < y)
    // 	       do y();
    //       else
    // 	       do z();
    //     }
    //
    //     virtual ownStrRep()
    //     {
    //       std::ostringstream oss;
    //       oss << x << " "
    // 	         << y << " "
    // 	         << z << " "
    // 	         ;
    //       return oss.str();
    //     }
    //
    //----------------------------------------------------------------------
    virtual MCMCObject suggestOwnState() = 0;
    virtual MCMCObject suggestOwnState(unsigned x);

    virtual void commitOwnState() = 0;
    virtual void commitOwnState(unsigned x);
    virtual void discardOwnState() = 0;
    virtual void discardOwnState(unsigned x);

    virtual std::string ownStrRep() const = 0;
    virtual std::string ownHeader() const = 0;

    virtual void setChangingSuggestRatio(Real finalRatio, unsigned noOfSteps);
	
    // I have lumped calls to p.update() and p.calculateDataProbability(),
    // where p is the ProbabilityModel "baseclass", because I think that they
    // will almost always be called together
    //----------------------------------------------------------------------
    virtual Probability updateDataProbability()  = 0;

    //----------------------------------------------------------------------
    //
    // I/O
    // I have not converged on a consensus for this yet, but the 
    // following functions should suffice
    //
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const StdMCMCModel& A);
    virtual std::string print() const;

    //-----------------------------------------------------------
    // Recursive method which returns a string with acceptance
    // information for each model in whatever format suitable
    // (all lines are prepended with '#' though).
    //-----------------------------------------------------------
    virtual std::string getAcceptanceInfo() const;

  protected:
    //---------------------------------------------------------------
    //
    // Implementation
    // 
    //---------------------------------------------------------------

    //! updates paramIdx if, e.g., n_params has been changed since 
    //! construction. NOTE! This can not redirect update to StdMCMCModls 
    //! nested higher in MCMC-chain, thus n-params should not be changed 
    //! once MCMC-chain has been setup
    // TODO: this should be fixed! We might need such changes /bens
    void
    updateParamIdx();
    void //REMOVE
    raiseSuggestRatio();

  protected:
    //! Assigns a unique name to an obejct of this class.
    //! Typically '[base][unique number]'.
    virtual void initName(std::string base);

    //! samples \f$ Y \sim N(x, v) \f$ where x = value and v =
    //! variance and proposalRatio = 1.0.
    //! \todo{The performance of this has been tested on a likelihood 
    //! distribution that is U(0,100) and a variance = 0.1. 
    //! If a direct rejection step for new value == old value or
    //! new value not in (0,100), the probabilities of margin values 
    //! will have a lower probability. /bens}
    virtual Real perturbNormal(Real value, Real variance, Real Min, 
			       Real Max, Probability& propRatio) const;

    //! samples \f$ \log{y} \sim logN(\log{x}, v) \f$ where x = value and v = 
    //! variance and proposalRatio = 1.0.
    //! \todo{The performance of this has been tested on a likelihood 
    //! distribution that is U(0,100) and a variance = 0.1. 
    //! If a direct rejection step for new value == old value or
    //! new value not in (0,100), the probabilities of margin values 
    //! will have a lower probability. /bens}
    //! joelgs: Documentation seemed inconsistent with actual performance,
    //! see implementation.
    virtual Real perturbLogNormal(Real value, Real variance, Real Min, 
				  Real Max, Probability& propRatio, 
				  unsigned strict = 0) const;

    //! samples \f$ \log{y} \sim U(\log{x}, v) \f$ where x = value and v = 
    //! variance and proposalRatio = 1.0.
    //! joelgs: Documentation seemed inconsistent with actual performance,
    //! see implementation.
    virtual Real perturbUniform(Real value, Real variance, Real Min, 
				Real Max, Probability& propRatio) const;

    //! Samples \f$ Y ~ N(x, var) \f$ conditional on boundaries a < Y < b,
    //! where x = value, var is computed as described below, a = min and b = max.
    //! The variance is calculated so that Pr[(1-s)*x < Y < (1+s)*x] = t where
    //! s = width and t = within, i.e. smaller t implies bolder proposals.
    //! Note that this variance formula refers to the equivalent non-truncated
    //! distribution, so if x is close to a or b, the situation might differ slightly.
    //! Note: After 100 failed attempts to draw, returns old value along with propRatio=0.
    virtual Real perturbTruncatedNormal(Real value, Real width, SuggestionAudacity within,
					Real min, Real max, Probability& propRatio) const;

    //! Samples \f$ Y ~ N(x, var) \f$ conditional on boundaries a < Y < b,
    //! where x = value, var = f(x,a,b,c), a = min, b = max, and c = hyper.
    //! The variance function, f = varFunc, is provided as a function pointer and
    //! may thus be of arbitrary nature based on (at most) current value,
    //! interval and an additional hyperparameter.
    //! Note that the variance refers to the equivalent non-truncated
    //! distribution, so if x is close to a or b, the situation might differ slightly.
    //! Note: After 100 failed attempts to draw, returns old value along with propRatio=0.
    virtual Real perturbTruncatedNormal(Real value, Real (*varFunc)(Real, Real, Real, Real),
					Real min, Real max, Probability& propRatio, Real hyper=-1) const;

  private:

    //! Static methods used by certain proposal methods.
    static Real GetSuggVarForFivePct(Real val, Real min, Real max, Real width)
    { Real stddev = (width*val)/0.0627;   return (stddev*stddev); }
    static Real GetSuggVarForTwentyFivePct(Real val, Real min, Real max, Real width)
    { Real stddev = (width*val)/0.3186;   return (stddev*stddev); }
    static Real GetSuggVarForFiftyPct(Real val, Real min, Real max, Real width)
    { Real stddev = (width*val)/0.6745;   return (stddev*stddev); }
    static Real GetSuggVarForSeventyFivePct(Real val, Real min, Real max, Real width)
    { Real stddev = (width*val)/1.1504;   return (stddev*stddev); }
    static Real GetSuggVarForNinetyFivePct(Real val, Real min, Real max, Real width)
    { Real stddev = (width*val)/1.96;     return (stddev*stddev); }

  protected:
    //---------------------------------------------------------------
    //
    // Attributes
    // 
    //---------------------------------------------------------------
    MCMCModel* prior;          //! Nested StdMCMCModel in MCMC

    unsigned n_params;         //!< size of parameter space for *this.
    Probability stateProb;     //! Current probability of *this and prior.
    Probability old_stateProb; //! Previous probability of *this and prior.
    Real suggestRatio;         //!< Weight given to suggesting *this instead of prior
    Real suggestRatioDelta;    //!< In-/decrease delta when ratio allowed to change.
    unsigned suggestRatioPendingUpdates; //!< No of steps ratio is allowed to change
    //!< with delta (in beginning of chain).
    Real paramIdxRatio;        //!< Probability of perturbing *this instead of 
    //!< prior in MCMC-iteration
    Real paramIdx;             //!< Keeps track of perturbed object (*this or prior)

    static unsigned unique; //!< Assigns unique numbers to name
    std::string name;       //!< Unique name of model. 
    //!< Defaults to 'model[uniquenumber]'
  };
}//end namespace beep

#endif
