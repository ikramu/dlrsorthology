#include "LengthRateModel.hh"
#include "CongruentGuestTreeTimeMCMC.hh"

namespace beep
{
  using namespace std;

  //--------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //--------------------------------------------------------
  LengthRateModel::LengthRateModel(Density2P& rateProb_in, Tree& T_in,
		  EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeWeightModel(),
      rateModel(rateProb_in, T_in),
      cttm(NULL),
      perturbedRootEdges(rwp)
  {
    if(T_in.hasLengths())
      {
	weights = &T_in.getLengths();
      }
    else
      {
	weights = new RealVector(T_in);
      }
  }
  
  LengthRateModel::~LengthRateModel()
  {};
    
  //--------------------------------------------------------
  //
  // Interface
  //
  //--------------------------------------------------------
  // In addition to the standard ProbabilityModel interface, we have:
  // use CongruentTreeTimeMCMC
  void 
  LengthRateModel::setTimeListener(CongruentGuestTreeTimeMCMC& m)
  {
    cttm = &m;
  }
  
  // Access parameters
  //------------------------------------------------------------------
  // Returns the tree on which edge weights are modeled
  const Tree& 
  LengthRateModel::getTree() const
  {
    return rateModel.getTree();
  }
  
  // number of weights that are modeled
  unsigned 
  LengthRateModel::nWeights() const
  {
    return weights->size();
  }
  
  // Returns reference to vector of weight for (incoming edge to) Node u. 
  RealVector& 
  LengthRateModel::getWeightVector() const 
  {
    return *weights;
  }
  
  // Returns weight for (incoming edge to) Node u. 
  Real 
  LengthRateModel::getWeight(const Node& u) const
  {
    return (*weights)[u];
  }
  
  // set the weight for (incoming edge to) Node u to 'weight'.
  void 
  LengthRateModel::setWeight(const Real& weight, const Node& u)
  {
    (*weights)[u] = weight;
  }
  
  // set the weight for (incoming edge to) Node u to 'weight'.
  void 
  LengthRateModel::setWeightVector(RealVector& newWeights)
  {
    weights = &newWeights;
  }
  
  // get legitimate range of weights
  void 
  LengthRateModel::getRange(Real& low, Real& high)
  {
    rateModel.getRange(low, high); // * rateModel.getTree().rootToLeafTime()?
  }
  
  void 
  LengthRateModel::update()
  {
    if(cttm)
      {
	cttm->update();
      }
    const Tree& refT = rateModel.getTree();
    for(unsigned i = 0; i < refT.getNumberOfNodes(); i++)
      {
	Node* n = refT.getNode(i);
	if(n->isRoot())
	  {
	    continue;
	  }
	else if(n->getParent()->isRoot())
	  {
	    Node* s = n->getSibling();
	    rateModel.setRate(((*weights)[n] + (*weights)[s]) /
		    ( n->getTime() + s->getTime()), n);
	  }	 
	else  
	  {   
	    Real new_rate = (*weights)[n] / n->getTime();
	    
	    rateModel.setRate(new_rate, n);
	  }
      }
    return; 
  }
  
  Probability 
  LengthRateModel::calculateDataProbability()
  {
    return rateModel.calculateDataProbability();
  }

  //-------------------------------------------------------------------
  //
  // IO
  //
  //-------------------------------------------------------------------
  string 
  LengthRateModel::print() const
  {
    return"FILL THIS IN!\n";
  }
  
  
}//end namespace beep

