#ifndef ORTHORECONMCMC_HH
#define ORTHORECONMCMC_HH

/*****************************************************************************

  OrthoReconMCMC

  This class implements an MCMC method for finding gene trees. Using tree
  change tricks such as NNI and re-rooting, the tree space is investigated.
  A prior probability of a gene tree is given by 
     $ \Pr(G | S) = \sum_{\gamma} \Pr(G, \gamma | S)$
  as computed by ReconciliationSampler (or rather the methods it inherits from
  ReconciliationModel). The likelihood of a gene tree given the sequence data
  is then approximated using ReconSeqApproximator.

*****************************************************************************/

#include "Beep.hh"
#include "DupSpecProbs.hh"
#include "MCMCModel.hh"
#include "PRNG.hh"
#include "ReconciliationApproximator.hh"
#include "ReconciliationSampler.hh"
#include "Tree.hh"


class OrthoReconMCMC : public beep::MCMCModel
{
public:
  //------------------------------------------------------------------
  //
  // Constructor
  // The birthRate and deathRate parameters are start values for
  // the ReconciliationSampler.  If you are not sure what a good
  // starting point is, use something simple such as (0.001, 0.001)
  // and then use the method chooseStartingRates below.
  //
  // The beta param is for the top-slice model, see
  // DupSpecProbs::topPartialProbOfCopies for details. 
  //
  // Please notice default values for birth/death rates. If negative,
  // a sensible starting point will be chosen automatically.
  // 
  OrthoReconMCMC(ReconciliationApproximator& rsa,
		 ReconciliationSampler &rs,
		 DupSpecProbs &nee,
		 Tree & S,
		 const Real& beta, 
		 Tree &G,
		 PRNG &R);

  //
  // Destroy! Destroy! Destroy!
  //
  ~OrthoReconMCMC();	       

  //---------------------------------------------------------------------
  //
  // Implementing the MCMCModel interface
  //
  Probability suggestNewState();
  Probability currentStateProb();
  void        commitNewState();
  void        discardNewState();
  std::string strRepresentation();

  //---------------------------------------------------------------------
  //
  // Access and manipulation
  //
  //  void getRates(Real &bRate, Real dRate) const;
  //  void setRates(Real birthRate, Real deathRate);
  void setFixedRates(bool ratesAreFixed);
  void setFixedGeneTree(bool geneTreeFixed);


  //-------------------------------------------------------------------
  //
  // I/O
  //
  friend std::ostream& 
  operator<<(std::ostream &o, const OrthoReconMCMC& m)
  {
    return o << "OrthoReconMCMC: Implementing MCMC methods gene trees.\n"
	     << std::endl;
  };

  //-------------------------------------------------------------------
  //
  // Internal methods
  //
protected:
  //
  // rateChange returns a number in the interval [0.8, 1.25].
  // Useful when perturbing rates.
  //
  Real rateChange();
  Probability calcRootTimeProb();

  //---------------------------------------------------------------------
  //
  // Attributes
  //
private:
  // These two rates params describe the state of the system.
  // In the nee object, there will be two rate params describing the
  // next state.
  Real                  birthRate; // $\lambda$ in the papers
  Real                  deathRate; // $\mu$ in the papers

  // The integrator approximates Pr(G | S) by sampling reconciliations
  // and node times for G and sum the likelihoods.
  ReconciliationApproximator &integrator;
  
  // M is used to compute gene tree prior probability and
  // is also shared with the ReconSeqApproximator.
  ReconciliationSampler &sampler;

  // nee contains the information to compute slice probabilitites.
  DupSpecProbs         &nee;

  //***************************************
  // We need access to the species tree to be able to change its
  // root time - Since this actually is a prior it could reside in 
  // a nested MCMCModel!
  Tree& S;
  Real currentRootTime;
  Real beta;

  // The main problem input G
  Tree                 &G;
  Tree                  old_G;

  // Keep track of when pre-computed variables become invalidated 
  // because G has changed. We do not always want to force those 
  // recomputations!
  bool                  GDependentObjectsAreInvalid; 


  // The parameter that we are adjusting (a rate or gene tree) is 
  // determined by the paramIdx.
  unsigned              paramIdx;


  // We can choose not use fixed birth/death params:
  bool                  rates_are_fixed;

  // Sometimes we are confident in the gene tree and only want 
  // rates etc to change.
  bool                  gene_tree_fixed;
};

#endif

