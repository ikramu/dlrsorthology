#ifndef SUBSTITUTIONMODEL_HH
#define SUBSTITUTIONMODEL_HH

#include "SequenceData.hh"
#include"ProbabilityModel.hh"

#include <map>
#include <string>
#include <utility>
#include <vector>


namespace beep
{
  // Forward declarations
class Node;
class EdgeWeightHandler;
class Probability;
class TransitionHandler;
class SiteRateHandler;
class Tree;



  //----------------------------------------------------------------------
  //
  // Class SubstitutionModel - subclass of ProbabilityModel
  // 
  //! This class implements the standard (probabilistic) Markov model for 
  //! the substitution process, see eg, Felsenstein 1981, for any type 
  //! of aligned sequence data.
  //! Data are divided into user-defined partitions (domains, codon
  //! positions, etc.), Within each partition sequence positions with 
  //! identical patterns of states over leaves are identified and counted.
  //! Rate variation across sites over discrete classes, e.g., Yang 1993
  //! are modelled
  //!
  //! Author: Bengt Sennblad  copyright: the mcmc-club SBC
  //
  //----------------------------------------------------------------------


  class SubstitutionModel: public ProbabilityModel
  {
  public:
    // Structure to save likelihoods in
    //----------------------------------------------------------------------
    //! Stores tree probabilities for each rate class. 
    //! Each element of RateLike corresponds to a column-specific rate
    //! hypotheses, r (cf. SiteRateHandler).
    //! Each elements of a LA_Vector corresponds to a state, i, in the 
    //! Markov substitution model, and stores the probability of the
    //! Markov process starting with i at the root of a (sub)tree of 
    //! interest and given r, produces the state pattern
    //! corresponding of that position at the leaves of the subtree. 
    typedef std::vector<LA_Vector>                     RateLike;

    //! Holds the probabilities of a column pattern.make
    //! Each element of a PatternLike should correspond to an element
    //! (a column) of a SequenceData::PatternVec
    typedef std::vector<RateLike> PatternLike;

    //! The patternLikes of different partitions of the data.
    //! Each element of a PartitionLike should correspond to an element of
    //! a SequenceData::PartitionVec
    typedef std::vector<PatternLike>                    PartitionLike;
    
    //--------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------------
    //! Sets up the attributes of the class. 
    //! Note that partitionsList should be an existing user-defined 
    //! partition (use 'all' as a default value).
    //-------------------------------------------------------------------
    SubstitutionModel(const SequenceData& Data, 
		      const Tree& T, 
		      SiteRateHandler& siteRates, 
		      const TransitionHandler& Q,
		      EdgeWeightHandler& edgeWeights,
		      const std::vector<std::string>& partitionsList);
  
    //! Copy Constructor
    //-------------------------------------------------------------------
    SubstitutionModel(const SubstitutionModel& sm);
  
    //! Destructor
    //-------------------------------------------------------------------
    virtual ~SubstitutionModel();

    //! Assignment operator
    //-------------------------------------------------------------------
    SubstitutionModel& operator=(const SubstitutionModel& sm);
  
    //-------------------------------------------------------------------
    //
    // Public interface
    //
    //-------------------------------------------------------------------

    //! Access to Tree T. Note that a const reference is returned. The 
    //! logic is that non-const access to T should be explicit in main()
    //-------------------------------------------------------------------
    const Tree& getTree();
  
    // Likelihood calculation - interface to MCMCModels
    //! Calculates the probability of the data given the tree.
    //! \f$ Pr[D|T, w, Q, \alpha, ncat]\f$, where \$ w\$ is the edgeweights 
    //! of T and \alpha is the parameter of SiteRateHandler and ncat is the 
    //! number of rate classes in SiteRateHandler. If D is partitioned, 
    //! different parameter values may be used for different partitions.
    // This is calculated recursively.
    // TODO: We probably need to add a calculateDataProbability(Node& n),
    // for the sake of a common interface.  This is used in
    // cachedSubstitutionModel, but here would just relay to
    // calculateDataProbability().
    //------------------------------------------------------------------
    Probability calculateDataProbability();

    // Pass on update to edgeWeightHandler, etc?
    void update();

    //------------------------------------------------------------------
    //
    // I/O
    // 
    //------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const SubstitutionModel& pm);
    std::string print() const;
  protected:
    //! \todo{ Is this really needed /bens}
    std::string print(const bool& estimateRates) const;

  private:
    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------

    // Helper function for Likelihood calculation
    //-------------------------------------------------------------------
    Probability rootLikelihood(const unsigned& partition);
    PatternLike recursiveLikelihood(const Node& n, const unsigned& partition);
    PatternLike leafLikelihood(const Node& n, const unsigned& partition);

  protected:
    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------

    //-------------------------------------------------------------------
    // Externally handled attributes
    //-------------------------------------------------------------------
    const SequenceData* D;      //!< The sequence data
    const Tree* T;              //!< The tree
    SiteRateHandler* siteRates; //!< Rate variation across sites
    const TransitionHandler* Q; //!< The transition rate matrix

    //-----------------------------------------------------------------
    // Internally handled attributes
    //-----------------------------------------------------------------
    EdgeWeightHandler* edgeWeights; //!< The Markov model 'time'
    PartitionVec partitions; //!<  State patterns and their frequency in D
    
  };
}//end namespace beep
#endif


