#ifndef RECONCILIATIONTIMEMODEL_HH
#define RECONCILIATIONTIMEMODEL_HH

#include <vector>
#include <iostream>

#include "Beep.hh"
#include "BeepVector.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "Node.hh"
#include "Tree.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "ReconciliationSampler.hh"

namespace beep
{

  //----------------------------------------------------------------------
  //
  //! Calculates Probabilities of divergence times from Gene evolution model
  //
  //----------------------------------------------------------------------
  class ReconciliationTimeModel : public ProbabilityModel 
  {
    typedef std::map<unsigned, unsigned> Table;
  public:
    //----------------------------------------------------------------------
    //
    // Construct / Destruct /assignment
    //
    //----------------------------------------------------------------------

    // Constructor
    //----------------------------------------------------------------------  
    ReconciliationTimeModel(Tree &G_in, BirthDeathProbs& bdp_in,
			    const GammaMap& gamma_in,
			    bool include_root_time = true);

    // Construct from existing ReconciliationModel (subclass); ensures that
    // *this has the most current gamma
    ReconciliationTimeModel(ReconciliationModel& rm,
			    bool include_root_time = true);

    // Copy constructor
    //----------------------------------------------------------------------  
    ReconciliationTimeModel(const ReconciliationTimeModel& RTS);

    // Destructor
    //----------------------------------------------------------------------  
    ~ReconciliationTimeModel();

    // Assignment operator
    //----------------------------------------------------------------------  
    ReconciliationTimeModel& operator=(const ReconciliationTimeModel& RTS);


    //----------------------------------------------------------------------
    //
    // Public interface
    //
    //----------------------------------------------------------------------

    // Access for shared parameter
    //----------------------------------------------------------------------
    Tree& getGTree();

    // Interface inherited from ProbabilityModel
    //----------------------------------------------------------------------
    Probability calculateDataProbability();
    void update();

    //----------------------------------------------------------------------
    //
    //I/O
    //
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const ReconciliationTimeModel& RTS);

    //----------------------------------------------------------------------
    //
    //I/O
    //
    //----------------------------------------------------------------------

    // print function for parameters used in operator<<
    //--------------------------------------------------------------------
    std::string print() const;


  protected:
    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------

    //! recursively calculates probability of edgeTimes
    //! within a slice this is modeled using the birth death process relating 
    //! to the slice. Suppose u has children v and w and that T=time of slice,
    //! then:
    //! \f$\mbox{Pr}(t_u|maxT, G_u^{x,y}) = 
    //!    (c-1)\tilde{P}(t_u)(1-\tilde{u}_{t_u})/\tilde{u}_T
    //!    (\mbox{Pr}(t_v|t_u, G_v^{x,y})\mbox{Pr}(t_v|t_u, G_v^{x,y}))\f$
    //! is the return value.
    //! In the arguments, gn is the current node (=u) of G, sn is the current
    //! slice of S, maxT is the upper possible limit of gn's edgetime, i.e,
    //! time between either gn's parent and the bottom of the slice or the
    //! time interval of the slice
    //----------------------------------------------------------------------
    Probability recursiveDataProb(Node* gn, Node* sn, Real maxT);
    Probability recursiveDataProb(Node* gn, Node* sn);
  
    // calculates for each gene node the number of children in the
    // lowest gamma below or at same level
    //----------------------------------------------------------------------
    unsigned
    recursiveUpdateTable(Node& gn);


    // helper print function for table
    //--------------------------------------------------------------------
    std::string
    table4os() const;
  

    //----------------------------------------------------------------------
    // Attributes 
    //----------------------------------------------------------------------
    Tree* G;
    BirthDeathProbs* bdp;
    const GammaMap* gamma;
    UnsignedVector table; // Maps #leaf in G_{u,lowest gamma} for each genenode u
    bool includeRootTime;
  }
  ;  
}//end namespace beep
#endif
