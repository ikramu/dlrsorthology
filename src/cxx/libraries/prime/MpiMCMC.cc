#include "MCMCModel.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "AbstractMCMC.hh"

#include "AnError.hh"
#include "Hacks.hh"
#include "MpiMCMC.hh"
#include "MCMCModel.hh"
#include "MCMCObject.hh"
#include "mpicxx.h"
#include "PRNG.hh"


#include <iostream>
#include <sstream>
#include <unistd.h>
#include <cmath>
#include <cassert>





namespace beep
{
  using namespace std;

  bool  MpiMCMC::swapNeeded(float myTemperature, float otherTemperature, Probability myProb, Probability otherProb, float randomFloat ) {
        // TODO: write some proper if statement here /Erik Sjolund 2010-03-01
            // Maybe floatArray should be used as input too... /Erik Sjolund 2010-03-02
    return true;
  }

  int  MpiMCMC::randomWorkerNodeIndex( int nrWorkerNodes, PRNG & R_ ) {
    // genenerate a random int so that,  1 <= generated value < nrWorkerNodes + 1

    //TODO fix this:
    return  1 + rand() / (RAND_MAX / nrWorkerNodes + 1);
  }

  void  MpiMCMC::fillRandomIndex( pairVec & pV, int nrWorkerNodes, int steps, PRNG & R_ ) {
    int nr = 0; 
    assert( nrWorkerNodes > 1 );
    assert( steps >= 1 );
    while( nr != steps ) { 
      std::pair< int, int > p;
      p.first = randomWorkerNodeIndex( nrWorkerNodes, R_ );
      p.second = randomWorkerNodeIndex( nrWorkerNodes, R_ );
      if ( p.first != p.second ) {   
	pV.push_back(p);
	nr++;
      }
    }
  }

  void  MpiMCMC::fillRandomFloat( std::vector<float> & floatV, int steps, PRNG & R ) {
    int nr = 0; 
    assert( steps >= 1 );
    while( nr != steps ) { 
      float f = R.genrand_real1();
      floatV.push_back(f);
      nr++;
    }
  }

  MpiMCMC::MpiMCMC(MCMCModel& model_, float temperature_, mpi::communicator * world_)
    : AbstractMCMC(), model(model_),
       R(model_.getPRNG())
  {
    assert(world_);
    world = world_;
    temperature = temperature_;
    p = model.initStateProb();    // To set stateProbs
    model.commitNewState();       // To set old_stateProbs, 
  }

  MpiMCMC::~MpiMCMC()
  {
  }

  bool MpiMCMC::iterate( IterationObserver & iterObs, unsigned int * iterations_done ) {
    unsigned int n_iters = iterObs.iterations();


    /* the numbers 102, 103, ... are arbitrary numbers */
    int temperatureType=102;
    int coldSwappedType=105;
    int logProbType=107;
    int signType=108;
    int stdoutStrType=110;
    int stderrStrType=112;

    int root=0;

    int rank = world->rank();
    int nrWorkerNodes = world->size()-1;
    assert( nrWorkerNodes > 1 );
    // int * indexArray = ( int * ) malloc(sizeof(int)*n_iters*2);
    float * floatArray = ( float * ) malloc(sizeof(float)*n_iters);
    //    assert( indexArray );
    assert( floatArray ); 

    int coldNodeStart = 1;
    pairVec pV;
    std::vector<float> floatVector;
    if(rank == 0) {
      fillRandomIndex( pV, nrWorkerNodes, n_iters, R );
      fillRandomFloat( floatVector, n_iters, R );
    }

    mpi::broadcast(*world, pV, 0);
    mpi::broadcast(*world, floatVector, 0);

    if (rank == root) {
      int coldNode = coldNodeStart;
      for ( pairVec::iterator i=pV.begin(); i != pV.end(); ++i) {
	int coldSwapped; 
	mpi::request reqs[1];
	reqs[0] = world->irecv(coldNode, coldSwappedType, coldSwapped);
	mpi::wait_all(reqs, reqs + 1);

	//	printf("root (rank=0): coldnode rank=%i had coldSwapped=%i\n",coldNode,coldSwapped); 
	if ( coldSwapped ) { 
	  assert( coldNode == i->first || coldNode == i->second );
	  if ( coldNode == i->first ) { 
	    coldNode = i->second;
	  }
	  else {
	    coldNode = i->first;
	  }
	};

	std::string stdoutStr;
	std::string stderrStr;

	world->recv(coldNode, stdoutStrType, stdoutStr);
	world->recv(coldNode, stderrStrType, stderrStr);

	std::cout << stdoutStr;
	std::cerr << stderrStr;
      }
    }
    else {
      bool gotTheCold = ( temperature == 1.0 );
      assert( nrWorkerNodes > 1 );
      for ( pairVec::iterator i=pV.begin(); i != pV.end(); ++i) {

	bool state_changed = false;
	int gotTheColdPrevious = gotTheCold;
	int coldSwapped = ( int ) false;
	MCMCObject proposal = model.suggestNewState();
	Probability alpha = 1.0;
	if(p > 0)
	  {
        // TODO: the temperature should influence alpha
	    alpha = proposal.stateProb * proposal.propRatio / p;
	  }
	    

	if(alpha >= 1.0 || Probability(R.genrand_real1()) <= alpha)
	  {
	    model.commitNewState();
	    p = proposal.stateProb;
	    state_changed = true;
	  }
	else
	  {
	    model.discardNewState();
	    // Note that since we are discarding we do NOT update output
	    // but leave it as in last iteration
	  }

	if ( rank == i->first or rank == i->second ) {
	  int other = i->first;
	  if ( other == rank ) { 
	    other = i->second;
	  }  

          Probability myProb = model.currentStateProb();
	  double myLogProb = myProb.getLogProb();
          int mySign = myProb.getSign();


	mpi::request reqs[6];
	reqs[0] = world->isend(other, temperatureType, temperature);
        float otherTemperature;
	reqs[1] = world->irecv(other, temperatureType, otherTemperature);
	reqs[2] = world->isend(other, logProbType, myLogProb);
        double otherLogProb;
	reqs[3] = world->irecv(other, logProbType, otherLogProb);
	reqs[4] = world->isend(other, signType, mySign);
        int otherSign;
	reqs[5] = world->irecv(other, signType, otherSign);

	mpi::wait_all(reqs, reqs + 6);

        bool otherGotTheCold = ( otherTemperature == 1.0 ); 

	  //	  printf("rank %i: node %i said it had temperature %f\n",rank,other , otherTemperature );
      
        Probability otherProb;
	otherProb.setLogProb(otherLogProb, otherSign);

	if ( swapNeeded( temperature, otherTemperature, myProb, otherProb, floatArray[ i - pV.begin() ] ) ) {
	    temperature=otherTemperature;
	    gotTheCold = otherGotTheCold;
	    coldSwapped = ( int ) true;
	  }
	}
	if ( gotTheColdPrevious ) {     
	  mpi::request reqs[1];
	  reqs[0] = world->isend(root, coldSwappedType, coldSwapped);
	  mpi::wait_all(reqs, reqs + 1);
	}
	if ( gotTheCold  ) {     
	  bool state_changed_or_swapped = ( gotTheCold != gotTheColdPrevious ) || state_changed;
          std::string stdoutStr;
	  std::string stderrStr;

	  int ret = iterObs.afterEachStep( model , (i - pV.begin()) + 1, state_changed_or_swapped, stdoutStr, stderrStr );
	  if ( ! ret ) { 
	    std::cerr << " Premature ending is not supported in yet... It is on the TODO list" << std::endl;
            abort();
	    //	    exit(EXIT_FAILURE);
	    return false; 
          };

	world->send(root, stdoutStrType, stdoutStr);
	world->send(root, stderrStrType, stderrStr);

	}
      };
    }
    free(floatArray);
    return true;
  }

}//end namespace beep
