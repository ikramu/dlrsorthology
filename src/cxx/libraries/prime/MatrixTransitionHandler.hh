#ifndef MATRIXTRANSITIONHANDLER_HH
#define MATRIXTRANSITIONHANDLER_HH

#include <iostream>
#include <string>

#include "Beep.hh"
#include "LA_DiagonalMatrix.hh"
#include "LA_Matrix.hh"
#include "LA_Vector.hh"
#include "MatrixCache.hh"
#include "TransitionHandler.hh"




namespace beep
{
  class MatrixTransitionHandler : public TransitionHandler
  {
    friend class TransitionHandler;
  public:
    //---------------------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //---------------------------------------------------------------------
    //! A convenience function calling 'named constructors'.
    //! Only [model]s for which a named constructor called [model]() exists
    //! are accepted, e.g., create("JTT");
    // TODO: Design an interface class containing a handle to
    // TransitionHandler and place create there!
    static MatrixTransitionHandler create(std::string model, std::map<char, double> *bf = new std::map<char, double>());
    //! Copy constructor.
    MatrixTransitionHandler(const MatrixTransitionHandler& Q);
    //! Destructor
    ~MatrixTransitionHandler();
    //! Assignement operator
    MatrixTransitionHandler& operator=(const MatrixTransitionHandler& Q);

  protected:
    //! protected constructor used by named constructors
    MatrixTransitionHandler(const std::string& name, 
			    const SequenceType& type,
			    const Real R_vec[], const Real Pi_vec[]);

    //---------------------------------------------------------------------
    //
    // Interface
    //
    //---------------------------------------------------------------------
  public:    
    //! Access to Pi
    //---------------------------------------------------------------------
    LA_DiagonalMatrix getPi();

    //! Updates the eigensystem to new values of R and Pi. 
    //---------------------------------------------------------------------
    void update();

    // Calculates exp(A*t) using the eigensystem attributes
    //---------------------------------------------------------------------
    void resetP(const Real& MarkovTime) const;

    //! (Matrix) multiplication \f$<result>=P<operand>\f$
    //------------------------------------------------------------------
    virtual void mult(const LA_Vector& operand, 
		      LA_Vector& result) const;
    
    std::map<char, double> getBaseFrequencies();
    void setBaseFrequencies(std::map<char, double> bf);

    //! (Matrix) multiplication 
    //! \f$\langle result\rangle=P<operand>_{<column>}\f$.
    //! returns \f$ P_{<column>} \f$.
    //------------------------------------------------------------------
    virtual bool col_mult(LA_Vector& result, const unsigned& column) const;

    //! Element-wise multiplication\f$<result>=\pi <operand>\f$
    //------------------------------------------------------------------
    virtual void multWithPi(const LA_Vector& operand, 
			    LA_Vector& result) const;
    //---------------------------------------------------------------------
    // I/O
    //---------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& os, 
				    const MatrixTransitionHandler& Q);
    std::string print() const;
    //TODO: Clean these print() functions
    std::string print(const bool& estimateR, const bool& estimatePi) const;
    
    // I/O helper function prints R
    //---------------------------------------------------------------------
    std::string R4os() const;

    //---------------------------------------------------------------------
    //
    // Implementation
    //
    //---------------------------------------------------------------------
    // TODO: the models jc69, m.fl. should be moved to another subclass
    // of TransitionHandler that implements analytical solution of the
    // member functions
    //! Named constructor for DNA model type described by Jukes, Cantor 1969.
    static MatrixTransitionHandler JC69();
    //! Named constructor for DNA model type described by Falsenstein 1981.
    static MatrixTransitionHandler F81(std::map<char, double> bf);
    //! Named constructor for amino acid model type corresponding to that 
    //! described by Jukes, Cantor 1969 for DNA.
    static MatrixTransitionHandler UniformAA();
    //! Named constructor for model type described by Jones, Taylor and 
    //! Thornton.
    static MatrixTransitionHandler JTT();
    //! Named constructor for codon model type corresponding to that 
    //! described by Jukes, Cantor 1969 for DNA.
    static MatrixTransitionHandler UniformCodon();
    //! Named constructor for codon model type corresponding to that 
    //! described by Arvestad(unpublished)
    static MatrixTransitionHandler ArveCodon();
    //! Named constructor for user-defined model type
    static MatrixTransitionHandler userDefined(std::string seqType, 
					       std::vector<Real> pi, 
					       std::vector<Real> r);
  private:
    //---------------------------------------------------------------------
    //
    // Attributes
    //
    //---------------------------------------------------------------------
    LA_Vector R;          //!< The 'intrinsic rate matrix' of the model.
    LA_DiagonalMatrix Pi; //!< The stationary frequencies of the model.
                          //!< formulated as a diagonal matrix.
    // Eigensystem
    LA_DiagonalMatrix E;  //!< Eigen values of the transition rate matrix.
    LA_Matrix V;          //!< Eigen vectors of the transition rate matrix.
    LA_Matrix iV;         //!< inverse of V.

    mutable LA_Matrix P;  //!< transition probability matrix (needs to be set 
                          //!< for particular (Markov) times.

    // Some temporary objects instead of creating them each time we do resetP
    mutable LA_Matrix tmp_matrix;            //!< Temporary storage matrix
    mutable LA_DiagonalMatrix tmp_diagonal;  //!< Temporary storage vector

    // Avoid recomputing frequent P-matrices by using a cache.
    //---------------------------------------------------------------------
    mutable MatrixCache<LA_Matrix> PCache; //!< A cache for saving instances
                                           //!< of P to avoid recalculations
    std::map<char, double> baseFrequencies;
    
  };

}

#endif
