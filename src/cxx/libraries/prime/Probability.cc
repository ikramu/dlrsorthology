#include "Probability.hh"

#include <cmath>
#include <limits>
#include <cassert>

#include "AnError.hh"

//#ifndef PROBABILITY_CHEAT
//----------------------------------------------------------------------
//Author: Bengt Sennblad
//@the MCMC-club, Stockholm Bioinformatics Center, 2002
//----------------------------------------------------------------------


//----------------------------------------------------------------------
//
// Member functions for Class Probability:
// This class holds the logProbabilities of a number, and defines 
// overloaded operators for two Probability variables:
//         +=, -=, *=, /=, +,-,*,/, <, >, <=, >=
// and the following functions for a Probability and an int variable
//         power, factorial, binomial
//
// Note that this overrides the std::pow/log, so be explicit if you want
// to use that.
//
// All functions return a Probability and none alters the original 
// parameter values
//
// This is somewhat old and could probably be better implemented, but 
// does what it should anyway.
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;

  Probability::Probability()        
  { 
    p = 0.0;        //Dummy value  
    sign = 0;       //zero
  };

  Probability::Probability(const Real& d)      
  { 
    assert(isnan(d) == false);
    assert(isinf(d) == false);

    if(d > 0.0)
      {
	p = std::log(d);
	sign = 1;    //positive
      }
    else if(d == 0.0)
      {
	p = 0.0;     //Dummy value
	sign = 0;
      }
    else 
      {
	p = std::log(-d);
	sign = -1; //negative
      }
  };

  //!\todo{If I implment this Probability constructed from int becomes ambiguous - fix thjis!}
  //   Probability::Probability(const unsigned& u)
  //   {
  //     Real d = static_cast<Real>(u);
  //     if(d > 0.0)
  //       {
  // 	p = std::log(d);
  // 	sign = 1;    //positive
  //       }
  //     else if(d == 0.0)
  //       {
  // 	p = 0.0;     //Dummy value
  // 	sign = 0;
  //       }
  //     else
  //       {
  // 	p = std::log(-d);
  // 	sign = -1; //negative
  //       }

  //   }        

  Probability::Probability(const Probability& P)        
  { 
    assert(isnan(P.p) == false);
    assert(isinf(P.p) == false);
    p = P.p;       
    sign = P.sign; 
  };


  // Named constructor for low level manipulations.
  Probability
  Probability::setLogProb(double logProb, int sign)
  {
    assert(isnan(logProb) == false);
    assert(isinf(logProb) == false);
    assert(sign <=1 && sign >=-1);
    Probability p;
    p.p = logProb;
    p.sign = sign;
    return p;
  }

  //---------------------------------------------------------------------
  //
  //Functions to access the log probability directly
  //
  //---------------------------------------------------------------------

  double
  Probability::getLogProb() const
  {
    return p;
  }

  int
  Probability::getSign() const
  {
    return sign;
  }



  //---------------------------------------------------------------------
  //
  //Household function - getting the exponentiated value
  //
  //---------------------------------------------------------------------
  Real//  double 
  Probability::val() const
  { 
    switch(sign)
      {
      case 1:
	{
	  return std::exp(p); 
	  break;
	}
      case 0:
	{
	  return 0.0;
	  break;
	}
      case -1:
	{
	  return - std::exp(p);
	  break;
	}
      default:
	{
	  throw AnError("Probability::sign has illegal value!", 1);
	  return 0;
	}
      }
  }

  // Probability::operator Real() const
  // {
  //   return val();
  // }


  //---------------------------------------------------------------------
  //
  //Assignment operators
  //
  //---------------------------------------------------------------------
  Probability& 
  Probability::operator=(const Probability& q)
  {
    if (this != &q)
      {
	assert(isnan(q.p) == false);
	assert(isinf(q.p) == false);
	p = q.p;
	sign = q.sign;
      }
    return *this;
  }

  Probability& 
  Probability::operator+=(const Probability& q)
  {
    assert(isnan(p) == false);
    assert(isinf(p) == false);
    assert(isnan(q.p) == false);
    assert(isinf(q.p) == false);
    switch(sign * q.sign )
      {
      case 1:
	{
	  add(q);
	  sign = sign;
	  break;
	}
      case 0:
	{
	  if(q.sign != 0)
	    {
	      p = q.p;
	      sign = q.sign;
	    }	
	  break;
	}
      case -1:
	{
	  subtract(q);
	  break;
	}
      }
    assert(isnan(p) == false);
    assert(isinf(p) == false);

    return *this;
  }

  Probability& 
  Probability::operator-=(const Probability& q)
  {
    switch(sign * q.sign )
      {
      case 1:
	{
	  subtract(q);
	  break;
	}
      case 0:
	{
	  if(q.sign != 0)
	    {
	      p = q.p;
	      sign = -q.sign;
	    }
	  break;
	}
      case -1:
	{
	  if(sign == 1)
	    {
	      add(q);
	    }
	  else
	    {
	      add(q);
	      sign = -1;
	    }
	  break;
	} 
      }
    assert(isnan(p) == false);
    assert(isinf(p) == false);
    return *this;
  }

  Probability& 
  Probability::operator*=(const Probability& q)
  {
    sign = sign * q.sign;
    if(sign != 0)
      {
	p = p + q.p;
      }  
    assert(isnan(p) == false);
    assert(isinf(p) == false);
    return *this;
  }

  Probability& 
  Probability::operator/=(const Probability& q)
  {
    if (q.sign != 0)
      {
	sign = sign * q.sign;
	if(sign != 0)
	  {
	    p = p - q.p;
	  }
      }
    else
      {
	throw AnError("Probability: Division with zero attempted!", 1);
      }
    assert(isnan(p) == false);
    assert(isinf(p) == false);
    return *this;
  }


  //---------------------------------------------------------------------
  //
  //Logical operators
  //
  //---------------------------------------------------------------------

  //Logical operators for two Probability variables
  //---------------------------------------------------------------------
  bool 
  Probability::operator>(const Probability& q)  const
  {
    if(sign == q.sign)
      {
	if(sign == 1)
	  {
	    return p > q.p;
	  }
	else if(sign == 0)
	  {
	    return false;
	  }
	else
	  {
	    return p < q.p;
	  }
      }
    else 
      {
	return sign > q.sign;
      }

  }

  bool 
  Probability::operator<(const Probability& q) const 
  {
    if(sign == q.sign)
      {
	if(sign == 1)
	  {
	    return p < q.p;
	  }
	else if(sign == 0)
	  {
	    return false;
	  }
	else
	  {
	    return p > q.p;
	  }
      }
    else 
      {
	return sign < q.sign;
      }
  }

  bool 
  Probability::operator>=(const Probability& q) const 
  {
    if(sign == q.sign)
      {
	if(sign == 1)
	  {
	    return p >= q.p;
	  }
	else if(sign == 0)
	  {
	    return true;
	  }
	else
	  {
	    return p <= q.p;
	  }
      }
    else
      {
	return sign>=q.sign;
      }
  }

  bool 
  Probability::operator<=(const Probability& q) const 
  {
    if(sign == q.sign)
      {
	if(sign == 1)
	  {
	    return p <=q.p;
	  }
	else if(sign == 0)
	  {
	    return true;
	  }
	else
	  {
	    return p >=q.p;
	  }
      }
    else
      {
	return sign <= q.sign;
      }
  }

  bool 
  Probability::operator==(const Probability& q) const 
  {
    if(sign == q.sign)
      {
	if(sign == 0)
	  {
	    return true;
	  }
	else
	  {
	    return p == q.p;
	  }
      }
    else
      {
	return false;
      }
  }


  bool 
  Probability::operator!=(const Probability& q) const 
  {
    if(sign == q.sign)
      {
	if(sign == 0)
	  {
	    return false;
	  }
	else
	  {
	    return p != q.p;
	  }
      }
    else
      {
	return true;
      }
  }


  //---------------------------------------------------------------------
  //
  //Log Arithemtics and operators for two Probability variables
  //
  //---------------------------------------------------------------------
  Probability
  operator+(const Probability& p, const Probability& q)
  {
    return Probability(p) += q;
  }

  Probability
  operator-(const Probability& p, const Probability& q)
  {
    return Probability(p) -= q;
  }

  Probability 
  operator*(const Probability& p, const Probability& q)
  {
    return Probability(p) *= q;
  }

  Probability 
  operator/(const Probability& p, const Probability& q)
  { 
    return Probability(p) /= q;
  }


  //---------------------------------------------------------------------
  //
  //Power and exp of a Probability and unary negation operator
  //
  //---------------------------------------------------------------------
  // Probability 
  // pow(const Probability& p, const unsigned& d)       
  // { 
  //   if(p.sign == 1)
  //     {
  //       Probability q(p);
  //       q.p = d * p.p;
  //       return q;
  //     }
  //   else if(p.sign == 0)
  //     {
  //       return p;
  //     }
  //   else
  //     {				// What? Can (-0.4)^2.1 really be a complex number?
  // 				// And what about (-0.4)^2 ?    /arve
  //       throw AnError("Probability.pow(int d) with a negative Probability "
  // 		    "may imply an imaginary number; this is not handled by "
  // 		    "Probability (...yet)");
  //     }
  // };
  // Probability 
  // pow(const Probability& p, const int& d)       
  // { 
  //   if(p.sign == 1)
  //     {
  //       Probability q(p);
  //       q.p = d * p.p;
  //       return q;
  //     }
  //   else if(p.sign == 0)
  //     {
  //       return p;
  //     }
  //   else
  //     {				// What? Can (-0.4)^2.1 really be a complex number?
  // 				// And what about (-0.4)^2 ?    /arve
  //       throw AnError("Probability.pow(int d) with a negative Probability "
  // 		    "may imply an imaginary number; this is not handled by "
  // 		    "Probability (...yet)");
  //     }
  // };

  Probability 
  pow(const Probability& p, const double& d)       
  { 
    assert(isnan(d) == false);
    assert(isnan(p.p) == false);
    assert(isinf(d) == false);
    assert(isinf(p.p) == false);
    if(p.sign == 1)
      {
	Probability q(p);
	q.p = d * p.p;
	return q;
      }
    else if(p.sign == 0)
      {
	if(d == 0)
	  return 1.0;
	else
	  return p;
      }
    else
      {
	throw AnError("Probability.pow(double d) with a negative Probability "
		      "may imply an imaginary number; this is not handled by "
		      "Probability (...yet)", 1);
      }
  };

  Probability 
  exp(const Probability& p)         
  { 
    Probability q(1.0);
    q.p = p.val();
    assert(isnan(q.p) == false);
    assert(isinf(q.p) == false);
    return q;
  };

  Probability 
  log(const Probability& p)         
  { 
    if(p.sign <= 0)
      {
	throw AnError("Can't log a negative number or zero\n", 1);
      }
    Probability q(p.p);
    assert(isnan(q.p) == false);
    assert(isinf(q.p) == false);
    return q;
  };

  Probability 
  Probability::operator-() const
  {
    Probability q(*this);
    q.sign = - sign;
    assert(isnan(q.p) == false);
    assert(isinf(q.p) == false);
    return q;
  }

  //---------------------------------------------------------------------
  //
  //Factorial and binomial ("u1 choose u2") of sunsigneds  
  //using logarithms - returning Probabilities
  //
  //---------------------------------------------------------------------
  Probability
  probFact(unsigned u)         
  {
    Probability q;
    while(u > 0)
      {	
	q.p = q.p + std::log((double)u);
	u--;
      }
    q.sign = 1;
    assert(isnan(q.p) == false);
    assert(isinf(q.p) == false);
    return q;  
  };

  Probability
  probBinom(unsigned u1, unsigned u2)
  {
    if(u1 >= u2)
      {
	Probability q = (probFact(u1) / (probFact(u2) * probFact(u1 - u2)));
	assert(isnan(q.p) == false);
	assert(isinf(q.p) == false);
	return q;
      }
    else
      {
	std::cerr<< "******************** \n Incompatibel terms in binomial \n ******************+n";
	throw AnError("first term in binomial must not be less than second", 1);
      }
  }


  //--- I/O --------------------------------------------------------------
  //
  // Write probabilities to a stream
  //
  //----------------------------------------------------------------------
  std::ostream&
  operator<<(std::ostream &o, const Probability& p)
  {
    switch (p.sign)
      {
      case 1:
	{
	  return o << p.p;
	  break;
	}
      case 0:
	{
	  return o << -numeric_limits<double>::max();
	  break;
	}
      case -1:
	{
	  throw AnError("Probability.operator<<: request to output a log of "
			"negative probability value. I do't know how to "
			"handle this in an unambiguous way,yet!\n", 1);
	  return o << -99999;
	  break;
	}
      default:
	{
	  throw AnError("Probability.operator<<: not a valid value of sign", 1);
	  return o << -99999;
	}
      }
  }


//   //private:
//   //---------------------------------------------------------------------
//   // mpi serialization functions
//   //---------------------------------------------------------------------
//   template<class Archive> 
//   void 
//   Probability::serialize(Archive& ar, const unsigned int version) 
//   {
//     ar & p;
//     ar & sign;
//   }


  //----------------------------------------------------------------------
  //
  //Helper arithmetics finctions
  //
  //----------------------------------------------------------------------

  //Helper for addition of two Probabilities
  //---------------------------------------------------------------------
  void
  Probability::add(const Probability &q)
  {
    assert(isnan(p) == false);
    assert(isnan(q.p) == false);
    assert(isinf(p) == false);
    assert(isinf(q.p) == false);
    if (p > q.p) 
      { 
	// TODO: joelgs: Is it worthwhile changing this to logp1l
	// (which it should be)? logp1l was 35-40% slower than logp1
	// on my computer in a test.
	p = p + log1p(std::exp(q.p - p));
	assert(isnan(p) == false);
	assert(isinf(p) == false);
      } 
    else 
      {
	if(isnan(p - q.p))
	  cerr << "p = " << p << "  q.p = " << q.p << endl;;
	assert(isnan(p - q.p) == false);
	assert(isnan(std::exp(p - q.p)) == false);
	p = q.p + log1p(std::exp(p - q.p));  // TODO: See log1pl-comment above.
	assert(isnan(p) == false);
	assert(isinf(p) == false);
      }
    return;
  };

  //Helper for subtraction of two Probabilities
  //---------------------------------------------------------------------
  void
  Probability::subtract(const Probability& q)
  {
    if (p > q.p) 
      { 
	p = p + log1pl(- std::exp(q.p-p));  // joelgs: Now log1pl instead of log1p.
	//      p = p + log1p(- std::exp(q.p-p));
      } 
    else if(p == q.p)
      {
	sign = 0;
      }      
    else
      {
	p = q.p + log1pl(- std::exp(p-q.p)); // joelgs: Now log1pl instead of log1p.
	//      p = q.p + log1p(- std::exp(p-q.p));
	sign = -1 * sign;
      }
    return;
  };
}//end namespace beep
//#endif
