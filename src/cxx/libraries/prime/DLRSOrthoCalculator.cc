/* 
 * File:   DLRSOrthoCalculator.cpp
 * Author: Ikram Ullah
 * 
 * Created on October 18, 2012, 4:23 PM
 */

#include "DLRSOrthoCalculator.hh"

DLRSOrthoCalculator::DLRSOrthoCalculator() {
    gsMap = new StrStrMap();
}

DLRSOrthoCalculator::DLRSOrthoCalculator(string gnt, string spt, double mean,
        double var, double birth, double death, bool read_tree_from_string) {
    /*
     * set gene and specie tree to NULL so that, unless they
     * are explicitly, orthology calculator returns proper 
     * warning message to calling function
     */
    gsMap = new StrStrMap();
    //tio = TreeIO::fromFile(spt);
    //species_tree = tio.readHostTree();
    read_species_tree(spt.c_str());
    read_gene_tree(gnt, read_tree_from_string);
    populateGsMap(spt);
    gamma = new GammaDensity(mean, var);
    bd_probs = new EdgeDiscBDProbs(DS, birth, death);
    gsr = new EdgeDiscGSR(&gene_tree, DS, gsMap, gamma, bd_probs);
    //create_lookup_tables(ID, inv_ID, gs_map);
}

map<string, double> DLRSOrthoCalculator::getOrthoEstimates() {

    map<string, double> ortho_probs;

    create_lookup_tables();

    map<int, Node*> gs_node_map;
    for (unsigned int i = 0; i < ID.size(); i++) {
        gs_node_map[i] = species_tree.findLeaf(gsMap->find(ID[i]));
    }

    map<string, Node*> gene_node_map;
    for (unsigned int i = 0; i < ID.size(); i++) {
        Node* gnode = gene_tree.findLeaf(gsMap->getNthItem(i));
        gene_node_map[gnode->getName()] = gnode;
    }

    //vector<Node*> all_leaves = gene_tree.getAllNodes();
    /*
    map<string, Node*> lca_mapping;
    vector<Node*> selected_nodes;
    for (vector<string>::iterator i = true_lcas.begin(); i != true_lcas.end(); i++) {
        Tokenizer tok(" ");
        tok.setString(*i);

        SetOfNodes gene_leaves;
                
        while (tok.hasMoreTokens()) {
            gene_leaves.insert(gene_node_map.at(tok.getNextToken()));
        }
        Node *lca_g = find_lca(gene_leaves, gene_tree);
        selected_nodes.push_back(lca_g);
        lca_mapping.insert(pair<string, Node*> (*i, lca_g));
    }    
     */

    //cout << gene_tree << endl;
    //for(map<string, Node*>::iterator iter = lca_mapping.begin(); iter != lca_mapping.end(); iter++){
    //for (vector<Node*>::iterator iter = all_leaves.begin(); iter != all_leaves.end(); iter++) {
    for(unsigned int x = 0; x < gene_tree.getNumberOfNodes(); x++){
        //Node* node = selected_nodes.at(i);
        //Node *node = *iter;
        Node *node = gene_tree.getNode(x);
        //cout << node->getNumber() << endl;
        //cout << node->getNumberOfLeaves() << endl;
        //cout << (*iter)->getNumber() << endl;

        if (node->isLeaf() || (node == NULL)) {
            continue;
        } else {
            vector<Node*> lnodes = getDescendentNodes(node->getLeftChild());
            vector<Node*> rnodes = getDescendentNodes(node->getRightChild());
            vector<unsigned> lnodes_id = getIdsFromNodes(lnodes);
            vector<unsigned> rnodes_id = getIdsFromNodes(rnodes);

            SetOfNodes species_leaves;
            //Left part
            for (unsigned i = 0; i < lnodes_id.size(); i++) {
                species_leaves.insert(gs_node_map[lnodes_id[i]]);
            }
            //Right part
            for (unsigned i = 0; i < rnodes_id.size(); i++) {
                species_leaves.insert(gs_node_map[rnodes_id[i]]);
            }

            Node *lca_s = find_lca(species_leaves, species_tree);

            //Create "discretized" speciation node
            EdgeDiscretizer::Point point(lca_s, 0);
            //Node *lca_g = node;

            Probability speciation_prob = gsr->getPlacementProbability(node, &point);

            if (speciation_prob.val() > 1.0) {
                cout << "Virtual vertex number is " << node->getLeaves() << endl;
                cout << "Compute_Ortho: WARNING, Speciation prob > 1.0." << endl
                        << "speciation_prob: " << speciation_prob.val() << endl;
            }

            vector<string> gene_pairs = get_gene_pairs_from_lca(node);
            for(unsigned int i = 0; i < gene_pairs.size(); i++)
                ortho_probs.insert(pair<string, double>(gene_pairs[i], speciation_prob.val()));
            //ortho_probs.insert(pair<string, double>(node->ge, speciation_prob.val()));
            //os << "\t" << speciation_prob.val() << endl;

            //cout << os.str();
        }
    }

    return ortho_probs;
}

DLRSOrthoCalculator::DLRSOrthoCalculator(const DLRSOrthoCalculator& orig) {
}

DLRSOrthoCalculator::~DLRSOrthoCalculator() {
}

/**
 * read_species_tree
 * Read a species tree from file.
 *
 * @param args_info Program arguments
 */
void DLRSOrthoCalculator::read_species_tree(string spfile) {
    tio = TreeIO::fromFile(spfile);
    species_tree = tio.readHostTree();
    rescale_specie_tree();

    // create descretized tree
    create_disc_tree();
}

vector<string> DLRSOrthoCalculator::get_gene_pairs_from_lca(Node *node) {
    vector<string> gene_pairs;
    //string cpair;
    vector<Node*> lNodes = gene_tree.getDescendentNodes(node->getLeftChild());
    vector<Node*> rNodes = gene_tree.getDescendentNodes(node->getRightChild());

    for (unsigned int i = 0; i < lNodes.size(); i++) {
        for (unsigned int j = 0; j < rNodes.size(); j++) {
            if (not_same_specie(lNodes[i]->getName(), rNodes[j]->getName())) {
                vector<string> ln;
                ln.push_back(lNodes[i]->getName());
                ln.push_back(rNodes[j]->getName());
                sort(ln.begin(), ln.end());
                string cpair = ln[0] + (string) " " + ln[1];
                
                gene_pairs.push_back(cpair);
            }
        }
    }
    
    return gene_pairs;
}

bool DLRSOrthoCalculator::not_same_specie(string left_gene, string right_gene) {
    if (get_specie_from_gene_name(left_gene) == get_specie_from_gene_name(right_gene))
        return false;
    else
        return true;
}

string DLRSOrthoCalculator::get_specie_from_gene_name(string gene) {
    Tokenizer tk("_");
    tk.setString(gene);

    // example would be like gene_mus_1 or gene_homo_2
    // first is the word "gene" so skip
    tk.getNextToken();

    // return the next one which is specie name
    return tk.getNextToken();
}

/**
 * read_gene_tree
 * Read a species tree from file.
 *
 * @param args_info Program arguments
 */
void DLRSOrthoCalculator::read_gene_tree(string g_tree, bool from_string) {
    if (from_string)
        tio = TreeIO::fromString(g_tree);
    else
        tio = TreeIO::fromFile(g_tree);
    gene_tree = tio.readBeepTree(NULL, gsMap);
}

/**
 * rescale_specie_tree
 * Rescales a species tree to [0,1].
 *
 * @param S The specie tree
 */
void DLRSOrthoCalculator::rescale_specie_tree() {
    Real sc = species_tree.rootToLeafTime();
    beep::RealVector* tms = new beep::RealVector(species_tree.getTimes());
    for (beep::RealVector::iterator it = tms->begin(); it != tms->end(); ++it) {
        (*it) /= sc;
    }
    species_tree.setTopTime(species_tree.getTopTime() / sc);
    species_tree.setTimes(*tms, true);
    //cout << "Specie tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
}

void DLRSOrthoCalculator::read_leaves_from_file(string path, vector<string> &mprSpecNodes) {
    ifstream infile;
    infile.open(path.c_str());
    while (!infile.eof()) {
        string line;
        getline(infile, line);
        size_t pos = line.find_last_of("\t");
        if (pos != std::string::npos)
            line.assign(line.begin() + 1, line.begin() + pos - 1);
        cout << line << endl;
        mprSpecNodes.push_back(line);
    }
}

/**
 * 
 * @param gtree Gene Tree
 * @param map Map object to be populated
 */
void DLRSOrthoCalculator::populateGsMap(string spfile) {
    // Tree gtree, StrStrMap &map
    //string gtfullname(spfile);
    string gtname = "";

    size_t pos = spfile.find_last_of("/");
    if (pos != std::string::npos)
        gtname.assign(spfile.begin() + pos + 1, spfile.end());
    else
        gtname = spfile;
    std::vector<Node*> nodes = gene_tree.getAllNodes();

    gsMap->clearMap();
    for (unsigned int i = 0; i < gene_tree.getNumberOfNodes(); i++) {
        if (nodes[i]->isLeaf() && nodes[i] != NULL) {
            std::vector<std::string> elems = split_str(nodes[i]->getName(), '_');
            gsMap->insert(nodes[i]->getName(), elems[1]);
        }
    }
}

/**
 * create_disc_tree
 * Creates a discretized species tree from a species tree.
 *
 * @param species_tree The species tree
 * @param DS At return: Pointer to the discretized species tree
 * @param args_info Program arguments
 *
 */
void DLRSOrthoCalculator::create_disc_tree() {
    float timestep; //Timestep for discretized tree
    int min_intervals; //Minimum number of intervals in DS
    EdgeDiscretizer *disc; //Used for creating discretized trees

    //Set timestep and min_interval parameters
    timestep = 0;
    min_intervals = 10;

    //Create discretized tree
    if (timestep == 0) {
        disc = new EquiSplitEdgeDiscretizer(min_intervals);
    } else {
        disc = new StepSizeEdgeDiscretizer(timestep, min_intervals);
    }
    DS = new EdgeDiscTree(species_tree, disc);

    delete disc;
}

void DLRSOrthoCalculator::computeAndWriteOrthologies(string geneFileName) {
    Tree G = gsr->getTree();
    StrStrMap gs_map = gsr->getGSMap();
    std::vector<Node*> nodeList = G.getAllNodes();
    char outfile[800] = {0};
    //cout << "gene file name would be " << geneFileName.data() << endl;
    strcat(outfile, geneFileName.data());
    strcat(outfile, ".dlrscomputed");



    create_lookup_tables();

    cout << "Computing orthology of input file..." << endl;
    calc_speciation_single(outfile);
    cout << "Done..." << endl;
    cout << "Computed orthologies are written to " << outfile << endl;
}

/**
 * find_lca
 * Finds and returns the lca node of a set of nodes.
 *
 * @param nodes A set of nodes
 * @param T Tree in which the nodes are found.
 *
 */
Node* DLRSOrthoCalculator::find_lca(SetOfNodes &nodes, Tree &T) {
    if (nodes.size() > 0) {
        Node *lca = nodes[0];
        for (unsigned int i = 1; i < nodes.size(); i++) {
            lca = T.mostRecentCommonAncestor(lca, nodes[i]);
        }
        return lca;
    }
    return NULL;
}

std::vector<Node*> DLRSOrthoCalculator::getDescendentNodes(Node* n) {
    return getDescendentNodeRecursive(n);
}

std::vector<Node*> DLRSOrthoCalculator::getDescendentNodeRecursive(Node* n) {
    std::vector<Node*> nodes; // = new std::vector<Node*>();
    if (n->isLeaf()) {
        nodes.push_back(n);
        return nodes;
    } else {
        std::vector<Node*> lnodes = getDescendentNodeRecursive(n->getLeftChild());
        std::vector<Node*> rnodes = getDescendentNodeRecursive(n->getRightChild());
        lnodes.insert(lnodes.end(), rnodes.begin(), rnodes.end());
        return lnodes;
    }
}

void DLRSOrthoCalculator::calc_speciation_single(char *filename) {
    Tree G = gsr->getTree();
    Tree S = gsr->getDiscretizedHostTree()->getTree();
    StrStrMap gsMap = gsr->getGSMap();
    ofstream outfile;
    outfile.open(filename);

    cout << G << endl;

    map<int, Node*> gs_node_map;
    for (unsigned int i = 0; i < ID.size(); i++) {
        gs_node_map[i] = S.findLeaf(gsMap.find(ID[i]));
    }

    map<string, double> vvProbs;
    vector<Node*> all_nodes = G.getAllNodes();

    for (unsigned i = 0; i < all_nodes.size(); i++) {
        Node* node = all_nodes.at(i);
        if (node == NULL || node->isLeaf()) {
            continue;
        } else {
            vector<Node*> lnodes = getDescendentNodes(node->getLeftChild());
            vector<Node*> rnodes = getDescendentNodes(node->getRightChild());
            vector<unsigned> lnodes_id = getIdsFromNodes(lnodes);
            vector<unsigned> rnodes_id = getIdsFromNodes(rnodes);

            SetOfNodes species_leaves;
            //Left part
            for (unsigned i = 0; i < lnodes_id.size(); i++) {
                species_leaves.insert(gs_node_map[lnodes_id[i]]);
            }
            //Right part
            for (unsigned i = 0; i < rnodes_id.size(); i++) {
                species_leaves.insert(gs_node_map[rnodes_id[i]]);
            }

            Node *lca_s = find_lca(species_leaves, S);

            //Create "discretized" speciation node
            EdgeDiscretizer::Point point(lca_s, 0);
            Node *lca_g = node;

            Probability speciation_prob = gsr->getPlacementProbability(lca_g, &point);

            if (speciation_prob.val() > 1.0) {
                cout << "Virtual vertex number is " << lca_g->getLeaves() << endl;
                cout << "Compute_Ortho: WARNING, Speciation prob > 1.0." << endl
                        << "speciation_prob: " << speciation_prob.val() << endl;
            }
            ostringstream os;
            os << "[" << lnodes[0]->getName();
            for (unsigned i = 1; i < lnodes.size(); i++)
                os << " " << lnodes[i]->getName();
            os << ",";
            for (unsigned i = 0; i < rnodes.size(); i++)
                os << " " << rnodes[i]->getName();
            os << "]";
            os << "\t" << speciation_prob.val() << endl;
            cout << os.str();
            outfile << os.str();
        }
    }
    outfile.close();
}

bool DLRSOrthoCalculator::isObligateDuplication(Node *lca, LambdaMap sigma) {
    vector<Node*> desc = getDescendentNodes(lca);
    string name = sigma[desc[0]]->getName();
    for (unsigned int i = 1; i < desc.size(); i++) {
        if (sigma[desc[i]]->getName() != name)
            return false;
    }
    return true;
}

std::vector<std::string> &DLRSOrthoCalculator::split_str(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(item);
    }
    return elems;
}

std::vector<std::string> DLRSOrthoCalculator::split_str(const std::string &s, char delim) {
    std::vector<std::string> elems;
    return split_str(s, delim, elems);
}

/**
 * create_lookup_tables
 * Create an id for each gene in the range 0,..., gs_map.size()-1.
 * Then created lookup tables for gene name to id mapping and vice versa.
 *
 * @param ID Storage of id to gene map upon return.
 * @param inv_ID Storage of gene to id map upon return.
 * @param gs_map Gene to species map
 */
void DLRSOrthoCalculator::create_lookup_tables() {
    int nr_genes; //Total number of genes
    string gene_name; //Gene name

    /*
     * Go through all genes in the gene-species map. Create an
     * id and a record in both lookup-tables for each gene found
     */
    nr_genes = gsMap->size();
    for (int i = 0; i < nr_genes; i++) {
        gene_name = gsMap->getNthItem(i);
        ID.insert(pair<int, string > (i, gene_name));
        inv_ID.insert(pair<string, int>(gene_name, i));
    }
}

void DLRSOrthoCalculator::printVector(vector<Node*> v) {
    for (unsigned i = 0; i < v.size(); i++)
        cout << v[i]->getNumber() << endl;
    cout << endl;
}

vector<unsigned> DLRSOrthoCalculator::getIdsFromNodes(vector<Node*> nodes) {
    vector<unsigned> ids;
    for (unsigned i = 0; i < nodes.size(); i++)
        ids.push_back(gsMap->getIdFromGeneName(nodes[i]->getName()));
    return ids;
}
