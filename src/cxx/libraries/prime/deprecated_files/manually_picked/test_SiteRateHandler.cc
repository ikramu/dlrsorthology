#include <iostream>

#include "AnError.hh"
#include "ConstRateModel.hh"
#include "Beep.hh"
#include "SiteRateHandler.hh"
#include "UniformDensity.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <nCat>"
       << " <alpha>"
       << "\n"
       << "Parameters:\n"
       << "   <nCat>            An int > 0\n" 
       << "   <alpha>           A double > 0\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 3) 
    {
      usage(argv[0]);
    }

  try
    {
      unsigned ncat = atoi(argv[1]);
      Real alpha_in = atof(argv[2]);

      cout << "Testing constructor\n"
	   << "(alpha now = 1.0)\n"
	   << "--------------------\n";
      UniformDensity ud(0,100 , true);
      ConstRateModel alpha(ud, Tree::EmptyTree(), 1.0);      
      SiteRateHandler srm(ncat, alpha);
      cout << srm;
      
			   
      cout << "\nTesting copy and assign\n"
	   << "---------------------    \n";
      SiteRateHandler copy(srm);
      cout << "copy:\n"
	   << copy
	   << "\n";
      SiteRateHandler assign = srm;
      cout << "assign:\n"
	   << assign
	   <<"\n";
      cout << "Testing getRate()\n"
	   << "-----------------\n"
	   << "cat\trate\n";
      for(unsigned i = 0; i < ncat; i++)
	{
	  cout << i << "\t" << srm.getRate(i) << "\n";
	}

      cout << "\nTesting setAlpha and update()\n"
	   << "-------------------------------\n";
      if(srm.setAlpha(alpha_in))
	{
	  cout << srm
	       << "cat\trate\n";
	  for(unsigned i = 0; i < ncat; i++)
	    {
	      cout << i << "\t" << srm.getRate(i) << "\n";
	    }
	}
      else
	cout <<  "SetAlpha failed\n";

    }
  catch(AnError e)
    {
      e.action();
    }
  return 0;
}
      
 
