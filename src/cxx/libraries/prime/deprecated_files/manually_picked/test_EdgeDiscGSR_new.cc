#include "AnError.hh"
#include "Beep.hh"
#include "BeepOption.hh"
#include "BirthDeathProbs.hh"
#include "EdgeDiscBDProbs.hh"
#include "EdgeDiscGSR.hh"
#include "EdgeDiscPtMapIterator.hh"
#include "EdgeDiscTree.hh"
#include "GammaDensity.hh"
#include "StrStrMap.hh"
#include "TreeDiscretizers.hh"
#include "TreeIO.hh"

/* The number of parameters required */
static const int NO_OF_PARAMS = 4;

/* Path to the species tree */
std::string g_paramSFile = "";

/* The time of the top edge in the species tree */
beep::Real  g_paramToptime = 0.0;

/* The path to the gene tree */
std::string g_paramGFile = "";

/* Path to the mapping from leaves of the gene tree to the leaves of the
 * species tree */
std::string g_paramGSMapFile = "";

/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
	using namespace beep;
	using namespace beep::option;
	using namespace std;

	try
	{
                /*
                 * Handle input arguments
                 */
		int argIndex = 1;
		if (argc - argIndex != NO_OF_PARAMS) {
                    throw AnError("Wrong number of input parameters!");
                }
		
		/* Store the path to the species tree */
		g_paramSFile = argv[argIndex++];

                /* Read the top time and make sure it is valid */
		if (!BeepOptionMap::toDouble(argv[argIndex++], g_paramToptime)) {
                    throw AnError("Invalid top time value!");
                }
                if(g_paramToptime <= 0){
                    throw AnError("Toptime must be larger than zero.");
                }

                /* Store path to gene tree and the mapping*/
		g_paramGFile = argv[argIndex++];
		g_paramGSMapFile = argv[argIndex++];


                /* Read the species tree at set its top time */
		Tree S(TreeIO::fromFile(g_paramSFile).readHostTree());
                S.setTopTime(g_paramToptime);

                /* Create the discretized species tree */
		cout << "**** Creating discretized host tree. ****" << endl;
		EquiSplitEdgeDiscretizer disc(7, 2);
		EdgeDiscTree* DS = disc.getDiscretization(S);

                /* Print the species and discretized species tree */
                cout << "**** Printing ordinary host tree. ****" << endl;
                cout << S << endl;

		cout << "**** Printing discretized host tree. ****" << endl;
		cout << (*DS) << endl;

                /* Read the gene tree */
		cout << "**** Creating guest tree. ****" << endl;
		StrStrMap gsMap(TreeIO::readGeneSpeciesInfo(g_paramGSMapFile));
		TreeIO tio = TreeIO::fromFile(g_paramGFile);
		Tree G = Tree(tio.readBeepTree(NULL, &gsMap));

                cout << "**** Printing gene tree. ****" << endl;
                cout << G << endl;
				
		cout << "**** Creating birth/death probabilities holder with rates 0.6/0.4. ****" << endl;
		EdgeDiscBDProbs BDProbs(*DS, 0.6, 0.4);
		
		cout << "**** Creating edge rate Gamma function with mean/variance 0.5/0.2. ****" << endl;
		GammaDensity rateDF(0.5, 0.2);
		
		cout << "**** Creating discretized GSR model. ****" << endl;
		EdgeDiscGSR gsr(G, *DS, gsMap, rateDF, BDProbs, NULL);

                EdgeDiscretizer::Point topPoint = DS->getTopmostPt();
                gsr.getPlacementProbability(G.getRootNode(), &topPoint);

                cout << gsr.getDebugInfo(true, true, true, true) << endl;

                Tree::const_iterator it;
                cout << "Total placement probability for vertices in G: " << endl;
                for (it = G.begin(); it != G.end(); ++it)
                {
                    Node* u = (*it);
                    Probability total = gsr.getTotalPlacementProbability(u);
                    cout << u->getNumber() << ": " << total.val() << endl;
                }
                
                cout << "This should be equal to P(G)" << endl;

		delete DS;
	}
	catch (AnError& e)
	{
		e.action();
		cout << "Usage: " << argv[0] << " <host tree file> <top time> <guest tree file> <guest-host map file>" << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
		cout << "Usage: " << argv[0] << " <host tree file> <top time> <guest tree file> <guest-host map file>" << endl;
	}
	cout << endl;
};
