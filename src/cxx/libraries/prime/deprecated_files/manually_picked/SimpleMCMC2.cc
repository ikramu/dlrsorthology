#include "SimpleMCMC2.hh"

#include "AnError.hh"
#include "Hacks.hh"
#include "MCMCObject.hh"
#include "MCMCModel.hh"

#include <iostream>
#include <sstream>
#include <unistd.h>
#include <cmath>


namespace beep
{
  using namespace std;
  
  SimpleMCMC2::SimpleMCMC2(MCMCModel& M)
    : AbstractMCMC(), model(M),
      R(M.getPRNG())
  {
    p = model.initStateProb();    // To set stateProbs
    model.commitNewState();       // To set old_stateProbs, 
  }

  SimpleMCMC2::~SimpleMCMC2()
  {
  }

  bool SimpleMCMC2::iterate( IterationObserver & iterObs, unsigned int * iterations_done ) {
      unsigned int n_iters = iterObs.iterations();
      bool state_changed = false;
      *iterations_done = 0;
      for (unsigned int i = 0; i < n_iters; i++,  *iterations_done++) 
        {
	  try 
	    {
	      MCMCObject proposal = model.suggestNewState();

	      Probability alpha = 1.0;
	      if(p > 0)
		{
		  alpha = proposal.stateProb * proposal.propRatio / p;
		}
	    
	      if(alpha >= 1.0 || Probability(R.genrand_real1()) <= alpha)
		{
		  model.commitNewState();
		  p = proposal.stateProb;
		  state_changed = true;
		}
	      else
		{
		  model.discardNewState();
		  // Note that since we are discarding we do NOT update output
		  // but leave it as in last iteration
		}

	std::string stdoutStr;
	std::string stderrStr;
	if ( ! iterObs.afterEachStep( model, i + 1, state_changed, stdoutStr, stderrStr ) ) {  return false; };
        cout << stdoutStr;
        cerr << stderrStr;
	      // 	  }
	    }
	  catch (AnError& e)
	    {
	      cerr << "SimpleMCMC2::iterate\nAt iteration " 
		   << i 
		   << ".\nState is "
		   << model.strRepresentation()
		   << endl;
	      e.action();
	    }
	}
      return true;
    };

}//end namespace beep
