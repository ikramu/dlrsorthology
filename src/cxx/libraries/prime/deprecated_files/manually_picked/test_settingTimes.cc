#include "AnError.hh"
#include "Node.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "ReconciliationTimeSampler.hh"


int
main()
{
  using namespace beep;  
  using namespace std;
  try
    {
      
      string S_str = 
	"(("
	"A:0.4 [&&BEEP ID=0],"
	"B:0.4 [&&BEEP ID=1]"
	"):0.6 [&&BEEP ID=2],"
	"C:1.0[&&BEEP ID=3]"
	")";
      string G_str =
	//        "(A[&&BEEP BL=45.0],B[&&BEEP BL=5.0])[&&BEEP BL=3.0]";

  	"((("
	"A1:5[&&BEEP ID=4 NT=0 S=A],"
	"A2:5[&&BEEP ID=1 NT=0 S=A]"
	"):4[&&BEEP ID=2 NT=0.5], "
	"B1:9[&&BEEP ID=3 NT=0 S=B]"
	"):1[&&BEEP ID=0 NT=0.9],"
	"C1:10[&&BEEP ID=5 NT=0 S=C]"
	"):0[&&BEEP ID=6 NT=1.0]";

      //  	"(A[&&BEEP ID=0 NT=0.0 BL=4.0],B[&&BEEP ID=1 NT=0.0 BL =5.0])[&&BEEP ID=2 NT=0.5 BL=3.0]";

      TreeIO ioS = TreeIO::fromString(S_str);
      TreeIO ioG = TreeIO::fromString(G_str);
      
      cout << "Testing TreeIO::readGeneTree()\n"
	   <<"*******************************\n";

      vector<SetOfNodes> AC;
      StrStrMap gs;
      Tree G = ioG.readBeepTree(false, true, true, false, &AC, &gs);
      cerr << G.print(true, true, true);
      cout << endl;

      Node* g = G.getNode(2);
      if(g->changeNodeTime(0.4))
	cout << "After 2.changeNodeTime(0.4):\n";
      else
	throw AnError("g->changeNodeTime() failed");
      cerr << G.print(true, true, true);
      cout << endl;
      
      if(g->changeTime(0.4))
	cout << "After 2.changeTime(0.4):\n";
      else
	throw AnError("g->changeTime() failed");
      cerr << G.print(true, true, true);

      cout << endl;
      
      g->setNodeTime(0.4);
      if(g->getLeftChild()->updateTime() &&
	 g->getRightChild()->updateTime() && 
	 (g->isRoot() ||
	  g->updateTime()))
	cout << "After 2.setNodeTime(0.4) and updateTims:\n";
      else
	throw AnError("g->updateTime() failed");	
      cerr << G.print(true, true, true);

      cout << endl;

      g->setTime(0.4);
      g->getLeftChild()->setTime(0.5);
      g->getRightChild()->setTime(0.5);
      g->getLeftChild()->updateNodeTime();
      g->getRightChild()->updateNodeTime();
      g->updateNodeTime();
      if(g->getParent()->updateNodeTime())
	cout << "After 2.setTime(0.2) and updateNodeTime:\n";
      else
	throw AnError("g->getParent()->updateNodeTime() failed");	
      cerr << G.print(true, true, true);

      cout << endl;

      cout << "Testing TreeIO::readSpeciesTree()\n"
	   <<"*******************************\n";

      Tree S = ioS.readHostTree();
      cerr << S.print(true, true, true);
      cout << endl;
      
      Node* s = S.getNode(2);
      if(s->changeNodeTime(0.6))
	cout << "After 2.changeNodeTime(0.6):\n";
      else
	throw AnError("s->changeNodeTime() failed");	
      cerr << S.print(true, true, true);

      cout << endl;

      if(s->changeTime(0.6))
	cout << "After 2.changeTime(0.6):\n";
      else
	throw AnError("s->changeTime() failed");
      cerr << S.print(true, true, true);

      cout << endl;
      
      s->setNodeTime(0.6);
      if(s->getLeftChild()->updateTime() &&
	 s->getRightChild()->updateTime() && 
	 (s->isRoot() ||
	  s->updateTime()))
	cout << "After 2.setNodeTime(0.6) and updateTime:\n";
      else
	throw AnError("s->updateTime() failed");	
      cerr << S.print(true, true, true);

      cout << endl;

      s->setTime(0.6);
      s->getLeftChild()->setTime(0.4);
      s->getRightChild()->setTime(0.4);
      if(!s->getLeftChild()->updateNodeTime())
	cout << "0.updateNodeTime() failed\n";
      if(!s->getRightChild()->updateNodeTime())
	cout << "1.updateNodeTime() failed\n";
      if(s->updateNodeTime())
	cout << "After 2.setTime(0.4) and updateNodeTime:\n";
      else
	throw AnError("s->updateNodeTime() failed");	
      cerr << S.print(true, true, true);

      cout << endl;

      cout << "Testing ReconcilliationSampler\n"
	   <<"*******************************\n";

      cout << "G before sampling\n";
      cerr << G.print(true, true, true);

      cout << endl;

      LambdaMap lambda(G, S, gs);
      GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda);
      BirthDeathProbs nee(S, 0.5, 0.5);
      PRNG rand;
      ReconciliationTimeSampler sampler(G, nee, gamma);
      sampler.sampleTimes();

      cout << "G after sampling\n";
      cerr << G.print(true, true, true);

      cout << endl;

    }
  catch(AnError e)
    {
      e.action();
    }
}

