#include "DummyStdMCMC.hh"

#include "DummyMCMC.hh"
#include "MCMCObject.hh"

#include <sstream>


//---------------------------------------------------------------
//
// classMCMCModel
// Baseclass that provides common implementation code 
// for the "MCMCModel-class family", which uses 
// MCMC to intergrate over parameters
//
// An instance of the DummyStdMCMC class will inherit from a 
// ProbabilityModel class for likelihood calculations. 
// It also contains a MCMCModel class for calculation of any 
// additional prior probs that might exists; give a DummyMCMC 
// in constructor if no such prior occur. These two classes 
// contain the parameters fro DummyStdMCMC to perturb.
//
// The main task for this class is to decide on when, on 
// suggestNewState()-calls, to pass this call on to the nested 
// prior classes and when to perturb the parameter of the 
// ProbabilityModel baseclass.
// The suggestRatio is a parameter that decides if a likelihood
// or a prior parameter should be updated:
// suggestRatio = pr{a}/pr{b}, where a = the average likelihood 
// parameter a is perturbed, b = the average prior parameter b 
// is perturbed. The attribute numParams is the number of local 
// parameters. If a likelihood parameter is to be perturbed, the 
// pure virtual' function perturbState() is called.
//
// This class currently inherits from MCMCModel, it is possible
// that it might eventually replace MCMCModel.
//
//---------------------------------------------------------------



namespace beep
{
  using namespace std;
  
  //---------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  // 
  //---------------------------------------------------------------

  // Constructor
  // suggestRatio = pr{a}/pr{b}, a = a like parameter is perturbed, 
  // b = a prior parameter is pertubed. paramIdxRatio adjusts for 
  // unequal number of like and prior parameters.
  //---------------------------------------------------------------
  DummyStdMCMC::DummyStdMCMC()
    : StdMCMCModel(*new DummyMCMC(), 0, 0)
  {
    std::cerr << "DummyStdMCMC::constructor: \n"
	      << "   We are not sure if this class is really needed, \n"
	      << "   Would it be possible for you to use DummyMCMC instead?/n"
	      << "   if not, please state this in DummyStdMCMCModel,hh/n";
//     paramIdxRatio = 1;
  }

  DummyStdMCMC::DummyStdMCMC(const DummyStdMCMC& A)
    : StdMCMCModel(A)
  {
  }

  DummyStdMCMC::~DummyStdMCMC()
  {
    delete prior;
  }

  DummyStdMCMC& 
  DummyStdMCMC::operator=(const DummyStdMCMC& A)
  { 
    if(this != &A)
      {
	StdMCMCModel::operator=(A);
      }
    return *this;
  }



//   // Return number of states in prior (probably only used first time!) 
//   //----------------------------------------------------------------------
//   unsigned   
//   DummyStdMCMC::nParams()
//   {
//     return 0;
//   }

  //----------------------------------------------------------------------
  //
  // Internal interface
  //
  //----------------------------------------------------------------------
  MCMCObject 
  DummyStdMCMC::suggestOwnState()
  {
    return MCMCObject(1,1);
  }
  
  void
  DummyStdMCMC::commitOwnState()
  {
    return;
  }
  
  void
  DummyStdMCMC::discardOwnState()
  {
    return;
  }
  
  string
  DummyStdMCMC::ownStrRep() const
  {
    return "";
  }
  
  string
  DummyStdMCMC::ownHeader() const
  {
    return "";
  }
  
  // I have lumped calls to p.update() and p.calculateDataProbability(),
  // where p is the ProbabilityModel baseclass, because I think that they
  // will almost always be called together
  //----------------------------------------------------------------------
  Probability
  DummyStdMCMC::updateDataProbability()
  {
    return 1;
  }    

  //
  // I/O
  // I have not converged on a consensus for this yet, but the 
  // following functions should suffice
  //
  //----------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const DummyStdMCMC& A)
  {
    return o << A.print()
      ;
  };

  string 
  DummyStdMCMC::print() const
  {
    return std::string("None\n");
  }

}//end namespace beep
