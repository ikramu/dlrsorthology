#include <iostream>

#include "AnError.hh"
#include "DummyMCMC.hh"
#include "Density2PMCMC.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "Probability.hh"
#include "SimpleMCMC.hh"
#include "UniformDensity.hh"

void
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <density>"
       << " <embedded>"
       << "\n"
       << "Parameters:\n"
       << "   <density>         'Gamma', 'InvGauss', 'LogNorm' or 'Uniform'\n"
       << "   <embedded>        a bool, if true, a and b are taken to be \n"
       << "\n"
    ;
  exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 3) 
    {
      usage(argv[0]);
    }

  try
    {
      Real mean = 100.0;
      Real variance = 0.1;
      bool embedded = false;
      if(string(argv[2]) == "true")
	embedded = true;

      Density2P* df;

      cout << "Construct a density with ";
      if(embedded)
	      cout << "alpha = "
		   << mean
		   << " beta = "
		   << variance 
		   << "\n";
      else	      
	cout << "mean = "
	     << mean 
	     << " variance = "
	     << variance 
	     <<"\n";
      switch(argv[1][0])
	{
	case 'G':
	  {
	    df = new GammaDensity(mean, variance, embedded);
	    break;
	  }
	case 'I':
	  {
	    df = new InvGaussDensity(mean, variance, embedded);
	    break;
	  }
	case 'L':
	  {
	    df = new LogNormDensity(mean, variance, embedded);
	    break;
	  }
	case 'U':
	  {
	    df = new UniformDensity(mean, variance, embedded);
	    break;
	  }
	default:
	  {
	    cerr << "density " << argv[1] << " is not recognized!\n";
	    exit(1);
	    break;
	  }
	}
      DummyMCMC dm;
      Density2PMCMC d2m(dm, *df);
      cout << d2m;

      SimpleMCMC iterator(d2m, 10000);

      time_t t0 = time(0); 
      clock_t ct0 = clock();

      iterator.iterate(100000);

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;

    }
  catch(AnError e)
    {
      cerr <<" error\n";
      e.action();
    }
}
