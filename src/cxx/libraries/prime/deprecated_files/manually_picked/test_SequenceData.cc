#include "AnError.hh"
#include "Beep.hh"
#include "SequenceType.hh"
#include "SequenceData.hh"


// TODO: Make this a more complete test of SequenceData /bens


int
main ()
{
  using namespace beep;
  using namespace std;
  
  try
    {
      DNA dna;
      SequenceData sd(dna);
      
      sd.addData("sp1", "ACGTACCGTGa");
      sd.addData("sp2", "AAAAACCGTAa");
      sd.addData("sp3", "AGBGACCGTBa");
     
      cout << "testing output\n--------------------------------------------\n"
	   << sd;
      cout << "The data matrix as ouput by data4os()\n"
	   << sd.data4os()
	   << endl;

      cout << "Testing getSortedData() on dataset above\n";
      PatternVec pv = sd.getSortedData();
      for(PatternVec::const_iterator i = pv.begin(); i !=pv.end(); i++)
	cout << i->first << "\t" << i->second << "\n";
      
    }
  catch(AnError e)
    {
      e.action();
    }
  return 0;
}
