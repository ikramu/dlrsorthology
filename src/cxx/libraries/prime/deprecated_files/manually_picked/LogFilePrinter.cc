/* 
 * File:   LogFilePrinter.cc
 * Author: peter9
 * 
 * Created on January 28, 2010, 1:41 PM
 */

#include <vector>

#include "LogFilePrinter.hh"
#include "PRNG.hh"
#include <ctime>
#include <iostream>
#include <fstream>

static const string ENTRY_LINE = "=======================================";
static string FILENAME = "log.txt";


//Initialize static members of LogFilePrinter
LogFilePrinter *LogFilePrinter::s_printer = 0;
LogFilePrinterDestroyer LogFilePrinter::s_destroyer;

LogFilePrinter* LogFilePrinter::getLogFilePrinter () {
    if (!s_printer) {
        //Create new singleton object
        s_printer = new LogFilePrinter;
        //Create new destroyer for the singleton object
        s_destroyer.setLogFilePrinter(s_printer);
    }
    return s_printer;
}

void
LogFilePrinter::printMessage(const string &message)
{
    m_file << message << endl;
    m_file.flush();
}


void 
 LogFilePrinter::printSetup(const string &message)
 {
     m_file << message << endl;
     printDateAndTime();
     printRandomSeed();
     m_file << endl;
     m_file.flush();
 }

void
LogFilePrinter::printEntry(const string &message)
{
    m_file << endl;
    m_file << ENTRY_LINE << endl;
    printDateAndTime();
    m_file << message << endl;
    m_file << ENTRY_LINE << endl;
    m_file << endl;
    m_file.flush();
}

void
LogFilePrinter::printRandomSeed()
{
    beep::PRNG r;
    m_file << "Random seed: " << r.getSeed() << endl;
    m_file.flush();
}

void
LogFilePrinter::printDateAndTime()
{
    // Get current timestamp
    time_t cur = time(0);

    // Convert to local time
    struct tm *localtm = localtime(&cur);

    //Print to file
    m_file << "Date and time: " << asctime(localtm);
    m_file.flush();
}

void
LogFilePrinter::setOutputFile(string &path)
{
    if(s_printer != 0){
        /* Close old file if logger is started */
        s_printer->m_file.close();
        s_printer->m_file.open(path.c_str());
    }
    else {
        /* Change standard filename */
        FILENAME = path;
    }
}

LogFilePrinter::LogFilePrinter():m_file(FILENAME.c_str()){}
LogFilePrinter::~LogFilePrinter(){}

LogFilePrinterDestroyer::LogFilePrinterDestroyer(LogFilePrinter *p) {
    m_printer = p;
}

LogFilePrinterDestroyer::~LogFilePrinterDestroyer() {
    delete m_printer;
}

void
LogFilePrinterDestroyer::setLogFilePrinter(LogFilePrinter *p) {
    m_printer = p;
}

///**
//     * active
//     *
//     * Decides if a log file is being used at the moment, i.e. the singleton
//     * object is "active".
//     */
//    static bool isActive(){return s_printer != NULL;}