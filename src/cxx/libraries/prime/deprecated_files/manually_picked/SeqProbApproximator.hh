#ifndef SEQPROBAPPROXIMATOR_HH
#define SEQPROBAPPROXIMATOR_HH

#include "BirthDeathProbs.hh"
#include "MCMCModel.hh"
#include "GammaMap.hh"
#include "ProbabilityModel.hh"
#include "ReconciliationTimeSampler.hh"
#include "SubstitutionMCMC.hh"

namespace beep
{

  class SeqProbApproximator : public ProbabilityModel, 
			      public StdMCMCModel
  {
  public:
    //---------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //---------------------------------------------------------------
    SeqProbApproximator(MCMCModel& prior,
			ReconciliationTimeSampler &rts,
			SubstitutionMCMC& sm,
			unsigned n_samples,
			bool includeTopTime = true);
    SeqProbApproximator(const SeqProbApproximator &spa);
    ~SeqProbApproximator();
    SeqProbApproximator& operator=(const SeqProbApproximator &spa);

    //---------------------------------------------------------------
    //
    // Interface
    //
    //---------------------------------------------------------------

    //---------------------------------------------------------------
    // StdMCMCModel interface
    //---------------------------------------------------------------
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
  
    std::string ownStrRep() const;
    std::string ownHeader() const;
    
    Probability updateDataProbability();

    //---------------------------------------------------------------
    // ProbabilityModel interface
    //---------------------------------------------------------------
    Probability calculateDataProbability();
    void        update();	// Should be empty?

    //----------------------------------------------------------------------
    // I/O
    // I have not converged on a consensus for this yet, but the 
    // following functions should suffice
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const SeqProbApproximator& A);
    std::string print() const;
    
  private:
    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
    Probability sumDataProb(unsigned samples_don, Probability sum);

    //---------------------------------------------------------------
    //
    // Attributes
    //
    //---------------------------------------------------------------
  private:
    ReconciliationTimeSampler &rts;
    SubstitutionMCMC& sm;

    unsigned     n_samples;

    bool topTime;

    //
    // An error is generated if you try to get more samples that this:
    //
    static const unsigned MAX_N_SAMPLES=10000000; 
  };

}//end namespace beep

#endif
