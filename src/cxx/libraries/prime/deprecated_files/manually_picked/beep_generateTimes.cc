
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <cstdlib>


#include "AnError.hh"
#include "Beep.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "PRNG.hh"
#include "ReconciliationTimeSampler.hh" 
#include "StrStrMap.hh"
#include "Tree.hh"
#include "TreeIO.hh"

using std::vector;

// Global options with default settings
//------------------------------------
int nParams = 1;

char* outfile = 0;   //NULL;
char* t90file = 0;  //NULL

// output to true file
bool outputSTree = true;
bool rootTime = false;
double birthRate = 1.0;
double deathRate = 1.0;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv)
{
  using namespace beep;
  using namespace std;
  
  if (argc < nParams+1) 
    {
      usage(argv[0]);
    }
  try
    {
      // Tell user we started 
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory";
      system("pwd");
      cerr << "\n\n";
      
      // Read options
      //---------------------------------------------
      int opt = readOptions(argc, argv);

      // Read arguments
      //---------------------------------------------
      if (opt >= argc)
	{
	  cerr << "Expected treefile argument\n";
	  usage(argv[0]);
	}
      string treefile(argv[opt++]);

     
      // Get the model tree
      //---------------------------------------------
      TreeIO io = TreeIO::fromFile(treefile);
      Tree G = io.readGuestTree();
      G.setName("G");
      cerr << G;

      // Set up priors
      //---------------------------------------------------------
      Tree S = Tree::EmptyTree();
      S.setName("S");
      //set up a gamma
      StrStrMap gs;
      // Map all leaves of G to the single leaf in S
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  gs.insert(G.getNode(i)->getName(), S.getRootNode()->getName());
	}
      LambdaMap lambda(G, S, gs);
      GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda); 
      BirthDeathProbs bdm(S, birthRate, deathRate);
      PRNG rand;
      ReconciliationTimeSampler rts(G, gs, bdm, gamma, rand);

      rts.sampleTimes(rootTime);


      // Output Data
      //---------------------------------------------------------
      cout << endl
	   << "Edgetimes were generated\n"
	   << G;

      // saving tree
      if(outfile != NULL)
	{
	  cout << "The tree is written to file "
	       << outfile
	       << "\n";
	  TreeIO io_out;
	  ofstream tree((string(outfile)).data());
	  tree << io_out.writeHostTree(G) << "\n";
	}


      if(t90file != 0)
	{
	  ofstream t90((string(t90file)).data());
	  TreeIO io_out;
	  ostringstream t90header;
	  ostringstream t90data;
	  t90header << "# True parameters input for 90% test\n"
		    << "# T N\tbirthRate(float)\tdeathRate(float)\tG(tree);\t";
	  t90data << "0 0\t"
		  << birthRate
		  << ";\t"
		  << deathRate
		  << ";\t";
	  t90data << io_out.writeGuestTree(G)
		  << ";\t";
	  // 	    }

	  for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	    {
	      Node * n = G.getNode(i);
	      if(n->isLeaf() || n->isRoot())
		{
		  continue;
		}
	      else
		{
		  t90header << "EdgeTimes.nodeTime[" << i << "](float);\t";
		  t90data << n->getNodeTime() << ";\t";
		}
	    }
	  if(outputSTree)
	    {
	      
	      t90header << "\tS(tree);";
	      t90data << "\t" 
		      << io_out.writeHostTree(S)
		      << ";";
	    }
	  t90header << "\n";
	  t90data << "\n";
	  t90 << t90header.str() << t90data.str();
	}
    }
  catch (AnError e)
    {
      cerr << "error found\n";
      
      e.action();
    }
}


  void 
    usage(char *cmd)
  {
    using std::cerr;
    cerr 
      << "Usage: "
      << cmd
      << " [<options>] <treefile>\n"
      << "\n"
      << "Parameters:\n"
      << "   <treefile>         name of file with tree to generated times on.\n"


      << "Options:\n"
      << "   -u, -h                This text.\n"
      << "   -o <filename>         output file\n"
      << "   -T<option>            Options for the output of true parameters\n"
      << "                         used in the generation (off by default)\n"
      << "     -To <file>          Output 'true' parameters to file <file>,\n"
      << "                         by default outputs all parameters.\n"
      << "     -Th                 Do not output the host tree and edge\n"
      << "                         times\n"
      << "                         used for data generation\n"
      << "   -G<option>            Options relating to the generated tree\n"
      << "     -Gr                 Generate also root's edge time\n"
      << "                         (default without)\n"
      << "   -B<option>            Options relating to the birth death model\n"
      << "     -Bp <float> <float> birth and death rate of tree model\n"
      << "                         Default is " << birthRate << " and "<< deathRate << ".\n";
    ;
    exit(1);
  }

  int
    readOptions(int argc, char **argv) 
  {
    using namespace beep;
    using namespace std;

    int opt=1;
    while (opt < argc && argv[opt][0] == '-') 
      {
	switch (argv[opt][1]) 
	  { //General options
	  case 'h':
	  case 'u':
	    {
	      usage(argv[0]);
	      break;
	    }
	  case 'o':
	    {
	      if (opt + 1 < argc)
		{
		  outfile = argv[++opt];
		}
	      else
		{
		  cerr << "Expected filename after option '-o'\n";
		  usage(argv[0]);
		}
	      break;
	    }
	  case 'T':
	    { // 'true' options
	      switch (argv[opt][2]) 
		{ // suboptions
		case 'o':
		  {
		    if (opt + 1 < argc)
		      {
			t90file = argv[++opt];
		      }
		    else
		      {
			cerr << "Expected filename after option '-To'\n";
			usage(argv[0]);
		      }
		    break;
		  }
		case 'h':
		  {
		    outputSTree = false;
		    break;
		  }
		default:
		  {
		    cerr << "option " << argv[opt] << " not recognized\n";
		    usage(argv[0]);
		    break;
		  }
		}
	      break;
	    }
	  case 'G':
	    { // Substitution model options
	      switch (argv[opt][2]) 
		{ // suboptions
		case 'r':
		  {
		    rootTime = true;
		    break;
		  }
		default:
		  {	
		    cerr << "option " << argv[opt] << " not recognized\n";
		    usage(argv[0]);
		    break;
		  }
		}
	      break;
	    }
	  case 'B':
	    {
	      switch (argv[opt][2]) 
		{
		case 'p':
		  {
		    if (++opt < argc) 
		      {
			birthRate = atof(argv[opt]);
			if (++opt < argc) 
			  {
			    deathRate = atof(argv[opt]);
			  }
			else
			  {
			    cerr << "Expected float (death rate)\n";
			    usage(argv[0]);
			  }
		      }
		    else
		      {
			cerr << "Expected pair of floats (birth and death rate "
			     << "for tree model) for option '-e'!\n";
			usage(argv[0]);
		      }
		    break;
		  }
		default:
		  {
		    cerr << "option " << argv[opt] << " not recognized\n";
		    usage(argv[0]);
		    break;
		  }
		}
	      break;
	    }
	  default:
	    {	
	      cerr << "option " << argv[opt] << " not recognized\n";
	      usage(argv[0]);
	      break;
	    }
	  }
	opt++;
      }
    return opt;
  }
