#ifndef DISTRRECONSEQAPPROXIMATOR_HH
#define DISTRRECONSEQAPPROXIMATOR_HH


#include "BirthDeathMCMC.hh"
#include "ConstRateModel.hh"
#include "OrthologyMatrix.hh"
#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "ReconciliationSampler.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "StdMCMCModel.hh"
#include "Tree.hh"

#include "PvmMaster.hh"
#include "PvmDeadClientError.hh"

using namespace beep;

namespace beep {

  //! DistrReconSeqApproximator
  //!
  //!
  //! The purpose of this class is to approximate \f$\Pr(Q, G| \lambda, \mu)\f$
  //! by sampling reconciliations and nodes times and compute an average.
  //! It is derived from ReconSeqApproximatorm, but will do the computation
  //! by performing sampling on client processes using PvmMaster and 
  //! friends. 
  //! 
  //! The main distributed protocol is as follows: 
  //! -# Tell clients about parameters
  //! -# Clients compute their stuff independently, but send data to master
  //!    (DistrReconSeqApproximator) every 100 or so computations. 
  //! -# Master receives partial results from clients and checks if there
  //!    is more to do.
  //! -# If enough computations are done, then master sums the sums and
  //!    computes averages and returns result.
  //! -# When new params arrive, goto 1.
  //! 
  //! Clients are never told to stop computing, so if the master is happy
  //! with the amount of data, clients will have computed too much. Those
  //! results are silently dropped. This makes for easy and clear
  //! implementation, and if the batch sizes are small enough the
  //! inefficiency will be limited. 
  //! 

class DistrReconSeqApproximator : public ProbabilityModel, public StdMCMCModel
{
public:
  //
  // Construct / Destruct / Assign
  //
  DistrReconSeqApproximator(MCMCModel& prior,
			    const std::string &clientProgram,
			    const unsigned n_clients,
			    BirthDeathProbs &bdp,
			    ConstRateModel  &alpha, //!< For substitution rate
			    SequenceData &F,       //!< Sequence data
			    const std::string& modelname,
			    Tree &G,
			    StrStrMap    &gs,      //!< gene->species map
			    const unsigned n_samples);


  DistrReconSeqApproximator(const DistrReconSeqApproximator &drsa); // Copy constuctor
 ~DistrReconSeqApproximator();
  //  DistrReconSeqApproximator& operator=(const DistrReconSeqApproximator& drsa);

  //! Choose mode of operation: Should orthology be estimated or not?
  //! This is on by default, but can be turned off with this method.
  //! I chose to not set this in the constructor since there are so many 
  //! args there already!
  //!
  void outputOrthology(bool val = true);


  //-----------------------------------------------------------------------
  //
  // StdMCMCModel interface
  //
  MCMCObject suggestOwnState();
  void commitOwnState();
  void discardOwnState();

  std::string ownStrRep() const;
  std::string ownHeader() const;

  Probability updateDataProbability();


  //-----------------------------------------------------------------------
  //
  // ProbabilityModel interface
  //
protected:
  virtual Probability calculateDataProbability();
  virtual void update();

private:
  //
  //! Get a string representation of a lower triangular matrix containing
  //! orthology predictions.
  //
  string strOrthology() const;

  //----------------------------------------------------------------------
  //
  // I/O
  //
  //----------------------------------------------------------------------
public:
  friend std::ostream& operator<<(std::ostream &o,
				  const DistrReconSeqApproximator& drsa);
  std::string print() const;


protected:
  //  Helpers
  //
  bool  receiveOneBatch(Probability &term); // Put batch prob estimate in 'term', return true if valid data.
  void  handleDeadClient(PvmDeadClientError &pe);

  //----------------------------------------------------------------------
  //
  // Attributes
  //  
private:
  //
  bool estimateOrthology;	//!< Flag, yes or no. 

  // PVM
  PvmMaster             master;	///< Handles all PVM communication
  int                   bsize;	//!< How many samples a client handles in one batch
  int                   n_dead_clients;	//!< Keep track of misfortune!
  
  //
  // Parameters owned by other objects:
  //
  BirthDeathProbs       &kendall;
  Tree*                 G;	   //!< The gene tree
  ConstRateModel        &molClock; //!< For now, assume a molecular clock, here and in client process


  //
  // Other params
  //
  unsigned               n_samples; //!< This tells us how many reconciliations to sample.
  unsigned               numDataPoints;	//!< The number of collected samples this far.

  //! Distinguish client results by a batch number.
  //! The client batches will overlap with different parameters.
  int                    current_batch;	

  OrthologyMatrix        orthology; //!< Collect orthology information in an lower triangular matrix

  //!
  //! An error is generated if you try to get more samples that this:
  //!
  static const unsigned  MAX_N_SAMPLES=10000000; 

};

}

#endif
