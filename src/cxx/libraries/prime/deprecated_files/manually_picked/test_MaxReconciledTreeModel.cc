#include "AnError.hh"
#include "BranchSwapping.hh"
#include "DummyMCMC.hh"
#include "BirthDeathMCMC.hh"
#include "MaxReconciledTreeModel.hh"
#include "EnumerateReconciliationModel.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"
#include "GuestTreeModel.hh"



void usage(char* cmsd);
int readOptions(int argc, char **argv);
int nParams = 2;

double topTime           = -1.0;
double birthRate         = 1.0;
double deathRate         = 1.0;

// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  if(argc <= nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = readOptions(argc, argv);
      if(opt + nParams > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	}
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[opt++]);
      TreeIO io = TreeIO::fromFile(guest);
      StrStrMap gs;
      Tree G = io.readBeepTree(0,&gs);  // Reads times?
      G.setName("G");
      cerr << "G = \n"
	   << G;

      string host(argv[opt++]);
      cerr << "host =  " << host << endl;
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  // Reads times?
      S.setName("S");
      cerr << "S = \n" << S;

      if(gs.size() == 0 && opt < argc)
	{
	  gs = TreeIO::readGeneSpeciesInfo(argv[opt++]);
	}
      else
	{
	  cerr << "No leaf-to-leaf map (gs) given\n";
	  usage(argv[0]);
	}

      cerr << gs << endl;

      BirthDeathProbs bdp(S, birthRate, deathRate, &topTime);

      MaxReconciledTreeModel a(G, gs, bdp);
      Probability p1 = a.getMLReconciliation();

      GuestTreeModel gtm(a);
      Probability p2 = gtm.calculateDataProbability();

      Probability p = p1/p2;
       cerr << "The ML reconciliation:\n";
       GammaMap gamma = a.getGamma();
       cerr << gamma.print(false);
       assert(p == a.calculateDataProbability());
       cerr << "\n\nP[mlGamma] = "
	    << p1.val() << " = exp(" << p1 << ")" 
 	   << endl;
       cerr << "\n\nP[G] = "
	    << p2.val() << " = exp(" << p2 << ")" 
 	   << endl;
       cerr << "\n\nP[mlGamma|G] = "
	    << p.val() << " = exp(" << p << ")" 
 	   << endl;


      EnumerateReconciliationModel N(G, gs, bdp);
      N.setGamma(gamma);
      cerr << "\n\nThis Reconciliation has number "
	   << N.computeGammaID()
	   << " in the enumeration of "
	   << N.getNumberOfReconciliations()
	   << " reconciliations\n\n\n";


       cerr << "The ML reconciled tree in PRIME format is written to stdout:\n\n";
       cout << TreeIO::writeGuestTree(G, &gamma) << endl;
    }
  catch(AnError e)
    {
      cerr << "Error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what();
    }
  return(0);
};

void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << "[<options>] <guesttree> <hosttree> [<gs>]"
    << "\n"
    << "Computes ML reconciliations of <guesttree> to <hosttree>\n" 
    << "Outputs the ML reconciliation and its probability\n" 
    << "under the GEM model.\n"
    << "\n"
    << "Parameters:\n"
    << "   <guesttree>/<hosttree>  (string) names of file with guest tree in\n"
    << "                           PrIME or Newick format\n"
    << "   <gs>                    (string, optional) name of file with \n"
    << "                           leaf-to-leaf map from guest to host tree\n"

    << "Options:\n"
    << "  -u, -h          This text.\n"
    << "  -B<option>      Options relating to the bith death process\n"
    << "   -Bp <float> <float>\n"
    << "                  Set BD parameters to these values.\n"
    << "   -Bt <float>    Fix top time to this value\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'p':
		{
		  if (opt + 1 < argc)
		    {		  
		      birthRate = atof(argv[++opt]);
		      if (opt + 1 < argc)
			{		  
			  deathRate = atof(argv[++opt]);
			}
		      else
			{
			  cerr << "Expected birth and death param (float) after -Bp/f\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected birth and death param (float) after -Bp/f\n";
		      usage(argv[0]);
		      exit(1);
		    }
		  break;
		}
	      case 't':
		{
		  if (opt + 1 < argc)
		    {		  
		      topTime = atof(argv[++opt]);
		    }
		  else
		    {
		      cerr << "Expected top time param (float) after -Bt\n";
		      usage(argv[0]);
		      exit(1);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

