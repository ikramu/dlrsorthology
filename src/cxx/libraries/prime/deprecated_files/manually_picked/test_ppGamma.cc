#include <iostream>

#include "AnError.hh"
#include "DiscreteGamma.hh"


int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 4) 
    {
      cerr << "Usage: test_ppGamma <p> <alpha> <beta>\n";
      exit(1);
    }

  try
    {
      Real p = atof(argv[1]);
      Real alpha = atof(argv[2]);
      Real beta = atof(argv[3]);

      cout << "ppGamma(" 
	   << p
	   << ", "
	   << alpha 
	   << ", "
	   << beta
	   << ") = ";
      cout.flush();
      cout << ppGamma(p,alpha, beta)
	   << "\n";
    }
  catch(AnError e)
    {
      e.action();
    }
    return 0;
  }
