/*
 * PrIME-Ortho
 *
 * Calculates the probability of two groups of genes forming a group orthology.
 * The probability is based on the GSR model
 *
 * Author: Peter Andersson (peter9@kth.se) December 2009
 */



/*Includes*/

//Standard libraries
#include <stdlib.h>
#include "Orthology/GSROrthologyEstimator.hh"
#include <iostream>
#include <string>

//Gengetopt
#include "cmdline.h"

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "SeqIO.hh"
#include "GammaDensity.hh"
#include "SequenceData.hh"
#include "MatrixTransitionHandler.hh"
#include "StrStrMap.hh"



/*Namespaces used*/
using namespace beep;
using namespace std;


/*Definitions and constants*/


//True if progress diagnostics should be printed to stdout
static bool DIAGNOSTICS = true;
//Input arguments
static const unsigned int NUMBER_OF_INPUTS = 5;
static const int INPUT_SEQUENCES_SAMPLES_FILE = 0;
static const int INPUT_SPECIES_TREE_FILE = 1;
static const int INPUT_GSMAP_FILE = 2;
static const int INPUT_GENE_FILE_1 = 3;
static const int INPUT_GENE_FILE_2 = 4;
//Default MCMC parameters
static const Real DEFAULT_BIRTH_RATE = 1.0f;
static const Real DEFAULT_DEATH_RATE = 1.0f;
static const Real DEFAULT_MEAN = 1.0f;
static const Real DEFAULT_VARIANCE = 1.0f;


/*Pre-declarations*/
static void read_cmd_opt(   struct gengetopt_args_info  &args_info,
                            int                         argc,
                            char                        *argv[]);

static Tree read_species_tree(struct gengetopt_args_info &args_info);

static void create_disc_tree(   Tree &species_tree,
                                EdgeDiscTree **DS,
                                struct gengetopt_args_info &args_info);


static void read_gene_file( int argument,
                            vector<string> &g,
                            StrStrMap &gs_map,
                            struct gengetopt_args_info &args_info);

Probability ortho_mcmc(EdgeDiscTree &DS, StrStrMap &gs_map, struct gengetopt_args_info &args_info);

/**
 * main
 */
int main(int argc, char *argv[]){

    /*Program variables*/
    struct gengetopt_args_info  args_info;      //Used for option parsing.
    TreeIO                      tree_reader;    //Used for reading from files.
    Tree                        species_tree;   //The species tree.
    EdgeDiscTree                *DS;            //Discretized species tree.
    StrStrMap                   gs_map;         //The gene-species map used.
    Probability                 ortho_prob;     //Orthology probability

    /*Read command-line options*/
    read_cmd_opt(args_info, argc, argv);


    /*Read species tree*/
    if(DIAGNOSTICS){cout << "Reading species tree.. ";}
    species_tree = read_species_tree(args_info);
    if(DIAGNOSTICS){cout << "done!" << endl;}



    /*Create discretized tree*/
    if(DIAGNOSTICS){cout << "Creating discretized tree.. ";}
    create_disc_tree(species_tree, &DS, args_info);    
    if(DIAGNOSTICS){cout << "done!" << endl;}



    /*Read gene-species map*/
    if(DIAGNOSTICS){cout << "Reading gene-species map.. ";}
    gs_map = tree_reader.readGeneSpeciesInfo(args_info.inputs[INPUT_GSMAP_FILE]);
    if(DIAGNOSTICS){cout << "done!" << endl;}

    

    /*Find orthology probability*/
    if(args_info.mcmc_flag){
        /*Perform MCMC calculations and simultaneously calculate probability*/
        if(DIAGNOSTICS) {cout << "Calculating orthology probability from MCMC.." << endl;}
        ortho_prob = ortho_mcmc(*DS, gs_map, args_info);
        if(DIAGNOSTICS) {cout << "done calculating orthology probability!" << endl;}
    }
    else{
        //This is not supported at the moment!!! (but easy to implement, just use GSROrthologyEstimator
        /*Read in samples from file then calculate orthology probability*/
        if(DIAGNOSTICS){cout << "Reading states from file and calculating probability." << endl;}
//        ortho_prob = calc_ortho_prob_from_file( *DS, gs_map, species_tree, args_info);
        if(DIAGNOSTICS){cout << "done reading states from file and calculating probability!" << endl;}
    }


    /*Print orthology probability*/
    if(DIAGNOSTICS) {cout << "Printing orthology probability.. " << endl;}
    if(args_info.output_file_given){
        ofstream file(args_info.output_file_arg);
        file << ortho_prob.val() << endl;;
        file.close();
    }
    else{
       cout << "Orthology probability: " << ortho_prob.val() << endl;
    }
    if(DIAGNOSTICS) {cout << "done printing orthology probability!" << endl;}

    /*Clean up*/
    
    //Delete discretized tree
    delete DS;

    /*Exit program*/
    exit(EXIT_SUCCESS);
}






Probability
ortho_mcmc(EdgeDiscTree &DS, StrStrMap &gs_map, struct gengetopt_args_info &args_info)
{
    //Create JTT substitution model
    MatrixTransitionHandler Q = MatrixTransitionHandler(MatrixTransitionHandler::create(args_info.substitution_model_arg));

    // Get sequence data.
    SequenceData D = SequenceData(
                        SeqIO::readSequences(args_info.inputs[INPUT_SEQUENCES_SAMPLES_FILE],
                                                Q.getType()));

    //Gamma distribution
    GammaDensity gd = GammaDensity(DEFAULT_MEAN, DEFAULT_VARIANCE);

    //Read gene names
    vector<string> gn1;
    vector<string> gn2;
    read_gene_file(INPUT_GENE_FILE_1, gn1, gs_map, args_info);
    read_gene_file(INPUT_GENE_FILE_2, gn2, gs_map, args_info);

    //Calculate orthology
    return GSROrthologyEstimator::estimateOrthologyFromMCMC(args_info.iterations_arg,
                                                    args_info.thinning_arg,
                                                    DS,
                                                    gs_map,
                                                    Q,
                                                    D,
                                                    gd,
                                                    DEFAULT_BIRTH_RATE,
                                                    DEFAULT_DEATH_RATE,
                                                    gn1,
                                                    gn2,
                                                    DIAGNOSTICS,
                                                    args_info.burn_in_arg);
}



/**
 * read_species_tree
 *
 * Read a species tree from file.
 *
 * @param args_info Program arguments
 */
Tree
read_species_tree(struct gengetopt_args_info &args_info)
{
    TreeIO tree_reader = TreeIO::fromFile(args_info.inputs[INPUT_SPECIES_TREE_FILE]);
    return tree_reader.readHostTree();
}



/**
 * read_cmd_opt
 *
 * Deal with typical command-line option parsing using gengetopt.
 *
 * @param args_info On return, contains all option and input info.
 * @param argc NO input parameters (same as for main)
 * @param argv Input arguments (same as for main)
 */
void
read_cmd_opt(struct gengetopt_args_info    &args_info,
                  int                           argc,
                  char                          *argv[])
{
    /*Parse options and input*/
    if(cmdline_parser(argc, argv, &args_info) != 0){
        exit(EXIT_FAILURE);
    }

    /*Check for correct number of inputs*/
    if(args_info.inputs_num != NUMBER_OF_INPUTS) {
        cerr    << gengetopt_args_info_usage << endl
                << "Type '" << argv[0] << " -h' for help" << endl;
        exit(EXIT_FAILURE);
    }

    /*Set diagnostics printout*/
    DIAGNOSTICS = !(args_info.silent_flag);
   
}



/**
 * create_disc_tree
 *
 * Creates a discretized species tree from a species tree.
 *
 * @param species_tree The species tree
 * @param DS At return: Pointer to the discretized species tree
 * @param args_info Program arguments
 *
 */
void
create_disc_tree(   Tree &species_tree,
                    EdgeDiscTree **DS,
                    struct gengetopt_args_info &args_info)
{
    float           timestep;   //Timestep for discretized tree
    int             min_intervals; //Minimum number of intervals in DS
    EdgeDiscretizer *disc;//Used for creating discretized trees

    
    timestep = args_info.timestep_arg;
    min_intervals = args_info.min_intervals_arg;
    if (timestep == 0)
    {
        disc = new EquiSplitEdgeDiscretizer(min_intervals);
    }
    else
    {
        disc = new StepSizeEdgeDiscretizer(timestep, min_intervals);
    }
    *DS = disc->getDiscretization(species_tree);
    
    delete disc;
}


/**
 * read_gene_file
 *
 * Reads a gene file. A gene file is assumed to be a text file where each
 * line states the name of a gene found in the gene species map.
 *
 * @param argument Input argument to read.
 * @param g On return, this contains the genes read.
 * @param gs_map Gene to species map.
 * @param args_info program arguments.
 */
void
read_gene_file( int argument,
                vector<string> &g,
                StrStrMap &gs_map,
                struct gengetopt_args_info &args_info)
{
    string curr_line;   //Current line in file.

    /*Open file*/
    ifstream file(args_info.inputs[argument]);

    /*Read gene(s)*/
    if ( file.is_open() ){
        while (! file.eof() )
        {
            //Get next line from file
            curr_line = "";
            getline(file, curr_line);

            //Skip empty lines
            if(curr_line == ""){
                continue;
            }
            if( gs_map.find(curr_line) == "" ){
                cerr << "primeGSROrtho: Inconsistent gene names in <gfile1> and <gs-map>" << endl;
                file.close();
                exit(EXIT_FAILURE);
            }

            g.push_back(curr_line);
        }
    }
    else{
        cerr << "primeOrtho: Error opening file <gfile1>." << endl;
        exit(EXIT_FAILURE);
    }

    /*Close file*/
    file.close();
}
