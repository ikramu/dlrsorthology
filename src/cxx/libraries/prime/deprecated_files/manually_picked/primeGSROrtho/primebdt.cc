/*
 * PrIME-BDT
 *
 * MCMC-chain to calculate Pr[lambda, my, tau | G, S]
 *
 * Author: Peter Andersson (peter9@kth.se) February 2010
 */



/*Includes*/


//Standard libraries
#include <stdlib.h>
#include <iostream>

//Gengetopt
#include "cmdline.h"

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "SeqIO.hh"
#include "StrStrMap.hh"
#include "mcmc/BDTSampler.hh"
#include "TimeEstimator.hh"
#include "LogFilePrinter.hh"
#include "TimeEstimator.hh"

/*Namespaces used*/
using namespace beep;
using namespace std;


/*Definitions and constants*/

//True if progress diagnostics should be printed to stdout
static bool DIAGNOSTICS = true;

//Input arguments
static const unsigned int NUMBER_OF_INPUTS = 3;
static const int INPUT_GENE_TREE_FILE = 0;
static const int INPUT_SPECIES_TREE_FILE = 1;
static const int INPUT_GSMAP_FILE = 2;

//Default MCMC parameters
static const Real DEFAULT_BIRTH_RATE = 1.0f;
static const Real DEFAULT_DEATH_RATE = 1.0f;


//Used for option parsing.
static struct gengetopt_args_info  args_info;

/*Pre-declarations*/
static void read_cmd_opt(   int                         argc,
                            char                        *argv[]);

static void parse_input(Tree &G,
                        Tree &S,
                        StrStrMap &gs_map);

void time_logger(BDTSample *s, unsigned int u);

void setup_log();

/**
 * main
 */
int main(int argc, char *argv[]){

    /*Program variables*/
    Tree      G;        //The gene tree
    Tree      S;        //The species tree.
    StrStrMap gs_map;   //The gene-species map used.

    /*Read command-line options*/
    read_cmd_opt(argc, argv);

    /*Parse trees*/
    parse_input(G, S, gs_map);

    //Init time estimation
    TimeEstimator::instance()->reset(args_info.iterations_arg);

    //Start logging
    setup_log();

    /*Start MCMC chain*/
    BDTSampler sampler(G, S, gs_map, DEFAULT_BIRTH_RATE, DEFAULT_DEATH_RATE);
    string ofile;
    if(args_info.output_file_given){
        ofile = args_info.output_file_arg;
    }
    else{
        ofile = "";
    }
    sampler.iterate(args_info.iterations_arg,
                    args_info.output_file_arg,
                    args_info.thinning_arg,
                    time_logger);

    /*Exit program*/
    exit(EXIT_SUCCESS);
}

void
setup_log()
{
    LogFilePrinter::getLogFilePrinter()->printSetup("Log for primeBDT.");
    stringstream ss;
    int add = args_info.iterations_arg % args_info.thinning_arg == 0 ? 0 : 1;
    ss << "Nr iterations: " << args_info.iterations_arg << endl
        << "Thinning: " << args_info.thinning_arg << endl
        << "Nr samples: " << args_info.iterations_arg / args_info.thinning_arg + add << endl
        << "Output file: " << args_info.output_file_arg;
    LogFilePrinter::getLogFilePrinter()->printMessage(ss.str());
}

void time_logger(BDTSample *s, unsigned int u)
{
    TimeEstimator::instance()->update(u);
    string time = TimeEstimator::instance()->getPrintableEstimatedTimeLeft();
    LogFilePrinter::getLogFilePrinter()->printEntry( time );
    //Need to be cerr because of a strange thing in SimpleMCMC where
    //cout is redirected.
    if(DIAGNOSTICS){cerr << time << endl;}
}

void
parse_input(    Tree &G,
                Tree &S,
                StrStrMap &gs_map)
{
    /*Read gene tree*/
    if(DIAGNOSTICS){cerr << "Reading gene tree.. ";}
    TreeIO tree_reader = TreeIO::fromFile(args_info.inputs[INPUT_GENE_TREE_FILE]);
    G = tree_reader.readNewickTree();
    if(DIAGNOSTICS){cerr << "done!" << endl;}

    /*Read species tree*/
    if(DIAGNOSTICS){cerr << "Reading species tree.. ";}
    tree_reader.setSourceFile(args_info.inputs[INPUT_SPECIES_TREE_FILE]);
    S = tree_reader.readHostTree();
    if(DIAGNOSTICS){cerr << "done!" << endl;}

    /*Read gene-species map*/
    if(DIAGNOSTICS){cerr << "Reading gene-species map.. ";}
    gs_map = tree_reader.readGeneSpeciesInfo(args_info.inputs[INPUT_GSMAP_FILE]);
    if(DIAGNOSTICS){cerr << "done!" << endl;}
}


void
read_cmd_opt(   int     argc,
                char    *argv[])
{
    /*Parse options and input*/
    if(cmdline_parser(argc, argv, &args_info) != 0){
        exit(EXIT_FAILURE);
    }

    /*Check for correct number of inputs*/
    if(args_info.inputs_num != NUMBER_OF_INPUTS) {
        cerr    << gengetopt_args_info_usage << endl
                << "Type '" << argv[0] << " -h' for help" << endl;
        exit(EXIT_FAILURE);
    }

    /*Set diagnostics printout*/
    DIAGNOSTICS = !(args_info.silent_flag);

}