#include "AnError.hh"
#include "BirthDeathMCMC.hh"
#include "IntegralBirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "Hacks.hh"
#include "TopTimeMCMC.hh"
#include "TreeMCMC.hh"
#include "TreeIO.hh"
#include "SimpleML.hh"
#include "SimpleMCMC.hh"
#include "Node.hh"
#include "ReconciliationTimeMCMC.hh"
#include "ReconciliationTimeSampler.hh"

#include "MCMCOutputReader.hh"
// Global options
//-------------------------------------------------------------
int nParams = 4;

char* outfile=NULL;
char* usrMCMCfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 10;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;
bool do_ML = false;

// HostTree related
float specRate = 1.0;
float extiRate = 1.0;
bool fixed_serates = false;
bool fixHostRoot = true;
bool fixHostTree = true;
float htsuggest = 1.0;

// Birth-death process related
bool fixed_bdrates = false;
double birthRate = 1.0;
double deathRate = 1.0;
float bdsuggest = 1.0;
double topTime = -1.0;
bool estimateTopTime = true;
double Beta = -1.0;
bool mustChooseRates = true;
float ttsuggest = 1.0;

// reconciliation related
bool estimate_orthology = false;
bool specprob = false;

bool fixTree = true;
bool fixRoot = true;


// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if (argc < nParams) 
    {
      usage(argv[0]);
      exit(1);
    }
  try
    {
      // tell the user we've started
      //---------------------------------------------------------
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      //---------------------------------------------------------
      // Read input and options
      //---------------------------------------------------------

      // Check for options
      //-----------------------------------
      int opt = readOptions(argc, argv);

      //Get the host tree
      //---------------------------------------------
      string host(argv[opt++]);
      cerr << "host = " << host << endl;
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readGuestTree();  
      if(S.getName() == "Tree")
	{ 
	  S.setName("S");
	}
      cerr << S << endl;

      string guest(argv[opt++]);
      TreeIO io = TreeIO::fromFile(guest);
      StrStrMap gs;
      cerr << "guest )= " <<guest << endl;
      vector<Tree> Gvec = io.readAllGuestTrees(0, &gs); 

      //--------------------------------------------------------
      // Set up all classes
      //--------------------------------------------------------

       //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}
      DummyMCMC dm;
 
      // Set up priors for species tree
      //-------------------------------------------------------
      Tree T = Tree::EmptyTree(1.0);
      T.setName("Dummy");
      StrStrMapDummy dgs(T.getRootNode()->getName());
      BirthDeathMCMC sem(dm, T, specRate, extiRate, !fixed_serates);
      sem.setName("SpecExti");
      sem.multiplySuggestionVariance(bdsuggest);
      GuestTreeMCMC hgtm(sem, S, dgs, sem);
      hgtm.setName(S.getName()+"_"+ T.getName()+"_RecTree");
      if(fixHostRoot)
	{
	  hgtm.fixRoot();
	}
      if(fixHostTree)
	{
	  hgtm.fixTree();
	}
      LambdaMap hl(S, T, dgs);
      GammaMap hgamma = GammaMap::MostParsimonious(S, T, hl);
      {
	ReconciliationTimeSampler tmp(S, sem,hgamma);
	tmp.sampleTimes();
	S.topTime = S.rootToLeafTime();
      }

       ReconciliationTimeMCMC hrtm(hgtm, S, sem, hgamma);
       hrtm.setName(S.getName()+"_EdgeTimes");
       hrtm.multiplySuggestionVariance(bdsuggest);

      // Set up priors for gene tree
      //-------------------------------------------------------

       // Use these vectors to keep track of allocated pointers to
       // OrthologyMCMC, BirthDeathMCMC and TopTimeMCMC
       vector<TopTimeMCMC*>    Tvec;
       vector<BirthDeathMCMC*> Bvec;
       vector<OrthologyMCMC*>  Ovec;

       StdMCMCModel* sm = &hrtm;
       unsigned j = 0;
       for(vector<Tree>::iterator i = Gvec.begin(); 
	   i != Gvec.end(); i++, j++)
	 {
	   if(Beta < 0)
	     {
	       Beta = S.getRootNode()->getNodeTime();
	     }
	   
	   StdMCMCModel* bds;
	   BirthDeathProbs* bdp;
	   if(estimateTopTime || topTime >= 0)
	     {
	       BirthDeathMCMC* bdm;
	       if(topTime >= 0)
		 {
		   S.getRootNode()->changeTime(topTime);
		 }
	       if(estimateTopTime)
		 {
		   TopTimeMCMC* stm = new TopTimeMCMC(*sm, S, Beta); 
		   stm->setName(i->getName()+"_"+S.getName()+"_TopTime");
		   stm->multiplySuggestionVariance(bdsuggest);
		   bdm= new BirthDeathMCMC(*stm, S, birthRate, 
					   deathRate, !fixed_bdrates);
		   bdm->setName(i->getName()+"_"+S.getName()+"_DupLoss");
		   Tvec.push_back(stm);
		   Bvec.push_back(bdm);
		 }
	       else
		 {
		   bdm= new BirthDeathMCMC(*sm, S, birthRate, 
					   deathRate, !fixed_bdrates);
		   bdm->setName(i->getName()+"_"+S.getName()+"_DupLoss");
		   Bvec.push_back(bdm);
		 }
	       bds = bdm;
	       bdp = bdm;
	     }
	   else
	     {
	       throw AnError("I am sorry, but there is a problem using the "
			     "-Bi option currently");
	       IntegralBirthDeathMCMC* bdm = 
		 new IntegralBirthDeathMCMC(*sm, S, birthRate, deathRate, 
					    Beta, !fixed_bdrates);
	       bds = bdm;
	       bdp = bdm;
	     }
	   Tree& G = *i; 
	   if(G.getName() == "Tree")
	     { 
	       ostringstream oss;
	       oss << "G" << j;
	       G.setName(oss.str());
	     }
	   OrthologyMCMC* gtm = new OrthologyMCMC(*bds, G, gs, *bdp);
	   gtm->setName(G.getName()+"_"+S.getName()+"_RecTree");
	   Ovec.push_back(gtm);
	   if (mustChooseRates) 
	     {
	       gtm->chooseStartingRates();
	     }

	   if(fixRoot)
	     {
	       gtm->fixRoot();
	     }
	   gtm->fixTree(); 
	   
	   if(estimate_orthology)
	     {
	       gtm->estimateOrthology(specprob);
	     }
	   sm = gtm;
	 }
      // Set up MCMC handler
      //-------------------------------------------------------
      SimpleMCMC* iterator;
      if(do_ML)
	{
	  iterator = new SimpleML(*sm, Thinning);
	}
      else
	{
	  iterator = new SimpleMCMC(*sm, Thinning);
	}

      if (do_likelihood)
	{
	  cout << sm->currentStateProb() << endl;
	  exit(0);
	}      

      if (outfile != NULL)
	{
	  try 
	    {
	      iterator->setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  

      if (quiet)
	{
	  iterator->setShowDiagnostics(false);
	}
      
      if (!quiet) 
	{
	  cerr << "Start MCMC (Seed = " << rand.getSeed() << ")\n";
	}


      // Copy startup info to outfile (can be used to restart analysis)
      cout << "# Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cout << argv[i] << " ";
	}
      cout << " in directory"
	   << getenv("PWD")
	   << "\n";
      
      //--------------------------------------------------------
      // Now we're set to do stuff
      //---------------------------------------------------------

      time_t t0 = time(0);
      clock_t ct0 = clock();


//       if(usrMCMCfile == 0) // no MCMC
// 	{
	  iterator->iterate(MaxIter, printFactor);
// 	}
//       else
// 	{
// 	  string usrMCMCfile = "tmp.mcmc";
// 	  vector<string> params;
	  
// 	  string semName = sem.getName();
// 	  params.push_back(semName+".birthRate(float)");
// 	  params.push_back(semName+".deathRate(float)");

// 	  string hgtmName = hgtm.getName();
// 	  params.push_back(hgtmName+".S(tree)");

// 	  string hrtmName = hrtm.getName();
// 	  for(unsigned i = 0; i < S.getNumberOfNodes(); i++)
// 	    {
// 	      Node* n = S.getNode(i);
// 	      if(n->isLeaf() == false && n->isRoot() == false)
// 		{
// 		  ostringstream oss;
// 		  oss << hrtmName
// 		      << ".S_nodeTime[" 
// 		      << n->getNumber() 
// 		      << "](float)";
// 		  params.push_back(oss.str());
// 		}
// 	    }

// 	  string cName;
// 	  for(vector<TopTimeMCMC*>::iterator i = Tvec.begin();
// 	      i != Tvec.end(); i++)
// 	    {
// 	      cName = (*i)->getName();
// 	      params.push_back(cName+".S_rootTime(float)");
// 	    }
// 	  for(vector<BirthDeathMCMC*>::iterator i = Bvec.begin();
// 	      i != Bvec.end(); i++)
// 	    {
// 	      cName = (*i)->getName();
// 	      params.push_back(cName+".birthRate(float)");
// 	      params.push_back(cName+".deathRate(float)");
// 	    }

// 	  MCMCOutputReader mor(usrMCMCfile, params); 
// 	  map< string, vector<Real> >& post = mor.getPosterior();

// 	  vector<Real> sRate = post[semName + "birthrate(float)"];
// 	  vector<Real> eRate = post[semName + "deathrate(float)"];

// 	  vector<Tree> hTree = post[hgtmName + "S(tree)"];


// 	  vector< vector<Real> > edgeTimeVec;
// 	  for(unsigned i = 0; i < S.getNumberOfNodes(); i++)
// 	    {
// 	      Node* n = S.getNode(i);
// 	      if(n->isLeaf() == false && n->isRoot() == false)
// 		{
// 		  ostringstream oss;
// 		  oss << hrtmName
// 		      << ".S_nodeTime[" 
// 		      << n->getNumber() 
// 		      << "](float)";
// 		  edgeTimeVec.push_back(post[oss.str()]);
// 		}
// 	    }

// 	  vector< vector<Real> > topTimeVec;
// 	  for(vector<TopTimeMCMC*>::iterator i = Tvec.begin();
// 	      i != Tvec.end(); i++)
// 	    {
// 	      cName = (*i)->getName();
// 	      topTimeVec.push_back(post[cName+".S_rootTime(float)"]);
// 	    }

// 	  vector< vector<Real> > birthVec;
// 	  vector< vector<Real> > deathVec;
// 	  for(vector<BirthDeathMCMC*>::iterator i = Bvec.begin();
// 	      i != Bvec.end(); i++)
// 	    {
// 	      cName = (*i)->getName();
// 	      birthVec.push_back(post[cName+".birthRate(float)"]);
// 	      deathVec.push_back(post[cName+".deathRate(float)"]);
// 	    }

	  
// 	  cout << "# L N "
// 	       << sm->strHeader()
// 	       << endl;
	  
// 	  for(unsigned i = 0; i < sRate.size(); i++)
// 	    {
// 	      sem.setRates(sRate[i], eRate[i]);
// 	      S.partialCopy(hTree[i]);
// 	      for(unsigned j = 0; j < S.getNumberOfNodes(); j++)
// 		{
// 		  Node* n = S.getNode(i);
// 		  if(n->isLeaf() == false && n->isRoot() == false)
// 		    {
// 		      S.times[j] = edgeTimeVec[j][i]
// 		    }
// 		}
// 	      for(unsigned j = 0; j < Tvec.size(); j++)
// 		{
// 		  Tvec[i]->setTopTime(topTimeVec[i][j]);
// 		  Bvec[i]->setRates(birthVec[i][j], deathVec[i][j]);
// 		}

// 	      cout << sm.initStateProb()
// 		     << "\t"
// 		     << i
// 		     << "\t"
// 		     << sm.strRepresentation()
// 		     << "\n";  
// 	    }
	  
	  

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << readableTime(t1 - t0) 
	   << endl
	   << "CPU time: " << readableTime((ct1 - ct0)/CLOCKS_PER_SEC)
	   << endl;
      
      if (!quiet)
	{
	  cerr << sm->getAcceptanceRatio()
	       << " = acceptance ratio   Wall time = "
	       << readableTime(t1-t0)
	       << "\n";
	}
      
      if (sm->getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }

//       std::cerr << "end\n";
//       delete iterator;
//       for(vector<TopTimeMCMC*>::iterator i = Tvec.begin();
// 	  i != Tvec.end(); i++)
// 	{
// 	  delete *i;
// 	}
//       for(vector<BirthDeathMCMC*>::iterator i = Bvec.begin();
// 	  i != Bvec.end(); i++)
// 	{
// 	  delete *i;
// 	}
//       for(vector<OrthologyMCMC*>::iterator i = Ovec.begin();
// 	  i != Ovec.end(); i++)
// 	{
// 	  delete *i;
// 	}
//       std::cerr << "end\n";
    }      
  catch(AnError& e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception& e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }
  return(0);
};


void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <host tree>  <guest tree>\n"
    << "\n"
    << "Parameters:\n"
    << "   <datafile>         Name of file containing sequences in FastA or\n"
    << "                      Genbank format.\n"
    << "   <species tree>     Species tree in Newick format. Branchlengths\n"
    << "                      are important and represent time.\n"
    << "   <gene-species map> Optional. This file contains lines with a\n"
    << "                      gene name in the first column and species\n"
    << "                      name as found in the species tree in the\n"
    << "                      second. You can also choose to associate the \n"
    << "                      genes with species in the gene tree. Please\n"
    << "                      see documentation.\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         Output file\n"
    << "   -u <filename>         Use MCMC output in filename, no MCMC!\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -m                    Do maximum likelihood. No MCMC.\n"
    << "   -w <float>            Write to cerr <float> times less often than\n"
    << "                         to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator.\n"
    << "                         If set to 0 (default), the process id is\n"
    << "                         used as seed.\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -g                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -G<option>            Options related to the gene tree\n"
    << "     -Gr                 reroot gene tree. Default is a fixed root\n"
    << "     -Go                 Estimate orthology. Default is discard \n"
    << "                         orthology data.\n"
    << "     -Gs                 record speciation probabilities (orthology\n" 
    << "                         probabilities will not be recorded)\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth/death rates to these values \n"
    << "     -Bp <float> <float> start values of birth/death rate parameters\n"
    << "     -Bt <float>         Start value for 'top time', the time\n"
    << "                         between first duplication and root of S.\n"
    << "     -Bv <float>         Suggestion variance for birth death changes\n"
    << "     -Bi                 Numerically integrate over 'top time'\n"
    << "     -Bb <float>         The beta parameter for a prior distribution\n"
    << "                         on species root distance\n"
    << "     -Bu <float>         Suggestion variance for top time changes\n"
    << "   -H<option>            Options related to the host tree\n"
    << "     -Hr                 reroot host tree\n"
    << "     -Ht                 swap on host tree\n"
    << "     -Hv <float>         Suggestion variance for host time changes\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    if (opt + 1 < argc)
	      {
		usrMCMCfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-u'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'w':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-w'\n";
		usage(argv[0]);
	      }
	    break;
	  }

	case 's':
	  {
	    if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	      {
		cerr << "Expected integer after option '-s'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'q':
	  {
	    quiet = true;
	    break;
	  }	
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'm':
	  {
	    do_ML = true;
	    break;
	  }	   
	case 'G':
	  {
	    switch(argv[opt][2])
	      {
	      case 'r':
		{
		  fixRoot = false;
		  break;
		}	      
	      case 's':
		{
		  specprob = true;
		  // no break here 
		}
	      case 'o':
		{
		  estimate_orthology = true;
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		  {
		    fixed_bdrates = true;
		    // Don't break here, because we want to fall through to 'r'
		    // set the rates that are arguments both to '-r' and '-f'!
		  }
	      case 'p':
		{
		  mustChooseRates = false;
		  if (++opt < argc) 
		    {
		      birthRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  deathRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'v':
		{
		  if (++opt < argc && atof(argv[opt]) > 0)
		    {
		      bdsuggest = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected a 'suggestion variance'!\n";
		      usage(argv[0]);
		    }
		  break;
		  }
	      case 't':
		{
		  if (++opt < argc)
		    {
		      topTime = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected a 'top time'!\n";
		      usage(argv[0]);
		    }
		  break;
		  }
	      case 'i':
		{
		  estimateTopTime = false;
		  break;
		  }
	      case 'b':
		{
		  if (++opt < argc) 
		    {
		      if (sscanf(argv[opt], "%lf", &Beta) == 0)
			{
			  cerr << "Expected number after option '-b'\n";
			  usage(argv[0]);
			}
		    }
		  break;
		}
	      case 'u':
		{
		  if (++opt < argc && atof(argv[opt]) > 0)
		    {
		      ttsuggest = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected a 'suggestion variance'!\n";
		      usage(argv[0]);
		    }
		  break;
		  }
	      default:
		{
		    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		}
	      }
	      break;
	  }
	case 'H':
	  {
	    switch(argv[opt][2])
	      {
	      case 'r':
		{
		  fixHostRoot = false;
		  break;
		}	      
	      case 't':
		{
		  fixHostTree = false;
		  break;
		}	      
	      case 'v':
		{
		  if (++opt < argc && atof(argv[opt]) > 0)
		    {
		      htsuggest = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected a 'suggestion variance'!\n";
		      usage(argv[0]);
		    }
		  break;
		  }
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  

	

