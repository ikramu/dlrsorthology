#include <cassert>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "GuestTreeModel_no2.hh"
#include "TreeAnalysis.hh"

#include <cmath>

namespace beep
{
  using namespace std;

  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

  GuestTreeModel_no2::GuestTreeModel_no2(Tree &G_in, 
				 StrStrMap &gs_in,
				 BirthDeathProbs &bdp_in)
    : ReconciliationModel(G_in, gs_in, bdp_in),
      S_A(*S, G_in),
      S_X(*S, G_in),
      doneSA(*S, G_in),
      doneSX(*S, G_in),
      orthoNode(0)
  {
    inits();		   // Compute important helper params
  }

  GuestTreeModel_no2::GuestTreeModel_no2(ReconciliationModel& rs)
    : ReconciliationModel(rs),
      S_A(*S, *G),
      S_X(*S, *G),
      doneSA(*S, *G),
      doneSX(*S, *G),
      orthoNode(0)
  {
     inits();		   // Compute important helper params
  }


  GuestTreeModel_no2::GuestTreeModel_no2(const GuestTreeModel_no2 &M)
    : ReconciliationModel(*M.G, *M.gs, *M.bdp),
      S_A(M.S_A),
      S_X(M.S_X),
      doneSA(M.doneSA),
      doneSX(M.doneSX),
      orthoNode(0)
  {
    //  WARNING1("GuestTreeModel_no2 copy constructor used. Probably buggy!");
    inits();
  }

 
  GuestTreeModel_no2::~GuestTreeModel_no2()
  {
    //  delete gamma;
  }


  GuestTreeModel_no2 &
  GuestTreeModel_no2::operator=(const GuestTreeModel_no2 &M)
  {
    if (this != &M)
      {
	ReconciliationModel::operator=(M);
	S_A = M.S_A;
	S_X = M.S_X;
	doneSA = M.doneSA;
	doneSX = M.doneSX;
	orthoNode = 0;
      }
    inits();
    return *this;
  }


  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------

  // Updating stuff when the gene tree has changed /this is cheap so O.K. /bens
  //-------------------------------------------------------------------------
  void
  GuestTreeModel_no2::update()
  {
    ReconciliationModel::update();
    inits();
  }


  //------------------------------------------------------------------------
  // Calculations and computations
  //------------------------------------------------------------------------

  // If u=NULL, then calculateData Probability() returns Pr[G], else if, 
  // e.g., u is the LCA of v and w, calculateDataProbability() returns 
  // Pr[G and v and w are orthologs].
  //------------------------------------------------------------------------
  void 
  GuestTreeModel_no2::setOrthoNode(const Node* u)
  {
    orthoNode = u;
  };
  
  // Since calculateDataProbability often is called both in e.g., 
  // ReconciliationSampler and in, e.g., GeneTreeMCMC, we get redundant 
  // calculations doen in a single MCMC-iteartions. Thus, maybe we should 
  // include a flag showing if the function needs to be recalculated. 
  // I leave it for now, though. /bens
  //------------------------------------------------------------------------
  Probability
  GuestTreeModel_no2::calculateDataProbability()
  {
    // Clear old values - Stupid way of doing it?
    doneSA = doneSX = NodeNodeMap<unsigned>(*S,*G, 0);

    Node& rootS = *S->getRootNode();
    Node& rootG = *G->getRootNode();
    
    // Compute the S_A and S_X structures
    computeSA(rootS,rootG, 0); 
    return S_A(rootS, rootG)[0];


  }

  //-------------------------------------------------------------------
  //
  // I/O
  //
  //-------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const GuestTreeModel_no2& pm)
  {
    return o << pm.print();
  };
  
  string 
  GuestTreeModel_no2::print() const
  {
    std::ostringstream oss;
    oss << "GuestTreeModel_no2: Computes the likelihood of a gene.\n"
	<< "tree given a species tree, by summing over all \n"
	<< "reconciliations.\n"
	<< indentString(G->getName() + " (guest tree)\n");
    return oss.str();
  }
  
  
  // Debugging support macros
  //-------------------------------------------------------------------
#ifdef DEBUG_DP
#define DEBUG_SX(S,X,U,K,F,L) {for(unsigned j = F; j<=L;j++){cerr << S << ":\tS_X(" << X.getNumber() << ", " << U.getNumber() << ")["<< j << "][ " << K << "] = " << S_X(X,U)[j][K-1].val() << endl;}}
#define DEBUG_SA(S,X,U,F,L) {for(unsigned j = F; j<=L; j++){cerr << S << ":\tS_A(" << X.getNumber() << ", " << U.getNumber() << ")[" << j<< "] = " << S_A(X,U)[j].val() << endl;}}

#define DEBUG_S_X(S,X,U,K) {cerr << S << ":\tS_X(" << X->getNumber() << ", " << U->getNumber() << ", " << K <<  ") = " << S_X(X,U)[K-1].val() << endl;}
#define DEBUG_S_A(S,X,U) {cerr << S << ":\tS_A(" << X->getNumber() << ", " << U->getNumber() << ") = " << S_A(X,U).val() << endl;}
#else
#define DEBUG_SX(S,X,U,K,F,L)
#define DEBUG_SA(S,X,U,F,L)

#define DEBUG_S_X(S,X,U,K)
#define DEBUG_S_A(S,X,U)
#endif



  //-----------------------------------------------------------------------
  //
  // Implementation
  //
  //-----------------------------------------------------------------------
  // computes \f$ s^i_V(x,u) \f$
  // for all 0<=i<level 
  // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
  //-----------------------------------------------------------------
  void 
  GuestTreeModel_no2::computeSV(Node& x, Node& u, unsigned level)
  {
    assert(x.dominates(*sigma[u])); //check precondition

    if(sigma[u] == &x)
      {
	if(x.isLeaf())
	  {
	    assert(u.isLeaf());
	    for(unsigned i = doneSX(x,u); i <= level; i++)
	      {
		S_X(x,u)[i][0] = 1.0;
	      }
	  }
	else
	  {
	    Node& v = *u.getLeftChild();
	    Node& w = *u.getRightChild();
	    Node& y = *x.getDominatingChild(sigma[v]);
	    Node& z = *x.getDominatingChild(sigma[w]);
	    
	    assert(y.getSibling() == &z); // Check sanity of recursion
	    computeSA(y,v, level);
	    computeSA(z,w, level);
	    for(unsigned i = doneSX(x,u); i <= level; i++)
	      {
		S_X(x,u)[i][0] = S_A(y,v)[i] * S_A(z,w)[i];
	      }	
	  }
      }
    else
      {
	Node& y = *x.getDominatingChild(sigma[u]);
	Node& z = *y.getSibling();
	computeSA(y,u,level);
	computeSA(z,u,level);
	
	for(unsigned i = doneSX(x,u); i <= level; i++)
	  {
	    //bdp->partialProbOfCopies(z, 0);
	    S_X(x,u)[i][0] = S_A(y,u)[i] * S_A(z,u)[i];  
	  }
      }
    DEBUG_SX("ComputeSX", x, u, 1, doneSX(x,u),level);
  }

  // computes \f$ s_A(x,u) \f$
  //-----------------------------------------------------------------
  void 
  GuestTreeModel_no2::computeSA(Node& x, Node& u, unsigned level)
  {
    assert(doneSA(x,u) <= S_A(x,u).size());

    for(unsigned i = doneSA(x,u); i <= level; i++)
      {
	if(S_A(x,u).size() <= i)
	  {
	    S_A(x,u).push_back(0);
	  }
	else
	  {
	    S_A(x,u)[i] = 0;

	  }
	Probability p = 0;

	// if u \in S_x, then s_A(x,u) = \sum_k\in[|L(G:U)|}Q_x(k)s_X(x,u,k)
	if(x.dominates(*sigma[u]))
	  {
	    computeSX(x,u,level);
	    
	    for(unsigned k = slice_L(x, u); k <= slice_U[u]; k++) 
	      {
		if(x.isRoot())
		  {
		    p += S_X(x,u)[i][k-1] * 
		      pow(bdp->topPartialProbOfCopies(k), std::pow(2.0,int(i)));      
		  }
		else
		  {
		    p += S_X(x,u)[i][k-1] *
		      pow(bdp->partialProbOfCopies(x, k), std::pow(2.0,int(i)));	  
		  }
	      }
	  }
	// if u \not\in S_x, then s_A(x,u) = X_A(x)
	else
	  {
	    p = pow(bdp->partialProbOfCopies(x,0), std::pow(2.0,int(i)));	  
	  }
	
	S_A(x,u)[i] = p;
      }
    DEBUG_SA("ComputeSA", x, u, doneSA(x,u),level);
    doneSA(x,u) = level + 1;
  }
  
  // computes \f$ s_X(x,u,k), k\in[|L(G_u)|] \f$
  // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
  //-----------------------------------------------------------------
  void 
  GuestTreeModel_no2::computeSX(Node& x, Node& u, unsigned level)
  {
    assert(x.dominates(*sigma[u])); // check precondition
    assert(doneSX(x,u) <= S_X(x,u).size());

    unsigned U = slice_U[u];
    unsigned L = slice_L(x, u);
    for(unsigned i = doneSX(x,u); i <= level; i++)
      {
	if(S_X(x,u).size() <= i)
	  {
	    S_X(x,u).push_back(vector<Probability>(U,0));
	  }
	else
	  {
	    S_X(x, u)[i] = vector<Probability>(U,0);
// 	    S_X(x, u)[i].assign(U, 0); // all elements initiated to 0
	  }
      }

    if(L == 1)
      {
	computeSV(x,u,level);
      }
    if(&u != orthoNode)
      {
	for(unsigned k = std::max(2u, L); k <= U; k++) 
	  {	
// 	    Probability factor(1.0 / (k-1));
	    // Loop through all valid solutions to k1 + k2 = k and sum
	    // $S_X(x, left, k1)S_X(x, right, k2)$
	    Node& v = *u.getLeftChild();
	    Node& w = *u.getRightChild();
	    if(isomorphy[u] == false)
	      {
		computeSX(x,v,level);
		computeSX(x,w,level);
	      }
	    else
	      {
		computeSX(x,v,level+1);
		computeSX(x,w,level+1);
	      }
	    unsigned Lv  = slice_L(&x,&v);
	    unsigned Uv  = slice_U[&v];
	    unsigned Lw = slice_L(&x,&w);
	    unsigned Uw = slice_U[&w];
	    assert(Lv > 0);
	    assert(Lw > 0);
	    for(unsigned i = doneSX(x,u); i <= level; i++)
	      {
		Probability sum(0.0);
	    
		for(unsigned k1 = Lv; k1 <= Uv; k1++)
		  {
		    unsigned k2 = k - k1;
		    if (k2 <= Uw && k2 >= Lw)
		      {
			if(isomorphy[u] == false || k1 != k2)  
			  {
			    sum +=  S_X(x,v)[i][k1-1] * S_X(x,w)[i][k2-1]; 
			  }
			else
			  {
			    sum += 0.5 *
			      (S_X(x,v)[i][k1-1] * S_X(x,v)[i][k2-1] + 
			       S_X(x,v)[i+1][k1-1]);// * S_X(x,v)[i+1][k1-1]);
			  }
		      }
		  }
		S_X(x, u)[i][k - 1] = sum / (k-1);
		DEBUG_SX("ComputeSX", x, u, k, doneSX(x,u),level);
	      }
	  }
	doneSX(x,u) = level +1;
      }
  }
  

}// end namespace beep
