#include <iostream>

#include "AnError.hh"
#include "Beep.hh"
#include "GammaRates.hh"


int
main()
{
  try
    {
      using std::cout;
      using std::cerr;

//       cout << "x\tnormal percentage point\n";
//       for(Real X = 0.1; X <1; X+=0.1)
// 	{
// 	  cout <<  X << "\t" << gauinv(X) << "\n";
// 	}
//       cout << "\n\n";


//       cout << "x\tGamma(x)\n";
//       for(Real X = 1; X <=2; X+=0.1)
// 	{
// 	  cout << X << "\t " << exp(loggamma_fn(X)) << "\n";
// 	}
//       cout << "\n\n";

//       cout << "x\talpha\tGamma_in\n";
//       for(Real X = 1e-30; X <=1e-29; X+=1e-30)
// 	{
// 	  for(Real i = 1; i <=10; i+=1.0)
// 	    {
// 	      cout << X << "\t" << i << "\t" << gamma_in(X,i) << "\n";
// 	    }
// 	}
//       cout << "\n\n";

//       cout << "x\tv\tppchi2\n";
//       for(Real X = 0.1; X < 0.9; X += 0.1)
// 	{
// 	  for(Real i = 1.0; i <=5; i+=1.0)
// 	    {
// 	      cerr << X << "\t" << i << "\t" << ppchi2(X,i) << "\n";
// 	    }
// 	}


      cerr << "Test of main function\n";
      cout << "ncat\talpha\trates\n";
      for(unsigned ncat = 4; ncat < 5; ncat++)
	{
	  for(Real alpha = 0.1; alpha <=5; alpha+=1.0)
	    {
	      cout << ncat << "\t" << alpha << "\t" ;
	      
	      std::vector<Real> rates =  calc_gamma_rate(ncat, alpha, alpha);
	      for(std::vector<Real>::const_iterator i = rates.begin(); 
		  i < rates.end(); i++)
		{
		  std::cout << *i << "\t";
		}
	      std::cout << "\n";
	    }
	}




      return 0;
    }
  catch(AnError e)
    {
      e.action();
    }

}

