#include <iostream>

#include "AnError.hh"
#include "DummyMCMC.hh"
#include "GammaDensity.hh"
#include "ConstRateModel.hh"
#include "EdgeRateModel.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "EdgeRateMCMC_common.hh"
#include "SiteRateHandler.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"
#include "VarRateModel.hh"
#include "EdgeWeightHandler.hh"


int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < 5) 
    {
      cerr << "usage : test_EdgeRatemodel <treefile> <model> <density> <mean> <variance>\n"
	   << "<treefile>   must have times\n"
	   << "<model>   'C','I' or 'G'\n"
	   << "<density  'U','G','I' or 'L'\n"
	   << "<mena> <variance>   doubles\n";
      exit(1);
    }

  try
    {
      //Get tree 
      //---------------------------------------------
      TreeIO treeIF = TreeIO::fromFile(string(argv[1]));
      Tree T = treeIF.readHostTree();
      
      Real mean = atof(argv[4]);
      Real variance = atof(argv[5]);

      cout << "Testing constructor\n";
      Density2P* df;
      switch(argv[3][0])
	{
	case 'U':
	  df = new UniformDensity(mean, variance,true);
	  break;
	case'G':
	  df = new GammaDensity(mean, variance);
	  break;
	case 'L':
	  df = new LogNormDensity(mean, variance);
	  break;
	case 'I':
	  df = new InvGaussDensity(mean, variance);
	  break;
	default:
	  cerr << argv[2] << " is not a valid density name\n";
	  exit(1);
	}
      DummyMCMC dm = DummyMCMC::TheEmptyMCMCModel();
      EdgeRateMCMC* b;
      switch(argv[2][0])
	{
	case 'C':
	  {
	    b = new ConstRateMCMC(dm, *df, T);
	    break;
	  }
	case'I':
	  {
	    b = new iidRateMCMC(dm, *df, T);
	    break;
	  }
	case'G':
	  {
	    b = new gbmRateMCMC(dm, *df, T);
	    break;
	  }
	default:
	  {
	    cerr << argv[2] << " is not a valid EdgeRateModel name\n";
	    exit(1);
	  }
	}
      cout << *b;
      cout << b->print();

      cout << "\nTesting generateRates()\n";
      cout  << b->strRepresentation() << "\n";
      b->generateRates();
      cout  << b->strRepresentation() << "\n";

      cout << "Take a look at the tree\n";
      EdgeTimeRateHandler ewh(*b);
      cout << T.print(true,false,true,true);

      cout << "\nTesting suggest/commit/discard\n";
      cout << "Prob iter\t" << b->strHeader() << "\n";
      MCMCObject MOb;
      Probability old = 0;
      for(unsigned i = 0; i < 10; i++)
	{ 
	  MOb =b->suggestNewState();
	  if(MOb.stateProb >= old)
	    {
	      b->commitNewState();
	      old = MOb.stateProb;
	    }
	  else
	    {
	      b->discardNewState();
	    }

	  cout << old.val() << "\t"
	       << b->strRepresentation() << "\n";
	}

      cout << "Acceptance ratio = " << b->getAcceptanceRatio() << "\n";



      cout << "\nTesting copy constructor c(b) and fixMean() "
	   << "using iidRateMCMC\n"
	   << "First create a iidRateMCMC a and do a.suggestNewState()\n";
      iidRateMCMC a(dm, *df, T);
      MOb =a.suggestNewState();
      cout << "MOb iter\t" << a.strHeader() << "\n";
      cout << "(" << MOb.stateProb 
	   << "," << MOb.propRatio << ")\t"
	   << a.strRepresentation() << "\n";


      cout << "\nThen use copyconstructor to create c, fix mean on c and do c.suggestNewState()\n";
      iidRateMCMC c(a);
      c.fixMean();
      
      MOb =c.suggestNewState();
      cout << "MOb iter\t" << c.strHeader() << "\n";
      cout << "(" << MOb.stateProb 
	   << "," << MOb.propRatio << ")\t"
	   << c.strRepresentation() << "\n";
      

      cout << "\nTesting assignment operator d = a, fixVariance() and do d.suggestNewState()\n";
      iidRateMCMC d = a;
      d.fixVariance();
      MOb =d.suggestNewState();
      cout << "(" << MOb.stateProb 
	   << "," << MOb.propRatio << ")\t"
	   << d.strRepresentation() << "\n";
      
      cout << "\nTesting fixRates() on c followed by c.suggestNewState()\n";
      c.fixRates();
      MOb =c.suggestNewState();
      cout << "MOb iter\t" << c.strHeader() << "\n";
      cout << "(" << MOb.stateProb 
	   << "," << MOb.propRatio << ")\t"
	   << c.strRepresentation() << "\n";
      
      cout << "MOb iter\t" << c.strHeader() << "\n";
      cout << "\nFixVariance() on c - c.suggestNewState() should now generate AnError\n";
      c.fixVariance();

      MOb =c.suggestNewState();
      cout << "(" << MOb.stateProb 
	   << "," << MOb.propRatio << ")\t"
	   << c.strRepresentation() << "\n";
      
    }
  catch(AnError& e)
    {
       e.action();
     }
  catch(exception& e)
    {
      cout << e.what();
    }
  return 0;
}
