#include <iostream>
#include <iomanip>


#include "AnError.hh"
#include "ConstRateModel.hh"
#include "EdgeWeightHandler.hh"
#include "GammaDensity.hh"
#include "Node.hh"
#include "TreeIO.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << "<treefile>]\n"
       << "\n"
       << "Parameters:\n"
       << "   <treefile>         a string, \n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 2) 
    {
      usage(argv[0]);
    }

  try
    {
      //Get tree 
      //---------------------------------------------
      TreeIO treeIF = TreeIO::fromFile(string(argv[1]));
      Tree G = treeIF.readHostTree();
      G.perturbedNode(G.getRootNode());
      cout << G.print(true,true,true,true);

      PRNG rand(getpid());
      GammaDensity gd(1.0, 1.0);
      ConstRateModel crm(gd, G, gd.sampleValue(rand.genrand_real1()));
      cout << crm;
      cout << "\nTesting constructors\n"
	   << "--------------------\n";

      cout << "\nEdgeWeightHandler\n"
	   << "--------------------\n";
      EdgeWeightHandler ewh(crm); 
      cout << ewh;
//       cout << G.print(true,true,true,true);

      cout << "\nEdgeTimeRateHandler\n"
	   << "--------------------\n";
      EdgeTimeRateHandler etrh(crm); 
      cout << etrh;
//       cout << G.print(true,true,true,true);

      cout << "\nTesting copy constructor\n";
      cout << "\nEdgeWeightHandler\n"
	   << "--------------------\n";
      EdgeWeightHandler cwh(ewh);
      cout << cwh;

      cout << "\nTesting Assignment\n";
      cout << "\nEdgeTimeRateHandler\n"
	   << "--------------------\n";
      EdgeTimeRateHandler ctrh(etrh);
      cout << ctrh;

      cout << "\nNode\ttime\trate\tewh\tetrh\tr*t\n";
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  Node& n = *G.getNode(i);
	  cout << n.getNumber() << "\t"
	       << std::setprecision(3)
	       << n.getTime() << "\t"
	       << crm.getRate(n) << "\t"
	       << ewh.getWeight(n) << "\t"
	       << etrh.getWeight(n) << "\t"
 	       << n.getTime() * crm.getRate(n)
	       <<"\n";
	}
      cout << "Testimg versions of getWeight(0)\n";
      cout << "class\tgetRate(&)\tgetRate(*)\top[&]\top[*]\n";
      const Node* n = G.getNode(1);
      cout << "ewh\t"
	   << ewh.getWeight(*n)
	   << "\t"
	   << ewh.getWeight(n)
	   << "\t"
	   << ewh[*n]
	   << "\t"
	   << ewh[n]
	   << "\n";

      cout << "etrh\t"
	   << etrh.getWeight(*n)
	   << "\t"
 	   << etrh.getWeight(n)
	   << "\t"
	   << etrh[*n]
	   << "\t"
	   << etrh[n]
	   << "\n";
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
}

