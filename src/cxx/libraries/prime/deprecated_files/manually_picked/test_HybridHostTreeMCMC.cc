#include "HybridHostTreeMCMC.hh"

#include "Node.hh"
#include "AnError.hh"
#include "BeepVector.hh"
#include"DummyMCMC.hh"
#include "HybridTreeIO.hh"

#include <iostream>

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc < 2)
    {
      cerr << "Usage: " 
	   << argv[0]
	   << " <host network> \n";
      exit(0);
    }
  try
    {
      int opt = 1;
      HybridTreeIO io_in = HybridTreeIO::fromFile(string(argv[opt++]));
      vector<HybridTree> Tvec = io_in.readAllHybridTrees(true, false, false, false, 0, 0);
      HybridTree& T = Tvec[atoi(argv[opt++])];
      if(T.getName() == "Tree")
	{
	  T.setName("Host");
	}
      
      cout << "Hybrid Tree:\n" << T.print() << endl;
      cout << "Update binary tree\n";
      cout << T.getBinaryTree();

      PRNG rand;
      if(opt < argc)
	{
	  rand.setSeed(atoi(argv[opt++]));
	}
      cout << "Seed = " << rand.getSeed() << endl;
      
      cout << "First construct a HybridHostTreeMCMC with tree First\n"
	   << "-----------------------------------------------------\n";   
      DummyMCMC dm;
      HybridHostTreeMCMC A(dm, T);
      cout << A;
//       cout << "Then copy construct B from A\n"
// 	   << "-----------------------------------------------------\n";      

//       HybridHostTreeMCMC B(A);
//       cout << B;
    
//       cout << "Change the rates of B so lambda = 1.2, mu = 0.7 and rho = 1.0\n"
// 	   << "-----------------------------------------------------\n";      
//       B.setLambda(1.2);
//       B.setMu(0.7);
//       B.setRho(1.0);
//       cout << B;
      
//       cout << "Lastly assign A to B\n"
// 	   << "-----------------------------------------------------\n";      
//       B = A;
//       cout << B;

      
       Probability P = A.calculateDataProbability();
      
       cout << "P = " << P.val() << endl;

       cout << "\n\nTest hybridSPR()\n";
       
       cout << "Seed = " << rand.getSeed() << endl;

       cout << "Now a short MCMC\n"
	    << "-----------------------------------------------------\n\n"
	    << A.strHeader() << "\n";

       for(unsigned i = 0; i < 10; i++)
	 {
	   Probability Q = A.suggestNewState().stateProb;
	   if(Q > P)
	     {
	       A.commitNewState();
	       P = Q;
	     }
	   else
	     {
	       A.discardNewState();
	     }
	   cout << A.strRepresentation() << "\n";
	 }
    }
  catch(AnError e)
    {
      e.action();
    }
};
