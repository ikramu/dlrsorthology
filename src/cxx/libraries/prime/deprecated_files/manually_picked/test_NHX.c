#include <stdio.h>
#include "NHXtree.h"
#include "NHXnode.h"

int
main(int argc, char** argv) {
  char *treebuf="(a, (b,c));";
  struct NHXtree *t = read_tree_string(treebuf);
  printf("Tree size of '%s': %d\n", treebuf, treeSize(t));
}
