/*
 * testOrtho
 *
 * Program to test the accuracy of orthology probabilities made by the class
 * GSROrthologyEstimator.
 *
 * Author: Peter Andersson (peter9@kth.se) Januari 2010
 *
 *
 *
 * Note: There is a known 'bug' in the code. The calculation of the number
 * number of thresholds may not be consistent with what is written out in the
 * results file. I just circumvented this when writing a script to interpret
 * the results.
 *
 * I also commented away some lines in the class AnError that terminated the
 * program, while I was running this program. This is advisable to any user of
 * this code in order to have the tests not being stopped after 30 hours because
 * of some strange error.
 */




/*Includes*/

//Boost
#include <boost/foreach.hpp>

//Standard libraries
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

//Gengetopt
#include "cmdline.h"

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "SeqIO.hh"
#include "GammaDensity.hh"
#include "SequenceData.hh"
#include "MatrixTransitionHandler.hh"
#include "StrStrMap.hh"
#include "EdgeDiscGSR.hh"
#include "DummyMCMC.hh"
#include "TopTimeMCMC.hh"
#include "BirthDeathMCMC.hh"
#include "SimpleMCMC.hh"
#include "TreeMCMC.hh"
#include "Orthology/GSROrthologyEstimator.hh"
#include "SequenceGenerator.hh"
#include "TimeEstimator.hh"
#include "ConstRateModel.hh"
#include "EdgeRateMCMC_common.hh"
#include "lsd/LSDGeneTreeGenerator.hh"
#include "lsd/LSDTimeProbs.hh"
#include "LogFilePrinter.hh"

#include "mcmc/BDTSampler.hh"


/*=================================Namespaces used==================*/
using namespace beep;
using namespace std;


/*=====================Definitions and constants=======================*/

//To be able to use foreach(...) construct
#define foreach BOOST_FOREACH

//True if progress diagnostics should be printed to stdout
static bool DIAGNOSTICS = true;

//Input arguments
static const unsigned int NUMBER_OF_INPUTS = 4;
static const int INPUT_SEQUENCES_FILE = 0;
static const int INPUT_GENE_TREE_FILE = 1;
static const int INPUT_SPECIES_TREE_FILE = 2;
static const int INPUT_GSMAP_FILE = 3;

//Default MCMC parameters
static const Real DEFAULT_BIRTH_RATE = 1.0;
static const Real DEFAULT_DEATH_RATE = 1.0;
static const Real DEFAULT_MEAN = 1.0;
static const Real DEFAULT_VARIANCE = 0.25;


/*Number of tries to generate a gene tree before re-sampling bdt parameters*/
static const unsigned int NR_TRIES = 40;

/*
 * Simple class used to keep track of collected data for analysis
 */
class TestData{
public:
    TestData(int iTP, int iFN, int iTN, int iFP):TP(iTP), FN(iFN), TN(iTN), FP(iFP){}

    int TP, FN, TN, FP;

    Real getSpecificity(){return (Real)TN/((Real)TN+(Real)FP);}
    Real getSensitivity(){return (Real)TP/((Real)TP+(Real)FN);}
};


/*===========================Globals==========================*/

//Log file manager
static LogFilePrinter *g_logfile;

//Results from tests are written on this stream.
static stringstream g_results;

//For easy input managing
static struct gengetopt_args_info  args_info;



/*======================Pre-declarations====================*/

/**
 * Read input parameters into args_info
 *
 * @param argc Same as for main.
 * @param argv Same as for main.
 */
static void read_cmd_opt(   int     argc,
                            char    *argv[]);

/**
 * Find out if a node is part of a vector of nodes that do not compare
 * pointer values.
 *
 * @param node The node.
 * @param set_of_nodes A vector of nodes.
 */
static bool find(Node *node, vector<Node*> set_of_nodes);

/**
 * Parses some of the input parameters.
 *
 * @param G Experimental gene tree.
 * @param S Experimental species tree.
 * @param gs_map Gene to species map from experiment.
 */
static void parse_input(Tree &G, Tree &S, StrStrMap &gs_map);

/**
 * Creates a discretized species tree.
 *
 * @param species_tree The species tree.
 * @param DS This will point to the discretized species tree on return.
 */
static void create_disc_tree(   Tree &species_tree,
                                EdgeDiscTree **DS);

/**
 * Calculates the specificity and sensitivity of this test.
 *
 * @param G Experimental gene tree.
 * @param S Experimental species tree.
 * @param gs_map Gene to species map for experiment.
 */
static vector< pair<Real, Real> > calc_spec_and_sens(Tree &G, Tree &S, StrStrMap &gs_map);

/**
 * Prints ROC data to an output stream.
 *
 * @param data The ROC data.
 * @param o An output stream.
 */
static void print_ROC_data(vector< pair<Real, Real> > &data, ostream &o);

/**
 * Generates sequences.
 *
 * @param in_geneTree Gene tree to generate sequences in.
 * @param densityFunction Density function used to generate site rates.
 * @param sequenceLength The length of the generated sequences.
 * @param in_substitutionModel The model to generate sequences from.
 * @param in_alpha Shape parameter for site rates.
 * @param in_numberOfCategories Number of intervals on each edge (?).
 * @param in_saveAncestral Decides if sequences for non-leaf vertices should be stored.
 */
static SequenceData generateSequences(  Tree &in_geneTree,
                                        Density2P &densityFunction,
                                        int sequenceLength,
                                        string in_substitutionModel,
                                        float in_alpha,
                                        float in_numberOfCategories,
                                        bool in_saveAncestral);

/**
 * Initiates the log file printing.
 */
static void setup_logfile();

/**
 * Finishes the log file printing.
 */
static void finish_logfile();

static bool generateGeneTree(   Tree &G,
                                StrStrMap &gs_map,
                                vector<Node *> &speciations,
                                vector<Node*> &orthology_nodes,
                                Tree &S,
                                unsigned int num_leaves,
                                Real birth,
                                Real death);

static vector<Probability> calc_speciation_probs(   Tree &Gtrue,
                                                    Tree &S,
                                                    StrStrMap &gstrue_map,
                                                    vector<Node*> &orthology_nodes);

static void gather_data(unsigned int sample,
                        vector<Probability> &ortho,
                        vector<Node*> &orthology_nodes,
                        vector<Node*> &speciations,
                        vector< vector<TestData> > &data);

unsigned int get_number_of_thresholds();

/*
 * A note on threshold steps.
 *
 * Let t1,...,tn be the thresholds, where
 * 0.0 < t1=args_info.threshold_step_arg < t2 = 2*args_info.threshold_step_arg, ..., tn = n*args_info.threshold_step_arg < 1.0
 * If args_info.threshold_step_arg >= 0.5, then n = 1 , otherwise
 * n = floor(1.0/args_info.threshold_step_arg)
 */


/*======================Subroutines and functions======================*/


/**
 * main
 */
int main(int argc, char *argv[]){

    /*Program variables*/
    Tree                        G;              //Experimental data gene tree
    Tree                        S;              //The species tree.
    StrStrMap                   gs_map;         //The gene-species map used.
    vector< pair<Real, Real> >  t_sens;         //Sensitivity and specificity for each threshold

    /*Read command-line options*/
    read_cmd_opt(argc, argv);

    /*Start logging*/
    setup_logfile();

    /*Parse input*/
    if(DIAGNOSTICS){cout << argv[0] << ": Reading input parameters.." << endl;}
    parse_input(G, S, gs_map);
    if(DIAGNOSTICS){cout << argv[0] << ": Done reading input parameters!" << endl;}

    //Print species tree to log
    g_logfile->printMessage("Species Tree: ");
    g_logfile->printMessage(S.print());

    /*Get sensitivity and specificity values for each threshold*/
    if(DIAGNOSTICS){cout << argv[0] << ": Calculating sensitivity and specificity.." << endl;}
    t_sens = calc_spec_and_sens(G, S, gs_map);
    if(DIAGNOSTICS){cout << argv[0] << ": Done calculating sensitivity and specificity!" << endl;}

    /*Print ROC data to file*/
    if(DIAGNOSTICS){cout << argv[0] << ": Printing sensitivity and specificity.." << endl;}
        if(args_info.output_file_given){
            ofstream file(args_info.output_file_arg);
//            print_ROC_data(t_sens, g_results);
            if(DIAGNOSTICS){
                cout << g_results.str() << endl;
            }
            file << g_results.str();
            file.close();
        }
        else{
//            print_ROC_data(t_sens, g_results);
            if(DIAGNOSTICS){
                cout << g_results.str();
            }
        }
    if(DIAGNOSTICS){cout << argv[0] << ": Done printing sensitivity and specificity!" << endl;}

    /*Finish log*/
    finish_logfile();

    /*Exit program*/
    if(DIAGNOSTICS){cout << argv[0] << ": Exiting program." << endl;}
    exit(EXIT_SUCCESS);
}


void
finish_logfile()
{
    g_logfile->printMessage("Program ended on:");
    g_logfile->printDateAndTime();
    g_logfile->printMessage("Result where:");
    g_logfile->printMessage(g_results.str());
}


void
setup_logfile()
{
    g_logfile = LogFilePrinter::getLogFilePrinter();
    g_logfile->printMessage("Log start for test_GSR_ortho.");
    g_logfile->printDateAndTime();
    g_logfile->printRandomSeed();
    stringstream ss;
    for(unsigned int i = 0; i < args_info.inputs_num; i++){
        ss << args_info.inputs[i] << endl;
    }
    ss << "Silent: " << args_info.silent_flag << endl;
    ss << "Output file: " << args_info.output_file_arg << endl;
    ss << "Number of iterations: " << args_info.iterations_arg << endl;
    ss << "Thinning: " << args_info.thinning_arg << endl;
    ss << "Timestep: " << args_info.timestep_arg << endl;
    ss << "Minimum number of intervals on an edge: " << args_info.min_intervals_arg << endl;
    ss << "Burn-in iterations for GSR chains: " << args_info.burn_in_iterations_arg << endl;
    ss << "Burn-in iterations for BDT chain: " << args_info.bdt_burn_in_iterations_arg << endl;
    ss << "Substitution model: " << args_info.substitution_model_arg << endl;
    ss << "Threshold step: " << args_info.threshold_step_arg << endl;
    ss << "Number of samples: " << args_info.nr_samples_arg << endl;
    ss << "Sequence file: " << args_info.inputs[INPUT_SEQUENCES_FILE] << endl;
    ss << "Gene tree file: " << args_info.inputs[INPUT_GENE_TREE_FILE] << endl;
    ss << "Species tree file: " << args_info.inputs[INPUT_SPECIES_TREE_FILE] << endl;
    ss << "Gene to species map file: " << args_info.inputs[INPUT_GSMAP_FILE] << endl;

    g_logfile->printMessage(ss.str());
}




void
print_ROC_data(vector< pair<Real, Real> > &data, ostream &o)
{
    typedef pair<Real,Real> PP;
    o << "# Average specificity and sensitivity" << endl;
    o << "# Threshold, Specificity, Sensitivity" << endl;
    Real threshold = args_info.threshold_step_arg;
    foreach(PP pp, data){
        o << setprecision(4) << threshold << "\t" <<  pp.first << "\t" << pp.second << endl;
        threshold += args_info.threshold_step_arg;
    }
}






void
parse_input(    Tree &G,
                Tree &S,
                StrStrMap &gs_map)
{
    /*Read gene tree*/
    if(DIAGNOSTICS){cout << "Reading gene tree.. ";}
    TreeIO tree_reader = TreeIO::fromFile(args_info.inputs[INPUT_GENE_TREE_FILE]);
    G = tree_reader.readNewickTree();
    if(DIAGNOSTICS){cout << "done!" << endl;}

    /*Read species tree*/
    if(DIAGNOSTICS){cout << "Reading species tree.. ";}
    tree_reader.setSourceFile(args_info.inputs[INPUT_SPECIES_TREE_FILE]);
    S = tree_reader.readHostTree();
    if(DIAGNOSTICS){cout << "done!" << endl;}

    /*Read gene-species map*/
    if(DIAGNOSTICS){cout << "Reading gene-species map.. ";}
    gs_map = tree_reader.readGeneSpeciesInfo(args_info.inputs[INPUT_GSMAP_FILE]);
    if(DIAGNOSTICS){cout << "done!" << endl;}
}


bool
find(Node *node, vector<Node*> set_of_nodes)
{
    foreach(Node *v, set_of_nodes){
        if( v->getNumber() == node->getNumber() ){
            return true;
        }
    }
    return false;
}



vector< pair<Real, Real> >
calc_spec_and_sens(Tree &G, Tree &S, StrStrMap &gs_map)
{
    
    /*Set up birth-death-toptime mcmc (BDT MCMC)*/
    BDTSampler bdt_sampler(G, S, gs_map, DEFAULT_BIRTH_RATE, DEFAULT_DEATH_RATE);
    //Burn-in
    if(DIAGNOSTICS){cout << "BDT MCMC burn-in..." << endl;}
    if(DIAGNOSTICS){
        bdt_sampler.nextStateWithTimeLeft(args_info.bdt_burn_in_iterations_arg, args_info.bdt_burn_in_iterations_arg/10);
    }
    else{
        bdt_sampler.nextState(args_info.bdt_burn_in_iterations_arg);
    }
    if(DIAGNOSTICS){cout << "BDT MCMC burn-in finished." << endl;}

    
    /*Create storage for all collected data*/
    unsigned int nr_thresholds = get_number_of_thresholds();
    vector< vector<TestData> > data(args_info.nr_samples_arg, vector<TestData>(nr_thresholds, TestData(0, 0, 0, 0) ) );

    /*Setup time estimation*/
    TimeEstimator time = TimeEstimator(args_info.nr_samples_arg);

    /*Go through samples*/
    if(DIAGNOSTICS){cout << "Working through samples.." << endl;}
//    Real lambda, my;
    bool redo_sample = false;
    for(int i = 0; i < args_info.nr_samples_arg; i++){
        try{
            stringstream log_message;
            /*Get sample from Pr[lambda, my, tau | G, S]*/
            BDTSample *sample = bdt_sampler.nextState(args_info.thinning_arg);
            S.setTopTime(sample->top_time);
//            cout << "Sample " << i+1 << ", lambda: " << sample->birth_rate << ", my: " << sample->death_rate << ", tau: " << sample->top_time << endl;
            

            

            /*Generate a 'true' tree Gtrue with the same number of leaves as G*/
            if(DIAGNOSTICS){cout << "Generating gene tree.." << endl;}
            Tree Gtrue;
            StrStrMap gstrue_map;
            vector<Node *> speciations;
            vector<Node*> orthology_nodes;
            redo_sample = !generateGeneTree(Gtrue, gstrue_map, speciations, orthology_nodes, S, G.getNumberOfLeaves(), sample->birth_rate, sample->death_rate);
            if(redo_sample){
                redo_sample = false;
                i--;
                continue;
            }

            if(DIAGNOSTICS){cout << "Done generating gene tree!" << endl;}

            /*
             * For each inner vertex in Gtrue:
             *   use GSR to find speciation probability for node
             *   then record the sensitivity and specificity for each threshold.
             */

            /*Calculate specificity and sensitivity for orthology estimation*/
            if(DIAGNOSTICS){cout << "Calculating speciation probabilities.." << endl;}
            vector<Probability> ortho = calc_speciation_probs(Gtrue, S, gstrue_map, orthology_nodes);
            if(DIAGNOSTICS){cout << "Done calculating speciation probabilities!" << endl;}


            /*Collect data*/
            if(DIAGNOSTICS){cout << "Collecting data from sample.." << endl;}
            gather_data(i, ortho, orthology_nodes, speciations, data);
            if(DIAGNOSTICS){cout << "Done collecting data from sample!" << endl;}

            /*Update timer*/
            time.update(1);
            if(DIAGNOSTICS){cout << "Total "<< time.getPrintableEstimatedTimeLeft() << endl;}
            
            /*Update log*/
            log_message << "Sample " << i+1 << ", lambda: " << sample->birth_rate << ", my: " << sample->death_rate << ", tau: " << sample->top_time << endl;
            log_message << "Number of orthology nodes: " << orthology_nodes.size() << endl
                        << "Number of speciations: " << speciations.size() << endl
                        << "Number of duplications: " << orthology_nodes.size() - speciations.size() << endl;
            log_message << time.getPrintableEstimatedTimeLeft();
            g_logfile->printEntry(log_message.str());

        }
        catch(AnError er){
            if(DIAGNOSTICS){cout << er.what() << endl;}
            redo_sample = true;
        }
        if(redo_sample){
            redo_sample = false;
            i--;
            continue;
        }
    }
    if(DIAGNOSTICS){cout << "Done with samples.." << endl;}
    
    /*Calculate the average specificity and sensitivity for each threshold*/
    if(DIAGNOSTICS){cout << "Calculating average specificity and sensitivity.." << endl;}
    g_results << "# Specificity and sensitivity for each sample and threshold" << endl
                << "# Number of samples: " << endl
                << args_info.nr_samples_arg << endl
                << "# nr_thresholds" << endl
                << nr_thresholds << endl
                << "# sample threshold specificity sensitivity" << endl;
    vector< pair<Real, Real> > ret(nr_thresholds, pair<Real, Real>(0.0, 0.0));
    for(int sample = 0; sample < args_info.nr_samples_arg; sample++){
        for(unsigned int step = 0; step < nr_thresholds; step++){
            TestData &td = data[sample][step];
            ret[step].first += td.getSpecificity();
            ret[step].second += td.getSensitivity();
            g_results << setprecision(4) << sample + 1 << "\t" << args_info.threshold_step_arg*(step+1) << "\t" <<
                    td.getSpecificity() << "\t" << td.getSensitivity() << endl;
        }
    }
    for(unsigned int step = 0; step < nr_thresholds; step++){
        ret[step].first /= (Real)args_info.nr_samples_arg;
        ret[step].second /= (Real)args_info.nr_samples_arg;
    }
    if(DIAGNOSTICS){cout << "Done calculating average sensitivity and specificity!" << endl;}

    return ret;

}



unsigned int
get_number_of_thresholds()
{
    static bool calc = true;
    static unsigned int ret;
    if(calc){
        if(args_info.threshold_step_arg >= 0.5){
            ret = 1;
        }
        else{
            Real tmp = 1.0/args_info.threshold_step_arg;
            ret = floor(tmp);
            if(tmp - (Real)ret < 1e-30){
                ret -= 1;
            }
        }
    }
    calc = false;
    return ret;
}



void
gather_data(unsigned int sample, vector<Probability> &ortho, vector<Node*> &orthology_nodes, vector<Node*> &speciations, vector< vector<TestData> > &data)
{
    Real threshold;
    unsigned int step;
    for(threshold = args_info.threshold_step_arg, step = 0; threshold < 1.0; threshold += args_info.threshold_step_arg, step++){
        //Go through all nodes
        for(unsigned int j = 0; j < orthology_nodes.size(); j++){
            //Get speciation probability and against 'true' data
            Node *node = orthology_nodes[j];
            Probability p = ortho[j];
            if( find(node, speciations) ){
                //If node is a speciation:
                //- If p above threshold we have true positive
                //- If p below threshold we have false negative
//                cout << "node " << node->getNumber() << "spec: p: " << p.val() << endl;
                if(p.val() >= threshold)
                    data[sample][step].TP++;
                else
                    data[sample][step].FN++;
            }
            else{
//                cout << "node " << node->getNumber() << "dup: p: " << p.val() << endl;
                //Not a speciation node
                if(p.val() >= threshold)
                    data[sample][step].FP++;
                else
                    data[sample][step].TN++;
            }
        }
    }
}





vector<Probability>
calc_speciation_probs(Tree &Gtrue, Tree &S, StrStrMap &gstrue_map, vector<Node*> &orthology_nodes)
{
    GSROrthologyEstimator::GroupPairVector group_pairs;

    //Generate sequences
    /*Create substitution model*/
    static MatrixTransitionHandler Q = MatrixTransitionHandler(MatrixTransitionHandler::create(args_info.substitution_model_arg));

    /*Get sequence data.*/
    static bool first = true;
    if(first){
        if(DIAGNOSTICS){cout << "Reading sequence data.." << endl;}
    }
    static SequenceData D = SequenceData(
                        SeqIO::readSequences(args_info.inputs[INPUT_SEQUENCES_FILE],
                                            Q.getType()));
    if(first){
        if(DIAGNOSTICS){cout << "Done reading sequence data." << endl;}
        first = false;
    }

    //Create rate density function
    GammaDensity gamma(DEFAULT_MEAN, DEFAULT_VARIANCE);
    SequenceData Dtrue = generateSequences( Gtrue,
                                            gamma,
                                            D.getNumberOfPositions(),
                                            args_info.substitution_model_arg,
                                            1.0,
                                            1.0,
                                            false);

    for(unsigned int n = 0; n < orthology_nodes.size(); n++){
        //Get a node from the 'true' tree
        Node *node = orthology_nodes[n];

        //Find leaves of children
        SetOfNodes left_leaves = node->getLeftChild()->getLeaves();
        SetOfNodes right_leaves = node->getRightChild()->getLeaves();

        //Convert to input to GSROrthologyEstimator
        GSROrthologyEstimator::Group n1, n2;
        GSROrthologyEstimator::GroupPair pair;

        for(unsigned int k = 0; k < left_leaves.size(); k++){
            n1.push_back( left_leaves[k]->getName() );
        }
        for(unsigned int k = 0; k < right_leaves.size(); k++){
            n2.push_back( right_leaves[k]->getName() );
        }
        pair.first = n1;
        pair.second = n2;

        group_pairs.push_back(pair);
    }

    /*Create discretized species tree*/
    EdgeDiscTree *DS;
    create_disc_tree(S, &DS);
    vector<Probability> ortho = GSROrthologyEstimator::estimateOrthologyFromMCMC(
                                            args_info.iterations_arg,
                                            args_info.thinning_arg,
                                            *DS,
                                            gstrue_map,
                                            Q,
                                            Dtrue,
                                            gamma,
                                            DEFAULT_BIRTH_RATE,
                                            DEFAULT_DEATH_RATE,
                                            group_pairs, 
                                            DIAGNOSTICS,
                                            args_info.burn_in_iterations_arg);

    //Delete discretized species tree
    delete DS;
    return ortho;
}












bool
generateGeneTree(   Tree &G,
                    StrStrMap &gs_map,
                    vector<Node *> &speciations,
                    vector<Node*> &orthology_nodes,
                    Tree &S,
                    unsigned int num_leaves,
                    Real birth,
                    Real death)
{
    bool tree_ok = false;
    bool redo_sample = false;
    LSDTimeProbs ltp; //Dummy variable
    LSDGeneTreeGenerator generator(S, birth, death, ltp);
    generator.setMinNumberOfLeaves( num_leaves );
    generator.setMaxNumberOfLeaves( num_leaves );

    unsigned int tries = 0;

    while(!tree_ok && !redo_sample){
        if(tries >= NR_TRIES){
            redo_sample = true;
            break;
        }
        try{
            //Generate tree
            generator.generateTree(G, S.getTopTime()/2);

            //Make sure we have both duplications and speciations in the tree
            speciations = generator.getSpeciations();
            unsigned int inner = G.getNumberOfNodes() - G.getNumberOfLeaves();
            if(speciations.size() == 0 || speciations.size() == inner ){
                ++tries;
                continue;
            }

            //Find orthology nodes
            gs_map = generator.exportGeneSpeciesMap();
            for(unsigned int i = 0; i < G.getNumberOfNodes(); i++){
                Node *n = G.getNode(i);
                if( !(n->isLeaf()) && !(GSROrthologyEstimator::isObligateDuplication(n, S, gs_map)) ){
                    orthology_nodes.push_back(n);
                }
            }

            if(orthology_nodes.size() <= speciations.size()){
                
                orthology_nodes.clear();
                ++tries;
                continue;
            }

            G.doDeleteLengths(); //Get some strange error if lengths are not removed
            tree_ok = true;
        }
        catch(AnError error){
            if(DIAGNOSTICS){cout << error.what();}
            redo_sample = true;
        }
    }
    gs_map = generator.exportGeneSpeciesMap();
    return !redo_sample;
}










SequenceData
generateSequences(Tree &in_geneTree,
                  Density2P &densityFunction,
                  int sequenceLength,
                  string in_substitutionModel,
                  float in_alpha,
                  float in_numberOfCategories,
                  bool in_saveAncestral)
{
    /* Set up substitution model */
    MatrixTransitionHandler substitutionModel = MatrixTransitionHandler::create(in_substitutionModel);

    /* Set up constant rate handler for each nucleotide site (position) */
    ConstRateModel constantRates(densityFunction, in_geneTree);
    SiteRateHandler siteRateGenerator(in_numberOfCategories, constantRates);
    siteRateGenerator.setAlpha(in_alpha);

    /* Set up i.i.d. rate handler for edges in the gene tree */
    DummyMCMC dummy;
    iidRateMCMC edgeRateGenerator(dummy, densityFunction, in_geneTree, "EdgeRates");

    /* Set up handler that updates branch lengths */
    EdgeTimeRateHandler edgeLengthHandler(edgeRateGenerator);

    /* Generate edge rates and update branch lengths */
    edgeRateGenerator.generateRates();

    in_geneTree.setRates(edgeRateGenerator.getWeightVector());
    edgeLengthHandler.update();

    /* Set up pseudo random generator */
    PRNG randomGenerator;

    /* Set up sequence generator and generate sequences */
    SequenceGenerator generator(in_geneTree,
                                substitutionModel,
                                siteRateGenerator,
                                edgeLengthHandler,
                                randomGenerator);

    return generator.generateData(sequenceLength, in_saveAncestral);
}






/**
 * read_cmd_opt
 *
 * Deal with typical command-line option parsing using gengetopt.
 *
 * @param argc NO input parameters (same as for main)
 * @param argv Input arguments (same as for main)
 */
void
read_cmd_opt(   int     argc,
                char    *argv[])
{
    /*Parse options and input*/
    if(cmdline_parser(argc, argv, &args_info) != 0){
        exit(EXIT_FAILURE);
    }

    /*Check for correct number of inputs*/
    if(args_info.inputs_num != NUMBER_OF_INPUTS) {
        cerr    << gengetopt_args_info_usage << endl
                << "Type '" << argv[0] << " -h' for help" << endl;
        exit(EXIT_FAILURE);
    }

    /*Set diagnostics printout*/
    DIAGNOSTICS = !(args_info.silent_flag);
   
}






/**
 * create_disc_tree
 *
 * Creates a discretized species tree from a species tree.
 *
 * @param DS At return: Pointer to the discretized species tree
 * @param args_info Program arguments
 *
 */
void
create_disc_tree(   Tree &species_tree,
                    EdgeDiscTree **DS)
{
    float           timestep;   //Timestep for discretized tree
    int             min_intervals; //Minimum number of intervals in DS
    EdgeDiscretizer *disc;//Used for creating discretized trees

    
    timestep = args_info.timestep_arg;
    min_intervals = args_info.min_intervals_arg;
    if (timestep == 0)
    {
        disc = new EquiSplitEdgeDiscretizer(min_intervals);
    }
    else
    {
        disc = new StepSizeEdgeDiscretizer(timestep, min_intervals);
    }
    *DS = disc->getDiscretization(species_tree);
    
    delete disc;
}
