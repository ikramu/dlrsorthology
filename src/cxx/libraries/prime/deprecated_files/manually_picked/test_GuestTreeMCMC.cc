#include "AnError.hh"
#include "BirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "TreeMCMC.hh"
#include "TreeIO.hh"
#include "SimpleMCMC.hh"


int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc < 6 || argc > 7)
    {
      cerr << "usage: test_GuestTreeModel <Guesttree> <HostTree> <gs> <birthrate> <deathrate> [<seed>]\n";
      exit(1);
    }
  try
    {
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[1]);
      TreeIO io = TreeIO::fromFile(guest);
      Tree G = io.readGuestTree();  // Reads times?
      G.setName("G");
      cout << "G = \n"
	   << G;

      string host(argv[2]);
      cerr << "host =  " << host << endl;
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  // Reads times?
      S.setName("S");
      cout << "S = \n" << S;

      StrStrMap gs;
      gs = TreeIO::readGeneSpeciesInfo(argv[3]);
      cout << gs << endl;
      
      Real brate = atof(argv[4]);
      Real drate = atof(argv[5]);
      Real toptime = atof(argv[6]);

      DummyMCMC dm;
      BirthDeathMCMC bdm(dm, S, brate, drate, &toptime);
      
      cout << "Testing constructors:"
	   << "------------------------------------------\n"
	   << "First construct GuestTreeModel a with G as the tree.\n";
      GuestTreeMCMC a(bdm, G, gs, bdm);
      a.fixRoot();
      a.fixTree();
      SimpleMCMC sm(a);
      sm.setThinning(10);
      sm.iterate(10000, 100);
    }      
  catch(AnError e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }


  return(0);
};
