#include "ReconciliationApproximator.hh"

#include <iostream>
#include <string>
#include <vector>

#include "AnError.hh"
#include "GammaMap.hh"

using namespace std;

ReconciliationApproximator::
ReconciliationApproximator(ReconciliationSampler &rs,
			   Tree &G,
			   unsigned samples)
  : G(G),
    sampler(rs),
    n_gamma_samples(samples),
    orthology(G.getNumberOfNodes(), G.getNumberOfNodes())
{
  // I think the asserts in spa against gamma will suffice here
  if (samples > MAX_N_SAMPLES)
    {
      throw AnError("Dude, you have requested way too many reconciliation samples!");
    }
}


// Copy constuctor
ReconciliationApproximator::ReconciliationApproximator(const ReconciliationApproximator &rsa)
  :  G(rsa.G),
     sampler(rsa.sampler),
     n_gamma_samples(rsa.n_gamma_samples)
{
}

//
// Death and destruction
//
ReconciliationApproximator::~ReconciliationApproximator()
{
  // Hubba
}



// Assignment
ReconciliationApproximator &
ReconciliationApproximator::operator=(const ReconciliationApproximator &rsa)
{
  if (this != &rsa)
    {
      G = rsa.G;
      sampler = rsa.sampler;
      n_gamma_samples = rsa.n_gamma_samples;
    }

  return *this;
}


//-----------------------------------------------------------------------
//
// Accessors
//
//-----------------------------------------------------------------------
ReconciliationSampler& 
ReconciliationApproximator::getReconciliationSampler()
{
  return sampler;
}

//
// Return a string representation of the orthology matrix.
// Two version available:
#ifdef MATLAB_ORTHOLOGS
// Using Matlab/Octave format, but with truncated rows. For instance, 
// instead of 
//    [[ 0 0 0]; [0.5 0 0]; [1 0 0]]
// we write
//    [[ 0]; [0.5 0]; [1 0 0]]
// which is everything up to and including the diagonal element and 
// nothing more.
//
string
ReconciliationApproximator::strOrthology()
{
  int size = orthology.nrows();
  string s;
  s.reserve(size * (size + 1) * 5); // Allocate some space

  s = "[";
  for (int row=0; row < size; row++)
    {
      s += "[";
      for (int col=0; col < row; col++) 
	{
	  ostringstream buf;
	  buf << orthology(row,col).val() << " ";
	  s += buf.str();
	}
      s += " 0];";
    }
  s += "]";
  return s;
}
#else
//
// A list of pairs. Format: 
// orthology := '[' + ortholist + ']'
// ortholist := empty
//            | orthopair + ' ' + ortholist
// orthopair := '[' + id1 + ',' + id2 + '] = ' + probability
//

string
ReconciliationApproximator::strOrthology()
{
  int size = orthology.nrows();
  string s;
  s.reserve(size * (size + 1) * 5); // Allocate some space

  s = "[";
  for (int row=0; row < size; row++)
    {
      Node *v1 = G.getNode(row);
      for (int col=0; col < row; col++) 
	{
	  //	  cerr << "O(" << row<<", "<<col<<")=" << orthology(row,col)<<endl;
	  if (orthology(row, col) > 0.0)
	    {
	      Node *v2 = G.getNode(col);
	      ostringstream buf;
	      buf << orthology(row,col).val() << " ";
	      s += '[' 
		+  v1->getName() 
		+  ',' 
		+  v2->getName()
		+  "]="
		+  buf.str();
	    }
	}
    }
  s += "]";
  return s;
}

#endif

void
ReconciliationApproximator::resetOrthology()
{
  // Reset the orthology matrix. We only use the lower half
  for (int i = 0; i < orthology.nrows(); i++) {
    for (int j = 0; j < i; j++) {
      orthology(i, j) = 0.0;
    }
  }
}



void
ReconciliationApproximator::recordOrthology(const GammaMap &gamma, const Probability p)
{
  multimap<int, int> ortho_list = gamma.getOrthology();
  for(multimap<int,int>::iterator iter = ortho_list.begin();
      iter != ortho_list.end();
      iter++) 
    {
      if ((*iter).first > (*iter).second) // Remember: Only lower half
	{
	  orthology((*iter).first, (*iter).second) += p;
	}
      else
	{
	  orthology((*iter).second, (*iter).first) += p;
	}
    }
}


//-------------------------------------------------------------------
//
// ProbabilityModel interface
//
// There are two way of doing this currently. 
// 1. Old style. Get some reconciliations, then compute their likelihoods
//    by sampling each n_gamma_samples (or similar). If we have many 
//    recociliations, this is expensive. But accurate I guess.
// 2. New style. Sample reconciliations and compute a likelihood from 
//    *one* time sample. Full control over the number of samples and, as 
//    Jens said, Hoefding works anyway. 
//
Probability
ReconciliationApproximator::calculateDataProbability()
{
// #ifdef ONLY_ONE_TIMESAMPLE
  return simple_calculateDataProbability(); // Type 2 call
// #else
//   return cached_calculateDataProbability(); // Type 1 call
// #endif
}

//
// For when G has changed
//
void
ReconciliationApproximator::update()
{
//   spa.update();
  sampler.update();
}



// Probability
// ReconciliationApproximator::cached_calculateDataProbability()
// {
//   Probability sum(0.0);
//   typedef map<GammaMap,Probability> GammaCacheType;
//   GammaCacheType cache;

//   resetOrthology();

//   for (unsigned i = 0; i < n_gamma_samples; i++)
//     {
//       // Get yourself a reconciliation
//       GammaMap gamma = sampler.getSample();
//       //GammaMap gamma = sampler.gamma_star;

//       // Behold the power of STL: We are caching the gammas to see if 
//       // we have already computed a mean for it.
//       GammaCacheType::iterator cached_item = cache.find(gamma);

//       Probability term;
//       if (cached_item == cache.end()) // See if it is in the cache. If not,...
// 	{
// 	  // tell our friends about it...
// 	  spa.setGamma(&gamma);

// 	  // ... and use it to approximate $\Pr(\tau | \gamma, G, etc.)$.
// 	  term = spa.calculateDataProbability();
// 	  //	  term = 1.0;
// 	  cache[gamma] = term;
// 	}
//       else			// Already computed this one: pull out the old result
// 	{
// 	  term = (*cached_item).second;
// 	}
//       sum += term;

//       // Add support for various orthology pairs
//       recordOrthology(gamma, term);

//     }

//   //  cerr << "#reconciliations: " << cache.size() << endl;

//   scale(orthology, 1.0 / sum);
//   return sum / n_gamma_samples;
// }



Probability
ReconciliationApproximator::simple_calculateDataProbability()
{
  Probability sum(0.0);

  resetOrthology();

  for (unsigned i = 0; i < n_gamma_samples; i++)
    {
      // Get yourself a reconciliation
      GammaMap gamma = sampler.getSample();

      // tell our friends about it...
      //      spa.setGamma(&gamma);

      // ... and use it to approximate $\Pr(\tau | \gamma, G, etc.)$.
      //      Probability term = spa.calculateDataProbability();
      Probability term = 1.0;

      sum += term;

      // Add support for various orthology pairs
      recordOrthology(gamma, term);

    }

  scale(orthology, 1.0 / sum);
  return sum / n_gamma_samples;
}



