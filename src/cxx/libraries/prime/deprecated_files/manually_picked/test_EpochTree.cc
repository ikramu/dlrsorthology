#include "AnError.hh"
#include "Beep.hh"
#include "BeepOption.hh"
#include "EpochTree.hh"
#include "TreeIO.hh"

static const int NO_OF_PARAMS = 2;

std::string ParamTreeFile = "";   // Param 1: Name of file containing tree S.
beep::Real ParamToptime = 1.0;    // Param 2: Top time.

/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
	using namespace beep;
	using namespace beep::option;
	using namespace std;

	try
	{
		int argIndex = 1;
		if (argc - argIndex != NO_OF_PARAMS)
		{
			throw beep::AnError("Wrong number of input parameters!");
		}
		ParamTreeFile = argv[argIndex++];
		if (!BeepOptionMap::toDouble(argv[argIndex++], ParamToptime))
		{
			throw beep::AnError("Invalid toptime value!");
		}
		
		// Create S from file.
		Tree S(TreeIO::fromFile(ParamTreeFile).readHostTree());
		S.setTopTime(ParamToptime);
		
		// Create discretized trees using different approaches.
		Real approxTimestep = 0.1;
		EpochTree ES1(S, 4, approxTimestep);
		EpochTree ES2(S, 4);
		
		// Print info.
		ES1.debugInfo();
		ES2.debugInfo();
	}
	catch (AnError& e)
	{
		e.action();
		cout << "Usage: " << argv[0] << " <tree file> <toptime>" << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;;
		cout << "Usage: " << argv[0] << " <tree file> <toptime>" << endl;
	}
};
