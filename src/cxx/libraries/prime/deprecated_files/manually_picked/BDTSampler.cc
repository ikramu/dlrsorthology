/* 
 * File:   BDTSampler.cc
 * Author: peter9
 * 
 * Created on February 2, 2010, 12:42 PM
 */

#include "BDTSampler.hh"

namespace beep{

BDTSampler::BDTSampler( const Tree G,
                        const Tree S,
                        const StrStrMap gs_map,
                        const Real start_birth,
                        const Real start_death):
                        MCMCSampler<BDTSample>(),
                        m_G(G),
                        m_S(S),
                        m_gs_map(gs_map)
                        
{
    setup(start_birth, start_death);
}

void
BDTSampler::setup(Real birth, Real death)
{
    //Create dummy
    m_mcmcDummy = new DummyMCMC();
    
    //Create beta
    Real beta = m_S.getRootNode()->getNodeTime();

    //Create top time MCMC
    m_mcmcTopTime = new TopTimeMCMC(*m_mcmcDummy, m_S, beta);

    //Create birth death MCMC
    m_mcmcBD = new BirthDeathMCMC(*m_mcmcTopTime, m_S, birth, death, &(m_mcmcTopTime->getTopTime()) );

    //Create guest tree MCMC
    m_mcmcGuest = new GuestTreeMCMC(*m_mcmcBD, m_G, m_gs_map, *m_mcmcBD);

    //Set rates and fix gene tree
    m_mcmcGuest->chooseStartingRates();
    m_mcmcGuest->fixRoot();
    m_mcmcGuest->fixTree();

    //Initialize sampler
    m_state = new BDTSample();
    updateState();
    m_mcmc_obj = new SimpleMCMC(*m_mcmcGuest, 1);
    initialize(m_state, m_mcmc_obj);
}

/**
 * updateState
 * 
 * Updates the BDTSample
 */
void BDTSampler::updateState()
{
    m_state->birth_rate = m_mcmcBD->getBirthRate();
    m_state->death_rate = m_mcmcBD->getDeathRate();
    m_state->top_time = m_mcmcTopTime->getTopTime();
}

BDTSampler::~BDTSampler()
{
    delete m_mcmcBD;
    delete m_mcmcDummy;
    delete m_mcmcGuest;
    delete m_mcmcTopTime;
    delete m_mcmc_obj;
    delete m_state;
}


BDTSample *
BDTSampler::nextState(int jumps)
{
    // Advance chain if we should do at least one jump
    if(jumps > 0) {
        m_mcmc_obj->advance(jumps);
        updateState();
        return m_state;
    }
    else {
        return m_state;
    }
}


BDTSample *
BDTSampler::nextState(int jumps,
                      int callbackInterval,
                      StateProcesser callback)
{
    // The number of blocks of callbackInterval to iterate
    unsigned int blockIterations = jumps / callbackInterval;

    // The number of iterations left
    unsigned int rest = jumps % callbackInterval;

    // Perform the block iterations
    for(unsigned int i = 0; i < blockIterations; i++) {
        nextState(callbackInterval);
        updateState();
        callback(m_state, callbackInterval);
    }

    // Some iterations are left perform them
    if(rest > 0) {
        nextState(rest);
        updateState();
        callback(m_state, rest);

    }
    return m_state;
}


void
BDTSampler::iterate(int jumps,
                    string output_file,
                    int callbackInterval,
                    StateProcesser callback)
{
    // The number of blocks of callbackInterval to iterate
    unsigned int blockIterations = jumps / callbackInterval;

    // The number of iterations left
    unsigned int rest = jumps % callbackInterval;

    // Perform the block iterations
    if(blockIterations > 1 || rest != 0){
        m_mcmc_obj->setLastIterate(false);
    }
    bool first = true;
    for(unsigned int i = 0; i < blockIterations; i++) {
        if(first){
            m_mcmc_obj->setFirstIterate(true);
            iterate(callbackInterval, output_file);
            m_mcmc_obj->setFirstIterate(false);
            first = false;
        }
        else{
            if(i == blockIterations - 1 && rest == 0){
                m_mcmc_obj->setLastIterate(true);
            }
            iterate(callbackInterval, "");
        }
        callback(m_state, callbackInterval);
    }

    // Some iterations are left perform them
    if(rest > 0) {
        if(first){
            iterate(callbackInterval, output_file);
        }
        else{
            m_mcmc_obj->setLastIterate(true);
            iterate(callbackInterval, "");
        }
        callback(m_state, rest);

    }

}

void
BDTSampler::iterateWithTimeEstimation(int jumps, string output_file, int printInterval)
{
    iterate(jumps, output_file, printInterval, MCMCSampler<BDTSample>::printTimeLeft);
}


void
BDTSampler::iterate(unsigned int iterations, string output_file, bool err_print)
{
    m_mcmc_obj->setShowDiagnostics(err_print);
    if(output_file != ""){
        m_mcmc_obj->setOutputFile(output_file.c_str());
    }
    m_mcmc_obj->iterate(iterations, 1);
    updateState();
}

};//Beep