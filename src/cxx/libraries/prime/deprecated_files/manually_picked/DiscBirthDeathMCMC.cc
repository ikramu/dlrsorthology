#include "DiscBirthDeathMCMC.hh"
#include "DiscTree.hh"
#include "MCMCObject.hh"

namespace beep
{
  //----------------------------------------------------------------------
  //
  // Formal defs of static const members
  // I haven't found where this is stated in the standard, but
  // apparently we now need to do this? /bens
  //
  //----------------------------------------------------------------------
  const Real DiscBirthDeathMCMC::MAX_INTENSITY;

  using namespace std;

  DiscBirthDeathMCMC::DiscBirthDeathMCMC(MCMCModel& prior, DiscBirthDeathProbs& BDProbs,
					 const Real& suggestRatio) :
    StdMCMCModel(prior, 2, BDProbs.getTreeName()+"_DupLoss", suggestRatio),
    m_BDProbs(BDProbs),
    m_fixRates(false),
    m_suggestionVariance(0.1 * (BDProbs.getBirthRate() + 
				BDProbs.getDeathRate()) / 2.0),
    m_oldBirthRate(),
    m_oldDeathRate(),
    m_oldBD_const(BDProbs.getTree()),
    m_oldBD_zero(BDProbs.getTree()),
    m_oldPt(),
    m_oldUt()      
  {
    for (unsigned i=0; i<m_oldBD_const.size(); ++i)
      {
	m_oldBD_const[i] = NULL;
      }
  }


  DiscBirthDeathMCMC::~DiscBirthDeathMCMC()
  {
    // Note: Cached pointer values should always be NULL at destruction.
  }


  void 
  DiscBirthDeathMCMC::fixRates()
  {
    m_fixRates = true;
    n_params = 0;
    updateParamIdx();
  }


  void 
  DiscBirthDeathMCMC::multiplySuggestionVariance(Real multiplier)
  {
    m_suggestionVariance = multiplier *
      (m_BDProbs.getBirthRate() + m_BDProbs.getDeathRate()) / 2.0;
  }


  MCMCObject 
  DiscBirthDeathMCMC::suggestOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = m_BDProbs.setPertNotificationStatus(false);
	
    // Cache old values directly from members (yes, yes, ugly...). 
    m_oldBirthRate = m_BDProbs.m_birthRate;
    m_oldDeathRate = m_BDProbs.m_deathRate;
    for (unsigned i=0; i<m_oldBD_const.size(); ++i)
      {
	vector<Probability>* vec = m_BDProbs.m_BD_const[i];
	m_oldBD_const[i] = new vector<Probability>();
	m_oldBD_const[i]->assign(vec->begin(), vec->end());
	m_oldBD_zero[i] = m_BDProbs.m_BD_zero[i];	
      }
    m_oldPt = m_BDProbs.m_Pt;
    m_oldUt = m_BDProbs.m_ut;
	
    // Choose what parameter to perturb using 'paramIdx'.
    MCMCObject mcmcObj(1.0, 1.0);
    Real idx = paramIdx / paramIdxRatio;
	
    // Calculate upper limit of the allowed rate.
    // TODO: Would it make more sense to include top time as well?
    Real t = m_BDProbs.getRootToLeafTime();
    if (t == 0)
      {
	t = m_BDProbs.getTopTime();
      }
    Real maxRate = (t != 0) ? (MAX_INTENSITY / t) : MAX_INTENSITY;

    // Equal chance of perturbing birth/death rate.
    // Update m_BDProbs when changing its rates.
    if (idx > 0.5)
      {
	// Perturb birth rate.
	m_BDProbs.setRates(
			   perturbLogNormal(m_BDProbs.getBirthRate(), 
					    m_suggestionVariance,
					    Real_limits::min(), maxRate, 
					    mcmcObj.propRatio),
			   m_BDProbs.getDeathRate());
      }
    else
      {
	// Perturb death rate.
	m_BDProbs.setRates(
			   m_BDProbs.getBirthRate(),
			   perturbLogNormal(m_BDProbs.getDeathRate(), 
					    m_suggestionVariance,
					    Real_limits::min(), maxRate, 
					    mcmcObj.propRatio));
      }
	
    // Notify listeners.
    m_BDProbs.setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::PERTURBATION);
    m_BDProbs.notifyPertObservers(&pe);
	
    return mcmcObj;
  }


  void 
  DiscBirthDeathMCMC::commitOwnState()
  {
    // Delete cached pointer values.
    for (unsigned i=0; i<m_oldBD_const.size(); ++i)
      {
	delete (m_oldBD_const[i]);
	m_oldBD_const[i] = NULL;
      }
  }


  void 
  DiscBirthDeathMCMC::discardOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = m_BDProbs.setPertNotificationStatus(false);
	
    // Restore cached values directly (yes, yes, ugly...).
    m_BDProbs.m_birthRate = m_oldBirthRate;
    m_BDProbs.m_deathRate = m_oldDeathRate;
    for (unsigned i=0; i<m_oldBD_const.size(); ++i)
      {
	assert(m_oldBD_const[i] != NULL);
	delete (m_BDProbs.m_BD_const[i]);
	m_BDProbs.m_BD_const[i] = m_oldBD_const[i];
	m_oldBD_const[i] = NULL;  // m_BDProbs is now owner of pointed-to data.
	m_BDProbs.m_BD_zero[i] = m_oldBD_zero[i];
      }
    m_BDProbs.m_Pt = m_oldPt;
    m_BDProbs.m_ut = m_oldUt;
    m_BDProbs.m_base_BD_const.assign(1, Probability(1.0));
    m_BDProbs.m_base_BD_zero = Probability(0.0);
    assert(m_BDProbs.m_birthRate > 0.0);
    assert(m_BDProbs.m_deathRate > 0.0);
	
    // Notify listeners.
    m_BDProbs.setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::RESTORATION);
    m_BDProbs.notifyPertObservers(&pe);
  }


  string 
  DiscBirthDeathMCMC::ownStrRep() const
  {
    ostringstream oss;
    if (!m_fixRates)
      {
	oss << m_BDProbs.getBirthRate() 
	    << ";\t" 
	    << m_BDProbs.getDeathRate()
	    << ";\t";
      }
    return oss.str();
  }


  string 
  DiscBirthDeathMCMC::ownHeader() const
  {
    ostringstream oss;
    if (!m_fixRates)
      {
	oss << "birthRate(float);\tdeathRate(float);\t";
      }
    return oss.str();
  }

  void 
  DiscBirthDeathMCMC::updateToExternalPerturb(Real newLambda, Real newMu)
  {
    if(newLambda != m_BDProbs.getBirthRate() ||
       newMu != m_BDProbs.getDeathRate())
      {
	// Turn off notifications just to be sure.
	bool notifStat = m_BDProbs.setPertNotificationStatus(false);

	// Do the update      
	m_BDProbs.setRates(newLambda, newMu);

	// Notify listeners.
	m_BDProbs.setPertNotificationStatus(notifStat);
	PerturbationEvent pe(PerturbationEvent::PERTURBATION);
	m_BDProbs.notifyPertObservers(&pe);
      }
  }

  Probability 
  DiscBirthDeathMCMC::updateDataProbability()
  {
    // This MCMC has no prior of its own.
    // Note: m_BDProbs should already be up-to-date.
    return 1.0;
  }


  ostream& 
  operator<<(ostream &o, const DiscBirthDeathMCMC& A)
  {
    return (o << A.print());
  }


  string 
  DiscBirthDeathMCMC::print() const
  {
    ostringstream oss;
    oss << name << ": Birth and death parameters ";
    if (!m_fixRates)
      {
	oss << "are estimated during MCMC.\n";
      }
    else
      {
	oss << "are fixed to " 
	    << m_BDProbs.getBirthRate()
	    << " and " 
	    << m_BDProbs.getDeathRate()	
	    << ", respectively.\n";
      }
    oss << StdMCMCModel::print();
    return oss.str();
  }

} //end namespace beep

