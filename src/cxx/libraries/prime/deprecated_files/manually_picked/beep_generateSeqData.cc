#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "AnError.hh"
#include "Beep.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeWeightMCMC.hh"
#include "LengthRateMCMC.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LengthRateModel.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "PRNG.hh" 
#include "SequenceData.hh"
#include "SequenceGenerator.hh"
#include "SequenceType.hh"
#include "SiteRateHandler.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"

using std::vector;

// Global options with default settings
//------------------------------------
int nParams = 2;

double alpha = 1.0;
unsigned n_cats = 1;
// double pinvar = 0.0;

bool useWeights = false;
bool onlyWeights = false;

double mean = 0.5;
double variance = 0.1;

std::string density = "UNIFORM";

std::string rateModel = "CONST";

std::string seqModel = "JC69";
bool generateRates = true;

char* outfile = 0;   //NULL;
char* t90file = 0;  //NULL

// allowed models
std::vector<std::string> subst_models;
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// output to true file
bool outputTree = false;
bool outputSubst = true;
bool outputRates = true;
bool outputNodeTimes = true;
bool outputBD = true;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv)
{
  using namespace beep;
  using namespace std;
  
  // Set up allowed models
  //-------------------------------------------------------------
  //rate model
  subst_models.push_back("UNIFORMAA");
  subst_models.push_back("JTT");
  subst_models.push_back("UNIFORMCODON");
  subst_models.push_back("ARVECODON");
  subst_models.push_back("JC69");
  
  //rate model
  rate_models.push_back("IID");
  rate_models.push_back("GBM");
  rate_models.push_back("CONST");
  
  //rate model
  rate_densities.push_back("UNIFORM");
  rate_densities.push_back("GAMMA");
  rate_densities.push_back("LOGN");
  rate_densities.push_back("INVG");

  if (argc < nParams+1) 
    {
      usage(argv[0]);
    }
  try
    {
      // Tell user we started 
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory: ";
      //      system("pwd");
      cerr << "\n\n";
      
      // Read options
      //---------------------------------------------
      int opt = readOptions(argc, argv);
      // Read arguments
      //---------------------------------------------
      if (opt >= argc)
	{
	  cerr << "Expected treefile argument\n";
	  usage(argv[0]);
	}
      string treefile(argv[opt++]);

      if (opt >= argc)
	{
	  cerr << "Expected number of character argument\n";
	  usage(argv[0]);
	}
      unsigned nchar = atoi(argv[opt]);
     
      // Get the model tree
      //---------------------------------------------
      TreeIO io = TreeIO::fromFile(treefile);
      TreeIOTraits traits;
      io.checkTagsForTree(traits);
      if(traits.hasNT())
	{
	  traits.setNT(generateRates);
	}
      else if(traits.hasET() || traits.hasNW())
	{
	  traits.setET(generateRates);
	  traits.setNWisET(generateRates);
	}
      else
	{
	  assert(generateRates == false);
	}
      traits.setBL(false);
//       traits.setBL(!generateRates);
      Tree G = io.readBeepTree(traits, 0,0);
//       Tree G = io.readBeepTree(0,0);
// generateRates, false, !generateRates, 
// 				false, 0, 0);
//       Tree G = io.readGuestTree();
      G.setName("G");
      cerr << G.print();

      // create the random number generator to use
      PRNG rand;
      
      // Set up the SubstitutionMatrix
      //---------------------------------------------------------
      MatrixTransitionHandler Q = MatrixTransitionHandler::create(seqModel);

      // Set up Density function for rates
      //---------------------------------------------------------
      Density2P* df;
      capitalize(density);
      if(density == "INVG")
	{
	  df = new InvGaussDensity(mean, variance);
	}
      else if(density == "LOGN")
	{
	  df = new LogNormDensity(mean, variance);
	}
      else if(density == "GAMMA")
	{
	  df = new GammaDensity(mean, variance);
	}
      else// (density == "UNIFORM")
	{
	  df = new UniformDensity(mean, variance, true);
	}
      // Test for sanenss of mean and variance!
      Real min, max;
      df->getRange(min, max);
      if(min < 0)
	{
	  cerr << "The mean and variance given for density yields\n"
	       << "yields a partially negative range. Please correct\n";
	  exit(1);
	}
      
       // Set up rates and EdgeWeightHandler
      //---------------------------------------------------------
      DummyMCMC dm;
      EdgeRateMCMC* erm;
      EdgeWeightModel* ewm;
      if(generateRates == false)
	{
	  ewm = new LengthRateModel(*df, G);
// 	  erm = new EdgeWeightMCMC(dm, *lrm);
// 	  throw AnError("Currently not possible to generate lengths directly",1);
 	  erm = new iidRateMCMC(dm, *df, G, "EdgeRates");
	}
      else if(rateModel == "CONST")
	{
	  ewm = erm = new ConstRateMCMC(dm, *df, G, "EdgeRates");
	}
      else if(rateModel == "IID")
	{
	  ewm = erm = new iidRateMCMC(dm, *df, G, "EdgeRates");
	}
      else //(rateModel == "GBM")
	{
	  ewm = erm = new gbmRateMCMC(dm, *df, G, "EdgeRates");
	}
      
      if(generateRates)
	{
	  G.setRates(erm->getWeightVector());
	}

      //generate Rates
      if(generateRates || onlyWeights)
	{
	  erm->generateRates(); 
	}

      EdgeWeightHandler* ewh;
      if(useWeights || onlyWeights)
	{
	  ewh = new EdgeWeightHandler(*ewm);
	}
      else if(!generateRates)
	{
	  ewh = new EdgeWeightHandler(*ewm);
	}
      else
	{
	  ewh = new EdgeTimeRateHandler(*erm);
	}
      if(onlyWeights)
	{
	  cerr << G.print(true, true, true,true);
	  TreeIOTraits traits;
	  traits.setID(true);
	  traits.setBL(true);
	  traits.setNW(true);
	  cout << TreeIO::writeBeepTree(G, traits, 0) << endl;//true, false, false, true, true, 0) << endl;
	  exit(0);
	}
      // Set up SiteRates
      //---------------------------------------------------------
      UniformDensity uf(0, 100, true);
      ConstRateMCMC alphaC(dm, uf, G, "Alpha");
      SiteRateHandler srh(n_cats, alphaC);
      srh.setAlpha(alpha);

      ewh->update();

      // Set up SequenceGenerator
      //---------------------------------------------------------
      //cerr << "here A\n";      
      SequenceGenerator SG(G, Q, srh, *ewh, rand);
      //cerr << "here B\n";
      //cout << SG; 
      //cerr << "here C\n";
      
      // Generate Sequence Data
      //---------------------------------------------------------
     SequenceData D = SG.generateData(nchar);

      // Output Data
      //---------------------------------------------------------
      cout << endl
	   << nchar 
	   << " characters were generated";

      if (outfile != NULL)
	{
	  ofstream data(outfile);
	  data << D.data4fasta() << "\n";
	  cout <<" and written to file "
	       << outfile
	       << endl;
	} 
      else
	{ 
	  cout << ":\n"
	       << D.data4fasta();
	}
      if(t90file != 0)
	{
	  cout << "'True' parameters for 90% test is written to file "
	       << t90file
	       << "\n"
	    ;
	  ofstream t90((string(t90file)).data());

	  ostringstream t90header;
	  ostringstream t90data;
	  TreeIO io_out;

	  t90header << "# True parameters input for 90% test\n"
		    << "# T N\t";
	  t90data << "0 0\t";
	  if(outputSubst && n_cats != 1)
	    {
	      t90header << alphaC.strHeader();
	      t90data << alphaC.strRepresentation();
	    }
	  if(outputRates)
	    {
	      t90header << erm->strHeader();
	      t90data << erm->strRepresentation();
	    }
	  if(outputTree)
	    {
	      t90header << G.getName() << "(tree);\t";
	      t90data << io_out.writeGuestTree(G) + ";\t";
	    }
	  if(outputNodeTimes)
	    {
	      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
		{
		  Node& n = *G.getNode(i);
		  if(!(n.isLeaf()||n.isRoot()))
		    {
		      t90header << "EdgeTimes.nodeTime[" 
				<< i << "](float); ";
		      t90data << n.getNodeTime() << "; ";
		    }
		}
	    }
	  t90header << "\n";
	  t90data << "\n";
	  t90 << t90header.str() << t90data.str();
	  delete erm;
	  delete df;
	  delete ewh;
	}
    }
  catch (AnError e)
    {
      cerr << "error found\n";

      e.action();
    }
}


void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <treefile> <nchar>\n"
    << "\n"
    << "Generates either:\n"
    << "1) edge lengths and sequence data on the input tree, \n"
    << "2) sequence data from a tree with given edge lengths, or\n"
    << "2) only edge lengths.\n"
    << "Edge lengths can be achieved either by a) by generating edge rates \n"
    << "from a model of rate variation over the tree and using the divergence\n"
    << "times to achieve the edge lengths, or b) directly generating\n"
    << "the edge lengths from the 'rate model'.\n"
    << "In 1) and 2) a user-specified substitution model is used to generate\n"
    << "sequences.\n"
    << "The '-T' options allows saving generation parameters (recommended)\n" 
    << "\n"
    << "Parameters:\n"
    << "   <treefile>         name of file with tree to generated data on.\n"
    << "   <nchar>            an int, the number of characters to generate\n"


    << "Options:\n"
    << "   -u, -h             This text.\n"
    << "   -o <filename>      output file\n"
    << "   -T<option>         Options for the output of true parameters\n"
    << "                      used in the generation (off by default)\n"
    << "     -To <filename>   Outputs 'true' parameters to file <filename>,\n"
    << "                      by default outputs all parameters.\n"
    << "     -Tg              Do not output the tree and edge times used for\n"
    << "                      data generation\n"
    << "     -Tr              Do not output (substitution) rates (including\n"
    << "                      the mean and variance of the rate model).\n"
    << "     -Ts              Do not output Substitution Model parameters (inkl.\n"
    << "                      parameters for site rate model)\n"
    << "   -S<option>         Options related to the Substitution model.\n"
    << "     -Sm <'JC69'/'UniformAA'/'JTT'/'UniformCodon'/'ArveCodon'>\n"
    << "                      The substitution model to use, (JC69 is the \n"
    << "                      default). Must fit data type \n"
    << "     -Sa <float>      alpha, the shape parameter of the Gamma \n"
    << "                      distribution modeling site rates (default: 1)\n"
    << "     -Sn <float>      nCats, the number of discrete rate categories.\n"
    << "                      (default is one category)\n"
    //     << "   -p <float>         probability of a site being invarant\n"
    << "   -G<option>         Options related to the tree.\n"
    << "     -Gl              Generate edge lengths only, no sequence data produced\n"
    << "     -Gu              Use edge lengths in tree for generating seqs,\n"
    << "   -E<option>         Options relating to model of substitution rates\n"
    << "                      over edges in the tree\n"
    << "     -Em <'iid'/'gbm'/'const'>\n"
    << "                      the edge rate model to use, (const is default)\n"
    << "     -Ep <float> <float>\n"
    << "                      mean and variance of edge rate model\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'> \n"
    << "                      the density function to use for edge rates, \n"
    << "                      (Gamma is the default) \n"
    << "     -El              Generate edge-lengths directly from rate model.\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{ //General options
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'T':
	  { // 'true' options
	    switch (argv[opt][2]) 
	      { // suboptions
	      case 'o':
		{
		  if (opt + 1 < argc)
		    {
		      t90file = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected filename after option '-To'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'g':
		{
		  outputTree = false;
		  break;
		}
	      case 'r':
		{
		  outputRates = false;
		  break;
		}
	      case 's':
		{
		  outputSubst = false;
		  break;
		}
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	  // Substitution model options
	  //-------------------------------------------------------
	case 'S':
	  { // Substitution model options
	    switch (argv[opt][2]) 
	      { // suboptions
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {	
		      seqModel = argv[++opt];
		      capitalize(seqModel);
		      if(find(subst_models.begin(), subst_models.end(), seqModel) 
			 == subst_models.end())
			{
			  cerr << "Expected 'UniformAA', 'JTT', 'UniformCodon' "
			       << "or 'JC69' for option -Sm\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!

			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-Sm'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      alpha = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Expected float (shape parameter for site rate\n"
			   << "variation) for option '-Sa'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc) 
		    {
		      n_cats = atoi(argv[opt]);
		    }
		  else
		    {
		    std::cerr << "Expected int (number of site rate classes) "
			      << "for option 'Sn'\n";
		    usage(argv[0]);
		    }
		  break;
		}
		// 	    case 'p':
		// 	      	{
		// 		  if (++opt < argc) 
		// 		    {
		// 		      pinv = atof(argv[opt]);
		// 		    }
		// 		  else
		// 		    {
		// 		      cerr << "Expected float (probability of a site being\n"
		// 			   << "invariant) for option '-p'!\n";
		// 		      usage(argv[0]);
		// 		    }
		// 		  break;
		// 		}
	      default:
		{	
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	case 'G':
	  { // Substitution model options
	    switch (argv[opt][2]) 
	      { // suboptions
	      case 'l':
		{
		  onlyWeights = true;
		  break;
		}
	      case 'u':
		{
		  generateRates = false;
		  break;
		}
	      default:
		{	
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	case 'E':
	  { // edge rate model options
	    switch (argv[opt][2]) 
	      { // suboptions
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      capitalize(rateModel);
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
		    variance = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Expected float (variance for edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance for\n"
			   << "edge rates) for option '-Ep'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		      capitalize(density);
		      if(find(rate_densities.begin(), rate_densities.end(), 
			      density) == rate_densities.end())
			{
			  cerr << "Expected 'InvG', 'LogN', 'Gamma' or "
			       << "'Uniform'for option -Ed\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected density after option '-Ed'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'l':
		{
		  useWeights = true;
		  break;
		}
	      default:
		{	
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	default:
	  {	
	    cerr << "option " << argv[opt] << " not recognized\n";
	    usage(argv[0]);
	    break;
	  }
	}
      opt++;
    }
  return opt;
}
