#include "AnError.hh"
#include "Beep.hh"
#include "BeepOption.hh"
#include "TreeDiscretizer.hh"
#include "TreeIO.hh"

static const int NO_OF_PARAMS = 2;

std::string ParamTreeFile = "";   // Param 1: Name of file containing tree S.
beep::Real ParamToptime = 1.0;    // Param 2: Top time.

/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
	using namespace beep;
	using namespace beep::option;
	using namespace std;

	try
	{
		int argIndex = 1;
		if (argc - argIndex != NO_OF_PARAMS)
		{
			throw beep::AnError("Wrong number of input parameters!");
		}
		ParamTreeFile = argv[argIndex++];
		if (!BeepOptionMap::toDouble(argv[argIndex++], ParamToptime))
		{
			throw beep::AnError("Invalid toptime value!");
		}
		
		// Create S from file.
		Tree S(TreeIO::fromFile(ParamTreeFile).readHostTree());
		S.setTopTime(ParamToptime);
		
		// Create discretized trees using different approaches.
		Real approxTimestep = S.rootToLeafTime() / 50;
		TreeDiscretizerOld DS_1(S, approxTimestep, 4);
		TreeDiscretizerOld DS_2(S, 4);
		
		// Print info.
		DS_1.debugInfo(true);
		DS_2.debugInfo(true);
	}
	catch (AnError& e)
	{
		e.action();
		cout << "Usage: " << argv[0] << " <tree file> <toptime>" << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;;
		cout << "Usage: " << argv[0] << " <tree file> <toptime>" << endl;
	}
};
