#include "AnError.hh"
#include "Beep.hh"
#include "BeepOption.hh"
#include "BirthDeathProbs.hh"
#include "DummyMCMC.hh"
#include "EdgeDiscBDMCMC.hh"
#include "EdgeDiscBDProbs.hh"
#include "EdgeDiscGSR.hh"
#include "EdgeDiscPtMapIterator.hh"
#include "EdgeDiscTree.hh"
#include "GammaDensity.hh"
#include "MCMCObject.hh"
#include "StrStrMap.hh"
#include "TreeDiscretizers.hh"
#include "TreeIO.hh"

static const int NO_OF_PARAMS = 4;

std::string ParamSFile = "";
beep::Real  ParamToptime = 0.0;
std::string ParamGFile = "";
std::string ParamGSMapFile = "";



/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
	using namespace beep;
	using namespace beep::option;
	using namespace std;

	try
	{
		int argIndex = 1;
		if (argc - argIndex != NO_OF_PARAMS)
			throw AnError("Wrong number of input parameters!");
		
		// Read parameters.
		ParamSFile = argv[argIndex++];
		if (!BeepOptionMap::toDouble(argv[argIndex++], ParamToptime))
			throw AnError("Invalid top time value!");
		ParamGFile = argv[argIndex++];
		ParamGSMapFile = argv[argIndex++];

		cout << "**** Creating discretized host tree. ****" << endl;
		Tree S(TreeIO::fromFile(ParamSFile).readHostTree());
		S.setTopTime(ParamToptime);
		EquiSplitEdgeDiscretizer disc(3, 3);
		EdgeDiscTree* DS = disc.getDiscretization(S);
		
		cout << "**** Printing discretized host tree. ****" << endl;
		cout << (*DS) << endl;
		
		cout << "**** Creating guest tree. ****" << endl;
		StrStrMap gsMap(TreeIO::readGeneSpeciesInfo(ParamGSMapFile));
		TreeIO tio = TreeIO::fromFile(ParamGFile);
		Tree G = Tree(tio.readBeepTree(NULL, &gsMap));
				
		cout << "**** Creating birth/death probs. holder with rates 0.6/0.4. ****" << endl;
		EdgeDiscBDProbs bdProbs(*DS, 0.6, 0.4);
		
		cout << "Creating birth-death MCMC perturbator." << endl;
		DummyMCMC dummyMCMC = DummyMCMC();
		Real suggVar = 0.5;
		EdgeDiscBDMCMC bdMCMC(dummyMCMC, bdProbs, suggVar);
		
		cout << "**** Creating edge rate Gamma function with mean/variance 0.5/0.2. ****" << endl;
		GammaDensity rateDF(0.5, 0.2);
		
		cout << "**** Creating discretized GSR model. ****" << endl;
		EdgeDiscGSR gsr(G, *DS, gsMap, rateDF, bdProbs, NULL);
		
		cout << "**** Printing reconciliation info. ****" << endl;
		cout << gsr.getDebugInfo() << endl;
		
		cout << "**** Printing initial probs. ****" << endl;
		cout << gsr.getDebugInfo(true, true) << endl;
		
		cout << "**** Perturbing probs. ****" << endl;
		bdMCMC.suggestOwnState();
		
		cout << "**** Printing probs (now with birth/death rates " << bdProbs.getBirthRate()
			<< "/" << bdProbs.getDeathRate() << "). ****" << endl;
		cout << gsr.getDebugInfo(true, true) << endl;
		
		cout << "**** Restoring probs. ****" << endl;
		bdMCMC.discardOwnState();
		
		cout << "**** Printing probs. ****" << endl;
		cout << gsr.getDebugInfo(true, true) << endl;
		
		delete DS;
	}
	catch (AnError& e)
	{
		e.action();
		cout << "Usage: " << argv[0] << " <host tree file> <top time> <guest tree file> <guest-host map file>" << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
		cout << "Usage: " << argv[0] << " <host tree file> <top time> <guest tree file> <guest-host map file>" << endl;
	}
	cout << endl;
};
