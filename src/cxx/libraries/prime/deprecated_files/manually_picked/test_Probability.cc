#include "Probability.hh"
#include "Beep.hh"
#include "AnError.hh"

int main()
{
  using namespace beep;
  using namespace std;
  Probability p1;
  Probability p2(2);
  Probability p3 = -3;
  cerr << "*************************\n"
       << "TESTING PROBABILITY CLASS\n"
       << "*************************\n"
       <<endl;

  cerr << "Constructors\n"
       << "p1 = " << p1.val() << endl
       << "p2 = " << p2.val() << endl
       << "p3 = " << p3.val() << endl
       << endl;

  cerr << "-p2 = " << -p2.val() << endl;

  cerr << endl;
  cerr << endl;
  cerr << "Arithmetics w Probabilities\n";
  cerr << "-----------\n";
  cerr << endl;

  cerr << "+ with self\n";
  cerr << "(p1 + p1) = " << (p1+p1).val() << endl;
  cerr << "(p2 + p2) = " << (p2+p2).val() << endl;
  cerr << "(p3 + p3) = " << (p3+p3).val() << endl;
  cerr << endl;

  cerr << "- with self\n";
  cerr << "(p1 - p1) = " << (p1-p1).val() << endl;
  cerr << "(p2 - p2) = " << (p2-p2).val() << endl;
  cerr << "(p3 - p3) = " << (p3-p3).val() << endl;
  cerr << endl;

  cerr << "* with self\n";
  cerr << "(p1 * p1) = " << (p1*p1).val() << endl;
  cerr << "(p2 * p2) = " << (p2*p2).val() << endl;
  cerr << "(p3 * p3) = " << (p3*p3).val() << endl;
  cerr << endl;

  cerr << "/ with self\n";
  try 
    {
      cerr << "(p1 / p1) = " << (p1/p1).val() << endl;
    }
  catch (AnError e)
    {
      cerr << "\nError:\n" << e.what() << endl;;
    }
  cerr << "(p2 / p2) = " << (p2/p2).val() << endl;
  cerr << "(p3 / p3) = " << (p3/p3).val() << endl;
  cerr << endl;

  cerr << "+ and - with pos and zero\n";
  cerr << "(p1 + p2) = " << (p1+p2).val() << endl;
  cerr << "(p1 - p2) = " << (p1-p2).val() << endl;
  cerr << "(p2 + p1) = " << (p2+p1).val() << endl;
  cerr << "(p2 - p1) = " << (p2-p1).val() << endl;
  cerr << endl;

  cerr << "+ and - with pos and neg\n";
  cerr << "(p2 + p3) = " << (p2+p3).val() << endl;
  cerr << "(p2 - p3) = " << (p2-p3).val() << endl;
  cerr << "(p3 + p2) = " << (p3+p2).val() << endl;
  cerr << "(p3 - p2) = " << (p3-p2).val() << endl;
  cerr << endl;

  cerr << "+ and - with neg and zero\n";
  cerr << "(p1 + p3) = " << (p1+p3).val() << endl;
  cerr << "(p1 - p3) = " << (p1-p3).val() << endl;
  cerr << "(p3 + p1) = " << (p3+p1).val() << endl;
  cerr << "(p3 - p1) = " << (p3-p1).val() << endl;
  cerr << endl;

  cerr << "* and / with pos and zero\n";
  cerr << "(p1 * p2) = " << (p1*p2).val() << endl;
  cerr << "(p1 / p2) = " << (p1/p2).val() << endl;
  cerr << "(p2 * p1) = " << (p2*p1).val() << endl;
  try 
    {
      cerr << "(p2 / p1) = " << (p2/p1).val() << endl;
    }
  catch(AnError e)
  {
    cerr << "\nError:\n" << e.what();
    cerr << "(p2 = 2) = " << (p2=2).val() << endl;
  }
  cerr << endl;

  cerr << "* and / with pos and neg\n";
  cerr << "(p2 * p3) = " << (p2*p3).val() << endl;
  cerr << "(p2 / p3) = " << (p2/p3).val() << endl;
  cerr << "(p3 * p2) = " << (p3*p2).val() << endl;
  cerr << "(p3 / p2) = " << (p3/p2).val() << endl;
  cerr << endl;

  cerr << "* and / with neg and zero\n";
  cerr << "(p1 * p3) = " << (p1*p3).val() << endl;
  cerr << "(p1 / p3) = " << (p1/p3).val() << endl;
  cerr << "(p3 * p1) = " << (p3*p1).val() << endl;
  try 
    {
      cerr << "(p3 / p1) = " << (p3/p1).val() << endl;
    }
  catch (AnError e)
  {
    cerr << "\nError:\n" << e.what();
    cerr << "(p3 = -3) = " << (p3=-3).val() << endl;    
  }
  cerr << endl;

  cerr << endl;
  cerr << "Arithmetics w doubles\n";
  cerr << "-----------\n";
  cerr << endl;

  cerr << "(p2 + 3) = " << (p2+3).val() << endl;
  cerr << "(p2 - 10) = " << (p2-10).val() << endl;
  cerr << "(p2 * 2) = " << (p2*2).val() << endl;
  cerr << "(p2 / -1) = " << (p2/-1).val() << endl;

  cerr << endl;

  cerr << "(p2 += 10) = " << (p2+=10).val() << endl;
  cerr << "(p2 += p3) = " << (p2+=p3).val() << endl;
  cerr << "(p2 += p2) = " << (p2+=p2).val() << endl;

  cerr << endl;
	      
 
  cerr << "4! = " << probFact(4).val() << endl;

  cerr << "pow("<< Probability(0) << ",0) = " << pow(Probability(0),0) << endl;   
}
    
