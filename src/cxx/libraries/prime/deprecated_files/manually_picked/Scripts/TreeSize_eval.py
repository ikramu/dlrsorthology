#! /usr/bin/python

import sys
import string
import re

def readData(filename):
    file=open(filename, 'r')  
    currline=file.readline()
    datalist=[]
    while currline!='':
        if currline=='\n': #SKIP EMPTY LINES!
            currline=file.readline()
            continue
        datalist.append(extractData(currline))
        currline=file.readline()
    file.close
    if datalist==[]:
        print 'file is not an mcmc outfile\n'
        sys.exit(0)
    return datalist


            
def extractData(currline):
    dataVector=[]
    j=0
    extract=0
    currattribute=''
    for n in range(len(currline)):
        if re.match("\s", currline[n]) == None:
            extract=1
        if extract==1:
            if re.match("\s", currline[n]):
                extract=0
                j=j+1
                if j>2:
                    print 'Error: tooMany arguments\n'
                    sys.exit(0)
                dataVector.append(float(currattribute))
                currattribute=''
            else:
                currattribute=currattribute+currline[n]
    return dataVector
                    
                    
                    
#main body of program
print
print '============================================================='
print
print 'Evaluation of treeSizes'
print
print '============================================================='
print

data=readData(sys.argv[1])

geneMean = 0.0
specMean = 0.0
gene_specMean = 0.0
geneMax = 0.0
specMax = 0.0
gene_specMax = 0.0
geneMin = 2000.0
specMin = 2000.0
gene_specMin = 2000.0

max = len(data)
for i in range(max):
    geneMean = geneMean + data[i][0]
    specMean = specMean + data[i][1]
    gene_specMean = gene_specMean + (data[i][0] / data[i][1])

    if geneMax < data[i][0]:
        geneMax = data[i][0]
    if specMax < data[i][1]:
        specMax = data[i][1]
    if gene_specMax < (data[i][0] / data[i][1]):
        gene_specMax = (data[i][0] / data[i][1])
        
    if geneMin > data[i][0]:
        geneMin = data[i][0]
    if specMin > data[i][1]:
        specMin = data[i][1]
    if gene_specMin > (data[i][0] / data[i][1]):
        gene_specMin = (data[i][0] / data[i][1])

            
print 'Mean value of TreeSizes'
print '--------------------------------------------------------------'
print string.rjust('gene',5), string.rjust('spec',15), string.rjust('gene/spec', 25)
print '%5f %15f %25f' % (geneMean / max, specMean / max, gene_specMean / max)
print
print 'Max value of TreeSizes'
print '--------------------------------------------------------------'
print string.rjust('gene',5), string.rjust('spec',15), string.rjust('gene/spec', 25)
print '%5f %15f %25f' % (geneMax, specMax, gene_specMax)
print
print 'Min value of TreeSizes'
print '--------------------------------------------------------------'
print string.rjust('gene',5), string.rjust('spec',15), string.rjust('gene/spec', 25)
print '%5f %15f %25f' % (geneMin, specMin, gene_specMin)

