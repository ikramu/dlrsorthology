#!/usr/bin/python

import getopt
import os
import sys

# Valid commands.
CMDS = ['GR', 'GRPLOT', 'G', 'GPLOT', 'RL', 'HW', 'A', 'APLOT', 'C', 'CPLOT', \
'ESS', 'S', 'V', 'DPLOT', 'TPLOT', 'PLOT']


def usage():
    print \
"""===============================================================================
Reads one or several parallel PrIME mcmc chains and constructs an R script.
The created script will produce coda-package mcmc objects corresponding to the
chains named m0, m1,... as well as an mcmc.list object collecting them.

There are two main ways of invoking the created R script:
1) Default: not invoked. One may start it manually, e.g. interactively in R
   by running 'source("<outfile>")'.
2) Executing it immediately (using R's batch mode) by specifying option -e.
   Output is stored in <outfile>.sink and plots in <outfile>.pdf.
   R with coda package must be installed for this to work.

Note: Before the R script is created, coda-compatible chain files are created
with aid of external script mcmc_analysis. These files are stored in the same
folder as the original ones, albeit with '.coda' appended to the filenames.
This step may be skipped by specifying option -c.

Usage:
   ./CodaAnalysis [options] <chain1> [<chain2> ...]
where options are:
   -h           shows this help message.
   -o <outfile> sets filename of output. Defaults to 'convdiag.r'.
   -b <burn-in> sets the burn-in ratio. No effect in conjunction with -c.         
   -c           input already on coda format, i.e. perform no conversion.
   -e           executes produced R script. Mostly useful in conjunction
                with -a.
   -a <cmd>     appends analysis/plotting commands to the R script.
                <cmd> is a comma-separated list (no spaces) of the following:
       GR       for Gelman-Rubin convergence diagnostic.
       GRPLOT   for Gelman-Rubin-Brooks plot.
       G        for Geweke convergence diagnostic.
       GPLOT    for Geweke-Brooks plot.
       RL       for Raftery-Lewis pilot run diagnostic.
       HW       for Heidelberger-Welch convergence diagnostic.
       A        for autocorrelation diagnostic
       APLOT    for autocorrelation plot.
       C        for crosscorrelation diagnostic.
       CPLOT    for crosscorrelation plot.
       ESS      for effective sample size.
       S        for summary statistics.
       V        for list of variable (parameter) names.
       DPLOT    for density plot.
       TPLOT    for trace plot.
       PLOT     for summary plot with both trace and density.
==============================================================================="""


def CodaAnalysis(rfiles, outfile, cmds, doBatch):
    
    outfilesink = "%s.sink" % outfile
    outfilepdf = "%s.pdf" % outfile
    
    # Construct R file with appropriate commands.
    f = open(outfile, 'w')
    
    # If batch mode, specify plot file.
    if doBatch:
        f.write('pdf(\"%s\")\n' % outfilepdf)
    
    # Produce mcmc objects for the chains, and a list.
    f.write('library(coda)\n')
    tstr = ''
    mstr = ''
    ml = []
    for i in range(len(rfiles)):
        tstr += ('t%s=read.table(\"%s\"); ' % (i, rfiles[i]))
        mstr += ('m%s=mcmc(t%s); ' % (i, i))
        ml.append('m%s' % i)
    f.write(tstr + '\n')
    f.write(mstr + '\n')
    f.write('ml=mcmc.list(%s);\n' % ','.join(ml))
    
    # Get rid of N parameter, etc.
    f.write(
"""
is.linear <- rep(FALSE, nvar(ml))
for (i in 1:nchain(ml)) {
   for (j in 1:nvar(ml)) {
      lm.out <- lm(as.matrix(ml[[i]])[, j] ~ time(ml))
      if (identical(all.equal(sd(residuals(lm.out)), 0), TRUE)) {
         is.linear[j] <- TRUE
      }
   }
}
if (any(is.linear)) {
   cat("*********************************************\\n")
   cat("Dropping the following variables, which are  \\n")
   cat("linear functions of the iteration number:    \\n")
   print(varnames(ml)[is.linear])
   cat("*********************************************\\n")
   inset <- varnames(ml)[!is.linear]
   ml <- ml[, inset, drop = FALSE]
}
cat("Checking effective sample size ...")
warn.small <- FALSE
for (i in 1:length(ml)) {
    ess <- effectiveSize(ml[i])
    if (any(ess < 200)) {
        warn.small <- TRUE
    }
}
if (warn.small) {
    cat("\\n")
    cat("*********************************************\\n")
    cat("WARNING !!!                                  \\n")
    cat("Some variables have an effective sample size \\n")
    cat("of less than 200 in at least one chain. This \\n")
    cat("This is too small, and may cause errors in   \\n")
    cat("the diagnostic tests.                        \\n")
    cat("                                             \\n")
    cat("HINTS: Look at plots first to identify       \\n")
    cat("variables with slow mixing. Re-run your chain\\n")
    cat("with a larger sample size and thinning       \\n")
    cat("interval. If possible, reparameterize your   \\n")
    cat("model to improve mixing.                     \\n")
    cat("You may also run 'effectiveSize(ml[i])' to   \\n")
    cat("inspect the ESS for chain i.                 \\n")
    cat("*********************************************\\n")
} else {
   cat("OK!\\n")
}
\n"""
    )
    
    # Add commands if desired.
    for cmd in cmds:
        if cmd == 'GR':
            f.write('gelman.diag(ml, 0.95, TRUE, TRUE)\n')
        elif cmd == 'GRPLOT':
            f.write('gelman.plot(ml, 10, 50, 0.95, TRUE, TRUE)\n')
        elif cmd == 'G':
            f.write('geweke.diag(ml)\n')
        elif cmd == 'GPLOT':
            f.write('geweke.plot(ml)\n')
        elif cmd == 'RL':
            f.write('raftery.diag(ml)\n')
        elif cmd == 'HW':
            f.write('heidel.diag(ml)\n')
        elif cmd == 'A':
            f.write('autocorr.diag(ml)\n')
        elif cmd == 'APLOT':
            f.write('autocorr.plot(ml)\n')
        elif cmd == 'C':
            f.write('crosscorr(ml)\n')
        elif cmd == 'CPLOT':
            f.write('crosscorr.plot(ml)\n')
        elif cmd == 'ESS':
            f.write('effectiveSize(ml)\n')
        elif cmd == 'S':
            f.write('summary(ml)\n')
        elif cmd == 'V':
            f.write('varnames(ml)\n')
        elif cmd == 'DPLOT':
            f.write('densplot(ml)\n')
        elif cmd == 'TPLOT':
            f.write('traceplot(ml)\n')
        elif cmd == 'PLOT':
            f.write('plot(ml)\n')
    
    if doBatch:
        f.write('dev.off()\n')
    f.close()
    print "Wrote R script to %s." % outfile
    
    # In batch mode, execute the whole thing.
    if doBatch:
        print "Executing R script..."
        os.system('R CMD BATCH --slave %s %s' % (outfile, outfilesink))
        print "Wrote text output to %s." % outfilesink
        print "Wrote graphical output to %s." % outfilepdf


# Starter.
if __name__ == "__main__":    
    
    # Read options.
    outfile = 'convdiag.r'
    doConvert = True
    cmds = []
    doBatch = False
    burninstr = ""
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ho:ca:eb:")
    except getopt.GetoptError:
        print "Invalid option."
        sys.exit(2)
    for opt, optval in opts:
        if opt == "-h":
            usage()
            sys.exit(0)
        elif opt == "-o":
            outfile = optval
        elif opt == '-c':
            doConvert = False
        elif opt == '-a':
            cmds = optval.split(',')
        elif opt == '-e':
            doBatch = True
        elif opt == '-b':
            bi = float(optval)
            burninstr = "-b %s" % bi 
    
    try:
        # Check input, etc.
        if len(args) < 1:
            raise Exception("Wrong number of input arguments. Use -h for help.")
        for cmd in cmds:
            if not cmd in CMDS:
                raise Exception("Invalid command name. Use -h for help.")
        infiles = args[0:]
                
        # Convert to coda format.
        if doConvert:
            rfiles = []
            for i in range(len(infiles)):
                fname = infiles[i]
                rname = fname + '.coda'
                print "Converting %s to %s ..." % (fname, rname),
                os.system("mcmc_analysis %s -coda %s > %s" % (burninstr, fname, rname))
                print "done."
                rfiles.append(rname)
        else:
            rfiles = infiles
            
        # Produce R file.
        CodaAnalysis(rfiles, outfile, cmds, doBatch)
        
    except Exception, e:
        print "Error: %s\n" % e
        sys.exit(2)
    except:
        print "Unexpected error: %s" % sys.exc_info()[0]
        sys.exit(2)
        

