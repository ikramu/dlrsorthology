#!/bin/bash
USAGE="test_integrateTimes.bash <prefix> <iterations>"

if (test -z $1 || test -z $2) then
	echo $USAGE
	exit
fi


# Change these parameters before running the script:
export PATH=..:$PATH

BIRTH=0.5
DEATH=0.5
LEAVES=10
SMODEL=jc69
EMODEL=const
EDENSITY=uniform
CHAR=1000
MEAN=0
VAR=0.5


beep_generateTree -To $1"_G.true" -o $1".G" -Gt -Bp $BIRTH $DEATH $LEAVES

beep_generateSeqData -To $1"_F.true" -o $1".F" -Sm $SMODEL -Sn 1 -Em $EMODEL -Ed $EDENSITY -Ep $MEAN $VAR $1".G" $CHAR
	
# The following lines currently only works for const models
RATE=`grep -v "#" $1"_F.true"|awk '{print$3}'`

ITERS=$2
for ((i=0; i<$ITERS; i++)); do
  test_integrateTimes_sample -Sm $SMODEL -Sn 1 -Em $EMODEL -Ed $EDENSITY -Er $RATE -Bf $BIRTH $DEATH  -l -p 100 $1".G" $1".F">>  $1"100.like" 2>/dev/null

  time test_integrateTimes_sample -Sm $SMODEL -Sn 1 -Em $EMODEL -Ed $EDENSITY -Er $RATE -Bf $BIRTH $DEATH  -l -p 1000 $1".G" $1".F">>  $1"1K.like" 2>/dev/null

#  test_integrateTimes_sample -Sm $SMODEL -Sn 1 -Em $EMODEL -Ed $EDENSITY -Er $RATE -Bf $BIRTH $DEATH  -l -p 10000 $1".G" $1".F">>  $1"10K.like" 2>/dev/null

#  test_integrateTimes_sample -Sm $SMODEL -Sn 1 -Em $EMODEL -Ed $EDENSITY -Er $RATE -Bf $BIRTH $DEATH  -l -p 100000 $1".G" $1".F">>  $1"100K.like" 2>/dev/null

#  test_integrateTimes_sample -Sm $SMODEL -Sn 1 -Em $EMODEL -Ed $EDENSITY -Er $RATE -Bf $BIRTH $DEATH  -l -p 1000000 $1".G" $1".F">>  $1"1G.like" 2>/dev/null

done

time test_integrateTimes_mcmc -o $1".mcmc" -q -Sm $SMODEL -Sn 1 -Em $EMODEL -Ed $EDENSITY -Er $RATE -Bf $BIRTH $DEATH -i 1000 -t 1000 $1".G" $1".F"


