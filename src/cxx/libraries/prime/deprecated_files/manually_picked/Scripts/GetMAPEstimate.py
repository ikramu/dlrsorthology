#!/usr/bin/python

import getopt
import glob
import string
import sys


# Input:  params    - list with parameter names.
#         flist     - list with names of parallel chain files.
#         doOutputL - true to prepend L.
# Output: a tab-separated string with extracted parameters.
def GetMAPEstimate(params, flist, doOutputL):
        MAPL = -1e100
        MAPParams = [None]*len(params)
        
        # Open and read files.
        for fname in flist:
            f = open(fname, 'r')
            indices = getParamIndices(f, params)
            (MAPL, MAPParams) = updateParamValues(f, indices, MAPL, MAPParams)
            f.close()
        
        # Return results.
        if doOutputL: MAPParams.insert(0, str(MAPL))
        return ("\t".join(MAPParams))



# Returns a list with indices of the sought parameters.
# Index -1 is used for L.
def getParamIndices(f, params):
    indices = []    
    
    # Get all parameters in file.
    ln = ""
    while not ln.startswith("# L N"):
        ln = f.readline()
    ln = ln[6:].strip()
    ps = ln.split(';')
    for i in range(len(ps)):
        ps[i] = ps[i].strip()
        ps[i] = ps[i][:ps[i].rfind('(')]
    
    # Find indices of those we're interested in.
    for i in range(len(params)):
        if params[i] == 'L':
            indices.append(-1)
            continue
        for j in range(len(ps)):
            if params[i] == ps[j]:
                indices.append(j)
                break
    if len(indices) != len(params):
        f.close()
        raise Exception("Failed to find all parameters %s!" % ",".join(params))
    return indices



# Updates the best seen MAP L and parameters.
def updateParamValues(f, indices, MAPL, MAPParams):
    
    # Retrieve best state in file.
    ln = ""
    while not ln.startswith("# local optimum"):
        ln = f.readline()
    lhood = float(ln[18:])
    if lhood > MAPL:
        MAPL = lhood
        
        # Retrieve all file values.
        ln = f.readline()
        ps = ln[13:].split(';')
        
        # Retrieve those we're interested in.
        for i in range(len(indices)):
            if indices[i] == -1:
                MAPParams[i] = str(lhood)
            else:
                MAPParams[i] = ps[indices[i]].strip()
    return (MAPL, MAPParams)


def usage():
    print """
===============================================================================
Script to extract parameters for the best sampled state of an MCMC chain file
on the PrIME format. Output is a tab-separated line. Multiple parallel chains
may be be used as input if desired (producing the best estimate of the chains
combined).

Example: ./GetMAPEstimate [-l] <params> <file1> [<parallel file2> ...]
where
  <params>  is a comma-separated list with parameter names (no spaces).
  -l        will insert the MAP likelihood before the parameters.
===============================================================================
"""



# Starter.
if __name__ == "__main__":
    
    # Read options.
    doOutputL = False
    try:
        opts, args = getopt.getopt(sys.argv[1:], "hl")
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, optval in opts:
        if opt == "-h":
            usage()
            sys.exit(0)
        if opt == "-l":
            doOutputL = True
        
    # Extract.
    try:
        if len(args) < 2:
            raise Exception( "Too few parameters. Use option -h for help.")
        params = args[0].split(',')
        fnames = args[1:]
        print GetMAPEstimate(params, fnames, doOutputL)
    except Exception, e:
        print "Error: %s\n" % e
        sys.exit(2)
    except:
        print "Unexpected error: %s" % sys.exc_info()[0]
        sys.exit(2)
