#! /usr/bin/perl -w

#
# In:  Output from MCMC iterations on the format
#            <float>   <iter>    <float>; <float>; <float>; <float>; <gene tree>; <orthology>;
#      The first float is a probability, the second and third are the birth and death rates.
#      Third is the average sequence evolution rate.
#      Fourth column is the time over the root arc in the species tree.
#      Gene tree may contain various annotations, but not orthology information,
#      which is given in the last column.
# Out: A list of sequence pairs and their likelihood of being orthologous.
#

my $usage = "Usage: beep_orthology <burn-in> [<filename>]

The burn-in parameter instructs how many of the initial samples should
be thrown away.  If no filename is given, ortho_prob reads from
STDIN. The input should be on the format provided by beep_genetree
(and possibly other programs):

 <float>   <iter>    <float>; <float>; <float>; <float>; <gene tree>; <orthology>;

The first float is a probability, the second and third are the birth
and death rates.  Third is the average sequence evolution rate.
Fourth column is the time over the root arc in the species tree.  Gene
tree may contain various annotations, but not orthology information,
which is given in the last column.

The output is a list of sequence pairs and their likelihood of being
orthologous, as well as a ranking of trees and their posterior
probabilities.
";


if (@ARGV == 0) {
  print STDERR "No burn-in parameter given!\n\n", $usage;
  exit 1;
}

my $burnin = shift @ARGV;
if (not $burnin =~ /^\d+$/) {
  print STDERR "Did you provide a burn-in parameter? Found '$burnin' when
an integer parameter was expected.\n\n", $usage;
  exit 2;
}

my $n_lines = 0;		# Number of lines with MCMC results
my %tree_hash = ();		# Collect tree strings in hash and count how many of each
my %ortho_hash = ();            # Collect orthology info
my %matrix = ();		# matrix{'nisse'}{'bertil'} stores the probability of
                                # nisse and bertil being orthologs.

while (<>) {
  chomp;
  if (m/^\S+\s+\d+\s+\S+;\s*\S+;\s*\S+;\s*\S+;\s*(\(.+\);)\s*\[(.*)\];\s*$/) {
    $n_lines++;
    if ($n_lines > $burnin) {
      $tree_hash{$1}++;		# Add one for that tree
      $ortho_hash{$2}++;
    }
  } else {
if (not $n_lines == 0){
    warn "Bad format of MCMC results on line $n_lines: '$_'";
  }
}
}

if ($n_lines < $burnin) {
  print STDERR "The burn-in parameter ($burnin) was too large. Only $n_lines samples found in input.\n";
  exit 3;
}


my %ortho_matrix = ();		# This is where we put the orthology results

foreach my $ortho_str (keys %ortho_hash) {
  my $n_observations = $ortho_hash{$ortho_str};

  my @pairs = split(/\s+/, $ortho_str);
  foreach my $ortho_pair (@pairs) {
    $ortho_pair =~ m/\[(\S+),(\S+)\]=([-+e0-9\.]+)/;
    $ortho_matrix{"$1, $2"} += $3 * $n_observations;
  }
}

foreach my $res (keys %ortho_matrix) {
  print $res, "\t", $ortho_matrix{$res} / $n_lines, "\n";
}

exit(0);


### Old stuff from ortho_prob ###

foreach my $tree (keys %tree_hash) {
  my $treecount = $tree_hash{$tree};

  my $fname = "/tmp/tree$$";
  open(F, ">$fname") or die "Could not open a temporary file '$fname'.";
  print F $tree;
  close(F);

  open(F, "ortho_parse $fname |") or die "Could not analyse $fname with ortho_parse!";
  while (<F>) {
    chomp;
    if (m/(\S+)\s+(\S+)/) {
      if ($1 gt $2) {
	$matrix{$2}{$1} += $treecount;
      } else {
	$matrix{$1}{$2} += $treecount;
      }
    } else {
      die "Bad output from ortho_parse! ('$_')";
    }
  }
  close(F);
  unlink $fname;
}



foreach my $key1 (keys %matrix) {
  foreach my $key2 (keys %{$matrix{$key1}}) {
    print "$key1\t$key2\t", $matrix{$key1}{$key2} / ($n_lines - $burnin), "\n";
  }
}
