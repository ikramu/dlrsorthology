#! /bin/bash


#!/bin/bash
# INPUT and OUTPUT:
# =================
# input_file: contains the result from an MCMC run
# output_file: contains the sorted states. Only unique states are kept.
#          For each unique state the number of occurances is also
#          written to the outfile.
# OBJECTIVE:
# ==========
# To get sorted, unique states, with the number of different unique states.
# Calling sequence:
# sortSample input_file output_file

E_BADARGS=65
    
    if [ ! -n "$1" ]
    then
    echo "Usage: `basename $0` input_file output_file."
    exit $E_BADARGS
    fi  
        
    index=1
    # The sorted input file
    burn_in=$1
    shift
    inputFile=$1
    shift
    # The output file to store a given recon
    outputFile=$1

    # Write tree numer treeNumber to outputFile  
    getTreesFile $burn_in $inputFile tmpFile  

    # Sort the tree file and write out the number of equal reconciliations.
    sort tmpFile |uniq -c > $outputFile
    rm tmpFile
    exit 0


