#! /usr/bin/perl -w

#
# In:  Output from MCMC iterations on the format
#            <float>   <iter>    <float>;<float>;<annotated tree>
#      The first float is a probability, the second and third are the birth and death rates.
#      The annotated tree can be read by ortho_parse (which is also called by this script).
# Out: A list of sequence pairs and their likelihood of being orthologous.
#

my $usage = "Usage: ortho_prob <burn-in> [<filename>]

The burn-in parameter instructs how many of the initial samples should be thrown away.
If no filename is given, ortho_prob reads from STDIN. The input should be on the format
provided by ortho_mcmc (and possibly other programs):

            <float>   <iter>    <float>;<float>;<annotated tree>

The first float is a probability, the second and third are the birth and death rates.
The annotated tree can be read by ortho_parse (which is also called by this script).

";


if (@ARGV == 0) {
  print STDERR "No burn-in parameter given!\n\n", $usage;
  exit 1;
}

my $burnin = shift @ARGV;
if (not $burnin =~ /^\d+$/) {
  print STDERR "Did you provide a burn-in parameter? Found '$burnin' when
an integer parameter was expected.\n\n", $usage;
  exit 2;
}

my $n_lines = 0;		# Number of lines with MCMC results
my %tree_hash = ();		# Collect tree strings in hash and count how many of each
my %matrix = ();		# matrix{'nisse'}{'bertil'} stores the probability of
                                # nisse and bertil being orthologs.

while (<>) {
  chomp;
  if (m/^\S+\s+\d+\s+\S+;\S+;(.+)$/) {
    $n_lines++;
    if ($n_lines > $burnin) {
      $tree_hash{$1}++;		# Add one for that tree
    }
  } else {
    warn "Bad format of MCMC results on line $n_lines: '$_'";
  }
}

if ($n_lines < $burnin) {
  print STDERR "The burn-in parameter ($burnin) was too large. Only $n_lines samples found in input.\n";
  exit 3;
}

foreach my $tree (keys %tree_hash) {
  my $treecount = $tree_hash{$tree};

  my $fname = "/tmp/tree$$";
  open(F, ">$fname") or die "Could not open a temporary file '$fname'.";
  print F $tree;
  close(F);

  open(F, "ortho_parse $fname |") or die "Could not analyse $fname with ortho_parse!";
  while (<F>) {
    chomp;
    if (m/(\S+)\s+(\S+)/) {
      if ($1 gt $2) {
	$matrix{$2}{$1} += $treecount;
      } else {
	$matrix{$1}{$2} += $treecount;
      }
    } else {
      die "Bad output from ortho_parse! ('$_')";
    }
  }
  close(F);
  unlink $fname;
}



foreach my $key1 (keys %matrix) {
  foreach my $key2 (keys %{$matrix{$key1}}) {
    print "$key1\t$key2\t", $matrix{$key1}{$key2} / ($n_lines - $burnin), "\n";
  }
}
