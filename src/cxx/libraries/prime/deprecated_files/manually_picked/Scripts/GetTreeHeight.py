#!/usr/bin/python

import sys

if len(sys.argv) < 2:
    print "Outputs the height of a Newick tree, where a single leaf tree has height 0."
    print "Usage: %s <treefile1> [<treefile2> ...]" % sys.argv[0]
    sys.exit(0)

infiles = sys.argv[1:]
for file in infiles:
    f = open(file, 'r')
    tree = f.readline()
    f.close()
    cnt = 0
    maxcnt = 0
    for c in tree:
        if c == '(':
            cnt += 1
            if cnt > maxcnt: maxcnt = cnt
        elif c == ')':
            cnt -= 1
    print maxcnt