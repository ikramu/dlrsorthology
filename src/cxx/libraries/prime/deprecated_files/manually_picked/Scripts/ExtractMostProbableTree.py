#!/usr/bin/python

import getopt
import re
import string
import sys

from subprocess import *

# Particularly notewothy constants.
ANALYSIS_CMD = "mcmc_analysis"
TREE_CONV_CMD = "showtree -p"
TREE_PARAM = "Tree_Model1.Tree"
TREE_STRIP_PATTERN = re.compile("(\;|\:[e\d.\+\-]*|\[[ \&\=\w]*\]|\s*)")


# Main program.
def main(argv):
    
    # Read options.
    optStr = ""
    doAll = False
    showProb = False
    trueTree = None   
    try:
        opts, args = getopt.getopt(argv[1:], "hb:art:")
    except getopt.GetoptError:
        usage(2)
    for opt, optval in opts:
        if opt == "-h":
            usage(0)
        elif opt == "-b":
            optStr += (" -b " + optval)
        elif opt == "-a":
            doAll = True
        elif opt == "-r":
            showProb = True
        elif opt == "-t":
            if optval.startswith('('):
                p1 = Popen(["echo '" + optval + "'"], shell=True, stdout=PIPE, stderr=PIPE)
                p2 = Popen([TREE_CONV_CMD], shell=True, stdout=PIPE, stderr=PIPE, stdin=p1.stdout)
                trueTree = p2.communicate()[0]
            else:
                p1 = Popen([TREE_CONV_CMD + " " + optval], shell=True, stdout=PIPE, stderr=PIPE)
                trueTree = p1.communicate()[0]
            trueTree = trueTree.strip().strip(';')
    
    try:
        # Read input arguments.
        if len(args) < 1: raise Exception("Too few input arguments.")
        infiles = args[0:]
        if len(infiles) > 1: optStr += " -P"
        infileStr = string.join(infiles)
        
        # For optimization, first get remaining parameter names so that they may be ignored.
        cmd = ANALYSIS_CMD + " -p dumtrams " + infiles[0]
        ign = Popen([cmd], shell=True, stdout=PIPE, stderr=PIPE).communicate()[1]
        ign = ign[ign.find(':')+1:].strip().replace(TREE_PARAM, '')
        ign = ign.replace(' ', '').replace('\t','').replace('\n', ',')
        optStr += (" -i " + ign)
        
        # Run analysis.
        cmd = ANALYSIS_CMD + optStr + ' ' + infileStr
        res = Popen([cmd], shell=True, stdout=PIPE, stderr=PIPE).communicate()[0]
        res = res[res.find(TREE_PARAM):].strip()
        res = res.split('\n')[2:]
        
        # Extract.
        n = 1
        if doAll: n = len(res)
        if trueTree == None:
            for i in range(n):
                cols = res[i].strip().split('\t')
                if showProb: print("%s\t%s" % (cols[2], cols[1]))
                else:        print(cols[2])
        else:
            rank = 0
            prob = 0.0
            for i in range(n):
                cols = res[i].strip().split('\t')
                if cols[2] == trueTree:
                    rank = cols[0]
                    prob = cols[1]
                    break
            if showProb: print("%s\t%s" % (rank, prob))
            else:        print(rank)
        
    except Exception, e:
        print "Error: %s\n" % e
        usage(2)
    except:
        print "Unexpected error: %s" % sys.exc_info()[0]
        sys.exit(2)


# Strips Newick trees of beanchlengths and tags.
def stripTree(tree):
    return TREE_STRIP_PATTERN.sub('', tree)


# Shows error/help message, then exits.
def usage(exitCode):
    print "Extracts the top-ranked tree from an MCMC output chain (or multiple"
    print "parallel chains). Note: uses mcmc_analysis script to accomplish this."
    print "Usage: " + sys.argv[0] + " [options] <chain1> <chain2> ..."
    print "Options:"
    print "  -h             Shows (this) help message."
    print "  -b <burn-in>   Changes the default burn-in ratio, e.g. 0.2 to"
    print "                 discard 20% first samples."
    print "  -a             Considers all visited trees rather than the top-ranked one."
    print "  -r             Appends an extra column showing the probability of the tree."
    print "  -t <true tree> Returns 1 if the true tree was top-ranked, otherwise"
    print "                 0. In conjunction with option -a, returns the rank"
    print "                 of the true tree (e.g. 2 if second best)."
    sys.exit(exitCode)
    
    
# Starter.
if __name__ == "__main__":
    main(sys.argv)
