#! /usr/bin/perl -w

#
# Output number of gene leaves, number of species, and absolute difference
# of estimated rates and true rates.
#

my $BURNIN=200;

#
# First find the true gamma/genetree:
#
my $TrueBirthRate;
my $TrueDeathRate;
my $TrueTree;

my $n_lines = 0;		# For error analysis
while (<>) {
  chomp;
  $n_lines++;

  if (m/^\s*True\s+([0-9\.ef+-]+);([0-9\.ef+-]+);(.+)\s*$/) {
    $TrueBirthRate = $1;
    $TrueDeathRate = $2;
    $TrueTree = $3;
    last;			# Jump out!
  }
}


#
# Verify that the we found the true parameters:
#
if (! (defined $TrueTree
       && defined $TrueDeathRate
       && defined $TrueBirthRate)) {
  print STDERR "First line of the evaluation data is expected to contain a line
on the format
   'true   true   <birthrate>;<deathrate>;<tree with gamma>'.
No such line was found.

";
  exit 1;
}

#
# Go forth and study the MCMC output
#
my $mcmc_lines = 0;		# Number of lines with MCMC results
my $b_rate = 0;			# For estimation
my $d_rate = 0;			# For estimation

my $n_hits=0;

while (<>) {
  chomp;
  if (m/^[\d\.efg\-\+]+\s+\d+\s+(\S+);(\S+);(.+)$/) {
    $n_lines++;
    if ($BURNIN > $n_lines)
      {
	next;
      }
    $mcmc_lines++;

    $b_rate += $1;
    $d_rate += $2;

  } 
}

#
# Estimate parameters
#

$b_rate = $b_rate / $mcmc_lines;
$d_rate = $d_rate / $mcmc_lines;

print count_gene_leaves($TrueTree), "\t";
print count_n_species($TrueTree), "\t";
print abs($b_rate-$TrueBirthRate), "\t";
print abs($d_rate-$TrueDeathRate), "\n";

exit 0;



### Helpers ####################################################

sub count_gene_leaves {
  my ($tree) = @_;

  my @commas = $tree=~ m/,/g;
  return scalar(@commas);
}


sub count_n_species {
  my ($tree) = @_;

  my $max_species = 0;
  while ($tree =~ m/Species(\d+)_/g) {
    if ($1 > $max_species) {
      $max_species = $1;
    }
  }

  return $max_species + 1;
}
