#!/bin/bash

# usage: "mcmcplots mcmcfile int1 int2" plots trajectory of mcmc-parameters int1 to int2." 

if (test -z $1 || test -z $2|| test -z $3) then
    echo "Usage: mcmcplots <mcmcfile> <int1> <int2> [<int3>]" plots \(separate\) trajectories of likelihood and mcmc-parameters int1 to int2, int3 indicates burn-in ;
    exit;
fi

BURNIN=`grep -v "#" $1 |wc -l |awk '{print $1}'`;
if(! test -z $4) then
    let BURNIN=$((BURNIN-$4));
fi

TITLE=`grep "# L " $1|sed s/"#"// |awk '{print$1}'`
grep -v "#" $1 |tail -$BURNIN| awk '{print$2"  "$1}'|sed s/\;//g|graph -TX -L $TITLE -;
ITER=$(($2+2));
while (test $ITER -le $(($3+2))) ; do
    TITLE=`grep "# L " $1|sed s/"#"// |awk '{print$'$ITER'}'`
    grep -v "#" $1|tail -$BURNIN |awk '{print$2"  "$'$ITER'}'|sed s/\;//g|graph -TX -L $TITLE -;
    let $((ITER++))
done


