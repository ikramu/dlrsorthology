#! /usr/bin/perl -w
# INPUT and OUTPUT:
# =================
# input_file: contains the accepted states from an
# MCMC run, or the sorted state from an MCMC run.
# output_file: contains a vector of likelihood values, or
#              a matrix, where the second column
#              holds the likelihood values, and the
#              first column holds the corresponding
#              frequencies.

my $usage ="Usage: getLike <mcmcdata>

 In:  A (probably) simulated *true* \"tree\" on the first line, and then
      output from MCMC iterations on the format
            <float>   <iter>    <float>;<float>;<annotated tree>

      The first float is a probability, the second and third are the birth and death rates.
      The annotated tree can be read by ortho_parse (which is also called by this script).

 Out: ...

";

if (@ARGV != 2) {
    print STDERR $usage;
    exit 1;
}

my $infile=$ARGV[0];
my $outfile=$ARGV[1];
open (IN, "<$infile")
    or die "Could not open '$infile' for reading. \n$usage";
open (OUT, ">$outfile")
    or die "Could not open '$outfile' for writing\n$usage";

my $n_lines = 0;		# For error analysis
# Set the number of burn_in iterations
my $burn_in = 30;
$n_lines = 0;
my $tmp = $infile;

# Check whether the input file is a sorted state file
# or the original output file from an MCMC run.
# The name of sorted files should contain the word sort in
# some form.
if ($tmp =~ /sort/){
    while (<IN>) {
	s/^\s+(\d+\s+\S+)\s+\S+;\S+;.+$/$1/g;
	print OUT $_;
	$n_lines++;
    }   
    print $n_lines;
}
# If the input_file is not sorted:
else{
#
# First find the true gamma/genetree:
#
    my $TrueBirthRate;
    my $TrueDeathRate;
    my $TrueTree;
    my $GammaTree;
    
    while (<IN>) {
	chomp;
	$n_lines++;
	
	if (m/^\s*True\s+([0-9\.ef+-]+);([0-9\.ef+-]+);(.+)\s*$/) {
	    $TrueBirthRate = $1;
	    $TrueDeathRate = $2;
	    $TrueTree = $3;
	    last;			# Jump out!
	}
    }
#
# Verify that the we found the true parameters:
#
    if (! (defined $TrueTree
	   && defined $TrueDeathRate
	   && defined $TrueBirthRate)) {
	print STDERR "First line of the evaluation data is expected to contain a line
on the format
   'true   true   <birthrate>;<deathrate>;<tree with gamma>'.
No such line was found.

$usage
";
	exit 1;
    }
    while (<IN>) {
	chomp;
	$n_lines++;
	
	if (m/^\s*GammaStar\s+(.+)\s*$/) {
	    $GammaTree = $1;
	    last;			# Jump out!
	}
    }
#
# Verify that the we found the true parameters:
#
    if (! (defined $GammaTree)) {
	print STDERR "Second line of the evaluation data is expected to contain a line
on the format
   'GammaStar <tree with gamma>'.
No such line was found.

$usage
";
	exit 1;
    }
    
# Calculate the mean values at all points
    while (<IN>) {
	if ($n_lines >= $burn_in){
	    s/^(\S+)\s+\d+\s+\S+;\S+;.+$/$1/g;
#      s/\d+\s+(\S+);(\S+);/$1;$2;/g;
	    print OUT $_;
	}    
	$n_lines++;
    }
}
close(IN);
close(OUT);
