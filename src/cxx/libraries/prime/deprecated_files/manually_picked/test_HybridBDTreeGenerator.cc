
#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "Beep.hh"
#include "PRNG.hh"
#include "HybridBDTreeGenerator.hh"
#include "HybridTree.hh"
#include "HybridTreeIO.hh"

// Global options with default settings
//------------------------------------
int nParams = 1;

double lambda = 1.0;
double mu = 1.0;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
 
  if (argc < nParams+1) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = 1;
      // Read arguments
      //---------------------------------------------
      if (opt >= argc)
	{
	  cerr << "Expected a tree\n";
	  usage(argv[0]);
	}
      
      HybridTreeIO io_in = HybridTreeIO::fromFile(string(argv[opt++]));
      HybridTree S = io_in.readHybridTree();
      if(S.getName() == "Tree")
	{
	  S.setName("Host");
	}
      
 
      cerr << "Print Hybrid Tree:\n"
	   << S.printHybridTree() 
	   << " and its corresponding binary tree:\n"
	   << S.getBinaryTree()
	   << endl;

      if(opt+1 < argc)
	{
	  lambda = atof(argv[opt++]);
	  mu = atof(argv[opt++]);
	}
      
      // Set up the model for the species tree
      //---------------------------------------------------------
      HybridBDTreeGenerator bdg(S, lambda, mu);
      
      Tree G;
      while(bdg.generateTree(G) == false);
      StrStrMap gs =  bdg.exportGS();
      cerr << G;
      cerr << gs;
      GammaMap gamma = bdg.exportGamma();
      cerr << gamma;
      cout << TreeIO::writeBeepTree(G, true, false, true, false, false, &gamma) << endl;;
      //      cout << TreeIO::writeBeepTree(G, true, false, true, false, false, 0) << endl;;
    }
  catch (AnError& e)
    {
      cerr << "error found\n";
      e.action();
    }
  catch (exception& e)
    {
      cerr << "exception found\n"
	   <<e.what()
	   << endl;
    }
}


void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " <STree> [<birthRate> <deathRate>]\n"
    << "\n"
    ;
  exit(1);
}

