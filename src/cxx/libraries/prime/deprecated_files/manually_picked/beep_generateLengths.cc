#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "AnError.hh"
#include "Beep.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "PRNG.hh" 
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"

using std::vector;

// Global options with default settings
//------------------------------------
int nParams = 1;

double mean = 1.0;
double variance = 0.2;

std::string density = "UNIFORM";

std::string rateModel = "CONST";

char* outfile = 0;  //NULL;
char* t90file = 0;  //NULL

// allowed models
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// output to true file
bool outputTree = true;
bool outputRates = true;
bool outputBD = true;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv)
{
  using namespace beep;
  using namespace std;
  
  // Set up allowed models
  //-------------------------------------------------------------
  //rate model
  rate_models.push_back("IID");
  rate_models.push_back("GBM");
  rate_models.push_back("CONST");
  
  //rate model
  rate_densities.push_back("UNIFORM");
  rate_densities.push_back("GAMMA");
  rate_densities.push_back("LOGN");
  rate_densities.push_back("INVG");

  if (argc < nParams+1) 
    {
      usage(argv[0]);
    }
  try
    {
      // Tell user we started 
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory";
      system("pwd");
      cerr << "\n\n";
      
      // Read options
      //---------------------------------------------
      int opt = readOptions(argc, argv);

      // Read arguments
      //---------------------------------------------
      if (opt >= argc)
	{
	  cerr << "Expected treefile argument\n";
	  usage(argv[0]);
	}
      string treefile(argv[opt++]);

      // Get the model tree
      //---------------------------------------------
      TreeIO io = TreeIO::fromFile(treefile);
      Tree G = io.readHostTree();  // reads times!
      G.setName("G");
      cout << G.print(true, true, true);

      // create the random number generator to use
      PRNG rand;
      
      // Set up Density function for rates
      //---------------------------------------------------------
      Density2P* df;
      capitalize(density);
      if(density == "INVG")
	{
	  df = new InvGaussDensity(mean, variance);
	}
      else if(density == "LOGN")
	{
	  df = new LogNormDensity(mean, variance);
	}
      else if(density == "GAMMA")
	{
	  df = new GammaDensity(mean, variance);
	}
      else// (density == "UNIFORM")
	{
	  df = new UniformDensity(mean, variance, true);
	}
      // Test for sanenss of mean and variance!
      Real min, max;
      df->getRange(min, max);
      if(min < 0)
	{
	  cerr << "The mean and variance given for density yields\n"
	       << "yields a partially negative range. Please correct\n";
	  exit(1);
	}
      
       // Set up rates 
      //---------------------------------------------------------
      DummyMCMC dm;
      EdgeRateMCMC* erm;
      if(rateModel == "CONST")
	{
	  erm = new ConstRateMCMC(dm, *df, G, "EdgeRates");
	}
      else if(rateModel == "IID")
	{
	  erm = new iidRateMCMC(dm, *df, G, "EdgeRates");
	}
      else //(rateModel == "GBM")
	{
	  erm = new gbmRateMCMC(dm, *df, G, "EdgeRates");
	}
      
      //generate Rates
      erm->generateRates(); 
      
      cerr << "node nr\ttime\trate\tlength\length/time\n";
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  Node* gn = G.getNode(i);
	  if(gn->isRoot())
	    {
	      cerr << "root is node " << i<< endl;
	      continue;
	    }
	  else
 	    {
	      gn->setLength(gn->getTime() * erm->getRate(gn));
	      cerr << gn->getTime() << "\t"
		   << erm->getRate(gn) << "\t"
		   << gn->getLength() << "\t"
		   << gn->getLength() / gn->getTime() <<"\n";
	    }
	}
      cout << "Check that times are still O.K.!\n";
      cout << G.print(true, true, true);
      
      // Outout Data
      //---------------------------------------------------------
      cout << endl
	   << " branchLengths were generated";

      cout << ":\n"
	   << G.print(true, true, true);
	  
      if (outfile != NULL)
	{
	  TreeIO io_out;
	  ofstream tree((string(outfile)).data());
//	  tree << io_out.writeNewickTree(G) << "\n"; 
	  tree << io_out.writeBeepTree(G, true, true, true, true, false, 0) << "\n"; 

	  cout <<" and written to file "
	       << outfile
	       << endl;
	}
      if(t90file != 0)
	{
	  cout << "'True' parameters for 90% test is written to file "
	       << t90file
	       << "\n"
	    ;
	  ofstream t90((string(t90file)).data());

	  ostringstream t90header;
	  ostringstream t90data;
	  TreeIO io_out;

	  t90header << "# True parameters input for 90% test\n"
		    << "# T N\t";
	  t90data << "0 0\t";
	  //OUTPUT RATES
	  for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	    {
	      Node* gn = G.getNode(i);
	      if(gn->isRoot() == false)
		{
		  t90header << "edgelength[" << i << "](float); \t";
		  t90data << gn->getTime() * erm->getRate(gn) << ";\t";
		}
	    }

	  if(outputRates)
	    {
	      t90header << erm->strHeader();
	      t90data << erm->strRepresentation();
	    }
	  if(outputTree)
	    {
	      t90header << G.getName() << "(tree);\t";
	      t90data << io_out.writeNewickTree(G) + ";\t";
	      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
		{
		  Node& n = *G.getNode(i);
		  if(!(n.isLeaf()||n.isRoot()))
		    {
		      t90header << "EdgeTimes.nodeTime[" 
				<< i << "](float); ";
		      t90data << n.getNodeTime() << "; ";
		    }
		}
	    }
	  t90header << "\n";
	  t90data << "\n";
	  t90 << t90header.str() << t90data.str();
	  delete erm;
	  delete df;
	}
    }
  catch (AnError e)
    {
      cerr << "error found\n";

      e.action();
    }
}


void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <treefile>\n"
    << "\n"
    << "Parameters:\n"
    << "   <treefile>         name of file with tree to generated data on.\n"


    << "Options:\n"
    << "   -u, -h             This text.\n"
    << "   -o <filename>      output file\n"
    << "   -T<option>         Options for the output of true parameters\n"
    << "                      used in the generation (off by default)\n"
    << "     -To <filename>   Outputs 'true' parameters to file <filename>,\n"
    << "                      by default outputs all parameters.\n"
    << "     -Tg              Do not output the tree and edge times used for\n"
    << "                      data generation\n"
    << "     -Tr              Do not output (substitution) rates (including\n"
    << "                      the mean and variance of the rate model).\n"
    << "   -E<option>         Options relating to modelling substitution rates\n"
    << "                      over edges in the tree\n"
    << "     -Em <'iid'/'gbm'/'const'>\n"
    << "                      the edge rate model to use, (const is default)\n"
    << "     -Ep <float> <float>\n"
    << "                      mean and variance of edge rate model\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'> \n"
    << "                      the density function to use for edge rates, \n"
    << "                      (Gamma is the default) \n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{ //General options
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'T':
	  { // 'true' options
	    switch (argv[opt][2]) 
	      { // suboptions
	      case 'o':
		{
		  if (opt + 1 < argc)
		    {
		      t90file = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected filename after option '-To'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'g':
		{
		  outputTree = false;
		  break;
		}
	      case 'r':
		{
		  outputRates = false;
		  break;
		}
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	case 'E':
	  { // edge rate model options
	    switch (argv[opt][2]) 
	      { // suboptions
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      capitalize(rateModel);
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
		    variance = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Expected float (variance for edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance for\n"
			   << "edge rates) for option '-Ep'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		      capitalize(density);
		      if(find(rate_densities.begin(), rate_densities.end(), 
			      density) == rate_densities.end())
			{
			  cerr << "Expected 'InvG', 'LogN', 'Gamma' or "
			       << "'Uniform'for option -Ed\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected density after option '-Ed'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{	
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	default:
	  {	
	    cerr << "option " << argv[opt] << " not recognized\n";
	    usage(argv[0]);
	    break;
	  }
	}
      opt++;
    }
  return opt;
}
