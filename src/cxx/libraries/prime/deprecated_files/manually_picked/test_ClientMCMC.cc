#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>

#include <sys/types.h>
#include <unistd.h>


// PVM stuff:
#include "PvmBeep.hh"
#include "PvmClient.hh"

#include "AnError.hh"
#include "BirthDeathMCMC.hh"
#include "ConstRateMCMC.hh"
#include "DummyMCMC.hh"
#include "GammaMap.hh"
#include "GeneTreeMCMC.hh"

#include "Probability.hh"

#include "PRNG.hh"
#include "ReconSeqApproximator.hh"
#include "SeqIO.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "SequenceModel.hh"
// #include "SimpleMCMC.hh"
#include "ClientMCMC.hh"

#include "SpeciesTreeMCMC.hh"
#include "StrStrMap.hh"
#include "SubstitutionMatrix.hh"
#include "SubstitutionModel.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "SeqIO.hh"

// Density functions:
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "UniformDensity.hh"

using namespace beep;
using namespace std;


int
main (int argc, char **argv) 
{

  double birthRate=-1.0, deathRate=-1.0, beta=1.0, alpha=-1.0;
  unsigned n_samples=10000, n_iter=100000,thinning=100, RSeed = 0;
  int my_id=1, numberOfClients;
  Tree G, S;
  StrStrMap gs;
  std::string seqModel;
  std::string density;
  SequenceModel* pmodel;  
  SequenceType m_st;
  std::string TypeOfData;
  //  char str[6];
  //  SequenceData D(myDNA);
  std::string outfile="";
  bool quiet = true;
  bool do_likelihood = false;
  bool fixed_gene_tree = false;
  bool show_debug_info = false;
  bool fixed_rates = false;

  try
    {

      // Receive the data from the master process:
      std::string group = "Clients";
      int numOfMembers = 2;

      
      PvmClient C;

      //      C.join(group);

      C.waitForMessage();
      C.receive(birthRate);
  
      C.waitForMessage();
      C.receive(deathRate);   
  
      C.waitForMessage();
      C.receive(beta);
  
      C.waitForMessage();
      C.receive(n_samples);
  
      C.waitForMessage();
      C.receive(n_iter);

      C.waitForMessage();
      C.receive(thinning);

      C.waitForMessage();
      C.receive(seqModel);
  
      C.waitForMessage();
      C.receive(density);
  
      C.waitForMessage();
      C.receive(G);

      C.waitForMessage();
      C.receive(S,PvmBeep::SpeciesTree);
  
      C.waitForMessage();
      C.receive(gs);
  
      C.waitForMessage();
      C.receive(TypeOfData);

      if (TypeOfData == "AA")
	{
	  m_st = myAminoAcid;
	}
      else
	{
	  m_st = myDNA;
	}

      SequenceData D(m_st);
      C.waitForMessage();
      C.receive(D);

      C.waitForMessage();
      C.receive(outfile);
     
      C.waitForMessage();
      C.receive(numberOfClients);
  
      // This is the chain number not the tid!      
      C.waitForMessage();
      C.receive(my_id);

      // Finnished receiving, no set up the model:
      
      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      if (RSeed == 0)
	{
	  RSeed = getpid();		// Seeded by process id
	}
    
      PRNG rand(RSeed);
      DummyMCMC dm(rand);
      
      // Set up priors
      //-------------------------------------------------------
      SpeciesTreeMCMC stm(dm, S, beta);
      BirthDeathMCMC bdm(stm, S, birthRate, deathRate, !fixed_rates);
    
      // Start building the Monte Carlo approximat
      //---------------------------------------------------------

      // Set up rates
      //---------------------------------------------------------
      UniformDensity df(0, 10000, true); //rate ~ U[0,10000]
      Real substRate = 1.00/S.rootToLeafTime(); // reasonable start substRate
      EdgeRateModel dummyRate(df, G); //dummy end-of-line ratemodel
      ConstRateMCMC molClock(df, G, substRate, dummyRate, dm, 0.5, false);
    
      // Set up the Model for the SubstitutionMatrix
      // how handle this in another way?
      //---------------------------------------------------------
      SequenceModel* pmodel;
      if(seqModel == string("UniformAA"))
	{
	  pmodel =  new UniformAA();
	}
      else if(seqModel == ("JTT"))
	{
	  pmodel =  new JTT();
	}
      else if(seqModel == ("UniformCodon"))
	{
	  pmodel =  new UniformCodon();
	}
      else if (seqModel == "JC69")
	{
	  pmodel =  new JC69(); 
	}	 
      else
	{
	  pmodel =  new JC69();
	}

      // Create Q, the substitution matrix
      //---------------------------------------------
      beep::LA_Vector R = pmodel->R();
      beep::LA_DiagonalMatrix Pi = pmodel->Pi();
      beep::SubstitutionMatrix Q(R, Pi);
    
      // Create the actual substitutionModel - incl rates and Q
      //---------------------------------------------
      vector<string> partitionList ;  //Will we ever use this? /bens
      partitionList.push_back(string("all"));
      SubstitutionMCMC sm(G, D, molClock, Q, partitionList, molClock, 0.5);
    
      // Set up Reconciliation handlers
      //---------------------------------------------------------
      ReconciliationSampler     rs(G, gs, bdm, rand);
      ReconciliationTimeSampler rts(rs, rand); // using info given in rs

#ifdef ONLY_ONE_TIMESAMPLE
      SeqProbApproximator spa(dm, rts, sm, 1);
#else
      SeqProbApproximator spa(dm, rts, sm, n_samples);
#endif
      //       ReconSeqApproximator rsa(dm, spa, rs, nGammaSamples);
      ReconSeqApproximator rsa(dm, spa, rs, n_samples);
    
      // The 'top' MCMC-model - interfaces with Monte Carlo approximator
      //---------------------------------------------------------
      GeneTreeMCMC gtm(bdm, rsa, rs, fixed_gene_tree, fixed_gene_tree);
        
      Real TEMP = 2.0;
      // Create MCMC handler
      //---------------------------------------------
      ClientMCMC iterator(C, gtm, rand, outfile, numOfMembers, group, thinning, my_id, TEMP);

//       SimpleMCMC iterator(gtm, rand, thinning);
      iterator.iterate(n_iter);
      
      // Clean up all allocations
     delete pmodel;


      
//       C.createMessage(birthRate);
//       C.sendToMaster();
    }
  
  catch(AnError e)
    {
      // Really, send error to master!
     cout <<" error\n";
      e.action();
    } 
}

