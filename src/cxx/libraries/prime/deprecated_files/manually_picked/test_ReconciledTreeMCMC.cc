#include "AnError.hh"
#include "BranchSwapping.hh"
#include "DummyMCMC.hh"
#include "ReconciledTreeMCMC.hh"
#include "ReconciliationTimeSampler.hh"
#include "TreeIO.hh"
#include "Probability.hh"


int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc != 6)
    {
      cerr << "usage: test_ReconciledTreeModel <Guesttree> <HostTree> <gs> <birthrate> <deathrate>\n";
      exit(1);
    }
  try
    {
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[1]);
      TreeIO io = TreeIO::fromFile(guest);
      Tree G = io.readGuestTree(0,0);
      if(G.getName() == "Tree")
	{
	  G.setName("G");
	}
      cout << "G = \n"
	   << G;

      string host(argv[2]);
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  // Reads times?
      if(S.getName() == "Tree")
	{
	  S.setName("S");
	}
      cout << "S = \n" << S;
      if(S.topTime == 0)
	{
	  S.topTime = S.rootToLeafTime();
	}

      StrStrMap gs;
      gs = TreeIO::readGeneSpeciesInfo(argv[3]);
      cout << gs << endl;

      Real brate = atof(argv[4]);
      Real drate = atof(argv[5]);

      BirthDeathProbs bdp(S, brate, drate);


      cout << "Testing constructors:"
	   << "-------------------------------------------------\n\n"
	   << "First construct ReconciledTreeMCMC a with Ggamma\n"
	   << "the tree and reconciliation.\n\n";
      DummyMCMC dm;
      ReconciledTreeMCMC a(dm,G, gs, bdp);

      cout << a.print() << endl<<endl;

      cout << "Then construct ReconciledTreeMCMC b with S as the\n"
	   << "tree with fixed reconciliation.\n\n";
      StrStrMap gs2;
      for(unsigned i = 0; i < S.getNumberOfNodes(); i++)
	{
	  Node& n = *S.getNode(i);
	  if(n.isLeaf())
	    {
	      gs2.insert(n.getName(), n.getName());
	    }
	}
      ReconciledTreeMCMC b(dm, S, gs2, bdp);
      b.fixGamma();
      cout << b.print() << endl<<endl
	   << "Then use copy constructor to create c from b\n";
      try
	{
	  ReconciledTreeModel c(b);
	  cout << c.print()<< endl<<endl;
	}
      catch(AnError e)
	{
	  cout << "This failed,a s expected, with error message:\n"
	       << e.what()
	       << "\nWe really do need to fix the assignment operator"
	       << " of GammaMap\n"
	       << endl << endl;
	}
	  cout << "\nFinally use operator=, to set b = a:\n";
      try
	{
	  b = a;
	  cout << b.print();
	}
      catch(AnError e)
	{
	  cout << "This failed,a s expected, with error message:\n"
	       << e.what()
	       << "\nWe really do need to fix the assignment operator"
	       << " of GammaMap\n"
	       << endl << endl;
	}
      cout << "Now test some probs using a\n"
	   << "Probability of (G, gamma) is " 
	   << a.calculateDataProbability()
	   << "\n\n";


      cout << " Try a short MCMC run using a\n";
      
      if(G.times == false)
	{
	  ReconciliationTimeSampler rts(a);
	  cout << "First we sample times with probability " << rts.sampleTimes() << endl;;
	}

      PRNG R;
      Probability old_p = a.initStateProb();
      a.commitNewState();
      cout << "\n# L " << a.strHeader() << endl;
      for(unsigned i = 0; i < 10; i++)
	{
	  MCMCObject p = a.suggestNewState();
	  if(true)//p.stateProb * p.propRatio / old_p >= R.genrand_real1())
	    {
	      old_p = p.stateProb; 
	      a.commitNewState();
	    }
	  else
	    {
	      a.discardNewState();
	    }
	  cout << old_p << "\t" << a.strRepresentation() << endl;
	}

	  
    }
  catch(AnError e)
    {
      cerr << "Error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what();
    }
  return(0);
};
