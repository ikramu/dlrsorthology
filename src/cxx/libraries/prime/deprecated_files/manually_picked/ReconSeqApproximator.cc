#include "ReconSeqApproximator.hh"

#include "AnError.hh"
#include "GammaMap.hh"
#include "MCMCObject.hh"
#include "TreeIO.hh"

#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Constructor/Destructor
  //);
  //----------------------------------------------------------------------
  ReconSeqApproximator::ReconSeqApproximator(MCMCModel& prior,
					     StdMCMCModel& nestedLike,
					     ReconciliationSampler& rs,
					     const unsigned& nSamples)
    : StdMCMCModel(prior, nestedLike.nParams()),
      G(rs.getGTree()),
      nestedLike(nestedLike),
      sampler(rs),
      gamma(&rs.getGamma()),
      n_samples(nSamples),
      orthology(G),
      reconciliations(),
      estimateGamma(false)
  {
    if (n_samples > MAX_N_SAMPLES)
      {
	throw AnError("Dude, you have requested way too many reconciliation samples!");
      }
  }

  // Copy constuctor
  //----------------------------------------------------------------------
  ReconSeqApproximator::ReconSeqApproximator(const ReconSeqApproximator &rsa)
    :  StdMCMCModel(rsa),
       G(rsa.G),
       nestedLike(rsa.nestedLike),
       sampler(rsa.sampler),
       gamma(rsa.gamma),
       n_samples(rsa.n_samples),
       orthology(rsa.orthology),
       reconciliations(rsa.reconciliations),
       estimateGamma(false)
  {
  }

  // Death and destruction
  //----------------------------------------------------------------------
  ReconSeqApproximator::~ReconSeqApproximator()
  {
    // Hubba
  }

  // Assignment
  //----------------------------------------------------------------------
  ReconSeqApproximator &
  ReconSeqApproximator::operator=(const ReconSeqApproximator &rsa)
  {
    if (this != &rsa)
      {
	StdMCMCModel::operator=(rsa); 
	G = rsa.G;
	nestedLike = rsa.nestedLike;
	sampler = rsa.sampler;
	gamma = rsa.gamma;
	n_samples = rsa.n_samples;
	orthology = rsa.orthology; 
	reconciliations = rsa.reconciliations;
	estimateGamma = rsa.estimateGamma;
      }

    return *this;
  }


  //-------------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------------
  void 
  ReconSeqApproximator::outputReconciliations(bool value)
  {
    estimateGamma = value;
  }
  

  //-------------------------------------------------------------------
  // StdMCMCModel interface
  //-------------------------------------------------------------------
  MCMCObject
  ReconSeqApproximator::suggestOwnState()
  {
    // clear old values for orthology and reconciliations
    orthology.reset();
    reconciliations.clear();

    // Get yourself a reconciliation
    sampler.generateReconciliation();
    // ... and use it to approximate $\Pr(\tau | \gamma, G, etc.)$.
    MCMCObject MOb = nestedLike.suggestNewState();

    // Add support for various orthology pairs and reconciliations
    orthology.recordOrthology(*gamma, MOb.stateProb);
    if(estimateGamma)
      {
	TreeIO io;
	reconciliations[io.writeGuestTree(G,gamma)] += MOb.stateProb;
      }
    
#ifdef ONLY_ONE_TIMESAMPLE
    MOb.stateProb = simple_sumDataProb(1, MOb.stateProb);
#else
    MOb.stateProb = cached_sumDataProb(1, MOb.stateProb);
#endif
    return MOb;
  }

  void
  ReconSeqApproximator::commitOwnState()
  {
    nestedLike.commitNewState();
  }

  void
  ReconSeqApproximator::discardOwnState()
  {
    nestedLike.discardNewState();
  }

  std::string
  ReconSeqApproximator::ownStrRep() const
  {
    std::ostringstream oss;
    oss << nestedLike.strRepresentation();
    oss << orthology.strRepresentation()
	<< "; ";
    if (estimateGamma)
      {
	TreeIO io;
	oss << io.writeGuestTree(G, gamma)
	    << "; ";
      }
    return oss.str();
  }

  std::string
  ReconSeqApproximator::ownHeader() const
  {
    std::ostringstream oss;
    oss << nestedLike.strHeader();
    oss << "ortho(orthologypairs); ";
    if(estimateGamma)
      oss << "reconciliation(reconciliation); ";
    return oss.str();
  }

  Probability
  ReconSeqApproximator::updateDataProbability()
  {
    update();
    return calculateDataProbability();
  }

  //-------------------------------------------------------------------
  // ProbabilityModel interface
  //
  // There are two way of doing this currently. 
  // 1. Old style. Get some reconciliations, then compute their likelihoods
  //    by sampling each n_samples (or similar). If we have many 
  //    recociliations, this is expensive. But accurate I guess.
  // 2. New style. Sample reconciliations and compute a likelihood from 
  //    *one* time sample. Full control over the number of samples and, as 
  //    Jens said, Hoefding works anyway. 
  //-------------------------------------------------------------------
  Probability
  ReconSeqApproximator::calculateDataProbability()
  {
    //reset orthology and reconciliations
    orthology.reset();
    reconciliations.clear();
#ifdef ONLY_ONE_TIMESAMPLE
    return simple_sumDataProb(0, 0.0); // Type 2 call
#else
    return cached_sumDataProb(0, 0.0); // Type 1 call
#endif
  }

  // For when G has changed
  //-------------------------------------------------------------------
  void
  ReconSeqApproximator::update()
  {
    //    nestedLike.update();
    sampler.update();
  }


  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const ReconSeqApproximator& A)
  {
    return o << A.print(); 
  }

  string 
  ReconSeqApproximator::print() const
  {
    ostringstream oss;
    oss 
      << "Reconciliation likelihood is calculated using birth-death model\n"
      << "and is approximated by "
      << n_samples
      << " MonteCarlo iterations.\n"
      << "Likelihood calculations is performed by the following models:\n"
      << indentString(nestedLike.print())
      << StdMCMCModel::print()
      ;
    return oss.str();
  }
  

  //-----------------------------------------------------------------------
  //
  // Implementation
  //
  //-----------------------------------------------------------------------
  // This function has not really been tested since update of this file to
  // new class architecture /bens
  Probability
  ReconSeqApproximator::cached_sumDataProb(const unsigned& samples_done,
					   Probability sum)
  {
    // This has not been updated for a long time and 
    // I want to know if we use it
    throw AnError("ReconSeqApproximator:: Someone just used cached_sumDataProb!");
    typedef map<GammaMap,Probability> GammaCacheType;
    GammaCacheType cache;

    for (unsigned i = samples_done; i < n_samples; i++)
      {
	// Get yourself a reconciliation
	GammaMap gamma = sampler.sampleReconciliation();
	//GammaMap gamma = sampler.gamma_star;

	// Behold the power of STL: We are caching the gammas to see if 
	// we have already computed a mean for it.
	GammaCacheType::iterator cached_item = cache.find(gamma);

	Probability term;
	if (cached_item == cache.end()) // See if it is in cache. If not,...
	  {
	    // ... and use it to approximate $\Pr(\tau | \gamma, G, etc.)$.
	    term = nestedLike.initStateProb();
	    //	  term = 1.0;
	    cache[gamma] = term;
	  }
	else  // Already computed this one: pull out the old result
	  {
	    term = (*cached_item).second;
	  }
	sum += term;

	// Add support for various orthology pairs
	orthology.recordOrthology(gamma, term);

      }

    //  cerr << "#reconciliations: " << cache.size() << endl;

    // Replacing: scale(orthology, 1.0 / sum); // Needs changing if own OrthologyMatrix 
    // with: 
    for (unsigned i=0; i<orthology.nrows(); i++)
      {
	for (unsigned j=0; j<orthology.ncols(); j++)
	  {
	    orthology(i, j) /= sum;
	  }
	   
      }
    return sum / n_samples;
  }


  Probability
  ReconSeqApproximator::simple_sumDataProb(const unsigned& samples_done, 
					   Probability sum)
  {
//      const GammaMap& gamma = sampler.accessGamma();
    for (unsigned i = samples_done; i < n_samples; i++)
      {
	// Get yourself a reconciliation
 	sampler.generateReconciliation();
	// ... and use it to approximate $\Pr(\tau | \gamma, G, etc.)$.
	Probability term = nestedLike.initStateProb();
	sum += term;

	// Add support for various orthology pairs
	orthology.recordOrthology(*gamma, term);
	if(estimateGamma)
	  {
	    TreeIO io;
	    reconciliations[io.writeGuestTree(G,gamma)] += term;
	  }
      }
    orthology.scale(1/sum);
    if(estimateGamma)
      {
	for_each(reconciliations.begin(), reconciliations.end(), 
		 pair_divide<string, Probability>(sum));
      }
    return sum / n_samples;
  }

} //end namespace beep
