#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <sys/types.h>
#include <algorithm>

#include "Beep.hh"
#include "fastGEM_BirthDeathMCMC.hh"
#include "Density2PMCMC.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeWeightMCMC.hh"
#include "fastGEM.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LengthRateModel.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "PRNG.hh"
#include "ReconciledTreeTimeMCMC.hh"
#include "ReconciliationTimeSampler.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SimpleML.hh"
#include "SiteRateHandler.hh"
#include "SubstitutionMCMC.hh"
#include "TopTimeMCMC.hh"
#include "RandomTreeGenerator.hh"
#include "TreeIO.hh"
#include "TreeMCMC.hh"
#include "UniformDensity.hh"
#include "VarRateModel.hh"


//-------------------------------------------------------------
//
//-------------------------------------------------------------




// Global options with default settings
//------------------------------------
int nParams = 2+1; // currently Stree_file has to be an argument rather than option

char* outfile            = NULL;
unsigned MaxIter         = 200000;
unsigned Thinning        = 100;
unsigned printFactor     = 1;
unsigned RSeed           = 0;
bool quiet               = false;
bool show_debug_info     = false;
bool do_likelihood       = false;
bool do_ML               = false;

char* Stree_file         = NULL;
char* Gtree_file         = NULL;

std::string seqModel     = "JC69";
std::string seqType;
std::vector<double> Pi;
std::vector<double> R;
unsigned n_cats          = 1;
double alpha             = 1.0;
bool estimateAlpha       = true;

std::string rateModel    = "IID";
std::string density      = "GAMMA";
double mean              = 1.0;
double variance          = 1.0;
bool fixed_density       = false;
bool fixed_weights        = false;

bool fixed_tree          = false;
std::vector<std::string> outgroup;

double topTime           = 1.0;
double beta              = -1.0;
double birthRate         = 1.0;
double deathRate         = 1.0;
bool fixedBDParams       = false;

unsigned noOfDiscrIntervals = 25;
double ewmSuggestRatio = 1.0;

// allowed models
std::vector<std::string> subst_models;
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
	using namespace beep;
	using namespace std;

	// Set up allowed models
	//! \todo{ This should be replaced by a factory function that
	//! includes the checking /bens}
	//-------------------------------------------------------------
	//Substitution model
	subst_models.push_back("UNIFORMAA");
	subst_models.push_back("JTT");
	subst_models.push_back("UNIFORMCODON");
	subst_models.push_back("ARVECODON");
	subst_models.push_back("JC69");

	//rate model
	rate_models.push_back("IID");
	rate_models.push_back("GBM");
	rate_models.push_back("CONST");

	//rate density
	rate_densities.push_back("UNIFORM");
	rate_densities.push_back("GAMMA");
	rate_densities.push_back("LOGN");
	rate_densities.push_back("INVG");

	if (argc < nParams)
	{
		usage(argv[0]);
		exit(0);
	}
	try
	{
		ostringstream preruninfo;

		int opt = readOptions(argc, argv);

		//Set up random number generator and "end MCMCModel"
		//---------------------------------------------------------
		PRNG rand;
		unsigned Rseed = 0;
		if (RSeed != 0)             // Use user-defined random seed
		{
			rand.setSeed(RSeed);
		}
		else
		{
			Rseed = rand.getSeed();
		}

		// For practical reasons, set up the SubstitutionMatrix already here
		//---------------------------------------------------------
		MatrixTransitionHandler* Q;
		if(seqModel == "USR")
		{
			Q = new MatrixTransitionHandler(MatrixTransitionHandler::userDefined(seqType, Pi, R));
		}
		else
		{
			Q = new MatrixTransitionHandler(MatrixTransitionHandler::create(seqModel));
		}

		//Get sequence data
		//---------------------------------------------
		string datafile(argv[opt++]);
		SequenceData D = SeqIO::readSequences(datafile, Q->getType());
		// Notice that Q was used here!

		// Set up host tree
		//---------------------------------------------
		Tree* S;
		Stree_file = argv[opt++];

		if(Stree_file)
		{
			TreeIO io = TreeIO::fromFile(Stree_file);
			S = new Tree(io.readHostTree());
		}
		else
		{
			S = new Tree(Tree::EmptyTree());
		}

		// joelgs: We must rescale S in case root-to-leaf time is significantly
		// different from 1.0 since so many parameters are hard-tuned for this scale.
		// Leaf time is assumed to be 0.
		Real rlt = S->rootToLeafTime();
		if (rlt >= 2.0 || rlt <= 0.5)
		{
			RealVector* tms = new RealVector(S->getTimes());
			for (RealVector::iterator it = tms->begin(); it != tms->end(); ++it)
			{
				(*it) /= rlt;
			}
			S->setTopTime(S->getTopTime() / rlt);
			S->setTimes(*tms, true);
			preruninfo << "# NOTE: Host tree root-to-leaf span was rescaled from " << rlt << " to 1.0.\n";
		}

		// Set up guest tree
		//---------------------------------------------
		StrStrMap gs;
		Tree* G;
		if(Gtree_file)
		{
			TreeIO io = TreeIO::fromFile(Gtree_file);
			G = new Tree(io.readBeepTree(NULL,&gs));
			G->doDeleteLengths();
		}
		else
		{
			G = new Tree(RandomTreeGenerator::generateRandomTree(D.getAllSequenceNames()));
		}

		// If gs is empty (no mapping info was found in the gene tree or a
		// random tree was created), we have to read a file with that info.
		if (gs.size() == 0)
		{
			if( opt < argc)
			{
				gs = TreeIO::readGeneSpeciesInfo(string(argv[opt++]));
			}
			else
			{
				cerr << "Missing gene-t-species leaf mapping\n";
				usage(argv[0]);
			}
		}

		// Set up priors
		//-------------------------------------------------------
		DummyMCMC dm;

		if(beta < 0)
		{
			beta = std::max(S->getRootNode()->getTime(), S->rootToLeafTime());
		}

		TopTimeMCMC stm(dm, *S, beta);
		if(topTime > 0.0) // Set user-defined topTime, assumed to be fixed
		{
			stm.setTopTime(topTime);
			stm.fixTopTime();
		}
		stm.fixTopTime();

		unsigned noOfDiscrPoints = 2 * noOfDiscrIntervals + 1;
		std::vector<double>* discrPoints;
		discrPoints = new std::vector<double>;

		fastGEM_BirthDeathMCMC bdm(stm, *S,  noOfDiscrPoints, discrPoints,
				birthRate, deathRate, &stm.getTopTime());
		if(fixedBDParams)
		{
			bdm.fixRates();
		}

		// joelgs: Antar att detta syftar till att sampla startlängder medelst mest parsimona
		// rekonsiliering? Nåja, verkar inte funka, så jag tar bort'et så länge.
		//if(G->hasTimes() == false)
		//{
		//  cerr << "Generating times (Varf�r sker detta? /Lasse)\n";
		// GammaMap gamma = GammaMap::MostParsimonious(*G,*S,gs);
		//ReconciliationTimeSampler rts(*G, bdm, gamma);
		//  rts.sampleTimes();
		//}

		// joelgs, G should only have lengths, right?
		// Other characteristics may cause problems when branch swapping.
		G->doDeleteRates();
		G->doDeleteTimes();

		UniformTreeMCMC utm(bdm, *G, 3.0);
		if(fixed_tree)
		{
			utm.fixTree();
			utm.fixRoot();
		}

		// Set up Density function for rates
		//---------------------------------------------------------
		Density2P* df = 0;
		if(density == "INVG")
		{
			df = new InvGaussDensity(mean, variance);
		}
		else if(density == "LOGN")
		{
			df = new LogNormDensity(mean, variance);
		}
		else if(density == "GAMMA")
		{
			df = new GammaDensity(mean, variance);
		}
		else if(density == "UNIFORM")
		{
			df = new UniformDensity(mean, variance, true);
			cerr << "*******************************************************\n"
					<< "Note! mean and variance will always be fixed when using\n"
					<< "UniformDensity, the default interval will be (0,10)\n"
					<< "You might want to use the -Ef option\n"
					<< "*******************************************************\n";
			fixed_density = true;
		}
		else
		{
			cerr << "Expected 'InvG', 'LogN', 'Gamma' or 'Const' "
					<< "for option -d\n";
			usage(argv[0]);
			exit(1);
		}

		Density2PMCMC d2m(utm, *df);
		if(fixed_density)    // user has requested that mean and
		{                  // variance should not be perturbed
			d2m.fixMean();
			d2m.fixVariance();
		}

		// Set up rates = lengths/times ...
		//---------------------------------------------------------
		fastGEM irm(*G,*S,gs,df,bdm,discrPoints,noOfDiscrPoints);

		EdgeWeightMCMC ewm(d2m, irm, ewmSuggestRatio);
		//      EdgeWeightMCMC ewm(d2m, irm);
		if(fixed_weights)
		{
			cerr << "cannot fix weights yet\n";
			exit(1);
			// 	  irm.setWeightVector(G->getLengths());
			//  	  ewm.fixWeights();
		}
		else
		{
			ewm.generateWeights();
		}

		// ... and edge weights
		//---------------------------------------------------------
		EdgeWeightHandler ewh(irm); // this equates lengths with rates
		//       cerr << "G with lengths:\n" << G->print(true,true,true,true);

		// Set up Site rates
		//---------------------------------------------------------
		UniformDensity uf(0, 3, true); // U[0,3]
		ConstRateMCMC alphaC(ewm, uf, *G, "Alpha");
		SiteRateHandler srm(n_cats, alphaC);

		if(n_cats == 1) // then it is meaningless to perturb alpha
		{
			alphaC.fixRates();
		}

		// ... and SubstitutionMCMC
		//---------------------------------------------------------
		vector<string> partitionList ;  //Will we ever use this?
		partitionList.push_back(string("all"));
		SubstitutionMCMC sm(alphaC, D, *G, srm, *Q, ewh, partitionList);

		// Create MCMC handler
		//---------------------------------------------
		SimpleMCMC* iterator;
		if(do_ML)
		{
			iterator = new SimpleML(sm, Thinning);
		}
		else
		{
			iterator = new SimpleMCMC(sm, Thinning);
		}

		if (do_likelihood)  // No MCMC
		{
			cout << sm.currentStateProb() << endl;
			exit(0);
		}

		if (outfile != NULL) //redirect stdout
		{
			try
			{
				iterator->setOutputFile(outfile);
				cout << preruninfo.str();
			}
			catch(AnError& e)
			{
				e.action();
			}
			catch (int e)
			{
				cerr << "Problems opening output file! ('"
						<< outfile
						<< "') Using stdout instead.\n";
			}
		}

		if (quiet)  // No stderr output
		{
			iterator->setShowDiagnostics(false);
		}

		if (!quiet) // Tell the user we start MCMC and output some info
		{
			cerr << "Running: ";
			for(int i = 0; i < argc; i++)
			{
				cerr << argv[i] << " ";
			}
			cerr << " in directory"
					<< getenv("PWD")
					<< "\n\n"
					<< "Start ";
			if(do_ML)
				cerr << "ML";
			else
				cerr << "MCMC";
			cerr << " (Seed = " << rand.getSeed() << ")\n";
		}

		cout << "#Running: ";
		for(int i = 0; i < argc; i++)
		{
			cout << argv[i] << " ";
		}
		cout << " in directory"
				<< getenv("PWD")
				<< "\n#\n"
				<< "#Start ";
		if(do_ML)
			cout << "ML";
		else
			cout << "MCMC";
		cout << " (Seed = " << rand.getSeed() << ")\n";
		if(fixed_tree)
		{
			cout << "# The tree is fixed so let's print out it out in\n"
					<< "# in PRIME format with ID's to make it easy to relate\n"
					<< "# the edge lengths back to the tree:\n";
			cout << "# " << TreeIO::writeGuestTree(*G) << endl;;
		}
		cout << "# Let's iterate!\n";

		// Perform and time the Likelihood calculation
		time_t t0 = time(0);
		clock_t ct0 = clock();
		iterator->iterate(MaxIter, printFactor);

		time_t t1 = time(0);

		clock_t ct1 = clock();
		cout << "# Wall time: " << difftime(t1, t0) << " s"
				<< endl
				<< "# CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
				<< endl;

		if (!quiet) // output some run statistics
		{
			cerr << sm.getAcceptanceRatio()
	    		   << " = acceptance ratio\n";
		}
		if (sm.getAcceptanceRatio() == 0)
		{
			cerr << "Warning! MCMC acceptance ratio was 0.\n";
		}

		// Important: finally clean up memory
		delete df;
		delete iterator;
	}
	catch(AnError& e)       // AnError or an exception has been thrown
	{
		cout <<" Error\n";    // tell user about it
		e.action();
	}
	catch(exception& e)       // AnError or an exception has been thrown
	{
		cout <<" Exception\n"     // tell user about it
				<< e.what();
	}
}


void 
usage(char *cmd)
{
	using std::cerr;
	cerr
	<< "Usage: "
	<< cmd
	<< " [<options>] <datafile> "
	<< " <hostfile> "
	<< "[<gs>]\n"
	<< "\n"
	<< "Gene tree reconstruction using the GSR model: the gene tree and its\n"
	<< "divergence times are modeled with the gene evolution model, \n"
	<< "substitution rate variation with an iid model with a user-defined\n"
	<< "underlying density and sequence evolution by a user-defined\n"
	<< "substitution model\n"
	<< "\n"
	<< "Note: Be advised that if the host tree's root-to-leaf time is >=2 or\n"
	<< "<=0.5, the tree will be rescaled internally so that it equals 1.0.\n"
	<< "Inferred parameters will then refer to this and not original scale.\n"
	<< "\n"
	<< "Parameters:\n"
	<< "   <datafile>         (string) name of file with aligned sequences\n"
	<< "   <hostfile>         (string) name of file with host tree\n"
	<< "   <gs>               (string, optional) name of file with\n"
	<< "                      leaf-to-leaf map from host tree to guest tree\n"


	<< "Options:\n"
	<< "  -u, -h          This text.\n"
	<< "  -v              Version information.\n"
	<< "  -o <filename>   Output file\n"
	<< "  -i <float>      Number of iterations\n"
	<< "  -t <float>      Thinning\n"
	<< "  -w <float>      Output to cerr <float> times less often than to cout\n"
	<< "  -s <int>        Seed for pseudo-random number generator. If not given\n"
	<< "                  the process ID will be used\n"
	<< "  -q              Do not output diagnostics to stderr.\n"
	//     << "  -d              Debug info.\n"
	<< "  -l              Output likelihood. No MCMC.\n"
	<< "  -m              Do maximum likelihood. No MCMC.\n"
	<< "  -S<option>      Options related to Substitution model\n"
	<< "   -Sm <'JC69'/'UniformAA'/'JTT'/'UniformCodon'>\n"
	<< "                  The substitution model to use, (JC69 is the default).\n"
	<< "                  Must fit data type \n"
	<< "   -Su <datatype='DNA'/'AminoAcid'/'Codon'> \n"
	<< "       <Pi=float1 float2 ... floatn>\n"
	<< "       <R=float1 float2 ...float(n*(n-1)/2)>\n"
	<< "                  The user-defined substitution model to use.The size of\n"
	<< "                  pi and R must fit data type (DNA: n=4, AminoAcid: n=20,\n"
	<< "                  Codon: n = 62), respectively. Notice that R is given as\n"
	<< "                  a upper triangular matrix. If both -Su and -Sm is given \n"
	<< "                  (don't do this!), only the last given is used\n"
	<< "   -Sn <float>    nCats, the number of discrete rate categories (default 1)\n"
	<< "   -Sa <float>    alpha, the shape parameter of the Gamma distribution for\n"
	<< "                  site rates (default: 1)\n"
	<< "  -E<option>      Options relating to edge rate model\n"
	// Change rate options to fit �rjan's model
	//     << "   -Em <'iid'/'const'> \n"
	//     << "                  The edge rate model to use (default: iid)\n"
	<< "   -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'> \n"
	<< "                  Underlying density for edge rate model (default Gamma)\n"
	<< "   -Ef <float> <float> \n"
	<< "                  Fixed mean and variance of edge rate model\n"
	//     << "  -H<option>      Options relating to the host tree\n"
	//     << "   -Hi <treefile> Use tree in file <treefile> as host tree\n"
	<< "  -G<option>      Options relating to the tree\n"
	<< "   -Gg            Fix guest tree. No branchswapping performed\n"
	<< "   -Gi <treefile> Use tree in file <treefile> as start guest tree\n"
	//     << "   -Go { <outgroup1> <outgroup2 ... }\n"
	//     << "                  Root tree at smallest subtree containing leaves\n"
	//     << "                  given by Go's argument (note spaces after '{'and\n"
	//     << "                  before '}'. Not meaningful together with -Hi\n"
	<< "  -B<option>      Options relating to the birth death process\n"
	<< "   -Bp <float> <float> \n"
	<< "                  Starting values for duplication and loss rates.\n"
	<< "   -Bf <float> <float>\n"
	<< "                  Fix duplication and loss rates to these values.\n"
	<< "   -Bt <float>    Fix top time to this value, i.e., the value tells how long\n"
	<< "                  before the host root the gene evolution process started\n"
	;
}

int
readOptions(int argc, char **argv) 
{
	using namespace beep;
	using namespace std;

	int opt=1;
	while (opt < argc && argv[opt][0] == '-')
	{
		switch (argv[opt][1])
		{
		case 'h':
		case 'u':
		{
			usage(argv[0]);
			exit(0);
		}
		case 'v':
			cout << "We need to fix this... version number missing" << endl;
			exit(0);
		case 'o':
		{
			if (opt + 1 < argc)
			{
				outfile = argv[++opt];
			}
			else
			{
				cerr << "Expected filename after option '-o'\n";
				usage(argv[0]);
				exit(1);
			}
			break;
		}
		case 'i':
		{
			if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
			{
				cerr << "Expected integer after option '-i'\n";
				usage(argv[0]);
				exit(1);
			}
			break;
		}
		case 't':
		{
			if (sscanf(argv[++opt], "%d", &Thinning) == 0)
			{
				cerr << "Expected integer after option '-t'\n";
				usage(argv[0]);
				exit(1);
			}
			break;
		}
		case 'w':
		{
			if (sscanf(argv[++opt], "%d", &printFactor) == 0)
			{
				cerr << "Expected integer after option '-w'\n";
				usage(argv[0]);
				exit(1);
			}
			break;
		}
		case 's':
			if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
			{
				cerr << "Expected integer after option '-s'\n";
				usage(argv[0]);
				exit(1);
			}
			break;

		case 'q':
		{
			quiet = true;
			break;
		}
		case 'd':
		{
			show_debug_info = true;
			break;
		}
		case 'l':
		{
			do_likelihood = true;
			break;
		}
		case 'm':
		{
			do_ML = true;
			break;
		}
		case 'S':
		{
			switch(argv[opt][2])
			{
			case 'm':
		{
			if (opt + 1 < argc)
			{
				seqModel = argv[++opt];
				capitalize(seqModel);
				if(find(subst_models.begin(), subst_models.end(), seqModel)
						== subst_models.end())
				{
					cerr << "Expected 'UniformAA', 'JTT', 'UniformCodon' "
							<< "or 'JC69' for option -Sm\n";
					usage(argv[0]);
					exit(1); // check which error code to use!
				}
			}
			else
			{
				cerr << "Expected seqModel after option '-m'\n";
				usage(argv[0]);
				exit(1);
			}
			break;
		}
			case 'u':
			{
				if (opt + 1 < argc)
				{
					seqModel = "USR";
					seqType = argv[++opt];
					capitalize(seqType);
					int dim = 0;
					if(seqType == "DNA")
						dim = 4;
					else if(seqType == "AMINOACID")
						dim = 20;
					else if(seqType == "CODON")
						dim = 61;
					else
					{
						cerr << seqType
								<< " is not a valid data type for option -Su\n";
						usage(argv[0]);
						exit(1); // check which error code to use!
					}
					if(opt + (dim * (dim + 1) / 2) < argc)
					{
						for(int i = 0; i < dim; i++)
						{
							if(argv[++opt][0] == '-')
							{
								cerr << "Inappropriate state for Pi: "
										<< argv[opt]
										        << " in option -Su\n";
								usage(argv[0]);
								exit(1); // check which error code to use!
							}

							Pi.push_back(atof(argv[opt]));
						}
						for(int i= 0; i < dim * (dim - 1) /2; i++)
						{
							if(argv[++opt][0] == '-')
							{
								cerr << "Inappropriate state for R: "
										<< argv[opt]
										        << " in option -Su\n";
								usage(argv[0]);
								exit(1); // check which error code to use!
							}

							R.push_back(atof(argv[opt]));
						}
					}
					else
					{
						cerr << "Too few parameters to -Su "
								<< seqType
								<< "\n";
						usage(argv[0]);
						exit(1); // check which error code to use!
					}
				}
				else
				{
					cerr << "Expected seqModel after option '-m'\n";
					usage(argv[0]);
				}
				break;
			}
			case 'n':
			{
				if (++opt < argc)
				{
					n_cats = atoi(argv[opt]);
				}
				else
				{
					std::cerr << "Expected int (number of site rate\n"
							<< "classes) for option 'n'\n";
					usage(argv[0]);
					exit(1);
				}
				break;
			}
			case 'a':
			{
				if (++opt < argc)
				{
					alpha = atof(argv[opt]);
					estimateAlpha = false;
				}
				else
				{
					cerr << "Expected float (shape parameter for site rate\n"
							<< "variation) for option '-a'!\n";
					usage(argv[0]);
					exit(1);
				}
				break;
			}
			// 	    case 'p':
			// 	      	{
			// 		  if (++opt < argc)
			// 		    {
			// 		      pinv = atof(argv[opt]);
			// 		    }
			// 		  else
			// 		    {
			// 		      cerr << "Expected float (probability of a site being\n"
			// 			   << "invariant) for option '-p'!\n";
			// 		      usage(argv[0]);
			// 		    }
			// 		  break;
			// 		}
			default:
			{
				cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
				usage(argv[0]);
				exit(1); //Check for correct error code
			}
			break;
			}
			break;
		}
		case 'E':
		{
			switch(argv[opt][2])
			{
			// 	      case 'm':
			// 		{
			// 		  if (opt + 1 < argc)
			// 		    {
			// 		      rateModel = argv[++opt];
			// 		      capitalize(rateModel);
			// 		      if(find(rate_models.begin(), rate_models.end(), rateModel)
			// 			 == rate_models.end())
			// 			{
			// 			  cerr << "Model "
			// 			       << argv[opt]
			// 			       << "does not exist\n"
			// 			       << "Expected 'iid', ''gbm' or 'const' "
			// 			       << "after option '-Em'\n";
			// 			  usage(argv[0]);
			// 			  exit(1); //Check what error code should be used!
			// 			}
			// 		      // else we use default = const
			// 		    }
			// 		  else
			// 		    {
			// 		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			// 			   << "option '-Em'\n";
			// 		      usage(argv[0]);
			// 		      exit(1); //Check what error code should be used!
			// 		    }
			// 		  break;
			// 		}
			case 'd':
			{
				if (opt + 1 < argc)
				{
					density = argv[++opt];
					capitalize(density);
				}
				else
				{
					cerr << "Expected density after option '-d'\n";
					usage(argv[0]);
					exit(1);
				}
				break;
			}
			case 'f':
			{
				if (++opt < argc)
				{
					mean = atof(argv[opt]);
					if (++opt < argc)
					{
						variance = atof(argv[opt]);
						fixed_density = true;
					}
					else
					{
						cerr << "Expected float (variance for "
								<< "edge rates)\n";
						usage(argv[0]);
						exit(1);
					}
				}
				else
				{
					cerr << "Expected pair of floats (mean and variance\n"
							<< "for edge rates) for option '-p' or '-f'!\n";
					usage(argv[0]);
					exit(1);
				}
				break;
			}
			default:
			{
				cerr << "Warning: Unknown option '" << argv[opt]
				                                            << "'!\n\n";
				usage(argv[0]);
				exit(1); // Check for correct error code
			}
			}
			break;
		}
		case 'H':
		{
			switch(argv[opt][2])
			{
			// 	      case 'i':
			// 		{
			// 		  if (opt + 1 < argc)
			// 		    {
			// 		      Stree_file = argv[++opt];
			// 		      break;
			// 		    }
			// 		  else
			// 		    {
			// 		      cerr << "Expected file name (string) after -Hi\n";
			// 		      usage(argv[0]);
			// 		      exit(1); //Check what error code should be used!
			// 		    }
			// 		}
			default:
			{
				cerr << "Warning: Unknown option '" << argv[opt]
				                                            << "'!\n\n";
				usage(argv[0]);
				exit(1); // Check for correct error code
			}
			}
			break;
		}
		case 'G':
		{
			switch(argv[opt][2])
			{
			case 'g':
		{
			fixed_tree = true;
			break;
		}
			case 'i':
			{
				if (opt + 1 < argc)
				{
					Gtree_file = argv[++opt];
					break;
				}
				else
				{
					cerr << "Expected file name (string) after -Gi\n";
					usage(argv[0]);
					exit(1); //Check what error code should be used!
				}
			}
			// 	      case 'o':
			// 		{
			// 		  if (string(argv[++opt])  == "{")
			// 		    {
			// 		      opt++;
			// 		      while(string(argv[opt]) != "}")
			// 			{
			// 			  outgroup.push_back(argv[opt++]);
			// 			}
			// 		      if(outgroup.empty())
			// 			{
			// 			  cerr << "Set of outgroup names expected after "
			// 			       << "option -Go\n";
			// 			  usage(argv[0]);
			// 			  exit(1);
			// 			}
			// 		    }
			// 		  else
			// 		    {
			// 		      cerr << "Expected outgroup names as strings after -Go\n";
			// 		      usage(argv[0]);
			// 		      exit(1); //Check what error code should be used!
			// 		    }
			// 		  break;
			// 		}
			default:
			{
				cerr << "Warning: Unknown option '" << argv[opt]
				                                            << "'!\n\n";
				usage(argv[0]);
				exit(1); // Check for correct error code
			}
			}
			break;
		}
		case 'B':
		{
			switch(argv[opt][2])
			{
			case 'f':
		{
			fixedBDParams = true;
		}
			case 'p':
			{
				if (opt + 1 < argc)
				{
					birthRate = atof(argv[++opt]);
					if (opt + 1 < argc)
					{
						deathRate = atof(argv[++opt]);
					}
					else
					{
						cerr << "Expected birth and death param (float) after -Bp/f\n";
						usage(argv[0]);
						exit(1);
					}
				}
				else
				{
					cerr << "Expected birth and death param (float) after -Bp/f\n";
					usage(argv[0]);
					exit(1);
				}
				break;
			}
			case 't':
			{
				if (opt + 1 < argc)
				{
					topTime = atof(argv[++opt]);
				}
				else
				{
					cerr << "Expected top time param (float) after -Bt\n";
					usage(argv[0]);
					exit(1);
				}
				break;
			}
			case 'b':
			{
				if (opt + 1 < argc)
				{
					beta = atof(argv[++opt]);
				}
				else
				{
					cerr << "Expected beta param (float) after -Bb\n";
					usage(argv[0]);
					exit(1);
				}
				break;
			}
			default:
			{
				cerr << "Warning: Unknown option '" << argv[opt]
				                                            << "'!\n\n";
				usage(argv[0]);
				exit(1); // Check for correct error code
			}
			}
			break;
		}
		default:
		{
			cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
			usage(argv[0]);
			exit(1); // Check for correct error code
		}
		break;
		}
		opt++;
	}
	return opt;
};



