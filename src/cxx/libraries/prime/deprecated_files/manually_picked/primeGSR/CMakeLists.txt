include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)

add_executable(${programname_of_this_subdir}
jarden.cc
)

target_link_libraries(${programname_of_this_subdir} prime-phylo prime-phylo-sfile)

install(TARGETS ${programname_of_this_subdir} DESTINATION bin)
