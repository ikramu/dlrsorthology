#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "Beep.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "PRNG.hh"
#include "ReconciliationSampler.hh"
#include "StrStrMap.hh"
#include "Tree.hh"
#include "TreeIO.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <guest tree> <species tree> <gs> <birthrate> <deathrate> [<seed>]\n"
       << "\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  if (argc < 6 || argc > 7) 
    {
      usage(argv[0]);
    }

  try
    {
      // get parameter from arguments
      //---------------------------------------------------------
      string gfile = argv[1];
      string sfile = argv[2];
      string gsfile = argv[3];
      Real lambda = atof(argv[4]);
      Real mu = atof(argv[5]);
      PRNG R;			// Seeded by process id
      if(argc == 7)
	{
	  R.setSeed(atof(argv[6]));
	}

      TreeIO io = TreeIO::fromFile(gfile);
      Tree G = io.readGuestTree();
      io.setSourceFile(sfile);
      Tree S = io.readHostTree();
//       io.setSourceFile(prefix+".gs");
      StrStrMap gs = io.readGeneSpeciesInfo(gsfile);
//       LambdaMap l(G, S, gs)
//       GammaMap gamma = GammaMap::MostParsimonious(G,S,l);

      //     T.debug_print();
      
      // Set up the model for the species tree
      //---------------------------------------------------------
//       cout<<"Set up a model for the tree\n"
// 	  <<"---------------------------------------------\n";
      BirthDeathProbs BDP(S, lambda, mu);
      ReconciliationSampler RS(G, gs, BDP);
      GammaMap origgamma = RS.getGamma();
      cout << origgamma << endl;//io.writeGuestTree(G, &origgamma) << endl << endl;
      float c = 0;
      for(unsigned i = 0; i < 1000; i+=1)
	{
	  GammaMap gamma = RS.sampleReconciliation();
	  if(io.writeGuestTree(G, &origgamma) != io.writeGuestTree(G, &gamma))
	    cout << gamma << endl;//io.writeGuestTree(G, &gamma) << endl << endl;
	  else
	    c+=1.0;
	}
      cout << "freq[origgamma] = " << c/1000 << endl;
    }
  catch (AnError e)
    {
      cerr << "error found\n";
      e.action();
    }
}
