#include "AnError.hh"
#include "Beep.hh"
#include "BirthDeathMCMC.hh"
#include "Density2PMCMC.hh"
#include "DummyMCMC.hh"
#include "EdgeDiscTree.hh"
#include "EdgeDiscBDMCMC.hh"
#include "EdgeDiscBDProbs.hh"
#include "EdgeDiscGSR.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeWeightMCMC.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "MultiGSR.hh"
#include "PrimeOptionMap.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "RandomTreeGenerator.hh"
#include "ReconciledTreeTimeMCMC.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SimpleML.hh"
#include "SiteRateHandler.hh"
#include "StrStrMap.hh"
#include "SubstitutionMCMC.hh"
#include "TreeDiscretizers.hh"
#include "TreeIO.hh"
#include "TreeMCMC.hh"
#include "UniformDensity.hh"

#include "cmdline.h"


#include <iostream>

// Suggestion rates. Affects perturbation frequency of the various parameters.
static const double   SUGG_RATIO_BD_RATES     = 1.0;
static const double   SUGG_RATIO_G_TOPO_START = 3.0; // Initial value.
static const double   SUGG_RATIO_G_TOPO_FINAL = 3.0; // Final value.
static const unsigned SUGG_RATIO_G_TOPO_STEPS = 1;   // Steps between start and final.
static const double   SUGG_RATIO_G_LENGTHS    = 1.0;

static const int NO_OF_PARAMS = 2;
static const int NO_OF_OPTIONAL_PARAMS = 1;

// Program arguments. Listed in expected input order.
std::vector<std::string> ParamSeqDataFile;   // 1. sequence data file name
std::string ParamSTreeFile   = "";
std::string ParamGSMapFile   = "";  // Optional.

// All options to program (and default values) in a map.
beep::PrimeOptionMap Options;	

// Forward declaration of helpers.
void readParameters(int& argIndex, 
		    int argc, 
		    char **argv);
void createUnderlyingObjects(std::vector<beep::MatrixTransitionHandler>& Q,
			     std:: vector<beep::SequenceData>& D,
			     beep::Tree*& S, 
// 			     beep::EdgeDiscTree*& DS, 
			     std::vector<beep::Tree>& G, 
			     std::vector<beep::StrStrMap>& gsMap, 
			     beep::LSDProbs* lsd);
beep::Density2P* createDensity2P(std::string id, 
				 double mean, 
				 double variance);
void createDefaultOptions(std::string progName);
void iterate(beep::StdMCMCModel* mcmcEnd, 
	     std::string runType, 
	     std::string preamble,
	     beep::MultiGSR& mmg);

int debugVar = 0;

/************************************************************************
 * Main program. See help message method for program description.
 ************************************************************************/
int main(int argc, char **argv)
{
  using namespace beep;
  using namespace std;

  try
    {
      // Create default options. Read options and arguments.
      createDefaultOptions(argv[0]);
      int argIndex = 1;
      if (!Options.parseOptions(argIndex, argc, argv))
	{
	  cerr << Options.getUsage();
	  exit(0);
	}
      readParameters(argIndex, argc, argv);

      // Pseudo random number generator.
      PRNG rand;
      int seed = Options.getUnsigned("PRNG Seed")[0];
      if(seed == 0) 
	{ 
	  seed = rand.getSeed(); 
	}
      rand.setSeed(seed);    
      cerr << "Seed = " << seed << endl;

      // Create substitution matrix, sequence data, trees, etc.
      vector<MatrixTransitionHandler> Q;
      vector<SequenceData> D;
      Tree* S;
      vector<Tree> G;
      vector<StrStrMap> gsMap;
      LSDProbs* lsd = NULL;
      createUnderlyingObjects(Q, D, S, G, gsMap, lsd);

      // MCMC: End-of-chain.
      
      Real rootTime = 1.0;
      // MCMC: Species tree MCMC
      if(S->hasTimes())
	{
	  rootTime = S->rootToLeafTime();
	}
       if(S->getName() == "")
 	{
 	  S->setName("S");
 	}

      DummyMCMC mcmcDummy = DummyMCMC();
      MCMCModel* prior = &mcmcDummy;

      Tree dummyTree = Tree::EmptyTree(rootTime);
      dummyTree.setName("Dummy");
      StrStrMapDummy dummyGs(dummyTree.getRootNode()->getName());
      
      unsigned int noOfIvs = Options.getUnsigned("No. of root-to-leaf ivs")[0];
      cerr << "noOfIvs = " << noOfIvs << endl;
      if (noOfIvs == 0) 
	{
	  noOfIvs = 100; 
	}
      BirthDeathMCMC sbdm(*prior, dummyTree, 1.0, 1.0);
      ReconciledTreeTimeMCMC* srtm;
      srtm =  new ReconciledTreeTimeMCMC(sbdm, *S, dummyGs, sbdm, 
					 dummyTree.getTopTime() / noOfIvs,
					 true, "S_times");
      srtm->chooseStartingRates();
      cerr << *S;
      if(true)
	{
	  prior = srtm;
	}
      // Create discretized tree DS based on S.
      EdgeDiscTree* DS;
      Real timestep = Options.getReal("DiscTimestep")[0];
      unsigned minIvs = Options.getUnsigned("DiscMinIvs")[0];
      unsigned ttIvs = Options.getUnsigned("TopTimeDiscIvs")[0];
      EdgeDiscretizer* discretizer;
      if (timestep == 0)
	{
	  if(ttIvs == 0)
	    {
	      ttIvs = minIvs;
	    }
	  discretizer = new EquiSplitEdgeDiscretizer(minIvs, ttIvs);
	}
      else
	{
	  if (ttIvs == 0) 
	    {
	      ttIvs = std::ceil(S->rootToLeafTime() / timestep); 
	    }
	  discretizer = new StepSizeEdgeDiscretizer(timestep, minIvs, ttIvs);
	}
      DS = new EdgeDiscTree(*S, discretizer);

      MultiGSR mmg(*prior, *DS);
      
      vector<Real> bdRates = Options.getReal("BD rates");
      cerr << "bdRates = [ ";
      for(vector<Real>::iterator i = bdRates.begin(); i !=bdRates.end();i++)
	{
	  cerr << *i << " ";
	}
      cerr << " ]" << endl;
      vector<Real> edgeRateDFParams = Options.getReal("Edge rate DF params");

      vector<Real>::iterator bdi = bdRates.begin();
      vector<Tree>::iterator gti = G.begin();
      vector<StrStrMap>::iterator gsi = gsMap.begin();
      vector<Real>::iterator edi = edgeRateDFParams.begin();
      vector<SequenceData>::iterator sdi = D.begin();
      vector<MatrixTransitionHandler>::iterator qi = Q.begin();
	  

      for(; sdi != D.end(); sdi++, gti++, gsi++)
	{
	  // MCMC: Birth-death rate.
	  if(bdi == bdRates.end()) // indicates shared starting values
	    {
	      bdi = bdRates.begin(); // for all trees
	    }
	  // MCMC: Birth-death rates (duplication-loss rates).
	  EdgeDiscBDProbs* bdp = 
	    new EdgeDiscBDProbs(DS, *(bdi++), *(bdi++));//, *lsd);
	  EdgeDiscBDMCMC* mcmcBDRates = 
	    new EdgeDiscBDMCMC(mcmcDummy, bdp, SUGG_RATIO_BD_RATES);

	  if (Options.getInt("Fixed BD rates")[0] == 0) 
	    { 
	      mcmcBDRates->fixRates(); 
	    };

	  // MCMC: Guest tree topology.
	  UniformTreeMCMC *mcmcGTopo = 
	    new UniformTreeMCMC(*mcmcBDRates, *gti, SUGG_RATIO_G_TOPO_START);
	  mcmcGTopo->setChangingSuggestRatio(SUGG_RATIO_G_TOPO_FINAL, 
					     SUGG_RATIO_G_TOPO_STEPS);
	  if (Options.getInt("Fixed G")[0] == 0)
	    {
	      mcmcGTopo->fixTree();
	      mcmcGTopo->fixRoot();
	    }
// 	  mcmcGTopo->setDetailedNotification(true);

	  // MCMC: Substitution rate variation over edges (IID).
	  string erdfname = Options.getString("Edge rate DF")[0];
	  if(edi == edgeRateDFParams.end()) //indicates shared starting values
	    edi = edgeRateDFParams.begin(); //for all trees
	  Density2P* erdf = createDensity2P(erdfname, *(edi++), *(edi++));
	  Density2PMCMC* mcmcEdgeRate = 
	    new Density2PMCMC(*mcmcGTopo, *erdf, true);
	  if (Options.hasBeenParsed("Fixed edge rate DF params") || erdfname == "UNIFORM")
	    {
	      mcmcEdgeRate->fixMean();
	      mcmcEdgeRate->fixVariance();
	    }

	  // Set up GSR integrator.
// 	  bool sharedRootChildEdges = 
// 	    (Options.getInt("Shared root child edges")[0] == 0);
	  EdgeDiscGSR* gsrModel = 
	    new EdgeDiscGSR(&*gti, DS, &*gsi, erdf, bdp);
// 	  gsrModel->setLSDProbabilities(*lsd);

	  // MCMC: Edge lengths. The GSR class holds these, 
	  // and may therefore be model.
	  EdgeWeightMCMC* mcmcGLengths = 
	    new EdgeWeightMCMC(*mcmcEdgeRate, *gsrModel, 
			       SUGG_RATIO_G_LENGTHS, true);
	  mcmcGLengths->generateWeights(true, 0.05);
	  mcmcGLengths->setDetailedNotification(true);

	  // Set up edge weights, equating weights with lengths.
	  EdgeWeightHandler* ewh = new EdgeWeightHandler(*gsrModel);

	  // MCMC: Uniform prior over [0,3].
	  UniformDensity* srdf = new UniformDensity(0, 3, true);	
	  ConstRateMCMC *mcmcSiteRate = new ConstRateMCMC(*mcmcGLengths, 
							  *srdf, *gti, 
							  "Alpha");

	  // Set up site rate handler.
	  unsigned srdfn = Options.getUnsigned("No. of site rate cats")[0];
	  SiteRateHandler* srh = new SiteRateHandler(srdfn, *mcmcSiteRate);
	  if (srdfn == 1) 
	    {
	      mcmcSiteRate->fixRates(); 
	    }

	  // MCMC: Substitution.
	  vector<string> partList;
	  partList.push_back(string("all"));
	  if(qi == Q.end()) // use same model for all data sets
	    qi = Q.begin();
	  SubstitutionMCMC* mcmcSubst = 
	    new SubstitutionMCMC(*mcmcSiteRate, *sdi, *gti, *srh, *(qi++), 
				 *ewh, partList);

// 	  if(Options.getInt("Show debug info")[0] == 0)
// 	    {
// 	      DS.debugInfo(true);
// 	      bdProbs->debugInfo(true);
// 	      gsrModel->debugInfo(true);
// 	    }
	  
          mmg.addGeneFamily(*mcmcSubst, *mcmcGTopo, 
			    *mcmcBDRates, *mcmcEdgeRate);
	}	

      string runType = Options.getString("Run type")[0];
      
//       cerr << "process " << world.rank() << mmg <<endl;
      // Get pre-run info.
      ostringstream oss;
      oss << "# Running: " << endl << "#";
      for (int i = 0; i < argc; i++) { oss << " " << argv[i]; }
      oss << endl
	  << "# of type " << runType << " with seed " << seed
	  << " in directory " << getenv("PWD") << endl;

      // Iterate according to choice of execution type.
      iterate(&mmg, runType, oss.str(), mmg);

      // Clean up in (somewhat) reverse order of creation.
      delete DS;
      delete S;
      delete lsd;
	}
  catch(AnError& e)
    {
      cerr << "\n" << e.what() << endl;
      cerr << "Use '" << argv[0] << " -h' to display help.";
      e.action();
    }
  catch(MPI::Exception& e)
    {
      cerr << "\n" <<  e.Get_error_string() << endl;
      cerr << "Use '" << argv[0] << " -h' to display help.";
    }
  catch(exception& e)
    {
      cerr << "\n" << e.what() << endl;
      cerr << "Use '" << argv[0] << " -h' to display help.";
    }
  cerr << endl;
}


/************************************************************************
 * Starts MCMC or hill-climbing.
 ************************************************************************/
void iterate(beep::StdMCMCModel* mcmcEnd,
	     std::string runType,
	     std::string preamble,
	     beep::MultiGSR& mmg)
{
  using namespace beep;
  using namespace std;
  bool quiet = (Options.getInt("Quiet run")[0]==0);
  // Iterate according to choice of execution type.
  if (runType == "L")
    {
      // First-iteration posterior density.
      cout << mcmcEnd->currentStateProb() << endl;
      return;
    }
  // MCMC: Iterator.
  unsigned thinning = Options.getUnsigned("Thinning")[0];
  SimpleMCMC* iter;
  if (runType == "PDHC")
    {
      // Posterior density hill-climbing.
      iter = new SimpleML(*mcmcEnd, thinning);
    }
  else
    {
      // MCMC.
      iter = new SimpleMCMC(*mcmcEnd, thinning);
    }
  
  // Output, diagnostics, etc.
  string out = Options.getString("Output file")[0];
  if (out != "")
    {
      try
	{
	  cerr << "trying to set output file\n" ;
	  iter->setOutputFile(out.c_str());
	}
      catch(AnError& e)
	{
	  e.action();
	}
      catch (...)
	{
	  cerr << "Problems opening output file '" << out << "'! "
	       << "Reverting to stdout." << endl;
	}
    }
  if (quiet) 
    {
      iter->setShowDiagnostics(false); 
    }
  cout << preamble;
  
  // Iterate!!!!
  time_t t0 = time(0);
  clock_t ct0 = clock();
  iter->iterate(Options.getUnsigned("No. of iterations")[0], 
		Options.getUnsigned("Print factor")[0]);
  time_t t1 = time(0);
  clock_t ct1 = clock();
  
  cout << "# Wall time: " << difftime(t1, t0) << " s." << endl;
  cout << "# CPU time: " << (Real(ct1 - ct0) / CLOCKS_PER_SEC) << " s."
       << endl;
  cout << "# Total acceptance ratio: "
       << mcmcEnd->getAcceptanceRatio() << endl
       << mcmcEnd->getAcceptanceInfo() << '#';      
  delete iter;
};

/************************************************************************
 * Helper. Creates sequence data, trees, etc.
 * Deletion must be taken care of by invoking method.
 ************************************************************************/
void createUnderlyingObjects(std::vector<beep::MatrixTransitionHandler>& Q,
			     std::vector<beep::SequenceData>& D,
			     beep::Tree*& S,
// 			     beep::EdgeDiscTree*& DS,
			     std::vector<beep::Tree>& G,
			     std::vector<beep::StrStrMap>& gsMap,
			     beep::LSDProbs* lsd)
{
  using namespace std;
  using namespace beep;

  // For practical reasons, set up the substitution matrix first.
  // Use user-defined matrix if such has been provided.
  if (Options.hasBeenParsed("User substitution model"))
    {
      if (Options.hasBeenParsed("Substitution model"))
	{
	  throw AnError("Cannot use both predefined and"
			" user-defined subst. model.");
	}
      // Create a class UserSubstitutionMatrix that takes a vector of 
      // Reals as argument and sets the SubstiMatrix accordingly
      vector<UserSubstMatrixParams> usmp = 
	Options.getUserSubstitutionMatrix("User substitution model");
      for(vector<UserSubstMatrixParams>::iterator i = usmp.begin(); 
	  i != usmp.end(); i++)
	{
	  UserSubstMatrixParams& smUsr = *i;
	  
	  // 	  Q = new 
	  Q.push_back(MatrixTransitionHandler(MatrixTransitionHandler::
					      userDefined(smUsr.seqtype, 
							  smUsr.Pi, 
							  smUsr.R)));
	}
    }
  else
    {
      string sm = Options.getString("Substitution model")[0];
      Q.push_back(MatrixTransitionHandler(MatrixTransitionHandler::create(sm)));
    }
	
  // Get sequence data. This is why Q must already exist.
  vector<MatrixTransitionHandler>::iterator qi = Q.begin();
  for(vector<string>::iterator i = ParamSeqDataFile.begin();
      i != ParamSeqDataFile.end(); i++, qi++)
    {
      if(qi == Q.end()) // i.e., all data set should use the same Q
	qi = Q.begin();
      D.push_back(SequenceData(SeqIO::readSequences(*i,qi->getType())));
    }

  // Create S from file.
  S = new Tree(TreeIO::fromFile(ParamSTreeFile).readBeepTree(0,0));
  if(Options.hasBeenParsed("TopTime"))
    {
      Real tt = Options.getReal("TopTime")[0];
      if (tt <= 0) {
	tt = S->rootToLeafTime();
	cout << "# Changing root edge time span to " << tt << ".\n";
      }
      S->setTopTime(tt);
    }
  if (Options.getInt("Rescale")[0]==0)
    {
      Real sc = S->rootToLeafTime();
      RealVector* tms = new RealVector(S->getTimes());
      for (RealVector::iterator it = tms->begin(); it != tms->end(); ++it)
	{
	  (*it) /= sc;
	}
      S->setTopTime(S->getTopTime() / sc);
      S->setTimes(*tms, true);
      cout << "# Host tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
    }
  if (S->getTopTime() < 1e-8)
    {
      throw AnError("Host tree top time must be greater than 0.");
    }
  
//   // Create discretized tree DS based on S.
//   Real timestep = Options.getDouble("DiscTimestep");
//   unsigned minIvs = Options.getUnsigned("DiscMinIvs");
//   if (timestep == 0)
//     {
//       EquiSplitEdgeDiscretizer disc(minIvs);
//       DS = disc.getDiscretization(*S);
//     }
//   else
//     {
//       StepSizeEdgeDiscretizer disc(timestep, minIvs);
//       DS = disc.getDiscretization(*S);
//     }
  TreeIO tio = TreeIO::fromFile(Options.getString("Guest tree file")[0]);
  if(Options.hasBeenParsed("Guest tree file"))
    {
      G = tio.readAllGuestTrees(NULL, &gsMap);
    }
  else
    {
      for(vector<SequenceData>::iterator di = D.begin();
	  di != D.end(); di++)
	{
	  G.push_back(RandomTreeGenerator::
		      generateRandomTree(di->getAllSequenceNames()));
	}
    }
	

  // Fill G-S map. If empty (no mapping info was found in the gene tree or a
  // random tree was created), we have to use file passed as parameter with that info.
  if(gsMap.size() == 0)
    {
      ParamGSMapFile = Options.getString("GS Map file")[0];
      if (ParamGSMapFile == "")
	{
	  throw AnError("Missing guest-to-host leaf mapping.");
	}
      else
	{
	  gsMap = TreeIO::readGeneSpeciesInfoVector(ParamGSMapFile); 
	}
    }
  if(gsMap.size() < D.size())
    {
      cerr << "There seem to be fewer gsmaps than datasets\n";
      cerr << gsMap.size() << endl;
      Options.getUsage();
    }
  if(G.size() < D.size())
    {
      cerr << "There seem to be fewer gene trees than datasets\n";
      cerr << G.size() << endl;
      Options.getUsage();
    }

  //?????????????????????
//   G->doDeleteTimes();
//   G->doDeleteRates();

//   lsd = new LSDProbs();
//   string lsdFilename = Options.getString("LSD");
//   if(lsdFilename != "")
//     {
//       ifstream lsdFileStream(lsdFilename.c_str());
//       LSDProbs::readFromFile(lsdFileStream, *DS, *lsd);
//       lsdFileStream.close();
//     }
}


/************************************************************************
 * Helper. Reads program arguments (not options).
 ************************************************************************/
void readParameters(int& argIdx, int argc, char **argv)
{
  if (argc - argIdx < NO_OF_PARAMS)
    {
      throw beep::AnError("Too few input parameters!", 1);
    }
  ParamSTreeFile = argv[argIdx++];
  while(argIdx < argc)
    {
      ParamSeqDataFile.push_back(argv[argIdx++]);// Parameter 1: Seq data.
    }
};


/************************************************************************
 * Helper. Creates a 2-parameter density function from its string ID.
 ************************************************************************/
beep::Density2P* createDensity2P(std::string id, double mean, double variance)
{
  using namespace beep;
  Density2P* df = NULL;
  if      (id == "INVG")    { df = new InvGaussDensity(mean, variance);      }
  else if (id == "LOGN")    { df = new LogNormDensity(mean, variance);       }
  else if (id == "GAMMA")   { df = new GammaDensity(mean, variance);         }
  else if (id == "UNIFORM") { df = new UniformDensity(mean, variance, true); }
  else { throw AnError("Unknown Density2P identifier: " + id +'.'); }
  return df;
};


/************************************************************************
 *  Helper. Creates default values corresponding to options and stores
 *  them in global map.
 ************************************************************************/
void createDefaultOptions(std::string progName)
{
  using namespace std;
  Options.addUsageText(progName, "<hostfile> <datafile>", 
		       "Description: Guest-in-host tree inference enabling "
		       "reconciliation analysis using the underlying GSR "
		       "model. Model properties:\n"
		       "1) the guest tree topology and its divergence times "
		       "are modelled with a duplication-loss process in "
		       "accordance with the Gene Evolution Model (GEM).\n"
		       "2) sequence evolution is modelled with a user-defined "
		       "substitution model.\n"
		       "3) sequence evolution rate variation over guest tree "
		       "edges (relaxed molecular clock) are modelled with iid "
		       "values from a user-selected distribution.\n"
		       "4) sequence evolution rate variation over sites are "
		       "modelled according to a discretized Gamma "
		       "distribution with mean 1.\n"
		       "The implementation uses a discretization of the host "
		       "tree to approximate the probability of all possible "
		       "reconciliation realizations for the current "
		       "parameter");
  Options.addStringOption("Output file", "o", 1, "<empty>", "",
			  "Name of output file.");
  Options.addUnsignedOption("PRNG Seed", "s", 1, "0", "",
			    "Seed for pseudo-random number generator.");
  Options.addUnsignedOption("No. of iterations", "i", 1, "200000", "",
			    "Number of iterations.");
  Options.addUnsignedOption("Thinning", "t", 1, "100", "",
			    "Thinning, i.e. sample every <value>-th "
			    "iteration.");
  Options.addUnsignedOption("Print factor", "w", 1, "1", "",
			    "Output info to stderr every <value>-th sample.");
  Options.addIntOption("Quiet run", "q", 1, "-1", "0 -1",
		       "true = 0, false = -1.");
  Options.addUnsignedOption("No. of root-to-leaf ivs", "v", 1, "50", "",
			    "Number of intervals from root to leaves in "
			    "discretization of host tree (i.e. excluding "
			    "top time edge).");
  Options.addIntOption("Show debug info", "d", 1, "-1", "0 -1",
		       "Show misc. info to stderr before iterating."
		       "true = 0, false = -1.");
  Options.addStringOption("Run type", "m", 1,"MCMC", "MCMC,ML,L",
			  "Execution type (MCMC, maximum likelihood or "
			  "likelihood).");
  Options.addStringOption("Substitution model", "Sm", 1, "JTT", 
			  "UniformAA,JC69,JTT,UniformCodon,ArveCodon",
			  "Substitution model.");
  Options.addUserSubstMatrixOption("User substitution model", "Su", 
				   beep::MAXPARAMS, 
				   "DNA 0.25 0.25 0.25 0.25 "
				   "1 1 1 1 1 1", 
				   "<'DNA'/'AminoAcid'/'Codon'>"
				   " <Pi=float1 float2 ... floatn> "
				   "<R=float1 float2 ...float(n*(n-1)/2)>",
				   "User-defined substitution model. The size "
				   "of Pi and R must fit data type (DNA: n=4, "
				   "AminoAcid: n=20, Codon: n = 62). R is "
				   "given as a flattened upper triangular "
				   "matrix. Don't use both option -Su and "
				   "option -Sm.");
  Options.addUnsignedOption("No. of site rate cats", "Sn", 1, "1", "",
			    "Number of discrete site rate categories.");
  Options.addStringOption("Edge rate DF", "Ed", 1, "Gamma", 
			  "Gamma,InvG,LogN,Uniform",
			  "Underlying density function for edge rate model.");
  Options.addRealOption("Edge rate DF params", "Ep", 2, "1.0 1.0", "",
 			"start mean and variance of edge rate density "
			"function.");
  Options.addRealOption("Fixed edge rate DF params", "Ef", 2, "1.0 1.0", "",
 			"Fix mean and variance of edge rate density "
			"function.");
  Options.addIntOption("Shared root child edges", "Es", 1, "-1", 
		       "o -1",
		       "Treat root's child edges in guest tree as shared "
		       "(as in unrooted tree). May improve accuracy at "
		       "expense of performance. true = 0, false = -1.");
  Options.addStringOption("Guest tree file", "Gi", 1, "<no default>", "",
			  "Name of file containing initial guest treeS.");
  Options.addStringOption("GS Map file", "Gs", 1, "<no default>", "",
			  "Name of file containing guest-host leaf maps.");
  Options.addIntOption("Fixed G", "Gg", 1, "-1", "0 -1",
		       "Fix guest tree, i.e. perform no branch-swapping. �"
		       "true = 0 false = -1");
  Options.addRealOption("BD rates", "Bp", 2, "1.0 1.0", "",
			"Initial birth and death rates for "
			"duplications-loss process.");
  Options.addIntOption("Fixed BD rates", "Bf", 1, "-1", "0 -1",
			"Fix birth and death rates to initial values.");
  Options.addRealOption("TopTime", "Bt", 1, "1.0","",
			"Time length of edge above root in host tree.");
  Options.addRealOption("DiscTimestep", "Dt", 1, "0.05","",
			"Approximate discretization timestep. Set to 0 to "
			"divide every edge in equally many parts (see -Di). "
			"Defaults to 0.05.");
  Options.addUnsignedOption("DiscMinIvs", "Di", 1, "3","",
			    "Minimum number of parts to slice each edge in. "
			    "If -Dt is set to 0, this becomes the exact "
			    "number of parts. Minimum 2. Defaults to 3.");
  Options.addUnsignedOption("TopTimeDiscIvs", "Dtt", 0, "0", "",
			    "Override number of discretization points for "
			    "edge above root in host tree. By default, "
			    "irrespective of time span, this is set to the"
			    "number of points for a (hypothetical) "
			    "root-to-leaf edge.");
Options.addIntOption("Rescale", "r", 1, "-1","0 -1",
		       "Rescale the host tree so that the root-to-leaf time "
		       "equals 1.0. All inferred parameters will refer to "
		       "the new scale. Off by default.");
  Options.addStringOption("LSD", "l", 1, "<empty>", "",
			  "File containing large-scale duplication "
			  "probabilities.");
};
