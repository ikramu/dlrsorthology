#include <algorithm>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <sys/types.h>
#include <time.h>
#include <vector>

#include "AnError.hh"
#include "Beep.hh"
#include "BirthDeathMCMC.hh"
#include "Density2PMCMC.hh"
#include "DiscBirthDeathMCMC.hh"
#include "DiscGEM.hh"
#include "DiscTree.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeWeightMCMC.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "PrimeOptionMap.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "RandomTreeGenerator.hh"
#include "ReconciledTreeTimeMCMC.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SimpleML.hh"
#include "SiteRateHandler.hh"
#include "StrStrMap.hh"
#include "SubstitutionMCMC.hh"
#include "TreeIO.hh"
#include "TreeMCMC.hh"
#include "UniformDensity.hh"
#ifdef DMALLOC
#include </opt/local/include/dmalloc.h>
#endif

// Number of parameters to program.
static const int NO_OF_PARAMS = 2;          // non-optional 	
static const int NO_OF_OPTIONAL_PARAMS = 1; // optional	
		
// Suggestion ratios in MCMC.
static const double SUGG_RATIO_BD_RATES = 1.0;      // birth-death rates
static const double SUGG_RATIO_G_TOPO_START  = 3.0; // start G topology
static const double SUGG_RATIO_G_TOPO_FINAL  = 3.0; // final G topology
static const double SUGG_RATIO_EDGE_WEIGHT = 1.0;   // edge weight

// Pertubation steps between G topology start and final ratios.
static const double SUGG_RATIO_G_TOPO_STEPS  = 3.0; 


// Parameters
std::vector<std::string> ParamSeqDataFile;   // 1. sequence data file name
std::string ParamHostTreeFile = "";  // 2. host tree (S) file name
std::string ParamGSMapFile = "";     // 3. guest-to-host leaf map

// All options to program (and default values) in a map.
beep::PrimeOptionMap Options;	

// Forward declaration of helpers.
void showHelpMsg();
void readParameters(int& argIndex, int argc, char **argv);
void createUnderlyingObjects(std::vector<beep::MatrixTransitionHandler>& Q,
			     std::vector<beep::SequenceData>& D,
			     beep::Tree*& S,// beep::DiscTree*& DS,
			     std::vector<beep::Tree>& G, 
			     std::vector<beep::StrStrMap>& gsMap);
void setStartValues(beep::DiscTree*& DS, beep::Tree*& G, 
		    beep::StrStrMap*& gsMap);
beep::Density2P* createDensity2P(std::string id, double mean, 
				 double variance);;
void createDefaultOptions(std::string progName);


/************************************************************************
 * Main program. See help message method for program description.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  try
    {
      // Create default options values, then read user options and parameters.
      createDefaultOptions(argv[0]);
      int argIndex = 1;
      if (!Options.parseOptions(argIndex, argc, argv))
	{
	  cerr << Options.getUsage();
	  exit(1);		// Positive exit codes for errors /arve
	}
      readParameters(argIndex, argc, argv);
	  
      // Set up random number generator.
      PRNG rand;
      int seed = Options.getUnsigned("PRNG seed")[0]; 
      if (seed != 0)
	{
	  rand.setSeed(seed); 	// Use user-defined random seed.
	}
      else
	{
	  seed = rand.getSeed();
	}
      cerr << "Seed = " << seed << endl;
		
      // Create substitution matrix, sequence data, trees, etc.
      vector<MatrixTransitionHandler> Q;
      vector<SequenceData> D;
      Tree* S;
      vector<Tree> G;
      vector<StrStrMap> gsMap;
      createUnderlyingObjects(Q, D, S, G, gsMap);
		
      // MCMC: End-of-chain dummy.
      DummyMCMC mcmcDummy = DummyMCMC();

      Real rootTime = 1.0;
      // Species tree MCMC
      if(S->hasTimes())
	{
	  rootTime = S->rootToLeafTime();
	}
      Tree dummyTree = Tree::EmptyTree(rootTime);
      dummyTree.setName("Dummy");
      StrStrMapDummy dummyGs(dummyTree.getRootNode()->getName());

      unsigned int noOfIvs = Options.getUnsigned("No. of root-to-leaf ivs")[0];
      cerr << "noOfIvs = " << noOfIvs << endl;
      if (noOfIvs == 0) 
	{
	  noOfIvs = 100; 
	}
      BirthDeathMCMC sbdm(mcmcDummy, dummyTree, 1.0, 1.0);
      ReconciledTreeTimeMCMC srtm(sbdm, *S, dummyGs, sbdm, 
				  dummyTree.getTopTime() / noOfIvs,
				  true, "S_times");
      
      srtm.chooseStartingRates();
      cerr << *S;

      DiscTree DS = DiscTree(*S, noOfIvs);

      StdMCMCModel* prior = & srtm;
      ProbabilityModel* model = & srtm;
      
      vector<Real> bdRates = Options.getReal("BD rates");
      vector<Real> edgeRateDFParams = Options.getReal("Edge rate DF params");

      vector<Real>::iterator bdi = bdRates.begin();
      vector<Tree>::iterator gti = G.begin();
      vector<StrStrMap>::iterator gsi = gsMap.begin();
      vector<Real>::iterator edi = edgeRateDFParams.begin();
      vector<SequenceData>::iterator sdi = D.begin();
      vector<MatrixTransitionHandler>::iterator qi = Q.begin();


      for(; sdi != D.end(); sdi++, gti++, gsi++)
	{
	  // MCMC: Birth-death rate.
	  if(bdi == bdRates.end()) // indicates shared starting values
	    bdi = bdRates.begin(); // for all trees
	  DiscBirthDeathProbs* bdProbs =
	    new DiscBirthDeathProbs(DS, *(bdi++), *(bdi++)); //bdRates[0], bdRates[1]);
	  DiscBirthDeathMCMC* mcmcBDRates = 
	    new DiscBirthDeathMCMC(*prior, *bdProbs, 
				   SUGG_RATIO_BD_RATES);
	  if (Options.getReal("Fixed BD rates")[0]) 
	    {
	      mcmcBDRates->fixRates(); 
	    };
		
	  // MCMC: Guest tree topology.
	  UniformTreeMCMC* mcmcGTopology = 
	    new UniformTreeMCMC(*mcmcBDRates, *gti, //G[0], 
				SUGG_RATIO_G_TOPO_START);
	  mcmcGTopology->setChangingSuggestRatio(SUGG_RATIO_G_TOPO_FINAL, 
						 SUGG_RATIO_G_TOPO_STEPS);
	  if (Options.getInt("Fixed G")[0] == 0)
	    {
	      mcmcGTopology->fixTree();
	      mcmcGTopology->fixRoot();
	    }
	  mcmcGTopology->setDetailedNotification(true);

	  // MCMC: Guest tree edge lengths (and thus rates). 
	  // Acts on a density function.
	  string edgeRateDFName = Options.getString("Edge rate DF")[0];
	  if(edi == edgeRateDFParams.end()) // indicates shared starting values
	      edi = edgeRateDFParams.begin(); // for all trees
	  Density2P* edgeRateDF = createDensity2P(edgeRateDFName, 
						  *(edi++), *(edi++));
	  // 					      edgeRateDFParams[0],
	  // 					      edgeRateDFParams[1]);
	  Density2PMCMC* mcmcEdgeRate = 
	    new Density2PMCMC(*mcmcGTopology, *edgeRateDF);
	  if (Options.hasBeenParsed("Edge rate DF params") || 
	      edgeRateDFName == "UNIFORM")
	    {
	      mcmcEdgeRate->fixMean();
	      mcmcEdgeRate->fixVariance();
	    }		

	  // Set up discretized GEM integrator. Note that rate is 
	  // handled as length div. by time.
	  bool sharedRootChildEdges = 
	    (Options.getInt("Shared root child edges")[0] == 0);
	  DiscGEM* gsrModel = new DiscGEM(*gti, DS, *gsi, *edgeRateDF, 
					  *bdProbs, sharedRootChildEdges);
	  //gsrModel.setSpecErrorVisible(true);
		
	  // MCMC: Edge weight based on rate model.
	  EdgeWeightMCMC* mcmcEdgeWeight = new EdgeWeightMCMC(*mcmcEdgeRate, 
							      *gsrModel, 
							      SUGG_RATIO_EDGE_WEIGHT);
	  mcmcEdgeWeight->generateWeights();
	  mcmcEdgeWeight->setDetailedNotification(true);

	  // Set up edge weights, equating lengths with rates.
	  EdgeWeightHandler* edgeWeightHandler = new EdgeWeightHandler(*gsrModel);
		
	  // MCMC: Site rate. Uniform in interval [0,3].
	  UniformDensity* uniformDF = new UniformDensity(0, 3, true); 
	  ConstRateMCMC* mcmcSiteRate = new ConstRateMCMC(*mcmcEdgeWeight, 
							  *uniformDF, *gti, 
							  "Alpha"); 
		
	  // Set up site rate handler.
	  unsigned noOfSiteRateCats = 
	    Options.getUnsigned("No. of site rate cats")[0];
	  SiteRateHandler* siteRateHandler = new SiteRateHandler(noOfSiteRateCats,
								 *mcmcSiteRate);
	  if (noOfSiteRateCats == 1) 
	    {
	      mcmcSiteRate->fixRates();	// Meaningless to perturb.
	    }

	  // MCMC: Substitution.
	  vector<string> partList;
	  partList.push_back(string("all"));
	  if(qi == Q.end()) // use same model for all data sets
	    qi = Q.begin();
	  SubstitutionMCMC* mcmcSubst = 
	    new SubstitutionMCMC(*mcmcSiteRate, 
				 *sdi, *gti,
				 *siteRateHandler, 
				 *(qi++),
				 *edgeWeightHandler,
				 partList);
		
	  if(Options.getInt("Show debug info")[0] == 0)
	    {
	      DS.debugInfo(true);
	      bdProbs->debugInfo(true);
	      gsrModel->debugInfo(true);
	    }
	  prior = mcmcSubst;
	  model = mcmcSubst;
	}	
      // Iterate according to choice of execution type.
      string runType = Options.getString("Run type")[0];
      if (runType == "L")
	{
	  cout << prior->currentStateProb() << endl;
	}
      else
	{
	  // MCMC: First-in-chain iterator.
	  unsigned thinning = Options.getUnsigned("Thinning")[0];
	  SimpleMCMC* mcmcIterator = (runType == "ML") ?
	    // Maximum likelihood hill-climbing.
	    new SimpleML(*prior, thinning) :	
	    // Actual MCMC.
	    new SimpleMCMC(*prior, thinning);	

	  // Try redirecting output to specified file.
	  string outputFile = Options.getString("Output file")[0];
	  cerr << "outputFile = " << outputFile << endl;
	  if (outputFile != "") 
	    {
	      try
		{
		  mcmcIterator->setOutputFile(outputFile.c_str());
		}
	      catch(AnError& e)
		{
		  e.action();
		}
	      catch (...)
		{
		  cerr << "Problems opening output file '" 
		       << outputFile << "'! "
		       << "Reverting to stdout." << endl;
		}
	    }  

	  // If quiet run, don't show diagnostics.
	  bool quietRun = (Options.getInt("Quiet run")[0] == 0);
	  if (quietRun)
	    {
	      mcmcIterator->setShowDiagnostics(false);
	    }
	  else
	    {
	      cerr << "# Running: ";
	      for (int i = 0; i < argc; i++) { cout << argv[i] << " "; }
	      cerr << " in directory"
		   << getenv("PWD")
		   << endl << "#" << endl;
	      cout << "# Start " << runType
		   << " (Seed = " << rand.getSeed() << ")"
		   << endl;
	    }
			
	  // Print run information.
	  cout << "# Running: ";
	  for (int i = 0; i < argc; ++i) { cout << argv[i] << " "; }
	  cout << " in directory" << getenv("PWD") << endl;
	  cout << "#" << endl;
	  cout << "# Start " << runType 
	       << " (Seed = " 
	       << rand.getSeed() 
	       << ")" 
	       << endl;
	  cout << "# Let's iterate!" << endl;
			
	  // Get time and clock before iterations.
	  time_t t0 = time(0);
	  clock_t ct0 = clock();
			
	  // Iterate!!!!
	  unsigned noOfIters = Options.getUnsigned("No. of iterations")[0];
	  unsigned printFactor = Options.getUnsigned("Print factor")[0];
	  mcmcIterator->iterate(noOfIters, printFactor);
					
	  // Get time and clock after iterations. Print them.
	  time_t t1 = time(0);    
	  clock_t ct1 = clock();
	  cout << "# Wall time: " << difftime(t1, t0) << " s." << endl;
	  cout << "# CPU time: " << (Real(ct1 - ct0)/CLOCKS_PER_SEC) << " s."	<< endl;
			
	  if(runType == "ML" || Options.getInt("Fixed G")[0])
	    {
	      cout << "# " << ((runType == "ML") ? "Last ML" : "Fixed") << " guest tree in PrIME format:" << endl;
	      cout << "# " << TreeIO::writeGuestTree(G[0]) << endl;
	    }
				
	  if (!quietRun)
	    {
	      cerr << prior->getAcceptanceRatio() 
		   << " = acceptance ratio" << endl;
	    }
			
	  if (prior->getAcceptanceRatio() == 0) 
	    {
	      cerr << "Warning! MCMC acceptance ratio was 0." << endl;
	    }
	  delete mcmcIterator;
	}

      // Clean up memory in (somewhat) reverse order of creation.
//       delete edgeRateDF;
      //       delete gsMap;
      //       delete G;
      delete S;
      //       delete D;
      //       delete Q;
    }
  catch (AnError& e)
    {
      e.action();
      cout << "Use '" << argv[0] << " -h' to display help.";
    }
  catch (exception& e)
    {
      cout << e.what() << endl;;
      cout << "Use '" << argv[0] << " -h' to display help.";
    }
  cout << endl;
}


/**
 * Helper. Creates sequence data, trees, etc. Deletion must be taken care of by invoking method.
 */
void createUnderlyingObjects(std::vector<beep::MatrixTransitionHandler>& Q, 
			     std::vector<beep::SequenceData>& D,
			     beep::Tree*& S, //beep::DiscTree* DS,
			     std::vector<beep::Tree>& G,
			     std::vector<beep::StrStrMap>& gsMap)
{
  using namespace std;
  using namespace beep;
	
  // For practical reasons, set up the substitution matrix first.
  // Use user-defined matrix if such has been provided.
  if (Options.hasBeenParsed("User substitution model"))
    {
      if (Options.hasBeenParsed("Substitution model"))
	{
	  throw AnError("Cannot use both predefined and"
			" user-defined subst. model.");
	}
      // Create a class UserSubstitutionMatrix that takes a vector of 
      // Reals as argument and sets the SubstiMatrix accordingly
      vector<UserSubstMatrixParams> usmp = 
	Options.getUserSubstitutionMatrix("User substitution model");
      for(vector<UserSubstMatrixParams>::iterator i = usmp.begin(); 
	  i != usmp.end(); i++)
	{
	  UserSubstMatrixParams& smUsr = *i;
	  
	  // 	  Q = new // Det h�r borde hanteras b�ttre (�ven om det �r b�ttre �n f�rut)
	  Q.push_back(MatrixTransitionHandler(MatrixTransitionHandler::
					      userDefined(smUsr.seqtype, 
							  smUsr.Pi, 
							  smUsr.R)));
	}
    }
  else
    {
      string sm = Options.getString("Substitution model")[0];
      Q.push_back(MatrixTransitionHandler(MatrixTransitionHandler::create(sm)));
    }
	
  // Get sequence data. This is why Q must already exist.
  vector<MatrixTransitionHandler>::iterator qi = Q.begin();
  for(vector<string>::iterator i = ParamSeqDataFile.begin();
      i != ParamSeqDataFile.end(); i++, qi++)
    {
      if(qi == Q.end())
	qi = Q.begin();
      D.push_back(SequenceData(SeqIO::readSequences(*i,qi->getType())));
    }
  // Create S from file.
  Real topTime = Options.getReal("Top time")[0];
  if(topTime <= 0)
    topTime = 1.0;
  S = new Tree(TreeIO::fromFile(ParamHostTreeFile).readBeepTree(0,0));
  S->setTopTime(topTime);

  // Create discretized tree DS based on S.
  //   unsigned int noOfIvs = Options.getUnsigned("No. of root-to-leaf ivs");
  //   if (noOfIvs == 0) 
  //     {
  //       noOfIvs = 1; 
  //     }
  //   DS = new DiscTree(*S, noOfIvs);
	
  // Create random guest tree G unless specified from file.
  TreeIO tio = TreeIO::fromFile(Options.getString("Guest tree file")[0]);
  if(Options.hasBeenParsed("Guest tree file"))
    {
      G = tio.readAllGuestTrees(NULL, &gsMap);
    }
  else
    {
      for(vector<SequenceData>::iterator di = D.begin();
	  di != D.end(); di++)
	{
	  G.push_back(RandomTreeGenerator::
		      generateRandomTree(di->getAllSequenceNames()));
	}
    }
	
  // Fill G-S map. If empty (no mapping info was found in the gene tree or a
  // random tree was created), we have to use file passed as parameter with that info.
  if(gsMap.size() == 0)
    {
      ParamGSMapFile = Options.getString("GS Map file")[0];
      if (ParamGSMapFile == "")
	{
	  throw AnError("Missing guest-to-host leaf mapping.");
	}
      else
	{
	  gsMap = TreeIO::readGeneSpeciesInfoVector(ParamGSMapFile); 

	}
    }
  if(gsMap.size() < D.size())
    {
      cerr << "There seem to be fewer gsmaps than datasets\n";
      cerr << gsMap.size() << endl;
      Options.getUsage();
    }
  if(G.size() < D.size())
    {
      cerr << "There seem to be fewer gene trees than datasets\n";
      cerr << G.size() << endl;
      Options.getUsage();
    }
}



/**
 * Helper. Reads parameters passed as arguments to global variables.
 */
void readParameters(int& argIndex, int argc, char **argv)
{
  if (argc - argIndex < NO_OF_PARAMS)
    {
      throw beep::AnError("Too few input parameters!");
    }
  ParamHostTreeFile = argv[argIndex++];		// Parameter 2: File containing S.
  while(argIndex < argc)
    ParamSeqDataFile.push_back(argv[argIndex++]);     	// Parameter 1: Sequence data.

};


/**
 * Helper. Factory function, creates a 2-parameter density function from its string ID.
 */
beep::Density2P* createDensity2P(std::string id, beep::Real mean, 
				 beep::Real variance)
{
  using namespace beep;
  Density2P* df = NULL;
  if (id == "INVG")         { df = new InvGaussDensity(mean, variance); }
  else if (id == "LOGN")    { df = new LogNormDensity(mean, variance); }
  else if (id == "GAMMA")   { df = new GammaDensity(mean, variance); }
  else if (id == "UNIFORM") { df = new UniformDensity(mean, variance, true); }
  else { throw AnError("Unknown Density2P identifier: " + id +'.'); }
  return df;
};


/**
 *  Helper. Creates default values corresponding to options and stores 
 them in global map.
*/
void createDefaultOptions(std::string progName)
{
  using namespace beep;
  using namespace std;

  Options.addUsageText(progName, "<datafile> <hostfile> [<gs>]", 
		       "Guest tree reconstruction using the GSR model: the "
		       "guest tree and its divergence times are modeled with "
		       "the Gene Evolution Model (GEM), substitution rate "
		       "variation as IID with a user-defined underlying "
		       "probability distribution, and sequence evolution by a "
		       "user-defined substitution model.");
  Options.addStringOption("Output file", "o", 1, "<empty>", "",
			  "Name of output file.");
  Options.addUnsignedOption("PRNG seed", "s", 1, "0", "",
			    "Seed for pseudo-random number generator.");
  Options.addUnsignedOption("No. of iterations", "i", 1, "200000", "",
			    "Number of iterations.");
  Options.addUnsignedOption("Thinning", "t", 1, "100", "",
			    "Thinning, i.e. sample every <value>-th "
			    "iteration.");
  Options.addUnsignedOption("Print factor", "w", 1, "1", "",
			    "Output info to stderr every <value>-th sample.");
  Options.addIntOption("Quiet run", "q", 1, "-1", "0 -1",
			"true = 0, false = -1.");
  Options.addUnsignedOption("No. of root-to-leaf ivs", "v", 1, "50", "",
			    "Number of intervals from root to leaves in "
			    "discretization of host tree (i.e. excluding "
			    "top time edge).");
  Options.addIntOption("Show debug info", "d", 1, "-1", "0 -1",
			"Show misc. info to stderr before iterating."
		       "true = 0, false = -1.");
  Options.addStringOption("Run type", "m", 1,"MCMC", "MCMC,ML,L",
			  "Execution type (MCMC, maximum likelihood or "
			  "likelihood).");
  Options.addStringOption("Substitution model", "Sm", 1, "JTT", 
			  "UniformAA,JC69,JTT,UniformCodon,ArveCodon",
			  "Substitution model.");
  Options.addUserSubstMatrixOption("User substitution model", "Su", 
				   MAXPARAMS, 
				   "DNA 0.25 0.25 0.25 0.25 "
				   "1 1 1 1 1 1", 
				   "<'DNA'/'AminoAcid'/'Codon'>"
				   " <Pi=float1 float2 ... floatn> "
				   "<R=float1 float2 ...float(n*(n-1)/2)>",
				   "User-defined substitution model. The size "
				   "of Pi and R must fit data type (DNA: n=4, "
				   "AminoAcid: n=20, Codon: n = 62). R is "
				   "given as a flattened upper triangular "
				   "matrix. Don't use both option -Su and "
				   "option -Sm.");
  Options.addUnsignedOption("No. of site rate cats", "Sn", 1, "1", "",
			    "Number of discrete site rate categories.");
  Options.addStringOption("Edge rate DF", "Ed", 1, "Gamma", 
			  "Gamma,InvG,LogN,Uniform",
			  "Underlying density function for edge rate model.");
  Options.addRealOption("Edge rate DF params", "Ef", 2, "1.0 1.0", "",
 			"Fix mean and variance of edge rate density "
			"function.");
  Options.addIntOption("Shared root child edges", "Es", 1, "-1", 
			"o -1",
			"Treat root's child edges in guest tree as shared "
			"(as in unrooted tree). May improve accuracy at "
			"expense of performance. true = 0, false = -1.");
  Options.addStringOption("Guest tree file", "Gi", 1, "<no default>", "",
			  "Name of file containing initial guest treeS.");
  Options.addStringOption("GS Map file", "Gs", 1, "<no default>", "",
			  "Name of file containing guest-host leaf maps.");
  Options.addIntOption("Fixed G", "Gg", 1, "-1", "0 -1",
			"Fix guest tree, i.e. perform no branch-swapping. �"
		       "true = 0 false = -1");
  Options.addRealOption("BD rates", "Bp", 2, "1.0 1.0", "",
			"Initial birth and death rates for "
			"duplications-loss process.");
  Options.addRealOption("Fixed BD rates", "Bf", 1, "false", "1.0 1.0",
			"Fix birth and death rates to initial values.");
  Options.addRealOption("Top time", "Bt", 1, "1.0","",
			"Time length of edge above root in host tree.");
};

