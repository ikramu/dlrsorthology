#include "AnError.hh"
#include "Density2P.hh"
#include "DiscBirthDeathProbs.hh"
#include "DiscGEM.hh"
#include "Node.hh"
#include "Probability.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"
#include "TreeDiscretizer.hh"
#include "TreePerturbationEvent.hh"

#include "fastGEM.hh"

#include <cassert>
#include <math.h>

namespace beep
{

  using namespace std;


  DiscGEM::DiscGEM(Tree& G, DiscTree& DS, StrStrMap& GSMap,
		   Density2P& rateDensFunc, DiscBirthDeathProbs& BDProbs,
		   bool sharedRootChildEdges, BeepVector<unsigned>* fixedGNodes) :
    EdgeWeightModel(),
    PerturbationObserver(),
    m_G(G),
    m_DS(DS),
    m_GSMap(GSMap),
    m_rateDensFunc(rateDensFunc),
    m_BDProbs(BDProbs),
    m_sigma(G, DS.getOrigTree(), GSMap),
    // 	m_lengths(G.getNumberOfNodes(), rateDensFunc.getMean()),
    m_lengths(NULL),
    m_sharedRootChildEdges(sharedRootChildEdges),
    m_fixedGNodes(fixedGNodes),
    m_showSpecError(false),
    m_loGridIndices(G),
    m_upGridIndices(G),
    m_probs(G),
    m_probs_old(G)
  {
    if(G.hasLengths())
      {
	m_lengths = &G.getLengths();
      }
    else
      {
	m_lengths = new RealVector(G.getNumberOfNodes(), 
				   rateDensFunc.getMean());
	G.setLengths(*m_lengths, true);
      }

    if (m_DS.getNoOfTopTimeIvs() <= 0)
      {
	throw AnError("Top time of discretized host tree is "
		      "required to be greater than zero in DiscGEM.");
      }
	
    // Create member vectors.
    unsigned int initialCapacity = m_DS.getNoOfIvs() + 1;
    for (unsigned int i=0; i<m_probs.size(); ++i)
      {
	m_probs[i] = new ProbVector();
	m_probs[i]->reserve(initialCapacity);
      }
	
    // Compute values to have something to start with.
    updateHelpStructures();
    updateProbsFull();
	
    // Register as listener on parameter holders we depend on.
    m_G.addPertObserver(this);  
    m_BDProbs.addPertObserver(this);
    m_rateDensFunc.addPertObserver(this);
    m_DS.getOrigTree().addPertObserver(this); // Host tree
  }


  DiscGEM::~DiscGEM()
  {
    for (unsigned int i=m_probs.size(); i>0; --i)
      {
	delete (m_probs[i-1]);
	m_probs[i-1] = NULL;
	if (m_probs_old[i-1] != NULL)
	  {
	    delete (m_probs_old[i-1]);
	    m_probs_old[i-1] = NULL;
	  }
      }
  }


  const Tree& DiscGEM::getTree() const
  {
    return m_G;
  }


  unsigned DiscGEM::nWeights() const
  {
    // Top time edge is never perturbed.
    // When shared root edge(s), one parameter less.
    return (m_sharedRootChildEdges ?
	    m_G.getNumberOfNodes()-2 : m_G.getNumberOfNodes()-1);
  }


  RealVector& DiscGEM::getWeightVector() const
  {
    return *m_lengths;
  }


  Real DiscGEM::getWeight(const Node& node) const
  {
    return (*m_lengths)[node];
  }


  void DiscGEM::setWeight(const Real& weight, const Node& u)
  {
    (*m_lengths)[u] = weight;
	
    // When setting one of shared edges, change the other as well.
    // This affects variance, but never mind.
    // NOTE: This may not be the case; see nWeights().
    if (m_sharedRootChildEdges && !u.isRoot() && u.getParent()->isRoot())
      {
	(*m_lengths)[u.getSibling()] = weight;
      }
  }


  void DiscGEM::getRange(Real& low, Real& high)
  {
    m_rateDensFunc.getRange(low, high);
  }


  string DiscGEM::print() const
  {
    ostringstream oss;
    oss << "The rate probabilities are modeled using a \n"
	<< m_rateDensFunc.print();
    return oss.str();
  }


  void DiscGEM::perturbationUpdate(const PerturbationObservable* sender, 
				   const PerturbationEvent* event)
  {
    //!\todo{if host tree is changed this might need to be 
    //! implemented differently}
    if(sender == &m_DS.getOrigTree())
      {
	m_DS.update();
	m_BDProbs.update();
	updateHelpStructures();
// 	updateProbsFull();
      }
    else
      {
	// We always update help structures to be on the safe side.
	updateHelpStructures();
      }
	
    // We may have access to details if sender is guest tree.
    const TreePerturbationEvent* details = dynamic_cast<const TreePerturbationEvent*>(event);
	
    // Restore or recompute probabilities depending on event.
    // If G is sender, look for detailed info to make optimized update.
    if (event != NULL && event->getType() == PerturbationEvent::RESTORATION)
      {
	restoreCachedProbs();
      }
    else if (sender == &m_G && details != NULL)
      {
	cacheProbs(details);
	// 	updateProbsPartial(details);
	updateProbsFull();
      }
    else
      {
	cacheProbs(NULL);
	updateProbsFull();
      }
  }


  void DiscGEM::update()
  {
  }


  void DiscGEM::setSpecErrorVisible(bool showSpecError)
  {
    m_showSpecError = showSpecError;
  }


  void DiscGEM::debugInfo(bool printNodeInfo) const
  {
    cerr << "# ===================================== DiscGEM =======================================" << endl;
    if (printNodeInfo)
      {
	cerr << "# Node:\tName:\tP:\tLC:\tRC:\tSigma\tSpan:\tProbs:" << endl;
	for (unsigned int i=0; i<m_G.getNumberOfNodes(); ++i)
	  {
	    Node* u = m_G.getNode(i);
	    cerr << "# "
		 << i << '\t'
		 << (u->isLeaf() ? u->getName() : (u->isRoot() ? "Root" : "    ")) << '\t'
		 << (u->isRoot() ? -1 : static_cast<int>(u->getParent()->getNumber())) << '\t'
		 << (u->isLeaf() ? -1 : static_cast<int>(u->getLeftChild()->getNumber())) << '\t'
		 << (u->isLeaf() ? -1 : static_cast<int>(u->getRightChild()->getNumber())) << '\t'
		 << m_sigma[u]->getNumber() << '\t'
		 << m_loGridIndices[u].first << "..." << m_upGridIndices[u] << '\t'
		 << m_probs[u]->front().val() << "..." << m_probs[u]->back().val() << '\t'
		 << endl;
	  }
      }
    cerr << "# =====================================================================================" << endl;
  }


  void DiscGEM::updateHelpStructures()
  {
    // Note: Order of invokation matters.
    m_sigma.update(m_G, m_DS.getOrigTree(), &m_GSMap);
    const Node* uRoot = m_G.getRootNode();
    updateLoGridIndices(uRoot);
    updateUpGridIndices(uRoot);
  }


  void DiscGEM::updateLoGridIndices(const Node* u)
  {
    if (u->isLeaf())
      {
	m_loGridIndices[u] = DiscTree::Point(0, m_sigma[u]);
      }
    else
      {
	// Update children first.
	updateLoGridIndices(u->getLeftChild());
	updateLoGridIndices(u->getRightChild());
		
	const Node* sigma = m_sigma[u];
	DiscTree::Point loIndex = DiscTree::Point(m_DS.getGridIndex(sigma), sigma);
		
	// Set limit of u to highest of sigma and points above childrens' limits.
	DiscTree::Point vLoParIndex = m_loGridIndices[u->getLeftChild()];
	vLoParIndex.first++;
	DiscTree::Point wLoParIndex = m_loGridIndices[u->getRightChild()];
	wLoParIndex.first++;
	if (loIndex.first < vLoParIndex.first) { loIndex = vLoParIndex; }
	if (loIndex.first < wLoParIndex.first) { loIndex = wLoParIndex; }
	if (m_DS.isAboveEdge(loIndex.first, loIndex.second))
	  {
	    loIndex.second = loIndex.second->getParent(); // We may have moved to edge above.
	  }
	m_loGridIndices[u] = loIndex;
		
	// If flag set, verify that if u has other sigma than children, the spec. can really occur.
	if (m_showSpecError &&
	    sigma != m_sigma[u->getLeftChild()] &&
	    sigma != m_sigma[u->getRightChild()] &&
	    loIndex.first != m_DS.getGridIndex(sigma))
	  {
	    // Just show error message. May happen even at reasonably high resolution
	    // (but hopefully mostly poor trees in that case).
	    const Node* lc = sigma->getLeftChild();
	    const Node* rc = sigma->getRightChild();
	    cerr << "# Speciation error: Can't place guest node no. " << u->getNumber()
		 << " on host node no. " << sigma->getNumber()
		 << " due to too few disc. steps beneath!" << endl
		 << "# (Left edge has " << m_DS.getNoOfPtsOnEdge(lc)
		 << " point(s) and length " << m_DS.getEdgeTime(lc)
		 << ", right node " << m_DS.getNoOfPtsOnEdge(rc)
		 << " point(s) and length " << m_DS.getEdgeTime(rc) << ")." << endl;
	  }
      }
  }


  void DiscGEM::updateUpGridIndices(const Node* u)
  {
    if (u->isLeaf() || (m_fixedGNodes != NULL && (*m_fixedGNodes)[u]))
      {
	// Leaf and fixed node case: set to sigma.
	m_upGridIndices[u] = m_DS.getGridIndex(m_sigma[u]);
      }
    else if (u->isRoot())
      {
	// Root case: set to point just below top point.
	m_upGridIndices[u] = m_DS.getNoOfIvs() - 1;
      }
    else
      {
	// Normal case: set u's limit just beneth parent's limit.
	m_upGridIndices[u] = m_upGridIndices[u->getParent()] - 1;
      }
	
    // Verify limits.
    if (m_loGridIndices[u].first > m_upGridIndices[u])
      {
	throw AnError("Too few host tree discretization steps: guest tree won't fit!");
      }
	
    // Update children afterwards.
    if (!u->isLeaf())
      {
	updateUpGridIndices(u->getLeftChild());
	updateUpGridIndices(u->getRightChild());
      }
  }


  Probability DiscGEM::calculateDataProbability()
  {
    const Node* root = m_G.getRootNode();
	
    // Retrieve all lineages (for root) starting at very top of host tree.
    ProbVector topLins = ProbVector();
    bool isFixed = (m_loGridIndices[root].first == m_upGridIndices[root]);
    calcLinProbs(topLins, root, m_DS.getTopPt(), isFixed);
	
    // Return the sum of each lineage. Don't include rate for this edge.
    Probability sumProb(0.0);
    for (ProbVector::iterator it=topLins.begin(); it!=topLins.end(); ++it)
      {
	sumProb += (*it);
      }
    return sumProb;
  }


  void DiscGEM::updateProbsFull()
  {
    // Recursive clean update.
    updateNodeProbs(m_G.getRootNode(), true, true);
  }


  void DiscGEM::updateProbsPartial(const TreePerturbationEvent* details)
  {	
    // Do a recursive non-clean update on all changed subtrees.
    const set<const Node*>& subtrees = details->getSubtrees();
    set<const Node*>::iterator it = subtrees.begin();
    for (; it != subtrees.end(); ++it)
      {
	updateNodeProbs(*it, true, false);
      }
	
    // Do a non-recursive clean update along the changed root paths.
    // If we have two paths, follow the second one up to intersection
    // with the first, then do entire first.
    const Node* p1;
    const Node* p2;
    details->getRootPaths(p1, p2);
    if (p2 != NULL)
      {
	const Node* lca = m_G.mostRecentCommonAncestor(p1, p2);
	while (p2 != lca)
	  {
	    updateNodeProbs(p2, false, true);
	    p2 = p2->getParent();
	  }
      }
    while (p1 != NULL)
      {
	updateNodeProbs(p1, false, true);
	p1 = p1->getParent();
      }
  }


  void DiscGEM::updateNodeProbs(const Node* u, bool doRecurse, bool cleanUpdate)
  {
    if (cleanUpdate)
      {
	// Forces all values to be recomputed.
	m_probs[u]->clear();
      }
	
    if (u->isLeaf())
      {
	// Just overwrite whatever was there.
	m_probs[u]->assign(1, Probability(1.0));
      }
    else
      {
	// Must do children first, if specified.
	if (doRecurse)
	  {
	    updateNodeProbs(u->getLeftChild(), true, cleanUpdate);
	    updateNodeProbs(u->getRightChild(), true, cleanUpdate);
	  }
		
	// Retrieve placement bounds.
	unsigned upIndex = m_upGridIndices[u];
	DiscTree::Point loIndex = m_loGridIndices[u];
	int noOfPts = (upIndex - loIndex.first + 1);
	int noOfPtsOld = m_probs[u]->size();
		
	if (noOfPts < noOfPtsOld)
	  {
	    // Less points required than before. Just shrink the array.
	    m_probs[u]->resize(noOfPts);
	  }
	else if (noOfPts > noOfPtsOld)
	  {
	    // More points required. Append the needed ones.
	    loIndex = m_DS.getPt(loIndex.first+noOfPtsOld, loIndex.second);
	    appendNodeProbs(u, loIndex, upIndex);
	  }
      }
    assert((m_upGridIndices[u]-m_loGridIndices[u].first+1) == m_probs[u]->size());
  }


  void DiscGEM::appendNodeProbs(const Node* u, DiscTree::Point loIndex, unsigned upIndex)
  {	
    // Retrieve children etc.
    bool sharedEdge = (u->isRoot() && m_sharedRootChildEdges);
    const Node* v = u->getLeftChild();
    const Node* w = u->getRightChild();
    DiscTree::Point vLoIndex = m_loGridIndices[v];
    DiscTree::Point wLoIndex = m_loGridIndices[w];
    bool vIsFixed = (vLoIndex.first == m_upGridIndices[v]);
    bool wIsFixed = (wLoIndex.first == m_upGridIndices[w]);
	
    // Calculate lineage-event probs. Y and Z are the highest nodes
    // *strictly* beneath loIndex stemming from v and w respectively.
    ProbVector vLins = ProbVector();
    ProbVector wLins = ProbVector();
    const Node* Y = calcLinProbs(vLins, v, loIndex, vIsFixed);
    const Node* Z = calcLinProbs(wLins, w, loIndex, wIsFixed);
	
    // Current grid index.
    unsigned i = loIndex.first; 
	
    Probability segVal;     // Prob. of const lineage for segment above current index.
    Probability lossVal;    // Loss value at current index (i.e. akin to "survival chance").
    Probability dupFactor;  // Duplication factor, e.g. 2*birthRate*timestep.
    Probability sumProb;    // Summed prob. from children at current index, excl. dup. factor.
	
    // If we have a speciation, we handle the first iteration separately.
    if (Y != Z)
      {
	// SPECIATION CASE: Add the prob. for the speciation directly.
	sumProb = calcRateSumProb(vLins, wLins, (*m_lengths)[v], 
				  (*m_lengths)[w], (i - vLoIndex.first), 
				  (i - wLoIndex.first), sharedEdge);
	m_probs[u]->push_back(sumProb);
	if (i == upIndex) { return; }
		
	// Since v and w come from different branches of DS, we extend all lineages with
	// one step before continuing. Also add new lineage where applicable.
	segVal = m_BDProbs.getConstLinValForSeg(Y->getParent());
	extendLins(vLins, segVal * m_BDProbs.getLossVal(Z));
	extendLins(wLins, segVal * m_BDProbs.getLossVal(Y));
	if (!vIsFixed) { vLins.push_back(segVal * getProb(v, i)); }
	if (!wIsFixed) { wLins.push_back(segVal * getProb(w, i)); }
	Y = Y->getParent();
	i++;
      }
	
    // For each placement of u, calculate duplication probs. In each iteration,
    // let Y be the highest node *strictly* below current grid index.
    while (true)
      {	
	// Check whether we have reached a new edge of DS.
	if (m_DS.isAboveEdge(i, Y))
	  {
	    // DUPLICATION ON SPECIATION NODE: Use approximation.
	    lossVal = m_BDProbs.getLossVal(Y->getSibling());
	    dupFactor = (m_BDProbs.getBirthRate() * m_DS.getTimestep()) *
	      lossVal * (lossVal + 1.0);
	    Y = Y->getParent();
	    segVal = m_BDProbs.getConstLinValForSeg(Y);
	  }
	else
	  {
	    // DUPLICATION ON PURE DISCRETIZATION NODE: Normal duplication.
	    lossVal = Probability(1.0);
	    dupFactor = Probability(2 * m_BDProbs.getBirthRate() * m_DS.getTimestep());
	    segVal = m_BDProbs.getConstLinValForSeg(DiscTree::Point(i, Y));
	  }
		
	// Store the probability.
	sumProb = calcRateSumProb(vLins, wLins, (*m_lengths)[v], (*m_lengths)[w],
				  (i - vLoIndex.first), (i - wLoIndex.first), sharedEdge);
	m_probs[u]->push_back(dupFactor * sumProb);
		
	if (i == upIndex) { break; }
		
	// Extend lineages by one step and add a new shortest lineage where applicable.
	extendLins(vLins, segVal * lossVal);
	extendLins(wLins, segVal * lossVal);
	if (!vIsFixed) { vLins.push_back(segVal * getProb(v, i)); }
	if (!wIsFixed) { wLins.push_back(segVal * getProb(w, i)); }
	i++;
      }
  }

  const Node* DiscGEM::calcLinProbs(ProbVector& lins, const Node* v,
				    DiscTree::Point linStart, bool singleLin) const
  {
    // Get lineage BD probabilities.
    const Node* Y =
      m_BDProbs.getConstLinValsForPath(lins, linStart, m_loGridIndices[v], singleLin);
	
    // Include the event probabilities at lineage ends.
    assert(lins.size() <= m_probs[v]->size());
    assert(lins.front() <= lins.back());
    ProbVector::iterator it = lins.begin();
    ProbVector::iterator jt = m_probs[v]->begin();
    for (; it != lins.end(); ++it, ++jt)
      { 
	(*it) *= (*jt); 
      }
    return Y;
  }


  Probability DiscGEM::calcRateSumProb(ProbVector& vLins, ProbVector& wLins,
				       Real vLength, Real wLength, unsigned vTimesteps, unsigned wTimesteps, bool sharedEdge) const
  {
    Real timestep = m_DS.getTimestep();
    Real vTime = timestep * vTimesteps;
    Real wTime = timestep * wTimesteps;
    if (sharedEdge)
      {
	// Include rate and sum all placements for v against all placements of w.
	// We cannot do separate summing since the edge length (and thus rate) is shared.
	Real length = vLength + wLength;
	Real wMaxTime = wTime;
	Probability sum(0.0);
	for (ProbVector::iterator it=vLins.begin(); it!=vLins.end(); ++it, vTime-=timestep)
	  {
	    wTime = wMaxTime;
	    for (ProbVector::iterator jt=wLins.begin(); jt!=wLins.end(); ++jt, wTime-=timestep)
	      {
		assert(vTime > 0);
		assert(wTime > 0);
#ifndef DISCGEM_CDF
		Probability rp = m_rateDensFunc(length / vTime + wTime);
#else
		// NOTE: The following uses rate cdf instead of pdf. 
		Probability rp =m_rateDensFunc.cdf(length/(vTime+wTime-timestep/2))
		  - m_rateDensFunc.cdf(length / (vTime + wTime + timestep / 2));
#endif
		assert(rp >= 0);
		sum += (*it) * (*jt) * rp;
	      }
	  }
	return sum;
      }
    else
      {
	// Include rate for each of the lineage vectors. Then multiply together the sums.
	Probability vSum = Probability(0.0);
	Probability wSum = Probability(0.0);
	for (ProbVector::iterator it=vLins.begin(); it!=vLins.end(); ++it, vTime-=timestep)
	  {
	    assert(vTime > 0);
#ifndef DISCGEM_CDF
	    Probability rp = m_rateDensFunc(vLength / vTime);
#else
	    // NOTE: The following uses rate cdf instead of pdf.
	    Probability rp = m_rateDensFunc.cdf(vLength / (vTime - timestep / 2))
	      - m_rateDensFunc.cdf(vLength / (vTime + timestep / 2));
#endif
	    assert(rp >= 0);
	    vSum += (*it) * rp;
	  }
	for (ProbVector::iterator it=wLins.begin(); it!=wLins.end(); ++it, wTime-=timestep)
	  {
	    assert(wTime > 0);
#ifndef DISCGEM_CDF
	    Probability rp = m_rateDensFunc(wLength / wTime);
#else	
	    // NOTE: The following uses rate cdf instead of pdf.
	    Probability rp = m_rateDensFunc.cdf(wLength / (wTime - timestep / 2))
	      - m_rateDensFunc.cdf(wLength / (wTime + timestep / 2));
#endif
	    assert(rp >= 0);
	    wSum += (*it) * rp;
	  }
	return (vSum * wSum);
      }
  }


  void DiscGEM::extendLins(ProbVector& lins, Probability linSeg)
  {
    for (ProbVector::iterator it = lins.begin(); it != lins.end(); ++it)
      {
	(*it) *= linSeg;
      }
  }


  void DiscGEM::cacheProbs(const TreePerturbationEvent* details)
  {
    // Clear cached values.
    for (unsigned int i=0; i<m_G.getNumberOfNodes(); ++i)
      {
	if (m_probs_old[i] != NULL)
	  {
	    delete (m_probs_old[i]);
	    m_probs_old[i] = NULL;
	  }
      }
	
    if (details == NULL)
      {
	// Store all values.
	cacheNodeProbs(m_G.getRootNode(), true);
      }
    else
      {
	// Store only relevant values.
	const set<const Node*>& subtrees = details->getSubtrees();
	set<const Node*>::iterator it = subtrees.begin();
	for (; it != subtrees.end(); ++it)
	  {
	    cacheNodeProbs(*it, true);
	  }
	const Node* p1;
	const Node* p2;
	details->getRootPaths(p1, p2);
	for (; p1 != NULL; p1 = p1->getParent())
	  {
	    cacheNodeProbs(p1, false);
	  }
	for (; p2 != NULL; p2 = p2->getParent())
	  {
	    if (m_probs_old[p2] == NULL) { cacheNodeProbs(p2, false); }
	  }
      }
  }


  void DiscGEM::cacheNodeProbs(const Node* u, bool doRecurse)
  {
    m_probs_old[u] = new ProbVector();
    m_probs_old[u]->assign(m_probs[u]->begin(), m_probs[u]->end());
    if (!u->isLeaf() && doRecurse)
      {
	cacheNodeProbs(u->getLeftChild(), true);
	cacheNodeProbs(u->getRightChild(), true);
      }
  }


  void DiscGEM::restoreCachedProbs()
  { 
    // Point to cached values.
    for (unsigned int i=0; i<m_G.getNumberOfNodes(); ++i)
      {
	if (m_probs_old[i] != NULL)
	  {
	    delete (m_probs[i]);
	    m_probs[i] = m_probs_old[i];
	    m_probs_old[i] = NULL;
	  }
      }
  }


  Probability& DiscGEM::getProb(const Node* u, unsigned gridIndex) const
  {
    assert(m_probs[u] != NULL);
    assert(gridIndex >= m_loGridIndices[u].first);
    assert(gridIndex - m_loGridIndices[u].first < m_probs[u]->size());
    return (*m_probs[u])[gridIndex - m_loGridIndices[u].first];
  };

} // end namespace beep
