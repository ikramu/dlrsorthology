#include "ReconciliationTimeMCMC.hh"

#include "AnError.hh"
#include "BirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "ReconciledTreeMCMC.hh"
#include "Hacks.hh"
#include "PRNG.hh"
#include "SimpleML.hh"
#include "TopTimeMCMC.hh"
#include "Tree.hh"
#include "TreeIO.hh"

// Global options
//-------------------------------------------------------------
int nParams = 6;

char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 10;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;
bool do_ML = false;

// Birth-death process related
bool fixed_bdrates = false;
double birthRate = 1.0;
double deathRate = 1.0;
double topTime = -1.0;
bool estimateTopTime = true;
double Beta = -1.0;
bool mustChooseRates = true;

// reconciliation related
bool estimate_times = true;
bool specprob = false;

bool fixTree = true;
bool fixRoot = true;


using namespace beep;
using namespace std;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);

int
main(int argc, char **argv) 
{  
  using namespace beep;
  using namespace std;

  if (argc < nParams) 
    {
      usage(argv[0]);
      exit(1);
    }
  try
    {
      // tell the user we've started
      //---------------------------------------------------------
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      //---------------------------------------------------------
      // Read input and options
      //---------------------------------------------------------

      // Check for options
      //-----------------------------------
      int opt = readOptions(argc, argv);

      if(opt + 2 > argc)
	{
	  cerr << "Arguments missing\n";
	  exit(2);
	}

      //Get the trees
      //---------------------------------------------
      string guest(argv[opt++]);
      TreeIO io = TreeIO::fromFile(guest);
      StrStrMap gs;
//       Tree G = io.readGuestTree(0, &gs); 
      Tree G = io.readBeepTree(true, false,false, false,0, 0); 
      if(G.getName() == "Tree")
	{ 
	  G.setName("G");
	}


      string host(argv[opt++]);
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  
      if(S.getName() == "Tree")
	{ 
	  S.setName("S");
	}

      if(gs.size() == 0)
	{
	  if(opt + 1 > argc)
	    {
	      cerr << "gs was not present in guest tree, "
		   << "therefore I expected a <gs> argument\n";
	      usage(argv[0]);
	    }
	  gs = TreeIO::readGeneSpeciesInfo(argv[opt]);
	}

      //--------------------------------------------------------
      // Set up all classes
      //--------------------------------------------------------
       //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}
      DummyMCMC dm;

      // Set up priors
      //-------------------------------------------------------
       if(Beta < 0)
 	{
 	  Beta = S.getRootNode()->getNodeTime();
 	}

       TopTimeMCMC ttm(dm, S, Beta);
       if(estimateTopTime == false)
       {
	 if(topTime >= 0)
	   {
	     ttm.setTopTime(topTime);
	   }
	 ttm.fixTopTime();
       }

      BirthDeathMCMC bdm(ttm, S, birthRate, deathRate, false);
      if(fixed_bdrates)
	{
	  bdm.fixRates();
	}

      ReconciledTreeMCMC rtm(bdm, G, gs, bdm);

      cout << "Constructing a ReconciliationTimeMCMC object\n";
      ReconciliationTimeMCMC rttm(rtm,rtm);
      if(estimate_times == false)
	{
	  rtm.fixGamma();
	  rttm.fixTimes();
	}
      cout << rttm << endl;
      cout << G.print(false,true,false,false); 

//       cout << "Testing initStateProb()\n";
//       Probability p = rttm.initStateProb();     
//       cout << "InitStateProb = " << p.val() << endl;
//       rttm.commitNewState(); 

      // Set up MCMC handler
      //-------------------------------------------------------
      SimpleMCMC* iterator;
      if(do_ML)
	{
 	  iterator = new SimpleML(rttm, Thinning);
	}
      else
	{
	  iterator = new SimpleMCMC(rttm, Thinning);
	}

      if (outfile != NULL)
	{
	  try 
	    {
	      iterator->setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  

      if (quiet)
	{
	  iterator->setShowDiagnostics(false);
	}

      if (do_likelihood)
	{
	  cout << "# L\tN\t" 
	       << rttm.strHeader()
	       << endl
	       << rttm.currentStateProb() 
	       << "\t0\t" 
	       << rttm.strRepresentation() 
	       << endl;
	  exit(0);
	}      
      
      if (!quiet) 
	{
	  cerr << "Start MCMC (Seed = " << rand.getSeed() << ")\n";
	}


      // Copy startup info to outfile (can be used to restart analysis)
      cout << "# Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cout << argv[i] << " ";
	}
      cout << " in directory"
	   << getenv("PWD")
	   << "\n";
      
      //--------------------------------------------------------
      // Now we're set to do stuff
      //---------------------------------------------------------

      time_t t0 = time(0);
      clock_t ct0 = clock();


      iterator->iterate(MaxIter, printFactor);

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << readableTime(t1 - t0) 
	   << endl
	   << "CPU time: " << readableTime((ct1 - ct0)/CLOCKS_PER_SEC)
	   << endl;
      
      if (!quiet)
	{
	  cerr << rttm.getAcceptanceRatio()
	       << " = acceptance ratio   Wall time = "
	       << readableTime(t1-t0)
	       << "\n";
	}
      
      if (rttm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }






//       cout << "Simulate a short MCMC run\n";
//       Probability oldP = p;
//       PRNG R;
//       Probability gamma = 0;
//       Probability gammap = 0;
//       Probability sum = 0;
//       cout << "L\tN\t" << rttm.strHeader() << endl;
//       for(unsigned i = 0; i < nSamples; i++)
// 	{
// 	  MCMCObject MOb = rttm.suggestNewState();
// 	  p = MOb.stateProb;
// 	  Probability alpha = p * MOb.propRatio / oldP;
// 	  if(alpha > 1.0 || Probability(R.genrand_real1()) <= alpha)
// 	    {
// 	      rttm.commitNewState();
// 	      oldP = p;
// 	    }
// 	  else
// 	    {
// 	      rttm.discardNewState();
// 	    }
// // 	  cout << oldP << "\t" << i << "\t" << rttm.strRepresentation() << endl;
// 	  sum += oldP;
// 	  // The folowing outcommented stuff is a test that applies to 
// 	  // the special case where S has two leaves and G has to leaves
// 	  // gs maps one G leaf to each S leaf
// 	  if(G.getRootNode()->getNodeTime() == 1.0)
// 	    {
// 	      gamma += 1.0;
// 	      gammap = oldP;
// 	    }
// 	}
//       EnumerateLabeledReconciliationModel N(G, gs, bdm);
//       LabeledGuestTreeModel M(G, gs, bdm);
//       N.setGamma(0);
//       cout << "The following equality should hold:\n"
//  	   << (gamma/nSamples).val()
// 	   << " = " 
// 	   << (N.calculateDataProbability()/ M.calculateDataProbability()).val()
// 	   << "( ~= " 
// 	   << (gammap*nSamples/sum).val()
// 	   << ")\n"
// 	   << (sum/nSamples).val()
// 	   << " ~= " 
// 	   << M.calculateDataProbability().val()
// 	   << endl;
    }
  catch(AnError e)
    {
      cerr <<" error\n";
      e.action(); 
    }
}

void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <guest tree> <host tree> [<gene-species map<]\n"
    << "\n"
    << "Parameters:\n"
    << "   <guest tree>       Name of file containing guest tree\n"
    << "   <species tree>     Species tree in Newick format. with divergence\n"
    << "                      times\n"
    << "   <gene-species map> Optional. This file contains lines with a\n"
    << "                      gene name in the first column and species\n"
    << "                      name as found in the species tree in the\n"
    << "                      second. You can also choose to associate the \n"
    << "                      genes with species in the gene tree. Please\n"
    << "                      see documentation.\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         Output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -m                    Do maximum likelihood. No MCMC.\n"
    << "   -w <float>            Write to cerr <float> times less often than\n"
    << "                         to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator.\n"
    << "                         If set to 0 (default), the process id is\n"
    << "                         used as seed.\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -g                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -G<option>            Options related to the gene tree\n"
    << "     -Gr                 reroot gene tree. Default is a fixed root\n"
    << "     -Gt                 Fix times in gene tree\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth/death rates to these values \n"
    << "     -Bp <float> <float> start values of birth/death rate parameters\n"
    << "     -Bt <float>         Start value for 'top time', the time\n"
    << "                         between first duplication and root of S.\n"
    << "     -Bb <float>         The beta parameter for a prior distribution\n"
    << "                         on species root distance\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'w':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-w'\n";
		usage(argv[0]);
	      }
	    break;
	  }

	case 's':
	  {
	    if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	      {
		cerr << "Expected integer after option '-s'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'q':
	  {
	    quiet = true;
	    break;
	  }	
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'm':
	  {
	    do_ML = true;
	    break;
	  }	   
	case 'G':
	  {
	    switch(argv[opt][2])
	      {
	      case 'r':
		{
		  fixRoot = false;
		  break;
		}	      
	      case 't':
		{
		  estimate_times = false;
		  estimateTopTime = false;
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		  {
		    fixed_bdrates = true;
		    // Don't break here, because we want to fall through to 'r'
		    // set the rates that are arguments both to '-r' and '-f'!
		  }
	      case 'p':
		{
		  mustChooseRates = false;
		  if (++opt < argc) 
		    {
		      birthRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  deathRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 't':
		{
		  if (++opt < argc)
		    {
		      topTime = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected a 'top time'!\n";
		      usage(argv[0]);
		    }
		  break;
		  }
	      case 'b':
		{
		  if (++opt < argc) 
		    {
		      if (sscanf(argv[opt], "%lf", &Beta) == 0)
			{
			  cerr << "Expected number after option '-b'\n";
			  usage(argv[0]);
			}
		    }
		  break;
		}
	      default:
		{
		    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		}
	      }
	      break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  

	

