#include "DummyMCMC.hh"
#include "StdMCMCModel.hh"
#include "MCMCObject.hh"

namespace beep
{
  class test : public StdMCMCModel
  {
  public:
    // constructor just passes to StdMCMCModelConstructors
    test(MCMCModel& p, const Real& s = 1.0)
      :StdMCMCModel(p, 1, s),
      a(true)
    {};
    
    test(const test& s)
      :StdMCMCModel(s),
       a(s.a)
    {};
    
    ~test(){};

    test&
    operator=(const test& s)
    { 
      if(&s != this)
	StdMCMCModel::operator=(s);
      a = s.a;
      return *this;
    };
    
    
    // define pure virtual functions
    MCMCObject suggestOwnState()
    {
      a = !a;
       return MCMCObject(updateDataProbability(), 1.0);
    };
    
    void commitOwnState()
    {};
    
    void discardOwnState()
    {};
    
    std::string ownStrRep() const
    { return (a?"true\t":"false\t");};
    
    std::string ownHeader() const
    { return "a\t";};
    
    Probability updateDataProbability()
    { return a ? 1 : 0;};

    //Extra
    // Retrieve n_params
    Real get_n_params()
    { return n_params;} 

    // Attribute
    bool a;
  };
}

int main (int argc, char **argv) 
  {
    using namespace beep;
    using namespace std;
    if (argc != 2) 
      {
	cerr << "usage: test_StdMCMCModel <suggestRatio>\n";
	exit(1);
      }

    Real suggestRatio = atof(argv[1]);
    cout << "creating a DummyMCMC\n";
    DummyMCMC d;
    cout << d;
    cout << "creating a test prior\n" ;
    test s(d, 1);
    cout << s;
    cout << "Constructing test t\n";
    test t(s, suggestRatio) ;
    cout << t;
    cout << "Testing a 1000 iterations of suggest, commit w/o output\n";
    bool last = t.a;
    for(unsigned i = 0; i < 1000; i++)
      {
	t.suggestNewState();
	if(t.a != last)
	  {
	    t.commitNewState();
	    last = t.a;
	  }
	else
	  t.discardNewState();
      }
    Real p = t.getAcceptanceRatio() ;
    cout << "\np = t.getAcceptanceRatio = " << p
	 << "\nempirical suggest Ratio = " 
 	 << (p*s.get_n_params())/(t.get_n_params() *(1.0-p)) 
	 << " should equal " << suggestRatio << " approxiamtely.\n";

    cout << "Testing 10 iterations of suggest, commit/discard, strRepr.\n";    
    cout << "\n(l, p)\t " << t.strHeader() << endl;  
    for(unsigned i = 0; i < 10; i++)
      {
	MCMCObject MOb = t.suggestNewState();
	cout << "\n(" 
	     << MOb.stateProb.val() << ", " 
	     << MOb.propRatio.val() << ")\t";
	cout << t.strRepresentation();  
      }

    cout << "\nTesting copy constructor: u(t) \n";
    test u(t);
    cout << "\tCommiting state\n";
    u.commitNewState();
    cout << "\tu.getAcceptanceRatio = " << u.getAcceptanceRatio() << endl;

    cout << "\nTesting assignment operator: t = u\n";
    t = u;
    cout << "\tt.getAcceptanceRatio = " << u.getAcceptanceRatio() << endl;

  
    return(0);
  }


