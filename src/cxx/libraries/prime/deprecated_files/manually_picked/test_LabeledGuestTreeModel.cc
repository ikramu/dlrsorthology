#include "AnError.hh"
#include "BranchSwapping.hh"
#include "GuestTreeModel.hh"
#include "TreeIO.hh"



int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc < 6 || argc > 7)
    {
      cerr << "usage: test_labeledGuestTreeModel <Guesttree> <HostTree> <gs> <birthrate> <deathrate> [<seed>]\n";
      exit(1);
    }
  try
    {
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[1]);
      TreeIO io = TreeIO::fromFile(guest);
      StrStrMap gs;

      Tree G = io.readGuestTree(0, &gs); 
      G.setName("G");

      string host(argv[2]);
      cerr << "host =  " << host << endl;
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  // Reads times?
      S.setName("S");

      if(gs.size() == 0)
	{
	  gs = TreeIO::readGeneSpeciesInfo(argv[3]);
	}

      LambdaMap lambda(G,S,gs);      

      Real brate = atof(argv[4]);
      Real drate = atof(argv[5]);

      BirthDeathProbs bdp(S, brate, drate);

      cout << "Testing constructors:"
	   << "------------------------------------------\n"
	   << "First construct LabeledGuestTreeModel a with G as the tree.\n";
      LabeledGuestTreeModel a(G, gs, bdp);
      cout << indentString(a.print())
	   << "Then construct LabeledGuestTreeModel b with S as the tree.\n";
      StrStrMap gs2;
      for(unsigned i = 0; i < S.getNumberOfNodes(); i++)
	{
	  Node& n = *S.getNode(i);
	  if(n.isLeaf())
	    {
	      gs2.insert(n.getName(), n.getName());
	    }
	}
      LabeledGuestTreeModel b(S, gs2, bdp);
      cout << indentString(b.print())
	   << "Then use copy constructor to create c from b\n";
      LabeledGuestTreeModel c(b);
      try
	{
	  cout << indentString(c.print())
	       << "\nFinally use operator=, to set c = a:\n";
	  c = a;
	  cout << indentString(c.print());
	}
      catch(AnError e)
	{
	  cerr << "As expected, this did not work, but yielded AnError:\n";
	  cerr << indentString(e.message());
	  cerr << "I guess that we really ought to  fix the assignment\n"
	       << "operator of GammaMap, eventually\n\n";
	}

      cout << "a: Probability of G is " ;
      Probability prG =  a.calculateDataProbability();
      cout << prG
	   << "\t(" << prG.val() << ")\n";

      exit(0);

      cout << "For any pair l1 and l2 such that lca(l1, l2) is node i, "
	   << "the orthology probability is:\n\n"
	   << "i\tprOrtho\tpostprOrtho\n";

      prG = 0;
      ProbVector prOrtho(G);
      LabeledGuestTreeModel g(a);

      prG +=  g.calculateDataProbability();
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  Node* u = G.getNode(i);
	  if(u->isLeaf() == false && lambda[u]->isLeaf() == false)
	    {
	      a.setOrthoNode(u);
	      prOrtho[u] += a.calculateDataProbability();
	    }
	}
      
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  Node* u = G.getNode(i);
	  if(u->isLeaf() == false && lambda[u]->isLeaf() == false)
	    {
	      cout << u->getNumber() <<"\t"<<(prOrtho[u]/prG).val() << endl;
	    }
	}
      
      a.setOrthoNode(0);

//       cout << "To test update(), reroot G from:\n"
// 	   << G;
//       PRNG R;
//       if(argc == 7)
// 	{
// 	  R.setSeed(atof(argv[6]));
// 	}

//       BranchSwapping swap;
//       swap.reRoot(G);
//       swap.NNI(G);
//       cout << "\n to:\n"
// 	   << G;

//       //This fails:
//       //       cout << "This gives a probability of G " 
//       // 	   << a.calculateDataProbability() 
//       // 	   <<"\n";

//       a.update();

//       cout << "After a.update() a gives Probability " 
// 	   << a.calculateDataProbability()
// 	   << "\n";
    }      
  catch(AnError e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }


  return(0);
};
