#include "AnError.hh"

#include "IntegralBirthDeathMCMC.hh"
#include "MCMCObject.hh"

#include <sstream>

namespace beep
{

  using namespace std;

  //-------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //-------------------------------------------------------------
  IntegralBirthDeathMCMC::IntegralBirthDeathMCMC(MCMCModel& prior, Tree &S, 
						 Real birthRate, 
						 Real deathRate,
						 Real beta,
						 bool estimateRates)
    : StdMCMCModel(prior, estimateRates?2:0, S.getName()+"_DupLoss"), 
      IntegralBirthDeathProbs(S, birthRate, deathRate, beta),
      old_birth_rate(birthRate),
      old_death_rate(deathRate),
      estimateRates(estimateRates),
      suggestion_variance(0.1 * (birthRate+deathRate) / 2.0)
  {
  }

  IntegralBirthDeathMCMC::~IntegralBirthDeathMCMC()
  {
  }

  //-------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------
  MCMCObject
  IntegralBirthDeathMCMC::suggestOwnState()
  {
    MCMCObject MOb(1.0, 1.0);
    // Choose what parameter to perturb using 'param':
    Real Idx = paramIdx / paramIdxRatio;
    getRates(old_birth_rate, old_death_rate);
    Real max_rate = MAX_INTENSITY / (getStree().rootToLeafTime() + 
				     getStree().getRootNode()->getTime());
    if(Idx >0.5)
      {
 	setRates(perturbLogNormal(old_birth_rate, 
				  suggestion_variance,
				  Real_limits::min(), 
 				  max_rate,
//  			       MAX_INTENSITY / getStree().rootToLeafTime(),
				  MOb.propRatio),
		 old_death_rate, true);
      }
    else			// Loss rate
      {
	setRates(old_birth_rate, 
		 perturbLogNormal(old_death_rate,
				  suggestion_variance,
				  Real_limits::min(), 
 				  max_rate,
//  				  MAX_INTENSITY / getStree().rootToLeafTime(),
				  MOb.propRatio),
		 true);
      }
    return MOb;  // uniform prior on birth/death rates
  }

  void 
  IntegralBirthDeathMCMC::commitOwnState()
  {
  }

  void
  IntegralBirthDeathMCMC::discardOwnState()
  {
    setRates(old_birth_rate, old_death_rate, true);
  }

  string
  IntegralBirthDeathMCMC::ownStrRep() const
  {
    ostringstream oss;
    if(estimateRates)
      {
	Real b, d;
	getRates(b, d);
	oss << b
	    << ";\t"
	    << d
	    << ";\t"
	  ;
      }
    return oss.str();
  }

  string
  IntegralBirthDeathMCMC::ownHeader() const
  {
    ostringstream oss;
    if(estimateRates)
      {
	oss << "birthRate(float);\tdeathRate(float);\t"
	  ;
      }
    return oss.str();
  }

  Probability IntegralBirthDeathMCMC::updateDataProbability()
  {
    update();
    return 1.0;
  }

  //-------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const IntegralBirthDeathMCMC& A)
  {
    return o << A.print();
  }

  string 
  IntegralBirthDeathMCMC::print() const
  {
    ostringstream oss;
    oss << "Birth and death parameters ";
    if(estimateRates)
      {
	oss << "are estimated during MCMC.\n";
      }
    else
      {
	Real b, d;
	getRates(b, d);
	oss << "are fixed to "
	    << b
	    << " and "
	    << d
	    << ", respectively.\n"
	  ;
      }
    oss << "The time of the first duplication preceeding the root of \n"
	<< "the species tree is integrated over using an exponential\n"
	<< "prior with the fixed parameter, Beta = "
	<< beta
	<< "\n";
    oss << indentString(StdMCMCModel::print());
    return oss.str();
  }
  


//   //-------------------------------------------------------------
//   //
//   // Implementation
//   //
//   //-------------------------------------------------------------

//   // Utility function for changing birth/death rates.
//   // Returns a number in the interval [0.8, 1.25].
//   //-------------------------------------------------------------
//   Real
//   IntegralBirthDeathMCMC::rateChange()
//   {
//     Real r = R.genrand_real2(); 

//     if(r < 5/9)			// Set rate in [0.8, 1.0)
//       {
// 	r = r * 9/5;		// From [0, 5/9) to [0, 1.0)
// 	return  0.8 + 0.2 * r;  // [0.8, 1.0)
//       }
//     else			// Set rate in (1.0, 1.25]
//       {
// 	r = r * 9/4;		// From [5/9, 1.0) to [0, 1.0)
// 	return 1.25 - 0.25 * r; // (1.0, 1.25]
//       }  
//   }


}//end namespace beep
