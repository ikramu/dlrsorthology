#include <iostream>

#include <sys/types.h>
#include <unistd.h>

#include "AnError.hh"
#include "IntegralBirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "DummyStdMCMC.hh"
#include "Hacks.hh"
#include "PRNG.hh"
#include "ReconSeqApproximator.hh"
#include "TreeMCMC.hh"
//#include "ReconciledTreeMCMC_extra.hh"
#include "SimpleMCMC.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"

using namespace beep;
using namespace beep;

//
// Global options
//
char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned printFactor = 1;
unsigned Thinning = 100;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;

unsigned nGammaSamples = 1000;
bool outputGamma = false;

// Birth-Death related
std::map<unsigned, beep::Real> start_times;
bool fixed_rates = false;
double BirthRate = 1.0;
double DeathRate = 1.0;
double topTime = -1.0;
double Beta = 1.0;
bool MustChooseRates = true;

// tree-related
bool fixed_root = false;


// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  
  if (argc < 3) 
    {
      usage(argv[0]);
    }

  try 
    {
      //---------------------------------------------------------
      // Read input and options
      //---------------------------------------------------------

      // Check for options
      //-----------------------------------
      int opt = readOptions(argc, argv);

      if(!quiet)
	{
	  cerr << "Running: ";
	  for(int i = 0; i < argc; i++)
	    {
	      cerr << argv[i] << " ";
	    }
	  cerr << " in directory ";
	  cerr << getenv("PWD");
	  cerr << "\n\n";
	}


      //Get input - tree
      //---------------------------------------------
      if(opt + 2 > argc)
	{
	  cerr << "Arguments missing\n";
	  exit(2);
	}
      StrStrMap gs;
      string G_filename(argv[opt++]);
      TreeIO io = TreeIO::fromFile(G_filename);
      // The new readBeepTree functions does not yet support providing
      // gs when gs-info might not exist!
      Tree G = io.readGuestTree(0, 0);

      io.setSourceFile(argv[opt++]);
      Tree S = io.readHostTree();
      Node *r = S.getRootNode();
      if (r->getTime() == 0) {
	r->setTime(S.rootToLeafTime() / 2);
      }


      cout << "The following Gene tree:\n"
	   << G
	   << " is reconciled with the following Species tree:\n"
	   << S
	   << endl;

      // Check for sanity of input and options:
      //----------------------------------------------------

      if(fixed_root && fixed_rates &&(!do_likelihood))
	{
	  cerr << "All parameters are fixed, there's none to do MCMC over.\n"
	       << "You might want to use the -l option.\n";
	  exit(2);
	}


      // We must not have a zero root time! We have to pretend we know
      // the time from the parent to the previous ancestor.
      //! \todo{ This actually represent a rather abstract variable 
      //! representing a time preceeding than the first branching point in 
      //! the guest tree. It is definitely not related to the host tree 
      //! (except that it must precede - or equal  - its root). Thus the 
      //! class should be renamed and the variable should not be stored in 
      //! S's root time. /bens}
      Node *sr = S.getRootNode();
      if (topTime <= 0.0) 
	{
	  if (sr->getTime() <= 0.0) 
	    {
	      Real height = S.rootToLeafTime();
	      sr->setTime(height / 10.0);
	    }
	}
      else
	{
	  sr->setTime(topTime);
	}

      // If gs is empty (no mapping info was found in the gene tree), 
      // we have to read a file with that info.
      if (gs.size() == 0  && opt < argc)
	{
	  gs = TreeIO::readGeneSpeciesInfo(string(argv[opt++]));
	}
      else
	{
	  cerr << "No mapping gene-name -> species-name found in the gene \n"
	       << "tree ("
	       << G_filename
	       << ") and no file with that mapping given.\n";
	  exit(2);
	}

      





      //--------------------------------------------------------
      // Set up all classes
      //--------------------------------------------------------

      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}

      DummyMCMC dm(rand);
      
      IntegralBirthDeathMCMC bdm(dm, S, BirthRate, DeathRate, 
				 Beta, !fixed_rates);

      // Set up Reconciliation handlers
      //---------------------------------------------------------
      ReconciliationSampler rs(G, gs, bdm, rand);
      if (MustChooseRates) 
	{
	  rs.chooseStartingRates(BirthRate, DeathRate);
	  cerr << "Birth- and Death rates are set to: "
	       << BirthRate 
	       << " and "
	       << DeathRate
	       << ", respectively.\n";
	}

      // MCMC class for tree perturbations
      //---------------------------------------------------------
      ReconciledTreeMCMC gtm(bdm, rs);
      gtm.fixTree();
      if(fixed_root)
	{
	  gtm.fixRoot();
	}


      // Start building the Monte Carlo approximat
      //---------------------------------------------------------
      DummyStdMCMC dsm;
      ReconSeqApproximator rsa(dm, dsm, rs, nGammaSamples);
      if(outputGamma)
	{
	  rsa.outputReconciliations(true);
	}

      if (do_likelihood)
	{
	  cout << gtm.initStateProb() << endl;
	  exit(0);
	}      

      // Optional debug info
      //---------------------------------------------
      if (show_debug_info)
	{
	  G.debug_print();
	  cerr << endl;
	  
	  S.debug_print();
	  cerr << endl;
	}
      
      // Create MCMC handler
      //---------------------------------------------
      SimpleMCMC iterator(gtm, Thinning);
      if (outfile != NULL)
	{
	  try 
	    {
	      iterator.setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  

      if (quiet)
	{
	  iterator.setShowDiagnostics(false);
	}
      else
	{
	  cerr << "Start MCMC\n";
	}

      //--------------------------------------------------------
      // Now we're set to do stuff
      //---------------------------------------------------------

      time_t t0 = time(0);
      clock_t ct0 = clock();


      iterator.iterate(MaxIter, printFactor);
      // //       Probability p = rsa.suggestNewState().stateProb;
      // //       //Probability p = rsa.updateDataProbability();

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;
      
      if (!quiet)
	{
	  cerr << gtm.getAcceptanceRatio()
	       << " = acceptance ratio\n";
	}
      
      if (gtm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }

    }
  catch (AnError e) 
    {
      e.action();
    }
}

void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <gene tree> <species tree>"
    << " [<gene-species map<]\n"
    << "\n"
    << "This program uses MCMC-analysis to estimate orthology probabilities\n"
    << "for gene-pairs according to a duplications-loss model, and for a\n"
    << "given gene- and species tree. Unless the option -Gg is given, the\n"
    << "input gene tree is considered unrooted, and the root is integrated\n"
    << "over in the MCMC-analysis. Similarly, unless option -Bf is given the\n"
    << "MCMC-analysis integrates over rates for the duplication-loss model\n"
    << "Output comprise values for: (rooted) genetree, orthology pair\n"
    << "probabilities, birth and death rate in MCMC analysis.\n"
    << "Parameters:\n"
    << "   <gene tree>           Gene tree in Newick format.\n"
    << "   <species tree>        Species tree in Newick format. Edgelengths\n"
    << "                         are important and represent time.\n"
    << "   <gene-species map>    Optional. This file contains lines with a\n"
    << "                         gene name in the first column and species\n"
    << "                         name as found in the species tree in the\n"
    << "                         second. You can also choose to associate\n"
    << "                         the genes with species in the gene tree.\n"
    << "                         Please see documentation.\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -s <int>              Seed for pseudo-random number generator. If\n"
    << "                         set to 0 (default), the process id is used\n"
    << "                         as seed.\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -d                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -p <int>              Precision: The number of iterationss to\n"
    << "                         approximate the likelihood of reconciliations\n"
    << "                         (" << nGammaSamples << ")\n"
    << "   -G<option>            Options related to the gene tree\n"
    << "     -Gg                 Fix the root of the input gene tree.\n"
    << "     -Gr                 Output reconciliation estimates.\n"
    << "   -B<option>            Options related to the birth death model\n"
    << "     -Bf <float> <float> Fix rates for duplication and loss. Rates are \n"
    << "                         given as floating point values.\n"
    << "     -Bp <float> <float> Start values for duplication and loss rate.\n"
    << "                         If no rates are given, sensible start values\n"
    << "                         are chosen automatically.\n"
    << "     -Bb <float>         The beta parameter for a prior distribution\n"
    << "                         on species root distance. (" << Beta << ")\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  usage(argv[0]);
	  break;

	case 'o':
	  if (opt + 1 < argc)
	    {
	      outfile = argv[++opt];
	    }
	  else
	    {
	      cerr << "Expected filename after option '-o'\n";
	      usage(argv[0]);
	    }
	  break;
	case 'i':
	  if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	    {
	      cerr << "Expected integer after option '-i'\n";
	      usage(argv[0]);
	    }
	  break;

	case 't':
	  if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	    {
	      cerr << "Expected integer after option '-t'\n";
	      usage(argv[0]);
	    }
	  break;

	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  quiet = true;
	  break;
	
	case 'd':
	  show_debug_info = true;
	  break;
	  
	case 'l':
	  do_likelihood = true;
	  break;
	   
	case 'p':
	  if (sscanf(argv[++opt], "%d", &nGammaSamples) == 0)
	    {
	      cerr << "Expected integer after option '-p'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'G':
	  {
	    switch(argv[opt][2])
	      {
	      case 'g':
		{
		  fixed_root = true;
		  break;
		}
	      case 'r':
		{
		  outputGamma = true;
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); //Check for correct error code
		}
		break;
	      }
	    break;
	  }

	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		{
		  fixed_rates = true;
		  // Don't break here, because we want to fall through to 'r' to 
		  // set the rates that are arguments both to '-p' and '-f'!
		}
	      case 'p':
		{
		  MustChooseRates = false;
		  if (++opt < argc) 
		    {
		      float tmp=0.0;
		      if (sscanf(argv[opt], "%f", &tmp) < 1) 
			{
			  cerr << "Error: Expected a gene duplication rate, found '";
			  cerr << argv[opt] << "'\n";
			  usage(argv[0]);
			}
		      cerr << tmp << endl;
		      opt++;
		      if (sscanf(argv[opt], "%f", &DeathRate) < 1) 
			{
			  cerr << "Error: Expected a gene loss rate, found '";
			  cerr << argv[opt] << "'\n";
			  usage(argv[0]);
			}
		      
		    }
		  else
		    {
		      cerr << "Expected gene duplication and loss rates (birth "
			   << "and death rates) for option '-r' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'b':
		{
		  if (++opt < argc) 
		    {
		      if (sscanf(argv[opt], "%lf", &Beta) == 0)
			{
			  cerr << "Expected number after option '-b'\n";
			  usage(argv[0]);
			}
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); //Check for correct error code
		}
		break;
	      }
	  }
	}
      opt++;
    }
  return opt;
}
