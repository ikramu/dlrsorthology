#include "AnError.hh"
#include "DummyMCMC.hh"
#include "MCMCObject.hh"

  int main()
  {
    using namespace beep;
    using namespace std;
    cout << "Constructing DummyMCMC - Should throw AnError\n";
    DummyMCMC t;

    try
      { MCMCObject MOb = t.suggestNewState();}
    catch(AnError e)
      { e.action();}
    cout << "t.initStateProb() = " << t.initStateProb().val() << endl << endl;
    cout << "t.currentStateProb = " << t.currentStateProb().val() << endl<< endl;

    cout << "t.commitNewState - Should throw AnError\n";
    try
      { t.commitNewState();}
    catch(AnError e)
      { e.action();}
    cout << "\nt.discardNewState - Should throw AnError\n";
    try
      { t.discardNewState();}
    catch(AnError e)
      { e.action();}


    cout << "\nt.strHeader: " << t.strHeader() << " (should be empty)\n";
    cout << "\nt.strRepresentation: " << t.strRepresentation() 
	 << " (should be empty)\n";
    cout << "\nt.nParams() = " << t.nParams() << endl;

    cout << "\nt.getAcceptanceRatio = " << t.getAcceptanceRatio() << endl;

    PRNG& r = t.getPRNG();
    cout << "\nTesting t.getPRNG()->genrand_modulo(3) = " 
	 <<  r.genrand_modulo(3) << endl;

    cout << "\nTesting copy constructor: u(t) \n";
    DummyMCMC u(t);
    cout << "\tu.getAcceptanceRatio = " << u.getAcceptanceRatio() << endl;

    cout << "\nTesting assignment operator: t = u\n";
    t = u;
    cout << "\tt.getAcceptanceRatio = " << u.getAcceptanceRatio() << endl;  

    return(0);
  }

