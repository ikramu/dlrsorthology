#include "AnError.hh"
#include "BranchSwapping.hh"
#include "DummyMCMC.hh"
#include "TreeMCMC.hh"
#include "TreeIO.hh"



int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc != 2)
    {
      cerr << "usage: test_UniformTreeMCMC <Tree> \n";
      exit(1);
    }

  //Get tree and Data
  //---------------------------------------------
  string guest(argv[1]);
  TreeIO io = TreeIO::fromFile(guest);
  Tree G = io.readHostTree();  // Reads times?
  G.setName("G");

  cerr << G.print(true,true,true,true);
  DummyMCMC dm;

  cout << "Testing constructors:"
       << "------------------------------------------\n"
       << "First construct UniformTreeMCMC 'a' with name 'Tree'.\n";
  UniformTreeMCMC a(dm, G, "Tree", 0.5);
  cout << indentString(a.print())
       << "Then use copy constructor to create 'b' from 'a', and fixRoot and fixTree()of 'b'\n";
  UniformTreeMCMC b(a);
  b.fixRoot();
  b.fixTree();
  cout << indentString(b.print())
       << "\nFinally use operator=, to set 'b' = 'a':\n";
  b = a;
  cout << indentString(b.print());


  cout << "Simulate a short MCMC-run, accepting every move, on a to test \n"
       << "MCMC-associated functions. Use dynamic binding through reference\n"
       << "-----------------------------------------------------------------\n";
  TreeMCMC& ra = a;
  cout << "#prob\titer\t" << a.strHeader() << "\n";
  MCMCObject MOb;
  Probability prob = 0;
  for(unsigned i = 0; i < 10; i++)
    {
      MOb = a.suggestNewState();
      if(MOb.stateProb >= prob)
	{
	  a.commitNewState();
	  prob = MOb.stateProb;
	}
      else
	a.discardNewState();
      cout << prob << "\t" << i << "\t"
	   << a.strRepresentation()<< "\n";
    }


  return(0);
};
