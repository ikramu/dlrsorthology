//#define DEBUG_Q

#include <iostream>
#include "LA_DiagonalMatrix.hh"
#include "LA_Matrix.hh"
#include "LA_Vector.hh"

using namespace beep;
using namespace std;
int
main()
{
  try
    {
      cout << "\nTest av LA_Vector\n" 
	   << "-----------------------------\n";
      
      unsigned dim = 2;

      double dy[] = {1,2};
      beep::LA_Vector y(dim, dy);
      cout << " y\n" << y;

      beep::LA_Vector x(dim);
      cout << " x\n" << x;

      x = y * 5; 
      cout << " x = y * 5\n" << x;

      beep::LA_Vector z(0.2 * x);
      cout << " z(0.2 * x)\n" << z;

      beep::LA_Vector w = 0.2 * x;
      cout << " w = 0.2 * x\n" << w;
      
      x[1] = 5;
      cout << " x[1] = 5\n" << x;

      cout << " w.* z\n" << w.ele_mult(z);
      cout << " x * y\n" << x * y << "\n";
      cout << " x.sum()\n" << x.sum() << "\n";

      cout << "\nTest av LA_Matrix\n" 
	   << "-----------------------------\n";

      dim = 2;

      double a[] = {1,2,3,4};
      beep::LA_Matrix A(dim, a);
      cout << " A\n" << A;
      
      beep::LA_Matrix B(dim);
      cout << " B\n" << B;
      
      B = A * 5; 
      cout << " B = A * 5\n" << B;
      
      beep::LA_Matrix C(0.2 * B);
      cout << " C(0.2 * B)\n" << C;
      
      beep::LA_Matrix D = 0.2 * B;
      cout << " D = 0.2 * B\n" << D;
      
      A(1,1) = 5;
      cout << " A(1,1) = 5\n" << A << endl;
      cout << " A * y\n" << A * y << "\n";
      cout << " A * B\n" << A * B << "\n";
      cout << " transpose(A)\n" << A.transpose();
      cout << " A*inverse(A) \n" << A * A.inverse();
      cout << " A.trace()\n" << A.trace() << "\n";

      cout << "\nTest av LA_MAtrix::col_mult\n" 
	   << "-----------------------------\n";
      double s[] = {1,2,3,4, 5,6,7,8,2,3,4,5,1,2,3,4};
      beep::LA_Matrix S(4, s);
      cout << " S\n" << S << endl;

      beep::LA_Vector t(4);
      cout << "t = " <<t <<endl;

      t[1] = 1;      
      cout << " t(4), t[1] = 1\n"  << t << endl;     
      cout << "S * t \n" << S*t << endl;

//       for(unsigned i = 0; i < 4; i++)
// 	{
// 	  cout << " S.col_mult(1,"<<i<<")\n" << S.col_mult(1,i) << "\n";
// 	}

      for(unsigned i = 0; i < 4; i++)
	{
	  S.col_mult(t,1,i);
	  cout << " S.col_mult(t, 1,"<<i<<")\n" << t << "\n";
	}


      cout << "\nTest av LA_DiagonalMatrix\n" 
	   << "-----------------------------\n";

      dim = 2;

      double k[] = {1,2};
      beep::LA_DiagonalMatrix K(dim, a);
      cout << " K\n" << K;
      
      beep::LA_DiagonalMatrix L(dim);
      cout << " L\n" << L;
      
      L = K * 5; 
      cout << " L = K * 5\n" << L;
      
      beep::LA_DiagonalMatrix M(0.2 * L);
      cout << " M(0.2 * L)\n" << M;
      
      beep::LA_DiagonalMatrix N = 0.2 * L;
      cout << " N = 0.2 * L\n" << N;
      
      K(1,1) = 5;
      cout << " K(1,1) = 5\n" << K;
      
      cout << " K * y\n" << K * y << "\n";
      cout << " K * L\n" << K * L << "\n";
      cout << " transpose(K)\n" << K.transpose();

      cout << "inverse(K) * K \n" << K.inverse() * K;
      cout << " K.trace()\n" << K.trace() << "\n";



      cout << "\nTest av mixed Matrix multiplications\n" 
	   << "-----------------------------\n";

      cout << " A \n" << A;
      cout << " K \n" << K;
      cout << " A * K \n" << A * K;
      cout << " K * A \n" << K * A;

      

    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
}

