#ifndef RECONSEQAPPROXIMATOR_HH
#define RECONSEQAPPROXIMATOR_HH

#include <string>

#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "ReconciliationSampler.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "StdMCMCModel.hh"
//#include "mtl/mtl.h"
#include "OrthologyMatrix.hh"

/*********************************************************************

  ReconSeqApproximator

  The purpose of this class is to approximate $\Pr(Q, G| \lambda, \mu)$
  by sampling reconciliations and nodes times and compute an average.
  The node time sampling is delegated to another sub class of 
  ProbabilityModel: SeqProbApproximator.

**********************************************************************/

namespace beep
{

  class ReconSeqApproximator : public ProbabilityModel, 
			       public StdMCMCModel
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //-------------------------------------------------------------
    ReconSeqApproximator(MCMCModel& prior,
// 			 SeqProbApproximator& spa,
			 StdMCMCModel& nestedLike,
			 ReconciliationSampler& rs,
			 const unsigned& n_samples);
    ReconSeqApproximator(const ReconSeqApproximator &rsa); // Copy constuctor
    ~ReconSeqApproximator();
    ReconSeqApproximator& operator=(const ReconSeqApproximator& rsa);


    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    void outputReconciliations(bool value = true);

    //-----------------------------------------------------------------------
    // StdMCMCModel interface
    //-----------------------------------------------------------------------
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();

    std::string ownStrRep() const;
    std::string ownHeader() const;

    Probability updateDataProbability();


  protected:
    //-----------------------------------------------------------------------
    // ProbabilityModel interface
    //-----------------------------------------------------------------------
    virtual Probability calculateDataProbability();
    virtual void update();

public:
    //----------------------------------------------------------------------
    // I/O
    // I have not converged on a consensus for this yet, but the 
    // following functions should suffice
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const ReconSeqApproximator& A);
    std::string print() const;
    
  private:
    //-----------------------------------------------------------------------
    //
    // Implementation
    //
    //-----------------------------------------------------------------------
    Probability cached_sumDataProb(const unsigned& samples_done,
				   Probability sum);
    Probability simple_sumDataProb(const unsigned& samples_done,
				   Probability sum);

    // Reset orthology matrix
    //-----------------------------------------------------------------------
    //    void resetOrthology();

    // Update the orthology matrix;
    //-----------------------------------------------------------------------
    //    void recordOrthology(const GammaMap &gamma, const Probability p);

    // Get a string representation of a lower triangular matrix containing
    // orthology predictions.
    //-----------------------------------------------------------------------
    //   std:: string strOrthology();

  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //  
    //----------------------------------------------------------------------
    Tree                  &G;	// The gene tree
    StdMCMCModel& nestedLike;
    ReconciliationSampler& sampler;
    const GammaMap* gamma;     // ref to sampler's gamma to make access easier
    unsigned               n_samples; // This tells us how many 
    // reconciliations to sample.

    OrthologyMatrix orthology;	// Collect orthology information in an lower triangular matrix
    std::map<std::string, Probability> reconciliations;

    bool estimateGamma;
    //
    // An error is generated if you try to get more samples that this:
    //
    static const unsigned  MAX_N_SAMPLES=10000000; 

    //
    // I am not fond of introducing this TreeIO object, but output of
    // reconciled tree, i.e., a gene tree with a reconciliation
    // (GammaMap) embedded, is done in TreeIO and I don't want to
    // create that object everytime I output a tree.
    //
  };


  // This is a functor class so that I'll be able to use STL for the 
  // scaling of the reconciliations attribute in simple_sumDataProb()
  // I am not sure if it's really any speed-up / bens
  //----------------------------------------------------------------------
  template<class T1, class T2>
  class pair_divide 
//     : public std::unary_function<std::pair<T1, T2>, void>
  {
  public:
    pair_divide(const T2& denom)
      : denominator(denom)
    {
    }  

    void operator()(std::pair<const T1, T2>& numerator) const
    {
      numerator.second /= denominator;
      return;
    };
  private:
    T2 denominator;
  };


}//end namespace beep
#endif
