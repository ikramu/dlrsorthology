#include <iomanip>
#include <iostream>

#include "AnError.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "Probability.hh"
#include "UniformDensity.hh"

void
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <density>"
       << " <parameter a>"
       << " <parameter b>"
       << " <start interval>"
       << " <end interval>"
       << " <step>"
       << " <embedded>"
       << "\n"
       << "Parameters:\n"
       << "   <density>         'Gamma', 'InvGauss', 'LogNorm' or 'Uniform'\n"
       << "   <parameter a>     a double\n"
       << "   <parameter b>     a double\n"
       << "   <start>           a double\n"
       << "   <end>             a double > start\n"
       << "   <step>            a double from ]0, stop - start]\n"
       << "   <embedded>        a bool, if true, a and b are taken to be \n"
       << "                     embedded parameters, else a = mean, b = var\n"
       << " the program will output step through the interval[start, stop],\n"
       << " by step, outputting Uniform(x), Gamma(x), InvGauss(x) and  \n"
       << " logNorm(x), parameterized with mean and variance\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 8) 
    {
      usage(argv[0]);
    }

  try
    {
      Real mean = atof(argv[2]);
      Real variance = atof(argv[3]);
      Real start = atof(argv[4]);
      Real stop = atof(argv[5]);
      Real step = atof(argv[6]);
      bool embedded = false;
      if(string(argv[7]) == "true")
	embedded = true;

      Density2P* df;

      cerr << "Testing constructor, copy constructor, assigment op, \n"
	   << "set(Embedded)Parameters(mean, variance),getMean(), \n"
	   << "getVariance() and print()\n"
	   << "---------------------------------------------------------\n";
      switch(argv[1][0])
	{
	case 'G':
	  {
	    cerr << "Construct a density, Orig, with ";
	    if(embedded)
	      cerr << "alpha = beta = 1.0:\n";
	    else	      
	      cerr << "mean = variance = 1.0:\n";
	    GammaDensity& Orig = *new GammaDensity(1.0,1.0, embedded);
	    cerr << indentString(Orig.print());
	    cerr << "make a copy, Copy, of Orig. Copy is now:\n";
	    GammaDensity Copy(Orig);
	    cerr << indentString(Copy.print());
	    if(embedded)
	      {
		cerr << "set its alpha and beta to those in arguments\n";
		Copy.setEmbeddedParameters(mean, variance);
	      }
	    else
	      {
		cerr << "set its mean and variance to those in arguments\n";
		Copy.setParameters(mean, variance);
	      }
	    cerr << indentString(Copy.print())
		 << indentString("Copy's mean = ") << Copy.getMean()
		 << " Copy's variance = " << Copy.getVariance() << "\n";
	    cerr << "Set df = Copy. df  is now:\n";
	    Orig = Copy;
	    cerr << indentString(Orig.print());
	    cerr << "set Density2P* df = &Orig:\n";
	    df = &Orig;
	    break;
	  }
	case 'I':
	  {
	    cerr << "Construct a density, Orig, with ";
	    if(embedded)
	      cerr << "alpha = beta = 1.0:\n";
	    else	      
	      cerr << "mean = variance = 1.0:\n";
	    InvGaussDensity& Orig = *new InvGaussDensity(1.0,1.0, embedded);
	    cerr << indentString(Orig.print());
	    cerr << "make a copy, Copy, of Orig. Copy is now:\n";
	    InvGaussDensity Copy(Orig);
	    cerr << indentString(Copy.print());
	    if(embedded)
	      {
		cerr << "set its alpha and beta to those in arguments\n";
		Copy.setEmbeddedParameters(mean, variance);
	      }
	    else
	      {
		cerr << "set its mean and variance to those in arguments\n";
		Copy.setParameters(mean, variance);
	      }
	    cerr << indentString(Copy.print())
		 << indentString("Copy's mean = ") << Copy.getMean()
		 << " Copy's variance = " << Copy.getVariance() << "\n";
	    cerr << "Set df = Copy. df  is now:\n";
	    Orig = Copy;
	    cerr << indentString(Orig.print());
	    cerr << "set Density2P* df = &Orig:\n";
	    df = &Orig;
	    break;
	  }
	case 'L':
	  {
	    cerr << "Construct a density, Orig, with ";
	    if(embedded)
	      cerr << "alpha = beta = 1.0:\n";
	    else	      
	      cerr << "mean = variance = 1.0:\n";
	    LogNormDensity& Orig = *new LogNormDensity(1.0,1.0, embedded);
	    cerr << indentString(Orig.print());
	    cerr << "make a copy, Copy, of Orig. Copy is now:\n";
	    LogNormDensity Copy(Orig);
	    cerr << indentString(Copy.print());
	    if(embedded)
	      {
		cerr << "set its alpha and beta to those in arguments\n";
		Copy.setEmbeddedParameters(mean, variance);
	      }
	    else
	      {
		cerr << "set its mean and variance to those in arguments\n";
		Copy.setParameters(mean, variance);
	      }
	    cerr << indentString(Copy.print())
		 << indentString("Copy's mean = ") << Copy.getMean()
		 << " Copy's variance = " << Copy.getVariance() << "\n";
	    cerr << "Set df = Copy. df  is now:\n";
	    Orig = Copy;
	    cerr << indentString(Orig.print());
	    cerr << "set Density2P* df = &Orig:\n";
	    df = &Orig;
	    break;
	  }
	case 'U':
	  {
	    cerr << "Construct a density, Orig, with ";
	    if(embedded)
	      cerr << "alpha = beta = 1.0:\n";
	    else	      
	      cerr << "mean = variance = 1.0:\n";
	    UniformDensity& Orig = *new UniformDensity(1.0,1.0, embedded);
	    cerr << indentString(Orig.print());
	    cerr << "make a copy, Copy, of Orig. Copy is now:\n";
	    UniformDensity Copy(Orig);
	    cerr << indentString(Copy.print());
	    if(embedded)
	      {
		cerr << "set its alpha and beta to those in arguments\n";
		Copy.setEmbeddedParameters(mean, variance);
	      }
	    else
	      {
		cerr << "set its mean and variance to those in arguments\n";
		Copy.setParameters(mean, variance);
	      }
	    cerr << indentString(Copy.print())
		 << indentString("Copy's mean = ") << Copy.getMean()
		 << " Copy's variance = " << Copy.getVariance() << "\n";
	    cerr << "Set df = Copy. df  is now:\n";
	    Orig = Copy;
	    cerr << indentString(Orig.print());
	    cerr << "set Density2P* df = &Orig:\n";
	    df = &Orig;
	    break;
	  }
	default:
	  {
	    cerr << "density " << argv[1] << " is not recognized!\n";
	    exit(1);
	    break;
	  }
	}
      cerr << "\nTesting df's operator<<(), getRange(),getMeanRange()\n"
	   << "getVarianceRange()\n"
	   << "-----------------------------------------------------------\n"
	   << *df;
      Real min, max;
      cerr << "Valid ranges:\n"
	   << "mean\t\t";
      df->getMeanRange(min, max);
      cerr << "(" << min << "," << max << ")\n"
	   << "variance\t";
      df->getVarianceRange(min, max);
      cerr << "(" << min << "," << max << ")\n"
	   << "value\t\t";
      df->getRange(min, max);
      cerr << "(" << min << "," << max << ")\n";
      cerr << "Try resetting range to (0.3,100)\n";
      df->setRange(0.3, 100);
      cerr << "Valid value range:\t";
      df->getRange(min, max);
      cerr << "(" << min << "," << max << ")\n";


      cerr << "\nTesting sampleValue(p) for Errors over a range of p\n"
	   << "values (not InvGaussian)\n"
	   << "--------------------------------------------------------\n";
      for(Real x = 0.1; x < 1; x += 0.1)
	{
	  cerr << x << "\t"
	       << df->sampleValue(x) << "\n";
	}


      cerr << "\nTesting operator(x) over (start, stop) incr. step\n"
	   << "--------------------------------------------------------\n";
      cerr << "x\tf(x)\tisInRange():\n";
      for(Real i = start; i <= stop; i += step)
	{
// 	  Real j = 0.1/i;
	  cout << i << "\t" << (*df)(i).val();
// 	  if(df->isInRange(j))
// 	    cout << "\ttrue\n";
// 	  else
// 	    cout << "\tfalse\n";
	  cout << endl;
	}
      if(argv[1][0] == 'G')
	{
	  cerr << "\nTesting cdf(x) over (start, stop) incr. step\n"
	       << "--------------------------------------------------------\n";
	  cerr << "x\tf(x)\tisInRange():\n";
	  for(Real i = start; i <= stop; i += step)
	    {
	      // 	  Real j = 0.1/i;
	      cout << i << "\t" << df->cdf(i).val();
	      // 	  if(df->isInRange(j))
	      // 	    cout << "\ttrue\n";
	      // 	  else
	      // 	    cout << "\tfalse\n";
	      cout << endl;
	    }
	}

      if(argv[1][0] == 'L')
	{
	  cout << "\n\nTesting operator(x, precision) incr. step\n"
	       << "-----------------------------------------------------\n"
	       <<"prec\tx\tP(x+1/2i)-\tP(x-1/2i)\tp(x)\tisInRange();\n";
	  Real precision = 1.0;
	  for(Real x = start; x <= stop; x += step)
	    {
	      for(unsigned i = 1; i < 10E5; i*=10)
		{
		  cout << precision/i << "\t" << i << "\t" << (*df)(x,precision/i).val()  << "\t" << (*df)(x).val();
		  if(df->isInRange(x))
		    cout << "\ttrue\n";
		  else
		    cout << "\tfalse\n";
		}
	      cout << endl;
	    }
	}
//       else cout << "argv[1][0] =" << argv[1][0]<< endl;


      delete df;
      return 0;

    }
  catch(exception& e)
    {
      cerr << e.what();
      return -1;
    }
}
 
