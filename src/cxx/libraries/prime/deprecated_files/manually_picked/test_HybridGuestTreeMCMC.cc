#include "AnError.hh"
#include "BranchSwapping.hh"
#include "HybridGuestTreeMCMC.hh"
#include "HybridTreeIO.hh"
#include "DummyMCMC.hh"


int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc < 5 || argc > 6)
    {
      cerr << "usage: test_GuestTreeMCMC <Hosttree> <Guesttree> <gs> <birthrate> <deathrate> [<seed>]\n";
      exit(1);
    }
  try
    {
      unsigned opt = 1;
      HybridTreeIO io_in = HybridTreeIO::fromFile(string(argv[opt++]));
      HybridTree S = io_in.readHybridTree();
      if(S.getName() == "Tree")
	{
	  S.setName("Host");
	}
      cout << "Hybrid tree\n"
	   << S.print();
      cout << "Update binary tree\n";
      cout << S.getBinaryTree();
      cout << "binary2Hybrid\n"
	   << S.printBinary2Hybrid()<<endl;
      cout << "hybrid2Binary\n" 
	   << S.printHybrid2Binary() << endl;

      //Get tree and Data
      //---------------------------------------------
      string guest(argv[opt++]);
      TreeIO io = TreeIO::fromFile(guest);
      StrStrMap gs;

      Tree G = io.readGuestTree(0, &gs); 
      G.setName("G");

      cerr << G<<endl;
      if(gs.size() == 0)
	{
	  gs = TreeIO::readGeneSpeciesInfo(argv[opt++]);
	}
      cout << "gs = \n" << gs << endl;

      Real brate = atof(argv[opt++]);
      Real drate = atof(argv[opt++]);

      BirthDeathProbs bdp(S.getBinaryTree(), brate, drate);
 
      cout << "Testing constructors:"
	   << "------------------------------------------\n"
	   << "First construct GuestTreeMCMC a with G as the tree.\n";
      DummyMCMC dm; 
      HybridGuestTreeMCMC a(dm, G, S, gs, bdp, 0.5);
      cout << indentString(a.print())
	   << "\nThen construct GuestTreeMCMC b with S as the tree.\n";
      StrStrMap gs2;
      for(unsigned i = 0; i < S.getBinaryTree().getNumberOfNodes(); i++)
	{
	  Node* n = S.getBinaryTree().getNode(i);
	  if(n->isLeaf())
	    {
	      gs2.insert(n->getName(), 
			 S.getCorrespondingHybridNode(n)->getName());
	    }
	}
      HybridGuestTreeMCMC b(dm, S.getBinaryTree(), S, gs2, bdp, 0.5);
      cout << indentString(b.print())
	   << "\nThen use copy constructor to create c from b\n";
      HybridGuestTreeMCMC c(b);
      try
	{
	  cout << indentString(c.print())
	       << "\nFinally use operator=, to set c = a:\n";
	  c = a;
	  cout << indentString(c.print());
	}
      catch(AnError e)
	{
	  cerr << "As expected, this did not work, but yielded AnError:\n";
	  cerr << indentString(e.message());
	  cerr << "I guess that we really ought to  fix the assignment\n"
	       << "operator of GammaMap, eventually\n\n";
	}

      bdp.update();
      a.update();
      cout << "a: Probability of G is " ;
      Probability prG =  a.calculateDataProbability();
      cout << prG
	   << "\n";

      cout << "Let's try a simple MCMC:\n\n";

      Probability p = 0;
      cout << "#" << "L\t" <<a.strHeader() << endl;
      for(unsigned i = 0; i < 10; i++)
	{
	  Probability p1 = a.suggestNewState().stateProb;
	  if(p1 < p)
	    a.discardNewState();
	  else
	    {
	      a.commitNewState();
	      p = p1;
	    }
	  cout << p << a.strRepresentation() <<endl;
	}
      
      exit(0);
    }      
  catch(AnError e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }


  return(0);
};
