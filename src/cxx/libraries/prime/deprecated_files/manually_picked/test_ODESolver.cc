#include <iostream>
#include <math.h>
#include <vector>

#include "AnError.hh"
#include "Beep.hh"
#include "ODESolver.hh"


/**
 * Simple 1-component ODE.
 * Compute for e.g. 0<=x<=2 and y(0)=6.
 */
struct SinODE : public beep::ODESolver
{
	SinODE() : beep::ODESolver(1e-4, 1e-4) {};
	virtual ~SinODE() {};
	
	virtual void fcn(beep::Real x, const std::vector<beep::Real>& y, std::vector<beep::Real>& f)
	{
		f[0] = sin(x * y[0]);
	}
};

/**
 * 2-component ODE (the Brusselator).
 * Compute for e.g. 0<=x<=20 and y(0)=[1.5, 3.0].
 */
struct BrusselatorODE : public beep::ODESolver
{
	BrusselatorODE() : beep::ODESolver(1e-4, 1e-4) {};
	virtual ~BrusselatorODE() {};
	
	virtual void fcn(beep::Real x, const std::vector<beep::Real>& y, std::vector<beep::Real>& f)
	{
		f[0] = 1 + y[0] * y[0] * y[1] - 4 * y[0];
		f[1] = 3 * y[0] - y[0] * y[0] * y[1];
	}
};

/**
 * Classic 2-component autonomous ODE for fox-bunny growth.
 * Compute for e.g. 0<=x<=9 and y(0)=[150,300]. 
 */
struct FoxesAndBunniesODE : public beep::ODESolver
{
	FoxesAndBunniesODE() : beep::ODESolver(1e-4, 1e-4) {};
	~FoxesAndBunniesODE() {};
	
	virtual void fcn(beep::Real x, const std::vector<beep::Real>& y, std::vector<beep::Real>& f)
	{
		f[0] = -y[0] + 0.01 * y[0] * y[1];     // Fox growth rate.
		f[1] = 2 * y[1] - 0.01 * y[0] * y[1];  // Bunny growth rate.
	}
};


/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
	using namespace std;
	using namespace beep;

	try
	{
		Real tempr;
		
		
		// Create ODE solver of choice.
		ODESolver* ode = NULL;
		unsigned n;
		
		cout << "Available ODEs:" << endl
			<< " 1: Sine (try e.g. 0<=x<=2 and y(0)=6)" << endl
			<< " 2: Brusselator (try e.g. 0<=x<=20 and y(0)=[1.5, 3.0])" << endl
			<< " 3: Foxes and bunnies (try e.g. 0<=x<=9 and y(0)=[150, 300])" << endl;
		cout << "Please choose function: ";
		int funcNo; cin >> funcNo;
		switch (funcNo)
		{
		case 1:
			ode = new SinODE();
			n = 1;
			break;
		case 2:
			ode = new BrusselatorODE();
			n = 2;
			break;
		case 3:
			ode = new FoxesAndBunniesODE();
			n = 2;
			break;
		default:
			throw AnError("Wrong function number!");
		}
		
		// Read initial values.
		Real x;
		Real xend;
		vector<Real> y = vector<Real>();
		Real h;
		cout << "Enter initial x: ";
		cin >> x;
		cout << "Enter x end: ";
		cin >> xend;
		cout << "Enter initial y (one component per line): ";
		for (unsigned i=0; i<n; ++i) { cin >> tempr; y.push_back(tempr); }
		cout << "Enter initial h (0 for auto): ";
		cin >> h;
		
		// Solve!!!!
		cout << "STARTED WITH x: " << x << ", xend: " << xend << ", y:";
		for (unsigned i=0; i<n; ++i) { cout << " " << y[i]; }
		cout << ", h: " << h << endl;
		ODESolver::SolverResult res = ode->dopri5(x, xend, y, h);
		switch (res)
		{
		case ODESolver::SUCCESSFUL:
		case ODESolver::SUCCESSFUL_INTERRUPTED:
			cout << "SUCCESS"; break;
		case ODESolver::INCONSISTENT_INPUT:
			cout << "FAILED WITH BAD INPUT"; break;
		case ODESolver::INSUFFICIENT_NMAX:
			cout << "FAILED WITH TOO MANY ITERATIONS"; break;
		case ODESolver::TOO_SMALL_GEN_STEP_SIZE:
			cout << "FAILED WITH TOO SMALL STEP SIZE"; break;
		case ODESolver::PROBABLY_STIFF:
			cout << "FAILED WITH PROBABLY STIFF FUNCTION"; break;
		default:
			break;
		}
		cout << " x: " << x << ", xend: " << xend << ", y:";
		for (unsigned i=0; i<n; ++i) { cout << " " << y[i]; }
		cout << ", h: " << h << endl;
		
		// Print stats.
		unsigned nfcn, nstep, naccpt, nrejct;
		ode->getStatistics(nfcn, nstep, naccpt, nrejct);
		cout << "Function evals: " << nfcn
			<< ", iterations: " << nstep
			<< ", acc. steps: " << naccpt
			<< ", rej. steps: " << nrejct
			<< endl;
		
		delete ode;
	}
	catch (AnError& e)
	{
		e.action();
		cout << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
	}
};
