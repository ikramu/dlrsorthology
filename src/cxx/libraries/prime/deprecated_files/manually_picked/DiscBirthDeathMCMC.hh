#ifndef DISCBIRTHDEATHMCMC_HH
#define DISCBIRTHDEATHMCMC_HH

#include <string>
#include <iostream>

#include "DiscBirthDeathProbs.hh"
#include "StdMCMCModel.hh"

namespace beep
{

  /**
   * MCMC model for altering birth and death rates for a discretized tree
   * (see documentation for DiscBirthDeathProbs for more info).
   * 
   * The birth and the death rates are perturbed with equal chance according
   * to a log normal distribution on the suggestion variance.
   */
  class DiscBirthDeathMCMC : public StdMCMCModel
  {

  public:

    /**
     * Constructor.
     * @param prior the prior in the MCMC-chain.
     * @param BDProbs the discretized birth-death probabilities, encapsulating
     * the birth and death rates.
     * @param suggestRatio the suggestion ratio.
     */
    DiscBirthDeathMCMC(MCMCModel& prior, DiscBirthDeathProbs& BDProbs,
		       const Real& suggestRatio);
	
    /**
     * Destructor.
     */
    virtual ~DiscBirthDeathMCMC();

    //! Access to member 
    virtual DiscBirthDeathProbs& getModel()
    {
      return m_BDProbs;
    }

    /**
     * Fixes birth and death rates to their current values.
     */
    void fixRates();

    /**
     * Sets the scale factor for the suggestion variance.
     * @param multiplier a linear scale factor.
     */
    void multiplySuggestionVariance(Real multiplier);

    /**
     * Perturbs the birth and death rates with equal probability.
     * @return the MCMC object associated with the state.
     */
    MCMCObject suggestOwnState();
	
    /**
     * Commits the state. Resets the perturbation status of 
     * the object with BD parameters.
     */
    void commitOwnState();
	
    /**
     * Restores previous birth and death rates.
     */
    void discardOwnState();
	
    /**
     * Returns a string with parameter values in accordance with MCMC model guidelines.
     * @return the current parameter values in a string.
     */
    std::string ownStrRep() const;
	
    /**
     * Returns a string with parameter names in accordance with MCMC model guidelines.
     * @return the parameter names in a string.
     */
    std::string ownHeader() const;
	
    /**
     * No data probability of its own, returns 1.0.
     * @return 1.
     */
    Probability updateDataProbability();
	
    //! Used in parallel implementation of mpiwaladen, when updating
    //! slave chains to perturbations in master chain
    void updateToExternalPerturb(Real newLambda, Real newMu);

    /**
     * Friend helper for printing the object. Uses member method print() for the task.
     * @param o the ostream reference.
     * @param A the object to print.
     * @return the appended ostream reference.
     */
    friend std::ostream& operator<<(std::ostream& o, const DiscBirthDeathMCMC& A);
	
    /**
     * Returns an information string for the object.
     * @return the info string.
     */
    std::string print() const;

  private:

  public:

  private:
	
    /**
     * We have to put a limit on the size of birth-death parameters
     * or our program will crash when (birth_rate - death_rate) * t is too big.
     */
    static const Real MAX_INTENSITY = 10.0;
	
    /** The birth and death rates (including precalculated values). */
    DiscBirthDeathProbs& m_BDProbs;
	
    /** Set to true if the birth and death rate should be kept fixed. */
    bool m_fixRates;

    /**
     * The suggestion function uses a normal distribution around the current
     * value. For the proposal ratios to function right, we need to keep the
     * variance to the distribution constant. This is a problem since we do not
     * know the location/scale of the distribution ahead of time. As a fix, we
     * store a variance initiated from the start values of birth death rates.
     */
    Real m_suggestionVariance;
	
    Real m_oldBirthRate;                                   /**< Cached old value. */
    Real m_oldDeathRate;                                   /**< Cached old value. */
    BeepVector< std::vector<Probability>* > m_oldBD_const; /**< Cached old value. */
    BeepVector<Probability> m_oldBD_zero;                  /**< Cached old value. */
    Probability m_oldPt;                                   /**< Cached old value. */
    Probability m_oldUt;                                   /**< Cached old value. */
  };

}//end namespace beep

#endif /*DISCBIRTHDEATHMCMC_HH*/

