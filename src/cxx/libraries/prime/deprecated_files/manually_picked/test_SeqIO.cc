#include <iostream>

#include "AnError.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"

using namespace std;
using namespace beep;

void 
usage(char *cmd)
{
    cerr << "Usage: "
      	 << cmd
	 << " <seqfile>\n";

    exit(1);
}

char *in1 = "acgtacgt";
char *in2 = "acgtacg";

void
incorporate_data(SequenceData &sd)
{
  char *a = strdup(in1);
  char *b = strdup(in2);
  sd.changeType(SequenceType::getSequenceType("DNA"));
  sd.addData("Lasse", a);
  sd.addData("Miranda", b);
  free(a); free(b);
  a = b = NULL;
}


int
main (int argc, char **argv) {
  SequenceData Axel(myDNA);

  if (argc != 2) {
    usage(argv[0]);
  }

  cout << "Reading as DNA: \n";
  try {
    SequenceData SD = SeqIO::readSequences(argv[1], myDNA);

    cout << "  " << SD << endl;
  } 
  catch (AnError e)
    {
      cout << e.message() << endl;
    }

  cout << "\nReading as codons: \n";
  try {
    SequenceData SD = SeqIO::readSequences(argv[1], myCodon);

    cout << "  " << SD << endl;
  } 
  catch (AnError e)
    {
      cout << e.message() << endl;
    }

  cout << "\nReading as amino acids: \n";
  try {
    SequenceData SD = SeqIO::readSequences(argv[1], myAminoAcid);

    cout << "  " << SD << endl;
  } 
  catch (AnError e)
    {
      cout << e.message() << endl;
    }

  try {
    cout << "Incorporating!\n";
    SequenceData A(myDNA);
    incorporate_data(A);
    cout << A << endl;
  }
  catch (AnError e)
    {
      cout << e.message() << endl;
    }

  try {
    cout << "Copying!\n";
    Axel = SeqIO::readSequences(argv[1], myDNA);
  }
  catch (AnError e)
    {
      cout << e.message() << endl;
    }
}
