#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <sys/types.h>
#include <algorithm>

#include "Beep.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "PRNG.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SimpleML.hh"
//#include "SimpleMCMC.hh"
#include "SubstitutionMCMC.hh"
#include "RandomTreeGenerator.hh"
#include "TreeIO.hh"
#include "TreeMCMC.hh"
#include "UniformDensity.hh"


//-------------------------------------------------------------
//
// This program mimics Bayesian phylogenetics analysis based on
// standard reversible substitution models only, i.e., it
// branch swaps and perturbs edge weights/lengths and any other
// parameters of the chosen substitution model, e.g., alpha/shape
// parameter of the Gamma Rates-Across-Sites model, etc.
// If the tree is fixed, only edge lengths are estimated.
// Some specialties that may need noting:
//  (1) With reversible models, the tree is in effect unrooted
//      The algorithms for substitution likelihood computation
//      do use a rooted tree, but any root gives the same result.
//      The program therefore do not perturb the initial root.
//      This initial root can be set by the user, else the tree
//      is arbitrarily rooted at the first leaf.
//  (2) There are no substitution rates modelled in htis program.
//      However, the EdgeWeightHandler class uses the EdgeRateMCMC
//      class to handle the edge lengths/weights, i.e., the edge
//      rates in EdgeRateMCMC works as a proxy for the edge lengths.
//      This means that a prior model can be given for the lengths.
//      the default is a uniform prior (as in most phylogeny programs)
//  (3) There are no modelling of times or reconciliations whatsoever!
// The requested Proability distribution is
// Pr[G,w,m,v|D] = Pr[D|G,w]Pr[w|G, m,v]Pr[m,v]Pr[G] / Pr[D]
// where Pr[G] and =r[m,v] are from uniform distribution over trees 
// and R^+, resectively, Pr[w|G] is from a model that assumes 
// that edge weights is drawn from a Gamma-distribution with 
// mean m and variance v, and Pr[D|G,w] is from a standard 
// substitution model. The division with 
// Pr[D] = \int_{G,w,m,v} Pr[D|G,w]Pr[w|G, m,v]Pr[m,v]Pr[G]
// is done in the MCMC.
//
//-------------------------------------------------------------




// Global options with default settings
//------------------------------------
int nParams = 2;

char* outfile            = NULL;
unsigned MaxIter         = 10000;
unsigned Thinning        = 100;
unsigned printFactor     = 1;
unsigned RSeed           = 0;
bool quiet               = false;
bool show_debug_info     = false;
bool do_likelihood       = false;
bool do_ML                = false;

char* tree_file          = 0; //NULL
bool fixed_tree          = false;
std::vector<std::string> outgroup;

std::string seqModel     = "JC69";
std::string seqType;
std::vector<double> Pi;
std::vector<double> R;
unsigned n_cats          = 1;
double alpha             = 1.0;
bool estimateAlpha       = true;
// double pinvar             = 0.0;

std::string rateModel    = "CONST";
std::string density      = "UNIFORM";
double mean              = 1.0;
double variance          = 1.0;
bool fixed_rateparams    = false;

// allowed models
std::vector<std::string> subst_models;
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  // Set up allowed models
  //! \todo{ This should be replaced by a factory function that 
  //! includes the checking /bens}
  //-------------------------------------------------------------
  //Substitution model
  subst_models.push_back("UNIFORMAA");
  subst_models.push_back("JTT");
  subst_models.push_back("UNIFORMCODON");
  subst_models.push_back("ARVECODON");
  subst_models.push_back("JC69");
  
  //rate model
  rate_models.push_back("IID");
  rate_models.push_back("GBM");
  rate_models.push_back("CONST");
  
  //rate density
  rate_densities.push_back("UNIFORM");
  rate_densities.push_back("GAMMA");
  rate_densities.push_back("LOGN");
  rate_densities.push_back("INVG");

  if (argc < nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = readOptions(argc, argv);
      
      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)             // Use user-defined random seed
	{
	  rand.setSeed(RSeed);
	}

      // For practical reasons, set up the SubstitutionMatrix already here
      //---------------------------------------------------------
      MatrixTransitionHandler* Q;
      if(seqModel == "USR")
	{
	  Q = new MatrixTransitionHandler(MatrixTransitionHandler::userDefined(seqType, Pi, R)); //So Stupid way of creating an instance!!!
	}
      else
	{
	  Q = new MatrixTransitionHandler(MatrixTransitionHandler::create(seqModel));
	}

      //Get Data
      //---------------------------------------------
      string datafile(argv[opt++]);
      SequenceData D = SeqIO::readSequences(datafile, Q->getType());
      
      // Create end of chain of MCMC-models
      //---------------------------------------------
      DummyMCMC dm;

      // Set up tree model
      //---------------------------------------------
      Tree* T;
      if(tree_file)
	{
	  TreeIO io = TreeIO::fromFile(tree_file);
	  T = new Tree(io.readNewickTree());  
	}
      else
	{
	  T = new 
	    Tree(RandomTreeGenerator::generateRandomTree(D.getAllSequenceNames()));
	}
	  
      BranchSwapping bs;

      //! \todo Add a check that outgroup taxa exists! /bens
      if(outgroup.empty())
	{
	  outgroup.push_back(T->getNode(0)->getName());
	}  
      bs.rootAtOutgroup(*T, outgroup);
      UniformTreeMCMC utm(dm, *T, "T", 1.0);
      utm.fixRoot(); // This means that we in practice work on unrooted trees
      if(fixed_tree)
	{
	  utm.fixTree();
	}

      //Set up mean and variance of substitution rates
      //---------------------------------------------------------
      if(!fixed_rateparams)
	{
	  mean = 0.5;
	  variance = mean * rand.genrand_real1(); 
	}

      // Set up Density function for rates as a proxy for lengths!s
      //---------------------------------------------------------
      Density2P* df = 0;
      if(density == "INVG")
	{
	  df = new InvGaussDensity(mean, variance);
	}
      else if(density == "LOGN")
	{ 
	  df = new LogNormDensity(mean, variance);
	}
      else if(density == "GAMMA")
	{
	  df = new GammaDensity(mean, variance);
	}
      else if(density == "UNIFORM")
	{
	  df = new UniformDensity(0, 2, true);
	  cerr << "*******************************************************\n"
	       << "Note! mean and variance will always be fixed when using\n"
	       << "UniformDensity, the default interval will be (0,10)\n"
	       << "You might want to use the -Ef option\n"
	       << "*******************************************************\n";
	  fixed_rateparams = true;
	}
      else
	{
	  cerr << "Expected 'InvG', 'LogN', 'Gamma' or 'Const' "
	       << "for option -d\n";
	  usage(argv[0]);
	  exit(1);
	}
      // Set up rates = lengths ...
      //---------------------------------------------------------
      EdgeRateMCMC* erm;
      if(rateModel == "GBM")
	{
	  erm = new gbmRateMCMC(utm, *df, *T, "EdgeRates");
	}
      else if(rateModel == "IID")
	{
	  erm = new iidRateMCMC(utm, *df, *T, "EdgeRates");
	}
      else
	{	 
	  erm = new ConstRateMCMC(utm, *df, *T, "EdgeRates");
	}
      
      if(fixed_rateparams) // user has requested that mean and 
	{                  // variance should not be perturbed
	  erm->fixMean();
	  erm->fixVariance();
	}
      erm->generateRates();

      // ... and edge weights 
      //---------------------------------------------------------
      EdgeWeightHandler ewh(*erm); // this equates lengths with rates


      // Set up Site rates
      //---------------------------------------------------------
      UniformDensity uf(0, 3, true); // U[0,3]
      ConstRateMCMC alphaC(*erm, uf, *T, "Alpha"); 
      SiteRateHandler srm(n_cats, alphaC);

      if(n_cats == 1) // then it is meaningless to perturb alpha 
	{
	  alphaC.fixRates();
	}

 
      // ... and SubstitutionMCMC
      //---------------------------------------------------------
      vector<string> partitionList ;  //Will we ever use this?
      partitionList.push_back(string("all"));
      SubstitutionMCMC sm(alphaC, D, *T, srm, *Q, ewh, partitionList);


      // Create MCMC handler
      //---------------------------------------------
      SimpleMCMC* iterator;
      if(do_ML)
	{
	  iterator = new SimpleML(sm, Thinning);
	}
      else
	{
	  iterator = new SimpleMCMC(sm, Thinning);
	}

      if (do_likelihood)  // No MCMC
	{
	  cout << sm.currentStateProb() << endl;
	  exit(0);
	}      

      if (outfile != NULL) //redirect stdout 
	{
	  try 
	    {
	      iterator->setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  


      if (quiet)  // No stderr output
	{
	  iterator->setShowDiagnostics(false);
	}
      
      if (!quiet) // Tell the user we start MCMC and output some info
	{
	  cout << "#Running: ";
	  for(int i = 0; i < argc; i++)
	    {
	      cout << argv[i] << " ";
	    }
	  cout << " in directory"
	       << getenv("PWD")
	       << "\n#\n"
	       << "#Start ";
	  if(do_ML)
	    cout << "ML";
	  else
	    cout << "MCMC";
	  cout << " (Seed = " << RSeed << ")\n";
	}

      if(fixed_tree)
	{
	  cout << "# The tree is fixed so let's print out it out in\n"
	       << "# in PRIME format with ID's to make it easy to relate\n"
	       << "# The edge lengths back to the tree:\n";
	  cout << "# " << TreeIO::writeGuestTree(*T) << endl;;
	}
	  
      // Perform and time the Likelihood calculation
      time_t t0 = time(0);
      clock_t ct0 = clock();
      
      iterator->iterate(MaxIter, printFactor);

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;

      if (!quiet) // output some run statistics
	{
	  cerr << sm.getAcceptanceRatio()
	       << " = acceptance ratio\n";
	}
      if (sm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }

      // Important: finally clean up memory
      delete df;  
      delete erm;
      delete iterator;
    }
  catch(exception& e)       // AnError or an exception has been thrown
    {
      cout <<" error\n"     // tell user about it
	   << e.what();     
    }
}

	      
void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <datafile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <datafile>         a string\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -p <float>            Output to cerr <float> times less often\n"
    << "                         than to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator. If\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -d                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -m                    Do maximum likelihood. No MCMC.\n"
    << "   -S<option>            Options related to Substitution model\n"
    << "     -Sm <'JC69'/'UniformAA'/'JTT'/'UniformCodon'>\n"
    << "                         the substitution model to use, (JC69 is the\n"
    << "                         default). Must fit data type \n"
    << "     -Su <datatype='DNA'/'AminoAcid'/'Codon'> <Pi=float1 float2 ... floatn>\n"
    << "                         <R=float1 float2 ...float(n*(n-1)/2)>\n"
    << "                         the user-defined substitution model to use.\n"
    << "                         The size of pi and R must fit data type (DNA: n=4\n"
    << "                         AminoAcid: n=20, Codon: n = 62), respectively. \n"
    << "                         If both -Su and -Sm is given (don't do this!),\n"
    << "                         only the last given is used\n"
    << "     -Sn <float>         nCats, the number of discrete rate\n"
    << "                         categories (default is one category)\n"
    << "     -Sa <float>         alpha, the shape parameter of the Gamma\n"
    << "                         distribution for site rates (default: 1)\n"
    //     << "     -p <float>         probability of a site being invarant\n"
    << "   -E<option>            Options relating to edge rate model\n"
    << "     -Em <'iid'/'gbm'/'const'> \n"
    << "                         the edge rate model to use (default: const)\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'> \n"
    << "                         the density function to use for edge rates,\n"
    << "                         (Uniform is the default) \n"
    << "     -Ef <float> <float> fixed mean and variance of edge rate model\n"
    << "   -G<option>            Options relating to the tree\n"
    << "     -Gg                 fix tree. If not set, NNI branch.-swapping\n"
    << "                         will be performed on the tree\n"
    << "     -Gi <treefile>      use tree in file <treefile> as start tree\n"
    << "     -Go { <outgroup1> <outgroup2 ... }\n"
    << "                         Root tree at smallest subtree containing\n"
    << "                         leaves given by Go's argument (note spaces\n"
    << "                         after '{'and before '}'\n"; 
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'p':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-p'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  {
	    quiet = true;
	    break;
	  }
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'm':
	  {
	    do_ML = true;
	    break;
	  }	   
	case 'S':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = argv[++opt];
		      capitalize(seqModel);
		      if(find(subst_models.begin(), subst_models.end(), seqModel) 
			 == subst_models.end())
			{
			  cerr << "Expected 'UniformAA', 'JTT', 'UniformCodon' "
			       << "or 'JC69' for option -Sm\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'u':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = "USR";
		      seqType = argv[++opt];
		      capitalize(seqType);
		      int dim = 0;
		      if(seqType == "DNA")
			dim = 4;
		      else if(seqType == "AMINOACID")
			dim = 20;
		      else if(seqType == "CODON")
			dim = 61;
		      else
			{
			  cerr << seqType
			       << " is not a valid data type for option -Su\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		      if(opt + (dim * (dim + 1) / 2) < argc)
			{
			  for(int i = 0; i < dim; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for Pi: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}
				    
			      Pi.push_back(atof(argv[opt]));
			    }
			  for(int i= 0; i < dim * (dim - 1) /2; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for R: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}

			      R.push_back(atof(argv[opt]));
			    }
			}
		      else
			{
			  cerr << "Too few parameters to -Su " 
			       << seqType
			       << "\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc) 
		    {
		      n_cats = atoi(argv[opt]);
		    }
		  else
		    {
		      std::cerr << "Expected int (number of site rate\n"
				<< "classes) for option 'n'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      alpha = atof(argv[opt]);
		      estimateAlpha = false;
		    }
		  else
		    {
		      cerr << "Expected float (shape parameter for site rate\n"
			   << "variation) for option '-a'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	  // 	    case 'p':
	  // 	      	{
	  // 		  if (++opt < argc) 
	  // 		    {
	  // 		      pinv = atof(argv[opt]);
	  // 		    }
	  // 		  else
	  // 		    {
	  // 		      cerr << "Expected float (probability of a site being\n"
	  // 			   << "invariant) for option '-p'!\n";
	  // 		      usage(argv[0]);
	  // 		    }
	  // 		  break;
	  // 		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); //Check for correct error code
		}
		break;
	      }
	    break;
	  }	
	case 'E':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      capitalize(rateModel);
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Model "
			       << argv[opt] 
			       << "does not exist\n" 
			       << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		      capitalize(density);
		    }
		  else
		    {
		      cerr << "Expected density after option '-d'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'f':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  variance = atof(argv[opt]);
			  fixed_rateparams = true;
			}
		      else
			{
			  cerr << "Expected float (variance for "
			       << "edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'G':
	  {
	    switch(argv[opt][2])
	      {
	      case 'g':
		{
		  fixed_tree = true;
		  break;
		}
	      case 'i':
		{
		  if (opt + 1 < argc)
		    {		  
		      tree_file = argv[++opt];
		      break;
		    }
		  else
		    {
		      cerr << "Expected file name (string) after -Gi\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		}
	      case 'o':
		{
		  if (string(argv[++opt])  == "{")
		    {
		      opt++;
		      while(string(argv[opt]) != "}")
			{
			  outgroup.push_back(argv[opt++]);
			}
		      if(outgroup.empty())
			{
			  cerr << "Set of outgroup names expected after "
			       << "option -Go\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected outgroup names as strings after -Go\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  
