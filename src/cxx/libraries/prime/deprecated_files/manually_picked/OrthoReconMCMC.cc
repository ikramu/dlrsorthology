#include "OrthoReconMCMC.hh"
#include "TreeIO.hh"

using namespace std;

//-------------------------------------------------------------
//
// Construct/Destruct/Assign
//
//-------------------------------------------------------------
OrthoReconMCMC::OrthoReconMCMC(ReconciliationApproximator& rsa,
			       ReconciliationSampler& rs,
			       DupSpecProbs& kendall,
			       Tree& S,
			       const Real& beta,
			       Tree& G,
			       PRNG &R)
  : MCMCModel(R),
    integrator(rsa),
    sampler(rs), 
    nee(kendall), 
    S(S),
    beta(beta),
    G(G),
    old_G(G),
    GDependentObjectsAreInvalid(true),
    paramIdx(0),	
    rates_are_fixed(false),
    gene_tree_fixed(true)
{
  //
  // If badly chosen rates, then go ahead and compute a sensible starting point
  //
  nee.getRates(birthRate, deathRate);
  if (birthRate <= 0.0 || deathRate <= 0.0)
    {
      sampler.chooseStartingRates(birthRate, deathRate);
    }
  Real height = S.rootToLeafTime();
  if (height * birthRate > MAX_INTENSITY 
      || height * deathRate > MAX_INTENSITY)
    {
      sampler.chooseStartingRates(birthRate, deathRate);
    }

  //
  // Starting point for species tree root time
  //
  Node& sr = *S.getRootNode();
  currentRootTime = sr.getLeftChild()->getTime(); // Most now have = 0! * getPRNG().genrand_real2();
  sr.setTime(currentRootTime);

}

OrthoReconMCMC::~OrthoReconMCMC()
{

}



//-------------------------------------------------------------
//
// Interface
//
//-------------------------------------------------------------
Probability
OrthoReconMCMC::suggestNewState()
{

  Probability prior_root_time(1.0);
  // Choose what parameter to perturb using 'param':
  paramIdx = iterationNumber() % 3;
//   if (gene_tree_fixed)
//     {
//       paramIdx = paramIdx % 4;	// Avoid tree changing states.
//     }
//   if (rates_are_fixed && paramIdx < 2)
//     {
//       paramIdx += 2;
//     }
  
  switch (paramIdx) 
    {
    case 0:			// Duplication  rate
      {
	if (GDependentObjectsAreInvalid)
	  {
	    integrator.update();
	    GDependentObjectsAreInvalid == false;
	  }
	// Generate new birth rate by changing -20 % to +25 %
	Real new_rate;
	do {
	  new_rate = birthRate * rateChange();
	} while (new_rate * S.rootToLeafTime() > MAX_INTENSITY);
	nee.setRates(new_rate, deathRate);
	break;
      }
    case 1:			// Loss rate
      {
	if (GDependentObjectsAreInvalid)
	  {
	  integrator.update();
	  GDependentObjectsAreInvalid == false;
	  }
	// Generate new death rate
	Real new_rate;
	do {
	  new_rate = deathRate * rateChange();
	} while (new_rate * S.rootToLeafTime() > MAX_INTENSITY);
	nee.setRates(birthRate, new_rate);
	break;
      }
    case 2:			// Species tree root time
      {
	if (GDependentObjectsAreInvalid)
	  {
	    integrator.update();
	    GDependentObjectsAreInvalid == false;
	  }
      // Generate new root Time
	Node& sr = *S.getRootNode();
	Real new_time;
	do {
	  new_time = sr.getTime() * rateChange();
	} while (new_time > S.rootToLeafTime());
	sr.setTime(new_time);
	break;
      }
//     case 3:			// Average substitution rate
//       {
// 	if (GDependentObjectsAreInvalid)
// 	  {
// 	    integrator.update();
// 	    GDependentObjectsAreInvalid == false;
// 	  }
// 	substRate *= rateChange();
// 	break;
//       }
//     case 4:
//       {
// #ifdef RECOMB
// 	mrGardener.rootSafeNNI(G);
// #else
// 	mrGardener.reRoot(G);	// re-balance (or unbalance) the tree
// #endif
// 	integrator.update();
// 	GDependentObjectsAreInvalid == false;
// 	break;
//       }
    default:	   	// Not 0 or 1, as that handles the birth/death params
      {
// #ifdef RECOMB
// 	mrGardener.rootSafeNNI(G);	// Perturb the gene tree
// #else
// 	mrGardener.NNI(G);	// Perturb the gene tree
// #endif
// 	GDependentObjectsAreInvalid == false;
	
// 	// Try a new ReconciliationSampler for the new tree. This tree
// 	// will be used when we perturb the rates too.
// 	integrator.update(); //updates G for everything below incl. sampler
      }
    }
      
  // Compute the prior probability of the gene tree / parameters here:
  //------------------------------------------------------------------
  Probability prior = calcRootTimeProb();
  
  //
  // Then approximate the likelihood:
  //------------------------------------------------------------------
  Probability likelihood = integrator.calculateDataProbability();

  //
  // Check for reasonable answers:
  //------------------------------------------------------------------
  if (prior == 0.0) 
    {
      WARNING1("A prior of 0.0 was encountered. Please check your parameters!");
    }
  if (likelihood == 0.0) 
    {
      WARNING1("A likelihood of 0.0 was encountered. Please check your parameters!");
    }

  return prior * likelihood;
}


Probability
OrthoReconMCMC::currentStateProb()
{
  if (GDependentObjectsAreInvalid)
    {
      integrator.update();
    }
  Probability prior = calcRootTimeProb();
  Probability likelihood = integrator.calculateDataProbability();

  return prior * likelihood;
}

void 
OrthoReconMCMC::commitNewState()
{
  // Tell the statistics support in superclass what happened:
  registerCommit();

  //
  // Accept suggested parameters (as tested in nee),
  // as the current state.
  //
  switch (paramIdx)
    {
    case 0:
    case 1:
      nee.getRates(birthRate, deathRate);
      break;

    case 2:
      currentRootTime = S.getRootNode()->getTime();
      break;

//     case 3:
//       currentSubstRate = substRate;
//       break;

//     default:
//       old_G = G;		// We accepted the current tree:
// 				// Keep a copy for the next perturbation
    }
}

void
OrthoReconMCMC::discardNewState()
{
  registerDiscard();
  switch (paramIdx)
    {
    case 0:
    case 1:
      nee.setRates(birthRate, deathRate);
      break;

    case 2:
      {
	Node& sr = *S.getRootNode();
	sr.setTime(currentRootTime);
	break;
      }

//     case 3:
//       substRate = currentSubstRate;
//       break;

//     default:
//       G = old_G;
//       GDependentObjectsAreInvalid = true; // Mark updates as necessary
    }
}


string
OrthoReconMCMC::strRepresentation()
{
  std::ostringstream birth_str, death_str, rootTime_str;
  
  birth_str << birthRate;
  death_str << deathRate;
  rootTime_str << S.getRootNode()->getTime();
  string s;
  s += birth_str.str() + "; ";
  s += death_str.str() + "; ";
  s += rootTime_str.str() + "; ";
  s += rootTime_str.str() + "; ";
  s += TreeIO::newickString(G.getRootNode()) + "; ";
  s += integrator.strOrthology() + ";";
  return s;

}


void
OrthoReconMCMC::setFixedRates(bool ratesAreFixed)
{
  rates_are_fixed = ratesAreFixed;
}


void
OrthoReconMCMC::setFixedGeneTree(bool geneTreeFixed)
{
  gene_tree_fixed = geneTreeFixed;
}


//
// Utility function for changing birth/death rates.
// Returns a number in the interval [0.8, 1.25].
//
Real
OrthoReconMCMC::rateChange()
{
  double r = getPRNG().genrand_real2(); 

  if(r < 5/9)			// Set rate in [0.8, 1.0)
    {
      r = r * 9/5;		// From [0, 5/9) to [0, 1.0)
      return  0.8 + 0.2 * r;  // [0.8, 1.0)
    }
  else			// Set rate in (1.0, 1.25]
    {
      r = r * 9/4;		// From [5/9, 1.0) to [0, 1.0)
      return 1.25 - 0.25 * r; // (1.0, 1.25]
    }  
}

Probability
OrthoReconMCMC::calcRootTimeProb()
{
  return 1/beta * exp(- S.getRootNode()->getTime()/beta);
}
