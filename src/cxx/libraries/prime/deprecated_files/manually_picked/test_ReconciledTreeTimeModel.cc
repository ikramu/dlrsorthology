#include "ReconciledTreeTimeModel.hh"

#include "AnError.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "EnumerateReconciliationModel.hh"
#include "ReconciliationTimeModel.hh"
#include "ReconciliationTimeSampler.hh"
#include "StrStrMap.hh"
#include "Tree.hh"
#include "TreeIO.hh"

// Global options with default settings
//------------------------------------
int nParams = 5;

bool fixed_tree          = true;
std::vector<std::string> outgroup;

std::map<unsigned, beep::Real> start_times;
bool fixed_bdrates = false;
double birthRate = -1.0;
double deathRate = -1.0;

using namespace beep;
using namespace std;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);

int
main(int argc, char **argv) 
{  
  using namespace beep;
  using namespace std;

  if (argc <nParams) 
    {
      cerr << "usage: test_ReconciliedtreetimeModel <Guesttree> <HostTree> <gs> <birthrate> <deathrate>\n";
      exit(1);
    }
  try
    { 
      //Get tree and Data
      //---------------------------------------------
      string G_filename(argv[1]);
      TreeIO io = TreeIO::fromFile(G_filename);
      StrStrMap gs;
      Tree G = io.readGuestTree(0,&gs);

      string S_filename(argv[2]);
      io.setSourceFile(S_filename);
      Tree S = io.readHostTree();
      S.setName("S");

      if(gs.size() == 0)
	{
	  gs = TreeIO::readGeneSpeciesInfo(argv[3]);
	}
      
      birthRate = atof(argv[4]);
      deathRate = atof(argv[5]);

      cerr << G << S << gs << birthRate << endl << deathRate << endl;
      
      BirthDeathProbs bdm(S, birthRate, deathRate);

      ReconciledTreeTimeModel tm(G,gs,bdm);
      
      EnumerateLabeledReconciliationModel erm(G,gs,bdm);

      for(unsigned i = 0; i < erm.getNumberOfReconciliations(); i++)
	{
	  cout << "Reconciliation " << i << endl;
	  erm.setGamma(i);
 	  ReconciliationTimeSampler rts(erm); // using info given in rs

 	  Probability q = rts.sampleTimes();
	  tm.setGamma(erm.getGamma());
	  ReconciliationTimeModel a(erm);
	  q = a.calculateDataProbability();
	  cerr << "Rtm_prob: " << q.val() << endl;
	  q *= erm.calculateDataProbability();
	  cerr << "EnumLrm_prob: " << (erm.calculateDataProbability()).val() << endl;
	  Probability p = tm.calculateDataProbability();
	  cerr << "Rttm_prob: " << q.val() << endl;
	  cout << "p = " << p.val() << "    q = " << q.val()  << endl;
	  cout << "p-q = " << (p-q).val() << "  p/q = " << (p/q).val() << endl;
	}
    }
  catch(AnError e)
    {
      cerr <<" error\n";
      e.action(); 
    }
}

