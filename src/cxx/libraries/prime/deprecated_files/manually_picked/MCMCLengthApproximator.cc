#include "MCMCLengthApproximator.hh"

#include "AnError.hh"
#include "MCMCObject.hh"

#include <sstream>

namespace beep
{
  using namespace std;
  //---------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //---------------------------------------------------------  
  MCMCLengthApproximator::
  MCMCLengthApproximator(ReconciledTreeTimeMCMC& prior, Density2P& rateProb_in, 
			 Tree& T_in)
    : LengthRateModel(rateProb_in, T_in),
      model(&prior),
      nIter(1000),
      thin(1),
      burnin(100),
      doML(false)
  {
  }

  MCMCLengthApproximator::~MCMCLengthApproximator()
  {}

  MCMCLengthApproximator::
  MCMCLengthApproximator(const MCMCLengthApproximator& M)
    : LengthRateModel(M),
      model(M.model),
      nIter(M.nIter),
      thin(M.thin),
      burnin(M.burnin),
      doML(M.doML)
  {}

  MCMCLengthApproximator& 
  MCMCLengthApproximator::operator=(const MCMCLengthApproximator& M)
  {
    if(& M != this)
      {
	LengthRateModel::operator=(M);
	model = M.model;
	nIter = M.nIter;
	thin = M.thin;
	doML = M.doML;
	burnin = M.burnin;
      }
    return *this;
  }

  //---------------------------------------------------------
  //
  // Interface
  //
  //--------------------------------------------------------- 
  void
  MCMCLengthApproximator::setMCMCparams(unsigned iterations, 
					unsigned thinning, 
					unsigned burn_in,
					bool useML)
  {
    assert(iterations >= burn_in);
    assert(iterations >= thin);

    thin = thinning;
    nIter = iterations;
    burnin = burn_in;
    doML = useML;
  }

  //-------------------------------------------------------------------
  // ProbabilityModel interface
  //-------------------------------------------------------------------
  Probability 
  MCMCLengthApproximator::calculateDataProbability()
  {
    if(doML)
      {
	return calcML();
      }
    else
      {
	return calcMCMC();
      }
  }
  
  void 
  MCMCLengthApproximator::update()
  {
    model->update();
    model->sampleTimes();
    model->initStateProb();
  }
  
  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const MCMCLengthApproximator& A)
  {
    return o << A.print();
  }

  string 
  MCMCLengthApproximator::print() const
  {
    ostringstream oss;
    oss 
      << "The probability is approximated by "
      << nIter;
	 if(doML)
	   oss << "ml";
	 else
	   oss << "MCMC";
    return oss.str();
  }
  
  //----------------------------------------------------------------------
  // 
  // Implementation
  // 
  //----------------------------------------------------------------------
  Probability
  MCMCLengthApproximator::calcMCMC()
  {
    unsigned check_number_nonupdate = 0;
    unsigned iteration = 0;    
    Probability p = model->initStateProb();
    LengthRateModel::update();
    p *= LengthRateModel::calculateDataProbability();
    Probability sumP = 0;
    unsigned N_stored = 0;
    for (unsigned i = 0; i < nIter; i++) 
      {
	try 
	  {
	    MCMCObject proposal = model->suggestNewState();
	    LengthRateModel::update();
	    proposal.stateProb *= LengthRateModel::calculateDataProbability();
 	    Probability alpha = proposal.stateProb * proposal.propRatio / p;
	    
	    if(alpha >= 1.0 || Probability(R.genrand_real1()) <= alpha)
	      {
		model->commitNewState();
		// The following line should be deleted:
		check_number_nonupdate = 0;
		p = proposal.stateProb;
	      }
	    else
	      {
		// If nothing has happend we wish to output the discarded 
		// states for diagnostic reasons. This if-statement should 
		// be deleted later:
		if (check_number_nonupdate >= 20 * thin)
// 		  &&show_diagnostics)
		  {
		    cerr << "Warning! The MCMC chain seems stuck. "
			 << "No state change in "
			 << check_number_nonupdate
			 << " iterations.\n";
		    check_number_nonupdate = 0;
		  }
		// The following line should be deleted:
		check_number_nonupdate++;
		model->discardNewState();
		// Note that since we are discarding we do NOT update output
		// but leave it as in last iteration
	      }
	  }
	catch (AnError& e)
	  {

	    cerr << "MCMCLengthApproximator::calcMMCMC\nAt iteration " 
		 << i 
		 << ".\nState is "
		 << model->strRepresentation()
		 << endl;
	    e.action();
	  }

	if (iteration > burnin && iteration % thin == 0)
	  {
	    sumP += p;
	    N_stored++;
	  }
	iteration = iteration + 1;
      }
    
    return sumP / N_stored;
  }

  Probability
  MCMCLengthApproximator::calcML()
  {
    unsigned no_update = 0;
    unsigned iteration = 0;    
    Probability p = 0;

    while(iteration < nIter && no_update < 100) //10000)
      {
	try 
	  {
	    MCMCObject proposal = model->suggestNewState();
	    LengthRateModel::update();
	    proposal.stateProb *= LengthRateModel::calculateDataProbability(); 
 	    Probability alpha = proposal.stateProb;	      
	    
	    if(alpha > p)
	      {
		model->commitNewState();		
                p = proposal.stateProb;
		no_update = 0;
	      }
	    else
	      {
		model->discardNewState();
		no_update++;
	      }     	    
	  }
	catch (AnError& e)
	  {
	    cerr << "MCMCLengthApproximator::calcML\nAt iteration " 
		 << iteration 
		 << ".\nState is "
		 << model->strRepresentation()
		 << endl;
	    e.action();
	  }
	
	iteration = iteration + 1;
      }
    return p;
  }



} // end namspace beep
  
