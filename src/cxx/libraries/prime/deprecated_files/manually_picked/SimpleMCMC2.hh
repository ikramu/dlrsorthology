#ifndef SIMPLEMCMC2_HH
#define SIMPLEMCMC2_HH

#include "MCMCModel.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "AbstractMCMC.hh"

//-------------------------------------------------------------
//
// A very simple version of the Metropolis algorithm.
//
//! Author: Lars Arvestad, SBC, � the MCMC-club, SBC, all rights reserved
//
//-------------------------------------------------------------

namespace beep
{
  class SimpleMCMC2 : public AbstractMCMC
  {
  public:

    SimpleMCMC2(MCMCModel &M)  ;
    virtual    ~SimpleMCMC2();
    virtual bool iterate( IterationObserver & iterObs, unsigned int * iterations_done );
  protected:
    MCMCModel &model;
    PRNG &R;		       // Source of randomness
    Probability p;	       // Probability of the model's current state
  };

}//end namespace beep
#endif
