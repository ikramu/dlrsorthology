#include <cmath>
#include <sstream>
#include <cassert>
#include <gsl/gsl_randist.h>
#include <gsl/gsl_cdf.h>

#include "AnError.hh"
#include "Beep.hh"
#include "Probability.hh"

int main()
{
  using namespace std;
  using namespace beep;
  cout.precision(10);

  for(float e = 0; e < 2; e += 1)
    {
      float p = 0.9999982119;
      
      for(int n = 0; n <= 10; n++)
	{
	  cout << endl << "p = " << p << "n = " << n << " p^n = " << pow((1-p),n) << endl;

	  for(int k = 0; k <= n; k++)
	    {
	      Real P = 0;
	      for(int k2 = 0; k2 <= k; k2++)
		{
		  for(int m = 0; m <= k2; m++)
		    {
		      P += probBinom(n,m).val() * std::pow(p,m) * std::pow(1.0-p, n-m);
		    }
		}

	      cout << p << "\t"
		   << k << "\t"
		   << P<< "\t";
	      if(k == 0)
		{
		  cout << pow(1.0-p,n) << "\t";
		}
	      else
		{
		  cout << gsl_cdf_binomial_P(k,p,n) * static_cast<float>(k+1)
		    - n * p* gsl_cdf_binomial_P(k-1,p,n-1) << "\t";
		}
	      if(k == n)
		{
		  cout << pow(p,n) << "\t";
		}
	      else
		{
		  cout << "\t" << static_cast<double>(k+1) -
		    (gsl_cdf_binomial_Q(k,p,n) * static_cast<double>(k+1)
		    - n * (1.0-p) * gsl_cdf_binomial_Q(k-1,p,n-1)) << "\t";
		}
	      cout   << endl;
	  
	    }
	  cout << endl;
	}
      cout << endl;
    }
  cout << endl;
  float  p = 0.9804959032;
  unsigned n = 1;
  unsigned k = 0;
  double  B = gsl_cdf_binomial_P(k, p, n);
  double  b = gsl_cdf_binomial_P(k-1, p, n-1);
  
  double ret = B * (static_cast<double>(k+1)) - n * p * b;
  cout.precision(10);
  cout << "HybridHostTreeModel::computeB(" 
       << p << ", " << n << ", " << k << ") = " << ret << "\n";

   B = gsl_cdf_binomial_Q(k, 1.0 -p, n);
   b = gsl_cdf_binomial_Q(k-1, 1.0 -p, n-1);
  
   ret = B * (static_cast<float>(k+1)) - n * p * b;
  cout.precision(10);
  cout << "HybridHostTreeModel::computeB(" 
       << p << ", " << n << ", " << k << ") = " << 1-ret << "\n";
}
