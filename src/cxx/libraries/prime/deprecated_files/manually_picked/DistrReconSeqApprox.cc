#include "DistrReconSeqApprox.hh"

#include <iostream>
#include <string>
#include <vector>

#include "AnError.hh"
#include "PvmDeadClientError.hh"
#include "GammaMap.hh"
#include "SeqProbApproximator.hh"

#ifdef DISTR_DEBUG
#define SHOWMSG(A) {cerr << "DistrReconSeqApprox: " << A << endl; }
#else
#define SHOWMSG(A)
#endif





namespace beep 
{
  using namespace std;
  
  //-------------------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //-------------------------------------------------------------------
  DistrReconSeqApproximator::DistrReconSeqApproximator(MCMCModel& prior,
						       const string& clientProgram,
						       unsigned n_clients,
						       BirthDeathProbs& bdp,
						       ConstRateModel& molClock,
						       SequenceData& F,
						       const string& modelname,
						       Tree& G_in,
						       StrStrMap& gs,
						       const unsigned n_samples)
    : StdMCMCModel(prior, 0),
      master(clientProgram, n_clients),
      n_dead_clients(0),
      kendall(bdp),
      G(&G_in),
      molClock(molClock),
      n_samples(n_samples),
      orthology(*G)
  {
    // I think the asserts in spa against gamma will suffice here
    if (n_samples > MAX_N_SAMPLES)
      {
	throw AnError("You have requested way too many reconciliation samples!");
      }

    //

#ifdef DISTR_DEBUG
    master.debug_msg("Master!");
#endif
    SHOWMSG("First multicast");
    if (n_clients == 0) 
      {
	n_clients = master.numHosts();
      }
    bsize = n_samples / (n_clients * PvmBeep::BatchReductionFactor);
    if (bsize < 1) 
      {
	bsize = 1;
      }
  
    master.createMessage(bsize);
    master.multiCast(PvmBeep::BatchSize);
    SHOWMSG("Done with firts multicast");

    SHOWMSG("Multicasting modelname: " << modelname);
    master.createMessage(modelname);
    master.multiCast(PvmBeep::String);

    SHOWMSG("Multicasting GS");
    master.createMessage(gs);
    master.multiCast(PvmBeep::G2S);

    SHOWMSG("Multicasting S");
    master.createMessage(kendall.getStree());
    master.multiCast(PvmBeep::SpeciesTree);

    SHOWMSG("Multicasting G");
    master.createMessage(*G);
    master.multiCast(PvmBeep::GeneTree);

    SHOWMSG("Multicasting subst rate: " << molClock.getRateParam());
    master.createMessage(molClock.getRateParam());
    master.multiCast(PvmBeep::SubstRate);


    SHOWMSG("Multicasting sequence data: " << F);
    master.createMessage(F);
    master.multiCast(PvmBeep::SequenceData);

    SHOWMSG("Done in constructor.");
  }



  // Copy constuctor
  DistrReconSeqApproximator::DistrReconSeqApproximator(const DistrReconSeqApproximator &drsa)
    :  StdMCMCModel(drsa),
       master(drsa.master),
       n_dead_clients(drsa.n_dead_clients),
       kendall(drsa.kendall),
       G(drsa.G),
       molClock(drsa.molClock),
       n_samples(drsa.n_samples),
       current_batch(drsa.current_batch),
       orthology(drsa.orthology)
  {
  }

  //
  // Death and destruction
  //
  DistrReconSeqApproximator::~DistrReconSeqApproximator()
  {
    // Some final notes:
    master.writeStatistics();

    // Don't worry about PVM, the destructor for M is called here,
    // hence pvm_exit is called and all PVM clients are shut down properly.
  }



  // Assignment
  /*
    DistrReconSeqApproximator &
    DistrReconSeqApproximator::operator=(const DistrReconSeqApproximator &rsa)
    {
    if (this != &rsa)
    {
    StdMCMCModel::operator=(rsa); 
    G = rsa.G;
    n_samples = rsa.n_samples;
    master = rsa.master;
    orthology = rsa.orthology;
    }

    return *this;
    }
  */

  //-------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------ 

  //-------------------------------------------------------------------
  // StdMCMCModel interface
  //------------------------------------------------------------------------ 
  Probability
  DistrReconSeqApproximator::updateDataProbability()
  {
    update();
    return calculateDataProbability();
  }

  MCMCObject
  DistrReconSeqApproximator::suggestOwnState()
  {
    throw AnError("We shouldn't got here, right?");
    return MCMCObject(1.0, 1.0);
  }

  void
  DistrReconSeqApproximator::commitOwnState()
  {
  }

  void
  DistrReconSeqApproximator::discardOwnState()
  {
  }

  std::string
  DistrReconSeqApproximator::ownStrRep() const
  {
    if (estimateOrthology)
      {
	return strOrthology() + "; ";
      }
    else
      {
	return "";
      }
  }

  std::string
  DistrReconSeqApproximator::ownHeader() const
  {
    if (estimateOrthology)
      {
	return "ortho(orthologypairs); ";
      }
    else
      {
	return "";
      }
  }

  //-------------------------------------------------------------------
  // ProbabilityModel interface
  //------------------------------------------------------------------------ 
  Probability
  DistrReconSeqApproximator::calculateDataProbability()
  {
    Probability sum(0.0);
    numDataPoints = 0;	// Reset the counter

    // Clients should have received data with an update().
    // Can now ask them to compute.
    SHOWMSG("Multicasting 'PleaseStartWorking'");
    master.multiCast(PvmBeep::PleaseStartWorking);
    orthology.reset();

    SHOWMSG("Entering while loop");
    while (numDataPoints < n_samples) 
      {
      
      try {
	Probability term;
	if (receiveOneBatch(term)) 
	  {
	    sum += term;
	  }
	else 
	  {
	    SHOWMSG("Dropping old data");
	  }

// 	Probability term = receiveOneBatch();
// 	sum += term;
      }
      catch (PvmDeadClientError &pe)
	{
	  handleDeadClient(pe);
	}
      catch (AnError e)
	{
	  e.action();
	}

      }

    SHOWMSG("Sum = " << sum << "\nOM = " << orthology.strRepresentation());
    if (estimateOrthology)
      {
	if (sum > 0.0) 
	  {
	    orthology.scale(Probability(1.0) / sum);
	  } 
	else 
	  {
	    WARNING1("No orthology probability!");
	  }
      }

    return sum / numDataPoints;
  }

  //! Book keeping for one batch of results
  //! 
  bool
  DistrReconSeqApproximator::receiveOneBatch(Probability &p)
  {
    int i, client_batch;
    OrthologyMatrix OM(*G);
    
    SHOWMSG("#nodes: " << G->getNumberOfNodes() << ", now waiting");
    master.waitForMessage();

    master.receive(client_batch);	// Batch number
    SHOWMSG("Batch id: " << client_batch 
	    << "\tTask ID: " << master.messageSender());
      
    master.receive(i);	// Number of datapoints (or terms) in batch
    SHOWMSG("Receiving " << i << " datapoints" << "\t(" 
	    << (100 * numDataPoints) / n_samples << "%)");

    master.receive(p);	// Sum of likelihoods
    SHOWMSG("p = " << p);

    master.receive(OM);	// Orthology predictions
    //     SHOWMSG("OM.nrows = " << OM.nrows);

    int waiting = 0;
    master.receive(waiting); // Number  of seconds that the client has been waiting
    master.registerWaitTime(master.messageSender(), waiting);
    master.registerCompletion(master.messageSender());


    // Make sure that the client's data is for current parameters.
    // Otherwise we just drop it; The client will pick up the multicasted
    // request with other params, and start computing on that instead.
    if (client_batch == current_batch)
      {
	numDataPoints += i;

	if (estimateOrthology)
	  {
	    orthology += OM;
	  }
	if (numDataPoints < n_samples) { // One more is probably needed
	  master.createMessage();
	  master.send(master.messageSender(), PvmBeep::PleaseStartWorking);
	} else {
	  master.createMessage();
	  master.multiCast(PvmBeep::YouAreDone);
	}
	return true;		// This is valid data we will use!
      }
    
    return false;		// This was old uninteresting data!
  }

  void
  DistrReconSeqApproximator::handleDeadClient(PvmDeadClientError &pe)
  {
    n_dead_clients++;
    pe.action();
    cerr << n_dead_clients << " dead clients, " << master.getNumClients() - n_dead_clients<< " remaining." << endl;
    if (n_dead_clients >= master.getNumClients()) 
      {
	throw AnError("No more clients available: All have mysteriosly died.", 1);
      }
    else 
      {
	cerr << pe.message()
	     << " Did the client crash?" << endl;
      }
  }


  // For when G or other params has changed
  //------------------------------------------------------------
  void
  DistrReconSeqApproximator::update()
  {
    Real birth, death;
    kendall.getRates(birth, death);

    current_batch++;

    master.createMessage(current_batch);
    master.multiCast(PvmBeep::BatchNumber);

    master.createMessage(*G);
    master.multiCast(PvmBeep::GeneTree);

    master.createMessage(kendall.getStree());
    master.multiCast(PvmBeep::SpeciesTree);

    master.createMessage(birth);
    master.multiCast(PvmBeep::BirthRate);

    master.createMessage(death);
    master.multiCast(PvmBeep::DeathRate);

    Real x = molClock.getRateParam();
    master.createMessage(x);
    master.multiCast(PvmBeep::SubstRate);
  }


  // Return a string representation of the orthology matrix.
  // Two version available:
  //-------------------------------------------------------------------------
#ifdef MATLAB_ORTHOLOGS
  // Using Matlab/Octave format, but with truncated rows. For instance, 
  // instead of 
  //    [[ 0 0 0]; [0.5 0 0]; [1 0 0]]
  // we write
  //    [[ 0]; [0.5 0]; [1 0 0]]
  // which is everything up to and including the diagonal element and 
  // nothing more.
  string
  DistrReconSeqApproximator::strOrthology() const
  {
    int size = orthology.nrows();
    string s;
    s.reserve(size * (size + 1) * 5); // Allocate some space

    s = "[";
    for (int row=0; row < size; row++)
      {
	s += "[";
	for (int col=0; col < row; col++) 
	  {
	    ostringstream buf;
	    buf << orthology(row,col).val() << " ";
	    s += buf.str();
	  }
	s += " 0];";
      }
    s += "]";
    return s;
  }
#else
  // A list of pairs. Format: 
  // orthology := '[' + ortholist + ']'
  // ortholist := empty
  //            | orthopair + ' ' + ortholist
  // orthopair := '[' + id1 + ',' + id2 + '] = ' + probability

  string
  DistrReconSeqApproximator::strOrthology() const
  {
    int size = orthology.nrows();
    string s;
    s.reserve(size * (size + 1) * 5); // Allocate some space

    s = "[";
    for (int row=0; row < size; row++)
      {
	Node *v1 = G->getNode(row);
	for (int col=0; col < row; col++) 
	  {
	    //	  cerr << "O(" << row<<", "<<col<<")=" << orthology(row,col)<<endl;
	    if (orthology(row, col) > 0.0)
	      {
		Node *v2 = G->getNode(col);
		ostringstream buf;
		buf << orthology(row,col).val() << " ";
		s += '[' 
		  +  v1->getName() 
		  +  ',' 
		  +  v2->getName()
		  +  "]="
		  +  buf.str();
	      }
	  }
      }
    s += "]";
    return s;
  }

#endif

  void
  DistrReconSeqApproximator::outputOrthology(bool val)
  {
    estimateOrthology = val;
  }

  // void
  // DistrReconSeqApproximator::resetOrthology()
  // {
  //   // Reset the orthology matrix. We only use the lower half
  //   for (int i = 0; i < orthology.nrows(); i++) {
  //     for (int j = 0; j < i; j++) {
  //       orthology(i, j) = 0.0;
  //     }
  //   }
  // }



  // void
  // DistrReconSeqApproximator::recordOrthology(const GammaMap &gamma, const Probability p)
  // {
  //   multimap<int, int> ortho_list = gamma.getOrthology();
  //   for(multimap<int,int>::iterator iter = ortho_list.begin();
  //       iter != ortho_list.end();
  //       iter++) 
  //     {
  //       if ((*iter).first > (*iter).second) // Remember: Only lower half
  // 	{
  // 	  orthology((*iter).first, (*iter).second) += p;
  // 	}
  //       else
  // 	{
  // 	  orthology((*iter).second, (*iter).first) += p;
  // 	}
  //     }
  // }



  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  ostream& 
  beep::operator<<(ostream &o, const DistrReconSeqApproximator& A)
  {
    return o << A.print(); 
  }

  std::string 
  DistrReconSeqApproximator::print() const
  {
    std::ostringstream oss;
    oss 
      << "Distributed computing of "
      << "reconciliation likelihood is calculated using birth-death model\n"
      << "and is approximated by "
      << n_samples
      << " Monte Carlo iterations.\n"
      << master.getNumClients() << " clients were available, batch size is "
      << bsize << ".\n"
      << "Likelihood calculations is performed by the following models:\n"
      << indentString("An EdgeRateModel (see prior's output)\n")
      << indentString("An BirthDeathProbs (see prior's output)\n")
      << indentString("and the program ClientReconSeqApprox\n")
      << StdMCMCModel::print()
      ;
    if(estimateOrthology)
      oss << "Orthology is estimated during analysis.\n";
    return oss.str();
  }

}
