#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>

#include "BirthDeathMCMC.hh"
#include "ConstRateModel.hh"
#include "DummyMCMC.hh"
#include "GammaMap.hh"
#include "GeneTreeMCMC.hh"
#include "PRNG.hh"
#include "ReconciliationTimeSampler.hh"
#include "ReconSeqApproximator.hh"
#include "SeqIO.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "SequenceModel.hh"
#include "SimpleMCMC.hh"
#include "SiteRateMCMC.hh"
#include "TopTimeMCMC.hh"
#include "StrStrMap.hh"
#include "SubstitutionMatrix.hh"
#include "SubstitutionMCMC.hh"
#include "SubstitutionModel.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <infileprefix> birth_rate death_rate beta alpha n_samples n_iter [model]\n"
       << "\n"
       << "Parameters:\n"
       << "   <infileprefix>     a string, data file is <prefix>.fasta, \n"
       << "                      tree file is <prefix.gene>\n"
       << "   <birth_rate>       a double\n"
       << "   <death_rate>       a double\n"
       << "   <beta>             a double, parameter for distr of S rootTime\n"
       << "   <alpha>            a double, the shape parameter for the gamma\n"
       << "                      distribution modelling site rate variation\n"
       << "   <n-samples>        an unsigned, gives the number of Monte\n" 
       << "                      Carlo samples to perform when esti-\n"
       << "                      mating likelihood of gene tree\n"
       << "   <n-iter   >        an unsigned, gives the number of MCMC\n" 
       << "                      iterations to perform \n"
       << "   <model>            a string, JC69, UniformAA, JTT, \n"
       << "                      UniformCodon, ,ust ,atch datatype\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < 8|| argc >9) 
    {
      usage(argv[0]);
    }
  try
    {
      SequenceModel* pmodel;
      if(argc == 9)
	{
	  string arg(argv[8]);
	  if(arg == string("UniformAA"))
	    {
	      pmodel =  new UniformAA();
	    }
	  else if(arg == ("JTT"))
	    {
	      pmodel =  new JTT();
	    }
	  else if(arg == ("UniformCodon"))
	    {
	      pmodel =  new UniformCodon();
	    }
	  else
	    {
	      pmodel =  new JC69();
	    }	  
	}
      else
	{
	  pmodel =  new JC69();
	}	  

      //      cout << *pmodel;

      double birthRate = atof(argv[2]);
      double deathRate = atof(argv[3]);
      double beta = atof(argv[4]);
      double alpha = atof(argv[5]);
      unsigned n_samples = atoi(argv[6]);
      unsigned n_iter = atoi(argv[7]);

      //Get tree and Data
      //---------------------------------------------
      TreeIO io = TreeIO::fromFile(string(argv[1])+".gene");
      Tree G = io.readSpeciesTree();
      io.setSourceFile(string(argv[1])+".species");
      Tree S = io.readSpeciesTree();
      StrStrMap gs = TreeIO::readGeneSpeciesInfo(string(argv[1])+".gs");
      SequenceData D = SeqIO::readSequences(string(argv[1])+".fasta");
      unsigned ntax = G.getNumberOfLeaves();
      unsigned nchar = D.getNumberOfPositions();

      //Create Q
      //---------------------------------------------
      beep::LA_Vector R = pmodel->R();
      beep::LA_DiagonalMatrix Pi = pmodel->Pi();
      beep::SubstitutionMatrix Q(R, Pi);
 
      //Create SubstitutionMCMC
      //---------------------------------------------
      UniformDensity gd(1.0, 0);
      PRNG rand;
      DummyMCMC dm(rand);
      EdgeRateModel erm(gd);
      ConstRateModel crm(gd, 1.0, erm);
      SiteRateMCMC srm(crm, 4, alpha, dm, 0.5, true);
      vector<string> partitionList ;
      partitionList.push_back(string("all"));
      SubstitutionMCMC sm(G, D, srm, Q, partitionList, srm, 0.5);
      
      cout << "\nTest av simpleMCMC\n" 
	   << "-----------------------------\n";
      TopTimeMCMC stm(dm, S, beta);
      BirthDeathMCMC bdm(stm, S, birthRate, deathRate);
      ReconciliationSampler rs(G, gs, bdm, rand);
      ReconciliationTimeSampler rts(rs, rand);
      SeqProbApproximator spa(dm, rts, sm, 1);
      ReconSeqApproximator rsa(dm, spa, rs, n_samples);
      GeneTreeMCMC gtm(bdm, rsa, rs, true, true);
      SimpleMCMC iterator(gtm, rand, 1);

      //      cout << iterator;
      cout << "like iteration " <<gtm.strHeader()<<endl<<endl;


      // Perform and time the Likelihood calcualtion
      time_t t0 = time(0);
      clock_t ct0 = clock();

      iterator.iterate(n_iter);
//       Probability p = rsa.suggestNewState().stateProb;
//       //Probability p = rsa.updateDataProbability();

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cout //<< "joint prob of tree is : "
// 	   << p
//	   << endl
	   << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << double(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
}

