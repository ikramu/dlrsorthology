#include "AnError.hh"
#include "DummyMCMC.hh"
#include "fastGEM.hh"
#include "fastGEM_BirthDeathMCMC.hh"
#include "GammaDensity.hh"
#include "TopTimeMCMC.hh"
#include "TreeIO.hh"

double mean;
double variance;

double birthRate;
double deathRate;
unsigned noOfDiscrIntervals;

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc != 9)
    {
      cerr << "usage: test_fastGEM <Guesttree> <HostTree> <gs> <birthrate> <deathrate> <mean> <variance> <noOfDiscrIntervals>\n";
      exit(1);
    }
  try
    {
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[1]);
      TreeIO io = TreeIO::fromFile(guest);
      Tree G = io.readGuestTree(0,0);  // Reads times?
      G.setName("G");
      cout << "G = \n"
	   << G;

      string host(argv[2]);
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  // Reads times?
      S.setName("S");
      cout << "S = \n" 
	   << S;

      StrStrMap gs;
      gs = TreeIO::readGeneSpeciesInfo(argv[3]);

      cout << "gs = \n" 
	   << gs;

      birthRate = atof(argv[4]);
      deathRate = atof(argv[5]);
      mean = atof(argv[6]);
      variance = atof(argv[7]);
      noOfDiscrIntervals = atoi(argv[8]);

      // Set up Density function for rates as a proxy for lengths!s
      //---------------------------------------------------------
      Density2P* df = 0;
      df = new GammaDensity(mean, variance);

      DummyMCMC dm;
      double beta = std::max(S.getRootNode()->getTime(), S.rootToLeafTime());
      TopTimeMCMC stm(dm, S, beta);
      stm.setTopTime(1.0);
      stm.fixTopTime();

      cerr << "beta: " << beta << " topTime: " << stm.getTopTime() << "\n";

      unsigned noOfDiscrPoints = 2*noOfDiscrIntervals+1;
      std::vector<double>* discrPoints;
      discrPoints = new std::vector<double>;

      fastGEM_BirthDeathMCMC bdm(stm, S, noOfDiscrPoints, discrPoints, birthRate, deathRate, &stm.getTopTime());
      fastGEM fgem(G,S,gs,df,bdm,discrPoints,noOfDiscrPoints);

      Node* gRoot = G.getRootNode();
      unsigned gRootIndex = gRoot->getNumber();
      Probability sumReconProb = fgem.calculateDataProbability();
      cout << "sumProb: " << sumReconProb << " (" <<  sumReconProb.val() <<")\n";
    }
  catch(AnError e)
    {
      cerr << "Error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what();
    }
  return(0);
};
