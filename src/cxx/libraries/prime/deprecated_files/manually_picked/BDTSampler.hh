/*
 * File:   BDTSampler.hh
 * Author: Peter Andersson
 *
 * Created on February 2, 2010
 *
 *
 *
 *
 * class BDTSample
 *
 * Simple class to keep track of parameters.
 *
 *
 * 
 *
 * class BDTSampler
 *
 * Easy interface to performing an MCMC run for birth rate, death rate and
 * top time (lambda, my and tau) given a gene tree and a species tree, i.e.
 * to sample from the distribution Pr[lambda, my, tau | G, S].
 *
 *
 */

#ifndef _BDTSAMPLER_HH
#define	_BDTSAMPLER_HH


#include "MCMCSampler.hh"
#include "Tree.hh"
#include "StrStrMap.hh"
#include "DummyMCMC.hh"
#include "TopTimeMCMC.hh"
#include "BirthDeathMCMC.hh"
#include "TreeMCMC.hh"
#include "SimpleMCMC.hh"
#include <string>

using namespace std;

namespace beep {

    
class BDTSample 
{
public:
    Real birth_rate;
    Real death_rate;
    Real top_time;
};



class BDTSampler : public MCMCSampler<BDTSample> {
public:

    /**
     * Creates a sampler for birth, death and toptime.
     *
     * @param S The species tree.
     * @param G The gene tree.
     * @param gs_map - A map between genes and species.
     */
    BDTSampler( const Tree G,
                const Tree S,
                const StrStrMap gs_map,
                const Real start_birth = 1.0,
                const Real start_death = 1.0);


    /**
     * Destructor.
     */
    ~BDTSampler();

    /**
     * Runs an MCMC chain with the 'old' way of printing things, i.e.
     * states are printed on a file while diagnostics about the mcmc
     * run is printed on cout.
     *
     * @param iteration Number of iterations.
     * @param output_file States will be printed here.
     */
    virtual void iterate(unsigned int iterations, string output_file = "", bool err_prints = false);

    /**
     * iterate
     *
     * Same as iterate(int, string), but also calls the state process 
     * function when 'callbackInterval' jumps has been performed.
     *
     * jumps - The total number of iterations to be performed.
     * output_file An output file for MCMC states.
     * callbackInterval - The interval in which the callback should be called,
     *                    i.e. a value of 100 indicates that the callback should
     *                    be called each 100 iteration.
     * callback - A method that is called on each callbackInterval sample.
     */
    virtual void iterate(int jumps,
                         string output_file,
                         int callbackInterval,
                         StateProcesser callback);

    /**
     * iterateWithTimeEstimation
     *
     * Performs jumps iterations and prints the time left each printInterval
     * iterations.
     *
     * jumps - Number of iterations to perform.
     * output_file An output file for MCMC states.
     * printInterval - Determines how often we should print the time left.
     */
    virtual void iterateWithTimeEstimation(int jumps, string output_file, int printInterval);



    /*================Overrided methods from MCMCSampler=======================*/
    /**
     * nextState
     *
     * Performs jumps iterations in the underlying MCMC chain and returns
     * the last state.
     */
    virtual BDTSample *nextState(int jumps = 1);

    /**
     * nextState
     *
     * Does exactly like nextState but also calls the process function when
     * callbackInterval jumps has been performed.
     *
     * jumps - The total number of iterations to be performed.
     * callbackInterval - The interval in which the callback should be called,
     *                    i.e. a value of 100 indicates that the callback should
     *                    be called each 100 iteration.
     * callback - A method that is called on each callbackInterval sample.
     */
    virtual BDTSample *nextState(int jumps,
                                 int callbackInterval,
                                 StateProcesser callback);

     /*=======================================================================*/

private:
    // Do not allow copy constructor to be called.
    BDTSampler(const BDTSampler &);

    /**
     * setup
     *
     * Sets up the nested BDT MCMC chain.
     */
    void setup(Real birth, Real death);


    /**
     * updateState
     * 
     * Updates the BDTSample
     */
    void updateState();

    //The BDTSample
    BDTSample *m_state;

    // The trees
    Tree m_G;
    Tree m_S;

    // A map from genes to species
    StrStrMap m_gs_map;

    //MCMC variables
    DummyMCMC *m_mcmcDummy;
    TopTimeMCMC *m_mcmcTopTime;
    BirthDeathMCMC *m_mcmcBD;
    GuestTreeMCMC *m_mcmcGuest;
    SimpleMCMC *m_mcmc_obj;

};

}

#endif	/* _BDTSAMPLER_HH */

