/* 
 * File:   DiscretizedDensity.hh
 * Author: fmattias
 *
 * Created on December 3, 2009, 2:26 PM
 */

#ifndef _DISCRETIZEDDENSITY_HH
#define	_DISCRETIZEDDENSITY_HH

#include <vector>

#include "Density2P.hh"
#include "Probability.hh"
#include "BeepVector.hh"


namespace beep {

class DiscretizedDensity {
public:
    /**
     * Constructor
     *
     * Creates a discretized version of a given continuous density. The
     * discretization is performed by restricting the continuous density
     * to be non-zero between two points (an interval), followed by a
     * discretization of the inner points in the interval such that it gives
     * equal to probability to points in an interval. The intervals all have
     * the same length.
     *
     * continousDensity - The density that will be discretized.
     * nonZeroStart - The start of the non-zero interval of the density.
     * nonZeroEnd - The end of the non-zero interval of the density.
     * intervals - The number of intervals in which the non-zero interval will
     *             be divided.
     */
    DiscretizedDensity(Density2P &continousDensity, float nonZeroStart, float nonZeroEnd, int intervals);

    /**
     *
     *
     */
    DiscretizedDensity(const DiscretizedDensity& orig);

    /**
     * Returns the probability of x.
     *
     * x - The value of Pr[X = x] will be returned.
     */
    Real pdf(Real x) const;

    /**
     * Returns the cumlative probability of x.
     *
     * x - The value of Pr[X <= x] will be returned.
     */
    Real cdf(Real x) const;

    /**
     * reDiscretize
     *
     * Recomputes the discretization, this is used when the parameters of the
     * continuous density has changed.
     */
    void reDiscretize();

    /**
     * Destructor
     */
    virtual ~DiscretizedDensity();
private:

    /**
     * Precomputes values for the pdf and cdf for the given density.
     */
    void createPDFAndCDF();

    /**
     * Returns the index of the interval given by the point x. If no such
     * interval exists -1 is returned.
     *
     * x - The point which interval will be returned.
     */
    int getInterval(Real x) const;

    // Vector of precomputed pdf values
    std::vector<Real> m_pdf;

    // Vector of precomputed cdf values
    std::vector<Real> m_cdf;

    // The density that is discretized
    Density2P & m_continousDensity;

    // The number of intervals which the non-zero part will be divided into
    int m_intervals;

    // The length of a single interval, assumes a equi-spaces discretization
    float m_intervalLength;

    // Start of the non-zero region of the density
    float m_nonZeroStart;

    // End of the non-zero region of the density
    float m_nonZeroEnd;
};

}

#endif	/* _DISCRETIZEDDENSITY_HH */

