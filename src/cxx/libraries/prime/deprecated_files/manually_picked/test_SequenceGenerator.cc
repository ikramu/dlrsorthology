#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "Beep.hh"
#include "DummyMCMC.hh"
#include "ConstRateModel.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "GammaDensity.hh"
#include "LA_Vector.hh"
#include "LA_DiagonalMatrix.hh"
#include "PRNG.hh"
#include "SequenceData.hh"
#include "SequenceGenerator.hh"
#include "SequenceType.hh"
#include "SiteRateHandler.hh"
#include "MatrixTransitionHandler.hh"
#include "Tree.hh"
#include "TreeIO.hh"


void 
usage(char *cmd)
{
  std::cerr << "Usage: "
	    << cmd
	    << " <treefile> <nchar> <nCats> <alpha> <SeqModel>\n"
	    << "\n"
	    << "Parameters:\n"
	    << "   <treefiolw>         The tree is \n"
	    << "   <nchar>             unsigned int, number of characters\n"
	    << "   <nCats>             <unsigned>; number of rate categories\n"
	    << "   <alpha>             <double>; shape parameter of Gamma\n"
	    << "                       distribution modeling rate variation\n"
	    << "                       across sites.\n"
	    << "   <SeqModel>          'jc69', etc\n"
    ;
  exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 6) 
    {
      usage(argv[0]);
    }

  try
    {
      string prefix(argv[1]);
      unsigned nchar = atoi(argv[2]);
      unsigned nCats = atoi(argv[3]);
      if(nCats < 0) 
	{
	  throw AnError
	    ("Error: nCats = 0; Must have at least one rate category");
	}
      Real alpha = atof(argv[4]);
      
      MatrixTransitionHandler pmodel = MatrixTransitionHandler::create(argv[5]);
      
      cout << "\nTest av SequenceGenerator\n" 
	   << "-----------------------------\n";
     TreeIO io = TreeIO::fromFile(prefix);
     Tree T = io.readHostTree();
     cout << T<<endl;

     PRNG rand;

     GammaDensity gd1(1, 0.5);
     ConstRateModel alphaC(gd1, T);
     SiteRateHandler srm(nCats, alphaC);
     srm.setAlpha(alpha);
     GammaDensity gd2(1, 0.5);  
     DummyMCMC dm;
     iidRateMCMC vrm(dm, gd2, T); 

     vrm.generateRates(); //generates rates iid gamma w. mean = 1.0
     
     cout << vrm
	  << endl;
     EdgeTimeRateHandler ewh(vrm);

     SequenceGenerator SG(T, pmodel, srm, ewh, rand);
     cout << SG;
     SequenceData D = SG.generateData(nchar);
     cout << D;
     
     cout << D.data4fasta() << "\n";
    }
  catch (exception& e)
    {
      cerr << "error found: "
	   << e.what() << "\n";
    }
}
