/* 
 * File:   LogFilePrinter.hh
 * Author: peter9
 * 
 * Created on January 28, 2010, 1:41 PM
 * 
 *
 * Simple log file management. Creates a log file called "log.txt" in the same
 * directory as the executable application file.
 * This class uses the singleton design pattern, which in this
 * case means there is only one log file for the entire application.
 *
 */

#ifndef _LOGFILEPRINTER_HH
#define	_LOGFILEPRINTER_HH
#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class LogFilePrinterDestroyer;

class LogFilePrinter {
public:
    /**
     * getLogFilePrinter
     *
     * Returns a pointer to the singleton LogFilePrinter object.
     *
     */
    static LogFilePrinter* getLogFilePrinter();

    /**
     * printMessage
     *
     * Prints a single message to the logfile.
     *
     * @param message The message to be printed.
     *
     */
    void printMessage(const string &message);

    /**
     * printSetup
     *
     * Convenience method for printing initial information and a start message.
     *
     * This is the setup printout:
     * 1. The message.
     * 2. A line with the date and time.
     * 3. The random seed.
     * 4. An empty line
     *
     * Note: The message do not have to end with a newline character for nice
     * printouts.
     *
     * @param message The message to be printed.
     *
     */
    void printSetup(const string &message);

    /**
     * printEntry
     *
     * Convenience method for printing an "entry" in the logfile using the
     * specified message.
     *
     * An entry is here defined as:
     * 1. A line of '=' characters.
     * 2. A line with the date and time.
     * 3. The message.
     * 4. A line with '=' characters.
     *
     * Note: The message do not have to end with a newline character for nice
     * printouts.
     *
     * @param message The message to be printed.
     *
     */
    void printEntry(const string &message);

    /**
     * printRandomSeed
     *
     * Prints a line in the log file stating the random seed used in the
     * application.
     */
    void printRandomSeed();

    /**
     * printDateAndTime
     *
     * Prints a line in the logfile with the current timestamp,
     * i.e. date and time.
     *
     */
    void printDateAndTime();

    /**
     * setOutputFile
     *
     * Changes the output file of the logger. If no singleton logger has been
     * created, the default log filename is changed to path.
     *
     * @param path The path to the log file.
     */
    static void setOutputFile(string &path);

    /**
     * active
     *
     * Decides if a log file is being used at the moment, i.e. the singleton
     * object is "active".
     */
    static bool isActive(){return s_printer != NULL;}

protected:

    /**
     * Protected constructor and destructor for use by the destroyer class.
     * The destroyer class must be a friend in this scope.
     */
    LogFilePrinter();
    friend class LogFilePrinterDestroyer;
    virtual ~LogFilePrinter();

private:

    //The singleton object.
    static LogFilePrinter *s_printer;
    //The destroyer object.
    static LogFilePrinterDestroyer s_destroyer;
    //The file stream used.
    ofstream m_file;
    
};

/**
 * This is a destroyer class which is used to deallocate the singleton object
 * at the end of the application.
 *
 */
class LogFilePrinterDestroyer {
public:
    LogFilePrinterDestroyer(LogFilePrinter* = 0);
    ~LogFilePrinterDestroyer();

    /**
     * setLogFilePrinter
     *
     * Sets the object to destroy.
     */
    void setLogFilePrinter(LogFilePrinter* s);
    
private:
    LogFilePrinter* m_printer;
    
};




#endif	/* _LOGFILEPRINTER_HH */

