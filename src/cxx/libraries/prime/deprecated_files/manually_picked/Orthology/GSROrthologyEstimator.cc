//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///*
// * File:   GSROrthologyEstimator.cc
// * Author: peter9
// *
// * Created on December 16, 2009, 3:25 PM
// */
//
//#include "GSROrthologyEstimator.hh"
//#include <boost/lexical_cast.hpp>
//#include <boost/algorithm/string.hpp>
//#include <boost/foreach.hpp>
//#include <stdlib.h>
//#include "mcmc/PosteriorSampler.hh"
//#include <utility>
//#include "Hacks.hh"
//#include "TimeEstimator.hh"
//#include "LogFilePrinter.hh"
//
////To be able to use foreach(...) construct
//#define foreach BOOST_FOREACH
//
//
//namespace beep{
//
//GSROrthologyEstimator::GSROrthologyEstimator(   const EdgeDiscTree &DS,
//                                                const StrStrMap &gs_map):
//                                                m_S(DS.getTree()),
//                                                m_DS(DS),
//                                                m_gs_map(gs_map),
//                                                m_states_calculated(false),
//                                                m_DIAGNOSTICS(true)
//{
//}
//
//GSROrthologyEstimator::GSROrthologyEstimator(const GSROrthologyEstimator& orig):
//                                m_S(orig.m_S),
//                                m_DS(orig.m_DS),
//                                m_gs_map(orig.m_gs_map),
//                                m_gsr_states(orig.m_gsr_states),
//                                m_gsr_objects(orig.m_gsr_objects),
//                                m_states_calculated(orig.m_states_calculated),
//                                m_DIAGNOSTICS(orig.m_DIAGNOSTICS)
//{
//}
//
//GSROrthologyEstimator::~GSROrthologyEstimator()
//{
//    //Delete states
//    foreach(EdgeDiscGSR *state, m_gsr_states){
//        delete state;
//    }
//    //Delete samples
//    foreach(EdgeDiscGSRObjects *s, m_gsr_objects){
//        delete s;
//    }
//}
//
//
///**
// * find_lca
// *
// * Finds the last common ancestor of a set of nodes in a tree.
// * Assumes all nodes is found in the same tree, otherwise the result
// * is undefined.
// * Note: If there is only one node in the set, then that node itself is
// * considered the lca.
// *
// * @param nodes A set of nodes.
// *
// */
//Node*
//GSROrthologyEstimator::find_lca(const SetOfNodes &nodes)
//{
//    Node *lca = nodes[0];
//    Tree T; //Which tree doesn't matter.
//    for(unsigned int i = 1; i < nodes.size(); i++){
//        lca = T.mostRecentCommonAncestor(lca, nodes[i]);
//    }
//    return lca;
//}
//
////void printson(const SetOfNodes &g){
////    cout << "Node set: " << endl;
////    for(unsigned int i= 0; i < g.size(); i++){
////        cout << g[i]->getName() << endl;
////    }
////}
//
//
///*
// * Assumes all genes are taken from the same tree, otherwise the
// * result is undefined.
// * Note that a pair of genes is always considered independent.
// */
//bool
//GSROrthologyEstimator::independent(const SetOfNodes &g1, const SetOfNodes &g2)
//{
//    //Find lcas
//    Node *lca_a = find_lca(g1);
//    Node *lca_b = find_lca(g2);
//
//    //Check for independence
//    return !( lca_a->dominates(*lca_b) || lca_b->dominates(*lca_a) );
//}
//
//
//
//
//bool
//GSROrthologyEstimator::isObligateDuplication(Node *node, Tree &S, StrStrMap &gs_map)
//{
//    if(node->isLeaf()){
//        return false;
//    }
//    //Not a leaf, then it is an obligate duplication if the leaf-groups are
//    //dependent in S.
//
//    //Get species groups
//    SetOfNodes left_leaves = node->getLeftChild()->getLeaves();
//    SetOfNodes left_species;
//    for(unsigned int i = 0; i < left_leaves.size(); i++){
//        left_species.insert(S.findLeaf( gs_map.find( left_leaves[i]->getName() ) ));
//    }
//
//    SetOfNodes right_leaves = node->getRightChild()->getLeaves();
//    SetOfNodes right_species;
//    for(unsigned int i = 0; i < right_leaves.size(); i++){
//        right_species.insert(S.findLeaf( gs_map.find( right_leaves[i]->getName() ) ));
//    }
//    return !( independent(left_species, right_species) );
//}
//
//
//
//
//
//void
//GSROrthologyEstimator::insert_species_leaves(   SetOfNodes &species_leaves,
//                                                const Tree &S,
//                                                const StrStrMap &gs_map,
//                                                const vector<string> &gn1,
//                                                const vector<string> &gn2)
//{
//    foreach(string s, gn1){
//        species_leaves.insert(S.findLeaf( gs_map.find( s ) ));
//    }
//    foreach(string s, gn2){
//        species_leaves.insert(S.findLeaf( gs_map.find( s ) ));
//    }
//}
//
//void
//GSROrthologyEstimator::insert_species_leaves(   vector<SetOfNodes> &S_vec,
//                                                const Tree &S,
//                                                const StrStrMap &gs_map,
//                                                const GroupPairVector &gpv)
//{
//    unsigned int i = 0;
//    foreach(GroupPair gp, gpv){
//        insert_species_leaves(S_vec[i], S, gs_map, gp.first, gp.second);
//        i++;
//    }
//}
//
//
//
//
//
//
//
///**
// * get_ortho_prob
// *
// * Finds the orthology probability of two sets of genes in a GSR state.
// *
// * @param g1 A set of genes.
// * @param g2 A set of genes.
// * @param species_leaves A set of species leaves in which the genes are found.
// * @param gsr A GSR state.
// */
//Probability
//GSROrthologyEstimator::get_ortho_prob( const SetOfNodes &g1,
//                const SetOfNodes &g2,
//                const SetOfNodes &species_leaves,
//                EdgeDiscGSR &gsr)
//{
//    /**
//     * This basically works by finding the lca of the genes in the gene tree
//     * and the lca of the genes in the species tree, then finding
//     * the orthology probability by calculating the probability of placing the
//     * lca in the gene tree at the lca in the species tree.
//     */
//
//    //Orthology probability, defaults to 0.
//    Probability ortho_prob;
//
//    //A condition for orthology is that the gene sets are independent.
//    if( independent(g1, g2) ){
//        /*
//         * Find LCA in species tree between all species having genes
//         * in one of the gene sets
//         */
//
//        //Create "discretized" speciation node
//        Node *lca_s = find_lca(species_leaves );
//        EdgeDiscretizer::Point point(lca_s, 0);
//
//        //Calculate speciation probability
//        //Get lcas
//        Node *lca_g1 = find_lca(g1);
//        Node *lca_g2 = find_lca(g2);
//        Node *lca_g = gsr.getTree().mostRecentCommonAncestor(lca_g1, lca_g2);
//
//        ortho_prob = gsr.getPlacementProbability(lca_g, &point);
//
//        if(ortho_prob.val() > (1.0 + 1e-10)){
//            stringstream ss;
//            ss << "GSROrthologyEstimator::get_ortho_prob: Orthology probability is too large!!!!!!" << endl
//            << "GSROrthologyEstimator::get_ortho_prob: For node: " << lca_g->getNumber() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: Placed on node: (" << lca_s->getNumber() << ", 0)" << endl
//            << "GSROrthologyEstimator::get_ortho_prob: OrtoProb: " << ortho_prob.val() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: PlacProb: " << gsr.getPlacementProbability(lca_g, &point).val() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: TreeProb: " << gsr.calculateDataProbability().val() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: TPlaProb: " << gsr.getTotalPlacementProbability(lca_g).val() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: TsumProb: " << gsr.getTotalPlacementSum(&point).val() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: birthrat: " << gsr.getBDProbs().getBirthRate() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: deathrat: " << gsr.getBDProbs().getDeathRate() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: mean    : " << gsr.getEdgeRateDensity().getMean() << endl
//            << "GSROrthologyEstimator::get_ortho_prob: variance: " << gsr.getEdgeRateDensity().getVariance() << endl
//            << endl;
//            ss << "Species tree: " << gsr.getDiscretizedHostTree().getTree() << endl
//                << "GS MAP: " << gsr.getGSMap() << endl;
//
//            ss << gsr.test(1e-10, true) << endl;
//
//            if(LogFilePrinter::isActive()){
//                LogFilePrinter::getLogFilePrinter()->printEntry(ss.str());
//            }
//            else{
//                cout << ss.str() << endl;
//            }
//            ortho_prob = Probability(1.0);
//
//        }
//    }
//
//    return ortho_prob;
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
///**
// * estimateOrthologyFromFile
// *
// * Calculates the orthology probability by reading GSR states from file.
// *
// *
// */
//Probability
//GSROrthologyEstimator::estimateOrthologyFromFile(   const string &filename,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    const vector<string> &gn1,
//                                                    const vector<string> &gn2)
//{
//    TreeIO tree_reader;                 //Used for parsing prime trees
//    ifstream sample_file;               //File object for samples
//    string curr_line;                   //Current line when reading samples
//    Tokenizer tokenizer(";");           //String tokenizer
//    string token;                       //Holder for current token
//    int nr_states;                      //Total number of states
//
//    float mean;                         //Mean for a density function
//    float variance;                     //Variance for a density function
//    float birth_rate;                   //Birth rate for a state
//    float death_rate;                   //Death rate for a state
//
//    Tree G;                             //Gene tree
//    Tree S;                             //Species tree
//    SetOfNodes species_leaves;          //Leaves in species tree where genes are found
//
//    SetOfNodes g1, g2;                  //Nodes for genes
//
//    /*Setup orthology estimation*/
//    Probability cum_ortho_prob;
//
//    //Find species nodes for genes
//    S = DS.getTree();
//    insert_species_leaves(species_leaves, S, gs_map, gn1, gn2);
//
//    /*Read states and calculate cumulative orthology probability*/
//    nr_states = 0;
//    sample_file.open( filename.c_str() );
//    if ( sample_file.is_open() ){
//        while (! sample_file.eof() )
//        {
//            //Skip lines of comments
//            if(sample_file.peek() == '#'){
//                sample_file.ignore(1024, '\n');
//                continue;
//            }
//
//            //Get next line from file
//            curr_line = "";
//            getline(sample_file, curr_line);
//
//            //Skip empty lines
//            if(curr_line == ""){
//                continue;
//            }
//
//            nr_states++;
//
//            /*
//             * Information in each line is separated by ';'. The tokenizer
//             * splits the line at each ';'
//             */
//            //Set tokenizer
//            tokenizer.setString(curr_line);
//
//            //Skip info about likelihoods and number of iterations
//            tokenizer.getNextToken();
//            tokenizer.getNextToken();
//
//            //Read gene tree
//            tree_reader.setSourceString(tokenizer.getNextToken());
//            G = tree_reader.readNewickTree();
//
//            //Read mean
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            mean = boost::lexical_cast<float> (token);
//
//            //Read variance
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            variance = boost::lexical_cast<float> (token);
//
//            //Skip unnecessary gene tree topology
//            tokenizer.getNextToken();
//
//            //Read birth rate
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            birth_rate = boost::lexical_cast<float> (token);
//
//            //Read death rate
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            death_rate = boost::lexical_cast<float> (token);
//
//            //Create density function
//            GammaDensity gamma(mean,variance);
//
//            //Create bd probs object
//            EdgeDiscBDProbs bd_probs(DS, birth_rate, death_rate);
//
//            //Create state
//            EdgeDiscGSR gsr(G, DS, gs_map, gamma, bd_probs );
//
//            g1 = find_gene_set(gsr, gn1);
//            g2 = find_gene_set(gsr, gn2);
//
//            /*Update probability*/
//            cum_ortho_prob += get_ortho_prob(g1, g2, species_leaves, gsr);
//        }
//        sample_file.close();
//    }
//    else{
//        cerr << "GSROrthologyEstimator::estimateOrthologyFromFile: Could not open file: " << sample_file << endl;
//        exit(EXIT_FAILURE);
//    }
//    //Divide by numver of states to get orthology probability
//    Probability tmp( static_cast<Real>(nr_states) );
//    cum_ortho_prob /= tmp;
//    return cum_ortho_prob;
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//vector<Probability>
//GSROrthologyEstimator::estimateOrthologyFromFile(   const string &filename,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    const GroupPairVector &pairs)
//{
//    TreeIO tree_reader;                 //Used for parsing prime trees
//    ifstream sample_file;               //File object for samples
//    string curr_line;                   //Current line when reading samples
//    Tokenizer tokenizer(";");           //String tokenizer
//    string token;                       //Holder for current token
//    int nr_states;                      //Total number of states
//
//    float mean;                         //Mean for a density function
//    float variance;                     //Variance for a density function
//    float birth_rate;                   //Birth rate for a state
//    float death_rate;                   //Death rate for a state
//
//    Tree G;                             //Gene tree
//    Tree S;                             //Species tree
//
//
//    /*Setup orthology estimation*/
//    vector<Probability> probs( pairs.size() );
//
//    //Find species nodes for genes
//    S = DS.getTree();
//    vector<SetOfNodes> S_leaves_vec( pairs.size() );
//    insert_species_leaves(S_leaves_vec, S, gs_map, pairs);
//
//    /*Read states and calculate cumulative orthology probability*/
//    nr_states = 0;
//    sample_file.open( filename.c_str() );
//    if ( sample_file.is_open() ){
//        while (! sample_file.eof() )
//        {
//            //Skip lines of comments
//            if(sample_file.peek() == '#'){
//                sample_file.ignore(1024, '\n');
//                continue;
//            }
//
//            //Get next line from file
//            curr_line = "";
//            getline(sample_file, curr_line);
//
//            //Skip empty lines
//            if(curr_line == ""){
//                continue;
//            }
//
//            nr_states++;
//
//            /*
//             * Information in each line is separated by ';'. The tokenizer
//             * splits the line at each ';'
//             */
//            //Set tokenizer
//            tokenizer.setString(curr_line);
//
//            //Skip info about likelihoods and number of iterations
//            tokenizer.getNextToken();
//            tokenizer.getNextToken();
//
//            //Read gene tree
//            tree_reader.setSourceString(tokenizer.getNextToken());
//            G = tree_reader.readNewickTree();
//
//            //Read mean
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            mean = boost::lexical_cast<float> (token);
//
//            //Read variance
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            variance = boost::lexical_cast<float> (token);
//
//            //Skip unnecessary gene tree topology
//            tokenizer.getNextToken();
//
//            //Read birth rate
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            birth_rate = boost::lexical_cast<float> (token);
//
//            //Read death rate
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            death_rate = boost::lexical_cast<float> (token);
//
//            //Create density function
//            GammaDensity gamma(mean,variance);
//
//            //Create bd probs object
//            EdgeDiscBDProbs bd_probs(DS, birth_rate, death_rate);
//
//            //Create state
//            EdgeDiscGSR gsr(G, DS, gs_map, gamma, bd_probs );
//
//            NodesPairVector npv = convert_vector(gsr, pairs);
//
//
//            /*Update probabilities*/
//            for(unsigned int i = 0; i < npv.size(); i++){
//                const NodesPair &p = npv[i];
//                probs[i] += get_ortho_prob(p.first, p.second, S_leaves_vec[i], gsr);
//            }
//        }
//        sample_file.close();
//    }
//    else{
//        cerr << "GSROrthologyEstimator::estimateOrthologyFromFile: Could not open file: " << sample_file << endl;
//        exit(EXIT_FAILURE);
//    }
//    //Divide by number of states to get orthology probability
//    Probability numstates( static_cast<Real>(nr_states) );
//    for(unsigned int i = 0; i < probs.size(); i++){
//        probs[i] /= numstates;
//    }
//    return probs;
//
//}
//
//Probability
//GSROrthologyEstimator::estimateOrthologyFromMCMC(   const int iterations,
//                                                    const int thinning,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    MatrixTransitionHandler &Q,
//                                                    SequenceData &D,
//                                                    Density2P &density,
//                                                    const Real birth_rate,
//                                                    const Real death_rate,
//                                                    const vector<string> &gn1,
//                                                    const vector<string> &gn2,
//                                                    const bool diagnostics,
//                                                    unsigned int burn_in)
//{
//    //Get Species tree
//    Tree S = DS.getTree();
//
//    //Create MCMC sampler
//    PosteriorSampler mcmc(D, S, gs_map, Q, density, DS);
//    mcmc.getGSRSampler()->setBDParameters(birth_rate, death_rate);
//
//    /*Burn-in*/
//    if(diagnostics){
//        cout << "Burn-in.." << endl;
//        mcmc.getGSRSampler()->nextStateWithTimeLeft(burn_in, thinning);
//        cout << "Burn-in complete." << endl;
//    }
//    else{
//        mcmc.getGSRSampler()->nextState(burn_in);
//    }
//
//    //Time estimation variables
//    TimeEstimator time(iterations);
//
//    //Variables used for ease of notation (not really..)
//    int j = thinning;
//    int it = iterations;
//
//    //Setup orthology estimation
//    SetOfNodes species_leaves;
//    Probability cum_ortho_prob;
//
//    //Find species leaves
//    insert_species_leaves(species_leaves, S, gs_map, gn1, gn2);
//
//    //Start iterations
//    if(diagnostics){cout << "Sampling.." << endl;}
//    int nr_states = 0;
//    for(int i = 0; i < it; i += j){
//        /*Get next sample*/
//        EdgeDiscGSR *gsr = mcmc.getGSRSampler()->nextState(j);
//        nr_states++;
//
//        /*Calculate estimated time left*/
//        if(diagnostics){
//            time.update(j);
//            time.printEstimatedTimeLeft();
//        }
//
//        /*Update probability*/
//        //Get gene sets
//        SetOfNodes g1 = find_gene_set( *(mcmc.getGSRSampler()->currentState()), gn1);
//        SetOfNodes g2 = find_gene_set( *(mcmc.getGSRSampler()->currentState()), gn2);
//        cum_ortho_prob += get_ortho_prob( g1, g2, species_leaves, *gsr);
//    }
//    //Divide cum_ortho_prob with the number of MCMC itertaions to get estimation
//    //of orthology probability
//    Probability tmp( static_cast<Real>( nr_states ) );
//    cum_ortho_prob /= tmp;
//    return cum_ortho_prob;
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//vector<Probability>
//GSROrthologyEstimator::estimateOrthologyFromMCMC(   const int iterations,
//                                                    const int thinning,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    MatrixTransitionHandler &Q,
//                                                    SequenceData &D,
//                                                    Density2P &density,
//                                                    const Real birth_rate,
//                                                    const Real death_rate,
//                                                    const GroupPairVector &pairs,
//                                                    const bool diagnostics,
//                                                    unsigned int burn_in)
//{
//    //Get species tree
//    Tree S = DS.getTree();
//
//    //Create MCMC layer
//    PosteriorSampler mcmc(D, S, gs_map, Q, density, DS);
//    mcmc.getGSRSampler()->setBDParameters(birth_rate, death_rate);
//
//    /*Burn-in*/
//    if(diagnostics){
//        cout << "Burn-in.." << endl;
//        mcmc.getGSRSampler()->nextStateWithTimeLeft(burn_in, thinning);
//        cout << "Burn-in complete." << endl;
//    }
//    else{
//        mcmc.getGSRSampler()->nextState(burn_in);
//    }
//
//    //Time estimation variable
//    TimeEstimator time(iterations);
//
//    //Variables used for ease of notation
//    int j = thinning;
//    int it = iterations;
//
//    /*Setup orthology estimation*/
//    vector<Probability> probs( pairs.size() );
//
//    //Find species nodes for genes
//    vector<SetOfNodes> S_leaves_vec( pairs.size() );
//    insert_species_leaves(S_leaves_vec, S, gs_map, pairs);
//
//    //Start iterations
//    if(diagnostics){cout << "Sampling.." << endl;}
//    int nr_states = 0;
//    for(int i = 0; i < it; i += j){
//        /*Get next sample*/
//        EdgeDiscGSR *gsr = mcmc.getGSRSampler()->nextState(j);
//        nr_states++;
//
//        /*Calculate estimated time left*/
//        if(diagnostics){
//            time.update(j);
//            time.printEstimatedTimeLeft();
//        }
//
//        NodesPairVector npv = convert_vector(*gsr, pairs);
//        /*Update probability*/
//        for(unsigned int i = 0; i < npv.size(); i++){
//            const NodesPair &p = npv[i];
//            probs[i] += get_ortho_prob(p.first, p.second, S_leaves_vec[i], *gsr);
//        }
//    }
//    //Divide cum_ortho_prob with the number of MCMC itertaions to get estimation
//    //of orthology probability
//    Probability numstates( static_cast<Real>(nr_states) );
//
//    for(unsigned int i = 0; i < probs.size(); i++){
//        probs[i] /= numstates;
//    }
//    return probs;
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//void
//GSROrthologyEstimator::calculateStatesByMCMC(   const int iterations,
//                                                const int thinning,
//                                                MatrixTransitionHandler &Q,
//                                                SequenceData &D,
//                                                Density2P &density,//Currently only support for gamma density
//                                                const Real start_birth_rate,
//                                                const Real start_death_rate)
//{
//    //Time estimation variables
//    TimeEstimator time(iterations);
//    //Variables used for ease of notation
//    int j = thinning;
//    int it = iterations;
//
//    //Create MCMC layer
//    PosteriorSampler mcmc(D, m_S, m_gs_map, Q, density, m_DS);
//    mcmc.getGSRSampler()->setBDParameters(start_birth_rate, start_death_rate);
//
//    //Start iterations
//        /*Start mcmc iterations.*/
//    for(int i = j; i <= it; i += j){
//        /*Get next sample*/
//        EdgeDiscGSR *gsr = mcmc.getGSRSampler()->nextState(j);
//
//        /*Calculate estimated time left*/
//        if(m_DIAGNOSTICS){
//            time.update(j);
//            time.printEstimatedTimeLeft();
//        }
//
//        /*
//         * Copy state variables into a EdgeDiscGSRObjects object, then create a new state from these parameters.
//         * This is because EdgeDiscGSR works with references internally so copying a state directly
//         * will not work. To save a state we actually have to construct a new state,
//         * otherwise the information about the state will be overwritten by MCMCGSRLayer.
//         */
//        const GammaDensity &g = static_cast<const GammaDensity&>( gsr->getEdgeRateDensity() );
//        EdgeDiscGSRObjects *ps = new EdgeDiscGSRObjects(gsr->getTree(), g, gsr->getBDProbs());
//        m_gsr_objects.push_back(ps);
//
//        m_gsr_states.push_back( new EdgeDiscGSR( ps->getGeneTree(), m_DS, m_gs_map, ps->getGamma(), ps->getBDProbs()) );
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//void
//GSROrthologyEstimator::readStatesFromFile(const string filename,
//                                            EdgeDiscTree &DS,
//                                            StrStrMap &gs_map)
//{
//    TreeIO tree_reader;                 //Used for parsing prime trees
//    ifstream sample_file;               //File object for samples
//    string curr_line;                   //Current line when reading samples
//    Tokenizer tokenizer(";");           //String tokenizer
//    string token;                       //Holder for current token
//
//    float mean;                         //Mean for a density function
//    float variance;                     //Variance for a density function
//    float birth_rate;                   //Birth rate for a state
//    float death_rate;                   //Death rate for a state
//
//    GammaDensity *gamma;                 //Density function
//    EdgeDiscBDProbs *bd_probs;           //Birth-death rates holder
//    Tree G;                             //Gene tree
//
//
//
//    /*Read samples and create states*/
//    sample_file.open( filename.c_str() );
//    if ( sample_file.is_open() ){
//        while (! sample_file.eof() )
//        {
//            //Skip lines of comments
//            if(sample_file.peek() == '#'){
//                sample_file.ignore(1024, '\n');
//                continue;
//            }
//
//            //Get next line from file
//            curr_line = "";
//            getline(sample_file, curr_line);
//
//            //Skip empty lines
//            if(curr_line == ""){
//                continue;
//            }
//
//            /*
//             * Information in each line is separated by ';'. The tokenizer
//             * splits the line at each ';'
//             */
//            //Set tokenizer
//            tokenizer.setString(curr_line);
//
//            //Skip info about likelihoods and number of iterations
//            tokenizer.getNextToken();
//            tokenizer.getNextToken();
//
//            //Read gene tree
//            tree_reader.setSourceString(tokenizer.getNextToken());
//            G = tree_reader.readNewickTree();
//
//            //Read mean
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            mean = boost::lexical_cast<float> (token);
//
//            //Read variance
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            variance = boost::lexical_cast<float> (token);
//
//            //Skip unnecessary gene tree topology
//            tokenizer.getNextToken();
//
//            //Read birth rate
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            birth_rate = boost::lexical_cast<float> (token);
//
//            //Read death rate
//            token = tokenizer.getNextToken();
//            boost::trim<string>(token);
//            death_rate = boost::lexical_cast<float> (token);
//
//            //Create density function
//            gamma = new GammaDensity(mean,variance);
//            bd_probs = new EdgeDiscBDProbs(DS, birth_rate, death_rate);
//
//            //Create sample
//            EdgeDiscGSRObjects *ps = new EdgeDiscGSRObjects(G, *gamma, *bd_probs);
//            m_gsr_objects.push_back( ps );
//
//            //Create state
//            m_gsr_states.push_back( new EdgeDiscGSR( ps->getGeneTree(), DS, gs_map, ps->getGamma(), ps->getBDProbs()) );
//
//            delete gamma;
//            delete bd_probs;
//        }
//        sample_file.close();
//    }
//    else{
//        cerr << "GSROrthologyEstimator::readStatesFromFile: Could not open file: " << sample_file << endl;
//        exit(EXIT_FAILURE);
//    }
//}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//Probability
//GSROrthologyEstimator::estimateOrthology(   const vector<string> &gn1,
//                                            const vector<string> &gn2) const
//{
//    if(!m_states_calculated){
//        cout << "GSROrthologyEstimator::estimateOrthology: Warning states not calculated." << endl;
//        return Probability(0.0);
//    }
//
//    //Setup orthology estimation
//    SetOfNodes species_leaves;
//    insert_species_leaves(species_leaves, m_S, m_gs_map, gn1, gn2);
//    Probability cum_ortho_prob;
//
//    SetOfNodes g1,g2;
//
//    //Walk though each state
//    foreach(EdgeDiscGSR *gsr, m_gsr_states){
//        g1 = find_gene_set(*gsr, gn1);
//        g2 = find_gene_set(*gsr, gn2);
//        cum_ortho_prob += get_ortho_prob(g1, g2, species_leaves, *gsr);
//    }
//
//    Probability tmp( static_cast<Real>(m_gsr_states.size()));
//    return cum_ortho_prob / tmp;
//}
//
//
//
//
//
//
//
//
//
//vector<Probability>
//GSROrthologyEstimator::estimateOrthology( const GroupPairVector &pairs) const
//{
//    if(!m_states_calculated){
//        cout << "GSROrthologyEstimator::estimateOrthology: Warning states not calculated." << endl;
//        return vector<Probability>(0);
//    }
//
//    vector<Probability> probs( pairs.size() );
//
//    //Find species nodes for genes
//    vector<SetOfNodes> S_leaves_vec( pairs.size() );
//    insert_species_leaves(S_leaves_vec, m_S, m_gs_map, pairs);
//
//    NodesPairVector npv;
//
//    //Walk though each state
//    foreach(EdgeDiscGSR *gsr, m_gsr_states){
//        npv = convert_vector(*gsr, pairs);
//        for(unsigned int i = 0; i < npv.size(); i++){
//            const NodesPair &p = npv[i];
//            probs[i] += get_ortho_prob(p.first, p.second, S_leaves_vec[i], *gsr);
//            i++;
//        }
//    }
//
//    Probability numstates( static_cast<Real>( m_gsr_states.size() ) );
//    for(unsigned int i = 0; i < probs.size(); i++){
//        probs[i] /= numstates;
//    }
//    return probs;
//}
//
//
//
//
//
//
//
//
//
//
//
//
//Probability
//GSROrthologyEstimator::estimateOrthologyFromState(  EdgeDiscGSR &gsr,
//                                                    const SetOfNodes  &g1,
//                                                    const SetOfNodes  &g2)
//{
//    Tree S = gsr.getDiscretizedHostTree().getTree();
//    SetOfNodes species_leaves;
//    for(unsigned int i = 0; i < g1.size(); i++){
//        species_leaves.insert(S.findLeaf( gsr.getGSMap().find( g1[i]->getName() ) ));
//    }
//    for(unsigned int i = 0; i < g2.size(); i++){
//        species_leaves.insert(S.findLeaf( gsr.getGSMap().find( g2[i]->getName() ) ));
//    }
//    return get_ortho_prob(g1, g2, species_leaves, gsr);
//}
//
//
//
//
//
//
//
//
//
//vector<Probability>
//GSROrthologyEstimator::estimateOrthologyFromState(  EdgeDiscGSR     &gsr,
//                                                    const NodesPairVector  &pairs)
//{
//    Tree S = gsr.getDiscretizedHostTree().getTree();
//
//    vector<Probability> probs( pairs.size() );
//
//    //Find species nodes for genes
//    vector<SetOfNodes> S_leaves_vec( pairs.size() );
//
//    for(unsigned int i = 0; i < pairs.size(); i++){
//        const NodesPair &p = pairs[i];
//        for(unsigned int j = 0; j < p.first.size(); j++){
//            S_leaves_vec[i].insert(S.findLeaf( gsr.getGSMap().find( p.first[j]->getName() ) ));
//        }
//        for(unsigned int j = 0; j < p.second.size(); j++){
//            S_leaves_vec[i].insert(S.findLeaf( gsr.getGSMap().find( p.second[j]->getName() ) ));
//        }
//    }
//
//    /*Estimate orthologies*/
//    for(unsigned int i = 0; i < pairs.size(); i++){
//        const NodesPair &p = pairs[i];
//        probs[i] = get_ortho_prob(p.first, p.second, S_leaves_vec[i], gsr);
//    }
//
//    return probs;
//}
//
//
//
//
//
//
//
//SetOfNodes
//GSROrthologyEstimator::find_gene_set(EdgeDiscGSR &gsr, const vector<string> &gene_names)
//{
//    SetOfNodes ret;
//
//    foreach(string s, gene_names){
//        ret.insert( gsr.getTree().findLeaf( s ) );
//    }
//
//
//    return ret;
//}
//
//
//
//GSROrthologyEstimator::NodesPairVector
//GSROrthologyEstimator::convert_vector(EdgeDiscGSR &gsr, const GroupPairVector &gpv)
//{
//    NodesPairVector v;
//    GroupPair gp;
//    foreach(GroupPair gp, gpv){
//        //Find leaves
//        SetOfNodes g1,g2;
//        foreach(string s, gp.first){
//            g1.insert(gsr.getTree().findLeaf(s));
//        }
//        foreach(string s, gp.second){
//            g2.insert(gsr.getTree().findLeaf(s));
//        }
//        //Insert into nodes vector
//        v.push_back(pair<SetOfNodes, SetOfNodes>(g1,g2));
//    }
//    return v;
//}
//
//};//namespace beep
