#include <map>
#include <fstream>

#include "AnError.hh"
#include "EnumerateReconciliationModel.hh"
#include "TreeIO.hh"
#include "GuestTreeModel.hh"
#include "MaxReconciledTreeModel.hh"
#include "LambdaMap.hh"

using namespace std;
using namespace beep;

void usage(char* cmsd);
int nParams = 3;


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  if(argc <= nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = 1;//readOptions(argc, argv);
      if(opt + 2 > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	}
      
      //Get tree and Data
      //---------------------------------------------
      string G_filename(argv[opt++]);
      TreeIO io = TreeIO::fromFile(G_filename);
      Tree G = io.readGuestTree(0, 0);//BeepTree(false, false, false, false, 0, 0);
      string S_filename(argv[opt++]);
      io.setSourceFile(S_filename);
      Tree S = io.readHostTree();//BeepTree(true, false, false, true, 0, 0);
      S.setName("S");

      StrStrMap gs;
      gs = TreeIO::readGeneSpeciesInfo(argv[3]);
      
      cout << G << S << gs << endl;

      S.topTime = 10.0;
      LambdaMap lambda(G, S, gs);
      Real b = 5;
      Real d = 8;
      BirthDeathProbs bdp(S, b,d);
      EnumerateLabeledReconciliationModel N(G, gs, bdp);
      cout << N << endl;

      cout << endl <<"N.getNumberOfReconciliations() = " << N.getNumberOfReconciliations() << endl <<endl;
      Probability sum = 0;
      Probability max = 0;
      unsigned best;
      for(unsigned i = 0; i < N.getNumberOfReconciliations(); i++)
//        for(unsigned i = 0; i < 4; i++)
	{
	  if(i%1000 == 0)
	    cout << i << endl;
	  cout << "\n\nReconciliation #" <<i << "\n";
	  N.setGamma(i);
 	  cout << N.getGamma().print(false) 
 	       << "\n\n";
 	  cout << " receives the unique number " 
 	       << N.computeGammaID()
 	       <<" and has Prob ";
	  Probability p = N.calculateDataProbability();
 	  cout << p
 	       << endl;
	  sum += p;
	  if(p > max)
	    {
	      max = p;
	      best = N.computeGammaID();
	    }
	}
      cout << "\nSum of Probabilities for these recoonciliations are "
	   << sum << endl;
//       GuestTreeModel a(G, gs, bdp);
      LabeledGuestTreeModel a(N);
      Probability p = a.calculateDataProbability();
      cout << "\nThe corresponding prob from LabeledGuestTreeModel is " 
	   << p
	   << "\nThe difference is ";
      if(sum > p)
	cout << (sum-p).val();
      else
	cout << (p-sum).val();
      cout<< endl;
      cout << "\nMax of Probabilities for these recoonciliations are "
	   << max << " for reconciliation " << best <<endl;
      MaxReconciledTreeModel m(N);
      p = m.getMLReconciliation();
      cout << "\nThe corresponding prob from MaxReconciliationModel is " 
	   << p
	   << "\nThe difference is ";
      if(max > p)
	cout << (max-p).val();
      else
	cout << (p-max).val();
      cout<< endl;
      
    }
  catch(AnError e) 
    {
      e.action();
    }
  catch (exception e) 
    {
      cerr << "Error: "
	   << e.what();
    }
}
void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " <guesttree> <hosttree> <gs>"
    << "\n"
    ;
  exit(1);
} 
