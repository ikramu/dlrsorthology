#include "AnError.hh"
#include "Beep.hh"
#include "SequenceType.hh"


int main()
{
  using namespace beep;
  try
  {

  std::cout << "DNA\n------------------------------------------------\n";
  beep::DNA dna;
  char s[]= 
    {'a','c','g','t','m','r','w','s','y','k','v','h','d','b','x','n','-'};
  for(char* sp = s; sp < s + 17; sp ++)
    {
      std::cout << "state " 
		<< *sp 
		<< " has leafLike \n"
		<< dna.getLeafLike(*sp)
		<< std::endl;
    }

  std::cout << "\n\nAA\n------------------------------------------------\n";
  beep::AminoAcid aa;
  char a[]= 
  {'a', 'r', 'n', 'd', 'c', 'q', 'e', 'g', 'h', 'i', 'l', 'k', 
   'm', 'f', 'p', 's', 't', 'w', 'y', 'v', 'b', 'z','x', '-'};
  for(char* ap = a; ap < a + 24; ap ++)
    {
      std::cout << "state " 
		<< *ap 
		<< " has leafLike \n"
		<< aa.getLeafLike(*ap)
		<< std::endl;
    }

  std::cout << dna
	    << aa;

  }
  catch(AnError e)
  {
    e.action();
  }
  return 0;
}
