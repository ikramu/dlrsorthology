#include "DummyMCMC.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeWeightMCMC.hh"
#include "GammaDensity.hh"
#include "MCMCLengthApproximator.hh"
#include "RandomTreeGenerator.hh"
#include "SimpleML.hh"
#include "TreeIO.hh"
#include "TreeIOTraits.hh"

double birthRate = 0.5;
double deathRate = 0.5;
double topTime = 1.0;
double mean = 0.1;
double variance = 0.01;

// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int nParams = 3;
  if (argc < nParams) 
    {
      cerr << "Usage: test_MCMCLengthApproximator <G> <S> [<gs>]\n";
      exit(1);
    }
  try
    {
      // Set up guest tree
      //---------------------------------------------
      StrStrMap gs;
      TreeIO io = TreeIO::fromFile(argv[1]);
//       io.checkTagsForTree(traits);
//       TreeIOTraits traits;
//       traits.setBL(true);
//       traits.setNWisET(false);
      Tree G = io.readNewickTree();//0,&gs);  

      // Set up host tree
      //---------------------------------------------
      TreeIO io2 = TreeIO::fromFile(argv[2]);
      Tree S = io2.readHostTree();

      // If gs is empty (no mapping info was found in the gene tree or a
      // random tree was created), we have to read a file with that info.
      if (gs.size() == 0)
	{
	  if( 2 < argc)
	    {
	      gs = TreeIO::readGeneSpeciesInfo(string(argv[3]));
	    }
	  else
	    {
	      cerr << "Missing gene-t-species leaf mapping\n";
	      exit(1);
	    }
	}

      // Set up priors
      //-------------------------------------------------------
      DummyMCMC dm;
      BirthDeathProbs bdm(S, birthRate, deathRate, &topTime);
      GammaDensity df(mean, variance);
      ReconciledTreeTimeMCMC rttm(dm, G, gs, bdm);
//       if(false)
// 	{
	  LengthRateModel lrm(df, G);
	  EdgeWeightMCMC ewm(rttm, lrm);
	  if(G.hasLengths() == false)
	    {
	      ewm.generateWeights();
	    }
	  ewm.fixWeights();
	  EdgeWeightHandler ewh(lrm);
	  SimpleMCMC iterator(ewm,10);
	  try 
	    {
	      iterator.setOutputFile("test.mcmc");
	    }
	  catch(AnError& e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << "test.mcmc"
		   << "') Using stdout instead.\n";
	    }
	  time_t t0 = time(0);
	  clock_t ct0 = clock();
	  iterator.iterate(100000, 100);
	  
	  time_t t1 = time(0);    
	  
	  clock_t ct1 = clock();
	  cout << "Wall time: " << difftime(t1, t0) << " s"
	       << endl
	       << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	       << endl;
	  
	  cerr << ewm.getAcceptanceRatio()
	       << " = acceptance ratio\n";
//  	}
//       else
// 	{
	  MCMCLengthApproximator irm(rttm, df, G);

	  // generate lengths
// 	  EdgeWeightMCMC ewm2(dm, irm);
// 	  ewm2.generateWeights();
	  
	  for(unsigned iter = 100; iter <= 1000000; iter*=10)
	    {
	      unsigned thin = max(iter/1000, 1u);
	      unsigned burnin = min(0u,iter/10u);
	      irm.setMCMCparams(iter, thin, burnin, false);
	      Probability p;
	      Probability sum = 0;
	      for(unsigned i = 0; i < 10; i++)
		{
		  irm.update();
		  p = irm.calculateDataProbability();
		  sum += p;
		  cerr << iter << "\t" << p << endl;
		}
	      cerr << "mean \t" << sum/10 << endl<<endl;
	    }
// 	}
      
    }
  catch(AnError& e)       // AnError or an exception has been thrown
    {
      cout <<" Error\n";    // tell user about it
      e.action();     
    }
  catch(exception& e)       // AnError or an exception has been thrown
    {
      cout <<" Exception\n"     // tell user about it
	   << e.what();     
    }
}
