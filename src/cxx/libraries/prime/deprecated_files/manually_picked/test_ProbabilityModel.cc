#include <iostream>

#include "AnError.hh"
#include "ProbabilityModel.hh"
#include "EdgeRateModel.hh"

namespace beep
{
  class Dummy :public ProbabilityModel
  {
  public: 
    Dummy()
      : ProbabilityModel("Dummy")
    {};
    Dummy(std::string name)
      : ProbabilityModel(name)
    {};
    Probability calculateDataProbability(){return 1.0;};
    void update() {return;};

  };
}//end namespce beep

int
main()
{
  using namespace beep;
  using namespace std;
  try
    {
      Dummy p0;
      cout << "Testing default constructor: \n" << p0;
      Dummy p1("EgetNamn");
      cout << "Testing constructor('EgetNamn'): \n" << p1;
      Dummy p2(p0);
      cout << "Testing copy constructor: \n" << p2;
      Dummy p3 = p0;
      cout << "Testing assignment operator: \n" << p3;
    }
  catch(AnError e)
    {
      e.action();
    }

}
