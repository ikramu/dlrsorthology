#ifndef INTEGRALS_HH
#define INTEGRALS_HH
#include "Beep.hh"

//-----------------------------------------------------------
//
// This file contains approximative functions for 
// numerical integration. The functions use a functor
// (function object) that can act as a link between 
// the integrating function and a (class member)integrand
// function and allows for variable number of parameters
// in the integrand function.
//
// Example of functor usage with SimpsonRule()
//
// class A
// { /*...*/
//   //Interface for intergration
//   Integrate()
//   { Real r = 5.0, lBnd = 0, uBnd = 0;
//     unsigned nIntervals = 10;
//     ExampleFunctor ef(*this, r);
//     SimpsonRule(lBnd, uBnd, nIntervals, ef); }
//   //Integrand function
//   intergrand(Real lBnd, Real r)
//   { return lBnd + r + attr; }
//   //Attributes
//   Real attr;
// }
//     
// class ExampleFunctor
// {
// public:
//   //Constructor
//   ExampleFunctor(A& a, Real r)
//     : a(a), r(r) {}
//   //Interface
//   Real operator() (Real t) 
//   { return a.integrand(t, r); }
//   //Attributes
//   A& a;
//   Real r;
// }
//
//-----------------------------------------------------------


namespace beep
{
//-----------------------------------------------------------
//
// Numerical integration with the Simpson rule and a functor
//
//-----------------------------------------------------------
template <class S, class T >
S simpsonRule(const Real& lBnd, const Real& uBnd, 
	      const unsigned nIntervals, T& functor)
{
  Real h = (uBnd - lBnd) / nIntervals;
  S sum;
  int coeff = 2;

  sum = functor(lBnd);  

  for (unsigned iter = 1; iter < nIntervals; iter++)
    {
      coeff = 6 - coeff;
      sum += coeff * functor(lBnd + h * iter);
    }
  sum += functor(uBnd);

  return (h * sum / 3.0);
}

//-----------------------------------------------------------
//
// Numerical integration with the adaptive Simpson rule.
//
//-----------------------------------------------------------
template <class S, class T>
S adaptiveSimpson(const Real& lBnd, const Real& uBnd, 
		  const S& epsilon, T& functor)
{
  double h = uBnd - lBnd;
  S sum = 0;
  S sumTot = 0;
  S sumRatio = 1;
  int iterations = 0;

  if ((lBnd > 0.0) && (uBnd < 1.0))
    {
      sum = (functor(lBnd) + functor(uBnd)) * h;
    }
  sum = sum / 2.0;
  
  // Loop until we reach the error level specified by epsilon
  while (sumRatio > epsilon){
    iterations++;
    S sum_m = functor(lBnd + h * 0.5);
    for (int iter = 2; h * (iter - 0.5) < uBnd; iter++)
      {
	sum_m += functor(lBnd+h*(iter-0.5));
      }
    sum_m *= h;
    sumTot = (sum + 2.0 * sum_m) / 3.0;
    h = h / 2;
    if (sum > sum_m)
      {
	sumRatio = (sum - sum_m) / sumTot;
      }
    else if (sum < sum_m)
      { 
	sumRatio = (sum_m - sum) / sumTot;
      }
    else
      {
	sumRatio = 1.0;
      }
    sum = (sum + sum_m) / 2.0;
  }
  return(sumTot);
};


//-----------------------------------------------------------
//
// Numerical integration with Newton-Cotes rule (open type).
//
//-----------------------------------------------------------
template <class S, class T>
S NewtonCotes(const Real& lBnd, const Real& uBnd, 
	      const unsigned& nIntervals, T& functor)
{
  double h = (uBnd - lBnd) / (nIntervals + 1.0);
  int rule = (int)nIntervals;
  S sum = 0;

  switch (rule)
    {
    case 2:
      {
	sum = 3.0 * h / 2.0 * (functor(lBnd +h) + functor(lBnd + 2 * h));
	break;
      }
    case 3:
      {
	sum = 2.0 * (functor(lBnd + h) + functor(lBnd + 3 * h));
	sum = 4.0 * h / 3.0 * (sum - functor(lBnd + 2 * h));
	break;
      }
    case 4:
      {
	sum = 11.0 * (functor(lBnd + h) + functor(lBnd + 4 * h));
	sum = 5.0 * h / 24.0 * (sum + functor(lBnd + 2* h) + 
				functor(lBnd + 3 * h));
	break;
      }
    case 5:
      {
	sum = 11.0 * (functor(lBnd + h) + functor(lBnd + 5 * h));
	sum += 26.0 * functor(lBnd + 3 * h);
	sum = 6.0 * h / 20.0 * (sum - 14.0 * (functor(lBnd + 2 * h) + 
					      functor(lBnd + 4 * h)));
	break;
      }
    case 6:
      {
	sum = 611.0 * (functor(lBnd + h) + functor(lBnd + 6 * h));
	sum += 562.0 * (functor(lBnd + 3 * h) + functor(lBnd + 4 * h));
	sum = 7.0 * h / 1440.0 * (sum - 453.0 * (functor(lBnd + 2 * h) +
						 functor(lBnd + 5 * h)));
	break;
      }
    default:
      {
	sum = 1.0;
	break;
      }
    }
  return(sum);
};


// //-----------------------------------------------------------
// //
// // Numerical integration with the Simpson rule.
// //
// //-----------------------------------------------------------
// template <class S, class T , S f(double b, double, double, double, int, T)>
// S simpsonRule(double lBnd, double uBnd, int nIntervals, double bRate, double dRate, double beta, int c, T D)
// {
//   double h = (uBnd-lBnd)/nIntervals;
//   S sum = 0;

//   if (lBnd >0)
//     sum = f(lBnd,bRate,dRate,beta,c,D)*0.5;
//   sum = f(lBnd+h,bRate,dRate,beta,c,D);
//   for (int iter=2;iter<nIntervals;iter++)
//     sum = sum+f(lBnd+h*iter,bRate,dRate,beta,c,D);

//   if (uBnd < 1)
//     sum = sum+f(uBnd,bRate,dRate,beta,c,D)*0.5;

//   S sum_m =f(lBnd+h*0.5,bRate,dRate,beta,c,D);
//   for (int iter=2;iter<=nIntervals;iter++)
//     sum_m = sum_m+f(lBnd+h*(iter-0.5),bRate,dRate,beta,c,D);

//   return (h*(sum+2.0*sum_m)/3.0);
// }

// //-----------------------------------------------------------
// //
// // Numerical integration with the adaptive Simpson rule.
// //
// //-----------------------------------------------------------
// template <class S, class T , S f(double,double,double,double,int,T)>
// S adaptiveSimpson(double lBnd,double uBnd,double bRate,double dRate,double beta,int c,T D,T epsilon)
// {
//   double h = uBnd-lBnd;
//   S sum = 0;
//   S sumTot = 0;
//   S sumRatio = 1;
//   int iterations = 0;

//   if ((lBnd >0.0) & (uBnd < 1.0))
//     sum = (f(lBnd,bRate,dRate,beta,c,D)+f(uBnd,bRate,dRate,beta,c,D))*h;
//   sum = sum/2.0;
  
//   // Loop until we reach the error level specified by epsilon
//   while (sumRatio > epsilon){
//     iterations++;
//     S sum_m = f(lBnd+h*0.5,bRate,dRate,beta,c,D);
//     for (int iter=2;h*(iter-0.5) <uBnd;iter++)
//       sum_m = sum_m+f(lBnd+h*(iter-0.5),bRate,dRate,beta,c,D);
//     sum_m = sum_m*h;
//     sumTot = (sum+2.0*sum_m)/3.0;
//     h = h/2;
//     if (sum > sum_m)
//       sumRatio = (sum-sum_m)/sumTot;
//     else if (sum < sum_m) 
//       sumRatio = (sum_m-sum)/sumTot;
//     else
//       sumRatio = 1.0;
//     sum = (sum+sum_m)/2.0;
//   }
//   return(sumTot);
// };
// //-----------------------------------------------------------
// //
// // Numerical integration with Newton-Cotes rule (open type).
// //
// //-----------------------------------------------------------template <class S, class T , S f(double,double,double,double,int,T)>
// S NewtonCotes(double lBnd,double uBnd,unsigned nIntervals,double bRate,double dRate,double beta,int c,T D)
// {
//   double h = (uBnd-lBnd)/(nIntervals+1.0);
//   int rule = (int)nIntervals;
//   S sum=0;

//   switch (rule){
//   case 2:
//     sum = 3.0*h/2.0*(f(lBnd+h,bRate,dRate,beta,c,D)+f(lBnd+2*h,bRate,dRate,beta,c,D));
//     break;
//   case 3:
//     sum = 2.0*(f(lBnd+h,bRate,dRate,beta,c,D)+f(lBnd+3*h,bRate,dRate,beta,c,D));
//     sum = 4.0*h/3.0*(sum-f(lBnd+2*h,bRate,dRate,beta,c,D));
//     break;
//   case 4:
//     sum = 11.0*(f(lBnd+h,bRate,dRate,beta,c,D)+f(lBnd+4*h,bRate,dRate,beta,c,D));
//     sum = 5.0*h/24.0*(sum+f(lBnd+2*h,bRate,dRate,beta,c,D)+f(lBnd+3*h,bRate,dRate,beta,c,D));
//     break;
//   case 5:
//     sum = 11.0*(f(lBnd+h,bRate,dRate,beta,c,D)+f(lBnd+5*h,bRate,dRate,beta,c,D));
//     sum = sum+26.0*f(lBnd+3*h,bRate,dRate,beta,c,D);
//     sum = 6.0*h/20.0*(sum- 14.0*(f(lBnd+2*h,bRate,dRate,beta,c,D)+f(lBnd+4*h,bRate,dRate,beta,c,D)));
//     break;
//   case 6:
//     sum = 611.0*(f(lBnd+h,bRate,dRate,beta,c,D)+f(lBnd+6*h,bRate,dRate,beta,c,D));
//     sum = sum+562.0*(f(lBnd+3*h,bRate,dRate,beta,c,D)+f(lBnd+4*h,bRate,dRate,beta,c,D));
//     sum = 7.0*h/1440.0*(sum-453.0*(f(lBnd+2*h,bRate,dRate,beta,c,D)+f(lBnd+5*h,bRate,dRate,beta,c,D)));
//   default:
//     Probability sum(1.0);
//     break;
//   }
//   return(sum);
//};

}//end namespace beep
#endif
