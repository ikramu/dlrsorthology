#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <cmath>

#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "MatrixTransitionHandler.hh"
#include "ReconciliationTimeMCMC.hh"
#include "ReconciliationTimeSampler.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SimpleMCMC.hh"
#include "SiteRateHandler.hh"
#include "SubstitutionMCMC.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <Tree> <Sequence data> <model> <n_cat> <alpha> <n_iter> <thin>\n"
       << "\n"
       << "Parameters:\n"
       << "   <model>            a string, 'JC69', 'UniformAA', 'JTT', \n"
       << "                      'UniformCodon', must match datatype\n"
       << "   <nCat>             an int, number of rate classes in discrete\n"
       << "                      gamma model of site rates\n"
       << "   <alpha>            a double, the shape parameter for the gamma\n"
       << "   <n-iter>           an unsigned, gives the number of MCMC\n" 
       << "                      iterations to perform \n"
       << "   <thin>             and int, thinning of MCMC\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 8) 
    {
      usage(argv[0]);
    }
  try
    {
      cerr << "Create TransitionHandler\n"
	   << "-------------------------------------------\n";
      string model = argv[3];
      MatrixTransitionHandler Q = MatrixTransitionHandler::create(model);


      //Get tree and Data
      //---------------------------------------------
      TreeIO io = TreeIO::fromFile(string(argv[1]));
      Tree G = io.readHostTree();
      SequenceData D = SeqIO::readSequences(string(argv[2]));

      cerr << "Create SiteRateHandler\n"
	   << "-------------------------------------------\n";
      DummyMCMC dm;
      unsigned nCat= atoi(argv[4]);
      UniformDensity ud(0, 100, true);
      ConstRateMCMC alpha(dm, ud, G, "alpha"); // TODO: this is somewhat clumsy /bens
      SiteRateHandler srh(nCat, alpha);
      srh.setAlpha(atof(argv[5]));

      cerr << "Create EdgeWeightHandler using EgdeRateMCMC \n"
	   << "with iidRateModel and U(0, 1) density\n"
	   << "-------------------------------------------\n";
      UniformDensity df(0, 1, true);

#ifdef PERTURBED_NODE
      Tree S = Tree::EmptyTree();
      S.setName("S");
      //set up a gamma
      StrStrMap gs;
      // Map all leaves of G to the single leaf in S
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  gs.insert(G.getNode(i)->getName(), S.getRootNode()->getName());
	}
      LambdaMap lambda(G, S, gs);
      GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda); 
      BirthDeathProbs bdm(S, 0.3, 0.4);
      ReconciliationTimeMCMC rtm(alpha, G, bdm, gamma, "EdgeTimes");
      ReconciliationTimeSampler sampler(G, bdm, gamma);
      sampler.sampleTimes();
      iidRateMCMC rates(rtm, df, G, "edgeRates_G");
#else
      iidRateMCMC rates(alpha, df, G, "edgeRates_G");
#endif

      rates.generateRates();

#ifdef PERTURBED_NODE
      EdgeTimeRateHandler weights(rates);
#else
      EdgeWeightHandler weights(rates);
#endif

      cerr << "Create SubstitutionMCMC\n"
	   << "-------------------------------------------\n";
      vector<string> partitionList ;  
      partitionList.push_back(string("all"));
      SubstitutionMCMC sm(rates, D, G, srh, Q, weights, partitionList);

      cerr << sm;


      cerr << "\nCreate simpleMCMC\n" 
	   << "-----------------------------\n";
      unsigned n_iter = atoi(argv[6]);
      unsigned thin = atoi(argv[7]);

      SimpleMCMC iterator(sm, thin);
      
      cerr << "\nTest simpleMCMC.iterate(),\n"
	   << "i.e., running an mcmc\n" 
	   << "-----------------------------\n";
       // Perform and time the Likelihood calculation
      time_t t0 = time(0); 
      clock_t ct0 = clock();

      iterator.iterate(n_iter);

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;

#ifdef PERTURBED_NODE
      cout <<G.print(true,true,true,true);
      sm.calculateDataProbability();
      PRNG rand = sm.getPRNG();

      cout << "\n\nRandomly perturb a node, n, and check if \n"
	   << "calculateDataProbability() and calculateDataProbability(n) \n"
	   << "gives same result:\n";
      for(unsigned i = 0; i < 10000; i++)
	{
	  rates.suggestOwnState();
	  weights.update();
	  Probability A = sm.updateDataProbability();
// 	  cout << "updateDataProbability() = "
// 	       << A << "\n";
	  Probability B = sm.calculateDataProbability();
// 	  cout << "calculateDataProbability() = "
// 	       << B
// 	       << endl;
	  Probability C = sm.SubstitutionMCMC::calculateDataProbability();
// 	  cout << "calculateDataProbability() = "
// 	       << C
// 	       << endl;
	  if(1.0-abs((A/B).val()) > 0.0001)
	    {
	      cerr << "DIFF TOO BIG: "
		   << abs((A/B).val())
		   << "\n";
	      exit(1);
	    }
// 	  if(rand.genrand_real1() > 0.5)
// 	    {
	      rates.commitOwnState();
// 	    }
// 	  else
// 	    {
// 	      rates.discardOwnState();
// 	    }
	}
      cout << "... and they apparently do!\n";

#endif

    }
  catch(AnError e)
    {
      cerr <<" error\n";
      e.action();
    }
}

