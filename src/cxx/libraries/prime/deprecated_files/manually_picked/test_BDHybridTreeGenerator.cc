#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "Beep.hh"
#include "PRNG.hh"
#include "BDHybridTreeGenerator.hh"
#include "HybridTree.hh"
#include "HybridTreeIO.hh"

// Global options with default settings
//------------------------------------
int nParams = 0;

double lambda = 1.0;
double mu = 1.0;
double rho = 0;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < nParams+1) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = 1;
      // Read arguments
      //---------------------------------------------
      if(opt+2 < argc)
	{
	  lambda = atof(argv[opt++]);
	  mu = atof(argv[opt++]);
	  rho = atof(argv[opt++]);
	}
      PRNG R;
      if(opt < argc)
	{
	  R.setSeed(atof(argv[opt++]));
	}
      cerr << "Using seed = " << R.getSeed() << endl;

      // Set up the model for the species tree
      //---------------------------------------------------------
      BDHybridTreeGenerator bdg(lambda, mu, rho);
      
      HybridTree G;
      while(bdg.generateHybridTree(G) == false);
      cerr << G.print(false,true,false, false);
      cerr << G.getBinaryTree();
//       StrStrMap gs =  bdg.exportGS();
//       GammaMap gamma = bdg.exportGamma();
      cout << HybridTreeIO::writeHybridTree(G, true, false, true, false, false, 0)//&gamma)
	   << endl;;
    }
  catch (AnError& e)
    {
      cerr << "error found\n";
      e.action();
    }
  catch (exception& e)
    {
      cerr << "exception found\n"
	   <<e.what()
	   << endl;
    }
}


void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " <STree> [<birthRate> <deathRate>]\n"
    << "\n"
    ;
  exit(1);
}

