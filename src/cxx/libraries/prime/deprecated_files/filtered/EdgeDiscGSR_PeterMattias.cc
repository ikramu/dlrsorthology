//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A (DIRTY) SNAPSHOT OF A MERGED VERSION OF THE "ORIGINAL" GSR
// IMPLEMENTATION AND PETER/MATTIAS' EXTENDED IMPLEMENTATION FROM TRUNK AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

//#include <vector>
//#include <cassert>
//#include <math.h>
//
//#include "AnError.hh"
//#include "EdgeDiscPtMaps.hh"
//#include "EdgeDiscBDProbs.hh"
//#include "EdgeDiscGSR.hh"
//#include "EdgeDiscPtMapIterator.hh"
//#include "EdgeDiscTree.hh"
//#include "Node.hh"
//#include "Probability.hh"
//#include "StrStrMap.hh"
//#include "TreeIO.hh"
//#include "TreePerturbationEvent.hh"
//#include "PRNG.hh"
//
//int state = 0;
//
//namespace beep
//{
//
//using namespace std;
//
//EdgeDiscGSR::EdgeDiscGSR(
//		Tree& G,
//		EdgeDiscTree& DS,
//		StrStrMap& GSMap,
//		Density2P& edgeRateDF,
//		EdgeDiscBDProbs& BDProbs,
//		UnsignedVector* fixedGNodes) :
//		EdgeWeightModel(),
//		PerturbationObserver(),
//		m_G(G),
//		m_DS(DS),
//		m_GSMap(GSMap),
//		m_edgeRateDF(edgeRateDF),
//		m_BDProbs(BDProbs),
//		m_sigma(G, DS.getTree(), GSMap),
//		m_lengths(NULL),
//		m_fixedGNodes(fixedGNodes),
//		m_loLims(G),
//		m_upLims(G),
//		m_ats(G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
//		m_belows(G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
//		m_at_bars(G, ProbabilityEdgeDiscPtMap(DS, 0.0)),
//		m_calculatedAtBars(false),
//		m_calculatedAtAndBelow(false),
//		m_rateDelta(0.0)
//		{
//	// Set G lengths, creating new if they don't exist.
//	if (G.hasLengths())
//	{
//		m_lengths = &G.getLengths();
//	}
//	else
//	{
//		// G itself responsible for destruction of lengths.
//		m_lengths = new RealVector(G.getNumberOfNodes(), edgeRateDF.getMean());
//		G.setLengths(*m_lengths, true);
//	}
//
//	//        joelgs: Commenting out the below section since it proved a bit too demanding.
//	//        In short, it requires the number of discretization points of an edge
//	//        (X,parent(X)) in S to be roughly as large or larger than |{u in V(G): sigma(u)=X}|.
//	//
//	//        /* Check that the discretized tree has a sufficient number of
//	//         * discretization points*/
//	//        std::vector< std::pair<Node *, int> > badEdges(0);
//	//        if (!sufficientDiscPoints(badEdges))
//	//        {
//	//            std::vector< std::pair<Node *, int> >::iterator badEdges_it;
//	//
//	//            ostringstream oss;
//	//            oss << "EdgeDiscGSR: Insufficient number of disc points on edges:" << endl;
//	//            oss << "Edge:\tHas:\tNeeds:\t" << endl;
//	//
//	//            /* For each edge which needs more discretization points */
//	//            for (badEdges_it = badEdges.begin(); badEdges_it != badEdges.end(); badEdges_it++)
//	//            {
//	//                std::pair<Node *, int> badEdge = (*badEdges_it);
//	//                int edgeNumber = badEdge.first->getNumber();
//	//
//	//                int currentDiscPoints = m_DS.getNoOfPts(badEdge.first) - 1;
//	//                if (badEdge.first->isRoot())
//	//                {
//	//                    // Do not include the tip point
//	//                    currentDiscPoints--;
//	//                }
//	//
//	//                int requiredDiscPoints = (*badEdges_it).second;
//	//
//	//                oss << edgeNumber << "\t\t" << currentDiscPoints << "\t"
//	//                        << requiredDiscPoints << endl;
//	//            }
//	//            throw AnError(oss.str());
//	//        }
//
//	// Set rate delta to a quarter of smallest timestep (for instance).
//	m_rateDelta = DS.getMinTimestep() / 4;
//
//	// Compute values to have something to start with.
//	updateHelpStructures();
//	updateProbsFull();
//
//	// Register as listener on parameter holders we depend on.
//	m_G.addPertObserver(this);
//	m_BDProbs.addPertObserver(this);
//	m_edgeRateDF.addPertObserver(this);
//	//m_DS.getTree().addPertObserver(this); // Host tree not supported yet!
//
//	// Maybe this should be used for the range instead? // fmattias
//	//        Real rateMin, rateMax;
//	//        m_edgeRateDF.getRange(rateMin, rateMax);
//	//        m_low = rateMin*m_DS.getMinTimestep();
//	//        m_high = rateMax*m_DS.getTree().getTopToLeafTime();
//}
//
//
//EdgeDiscGSR::~EdgeDiscGSR()
//{
//}
//
//
//const Tree&
//EdgeDiscGSR::getTree() const
//{
//	return m_G;
//}
//
//const EdgeDiscBDProbs &
//EdgeDiscGSR::getBDProbs() const
//{
//	return m_BDProbs;
//}
//
//
//unsigned
//EdgeDiscGSR::nWeights() const
//{
//	// Top time edge is never perturbed.
//	return (m_G.getNumberOfNodes() - 1);
//}
//
//
//RealVector&
//EdgeDiscGSR::getWeightVector() const
//{
//	return *m_lengths;
//}
//
//
//Real
//EdgeDiscGSR::getWeight(const Node& node) const
//{
//	return (*m_lengths)[node];
//}
//
//
//void
//EdgeDiscGSR::setWeight(const Real& weight, const Node& u)
//{
//	(*m_lengths)[u] = weight;
//}
//
//
//void
//EdgeDiscGSR::getRange(Real& low, Real& high)
//{
//	//      low = m_low;
//	//      high = m_high;
//	m_edgeRateDF.getRange(low, high);
//}
//
//EdgeDiscTree &
//EdgeDiscGSR::getDiscretizedHostTree()
//{
//	return m_DS;
//}
//
//StrStrMap &
//EdgeDiscGSR::getGSMap()
//{
//	return m_GSMap;
//}
//
//string
//EdgeDiscGSR::print() const
//{
//	ostringstream oss;
//	oss << "The rate probabilities are modeled using a \n"
//			<< m_edgeRateDF.print();
//	return oss.str();
//}
//
//
//void
//EdgeDiscGSR::perturbationUpdate(const PerturbationObservable* sender, const PerturbationEvent* event)
//{
//	// We have these scenarios:
//	// A) Perturbation event, where sender==
//	//      DS (topology or time change)          =>  not allowed at the moment!
//	//      G, lacking info                       =>  full update.
//	//      G, detailed info on topology change   =>  full update (partial update is tricky).
//	//      G, detailed info on length change     =>  partial update.
//	//      BDProbs (birth/death rate change)     =>  full update.
//	//      rateDF (edge rate change)             =>  full update.
//	// B) Restoration event                       =>  restore cached data.
//	//
//	// We always do a full update on help structures. HOWEVER: In case of a restoration,
//	// we do it afterwards to make sure we restore exactly cached parts. This latter update
//	// is not really necessary, but might cause confusion otherwise.
//	//
//	// TODO: If host tree is perturbed (i.e. topology, times, discretization)
//	// we need to recreate all data structures and then do a full update.
//
//	// Occasionally, we use this counter to replace partial update with full one
//	// to reduce risk of accumulated numeric error drift, etc.
//	static long iter = 0;
//
//	// We may have access to details if sender is G.
//	const TreePerturbationEvent* details = dynamic_cast<const TreePerturbationEvent*>(event);
//
//	// Restore or recompute probabilities depending on event.
//	// If G is sender, look for detailed info to make optimized update.
//	if (event != NULL && event->getType() == PerturbationEvent::RESTORATION)
//	{
//		restoreCachedProbs();
//		updateHelpStructures();  // After restoring! See also comment above.
//	}
//	else if (iter % 20 != 0 && sender == &m_G && details != NULL &&
//			details->getTreePerturbationType() == TreePerturbationEvent::EDGE_WEIGHT)
//	{
//		updateHelpStructures();
//
//		// Since we only allow partial updates at edge weight events (and the two root
//		// child edges are considered separate entities) we only consider the first root path.
//		// p2 may contain a node, but its weight should (and must) be untouched.
//		const Node* p1;
//		const Node* p2;
//		details->getRootPaths(p1, p2);
//		cacheProbs(p1);
//		updateProbsPartial(p1);
//	}
//	else
//	{
//		updateHelpStructures();
//
//		cacheProbs(NULL);
//		updateProbsFull();
//	}
//	++iter;
//}
//
//
//void
//EdgeDiscGSR::update()
//{
//}
//
//
//void
//EdgeDiscGSR::updateHelpStructures()
//{
//	// Note: Order of invokation matters.
//	m_calculatedAtBars = false;
//	m_sigma.update(m_G, m_DS.getTree());
//	const Node* uRoot = m_G.getRootNode();
//	updateLoLims(uRoot);
//	updateUpLims(uRoot);
//}
//
//
//void
//EdgeDiscGSR::updateLoLims(const Node* u)
//{
//	const Node* sigma = m_sigma[u];
//
//	if (u->isLeaf())
//	{
//		m_loLims[u] = Point(sigma, 0);
//	}
//	else
//	{
//		const Node* lc = u->getLeftChild();
//		const Node* rc = u->getRightChild();
//
//		// Update children first.
//		updateLoLims(lc);
//		updateLoLims(rc);
//
//		Point lcLo = m_loLims[lc];
//		Point rcLo = m_loLims[rc];
//
//		/* Set the lowest point at the left child to begin with */
//		Point lo(lcLo.first, lcLo.second + 1);
//
//		/* Start at the left child */
//		const Node* curr = lcLo.first;
//
//		/* Start at the lowest placement of the left child and move
//		 * on the path from u towards the root.  */
//		while (curr != NULL)
//		{
//			/* If we are at sigma(u) and we haven't marked it as
//			 * the lowest point of u, do so. */
//			if (curr == sigma && lo.first != sigma)
//			{
//				lo = Point(sigma, 0);
//			}
//
//			/* If we are at the same lowest edge as the right child */
//			if (curr == rcLo.first)
//			{
//				if (lo.first == curr)
//				{
//					/* u also has this edge as its lowest point */
//					lo.second = std::max(lo.second, rcLo.second + 1);
//				}
//				else
//				{
//					/* The right child is higher up in the tree
//					 *  than the left child  */
//					lo = Point(rcLo.first, rcLo.second + 1);
//				}
//			}
//
//			curr = curr->getParent();
//		}
//
//		// If we have moved outside edge's points, choose next pure disc. pt.
//		if (lo.second == m_DS.getNoOfPts(lo.first))
//		{
//			lo = Point(lo.first->getParent(), 1);
//			if (lo.first == NULL)
//			{
//				throw AnError("Insufficient no. of discretization points (errtype 3).\n"
//        				      "Try using denser dicretization for 1) top edge, 2) remaining vertices.", 1);
//			}
//		}
//		m_loLims[u] = lo;
//	}
//}
//
//
//void
//EdgeDiscGSR::updateUpLims(const Node* u)
//{
//	const Node* sigma = m_sigma[u];
//
//	if (u->isLeaf())
//	{
//		m_upLims[u] = Point(sigma, 0);
//	}
//	else if (m_fixedGNodes != NULL && (*m_fixedGNodes)[u])
//	{
//		if (sigma == m_sigma[u->getLeftChild()] || sigma == m_sigma[u->getRightChild()])
//		{
//			throw AnError("EdgeDiscGSR::updateUpLims: Cannot fix node in EdgeDiscGSR.", 1);
//		}
//		m_upLims[u] = Point(sigma, 0);
//	}
//	else if (u->isRoot())
//	{
//		// We disallow placement on very tip.
//		m_upLims[u] = m_DS.getTopmostPt();
//		--(m_upLims[u].second);
//	}
//	else
//	{
//		// Normal case: set u's limit just beneath parent's limit.
//		Point pLim = m_upLims[u->getParent()];
//		if (pLim.second >= 2)
//		{
//			/* There is a disc point available below the parent. */
//			m_upLims[u] = Point(pLim.first, pLim.second - 1);
//		}
//		else if (pLim.second == 1 && pLim.first == sigma)
//		{
//			/* We place u at speciation since upper and lower limits here coincide with sigma. */
//			m_upLims[u] = Point(pLim.first, 0);
//		}
//		else
//		{
//			// We can't place u below its sigma.
//			if (sigma == pLim.first)
//			{
//				throw AnError("Insufficient no. of discretization points (errtype 1).\n"
//        				       "Try using denser dicretization for 1) top edge, 2) remaining vertices.", 1);
//			}
//
//			// No disc point available on this edge; find the edge below.
//			const Node* n = sigma;
//			while (n->getParent() != pLim.first)
//			{
//				n = n->getParent();
//			}
//			m_upLims[u] = Point(n, m_DS.getNoOfPts(n) - 1);
//		}
//	}
//
//	// Catch insufficient discretizations.
//	if ((m_loLims[u].first == m_upLims[u].first && m_loLims[u].second > m_upLims[u].second)
//			|| (m_loLims[u].first == m_upLims[u].first->getParent()))
//	{
//		throw AnError("Insufficient no. of discretization points (errtype 2).\n"
//        		       "Try using denser dicretization for 1) top edge, 2) remaining vertices.", 1);
//	}
//
//	// Update children afterwards.
//	if (!u->isLeaf())
//	{
//		updateUpLims(u->getLeftChild());
//		updateUpLims(u->getRightChild());
//	}
//}
//
//
//Probability
//EdgeDiscGSR::calculateDataProbability()
//{
//	return m_belows[m_G.getRootNode()].getTopmost();
//	// POTENTIAL VARIANT: We divide density with p11 for entire host
//	// tree top edge so as to reduce its effect on BD rate scaling!
//	//return (m_belows[m_G.getRootNode()].getTopmost() / m_BDProbs.getOneToOneProb(m_DS.getRootNode()));
//}
//
//
//void
//EdgeDiscGSR::updateProbsFull()
//{
//	// Full recursive update.
//	updateAtProbs(m_G.getRootNode(), true);
//	m_calculatedAtAndBelow = true;
//}
//
//
//void
//EdgeDiscGSR::updateProbsPartial(const Node* rootPath)
//{
//	// We currently only support partial updates for edge weight
//	// changes. Therefore, we only consider partial caching along a
//	// single root path.
//
//	// Do a non-recursive update along the changed root path.
//	while (rootPath != NULL)
//	{
//		updateAtProbs(rootPath, false);
//		rootPath = rootPath->getParent();
//	}
//}
//
//
//bool
//EdgeDiscGSR::sufficientDiscPoints(std::vector< std::pair< Node *, int > > &badEdges)
//{
//	Tree::const_iterator S_it;
//	const Tree &S = m_DS.getTree();
//	bool sufficientNumberOfPoints = true;
//
//	/* Loop through each edge e in S and count the number nodes in G which have
//	 * have sigma on e */
//	for(S_it = S.begin(); S_it != S.end(); ++S_it)
//	{
//		Node *edge = *S_it;
//
//		/* Count the number of nodes that have sigma on the current edge */
//		int numNodesWithCurrentSigma = 0;
//		Tree::const_iterator G_it;
//		for(G_it = m_G.begin(); G_it != m_G.end(); ++G_it)
//		{
//			Node *u = *G_it;
//			if(m_sigma[u]->getNumber() == edge->getNumber())
//			{
//				numNodesWithCurrentSigma++;
//			}
//		}
//
//		int numDiscPointsOnEdge;
//		if(!edge->isRoot())
//		{
//			// Remove speciation disc point
//			numDiscPointsOnEdge = m_DS.getNoOfPts(edge) - 1;
//		}
//		else
//		{
//			// Remove speciation and tip points
//			numDiscPointsOnEdge = m_DS.getNoOfPts(edge) - 2;
//		}
//
//		// Check sufficient condition for the required number of disc points,
//		// Let d(x) be the number of disc points on the edge (x, parent(x)) then
//		// each edge x must satisfy d(x) >= |{ v in V(G) | sigma(v) = x }| - 1
//		if(numDiscPointsOnEdge < numNodesWithCurrentSigma - 1){
//			std::pair<Node *, int> badEdge;
//			// Iterator holds copy of edge so we need to get the real pointer
//			badEdge.first = S.getNode(edge->getNumber());
//			badEdge.second = numNodesWithCurrentSigma - 1;
//
//			sufficientNumberOfPoints = false;
//			badEdges.push_back(badEdge);
//		}
//	}
//	return sufficientNumberOfPoints;
//}
//
//
//void
//EdgeDiscGSR::updateAtProbs(const Node* u, bool doRecurse)
//{
//	if (u->isLeaf())
//	{
//		m_ats[u](m_loLims[u]) = Probability(1.0);
//	}
//	else
//	{
//		const Node* lc = u->getLeftChild();
//		const Node* rc = u->getRightChild();
//
//		// Must do children first, if specified.
//		if (doRecurse)
//		{
//			updateAtProbs(lc, true);
//			updateAtProbs(rc, true);
//		}
//
//		// Retrieve placement bounds. End is here also valid.
//		EdgeDiscTreeIterator x = m_DS.begin(m_loLims[u]);
//		EdgeDiscTreeIterator xend = m_DS.begin(m_upLims[u]);
//
//		// For each valid placement x.
//		while (true)
//		{
//			EdgeDiscretizer::Point xPt = x.getPt();
//			m_ats[u](x) = m_belows[lc](x) * m_belows[rc](x) * duplicationFactor(xPt);
//			if (x == xend)
//			{
//				break;
//			}
//			x.pp();
//		}
//	}
//
//	// Update planted tree probs. afterwards.
//	updateBelowProbs(u);
//}
//
//
//void
//EdgeDiscGSR::updateBelowProbs(const Node* u)
//{
//	// x refers to point of tip of planted tree G^u.
//	// y refers to point where u is placed (strictly below x).
//
//	Real l = (*m_lengths)[u];
//
//	// Get limits for x (both are valid placements).
//	EdgeDiscTreeIterator x, xend;
//	if (u->isRoot())
//	{
//		x = xend = m_DS.end();
//	}
//	else
//	{
//		x = m_DS.begin(m_loLims[u->getParent()]);
//		xend = m_DS.begin(m_upLims[u->getParent()]);
//	}
//
//	// Get limits for y (both valid placements).
//	EdgeDiscTreeIterator y;
//	EdgeDiscTreeIterator yend = m_DS.begin(m_upLims[u]);
//
//	// For each x.
//	while (true)
//	{
//		// For each y strictly below x.
//		m_belows[u](x) = Probability(0.0);
//		for (y = m_DS.begin(m_loLims[u]); y < x; y.pp())
//		{
//			//Probability rateDens = 1.0;
//			Probability rateDens = u->isRoot() ?
//					1.0 : calcRateDensity(l, m_DS(x) - m_DS(y));
//			Probability updateFactor = rateDens * m_BDProbs.getOneToOneProb(x, y) * m_ats[u](y);
//
//			//if ( !(u->isLeaf()) )
//			//{
//			//	updateFactor *= m_DS.getTimestep(y.getPt().first);
//			//}
//			m_belows[u](x) +=  updateFactor;
//
//			if (y == yend) { break; }
//		}
//		if (x == xend) { break; }
//		x.pp();
//	}
//}
//
//
//void
//EdgeDiscGSR::cacheProbs(const Node* rootPath)
//{
//	clearAllCachedProbs();
//
//	// This mirrors full and partial updates.
//	// See partialUpdate() for more info.
//	if (rootPath == NULL)
//	{
//		// Recursively store all values.
//		cacheNodeProbs(m_G.getRootNode(), true);
//	}
//	else
//	{
//		// Store only values along path to root.
//		while (rootPath != NULL)
//		{
//			cacheNodeProbs(rootPath, false);
//			rootPath = rootPath->getParent();
//		}
//	}
//}
//
//
//void
//EdgeDiscGSR::cacheNodeProbs(const Node* u, bool doRecurse)
//{
//	m_belows[u].cachePath(m_sigma[u]);
//	if (!u->isLeaf())
//	{
//		// Note: Leaf's "ats" are never changed.
//		m_ats[u].cachePath(m_sigma[u]);
//		if (doRecurse)
//		{
//			cacheNodeProbs(u->getLeftChild(), true);
//			cacheNodeProbs(u->getRightChild(), true);
//		}
//	}
//}
//
//
//void
//EdgeDiscGSR::restoreCachedProbs()
//{
//	for (Tree::const_iterator it = m_G.begin(); it != m_G.end(); ++it)
//	{
//		m_ats[*it].restoreCachePath(m_sigma[*it]);
//		m_belows[*it].restoreCachePath(m_sigma[*it]);
//	}
//}
//
//
//void
//EdgeDiscGSR::clearAllCachedProbs()
//{
//	for (Tree::const_iterator it = m_G.begin(); it != m_G.end(); ++it)
//	{
//		m_ats[*it].invalidateCache();
//		m_belows[*it].invalidateCache();
//	}
//}
//
//
//string
//EdgeDiscGSR::getDebugInfo(bool inclAts, bool inclBelows, bool inclAtBars, bool inclPlacementProbs)
//{
//	ostringstream oss;
//	Tree::const_iterator it;
//
//	oss << "# GENERAL INFO:" << endl;
//	oss << "# Node no:\tSigma:\tLoLim:\tUpLim:\t" << endl;
//	for (it = m_G.begin(); it != m_G.end(); ++it)
//	{
//		const Node* u = (*it);
//		oss
//		<< "# " << u->getNumber() << '\t' << '\t'
//		<< m_sigma[u]->getNumber() << '\t'
//		<< '(' << m_loLims[u].first->getNumber() << ',' << m_loLims[u].second << ")\t"
//		<< '(' << m_upLims[u].first->getNumber() << ',' << m_upLims[u].second << ")\t"
//		<< endl;
//	}
//	if (inclAts)
//	{
//		oss << "# AT-PROBABILITIES:" << endl;
//		for (it = m_G.begin(); it != m_G.end(); ++it)
//		{
//			oss << "# Node " << (*it)->getNumber() << ':' << endl
//					<< m_ats[*it].printPath(m_sigma[*it]);
//		}
//	}
//	if (inclBelows)
//	{
//		oss << "# BELOW-PROBABILITIES:" << endl;
//		for (it = m_G.begin(); it != m_G.end(); ++it)
//		{
//			oss << "# Node " << (*it)->getNumber() << ':' << endl
//					<< m_belows[*it].printPath(m_sigma[*it]);
//		}
//	}
//
//	if (inclAtBars)
//	{
//		oss << "# ATBAR-PROBABILITIES:" << endl;
//		for (it = m_G.begin(); it != m_G.end(); ++it)
//		{
//			oss << "# Node " << (*it)->getNumber() << ':' << endl
//					<< m_at_bars[*it].printPath(m_sigma[*it]);
//		}
//
//	}
//
//	if (inclPlacementProbs)
//	{
//		oss << "# PLACEMENT-PROBABILITIES:" << endl;
//		for (it = m_G.begin(); it != m_G.end(); ++it)
//		{
//			oss << "# Node " << (*it)->getNumber() << " in G" << ':' << endl;
//
//			// Create an iterator that moves over all possible discretization
//			// points on which *it can be placed.
//			EdgeDiscTreeIterator uPlacement_it, uPlacementEnd;
//			uPlacement_it = m_DS.begin(m_loLims[*it]);
//			uPlacementEnd = m_DS.begin(m_upLims[*it]);
//
//			// Calculate at bar for all possible discretization points
//			for(; m_DS.isAncestor(uPlacementEnd.getPt(), uPlacement_it.getPt()); uPlacement_it.pp()){
//				Point p = uPlacement_it.getPt();
//				Node *n = *it;
//				Probability prob = getPlacementProbability( n, &p);
//				oss << "(" << uPlacement_it.getPt().first->getNumber() <<
//						", " << uPlacement_it.getPt().second << ")" <<
//						":" <<
//						prob.val() << " ";
//			}
//			oss << endl;
//		}
//	}
//
//	return oss.str();
//}
//
//Probability
//EdgeDiscGSR::getPlacementProbability(const Node *u, const Point *x)
//{
//    /**
//     * Note:
//     * 'calculateDataProbability' does not actually give a probability,
//     * it is a density.
//     *
//     * This is the probability Pr[u on x | G, l, theta, S] approximated on the
//     * small intervall dt.
//     */
//    if(u->isLeaf()){
//        if(m_loLims[u] == *x) {
//            return Probability(1.0);
//        }
//        else {
//            return Probability(0.0);
//        }
//    }
//
//    Probability placementDensity = getJointTreePlacementDensity(u, x);
//    Real timeStep = m_DS.getTimestep(x->first);
//    Probability jointTreeLengthDensity = calculateDataProbability();
//
//    return placementDensity * timeStep / jointTreeLengthDensity;
//}
//
//Probability
//EdgeDiscGSR::getJointTreePlacementDensity(const Node *u, const Point *x)
//{
//	if(!m_calculatedAtAndBelow){
//		updateProbsFull();
//	}
//
//	if(!m_calculatedAtBars){
//		calculateAtBarProbabilities();
//		m_calculatedAtBars = true;
//	}
//
//        // Check if x < lolim
//        if( !(m_DS.isAncestor(*x, m_loLims[u])) ){
//            return 0.0;
//        }
//
//        //Speciation is only allowed if it is the lowest possible placement of u
//        if ( m_DS.isSpeciation(*x) && x->first->getNumber() != m_loLims[u].first->getNumber() ){
//            return 0.0;
//        }
//
//	return m_at_bars[u](*x) * m_ats[u](*x);
//}
//
////The return value of this method should be 1. It can be used as a test.
//Probability
//EdgeDiscGSR::getTotalPlacementProbability(const Node *u)
//{
//	EdgeDiscTreeIterator x, xend;
//
//	x = m_DS.begin(m_loLims[u]);
//	xend = m_DS.begin(m_upLims[u]);
//
//	Probability totalProbability(0.0);
//
//	/* Sum probability over each placement for u */
//	for(; m_DS.isAncestor(xend.getPt(), x.getPt()); x.pp())
//	{
//		Point xPoint = x.getPt();
//		totalProbability += getPlacementProbability(u, &xPoint);
//	}
//
//	return totalProbability;
//}
//
//Probability
//EdgeDiscGSR::getTotalPlacementDensity(const Node *u)
//{
//    EdgeDiscTreeIterator x, xend;
//
//    x = m_DS.begin(m_loLims[u]);
//    xend = m_DS.begin(m_upLims[u]);
//
//    Probability totalDensity(0.0);
//
//    /* Sum probability over each placement for u */
//    for(; m_DS.isAncestor(xend.getPt(), x.getPt()); x.pp())
//    {
//            Point xPoint = x.getPt();
//
//            Real timeStep = u->isLeaf() ? 1.0 : m_DS.getTimestep(xPoint.first);
//
//            totalDensity += getJointTreePlacementDensity(u, &xPoint) * timeStep;
//    }
//
//    return totalDensity;
//}
//
//Probability
//EdgeDiscGSR::getTotalPlacementSum(const Point *x)
//{
//	Tree::const_iterator G_it, G_itEnd;
//
//	G_it = m_G.begin();
//	G_itEnd = m_G.end();
//
//	Probability totalProbability(0.0);
//
//	/* Sum probability over the placement of each u on x */
//	for(; G_it != G_itEnd; ++G_it)
//	{
//		const Node *u = *G_it;
//
//            /* Skip leaf nodes since they can only be placed at the leaves of S */
//            if(u->isLeaf()) {
//                continue;
//            }
//
//            /* Assert that u really can be placed at x */
//            if(!m_DS.isAncestor(m_upLims[u], *x) || !m_DS.isAncestor(*x, m_loLims[u])) {
//                continue;
//            }
//
//            Probability placementProb = getPlacementProbability(u, x);
//            totalProbability += placementProb;
//	}
//
//	return totalProbability;
//}
//
//void EdgeDiscGSR::setLSDProbabilities(const LSDProbs &lsdProbs)
//{
//	m_LSD = lsdProbs;
//}
//
//void
//EdgeDiscGSR::calculateAtBarProbabilities()
//{
//	// Create levels
//	std::vector< std::vector< Node * > > levels;
//	Node *root = m_G.getRootNode();
//	createLevels(root, levels);
//
//	// Calculate at bar probabilities for all root placements
//	calculateRootAtBarProbability(root);
//
//	// Initialize iterator for levels
//	std::vector< std::vector<Node*> >::iterator level_it = levels.begin();
//	level_it++; /* Skip the root placements */
//
//	// Calculate at bar for each level
//	for(; level_it != levels.end(); level_it++)
//	{
//		// Initalize iterator over vertices in the current level
//		std::vector<Node*>::iterator levelVertex_it;
//		levelVertex_it = (*level_it).begin();
//
//		// For each vertex in the level calculate abar
//		for(; levelVertex_it != (*level_it).end(); levelVertex_it++)
//		{
//			calculateNodeAtBarProbability(*levelVertex_it);
//		}
//	}
//}
//
//void
//EdgeDiscGSR::calculateRootAtBarProbability(const Node *root)
//{
//	// Get lowest and highest point of placement for the root.
//	EdgeDiscTreeIterator rootPlacement_it, rootPlacementEnd;
//	rootPlacement_it = m_DS.begin(m_loLims[ m_G.getRootNode() ]);
//	rootPlacementEnd = m_DS.end();
//
//	// Iterate through vertices in DS and calculate at bar for
//	// the root at every point
//	for(; rootPlacement_it != rootPlacementEnd; rootPlacement_it.pp())
//	{
//
//		// Calculate at bar for root
//		// abar[root][x] = p_11(origin, x)
//		m_at_bars[root](rootPlacement_it) =
//                            m_BDProbs.getOneToOneProb(rootPlacementEnd.getPt(), rootPlacement_it.getPt());
//	}
//}
//
//string EdgeDiscGSR::strRep(const Point &x)
//{
//    stringstream ss;
//    ss << "(" << x.first->getNumber() << ", " << x.second << ")";
//    return ss.str();
//}
//
//void
//EdgeDiscGSR::calculateNodeAtBarProbability(const Node *u)
//{
//	Node *parent = u->getParent();
//
//	// Length of edge between u and its parent
//	Real edgeLength = (*m_lengths)[u];
//
//	// Create an iterator that moves over all possible discretization
//	// points on which u can be placed.
//	EdgeDiscTreeIterator uPlacement_it, uPlacementEnd;
//	uPlacement_it = m_DS.begin(m_loLims[u]);
//	uPlacementEnd = m_DS.begin(m_upLims[u]);
//
//	// Calculate at bar for all possible discretization points
//	for(; m_DS.isAncestor(uPlacementEnd.getPt(), uPlacement_it.getPt()); uPlacement_it.pp())
//	{
//		Point uPlacement = uPlacement_it.getPt();
//		// Determine the placement of the parent
//		EdgeDiscTreeIterator parentPlacement_it, parentPlacementEnd, lowestParentPlacement_it;
//		lowestParentPlacement_it = m_DS.begin(m_loLims[parent]);
//		parentPlacement_it = uPlacement_it;
//		parentPlacement_it++; // Parent of u must be placed above u
//
//		// Place the parent at the highest of loLims[parent(u)] and the
//		// discretization point above uPlacement
//        if(parentPlacement_it != lowestParentPlacement_it && m_DS.isSpeciation(parentPlacement_it.getPt()) ){
//            parentPlacement_it++;
//        }
//        if(m_DS.isProperAncestor(lowestParentPlacement_it.getPt(), parentPlacement_it.getPt()))
//		{
//			parentPlacement_it = lowestParentPlacement_it;
//		}
//
//		// The highest placement of the parent of u
//		parentPlacementEnd = m_DS.begin(m_upLims[parent]);
//
//		// Iterate through all valid placements of the parent of u
//		m_at_bars[u](uPlacement) = Probability(0.0);
//		for(; m_DS.isAncestor(parentPlacementEnd.getPt(), parentPlacement_it.getPt()); parentPlacement_it.pp())
//		{
//			// Placement of the parent of u
//			Point parentPlacement = parentPlacement_it.getPt();
//
//			Real edgeTime = m_DS(parentPlacement) - m_DS(uPlacement);
//			Probability rateDens = calcRateDensity(edgeLength, edgeTime);
//
//			// Calculate at bar probability
//			m_at_bars[u](uPlacement) += duplicationFactor(parentPlacement) *
//					m_BDProbs.getOneToOneProb(parentPlacement, uPlacement) *
//					rateDens *
//					m_at_bars[parent](parentPlacement) *
//					m_belows[ u->getSibling() ](parentPlacement)*
//					m_DS.getTimestep(parentPlacement.first);
//		}
//
//	}
//}
//
//void
//EdgeDiscGSR::createLevels(Node *root, std::vector< std::vector <Node *> > &levels)
//{
//	// Create vector for the leaves
//	std::vector< Node* > leaves;
//
//	// Level 0 only contains the root.
//	levels.push_back( std::vector<Node*>(1)  );
//	levels[0][0] = root;
//
//	// Create all other levels recursively
//	int i = 1;
//	while(true)
//	{
//		std::vector<Node*>::iterator level_it;
//
//		// Create the current level
//		levels.push_back( std::vector<Node*>() );
//
//		/* Create the current level by adding all non-leaf children of the
//		 * vertices in the previous level */
//		for(level_it = levels[i-1].begin(); level_it != levels[i-1].end(); level_it++)
//		{
//			// Get the two children
//			Node *leftChild = (*level_it)->getLeftChild();
//			Node *rightChild = (*level_it)->getRightChild();
//
//			// If the child isn't a leaf then add it to the current level
//			// otherwise put it in the leaf level
//			if(!leftChild->isLeaf())
//			{
//				levels[i].push_back(leftChild);
//			}
//			else
//			{
//				leaves.push_back(leftChild);
//			}
//
//			if(!rightChild->isLeaf())
//			{
//				levels[i].push_back(rightChild);
//			}
//			else
//			{
//				leaves.push_back(rightChild);
//			}
//		}
//
//		// If the last created level is empty, we are done.
//		if(levels[i].size() == 0)
//		{
//			// Set the last level to the leaves
//			levels[i] = leaves;
//			break;
//		}
//
//		i++;
//	}
//}
//
//Probability
//EdgeDiscGSR::getLeafAtBar(const Node *u)
//{
//	assert(u->isLeaf());
//	return m_at_bars[u](m_loLims[u]);
//}
//
//Real EdgeDiscGSR::duplicationFactor(Point &x)
//        {
//            if(m_DS.isSpeciation(x)) {
//                return 1.0;
//            }
//            else {
//                Real lsdProb = m_LSD.getProbability(x).val();
//                return (1.0 - lsdProb) * 2 * m_BDProbs.getBirthRate()  * m_DS.getTimestep(x.first) + lsdProb;
//            }
////            return m_DS.isSpeciation(x) ? 1.0 : 2 * m_BDProbs.getBirthRate() *
////                                                m_DS.getTimestep(x.first);
//}
//
//string
//EdgeDiscGSR::getRootProbDebugInfo()
//{
//	ostringstream oss;
//	const Node* u = m_G.getRootNode();
//	EdgeDiscTreeIterator top = m_DS.begin(m_DS.getTopmostPt());
//	EdgeDiscTreeIterator x = m_DS.begin(m_loLims[u]);
//	while (x != top)
//	{
//		oss << (m_BDProbs.getOneToOneProb(top, x) * m_ats[u](x)) << " ";
//		x = x.pp();
//	}
//	return oss.str();
//}
//
//
//string EdgeDiscGSR::test(Real precision, bool diagnostics)
//{
//    stringstream ss;
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: ================START OF TESTS==============" << endl << endl;}
//    bool test_fail = false;
//
//    /**
//     * Test 1. Total placement probability equals (or is close to) 1.0.
//     */
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 1: Total placement probability equals 1.0 >>>>" << endl;}
//    Tree::const_iterator it;
//    for (it = m_G.begin(); it != m_G.end(); ++it){
//        if(abs(getTotalPlacementProbability(*it).val() - 1.0) > precision){
//            ss << "EdgeDiscGSR::testTotalProbabilities: Test 1 failed." << std::endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Total placement probability for node " << (*it)->getNumber() << " is: " << getTotalPlacementProbability(*it).val() << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Diff: " << abs(getTotalPlacementProbability(*it).val() - 1.0) << endl;
//            test_fail = true;
//        }
//    }
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 1: END >>>>" << endl << endl;}
//
//    /**
//     * Test 2. Total placement density equals (or is close to the) tree density.
//     */
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 2: Total placement density equals tree density >>>>" << endl;}
//    for (it = m_G.begin(); it != m_G.end(); ++it)
//    {
//        if(abs((getTotalPlacementDensity(*it) - calculateDataProbability()).val()) > precision){
//            ss << "EdgeDiscGSR::testTotalProbabilities: Test 2 failed." << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Total placement density for node " << (*it)->getNumber() << " is: " << getTotalPlacementDensity(*it).val() << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Tree density is: " << calculateDataProbability().val() << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Diff: " << abs((getTotalPlacementDensity(*it) - calculateDataProbability()).val()) << endl << endl;
//            test_fail = true;
//        }
//    }
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 2: END >>>>" << endl << endl;}
//
//    /**
//     * Test 3. Leaf abar equals (or is close to) the tree density.
//     */
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 3: Leaf abar equals tree density >>>>" << endl;}
//    for(it = m_G.begin(); it != m_G.end(); ++it) {
//        const Node *u = *it;
//        if(!u->isLeaf()) {
//            continue;
//        }
//        Real diff = calculateDataProbability().val() - getLeafAtBar(u).val();
//        if( abs(diff) > precision ){
//            ss << "EdgeDiscGSR::testTotalProbabilities: Test 3 failed." << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Diff for node " << u->getNumber() << ": " << diff << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Diff for node " << u->getNumber() << ": " << (calculateDataProbability() - getLeafAtBar(u)).val() << endl << endl;
//            test_fail = true;
//        }
//    }
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 3: END >>>>" << endl << endl;}
//
//    /**
//     * Test 4.Ratio between total placement density and tree density equals 1.0.
//     */
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 4: Ratio between total placement density and tree density equals 1.0 >>>>" << endl;}
//    for(it = m_G.begin(); it != m_G.end(); ++it) {
//        const Node *u = *it;
//        Real ratio = (getTotalPlacementDensity(u) / calculateDataProbability()).val();
//        Real diff = ratio - 1.0;
//        if(abs(diff) > precision){
//            ss << "EdgeDiscGSR::testTotalProbabilities: Test 4 failed." << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Ratio for node " << u->getNumber() << ": " << ratio << endl;
//            ss << "EdgeDiscGSR::testTotalProbabilities: Diff: " << diff << endl << endl;
//            test_fail = true;
//        }
//    }
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: <<<< Test 4: END >>>>" << endl;}
//
//    if(test_fail){
//        ss << "EdgeDiscGSR::testTotalProbabilities: Some test failed. Printing info about tree: " << endl;
//        ss << "EdgeDiscGSR::testTotalProbabilities: Tree density: " << calculateDataProbability().val() << endl;
//        ss << "EdgeDiscGSR::testTotalProbabilities: Tree:" << endl;
//        ss << getTree() << endl;
//        ss << "Birth rate: " << m_BDProbs.getBirthRate() << endl;
//        ss << "Death rate: " << m_BDProbs.getDeathRate() << endl;
//        ss << getDebugInfo(true, true, true, true);
//    }
//    else{
//        if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: All tests succeeded! " << endl;}
//    }
//    if(diagnostics){ss << "EdgeDiscGSR::testTotalProbabilities: ================END OF TESTS==============" << endl << endl;}
//    return ss.str();
//}
//
//
//} // end namespace beep
