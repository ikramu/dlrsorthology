#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>

#include "AnError.hh"
#include "Beep.hh"
#include "PRNG.hh"
#include "BDHybridTreeGenerator.hh"
#include "HybridBDTreeGenerator.hh"
#include "HybridTree.hh"
#include "HybridTreeIO.hh"

#include "HybridHostTreeModel.hh"

//#define COMP_POST

// Global options with default settings
//------------------------------------
int nParams = 1;
long unsigned seed = 0;

unsigned nTrees = 1;
bool hybridTree = false;
bool hybridHost = false;
unsigned useTree = 0;
bool recordTreeStat = false;
bool requestFullGS = false;
bool noTopTime = false;

char* infile = 0; //NULL
char* outfile = 0; //NULL
char* t90file = 0;  //NULL
char* statfile = 0;  //NULL

bool showTrees = true;
bool showGS = true;
bool showGamma = true;

bool outputSTree = true;
bool outputGamma = true;
bool uniqueTrees = false;
bool edgetimes = false;
bool gamma_tree = false;
bool newick = false;

// Global variables for generation
beep::Tree G;  
beep::HybridTree H;  
beep::StrStrMap gs;
beep::GammaMap* reconciliation;

double lambda = 1.0;
double mu = 1.0;
double rho = 0;
double toptime = -1.0;

std::string leaf_name_prefix = "Leaf";
unsigned minleaves = 1;
unsigned maxleaves = 100000;
unsigned limit = 1000;


// helper functions
//-------------------------------------------------------------
void saveTrue(std::ostringstream& tree);
void saveTree(std::ofstream& tree, std::ofstream& gsmap);
void printTree();
void generateHybridTree();
void generateBinaryTree(bool hybrid);
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  map<string, Real> treeStat;
#ifdef COMP_POST
  map<string, Real> postTreeStat;
#endif

  if (argc < nParams + 1) 
    {
      usage(argv[0]);
    }
  try
    {
      // Read options
      //---------------------------------------------
      int opt = readOptions(argc, argv);

      //set seed
      //---------------------------------------------
      PRNG rand;
      if(seed != 0)
	  rand.setSeed(seed);
      else
	  seed = rand.getSeed();

      // Read arguments
      //---------------------------------------------	
      if(opt < argc)
	{
	  nTrees = atoi(argv[opt++]);
	}
      if(hybridTree == false)
	{
	  if(infile)
	    {
	      HybridTreeIO io_in = HybridTreeIO::fromFile(infile);
	      vector<HybridTree> Hvec = io_in.readAllHybridTrees(0);

	      if(useTree >= Hvec.size())
		{
		  throw AnError("The requested tree does not exist", 1);
		}
	      H = Hvec[useTree];
	    }
	  else if(hybridHost)
	    {
	      throw AnError("I need a hybrid host tree to generate on", 1);
	    }
	  else
	    {
	      //Create a dummy Species tree
	      //---------------------------------------------------------
	      if(toptime < 0)
		{
		  toptime = 1.0;
		}
	      
	      H = Tree::EmptyTree(toptime, leaf_name_prefix);
	    }
	  if(H.getName() == "Tree")
	    {
	      H.setName("Host");
	    }
	}

      if(toptime <0) // If we still have toptime < 0, use default = 1.0
	{
	  toptime = 1.0;
	}

      // Tell user we have started
      //---------------------------------------------
      cout << "Running ";
      for(unsigned i = 0; i < static_cast<unsigned>(argc); i++)
	cout << argv[i] << " ";
      cout << " with Seed = " << seed << endl;
      

      // Set up needed file handles and variables
      //---------------------------------------------
      // Global statistics variables
      Real nLeaves = 0;
      Real nHybrids = 0;
      Real nExtinct = 0;
      Real nRootGamma = 0;
      Real fullGS = 0;
      Real rootTime = 0;
      // Global sstreams 
      ostringstream statheader;
      ostringstream statdata;
      ostringstream t90header;
      ostringstream t90data;

      // global file handles
      ofstream stat(NULL);
      ofstream tree(NULL);
      ofstream gsmap(NULL); 
      ofstream t90(NULL);

      if(statfile) // open Statistics file
	{
	  cerr << "statfile\n";
	  stat.open((string(statfile)).data());       
	}
      

      // Open tree output file
      //---------------------------------------
      if(outfile != NULL)  
	{
	  if(showTrees)
	    {
	      cout << "The trees are written to file "
		   << outfile
		   << "\n";
	    }
	  tree.open((string(outfile)).data());

	  if(infile && hybridTree == false)
	    {
	      string gsfile;
	      string tmp(infile);
	      string::size_type idx = tmp.find_last_of('/');
	      if(idx == string::npos)
		{
		  gsfile = string(outfile) + "_" + string(infile) + ".gsmap";
		}
	      else
		{
		  gsfile = string(outfile) + "_" + tmp.substr(idx+1) + ".gsmap";
		}	
	      if(showTrees)
		{
		  cout << "The gene_species map is written to file "
		       << gsfile
		       << endl;
		}
	      gsmap.open(gsfile.data());
	    }
	}

      // Open the 'true' file for saving parameters for each generation
      //---------------------------------------------------------------
      if(t90file != 0)  // OPen 'True-file' (for recording paramters)
	{
	  t90.open((string(t90file)).data());
	}


      // Time for the generation of a user-defined number of trees
      //-----------------------------------------------------------
      vector<string> trees;
		
      for(unsigned i = 0; i < nTrees; i++)
	{
	  // Generate the tree
	  do
	    {
	      if(hybridTree) // Currently 
		{
		  generateHybridTree();
		}
	      else 
		{
		  generateBinaryTree(hybridHost);
		}
	    }
	  while(find(trees.begin(), trees.end(), 
		     TreeIO::writeNewickTree(H.getBinaryTree()))
		!= trees.end());

	  if(uniqueTrees)
	    {
	      if(hybridTree)
		{
		  trees.push_back(HybridTreeIO::writeNewickTree(H.getBinaryTree()));
		  ostringstream oss;
		  oss << "Hybrid_" << i;
		  H.setName(oss.str());
		}
	      else
		{
		  trees.push_back(TreeIO::writeNewickTree(H.getBinaryTree()));
		  ostringstream oss;
		  oss << "Guest_" << i;
		  G.setName(oss.str());
		}
	    }

	  // Print tree to screen
	  if(showTrees)
	    {
	      printTree();
	    }
	  // record tree fo tree statistics (posterior distr)
	  if(recordTreeStat)
	    {
 	      treeStat[TreeIO::writeNewickTree(H.getBinaryTree())]+=1.0;
#ifdef COMP_POST
	      postTreeStat[HybridTreeIO::writeHybridTree(H.getBinaryTree())]+=1.0;
#endif
	    }

	  // Record statistics about the tree
	  if(statfile)
	    {
	      if(hybridTree)
		{
		  nLeaves += H.getNumberOfLeaves();
		  nHybrids += H.countHybrids();
		  nExtinct += H.countExtinct();
		  rootTime += H.getTopTime();
		}
	      else
		{
		  nLeaves += G.getNumberOfLeaves();
		  if(reconciliation)
		    {
		      nRootGamma += 
			reconciliation->getSize(H.getBinaryTree().getRootNode());
		    }
		  fullGS += (gs.size() < H.getNumberOfLeaves() ? 0 : 1.0);
		  rootTime += toptime + H.getTime(*H.getRootNode());
		  assert(rootTime>0);
		  if(G.getRootNode())
		    {
		      rootTime -= G.getTime(*G.getRootNode());		  
		    }
		  assert(rootTime>0);
		}	  
	    }

	  // saving tree
	  if(outfile != NULL)
	    {
	      saveTree(tree, gsmap);
	    }
	  if(t90file != 0)
	    {
	      saveTrue(t90data);
	    }

	}
	      

      if(statfile)
	{
	  cout << "Statistics are written to file "
	       << statfile
	       << "\n\n";
	  statheader << "# Statistics for generation  with seed " 
		     << seed << "\n"
		     << "# S N\ttopTime(float);\tbirthRate(float);"
		     <<"\tdeathRate(float);\t";
	  statdata << "0 0 " << toptime << ";\t"<< lambda << ";\t" << mu << ";\t";

	  if(hybridTree)
	    {
	      statheader << "hybridRate(float);\tavLeaves(float);"
			 << "\tavHybrids(float);\tavExtinct(float);\t"
			 << "avRootTime;\t";
	      statdata   << rho << ";\t"
			 << nLeaves/nTrees << ";\t"
		         << nHybrids/nTrees << ";\t"
		         << nExtinct/nTrees << ";\t"
			 << rootTime/nTrees << ";\t";
	    }
	  else
	    {
	      statheader << "nLeaves(float);\trootGammaSize(float)\tpFullGS(float)\t"  
			 << "avRootTime;\t";;
	      statdata << nLeaves/nTrees << ";\t"
		       << nRootGamma/nTrees << ";\t"
		       << fullGS/nTrees << ";\t"
		       << rootTime/nTrees << ";\t";
	    }	  
	  if(outputSTree && hybridTree == false)
	    {
	      if(hybridHost)
		{
		  statheader <<"\tHost(HybridTree);";
		  statdata << HybridTreeIO::writeHybridTree(H) << ";\t";
		}
	      else
		{
		  statheader <<"\tHost(tree);";
		  statdata << TreeIO::writeHostTree(H.getBinaryTree()) 
			   << ";\t";
		}
 	    } 
	  statheader << "\n";
 	  statdata << "\n";
 	  stat << statheader.str() << statdata.str();
	  if(recordTreeStat)
	    {
	      cerr << "\n\nTree distribution\n"
		   << "prob\ttree\n";
	      for(map<string,Real>::iterator i = treeStat.begin();
		  i != treeStat.end(); i++)
		{
		  cerr << i->second/treeStat.size() << "\t" << i->first <<endl;
		}
	      cerr << "\n\n\n";
	    }
	}
      if(t90file != 0)
	{
	  cout << "'True' parameters for 90% test is written to file "
	       << t90file
	       << "\n";
	  t90header << "# True parameters input for 90% test\n"
		    << "# T N\tbirthRate(float);\tdeathRate(float);\t";
	  if(hybridTree)
	    {
	      t90header << "rho;\t";
	    }
	  t90header << "G(tree);\t";
	  if(outputSTree && hybridTree == false)
	    {
	      t90header << "S(tree);\t";
	    }
	  if(outputGamma && hybridTree == false)
	    {
	      t90header << "\tGamma(reconciliation);";
	    }
	  t90header << "\n";
	  t90data << "\n";
	  t90 << t90header.str() << t90data.str();
	}
      if(recordTreeStat)
	{
	  cerr << "recordTreeStat\n";
	  exit(0);
#ifdef COMP_POST
	  //Posterior probs
	  HybridTreeIO io;
	  HybridTree T;
	  Probability p;
	  map<string, Probability> post;
	  Probability postsum = 0;
	  map<string, int> done;
	  for(map<string, Real>::iterator i = postTreeStat.begin();
	      i != postTreeStat.end(); i++)
	    {
	      io.setSourceString(i->first);
	      T = io.readHybridTree();
	      if(done.find(i->first) == done.end())
		{
		  HybridHostTreeModel hhtm(T, lambda, mu ,rho, 100); 
		  p = hhtm.calculateDataProbability();
		  string key = io.writeNewickTree(T.getBinaryTree());
		  post[key] += p;
		  postsum += p;
		  done[i->first] = 1;
		}
	      
	    }
#endif

	  cout << "\n\nTree distribution\n"
#ifdef COMP_POST
	       << "rank\tprob\tpost P\n";
#else
	       << "rank\tprob\tcumul P\ttree\n";
#endif
	  multimap<Real, string, std::greater<Real> > out;
	  Real sum = 0;
	  for(map<string,Real>::iterator i = treeStat.begin();
	      i != treeStat.end(); i++)
	    {
	      out.insert(make_pair(i->second,
				   i->first));
	      sum += i->second;
	    }
	  Real cum = 0;
	  unsigned j = 0;
	  for(multimap<Real,string>::iterator i = out.begin();
	      i != out.end(); i++)
	    {
	      j++;
	      cum += i->first;
	      cout << j << " \t" 
 		   << 100.0 * i->first / sum << " %\t" 
//  		   << 100.0 * i->first / nTrees << " %\t" 
#ifdef COMP_POST
		   << 100.0 *  (post[i->second]/postsum).val()
#else
		   << 100.0 * cum / sum << " %\t"  
 		   << i->second 
#endif
		   << endl;
	    }
	  cout << "\n\n\n";
	}
      tree.close();
    }
  catch (AnError e)
    {
      cerr << "error found when running " ;
      for(int i = 0; i < argc; i++)
	cerr << argv[i] << " ";
      cerr << "with seed = " << seed << "\n";
      e.action();
    }
}

void saveTrue(std::ostringstream& t90data)
{	   
  using namespace std;
  using namespace beep;
  HybridTreeIO io_out;
	      t90data << "0 0\t"
		      << lambda
		      << ";\t"
		      << mu
		      << ";\t";
	      if(hybridTree)
		{
		  TreeIOTraits traits;
		  t90data << rho << ";\t"
			  << io_out.writeHybridTree(H, traits,0) << ";\t";
//  false, false, false, 
// 						    false, false,0) << ";\t";
		}
	      else
		{
		  TreeIOTraits traits;
		  t90data << io_out.writeBeepTree(G, traits, 0) << ";\t";
		}
	      
	      if(outputSTree && hybridTree == false)
		{
		  t90data << "\t" 
			  << io_out.writeHostTree(H)
			  << ";";
		}
	      if(outputGamma && hybridTree == false)
		{
		  t90data << "\t";
		  t90data << io_out.writeGuestTree(G, reconciliation);
		  t90data << ";";
		}
	      t90data << "\n";
}

void saveTree(std::ofstream& tree, std::ofstream& gsmap)
{
  using namespace std;
  using namespace beep;
  if(G.getNumberOfLeaves() == 0)
    {
      assert(minleaves == 0);
      return;
    }
  assert(G.getRootNode() != NULL);
  using namespace std;
  using namespace beep;
  HybridTreeIO io_out;
  TreeIOTraits traits;
  if(newick == false)
    {
      traits.setID(true);
      //	      traits.setET(edgetimes);
      traits.setNT(edgetimes);
      traits.setName(true);
    }
  if(hybridTree)
    {
      tree << io_out.writeHybridTree(H, traits, 0)//, true, edgetimes,
	//edgetimes, false, false, 0) 
	   << ";\n";
    }
  else
    {
      tree << io_out.writeBeepTree(G, traits, 0)//, !newick, edgetimes,
	// 					       false, false, newick, 
	// 					       (gamma_tree ? reconciliation : 0)) 
	   << ";\n";
    }
  if(infile && hybridTree == false)
    {
      gsmap << "#\n" << gs;
    }
}

void printTree()
{
  using namespace std;
  cout << "The generated trees are: \n";
  if(hybridTree)
    {
      cout << "Tree " << H.getName()
	   << "with "
	   << H.getNumberOfLeaves() 
	   << " leaves:\n"
	   << H.print(true, true, false, false);
    }
  else
    {
      cout << "Tree " << G.getName()
	   << "with "
	   << G.getNumberOfLeaves() 
	   << " leaves:\n"
	   << G.print(false, true, false, false);
    }
  if(showGS && gs.size() > 0)
    {
      cout << "\nThe corresponding gene species map is:\n"
	   << gs
	   << endl;
    }
  if(showGamma && reconciliation)
    {
      cout << " and the generated reconciliation is:\n"
	   << *reconciliation;
      if(hybridHost)
	{
	  cout << "# Please note, gamma("
	       << H.getBinaryTree().getName()
	       << ") maps the generated "
	       << "gene tree to the " 
	       << "corresponding binary tree of the hybrid tree " 
	       << H.getName() << ":\n" 
	       << H.getBinaryTree().print() << endl;
	}
    }
  cout << "\n";
}

void generateHybridTree()
{
  using namespace beep;
  using namespace std;
  BDHybridTreeGenerator tg(lambda, mu, rho);

  if(toptime >= 0)
    {
      tg.setTopTime(toptime);
    }
  else
    {
      toptime = tg.getTopTime();
    }
  cout << tg;
  unsigned nleaves;
  // To avoid too lengthy analyses, we restrict the size of G
  unsigned trials = 0;

  do
    {
      if(statfile)
	{
	  tg.generateHybridTree(H);
	}
      else
	{
	  while(tg.generateHybridTree(H) == false);
	}
      trials++;
      nleaves = H.getNumberOfLeaves() - H.countExtinct();
      if(trials > limit) // give up!
	{
	  cerr << "beep_generateTree: I'm sorry, but with the\n"
	       << "parameters you're giving, the trees either \n"
	       << "gets to big or just one leaf.\n"
	       << "I've tried "<< limit <<" times already!\n"; 
	  exit(1);
	}
    }
  while(nleaves > maxleaves || nleaves < minleaves);
  return;
};

void 
generateBinaryTree(bool hybrid)
{
  using namespace beep;
  using namespace std;
  TreeGenerator* tg = 0;
  gs.clearMap();

  if(hybridHost)
    {
      tg = new HybridBDTreeGenerator(H, lambda, mu);
    }
  else
    {
      tg = new BDTreeGenerator(H.getBinaryTree(), lambda, mu);
    }
  assert(tg != 0);

  if(toptime >= 0)
    {
      tg->setTopTime(toptime);
    }
  else
    {
      toptime = tg->getTopTime();
    }
  unsigned nleaves;
  unsigned trials = 0;
  unsigned cumGS = 0;
  unsigned cumLeaves = 0;
  do
    {
      if(minleaves == 0 || statfile)
	{
	  tg->generateTree(G, noTopTime);
	}
      else
	{
	  while(tg->generateTree(G, noTopTime) == false);
	}
      trials++;
      nleaves = G.getNumberOfLeaves();
      if(nleaves > 0)
	{
	  cumGS += tg->exportGS().size();
	  cumLeaves += nleaves;
	}
      if(trials > limit) // give up!
	{
	  cerr << "beep_generateTree: I'm sorry, but with the\n"
	       << "parameters you're giving, the trees either \n"
	       << " gets to big or just one leaf.\n"
	       << "gs on average " << static_cast<Real>(cumGS) / limit << endl
	       << "leaves on average " << static_cast<Real>(cumLeaves) / limit << endl
	       << "I've tried "<< limit <<" times already!\n"; 
	  exit(1);
	}
      
    }
  while(nleaves > maxleaves || nleaves < minleaves || (requestFullGS && (tg->exportGS().reverseSize() != H.getNumberOfLeaves()))) ;

  if(nleaves > 0)
    {
      assert(G.getRootNode() != NULL);
      gs = tg->exportGS();
      if(reconciliation)
	{
	  delete reconciliation;
	}
      reconciliation = new GammaMap(tg->exportGamma());
    }
};

void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] [<#tree>]\n"
    << "\n"
    << "Based on evolutionary parameters, generate:\n"
    << " - host trees under a birth/death model\n"
    << " - host networks under the HSM model\n"
    << " - guest trees given a host tree/network under the GEM model\n\n"
    << "Output of divergence times and reconciliations is optional.\n"
    << "Optionally can statistics or tree distributions, only, be output\n"
    << "The '-T' options output parameters related to the generation, it\n"
    << "is recommended to use these options.\n"
    << "\n"


    << "Options:\n"
    << "   -h                    This text.\n"
    << "   -q                    \"Quiet\", reduce output\n"
    << "   -s <int>              Use this as seed for random generator.\n"
    << "   -o <outfile>          output file. The generated tree will be \n"
    << "                         output to this file and any guest-host\n"
    << "                         leaf map is output to a file named \n"
    << "                         <infile>_<outfile>.gs.\n" 
    << "   -l <int>              failure limit for generation of the\n"
    << "                         requested tree (default is 500 trials).\n"
    << "   -T<option>            Options for the output of 'true' parameters\n"
    << "                         used in the generation (off by default)\n"
    << "     -To <filename>      Outputs, unless other 'T'-options state\n"
    << "                         otherwise, all 'true' parameters to file\n"
    << "                         <filename>\n"
    << "     -Ts                 Output only statistics for trees.\n"
    << "     -Td                 Show tree distribution\n"
    << "     -Th                 Do not output the host tree and edge times\n"
    << "                         used for data generation (default for hybrid)\n"
    << "     -Tr                 Do not output the generated reconciliation\n"
    << "   -H<option>            Options relating to the host tree\n"
    << "     -Hi <filename>      Input host tree\n"
    << "     -Hx                 Input host tree is a hybrid tree\n"
    << "     -Hn <uint>          Use tree number 'n' in host tree file\n"
    << "   -G<option>            Options relating to the generated tree\n"
    << "     -Gt                 Include edge times on tree in output file\n"
    << "     -Gr                 Include reconciliation in tree output\n"
    << "                         (default without)\n"
    << "     -Gh <uint>          Highest number of leaves (<1000)\n"
    << "     -Gl <uint>          Lowest number of leaves (>0)\n"
    << "     -Gp <string>        Leaf name prefix\n"
    << "     -Gn                 Output tree in newick-like format (Note \n"
    << "                         if edge times are requested (-Gt), they\n"
    << "                         are output in place of newick edge lengths\n"
    << "     -Gu                 All output trees must be unique\n"
    << "                         (default not unique)\n"
    << "     -Gf                 Request for at least one gene per species\n"
    << "     -Gs                 Start directly with a spli, i.e., no topTime\n"
    << "   -B<option>            Options relating to the tree generation model\n"
    << "     -Bp <float> <float> birth and death rate of tree model\n"
    << "                         Default is " << lambda << " and " << mu << ".\n"
    << "     -Bx <float>         hybridization rate of tree model\n"
    << "                         Default is " << rho << ".\n"
    << "     -Bt <float>         How long before root time of host tree did\n" 
    << "                         the process start?\n"
    ;
  ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'q':
	  {
	    showTrees = false;
	    break;
	  }
	case 's':
	  {
	    if (opt + 1 < argc)
	      {
		seed = atol(argv[++opt]);
		std::cerr << "set Seed to " << seed << std::endl;
	      }
	    else
	      {
		cerr << "Expected seed after option '-s'\n";
		usage(argv[0]);
	      }
	    break;
	    
	  }
	case 'l':
	  {
	    if (opt + 1 < argc)
	      {
		limit = atoi(argv[++opt]);
	      }
	    else
	      {
		cerr << "Expected seed after option '-s'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'T':
	  {
	    switch (argv[opt][2]) 
	      {
	      case 'o':
		{
		  if (opt + 1 < argc)
		    {
 		      t90file = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected filename after option '-t'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 's':
		{
		  statfile = t90file;
		  t90file = 0;
		  showTrees = false;
		  showGS = false;
		  showGamma = false;
		  break;
		}
	      case 'd':
		{
		  recordTreeStat = true;
		  showTrees = false;
		  showGS = false;
		  showGamma = false;
		  break;
		}
	      case 'h':
		{
		  outputSTree = false;
		  break;
		}
	      case 'r':
		{
		  outputGamma = false;
		  break;
		}
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	case 'H':
	  {
	    switch (argv[opt][2]) 
	      {
	      case 'i':
		{
		  if (opt + 1 < argc)
		    {
		      infile = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected filename after option '-o'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (opt + 1 < argc && atoi(argv[++opt]) >= 0) 
		    {
		      useTree = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr <<"Expected a uint after '-Hn'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 'x':
		{
		  hybridHost = true;
		  break;
		}
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	case 'G':
	  {
	    switch (argv[opt][2]) 
	      {
	      case 't':
		{
		  edgetimes = true;
		  break;
		}
	      case 'f':
		{
		  requestFullGS = true;
		  break;
		}
	      case 'l':
		{
		  if (opt + 1 < argc && atoi(argv[++opt]) >= 0) 
		    {
		      minleaves = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr <<"Expected a uint >= 0 after '-Gl'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 'h':
		{
		  if (opt + 1 < argc && atoi(argv[++opt]) > 0) 
		    {
		      maxleaves = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr <<"Expected a uint < 1000 after '-Gl'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 'p':
		{
		  if (opt + 1 < argc) 
		    {
		      leaf_name_prefix = argv[++opt];
		    }
		  else
		    {
		      cerr <<"Expected a string after '-Gp'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 'r':
		{
		  gamma_tree = true;
		  break;
		}
	      case 's':
		{
		  noTopTime = true;
		  break;
		}
	      case 'u':
		{
		  uniqueTrees = true;
		  break;
		}
	      case 'n':
		{
		  newick = true;
		  break;
		}
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	case 'B':
	  {
	    switch (argv[opt][2]) 
	      {
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      lambda = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  mu = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Expected float (death rate)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (birth and death rate "
			   << "for tree model) for option '-e'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 't':
		{
		  if (++opt < argc) 
		    {
		      toptime = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Expected float (start time)\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'x':
		{
		  if (++opt < argc) 
		    {
		      rho = atof(argv[opt]);
		      hybridTree = true;
		    }
		  else
		    {
		      cerr << "Expected float (hybridization rate)\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
}
