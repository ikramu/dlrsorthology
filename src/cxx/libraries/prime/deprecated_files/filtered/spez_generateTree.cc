#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "InvMRCA.hh"
#include "ReconciliationTreeGenerator.hh"
#include "TreeIO.hh"

// Global options with default settings
//------------------------------------
int nParams = 2;

std::string leaf_name_prefix = "";
double lambda = 1.0;
double mu = 1.0;
std::string infile = "";
std::string outfile = "";
unsigned minleaves = 1;
unsigned maxleaves = 100;
bool specprob = false;
bool falseOrthology = false;
bool onlyFalseOrthology = false;
unsigned maxTrials = 5000;  
beep::Real topTime = 0;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < nParams+1) 
    {
      usage(argv[0]);
    }
  try
    {
      // Read options
      //---------------------------------------------
      int opt = readOptions(argc, argv);

      // Check sanity of limiting option
      if(minleaves > maxleaves)
	{
	  cerr << "-Gl and -Gh: The highest number of leaves must be higher\n"
	       << "             than the lowest number of leaves.\n\n";
	  usage(argv[0]);
	}

      // Read arguments
      //---------------------------------------------
      if (opt >= argc)
	{
	  usage(argv[0]);
	}
      int leaves = atoi(argv[opt++]);

      infile = argv[opt];
      TreeIO io_in = TreeIO::fromFile(infile);
      Tree S = io_in.readHostTree();
      if(S.getName() == "Tree")
	{
	  S.setName("S");
	}
      if(topTime > 0)
	{
	  S.getRootNode()->setTime(topTime); 
	}

      // Set up the model for the species tree
      //---------------------------------------------------------
      BirthDeathProbs BDP(S, lambda, mu);
      PRNG R;
      ReconciliationTreeGenerator RTG(BDP, leaf_name_prefix);
      cout << RTG;

      // Generating evolutionary tree
      unsigned trials = 0;
      unsigned nleaves = 0;
      do
	{
	  falseOrthology = false;
	  if(leaves < 1)
	    {
	      RTG.generateGammaTree(true);
	    }
	  else
	    {
	      RTG.generateGammaTree(leaves, true);
	    } 	  
	  trials++;
	  if(trials > maxTrials) // give up!
	    {
	      cerr << "beep_generateTree: I'm sorry, but with the\n"
		   << "parameters you're giving, the trees either \n"
		   << " gets to big or too small.\n"
		   << "I've tried "<< maxTrials <<" times already!\n"; 
	      exit(6); // CHECK UP WHAT ERROR CODE SHOULD BE USED! /bens
	    }

	  Tree& G = RTG.getGtree();
	  G.setName("Guest");
	  GammaMap gamma = RTG.exportGamma();
	  
	  LambdaMap sigma(G, S, RTG.exportGS());
	  GammaMap gamma_star = GammaMap::MostParsimonious(G,S,sigma);
	  
	  for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	    {
	      Node* u = G.getNode(i);
	      if(gamma_star.isSpeciation(*u))
		{
		  if(gamma.isSpeciation(*u) == false)
		    {
		      falseOrthology = true;
      cout << "The generated tree: \n"
	   << G.print()
	   << "The gene species map:\n"
	   << RTG.gs4os() 
	   << "The generated reconciliation:\n"
	   << gamma;

      cout << "The mpr reconciliation:\n"
	   << gamma_star;
		    }
		}
	    }
	  nleaves = G.getNumberOfLeaves();
	}
      while((onlyFalseOrthology == true && falseOrthology == false) ||
	    nleaves < minleaves || nleaves > maxleaves);

      Tree& G = RTG.getGtree();
      G.setName("Guest");
      GammaMap gamma = RTG.exportGamma();
      
      LambdaMap sigma(G, S, RTG.exportGS());
      GammaMap gamma_star = GammaMap::MostParsimonious(G,S,sigma);

      cout << "The generated tree: \n"
	   << G.print()
	   << "The gene species map:\n"
	   << RTG.gs4os() 
	   << "The generated reconciliation:\n"
	   << gamma;

      cout << "The mpr reconciliation:\n"
	   << gamma_star;

      if(gamma == gamma_star)
	{
	  cout << "The generated reconciliation is the most "
	       << "parsimonious one (MPR)\n";
	}
      else
	{
	  cout << "The generated reconciliation is NOT the most "
	       << "parsimonious one (NOT MPR)\n";
	}
      cout << "\n";

      // try to identify what mpr speciation vertices are not 
      // true speciations
      BeepVector<char> nodeStatus(G);
      cout << "Node\tstatus\n";
      falseOrthology = false;
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  Node* u = G.getNode(i);
	  if(gamma_star.isSpeciation(*u))
	    {
	      if(gamma.isSpeciation(*u))
		{
		  nodeStatus[u] = 'S';
		}
	      else
		{
		  nodeStatus[u] = 'F';
		  falseOrthology = true;
		}
	    }
	  else
	    {
	      nodeStatus[u] = 'D';
	    }
	  cout << i << "\t" << nodeStatus[i] << "\n";
	}
      // saving tree
      if(outfile != "")
	{
	  if(falseOrthology)
	    {
	      outfile += "_FO";
	    }
	  else
	    {
	      outfile += "_TO";
	    }

	  cout << "The tree is written to file "
	       << outfile
	       << ".G\n";
	  TreeIO io_out;
	  ofstream tree((outfile+".G").c_str());
	  tree << io_out.writeGuestTree(G, &gamma) << "\n";
	  cout << "'True' parameters for 90% test is written to file "
	       << outfile + ".true"
	       << "\n"
	    ;
	  string RecName = G.getName() + "_" + S.getName() + "_GuestTree";
	  ofstream t90((outfile+".true").c_str());
	  ostringstream t90header;
	  ostringstream t90data;
	  t90header << "# True parameters input for 90% test\n"
		    << "# T N\t"
		    << S.getName()
		    << "_DupLoss.birthRate(float)\t"
		    << S.getName()
		    << "_DupLoss.deathRate(float)\t"
		    << G.getName() 
		    << "(tree);\t"
		    << S.getName()
		    << "(tree);\t"
		    << RecName
		    << ".reconciliation(reconciliation);\t";
	  t90data << "0 0\t"
		  << lambda
		  << ";\t"
		  << mu
		  << ";\t"
		  << io_out.writeGuestTree(G)
		  << ";"
		  << "\t" 
		  << io_out.writeHostTree(S)
		  << ";"
		  << "\t" 
		  << io_out.writeGuestTree(G, &gamma)
		  << ";\t"; 
	  if(specprob)
	    {
	      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
		{
		  Node* u = G.getNode(i);
		  if(gamma_star.isSpeciation(*u) && u->isLeaf() == false) 
		    {
		      t90header << RecName +".speciation[" << i 
				<< "](logfloat);\t";
		      t90data << (gamma.isSpeciation(*u)?1:0) << ";\t";
		    }	      
		}
// 	      t90header << "\n";
// 	      t90data << "\n";

	    }
// 	  else
// 	    {
	      InvMRCA im(G);
	      t90header << RecName +".orthology(orthologypairs);\n";
	      t90data << "[";
	      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
		{
		  Node* u = G.getNode(i);
		  if(gamma_star.isSpeciation(*u) && u->isLeaf() == false)  // only MPR spec yield orthology
		    {
		      t90data << im.getStrRep(*u, gamma.isSpeciation(*u)?1:0);
		    }	      
		}
	      t90data << "]\n";
// 	    }
	  t90 << t90header.str() << t90data.str();
	  
	  cout << "Node status is written to file "
	       << outfile + ".status"
	       << "\n"
	    ;
	  ofstream status((outfile+".status").c_str());
	  ostringstream statusheader;
	  ostringstream statusdata;
	  statusheader << "# Status of vertices in true reconciliation(TR) \n"
		       << "# and mostparsimonious reconciliation (MPR)\n"
		       << "# 'S' = speciation in TR and MPR, \n "
		       << "# 'D' = duplication in MPR\n"
		       << "# 'F' = duplication in TR and speciation in MPR\n"
		       << "# \n";
	  for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	    {
	      statusheader << "Status[" << i << "](char);\t";
	      statusdata << nodeStatus[i] << ";\t";
	    }
	  status << statusheader.str() << "\n"
		 << statusdata.str() << "\n";
	}
    }
  catch (AnError e)
    {
      cerr << "error found\n";
      e.action();
    }
}


void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <nleaves> <STree>\n"
    << "\n"
    << "Parameters:\n"
    << "   <nleaves>          If no host tree, <STree>, is given, a random\n"
    << "                      tree with <nleaves> leaves is generated. If a\n"
    << "                      host tree is given, then a guest tree that has\n"
    << "                      <nleaves> nodes reconciled to the root of the \n"
    << "                      host tree. If <nleaves> < 1.0, a new number\n"
    << "                      is drawn from the birth-death process .\n"
    << "   <STree>            The name of a file containing a tree on which\n"
    << "                      the generated tree is reconciled.\n"


    << "Options:\n"
    << "  -u, -h                This text.\n"
    << "  -o <out_prefix>       prefix of output file. The generated tree\n"
    << "                        is output to file 'out_prefix_<FO/TO>.G'\n"
    << "                        and the prameter values, etc. will be \n"
    << "                        written to file 'out_prefix_<FO/TO>.true',\n"
    << "                        where <FO/TO> is 'FO' if the most \n"
    << "                        parsimonious reconciliation will falsely\n"
    << "                        interpret a vertex as orthologous  and 'TO'\n"
    << "                        otherwise.\n"
    << "  -t <trials>           How many tries before giving up?\n"
    << "  -G<option>            Options relating to the generated tree\n"
    << "    -Gn <string>        Leaf name prefix\n"
    << "    -Gh <uint>          Highest number of leaves (<1000)\n"
    << "    -Gl <uint>          Lowest number of leaves (>0)\n"
    << "    -Gs                 record speciation probabilities instead \n"
    << "                        of orthology probabibities\n"
    << "    -Gf                 generate only trees with false orthologies\n"
    << "                        as estimated by mpr in them\n"
    << "    -Gt <float>         top time', i.e., how long before the root\n"
    << "                        of S did the process start (>0)\n"
    << "  -B<option>            Options relating to the birth death model\n"
    << "    -Bp <float> <float> birth and death rate of tree model\n"
    << "                        Default is " << lambda << " and " << mu << ".\n"
;
   ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
	      outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (opt + 1 < argc)
	      {
		maxTrials = atoi(argv[++opt]);
	      }
	    else
	      {
		cerr << "Expected <number of trials> after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'G':
	  {
	    switch (argv[opt][2]) 
	      {
	      case 'n':
		{
		  if (opt + 1 < argc) 
		    {
		      leaf_name_prefix = argv[++opt];
		    }
		  else
		    {
		      cerr <<"Expected a string after '-Gns'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 'l':
		{
		  if (opt + 1 < argc && atoi(argv[++opt]) > 0) 
		    {
		      minleaves = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr <<"Expected a uint > 0 after '-Gl'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 'h':
		{
		  if (opt + 1 < argc && atoi(argv[++opt]) > 0) 
		    {
		      maxleaves = atoi(argv[opt]);
		    }
		  else
		    {
		      cerr <<"Expected a uint > 0 after '-Gh'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 't':
		{
		  if (opt + 1 < argc && atof(argv[++opt]) > 0) 
		    {
		      topTime = atof(argv[opt]);
		    }
		  else
		    {
		      cerr <<"Expected a float > 0 after '-Gl'\n";
		      usage(argv[0]);
		    }
		  break;
		}	
	      case 's':
		{
		  specprob = true;
		  break;
		}	
	      case 'f':
		{
		  onlyFalseOrthology = true;
		  break;
		}	
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	case 'B':
	  {
	    switch (argv[opt][2]) 
	      {
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      lambda = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  mu = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Expected float (death rate)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (birth and death rate "
			   << "for tree model) for option '-e'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "option " << argv[opt] << " not recognized\n";
		  usage(argv[0]);
		  break;
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "option " << argv[opt] << " not recognized\n";
	    usage(argv[0]);
	    break;
	  }
	}
      

      opt++;
    }
  return opt;
}
