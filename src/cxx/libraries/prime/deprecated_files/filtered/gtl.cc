#include <iostream>

#include <sys/types.h>
#include <unistd.h>

#include "AnError.hh"
#include "IntegralBirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "LambdaMap.hh"
#include "MultiFamReconMCMC.hh"
#include "PRNG.hh"
#include "ReconciliationSampler.hh"
#include "SimpleMCMC.hh"
#include "TopTimeMCMC.hh"
#include "StrStrMap.hh"
#include "TreeAnalysis.hh"
#include "TreeIO.hh"

using namespace std;
using namespace beep;

//
// Global options
//
unsigned MaxNumberOfNodes = 1000; // Warning, arbitrary choice!
unsigned Thinning = 100;
unsigned MaxIter = 10000;
unsigned RSeed = 0;
char* outfile=NULL;
Real DefaultBirthRate = 0.001;
Real DefaultDeathRate = 0.0012;
Real DefaultBeta = 0.5;
bool reroot_on = true;		// Reroot trees by default!

void 
usage(char *cmd)
{
  cerr << "Usage: "
       << cmd
       << " [<options>] <gene tree> <species tree> [<gene-species mapping>]\n"
       << "\nCompute the likelihood of gene tree given species tree.\n";
  
  cerr << "\nOptions:\n\
   -u, -h     This text.\n\
\n\
   What to do: MCMC performed by default with options under 'For MCMC' below.\n\
   -L         Output likelihood only. This invalidates the -i and -t options.\n\
              Be sure that the rates are reasonable, perhaps by using -r.\n\
   -M         Output Matlab file showing likelihood surface. Rates are taken\n\
              as from 0.1 to 10 times the default birth rate. \n\
   -S         Sample a reconciliation.\n\
\n\
   Parameters:\n\
   -G         Turn off MCMC integration of all tree rootings\n\
   -s <int>   Seed for pseudo-random number generator. If set to 0 (default),\n\
              the process id is used as seed.\n\
   -r <birthrate> <deathrate>\n\
              Start values for birth and death rate. Without this option, a\n\
              rough guess is made regarding good start values.\n\
   -b <float> Beta parameter of the exponential distribution for root arc \n\
              length. Default is " << DefaultBeta << ".\n\
\n\
   For MCMC:\n\
   -o <file>  Give a file to write to.\n\
   -t <int>   Thinning. Print state every <int> iteration. (Default " << Thinning << ")\n\
   -i <int>   Iterations. The number of MCMC iterations to perform. (" << MaxIter << ")\n\
\n\
   -q         Do not output diagnostics to stderr.\n\
   -D         Debug info.\n\
";
    exit(1);
}



void
writeLikelihoodMatlabMesh(BirthDeathProbs &bdp, MultiFamReconMCMC &M)
{
  Real br, dr;
  float from = 0.1;
  float to   = 5.0;
  float step = 0.1;
  

  bdp.getRates(br, dr);

  cout << "x = [";
  for (float mul = from; mul < to; mul += step)
    {
      cout << mul * br << "  ";
    }

  cout << "];\ny = x;\n";
  cout << "Z = [";

  //	cout << "# BirthRate\tDeathRate\tE[#genes]\n";
  for (float b_mul = from; b_mul < to; b_mul += step)
    {
      for (float d_mul = from; d_mul < to; d_mul += step)
	{
	  bdp.setRates(br * b_mul, br * d_mul); // Using br twice for symmetry
	  Probability L = M.currentStateProb();
	  cout << L << "  ";
	}
      cout << ";\n";
    }
  cout << "];\n";
  cout << "surfc(x, y, Z'); xlabel('Birth rate'); ylabel('Death rate');";

}




int
main (int argc, char **argv) {
  bool quiet = false;
  bool show_debug_info = false;
  bool MustChooseRates = true;
  enum {do_mcmc,		// Standard operation
	do_likelihood,		// Output only likelihood for current rates
	do_matlabmesh,		// Produce likelihood output for Matlab mesh;
	do_sampling,		// Sample a reconciliation
  } operation = do_mcmc;
  bool only_likelihood = false;
  bool likelihood_mesh = false;
  double BirthRate = DefaultBirthRate;
  double DeathRate = DefaultDeathRate;
  Real Beta = DefaultBeta;



  if (argc < 3) {
    usage(argv[0]);
  }

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') {
    switch (argv[opt][1]) {
    case 'h':
    case 'u':
      usage(argv[0]);
      break;

    case 'G':
      reroot_on = false;
      break;

    case 'D':
      show_debug_info = true;
      break;

    case 'q':
      quiet = true;
      break;

//     case 'e':
//       show_expected_num_genes = true;
//       break;

    case 'L':
      operation = do_likelihood;
      break;

    case 'M':
      operation = do_matlabmesh;
      break;

    case 'S':
      operation = do_sampling;
      break;

    case 'b':
	if (++opt < argc) 
	  {
	    if (sscanf(argv[opt], "%lf", &Beta) == 0)
	      {
		cerr << "Expected number after option '-b'\n";
		usage(argv[0]);
	      }
	  }
      break;

    case 'r':
      {
	if (++opt < argc) 
	  {
	    BirthRate = atof(argv[opt]);
	    if (++opt < argc) 
	      {
		DeathRate = atof(argv[opt]);
	      }
	    else
	      {
		cerr << "Error: Expected a gene loss rate (death rate)\n";
		usage(argv[0]);
	      }
	    MustChooseRates = false;
	  }
	else
	  {
	    cerr << "Expected gene duplication and loss rates (birth and death rates) for option '-r'!\n";
	    usage(argv[0]);
	  }
      }
      break;

    case 't':
      if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	{
	  cerr << "Expected integer after option '-t'\n";
	  usage(argv[0]);
	}
      break;

    case 'i':
      if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	{
	  cerr << "Expected integer after option '-i'\n";
	  usage(argv[0]);
	}
      break;

    case 's':
      if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	{
	  cerr << "Expected integer after option '-s'\n";
	  usage(argv[0]);
	}
      break;

    case 'o':
      if (++opt < argc)
	{
	  outfile = argv[opt];
	}
      else
	{
	  cerr << "Expected filename after option '-o'\n";
	  usage(argv[0]);
	}
      break;

    default:
      cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
      usage(argv[0]);
    }

    opt++;
  }

  vector<Tree> GV;
  Tree S;  
  StrStrMap gs;
  vector<SetOfNodes> AC(MaxNumberOfNodes); // Warning, arbitrary choice!


  try {
    // First read the data, with or without an extra file of gene->species mapping.
    TreeIO io;
    if (opt + 3 == argc)
      {
	string gf(argv[opt++]);
	io.setSourceFile(gf);
	GV = io.readAllGeneTrees();
	
	string sf(argv[opt++]);
	io.setSourceFile(sf);
	S = io.readSpeciesTree();
	Node *r = S.getRootNode();
	if (r->getTime() == 0) {
	  r->setTime(S.rootToLeafTime() / 2);
	}

	gs = TreeIO::readGeneSpeciesInfo(string(argv[opt++]));
      }
    else if (opt + 2 == argc)
      {
	string gf(argv[opt++]);
	io.setSourceFile(gf);
	GV = io.readAllGeneTrees(&gs);
	
	string sf(argv[opt++]);
	io.setSourceFile(sf);
	S = io.readSpeciesTree();
      }
    else
      {
	cerr << "Error: Two tree files expected as last arguments." << endl;
	usage(argv[0]);
      }

    //     cerr << "Gene tree:\n";
    //     G->debug_print();
    //     cerr << "Species tree:\n";
    //     S->debug_print();


    PRNG R;
    if (RSeed == 0) {
      R.setSeed(RSeed);
    }

    DummyMCMC              DM;	// This is stupid. Why can't I replace DM with the actual call?
    IntegralBirthDeathMCMC bd_proc(DM,
				   S, BirthRate, DeathRate, Beta);
    MultiFamReconMCMC      recon(bd_proc, GV, gs, bd_proc, reroot_on);
    if (MustChooseRates) {
      recon.chooseStartingRates();
    }

    if (!(operation == do_likelihood) && !quiet) {
      cerr << "% Starting point: " << recon.strRepresentation() << endl;
    }  

    switch (operation)
      {
      case do_likelihood:
	cout << recon.initStateProb() << endl;
	break;

      case do_matlabmesh:
	writeLikelihoodMatlabMesh(bd_proc, recon);
	break;

      case do_sampling:
	{
	  cerr << "Not implemeted yet! Must support multiple trees.\n";
// 	  ReconciliationSampler smodel(GV, S, gs, bd_proc, R);
// 	  smodel.calculateDataProbability(); // To init stuff

// 	  pair<GammaMap,Probability> sample = smodel.getSampleAndProb();
// 	  cout << sample.second
// 	       << "\t"
// 	       << TreeIO::ATVNewickString(*G.getRootNode(), sample.first)
// 	       << endl;

// 	  pair<beep::GammaMap,Probability> sample2 = smodel.getSampleAndProb();
// 	  if (sample.first == sample2.first)
// 	    {
// 	      cout << "Two identical samples!\n";
// 	    }
// 	  else
// 	    {
// 	      cout << "Two distinct gamma samples\n";

// 	      if (sample.first < sample2.first) {
// 		cout << "The first one is under the second.\n";
// 		cout << sample.first << "\n\n\n" << sample2.first << endl;
// 	      }
// 	      else if (sample2.first < sample.first)
// 		{
// 		cout << "The second one is under the second.\n";
// 		}
// 	      else
// 		{
// 		  cout << "Ouch! Something is fishy!\n";
// 		}
		  
// 	    }

	}
	break;

      default:			// Default: Do MCMC
	SimpleMCMC metropolis(recon, Thinning);
      
	if (quiet)
	  {
	    metropolis.setShowDiagnostics(false);
	  }

	if (outfile != NULL)
	  {
	    try 
	      {
		metropolis.setOutputFile(outfile);
	      }
	    catch(AnError e)
	      {
		e.action();
	      }
	    catch (int e)
	      {
		cerr << "Problems opening output file! ('"
		     << outfile
		     << "') Using stdout instead.\n";
	      }
	  }
  
	if (!quiet) 
	  {
	    cerr << "% Start MCMC\n";
	  }

	// 
	// Do the work
	//
	metropolis.iterate(MaxIter);
  
	//
	// Done!
	//
  
	cout << "# Acceptance ratio: " << recon.getAcceptanceRatio() << endl;
	if (!quiet)
	  {
	    cerr << recon.getAcceptanceRatio()
		 << " = acceptance ratio\n";
	  }
  
	if (recon.getAcceptanceRatio() == 0) 
	  {
	    cerr << "Warning! MCMC acceptance ration was 0.\n";
	       }
      }
  }
  catch (AnError e) {
    e.action();
  }

 
  return EXIT_SUCCESS;
}

