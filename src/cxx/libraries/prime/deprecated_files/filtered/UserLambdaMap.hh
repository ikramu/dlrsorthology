#ifndef USERLAMBDAMAP_HH
#define USERLAMBDAMAP_HH

#include "LambdaMap.hh"


namespace beep
{

  //--------------------------------------------------------------------
  //
  //! UserLambdaMap extends the \f$ \sigma \f$ map between gene node and 
  //! species node, so that user can include additional info, ugs, about 
  //! ancestral nodes maps.
  //!
  //! lambda is then instead defined as follows
  //! 1. If \f$ u \in leaves(G) \f$ then \f$ \lambda(u) = s \in leaves(S)\f$,
  //!     in the natural way.
  //! 2. If v and w are children of u, and \f$ v \in ugs \f$, then 
  //!    \f$ \lambda(u) = MRCA(ugs(v), lambda(w)) \f$.
  //! 3. Otherwise, \f$ \lambda(u) = MRCA(lambda(left(u)), 
  //!    lambda(right(u)))\f$.
  //
  //--------------------------------------------------------------------
  class UserLambdaMap : public LambdaMap
  {
  public:
    //! Set up LambdaMap, gs is a leaf-to-leaf map and ugs (user's gs)
    //! holds additional info about ancestral node maps.
    UserLambdaMap(Tree &G, Tree &S, const StrStrMap &gs);//, const StrStrMap &ugs);
    virtual ~UserLambdaMap();
    
    void update(Tree& G, Tree& S);

    //-----------------------------------------------------------------
    //
    // Implementation
    //
    //-----------------------------------------------------------------
  protected:
    NodeVector ags; //! Ancestral node-to-node map

    //! Compute the rest of lambda
    Node* recursiveLambda(Node *g, Tree &S, const StrStrMap& gs); 

    //! Compute the new lambda using existing lambda
    Node* recursiveLambda(Node *g, Tree &S); 
    Node* compLeafLambda(Node *g, Tree &S, const StrStrMap &gs); 
  };

}// end namespace beep


#endif
