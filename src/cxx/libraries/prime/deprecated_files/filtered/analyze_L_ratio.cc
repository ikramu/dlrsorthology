#include <iostream>

#include "DupSpecModel.hh"
#include "PRNG.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"

using namespace beep;

void 
usage(char *cmd)
{
    cerr << "Usage: "
      	 << cmd
	 << " [<options>] <gene tree 1> <gene tree 2> <species tree>\n"

	 << "The gene trees are assumed to contain gene-to-species markup\
using an 'S' tag in the 'NHX' field. This is default output from e.g. ortho_mcmc.\
\
";
    exit(1);
}


void
writeExpGenesMatlabMesh(DupSpecModel &M1, DupSpecModel &M2)
{
  const double start_rate= 0.0001;
  const double step_size = 0.001;
  const double end_rate  = 0.05;
  const double max_expectance = 1000.0; // Give up when larger values than this!

  cout << "x = [";
  for (double b = start_rate; b < end_rate; b += step_size)
    cout << b << "  ";

  cout << "];\ny = x;\n";
  cout << "Z = [";

  //	cout << "# BirthRate\tDeathRate\tE[#genes]\n";
  for (double b=start_rate; b < end_rate; b += step_size)
    {
      for (double d=start_rate; d < end_rate; d += step_size)
	{
	  M1.setRates(b, d);
	  M2.setRates(b, d);
	  try 
	    {
	      Probability numerator(M1.currentStateProb());
	      Probability denominator(M2.currentStateProb());
	      Probability ratio = numerator / denominator;
	      if (ratio <= max_expectance) {
		cout << ratio << "  ";
		//		    cout << b << "\t" << d << "\t" << ratio << endl;
	      } else {
		cout  << max_expectance << "  ";
	      }
	    }
	  catch (AnError e) 
	    {
	      cout << "0.0  ";
	      e.action();
	    }
	}
      cout << ";\n";
    }
  cout << "];\nmesh(x, y, Z');\
xlabel('Birth rate'); \
ylabel('Death rate');\
title('L(1st) / L(2nd)')\n";
}




int
main (int argc, char **argv) {

  if (argc < 3) {
    usage(argv[0]);
  }


  int opt=1;
  Tree *G1, *G2;
  Tree *S;  
  StrStrMap gs1;
  StrStrMap gs2;
  vector<SetOfNodes> AC1(100);	// Warning, arbitrary choice!
  vector<SetOfNodes> AC2(100);

  try {
   // First read the data
    G1 = TreeIO::readGeneTree(argv[opt++], &AC1, &gs1);
    G2 = TreeIO::readGeneTree(argv[opt++], &AC2, &gs2);
    string sf(argv[opt++]);
    S = TreeIO::readSpeciesTree(sf);

    cerr << "Gene tree:\n";
    G1->debug_print();
    cerr << "Species tree:\n";
    S->debug_print();
  }
  catch (AnError e) {
    e.action();
  }

  PRNG R(0);			// We don't actually need it!

  DupSpecModel M1(*G1, *S, gs1, R);
  DupSpecModel M2(*G2, *S, gs2, R);
  M1.setGamma(AC1);
  M2.setGamma(AC2);

  cerr << M1.getGamma();

  writeExpGenesMatlabMesh(M1, M2);
}

