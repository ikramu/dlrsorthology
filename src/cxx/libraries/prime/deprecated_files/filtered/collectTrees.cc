#include "AnError.hh"
#include "TreeIO.hh"

#include <sstream>

// Global options
//-------------------------------------------------------------
int nParams = 4;

char* outfile=NULL;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if (argc < nParams) 
    {
      usage(argv[0]);
      exit(1);
    }
  try
    {
      
      //---------------------------------------------------------
      // Read input and options
      //---------------------------------------------------------

      // Check for options
      //-----------------------------------
      int opt = readOptions(argc, argv);

      //Get the trees
      //---------------------------------------------
      string host(argv[opt++]);
      cerr << "host = " << host << endl;
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  
      if(S.getName() == "Tree")
	{ 
	  S.setName("S");
	}
      ostringstream oss;
      while(opt+1 < argc)
	{
	  string guest(argv[opt++]);
	  cerr << "guest = " << guest << endl;
	  TreeIO io = TreeIO::fromFile(guest);
	  StrStrMap gs;
	  Tree G = io.readGuestTree(0, &gs); 
	  if(G.getName() == "Tree")
	    { 
	      G.setName("G");
	    }

	  if(gs.size() == 0)
	    {
	      if(opt > argc)
		{
		  cerr << "gs was not present in guest tree, "
		       << "therefore I expected a <gs> argument\n";
		  usage(argv[0]);
		}
	      gs = TreeIO::readGeneSpeciesInfo(argv[opt++]);
	    }
	  LambdaMap l(G, S, gs);
	  GammaMap gamma = GammaMap::MostParsimonious(G, S, l);

	  oss << TreeIO::writeGuestTree(G, &gamma) << ";\n";
	}
      cout << oss.str();
    }      
  catch(AnError& e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception& e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }
  return(0);
};


void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <guest tree> <host tree> [<gene-species map<]\n"
    << "\n"
    << "Parameters:\n"
    << "   <datafile>         Name of file containing sequences in FastA or\n"
    << "                      Genbank format.\n"
    << "   <species tree>     Species tree in Newick format. Branchlengths\n"
    << "                      are important and represent time.\n"
    << "   <gene-species map> Optional. This file contains lines with a\n"
    << "                      gene name in the first column and species\n"
    << "                      name as found in the species tree in the\n"
    << "                      second. You can also choose to associate the \n"
    << "                      genes with species in the gene tree. Please\n"
    << "                      see documentation.\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         Output file\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  

	

