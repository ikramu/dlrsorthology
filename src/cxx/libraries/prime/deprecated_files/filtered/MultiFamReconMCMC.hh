#ifndef MULTIFAMRECONMCMC_HH
#define MULTIFAMRECONMCMC_HH

//
// MCMC interface to MultiFamReconModel
//

#include "BranchSwapping.hh"
#include "MultiFamReconModel.hh"
#include "StdMCMCModel.hh"

namespace beep
{
  class MultiFamReconMCMC : public StdMCMCModel,
			    public MultiFamReconModel
  {
  public:
    MultiFamReconMCMC(MCMCModel &prior,
		      std::vector<Tree> &GV,
		      StrStrMap         &gs,
		      BirthDeathProbs   &kendall,
		      bool reroot_on=false);
    MultiFamReconMCMC(const MultiFamReconMCMC &M);
    ~MultiFamReconMCMC();

  //---------------------------------------------------------------------
  //
  // Implementing the StdMCMCModel interface
  //
  MCMCObject  suggestOwnState();
  void        commitOwnState();
  void        discardOwnState();
  std::string ownStrRep() const;
  std::string ownHeader() const;
  Probability updateDataProbability();


  //-------------------------------------------------------------------
  // I/O
  friend std::ostream& 
  operator<<(std::ostream &o, const MultiFamReconMCMC& m)
  {
    return o << "MultiFamReconMCMC: Implementing MCMC methods for\n"
	     << "MultiFamReconModel.\n"
	     << std::endl;
  };

  //---------------------------------------------------------------------
  //
  // Attributes
  //
private:
    bool reroot_on;		// Are we doing re-rooting?
    unsigned n_trees;		// Number of trees to work with
    Tree oldG;			// Last version of the tree that got rerooted
    unsigned ti;		// Tree index of "active" tree
    // Wee need some help to manipulate the trees:
    //-------------------------------------------------------------------
    BranchSwapping mrGardener;

};

}//end namespace beep


    

#endif
