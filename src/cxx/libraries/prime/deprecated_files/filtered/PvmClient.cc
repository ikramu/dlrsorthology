#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include "PvmClient.hh"

using namespace PvmBeep;
using namespace std;

extern "C" {
#include "pvm3.h"
}

PvmClient::PvmClient() 
  : parentId(pvm_parent())
{
  // Request notification of dead parents!
  //  pvm_notify(PvmTaskExit, PvmBeep::ProcessIsDead, 1, &parentId);
  //  pvm_notify(PvmHostDelete, PvmBeep::ProcessIsDead, 1, &parentId);

#ifdef DISTR_DEBUG
  cerr.rdbuf(debug_out.rdbuf());
#endif
  
}
PvmClient::~PvmClient() 
{
  
}


void
PvmClient::sendToMaster(enum MessageTypes mt)
{
  send(parentId, mt);
}

void PvmClient::saveDeathMessage(const string& msg)
{
  ofstream fout;
  ostringstream oss;
  oss << "client" << pvm_mytid() <<".txt";

  fout.open(oss.str().c_str());
  fout << msg << endl;
  fout.close();
}
