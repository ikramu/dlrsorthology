#include <iostream>

#include "GammaTreeGenerator.hh"
#include "PRNG.hh"
#include "StrStrMap.hh"
#include "TreeIO.hh"

void 
usage(char *cmd)
{
    cerr << "Usage: "
      	 << cmd
	 << " [<options>] <species tree>\n";
    exit(1);
}

const int iterations = 500;

void
writeExpGenesMatlabMesh(Tree *S, DupSpecProbs &dsp, PRNG &R)
{
  const double start_rate= 0.0001;
  const double step_size = 0.05;
  const double end_rate  = 1.0;


  cout << "x = [";
  for (double b = start_rate; b < end_rate; b += step_size)
    cout << b << "  ";

  cout << "];\ny = x;\n";
  cout << "Z = [";

  //	cout << "# BirthRate\tDeathRate\tE[#genes]\n";
  for (double b=start_rate; b < end_rate; b += step_size)
    {
      for (double d=start_rate; d < end_rate; d += step_size)
	{
	  dsp.setRates(b, d);
	  
	  float n_leaves = 0;
	  for (int i=0; i<iterations; i++) 
	    {
	      GammaTreeGenerator gtg(S, dsp, R);
	      gtg.simulateGeneTree();
	      Tree *G = gtg.exportGeneTree();
	      do {
		delete G;
		GammaTreeGenerator gtg(S, dsp, R);
		gtg.simulateGeneTree();
		G = gtg.exportGeneTree();
	      } while (G->getNumberOfLeaves() < S->getNumberOfLeaves()); 
	      n_leaves += G->getNumberOfLeaves();
	      delete G;
	    }
	  cout << n_leaves / iterations << "  ";
	}
      cout << ";\n";
    }
  cout << "];\nmesh(x, y, Z');\
xlabel('Birth rate'); \
ylabel('Death rate');\
title('Average number of genes')\n";
}




int
main (int argc, char **argv) {

  if (argc != 2) {
    usage(argv[0]);
  }


  int opt=1;
  Tree S;  



  try {
   // First read the data
    string sf(argv[1]);
    S = TreeIO::readSpeciesTree(sf);
  }
  catch (AnError e) {
    e.action();
  }

  PRNG R;
  DupSpecProbs dsp(S, 0.001, 0.001);

  writeExpGenesMatlabMesh(&S, dsp, R);
}

