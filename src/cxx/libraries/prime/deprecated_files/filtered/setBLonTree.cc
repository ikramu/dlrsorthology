#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "AnError.hh"
#include "TreeIO.hh"

// Global variables
//------------------------------------
char* outfile=NULL;
std::map<unsigned, beep::Real> lengths;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < 6) 
    {
      usage(argv[0]);
    }

  try
    {
      int opt = readOptions(argc, argv);
      if(opt + 1 > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	  exit(1);
	}

      //Get tree 
      //---------------------------------------------
      string treefile(argv[opt++]);
      TreeIO io = TreeIO::fromFile(treefile);
      Tree G = io.readBeepTree(false, false, false, 
			       false, 0, 0);

      // Set any user defined lengths
      if(lengths.empty() == false)
	{
	  Node* n = 0; 
	  Node* p = 0; 
	  if(lengths.size() < G.getNumberOfNodes() - 2 ||
	     lengths.size() > G.getNumberOfNodes() - 1)
	    {
	      cerr << "\nError: The number of edge lengths times, "
		   << lengths.size()
		   << " should be equal or one less than number of "
		   << "internal nodes (except root) in tree, "
		   << G.getNumberOfNodes() - 1
		   << " \n\n";
	      exit(23);
	    }
	  for(map<unsigned, Real>::const_iterator i = lengths.begin();
	      i != lengths.end(); i++)
	    {
	      if(i->first >= G.getNumberOfNodes())
		{
		  cerr << "\nError: Trying to set length for vertex "
		       << i->first
		       << ", which do not exist in tree\n\n";
		  exit(23);
		}
	      n = G.getNode(i->first);
	      p= n->getParent();
	      if(n->isRoot())
		{
		  cerr << "\nWarning: The length submitted for root "
		       << n->getNumber()
		       << " will be ignored! Root's length = 0.0\n\n";
		}
	      else 
		{
		  if(p->isRoot())
		    {
		      cerr << "\nWarning: The length, "
			   << i->second
			   << ", given for root's "
			   << "child node "
			   << n->getNumber()
			   << "\nwill overwrite the length of root's "
			   << "child node "
			   << n->getSibling()->getNumber() 
			   << " as these are assumed to be equal!\n\n";
		      n->getSibling()->setLength(i->second);
		    }
		  n->setLength(i->second);
		}
	    }
	  G.getRootNode()->setLength(0.0);
	}
      cerr << G.print(false, false, true) << endl;
      cout << TreeIO::writeNewickTree(G) << endl;

    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
  catch(exception e)
    {
      cout <<" error\n"
	   << e.what();
    }
}

	      
void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " <branch lengths> <treefile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <branch lengths>   User defined edge rates for tree in tree file\n"
    << "                      Root's rate is ignored. If rates are given for\n"
    << "                      both root'children, one is ignored. Must be in\n"
    << "                      form { <node#=length> ... <node#=length> }.\n"
    << "                      Note spaces after and before '}'\n"
    << "   <treefile>         a string\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  if (string(argv[opt++])  == "{")
    {
      string s = argv[opt++];
      while(s != "}")
	{
	  unsigned key = atoi(s.substr(0, s.find("=")).c_str());
	  Real data = atof(s.substr(s.find("=")+1).c_str());
	  lengths[key] = data;
	  s = argv[opt++];
	}
      if(lengths.empty())
	{
	  cerr << "Set of branch lengths expected\n";
	  usage(argv[0]);
	  exit(1);
	}
    }
  else
    {
      cerr << "Expected branch lengths as strings\n";
      usage(argv[0]);
      exit(1); //Check what error code should be used!
    }
  return opt;
}
 
