#include "HybridHostTreeMCMC.hh"

#include "AnError.hh"
#include "BDHybridTreeGenerator.hh"
#include "MCMCObject.hh"
#include "HybridTreeIO.hh"
#include "TreeIOTraits.hh"

#include <sstream>
#include <algorithm>
namespace beep
{
  using namespace std;
  //----------------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //----------------------------------------------------------------
  HybridHostTreeMCMC::HybridHostTreeMCMC(MCMCModel& prior,
					 HybridTree& hs, 
					 unsigned maxGhosts)
    : StdMCMCModel(prior, 3, // dummy value
		   hs.getName() + "_hybridModel"),
      HybridHostTreeModel(hs, 1.0, 1.0, 1.0, maxGhosts),
      oldValue(1.0),
      oldS(),
      oldTimes(0),
      oldRates(0),
      oldLengths(0),
      fixRates(false),
      treeFixed(false),
      suggestion_variance(0.1 * (lambda + mu + rho) / 3.0)
  {
    // update StdMCMCModel, now that we know more about #parameters
    n_params = 3 + nodeOrder.size(); // 3 rates + tree + event times except root
    updateParamIdx();
    initParameters();
  }

  HybridHostTreeMCMC::~HybridHostTreeMCMC()
  {}
    
  HybridHostTreeMCMC::HybridHostTreeMCMC(const HybridHostTreeMCMC& hhtm)
    : StdMCMCModel(hhtm),
      HybridHostTreeModel(hhtm),
      oldValue(hhtm.oldValue),
      oldS(hhtm.oldS),
      oldTimes(hhtm.oldTimes),
      oldRates(hhtm.oldRates),
      oldLengths(hhtm.oldLengths),
      fixRates(hhtm.fixRates),
      treeFixed(hhtm.treeFixed),
      suggestion_variance(hhtm.suggestion_variance)
  {
  }
    
  HybridHostTreeMCMC 
  HybridHostTreeMCMC::operator=(const HybridHostTreeMCMC& hhtm)
  {
    if(&hhtm != this)
      {
	StdMCMCModel::operator=(hhtm);
	HybridHostTreeModel::operator=(hhtm);
	oldValue = hhtm.oldValue;
	oldS = hhtm.oldS;
	oldTimes = hhtm.oldTimes;
	oldRates = hhtm.oldRates;
	oldLengths = hhtm.oldLengths;
	fixRates = hhtm.fixRates;
	treeFixed = hhtm.treeFixed;
	suggestion_variance = hhtm.suggestion_variance;
      }
    return *this;
  }


  //----------------------------------------------------------------
  //
  // Interface
  //
  //---------------------------------------------------------------- 
  void 
  HybridHostTreeMCMC::fixBDHparameters(Real newLambda, Real newMu, Real newRho)
  {
    lambda = newLambda;
    mu = newMu;
    rho = newRho;
    // updatenparams if needed
    if(fixRates == false)
      {
	n_params -= 3;
	fixRates = true;
	updateParamIdx();
      }
  }
  
  void 
  HybridHostTreeMCMC::fixTree()
  {
    if(treeFixed == false)
      {
	n_params--;
	treeFixed = true;
	updateParamIdx();
 	update();
      }
    assert(treeFixed);
  }
  
  MCMCObject 
  HybridHostTreeMCMC::suggestOwnState()
  {
    Idx = R.genrand_modulo(n_params);
    // Create a default MCMCObject (proposal_ratio = 1.0)
    MCMCObject MOb;

    // Perform the chosen suggest
    // NOTE! Currently cannot perturb model parameters or tree
    if(fixRates == false && Idx == n_params-1)
      {
	assert(Idx != 0);    //check for sane limits!
	  
	oldValue = lambda;
	  
	lambda = perturbLogNormal(oldValue,
				  suggestion_variance,
				  Real_limits::min(), 
				  5.0,
				  MOb.propRatio);
      }
    else if(fixRates == false && Idx == n_params - 2)
      {
	oldValue = mu;
	  
	mu = perturbLogNormal(oldValue,
			      suggestion_variance,
			      Real_limits::min(), 
			      5.0,
			      MOb.propRatio);
      }
    else if(fixRates == false && Idx == n_params - 3)
      {
	oldValue = rho;
	  
	rho = perturbLogNormal(oldValue,
			       suggestion_variance,
			       Real_limits::min(), 
			       5.0,
			       MOb.propRatio);
      }
    else if(treeFixed == false && Idx >= n_params - 1)
      {
	throw AnError("Tree-swapping not yet unctional", 1);
	MOb.propRatio = calculateDataProbability();
	BDHybridTreeGenerator treeMaker(lambda, mu, rho);
	treeMaker.setTopTime(S->getTime(*S->getRootNode()) + S->getTopTime());

	oldS = *S;
	do
	  {
	    treeMaker.generateHybridTree(*S);
	  }
	while(S->getNumberOfLeaves() != oldS.getNumberOfLeaves());

	MOb.propRatio /= updateDataProbability();
	S->perturbedNode(S->getRootNode());
      }
    else 
      {
	// Must be possible to do this in a better way Kolla i stl bok
	std::map< Real, std::pair<Node*, unsigned> >::iterator i = 
	  nodeOrder.begin();
	for(unsigned j = 0; j < Idx ; j++, i++)
	  {} // Do nothing just step through

// 	std::map< Real, std::pair<Node*, unsigned> >::iterator i = 
// 	  nodeOrder.begin();
// 	advance(i,Idx);

	idxNode = i->second.first; // the node to perturb
	// Check sanity
	assert(idxNode != 0);
	assert(idxNode->isLeaf() == false);
	assert(idxNode->isRoot() == false);

	oldValue = S->getTime(*idxNode);

	// find limits -- hybrid and autoploids are special cases
	unsigned strict = 0; // by default use strict limits
	Real lower = std::max(S->getTime(*idxNode->getLeftChild()),
			      S->getTime(*idxNode->getRightChild()));
	Real upper = S->getTime(*idxNode->getParent());

	// If hybridization other parent will affect limits
	Node* h = S->getHybridChild(*idxNode);
	if(h)
	  {
	    Node* op = h->getParent();
	    if(idxNode == op)
	      {
		op = S->getOtherParent(*h);
	      }
	    lower = std::max(std::max(S->getTime(*op->getLeftChild()),
				      S->getTime(*op->getRightChild())), lower);
	    upper = std::min(S->getTime(*op->getParent()), upper);

	    // If autoploid edge time may be zero
	    if(S->isExtinct(*h->getSibling()) || 
	       S->isExtinct(*S->getOtherSibling(*h)))
	      {
		strict = 1; // non-strict upper limit = parent time 
	      }
	  }

	// Now perturb the time
	Real t = perturbLogNormal(oldValue, suggestion_variance,
				  lower, upper, MOb.propRatio, strict);
	// 	  nodeOrder.erase(S->getTime(*idxNode));
	S->setTime(*idxNode, t);
	S->perturbedNode(idxNode);
      }
      
    MOb.stateProb = updateDataProbability();
      
    return MOb;
  }
    
  void 
  HybridHostTreeMCMC::commitOwnState()
  {
    S->perturbedNode(NULL);
  }
    
  void 
  HybridHostTreeMCMC::discardOwnState()
  {
    if(fixRates == false && Idx == n_params - 1)
      {
	lambda = oldValue;
      }
    else if(fixRates == false && Idx == n_params - 2)
      {
	mu = oldValue;
      }
    else if(fixRates == false && Idx == n_params - 3)
      {
	rho = oldValue;
      }
    else if(treeFixed == false && Idx >= n_params - 1)
      {
	throw AnError("Tree-swapping not yet unctional", 1);
	*S = oldS;
      }
    else 
      {
	S->setTime(*idxNode, oldValue);
      }
  }
    
  std::string 
  HybridHostTreeMCMC::ownStrRep() const
  {
    std::ostringstream oss;
    if(fixRates == false)
      {
	oss << lambda << ";\t" << mu << ";\t" << rho << ";\t";
      }
    if(treeFixed == false)
      {
	TreeIOTraits traits;
	oss << HybridTreeIO::writeHybridTree(*S, traits, NULL) << ";\t";
	traits.setNT(true);
	oss << HybridTreeIO::writeHybridTree(*S, traits, NULL) << ";\t";
      }
    else
      {
         for(map<Real,pair<Node*,unsigned> >::const_iterator i = nodeOrder.begin(); 
	     i != nodeOrder.end(); i++)
	   {	
	     oss << i->first <<";\t";
	   }
      }
    
    return oss.str();
  }
  
  std::string 
  HybridHostTreeMCMC::ownHeader() const
  {
    std::ostringstream oss;
    if(fixRates == false)
      {
	oss << "lambda(float);\tmu(float);\trho(float);\t";
      }
    if(treeFixed == false)
      {
	oss << "S(tree);\t";
	oss << "S_times(tree);\t";
      }
    else
      {
	for(map<Real, pair<Node*,unsigned> >::const_iterator i = nodeOrder.begin();
	    i != nodeOrder.end(); i++)
	  {	
	    oss << "nodeTime[" << i->second.first->getNumber() << "](float);\t";
	  }
      }
    return oss.str();
  }
    
  Probability 
  HybridHostTreeMCMC::updateDataProbability()
  {
    if(S->perturbedNode())
      {
	update(); 
      }
    return calculateDataProbability();
  }

  std::ostream& 
  operator<<(std::ostream& o, const HybridHostTreeMCMC& hhtm)
  {
    return o << hhtm.print();
  }

  std::string 
  HybridHostTreeMCMC::print() const
  {
    std::ostringstream oss;
    oss << HybridHostTreeModel::print()
	<< StdMCMCModel::print();
    return oss.str();
  }


  //----------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------
  //! \todo{Find better random sampling?}
  void 
  HybridHostTreeMCMC::initParameters()
  {
    lambda = R.genrand_real3() *0.5;
    mu = R.genrand_real3() * 0.5;
    rho = R.genrand_real3() * 0.5;
  }
    
}// end namespace beep
