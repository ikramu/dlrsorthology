#ifndef PRIMEOPTION_HH
#define PRIMEOPTION_HH

#include "AnError.hh"
#include "Beep.hh"
#include "PrimeOptionMap.hh"

#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <typeinfo>
#include <vector>
#include <cstdio>
#include <cxxabi.h>
#include <cassert>

namespace beep
{
  const unsigned MAXPARAMS = std::numeric_limits<unsigned>::max();
//   //! Enum for SeqTypes used in userSusbtitutionModel
   enum {DNA, AminoAcid, Codon};


  // Forward declaration
//   class PrimeOptionMap;

  class PrimeOption
  {
  public:
    PrimeOption(std::string identifier, unsigned nParameters)
      : id(identifier),
	type(),
	usage(),
	nParams(nParameters),
	beenParsed(false)
    {};

    virtual ~PrimeOption(){};

    virtual std::string getId()
    {
      return id;
    }
    virtual unsigned numberOfParameters()
    {
      return nParams;
    }
    virtual bool hasBeenParsed() const
    {
      return beenParsed;
    }
    virtual std::string getUsage() const
    {
      return usage;
    }
    virtual std::string getType() const
    {
      return type;
    }
 
    virtual void setParameters(std::string parameters) = 0;

    friend std::ostream& operator<<(std::ostream& o, const PrimeOptionMap &bo);

    std::string id;      //!< The identifier for this option
    std::string type;    //!< String representation of this option's type
    std::string usage;   //!< Usage message for this option
    unsigned nParams;    //!< Number of Parameters for this option
    bool beenParsed;     //!< if true, option has already been treated
  };




  //Forward declaration
  template<typename T> class TmplPrimeOption : public PrimeOption
  {
  public: 
    //! \name Construct/destruct/assign
    //@{
    //---------------------------------
    TmplPrimeOption(std::string identifier, std::string helpMessage, 
		    unsigned nParams, std::string defaultValues, 
		    std::string validValues = "");
    ~TmplPrimeOption();
    //@}
       
    //---------------------------------------------
    //! \name Interface
    //@{
    //---------------------------------------------
    virtual void setParameters(std::string parameters);
    virtual const std::vector<T> getParameters() const;
    //@}
      
  protected:
    //---------------------------------------------
    //! \name Implementation
    //@{
    //---------------------------------------------
    virtual void parseParams(std::string& inParams, unsigned nParams, 
			     std::vector<T>& paramStore);
    //@}

    //! \name Attributes
    //@{
    //-------------------
    std::string errMsg ;     //!< Error message for this option
    std::vector<T> vals;     //!< Option values initialized with default values
    std::vector<T> validVals;//!< The allowed values, empty if no restriction
    //@}
  };
  
  typedef beep::TmplPrimeOption<bool>          BoolOption; 
  typedef beep::TmplPrimeOption<unsigned>      UnsignedOption; 
  typedef beep::TmplPrimeOption<int>           IntOption; 
  typedef beep::TmplPrimeOption<Real>          RealOption;
  typedef beep::TmplPrimeOption<std::string>   StringOption; 

 
  class UserSubstitutionMatrixOption 
    : public PrimeOption
  {
  public: 
    //! \name Construct/destruct/assign
    //@{
    //---------------------------------
    UserSubstitutionMatrixOption(std::string identifier, 
				 std::string helpMessage, 
				 unsigned nParams, 
				 std::string defaultValues, 
				 std::string validValues = "");
    ~UserSubstitutionMatrixOption();
    //@}
             
    //---------------------------------------------
    //! \name Interface
    //@{
    //---------------------------------------------
    virtual void setParameters(std::string parameters);
    virtual const std::vector<UserSubstMatrixParams> getParameters() const;
    //@}

  protected:
    //---------------------------------------------
    //! \name Implementation
    //@{
    //---------------------------------------------
    void parseParams(std::string& inParams, unsigned nParams, 
		     std::vector<UserSubstMatrixParams>& paramStore);

    //! \name Attributes
    //@{
    //-------------------
    std::string errMsg;      //!< Error message for this option
    std::vector<UserSubstMatrixParams> vals; //!< Option values
    //@}
  };

  //-----------------------------------------------------
  //
  // Implementation of templatized TmplPrimeOption
  //
  //-----------------------------------------------------

  // Construct/destruct/assign
  //---------------------------------
  template<typename T>
  TmplPrimeOption<T>::TmplPrimeOption(std::string identifier, 
		  std::string helpMessage, 
		  unsigned nParameters, 
		  std::string defaultValues, 
		  std::string validValues)
    : PrimeOption(identifier, nParameters), 
      errMsg(),
      vals(),
      validVals()
  {
    // fix type
    T tmp;
    type = typeid2typestring(typeid(tmp).name());

    // fix usage message and default and valid parameters (if existing)
    std::ostringstream helptxt;
    std::ostringstream helpid;
    helpid << "-" << id;
    for(unsigned i=0; i<nParameters; i++) {
      helpid << " <";
      if(validValues != "")
	{
	  helpid << validValues;
	  parseParams(validValues, MAXPARAMS, validVals);
	}
      else
	{
	  helpid << getType();
	}
      helpid << ">";
    }

    if(helpMessage != "")
      helptxt << helpMessage << " ";
    if(defaultValues != "")
      {
	helptxt << "Default: " << defaultValues;
	parseParams(defaultValues, nParams, vals);
      }
    usage = PrimeOptionMap::formatMessage(helpid.str(), helptxt.str());

    // Fix error message
    std::ostringstream oss;
    oss << "Expected ";
    if(nParams == 1)
      {
	oss << " a " << getType();
      }
    else 
      {
	if(nParams == MAXPARAMS)
	  oss << nParams;
	else
	  oss << " a sequence of ";
	oss << getType() << "s";
      }
      oss << " after option -" << id << "!";
      errMsg = oss.str();
  }
  
  template<typename T>
  TmplPrimeOption<T>::~TmplPrimeOption()
  {}
  
  //---------------------------------------------
  // Interface
  //---------------------------------------------
//   template<typename T>
//   std::string 
//   TmplPrimeOption<T>::getType() const
//   {
//     using namespace abi;
//     if(vals.size() == 0)
//       return "empty";
//     else
//       {
// 	char s[100];
// 	int err; 
// 	size_t n;
// 	abi::__cxa_demangle(typeid(vals.front()).name(),s, &n, &err);
// 	return s;
//       }
//   }
  
  template<typename T>
  void 
  TmplPrimeOption<T>::setParameters(std::string parameters)
  {
    vals.clear();
    parseParams(parameters, nParams, vals);
    beenParsed = true;
  }
  
  template<typename T>
  const std::vector<T> 
  TmplPrimeOption<T>::getParameters() const
  {
    return vals;
  }

  //---------------------------------------------
  // Implementation
  //---------------------------------------------
  template<typename T>
  void 
  TmplPrimeOption<T>::parseParams(std::string& inParams, unsigned nParams, 
				  std::vector<T>& paramStore)
  {
    assert(paramStore.size() == 0);
    std::istringstream iss(inParams);
    T t;
    unsigned nStored = 0;
    while(iss.good())//peek() != EOF)
      {
	iss >> t;
	paramStore.push_back(t);
	nStored ++;
      }

    if(nParams != MAXPARAMS && // MAXPARMS indicate nParams not restricted
       nStored < nParams)
      {
	throw AnError(errMsg, 1);
      }
  }
    
  //-----------------------------------------------------
  //
  // Implementation of templatized UserSubstitutionMatrixOption
  //
  //-----------------------------------------------------

  // Construct/destruct/assign
  //---------------------------------
  UserSubstitutionMatrixOption::
  UserSubstitutionMatrixOption(std::string identifier, 
			       std::string helpMessage, 
			       unsigned nParameters, 
			       std::string defaultValues, 
			       std::string validValues)
    : PrimeOption(identifier, nParameters)
  {
    // Fix type 
    type = "SubstMatrix";

    // fix usage message and default and valid parameters (if existing)
    std::ostringstream helptxt;
    std::ostringstream helpid;
    helpid << "-" << id << " <" << getType() << ">";
    if(helpMessage != "")
      helptxt << helpMessage << " ";
    if(defaultValues != "")
      {
	helptxt << "Default: " << defaultValues;
	parseParams(defaultValues, nParams, vals);
      }
    usage = PrimeOptionMap::formatMessage(helpid.str(), helptxt.str());

    // Fix error message
    std::ostringstream oss;
    oss << "Expected ";
    if(nParams != MAXPARAMS)
      oss << nParams 
	  << " instance(s) of ";
    else
      oss << "a sequence of instances of ";
    oss << "a sequence type identifier ('DNA'/'AminoAcid'/'Codon') "
	<< "and a corresponding Pi matrix of size n "
	<< "and an R matrix of size n(n-1)/2, "
	<< "where 'n' depends on the sequence type (4/20/64), "
	<< "after option -" << id << "!";
    errMsg = oss.str();
  }
  
  UserSubstitutionMatrixOption::~UserSubstitutionMatrixOption()
  {}
  
  //---------------------------------------------
  // Interface
  //---------------------------------------------
  void 
  UserSubstitutionMatrixOption::setParameters(std::string parameters)
  {
    vals.clear();
    parseParams(parameters, nParams, vals);
    beenParsed = true;
  }
  
  const std::vector<UserSubstMatrixParams> 
  UserSubstitutionMatrixOption::getParameters() const
  {
    return vals;
  }

  //---------------------------------------------
  // Implementation
  //---------------------------------------------
  void 
  UserSubstitutionMatrixOption::
  parseParams(std::string& inParams, 
	      unsigned nParams, 
	      std::vector<UserSubstMatrixParams>& paramStore)
  {
    std::istringstream iss(inParams);
    std::vector<Real> tmp;
    std::string s;
    unsigned dim;
    unsigned nStored = 0;
    while(iss.peek() != EOF)
      {
	iss >> s; 
	if(s == "DNA")
	  dim = 4;
	else if(s == "AminoAcid")
	  dim = 20;
	else if(s == "Codon")
	  dim = 64;
	else
	  throw AnError("sequence type '" + s + "' not recognized", 1);
		
	struct UserSubstMatrixParams tmpVal;
	tmpVal.seqtype = s;
	Real r;
	try
	  {
	    //Read Pi
	    for(unsigned i = 0; i < dim; i++)
	      {
 		iss >> r;
		tmp.push_back(r);//std::atof(r.c_str()));
	      }
	    tmpVal.Pi = tmp;
	    tmp.clear();
	  }	
	catch(std::exception e)
	  {
	    throw AnError("Wrong dimension of Pi, should be " + dim, 1);
	  }
	try
	  {
	    //read R
	    for(unsigned i = 0; i < (dim-1)*dim/2; i++)
	      {
 		iss >> r;
		tmp.push_back(r);//std::atof(r.c_str()));
	      }
	    tmpVal.R = tmp;
	    tmp.clear();	
	    
	    paramStore.push_back(tmpVal);
	    nStored ++;
	  }
	catch(std::exception e)
	  {
	    throw AnError("Wrong dimension of R, should be " + 
			  dim*(dim-1)/2, 1);
	  }
      }
    if(nParams != MAXPARAMS && // MAXPARMS indicate nParams not restricted
       nStored < nParams)
      {
	throw AnError(errMsg, 1);
      }
  }
    
  
} // end namespace beep.

#endif ///!PRIMEOPTION_HH//!/
