#include "SubstitutionModel.hh"

#include "Node.hh"
#include "EdgeWeightHandler.hh"
#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "SequenceData.hh"
#include "TransitionHandler.hh"
#include "SiteRateHandler.hh"
#include "Tree.hh"

#include <sstream>
#include <cassert>

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------
  SubstitutionModel::SubstitutionModel(const SequenceData& Data, 
				       const Tree& T, 
				       SiteRateHandler& siteRates, 
				       const TransitionHandler& Q,
				       EdgeWeightHandler& edgeWeights,
				       const vector<string>& partitionsList)
    : D(&Data),
      T(&T),
      siteRates(&siteRates),
      Q(&Q),
      edgeWeights(&edgeWeights),
      partitions()
  {
    // Initiate partitions 
    for(vector<string>::const_iterator i = partitionsList.begin(); 
	i != partitionsList.end(); i++)
      {
	// Copy a PatternVec corresponding to the partition *i
	// from D and save it in partitions
	partitions.push_back(D->getSortedData(*i));
      }
  }
  
  // Copy Constructor
  //----------------------------------------------------------------------
  SubstitutionModel::SubstitutionModel(const SubstitutionModel& sm)
    :D(sm.D),
     T(sm.T),
     siteRates(sm.siteRates),
     Q(sm.Q),
     edgeWeights(sm.edgeWeights),
     partitions(sm.partitions)
  {
  }
  
  // Destructor
  //----------------------------------------------------------------------
  SubstitutionModel::~SubstitutionModel()
  {
  }

  // Assignment operator
  //----------------------------------------------------------------------
  SubstitutionModel& 
  SubstitutionModel::operator=(const SubstitutionModel& sm)
  {
    if(this != &sm)
      {
	D = sm.D;
	T = sm.T;
	siteRates = sm.siteRates;
	Q = sm.Q;
	edgeWeights = edgeWeights;
	partitions = sm.partitions; 
      }
    return *this;
  }
  
  //----------------------------------------------------------------------
  //
  // Public interface
  //
  //----------------------------------------------------------------------

  // Access to Tree
  //----------------------------------------------------------------------
  const Tree& 
  SubstitutionModel::getTree()
  {
    return *T;
  }
  
  // TODO: more access functions? /bens

  // Likelihood calculation - interface to MCMCModels
  //----------------------------------------------------------------------
  Probability 
  SubstitutionModel::calculateDataProbability()
  {
    // reset return value
    like = 1.0;
    // Loop over partitions
    for(unsigned i = 0; i < partitions.size(); i++)
      {
	// First recurse down to initiate likes
	like *= rootLikelihood(i);
      }
    return like;
  }

  void 
  SubstitutionModel::update()
  { 
    edgeWeights->update();
    siteRates->update();
  }

  //------------------------------------------------------------------
  //
  // I/O
  // 
  //------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, 
		   const SubstitutionModel& sm)
  {
    return o << sm.print()
      ;
  }

  // TODO: Shape up these print functions /bens
  std::string
  SubstitutionModel::print() const
  {
    return print(false);
  }

  std::string
  SubstitutionModel::print(const bool& estimateRates) const
  {
    ostringstream oss;
    oss
      << "Substitution likelihood is performed"
      // Here we should maybe only give the part of data that we are using,
      // i.e., the partitions used?
      << " using sequence data:\n"
      // The funciton below should be replaced by D.print(partition)
      // that should write a "name" for the Sequence data and the 
      // user-definedpartition that is used (e.g., either exactly which 
      // positions are defined or the "name" used for the partition
      << indentString(D->print(), "  ")
      << indentString("partitions, any user-defined partitions of the data\n")
      << "and substitution matrix:\n"
      << indentString(Q->print())
      << indentString(edgeWeights->print());
    return oss.str();
  }


  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------
 
  // Helper functions for Likelihood calculation
  // Recurse down to leaves and then updates Likelihoods for each node on 
  // the way up
  //----------------------------------------------------------------------
  // \f$ \{ Pr[D_{i,\cdot}|T_n, w, r, p], r\in siteRates, p \in pv\}\f$
  //----------------------------------------------------------------------
  Probability
  SubstitutionModel::rootLikelihood(const unsigned& partition)
  {
    Node&n = *T->getRootNode();
    if(n.isLeaf())
      {
	return 1.0;
      }
    else
      {
	// Initiate return value and get partition
	Probability ret(1.0);
	PatternVec& pv = partitions[partition];

	// Get nested Likelihoods
	// Note! pl_l doubles as temporary storage
	PatternLike pl_l = recursiveLikelihood(*n.getLeftChild(), partition);
	PatternLike pl_r = recursiveLikelihood(*n.getRightChild(), partition);

	// Lastly, loop over all unique patterns in pv
	for(unsigned i = 0; i < pv.size(); i++)
	  {
	    Probability patternL = 0.0;

	    // Compute Pr[Dk|T, ew, r(j)] for each siteRateCategory j
	    for(unsigned j = 0; j < siteRates->nCat(); j++)
	      {
		LA_Vector& left = pl_l[i][j];
		LA_Vector& right = pl_r[i][j];
		// left.*right, store result in left
		left.ele_mult(right, left);
		// pi * left, store result in right
		Q->multWithPi(left, right);
		patternL += right.sum();
	      } 
	    unsigned exp = pv[i].second; // # of occurrences of pattern
	    ret *= pow(patternL/ siteRates->nCat(), exp); // Pr[rateCat]=1/nCat
	  }
	return ret;
      }
  }

  SubstitutionModel::PatternLike
  SubstitutionModel::recursiveLikelihood(const Node& n, 
					 const unsigned& partition)
  {
    if(n.isLeaf())
      return leafLikelihood(n, partition);
    else
      {
	// Get data
	PatternVec& pv = partitions[partition];

	// Get nested Likelihoods
	// Note! pl_l doubles as temporary storage and pl_r as return value
	PatternLike pl_l = recursiveLikelihood(*n.getLeftChild(), partition);
	PatternLike pl_r = recursiveLikelihood(*n.getRightChild(), partition);

	// Compute Pr[Dk|T, ew, r(j)] for each siteRateCategory j
	for(unsigned j = 0; j < siteRates->nCat(); j++)
	  {
	    // Set up (j-specific) P matrix 
	    Q->resetP(edgeWeights->getWeight(n) * siteRates->getRate(j));
	
	    // Lastly, loop over all unique patterns in pv
	    for(unsigned i = 0; i < pv.size(); i++)
	      {
		LA_Vector& left = pl_l[i][j];
		LA_Vector& right = pl_r[i][j];
		// left.*right, store result in left
		left.ele_mult(right, left);
		Q->mult(left, right);
	      } 
	  }
	return pl_r;
      }
  }
  SubstitutionModel::PatternLike
  SubstitutionModel::leafLikelihood(const Node& n,  
				    const unsigned& partition)
  {
    // Get partition and create the return value 
    PatternVec& pv = partitions[partition];
    PatternLike pl(pv.size(), 
		   RateLike(siteRates->nCat(), 
			    LA_Vector(Q->getAlphabetSize())));
    
    //Loop over rate categories
    for(unsigned j = 0; j < siteRates->nCat(); j++)
      {
	// initiate P 
	Q->resetP(edgeWeights->getWeight(n) * siteRates->getRate(j));
	
	// Loop over each unique pattern in pv
	  for(unsigned i = 0; i < pv.size(); i++)
	    {
	      // Get position of pattern's first occurrence in partition
	      unsigned pos = pv[i].first; 
	      if(!Q->col_mult(pl[i][j], (*D)(n.getName(), pos)))
		Q->mult(D->leafLike(n.getName(),pos), pl[i][j]);
	      // 	      pl[i][j] = P * D->leafLike(n.getName(),pos);
	    }
	}
    return pl;
  }


    
}//end namespace beep
