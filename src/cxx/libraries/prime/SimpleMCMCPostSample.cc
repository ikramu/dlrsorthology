#include "SimpleMCMCPostSample.hh"

#include "AnError.hh"
#include "Hacks.hh"
#include "MCMCObject.hh"

#include <iostream>
#include <sstream>
#include <unistd.h>
#include <cmath>

namespace beep
{

using namespace std;


SimpleMCMCPostSample::SimpleMCMCPostSample(MCMCModel& M, unsigned thin) :
	SimpleMCMC(M, thin)
{
	p = model.initStateProb();   // To set stateProbs.
	model.commitNewState();      // To set old_stateProbs, 
}


SimpleMCMCPostSample::~SimpleMCMCPostSample()
{
}


void
SimpleMCMCPostSample::iterate(unsigned n_iters, unsigned print_factor)
{
	unsigned check_number_nonupdate = 0;
	start_time = time(NULL);
	printPreamble(n_iters);

	unsigned printing = thinning * print_factor;  // Output to cerr every x-th iteration.
	bool doSample = false;                        // Sample at next commit.
	bool doPrint = false;
	
	for (unsigned i = 0; i < n_iters; i++) 
	{
		try 
		{
			if (iteration % thinning == 0)
			{
				// We want to sample the next committed iteration.
				doSample = true;
				doPrint = (iteration % printing == 0);
			}
			
			MCMCObject proposal = model.suggestNewState();
			Probability alpha = 1.0;
			if (p > 0)
			{
				alpha = proposal.stateProb * proposal.propRatio / p;
			}
			
			if (alpha >= 1.0 || Probability(R.genrand_real1()) <= alpha)
			{
				// Iteration commited.
				model.commitNewState();
				check_number_nonupdate = 0;
				p = proposal.stateProb;
				if (doSample)
				{
					sample(doPrint, proposal, i, n_iters);
					doSample = doPrint = false;
				}
			}
			else
			{
				// Iteration discarded.
				// If nothing has happened we wish to output the discarded
				// states for diagnostic reasons.
				check_number_nonupdate++;
				model.discardNewState();
#ifdef DEBUG_DISCARD
				// Sample even though we've discarded.
				if (doSample)
				{
					sample(doPrint, proposal, i, n_iters);
					doSample = doPrint = false;
				}
#endif
			}
		}
		catch (AnError& e)
		{

			cerr << "SimpleMCMCPostSample::iterate" << endl
				<< "At iteration " << i << "." << endl
				<< "State is "	<< model.strRepresentation() << endl;
			e.action();
		}
		
		++iteration;
	}
	cout << "# acceptance ratio = " << model.getAcceptanceRatio() << endl;
}


void
SimpleMCMCPostSample::printPreamble(unsigned n_iters) const
{
	// First print out the settings and MCMC-header.
	cout << "#  Starting MCMC with the following settings:" << endl
		<< "#  " << n_iters << print()
		<< "#" << endl;
	
	cout << "# L N "
#ifdef DEBUG_PROPOSAL
		<< "Prop(logfloat)  "
#endif
		<< model.strHeader()
		<< endl;
	
	// Output a header to cerr if not quiet.
	if (show_diagnostics)
	{
		cerr.width(15);
		cerr << "L";
		cerr.width(15);
		cerr << "N";
		cerr.width(15);
		cerr << "alpha";
		cerr.width(15);
		cerr << "time" << endl;
	}
}


void
SimpleMCMCPostSample::sample(bool doPrint, const MCMCObject& proposal, unsigned i, unsigned n_iters)
{
	// Check if file descriptor is a TTY.
	if (doPrint && show_diagnostics) 
	{
		cerr.width(15);
		cerr << p;
		cerr.width(15);
		cerr << iteration;
		cerr.width(15);
		cerr << model.getAcceptanceRatio();
		cerr.width(15);
		cerr << estimateTimeLeft(i, n_iters);
		cerr << endl;
	}

	// This enables buffering and should speed up the program.
	// However, if the program exits prematurely, some iterations may be lost!
	cout << p << "\t"
		<< iteration << "\t"
#ifdef DEBUG_PROPOSAL
		<< proposal.stateProb << ";\t"
#endif
		<< model.strRepresentation()
		<< endl;
}

string 
SimpleMCMCPostSample::print() const
{
	ostringstream oss;
	oss << " MCMC iterations, saving every "
		<< thinning
		<< " post-sampled iteration.\n"
		<< indentString(model.print(), "#  ");
  return oss.str();
}


ostream& 
operator<<(ostream &o, const SimpleMCMCPostSample& A)
{
	return o << A.print();
}

}//end namespace beep
