#ifndef FASTGEM_BIRTHDEATHMCMC_HH
#define FASTGEM_BIRTHDEATHMCMC_HH

#include <string>
#include <iostream>

#include "fastGEM_BirthDeathProbs.hh"
#include "PRNG.hh"
#include "ReconciliationSampler.hh"
#include "StdMCMCModel.hh"
#include "Tree.hh"

namespace beep
{

  // TODO: This MCMCModel will be replaced by an EdgeRateMCMC, eventually /bens

  class fastGEM_BirthDeathMCMC : public StdMCMCModel,
				 public fastGEM_BirthDeathProbs
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------
    fastGEM_BirthDeathMCMC(MCMCModel& prior,Tree &S,unsigned noOfDiscrPoints, 
		   std::vector<double>* discrPoints, Real birthRate, 
		   Real deathRate, Real* topTime = 0);
    ~fastGEM_BirthDeathMCMC();


    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    void fixRates();
    void multiplySuggestionVariance(Real multiplier)
    {
      suggestion_variance = multiplier * (birth_rate+death_rate) / 2.0;
    }

    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const fastGEM_BirthDeathMCMC& A);
    std::string print() const;

  private:
//     //-------------------------------------------------------------
//     //
//     // Implementation
//     //
//     //-------------------------------------------------------------

//     // Utility function for changing birth/death rates.
//     // Returns a number in the interval [0.8, 1.25].
//     //-------------------------------------------------------------
//     Real rateChange();

    //-------------------------------------------------------------
    //
    // Attributes - none
    //
    //-------------------------------------------------------------
    Real old_birth_rate;
    Real old_death_rate;
    bool estimateRates;

    //! The suggestion function uses a normal distribution around the current
    //! value. For the proposal-ratios to function right, we need to keep the 
    //! variance to the distribution constant. This is a problem since we do not
    //! know the location/scale of the distribution ahead of time. As a fix, we
    //! store a variance initiated from the start values of dupl/loss rates.
    Real suggestion_variance;
  };
}//end namespace beep


#endif
