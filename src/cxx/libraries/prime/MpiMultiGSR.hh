/* 
 * File: MpiMultiGSR.hh Author: bens
 *
 * Created on den 4 mars 2010, 18:56
 */

#ifndef _MPIMULTIGSR_HH
#define	_MPIMULTIGSR_HH

#include <boost/mpi/environment.hpp> 
#include <boost/mpi/communicator.hpp> 
#include <boost/mpi.hpp>
namespace mpi = boost::mpi; 

#include "DiscTree.hh"
#include "EdgeDiscBDMCMC.hh"
#include "Density2PMCMC.hh"
#include "SubstitutionMCMC.hh"
#include "TreeMCMC.hh"

#include <sstream>
#include <ostream>
namespace beep
{
  class SeriGSRvars
  {
  public:
    SeriGSRvars();

    SeriGSRvars(unsigned geneFam, std::string G, 
		double lambda, double mu,
		double m, double v);
    virtual ~SeriGSRvars();
    SeriGSRvars(const SeriGSRvars& orig);
    SeriGSRvars& operator=(const SeriGSRvars& orig);

    friend std::ostream& operator<<(std::ostream& o, const SeriGSRvars& v)
    {
      std::ostringstream oss;
      oss << "'"
	  << v.geneFam << "'\t'"
	  << v.lambda << "'\t'"
	  << v.mu << "'\t'"
	  << v.m << "'\t'"
	  << v.v
	  << "'\t'"
	  << v.G
	  << "'";
      return o << oss.str();
    };

  private:
    friend class boost::serialization::access; 
  
    template<class Archive> 
    void serialize(Archive & ar, const unsigned int version);
  
  public:
    //Attributes
    unsigned geneFam;
    std::string G;
    double lambda;
    double mu;
    double m;
    double v;
  };
    
  class SeriMultiGSRvars
  {
  public:
    SeriMultiGSRvars();
    virtual ~SeriMultiGSRvars();
    SeriMultiGSRvars(const SeriMultiGSRvars& orig);
    SeriMultiGSRvars& operator=(const SeriMultiGSRvars& orig);

    void clear();

    friend std::ostream& operator<<(std::ostream& o, const SeriMultiGSRvars& v)
    {
      std::ostringstream oss;
      for(std::vector<SeriGSRvars>::const_iterator i = v.Gvars.begin(); 
	  i!= v.Gvars.end(); i++)
	{
	  oss << *i << "'\t'";
	}
      oss << v.S << "'";
      return o<<oss.str();
    };

  private:
    friend class boost::serialization::access; 
    
    template<class Archive> 
    void serialize(Archive & ar, const unsigned int version);

  public:
    //Attributes
    std::string S;
    std::vector<SeriGSRvars> Gvars;
  };
  
  class MpiMultiGSR
    : public StdMCMCModel
  {
  public:
    // enum aliases for mpi::isend tags
    enum {STOP, UPDATE, SCALC, RCALC1, RCALC2};
    
    // Construct/destruct/assign
    //---------------------------
    MpiMultiGSR(MCMCModel& prior, EdgeDiscTree& DS,
		mpi::communicator& world, 
		const Real& suggestRatio = 1.0);
    virtual ~MpiMultiGSR();

    // Interface
    //--------------------------
    void addGeneFamily(SubstitutionMCMC& like, TreeMCMC& genetree,
		       EdgeDiscBDMCMC& bdm, Density2PMCMC& dens,
		       bool isMaster);
    virtual MCMCObject suggestOwnState();
//     virtual MCMCObject suggestOwnState(unsigned x);
    virtual Probability updateDataProbability();
    virtual void commitOwnState ();
    virtual void discardOwnState ();
    virtual std::string ownStrRep() const;
    virtual std::string ownHeader () const;
    virtual Probability calcDataProbability(unsigned excludeGF);
    void update();
    void updateGvars(unsigned geneFamIdx);
    void stopSlaves();
    void waitingSlaves();
    void updateSlave();
    std::string print() const;

    // Attributes
    //-----------------------------
//     Tree* S;
    EdgeDiscTree* DS;
    std::vector<SubstitutionMCMC*> geneFams;
    std::vector<TreeMCMC*> trees;
    std::vector<EdgeDiscBDMCMC*> bds;
    std::vector<Density2PMCMC*> rateDensities;
    
    SeriMultiGSRvars vars;
    unsigned subIdx;
    mpi::communicator& world;
  };
}// end name space beep

#endif	/* _MPIINSIDEMCMC_HH */

