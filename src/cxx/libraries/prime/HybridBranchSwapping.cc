#include "HybridBranchSwapping.hh"

#include "HybridTree.hh"
#include "Node.hh"

#include <cassert>

namespace beep
{
  using namespace std;
  //--------------------------------------------------------------
  //
  // Construct destruct assign
  //
  //--------------------------------------------------------------
  HybridBranchSwapping::HybridBranchSwapping(HybridTree& H_in)
    : H(&H_in)
  {}

  HybridBranchSwapping::~HybridBranchSwapping()    
  {}

  HybridBranchSwapping::HybridBranchSwapping(const HybridBranchSwapping& hbs)
    : H(hbs.H)
  {}

  HybridBranchSwapping& 
  HybridBranchSwapping::operator=(const HybridBranchSwapping& hbs)
  {
    if(this != &hbs)
      {
	H = hbs.H;
      }
    return *this;
  }

  //--------------------------------------------------------------
  //
  // Interface
  //
  //--------------------------------------------------------------
    

  //--------------------------------------------------------------
  //
  // Implementation
  //
  //--------------------------------------------------------------
  Node*
  HybridBranchSwapping::addHybrid()
  {
    Node* u = H->getRootNode();
    do
      {
	u = H->getNode(R.genrand_modulo(H->getNumberOfNodes()));
      }
    while(u->isRoot() || H->isHybridNode(*u));
    Node* op = u->getParent();
    Node* s = u->getSibling();
    if(H->getOtherParent(*s) == op)
      {
	H->switchParents(*s);
      }

    Node* ns = u;
    do
      {
	ns = H->getNode(R.genrand_modulo(H->getNumberOfNodes()));
	assert(ns != NULL);
      }
    while(ns->isRoot() || ns == u->getSibling() ||
	  H->getTime(*ns) >= H->getTime(*op) || 
	  H->getTime(*ns->getParent()) < H->getTime(*op));
    
    if(ns == u)
      {
	// This is an autopolyploidization or a hybridization in which
	// both parents have become extinct, op will be the orginal sole 
	// parent that splits at pp, its children hybridize to form u 
	// and both siblings of u goes extinct

	// First ecide time for hybridization event (limited by u and op)
	Real stime = H->getTime(*u) + 
	  (H->getTime(*op) - H->getTime(*u)) * R.genrand_real3();
	//! \todo{Here we could allow some time betwen grand father split
	//! and polyploidization - should always be zero in autoployploidization
	Real htime = stime;

	// add left extinction node with parent
	Node* e1 = addExtinct(*op, *u);
	// and set their times
	H->setTime(*e1, htime);
	H->setTime(*e1->getParent(), htime);

	// then add the grand father split pp
	Node * pp = H->addNode(u, e1->getParent(), H->getNumberOfNodes(), "");
	// and connect to op, the previous parent of u
	op->setChildren(s, pp);
	// set its time
	H->setTime(*pp, stime);

	// Add second extinction node
	Node* e2 = addExtinct(*pp, *u);
	// set times
	H->setTime(*e2, htime);
	H->setTime(*e2->getParent(), htime);

	// Finally set OP
	H->setOtherParent(*u, e1->getParent());
      }
    else
      {
	// Normal hybridization
	Node* pp = ns->getParent();
	Node* ps = ns->getSibling();
	if(H->isHybridNode(*ns))
	  {
	    Node* nop = H->getOtherParent(*ns);
	    Node* e = addExtinct(*nop, *ns);
	    H->setTime(*e, H->getTime(*op));
	    H->setTime(*e->getParent(), H->getTime(*op));
	  }
	H->setOtherParent(*u,op);
	Node* p = H->addNode(ns, u, H->getNumberOfNodes(), "");
	H->setTime(*p, H->getTime(*op));
	if(H->getOtherParent(*ps) == pp)
	  {
	    H->switchParents(*ps);
	  }
	pp->setChildren(p,ps);
      }
    return u;
  }


  Node*
  HybridBranchSwapping::rmHybrid()
  {
    Node* u = H->getRootNode();
    do
      {
	u = H->getNode(R.genrand_modulo(H->getNumberOfNodes()));
      }
    while(H->isHybridNode(*u) == false);
    cerr << "rmHybrid(" << u->getNumber() << ")\n";

    // Remove p or op with equal prob. we will act on op
    if(R.genrand_real1() <0.5)
      {
	H->switchParents(*u);
      }
    Node* op = H->getOtherParent(*u);
    Node* s = u->getSibling();
    Node* os = H->getOtherSibling(*u);

    if(H->isExtinct(*s))           // Remove any extinct siblings
      {
	rmExtinct(*s);
      }
    if(H->isExtinct(*os))
      {
	rmExtinct(*os);
	os = H->getOtherSibling(*u);
	op = H->getOtherParent(*u);
      }
    else if(H->isHybridNode(*os))  // Don't mess up os's parents
      {
	if(H->getOtherParent(*os) == op)
	  {
	    H->switchParents(*os);
	  }
      }
    else if(H->isHybridNode(*op))
      {
	Node* opp = H->getOtherParent(*op);
	Node* ops = H->getOtherSibling(*op);
	if(H->getOtherParent(*ops) == opp)
	  {
	    H->switchParents(*ops);
	  }
	opp->setChildren(ops, os);
	H->setOtherParent(*os, opp);

	H->setOtherParent(*op, NULL);
      }
    op->setChildren(os, NULL);    
    suppress(*op);
    H->setOtherParent(*u,NULL);
    return u;
  }


  Node* 
  HybridBranchSwapping::mvHybrid()
  {
    Node* u;
    Node* op;
    map<const Node*, Node*>* OP = H->getOPAttribute();
    map<const Node*, Node*>::iterator i = OP->begin();
    for(unsigned j= 0; j < R.genrand_modulo(OP->size());j++)
      i++;
    op = i->second;
    const Node* pu = i->first;
    u = op->getLeftChild();
    if(u != pu)
      {
	u = op->getRightChild();
      }
    assert(H->isHybridNode(*u));
    Node* p = u->getParent();
    // \todo{Possiblitiy to add extinct here}
    
    Real t = R.genrand_real3() * (H->rootToLeafTime() - H->getTime(*u));
    
    Node* nos;
    do	
      {
	nos = H->getNode(R.genrand_modulo(H->getNumberOfNodes()));
      }
    while(nos == op|| nos == p || H->getTime(*nos) > t 
	  || H->getTime(*nos->getParent()) < t 
	  || H->getTime(*H->getOtherParent(*nos)) < t);
    nos->getParent()->setChildren(nos->getSibling(), op);
    op->setChildren(nos,u);
    
    Node* ns;
    do	
      {
	ns = H->getNode(R.genrand_modulo(H->getNumberOfNodes()));
      }
    while(ns == p|| ns == op || H->getTime(*ns) > t 
	  || H->getTime(*ns->getParent()) < t 
	  || H->getTime(*H->getOtherParent(*ns)) < t);
    
    ns->getParent()->setChildren(ns->getSibling(), p);
    p->setChildren(ns,u);
    
    return u;
  }


  Node*
  HybridBranchSwapping::addExtinct(Node& p, Node& u)
  {
    // Check sanity only hybrids can have extinction siblings
    // but sometimes we need to add extinct in the process of 
    // adding a hybrid, see addHYbrid
    //    assert(H->isHybridNode(u)); 
    assert((&p == u.getParent() && 
	    H->isExtinct(*u.getSibling())) == false);
    assert((&p == H->getOtherParent(u) && 
	    H->isExtinct(*H->getOtherSibling(u))) == false);

    Node* op = H->getOtherParent(u);
    Node* s = u.getSibling();
    if(&p == op)
      {
	op = u.getParent();
	s = H->getOtherSibling(u);
      }
      
    // Add a new extinction node
    Node* e = H->addNode(NULL, NULL, H->getNumberOfNodes(), "", true);
    H->setTime(*e, H->getTime(p));
    // Add a new pseudoparent node
    Node* np = H->addNode(&u, e, H->getNumberOfNodes(), "", false);
    np->setNodeTime(H->getTime(p));
    p.setChildren(np, s);
    H->setOtherParent(u, op);
    return e;
  }


  Node* 
  HybridBranchSwapping::rmExtinct(Node& e)
  {
    // Check sanity: 
    assert(H->isExtinct(e));

    Node* p = e.getParent();
    Node* s = e.getSibling();
    Node* pp = p->getParent();

    assert(H->isHybridNode(*s)); // sanity check      
    if(p != s->getParent())
      {
	H->switchParents(*s);
      }      
    p->setChildren(s,NULL);
    H->removeNode(&e);
    suppress(*p);

    return pp;
  }

  void 
  HybridBranchSwapping::suppress(Node& u)
  {
    cerr << "suppress(" << u.getNumber() << ")\n";
    assert(H->isHybridNode(u) == false);

    Node* v = u.getLeftChild();
    if(v == NULL)
      {
	v = u.getRightChild();
	assert(v != NULL); 
      }
    Node* s = u.getSibling();
    u.getParent()->setChildren(s,v);
    H->removeNode(&u);
  }

}//end namespace beep
