/* 
 * File: MultiGSR.cc Author: bens
 *
 * Created on den 4 mars 2010, 18:56
 */
#include "MultiGSR.hh"

#include "MCMCObject.hh"
#include "Probability.hh"
#include "TreeIO.hh"
#include "TreeIOTraits.hh"
#include "mpicxx.h"
#include "TreePerturbationEvent.hh"
#include <iostream>
#include <sstream>
#include <fstream>


namespace beep
{
  using namespace std;
  
  // Construct/destruct/assign
  //---------------------------
  MultiGSR::MultiGSR(MCMCModel& prior, EdgeDiscTree& DS,
			   const Real& suggestRatio)
    : StdMCMCModel (prior, 0, "multiGSR", suggestRatio),
      DS(&DS),
      geneFams(),
      trees(),
      bds(),
      rateDensities(),
      subIdx(0)
  {
  }
  
  MultiGSR::~MultiGSR()
  {}
  
  // Interface
  //--------------------------
  void
  MultiGSR::addGeneFamily(SubstitutionMCMC& like, TreeMCMC& genetree,
			     EdgeDiscBDMCMC& bdm, Density2PMCMC& dens)
  {
    geneFams.push_back(&like);
    trees.push_back(&genetree);
    bds.push_back(&bdm);
    rateDensities.push_back(&dens);
    // Update StdMCMCModel
    n_params += like.nParams();
    updateParamIdx();

    geneFams.back()->initStateProb();
  }
  
  
  MCMCObject 
  MultiGSR::suggestOwnState()
  {
    subIdx = R.genrand_modulo(geneFams.size());
    MCMCObject MOb = geneFams[subIdx]->suggestNewState ();

#ifdef DEBUGMULTIGSR
    //    MOb.stateProb ==  geneFams[subIdx]->initStateProb(); // XXX CHeck if needed
    if(MOb.stateProb != geneFams[subIdx]->initStateProb())
      {
	cerr << "Houston, we've got a problem" << endl;
	cerr << MOb.stateProb <<" != "<<geneFams[subIdx]->updateDataProbability()<< endl;
	throw AnError("Houston, we've got a problem", 1);
      }
#endif

    MOb.stateProb += calcDataProbability(subIdx);
    return MOb;
  }
  

  Probability 
  MultiGSR::updateDataProbability()
  {
    update();
    // All gene families should be treated, set excludeGF to 
    // an index > maxIndex
    return calcDataProbability(geneFams.size());
  }
  
  void 
  MultiGSR::commitOwnState ()
  {
  }
  
  void 
  MultiGSR::discardOwnState ()
  {
    geneFams[subIdx]->discardNewState();
    //DEBUG Needed?
//     geneFams[subIdx]->initStateProb();
  }
  
  std::string 
  MultiGSR::ownStrRep() const
  {
    ostringstream oss;
    for(unsigned i = 0; i < geneFams.size(); i++)
      {
  	oss << geneFams[i]->ownStrRep();
 	oss << trees[i]->ownStrRep();
 	oss << bds[i]->ownStrRep();
 	oss << rateDensities[i]->ownStrRep();
      }
    TreeIO tio;
    oss << tio.writeHostTree(DS->getTree()) << "\t";
    return oss.str();
  }
  
  std::string 
  MultiGSR::ownHeader () const
  {
    ostringstream oss;
    for(unsigned i = 0; i < geneFams.size(); i++)
      {
  	oss << geneFams[i]->ownHeader();
 	oss << trees[i]->ownHeader();
 	oss << bds[i]->ownHeader();
 	oss << rateDensities[i]->ownHeader();
      }
    oss << "S(Tree)\t";
    return oss.str();
  }
  
  Probability 
  MultiGSR::calcDataProbability(unsigned excludeGF)
  {
    Probability ret = 1.0;
    for (unsigned i = 0; i < geneFams.size(); i++)
      {
	if(i != excludeGF)
	  {
	    ret *= geneFams[i]->initStateProb();//updateDataProbability();
	  }
      } 
    return ret;
  }
  
  
  void 
  MultiGSR::update()
  {
    for (unsigned i = 0; i < geneFams.size(); i++)
      {
	geneFams[i]->update();
      }
  }
    
  string 
  MultiGSR::print() const
  {
    ostringstream oss;
    oss << "Parallelized, multi-gene version of GSR, the nested"
	<< "GSR classes are.";
    for (unsigned i = 0; i < geneFams.size (); i++)
      {
	oss << indentString(geneFams[i]->print());
      }
    oss << StdMCMCModel::print();
    return oss.str();
  }



}// end namespace beep

