#ifndef DENSITY2P_HH
#define DENSITY2P_HH

#include "AnError.hh"
#include "Beep.hh"
#include "PerturbationObservable.hh"

#include <iostream>
#include <string>

namespace beep
{
  class Probability;
  //----------------------------------------------------------------------
  //
  // Class Density2P
  //! Base class interface for probabilistic density functions that 
  //! takes 2 parameters.
  //! Now inherits PerturbationObservable which makes it possible
  //! to let listeners be notified of parameter changes.
  //!
  //! Invariants: variance > 0; 
  //!
  //! Author: Martin Linder, Bengt Sennblad, copyright: the MCMC-club, SBC.
  //! All rights reserved.
  //
  //----------------------------------------------------------------------
  class Density2P : public PerturbationObservable
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //----------------------------------------------------------------------
    virtual ~Density2P() 
    {};

  public:
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    //! Returns type of density, e.g., uniform, gamma, etc.
    virtual std::string densityName() const = 0;

    // Density-, distribution-, and sampling functions
    //----------------------------------------------------------------------
    //! The density function f(), returns f(x).
    virtual Probability operator()(const Real& value) const=0;

    virtual Probability operator()(const Real& x,const Real& interval) const=0;
    virtual Probability pdf(const Real& x) const = 0;
    virtual Probability cdf(const Real& x) const = 0;

    //! samples from distribution function, F(x), returns x: F(x) = p
    //! Precondition: 0 < p < 1.0.
    //! Postcondition: isInRange(sampleValue(p).
    virtual Real sampleValue(const Real& p) const=0;

    // Access parameters
    //----------------------------------------------------------------------
    //! Returns current value of mean. 
    virtual Real getMean() const=0; 

    //! Returns current value of variance. 
    virtual Real getVariance() const=0; 

    //! Returns current value of alpha. 
    virtual Real getAlpha() const=0; 

    //! Returns current value of beta. 
    virtual Real getBeta() const=0; 

    // Set Parameters
    //----------------------------------------------------------------------

    //! Sets mean and variance of density.
    //! Precondition: newVariance >= 0.
    //! Postcondition: getMean()==mean and getVariance()==variance
    virtual void setParameters(const Real& mean, const Real& variance)=0;

    //! Sets new mean of density. 
    //! Postcondition: getMean()==mean and getVariance()==variance
    virtual void setMean(const Real& mean)=0;

    //! Sets new variance of density. 
    //! Precondition: newVariance >= 0.
    //! Postcondition: getMean()==mean and getVariance()==variance
    virtual void setVariance(const Real& newVariance)=0;

    //! Sets embedded parameters (alpha and beta) of density 
    //! (see doc for subclass for exact meaning of these).
    //! Postcondition: getMean()==mean and getVariance()==variance
    virtual void setEmbeddedParameters(const Real& alpha, const Real& beta)=0;

    //! Sets embedded parameter alpha of density 
    //! Postcondition: alpha and beta is within precision limits.
    virtual void setAlpha(const Real& alpha)=0;

    //! Sets embedded parameters beta of density 
    //! Postcondition: getMean()==mean and getVariance()==variance
    virtual void setBeta(const Real& beta)=0;

    // tests and information on valid ranges
    //------------------------------------------------------------------

    //! Check if value is valid (has a non-zero probability).
    virtual bool isInRange(const Real& rate) const=0;

    //! Returns range in which density has a non-zero probability.
    virtual void getRange(Real& min, Real& max) const = 0;

    //! Returns range in which density has a non-zero probability.
    virtual void setRange(const Real& min, const Real& max) = 0;

    //! Get range in which mean is valid.
    virtual void getMeanRange(Real& min, Real& max) const = 0;

    //! Get range in which variance is valid.
    virtual void getVarianceRange(Real& min, Real& max) const = 0;

    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------

    //! ostream operator
    //------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const Density2P& df)
    {
      return o << df.print();
    };
    virtual std::string print() const = 0;

  };

}//end namespace beep
#endif
