#include <cassert>
#include <cmath>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "EdgeDiscBDProbs.hh"
#include "EdgeDiscPtMapIterator.hh"

namespace beep
{

  using namespace std;

  EdgeDiscBDProbs::EdgeDiscBDProbs(EdgeDiscTree* DS, 
				   Real birthRate, Real deathRate) :
    PerturbationObservable(),
    m_DS(DS),
    m_birthRate(birthRate),
    m_birthRateOld(-1),
    m_deathRate(deathRate),
    m_deathRateOld(-1),
    m_BD_const(*DS, 0.0, true),
    m_BD_zero(DS->getTree()),
    m_BD_zeroOld(DS->getTree())
  {
    Real mr = 0.95 * getMaxAllowedRate();
    if (birthRate > mr)
      {
	birthRate = mr;
	cout << "# EdgeDiscBDProbs: High initial birth rate; "
	     << "changing it to " 
	     << mr
	     << " (95% of max limit).\n"; 
      }
    if (deathRate > mr)
      {
	deathRate = mr;
	cout << "# EdgeDiscBDProbs: High initial death rate; "
	     << "changing it to "
	     << mr
	     << " (95% of max limit).\n";
	  }
    setRates(birthRate, deathRate, false);
    update();
  }


  EdgeDiscBDProbs::EdgeDiscBDProbs(const EdgeDiscBDProbs& probs) :
    m_DS(probs.m_DS),
    m_birthRate(probs.m_birthRate),
    m_birthRateOld(probs.m_birthRateOld),
    m_deathRate(probs.m_deathRate),
    m_deathRateOld(probs.m_deathRateOld),
    m_BD_const(probs.m_BD_const),
    m_BD_zero(probs.m_BD_zero),
    m_BD_zeroOld(probs.m_BD_zeroOld)
  {
  }


  EdgeDiscBDProbs::~EdgeDiscBDProbs()
  {
  }


  EdgeDiscBDProbs&
  EdgeDiscBDProbs::operator=(const EdgeDiscBDProbs& probs)
  {
    if (this != &probs)
      {
	m_DS = probs.m_DS;
	m_birthRate = probs.m_birthRate;
	m_birthRateOld = probs.m_birthRateOld;
	m_deathRate = probs.m_deathRate;
	m_deathRateOld = probs.m_deathRateOld;
	m_BD_const = probs.m_BD_const;
	m_BD_zero = probs.m_BD_zero;
	m_BD_zeroOld = probs.m_BD_zeroOld;
      }
    return *this;
  }


  void
  EdgeDiscBDProbs::update(bool rediscretize)
  {
    if (rediscretize)
      {
	m_BD_const.rediscretize(0.0);
      }
    const Node* root = m_DS->getRootNode();
    calcProbsForEdge(root, true);
    calcProbsForRootPath(root, true);
  }


  void
  EdgeDiscBDProbs::setRates(Real newBirthRate, Real newDeathRate,
			    bool doUpdate)
  {
    if (newBirthRate <= 0)
      throw AnError("Cannot have zero or negative birth rate in "
		    "EdgeDiscBDProbs.", 1);
    if (newDeathRate <= 0)
      throw AnError("Cannot have zero or negative death rate in "
		    "EdgeDiscBDProbs.", 1);

    assert(newBirthRate <= getMaxAllowedRate());
    assert(newDeathRate <= getMaxAllowedRate());


    m_birthRate = newBirthRate;
    m_deathRate = newDeathRate;

    if (doUpdate) { update(); }
  }


  Real
  EdgeDiscBDProbs::getMaxAllowedRate() const
  {
    // Use root-to-leaf time as indicator of scale.
    // Discard top time since it may vary dramatically
    // (unless it refers to the only edge).
    Real t = m_DS->getRootToLeafTime();
    if (t <= 1e-8)
      {
	t = m_DS->getTopTime();
      }
    const Real MAX_INTENSITY = 10.0;
    return (MAX_INTENSITY / t);
  }


  void
  EdgeDiscBDProbs::cache()
  {
    m_birthRateOld = m_birthRate;
    m_deathRateOld = m_deathRate;
    m_BD_const.cache();
    m_BD_zeroOld = m_BD_zero;
  }


  void 
  EdgeDiscBDProbs::restoreCache()
  {
    m_birthRate = m_birthRateOld;
    m_deathRate = m_deathRateOld;
    m_BD_const.restoreCache();
    m_BD_zero = m_BD_zeroOld;
  }


  void
  EdgeDiscBDProbs::calcProbsForEdge(const Node* node, bool doRecurse)
  {
    // Children must be updated first.
    if (doRecurse && !node->isLeaf())
      {
	calcProbsForEdge(node->getLeftChild(), true);
	calcProbsForEdge(node->getRightChild(), true);
      }

    // Three lower points and an iterator to the end.
    EdgeDiscretizer::Point pt0(node, 0), pt1(node, 1), pt2(node, 2);
    EdgeDiscTreeIterator itEnd = m_DS->end(node);

    // Compute Pt and ut for separately for inner segment 
    // and end segments because of different time spans 
    // (dt and dt/2 respectively).
    Real dt = m_DS->getTimestep(node);
    Real Pt, ut, PtEnd, utEnd;
    calcPtAndUt(dt, Pt, ut);
    calcPtAndUt(dt/2, PtEnd, utEnd);

    ///////// POINT-TO-NODE PROBS. ////////

      // Probability of death below for a lineage at the current point.
      Real D = node->isLeaf() ? 0.0 :(m_BD_zero[node->getLeftChild()]*
				      m_BD_zero[node->getRightChild()]);

      // Treat first segment separately.
      m_BD_const(pt0, pt0) = 1.0;
      Real p11 = (PtEnd * (1.0 - utEnd) / ((1.0 - utEnd * D) * 
					   (1.0 - utEnd * D)));
      D = 1.0 - PtEnd * (1.0 - D) / (1.0 - utEnd * D);
      m_BD_const(pt1, pt0) = p11;


      // Remaining inner points.
      for (EdgeDiscTreeIterator it = m_DS->begin(pt2); 
	   it != itEnd; ++it)
	{
	  p11 = (p11 * Pt * (1.0 - ut) / ((1.0 - ut * D) * 
					  (1.0 - ut * D)));
	  m_BD_const(it, pt0) = p11;
	  D = 1.0 - Pt * (1.0 - D) / (1.0 - ut * D);
	}

      // Special treatment of last segment.      
      p11 = (p11 * PtEnd * (1.0 - utEnd) / ((1.0 - utEnd * D) * 
					    (1.0 - utEnd * D)));
      D = 1.0 - PtEnd * (1.0 - D) / (1.0 - utEnd * D);
      m_BD_const(itEnd, pt0) = p11;

      // Extinction probability in the planted tree S^node.
      m_BD_zero[node] = D;

      ///////// POINT-TO-POINT PROBS. ////////

	// Use Markovian property to compute probability for 
	// inner point pairs.
	for (EdgeDiscTreeIterator loPt = m_DS->begin(node); 
	     loPt != itEnd; ++loPt)
	  {
	    for (EdgeDiscTreeIterator upPt = loPt; 
		 upPt != itEnd; ++upPt)
	      {
		m_BD_const(upPt, loPt) = computeInnerP11(loPt, upPt);
	      }
	    m_BD_const(itEnd, loPt) = computeInnerP11(loPt, itEnd);
	  }

	// For topmost pt.
	if (node->isRoot())
	  {
	    m_BD_const(itEnd, itEnd) = 1.0;
	  }
  }


  Real
  EdgeDiscBDProbs::computeInnerP11(const EdgeDiscretizer::Point& loPt,
				   const EdgeDiscretizer::Point& upPt)
  {
    if (loPt == upPt)
      return 1.0;
    EdgeDiscretizer::Point specPt(loPt.first, 0);
    return m_BD_const(upPt, specPt) / m_BD_const(loPt, specPt);
  }


  void
  EdgeDiscBDProbs::calcProbsForRootPath(const Node* node, 
					bool doRecurse)
  {
    if (doRecurse && !node->isLeaf())
      {
	calcProbsForRootPath(node->getLeftChild(), true);
	calcProbsForRootPath(node->getRightChild(), true);
      }

    if (!node->isRoot())
      {
	// The base edge refers to the edge where we start, i.e
	// the edge that corresponds to the input node.
	EdgeDiscTreeIterator baseBegin = m_DS->begin(node);
	EdgeDiscTreeIterator baseEnd = m_DS->end(node);

	// Ancestral edge, we are calculating probabilities from
	// points on the ancestral edge to points on the base edge
	const Node* ancEdge = node->getParent();

	// One-to-one for intermediate edges, incl. losses.
	Real p11ForIntermediateEdges = 1.0;

	// Loss for planted subtree ending in current ancestor 
	// (but of other clade).
	Real loss = m_BD_zero[node->getSibling()];

	// For each ancestral edge, compute probs. between points 
	// on that and the current.
	while (true)
	  {
	    EdgeDiscTreeIterator ancBegin = m_DS->begin(ancEdge);
	    EdgeDiscTreeIterator ancEnd = m_DS->endPlus(ancEdge);

	    // Current ancestral point.
	    EdgeDiscTreeIterator ancPt = ancBegin;

	    // Compute p11 from the first point on the ancestral
	    // edge to each point on the base edge.
	    for (EdgeDiscTreeIterator basePt = baseBegin; 
		 basePt != baseEnd; ++basePt)
	      {
		m_BD_const(ancBegin, basePt) = 
		  p11ForIntermediateEdges * m_BD_const(baseEnd, basePt);
	      }

	    // The next iterations will be to points above the
	    // speciation, so add the current loss factor to p11.
	    p11ForIntermediateEdges *= loss;
	    ++ancPt;

	    // For each point a on the ancestor edge:
	    //    for each point b on the bottom edge:
	    //        calculate p11(a, b)
	    for (; ancPt != ancEnd; ++ancPt)
	      {
		Real p11ToSpec = m_BD_const(ancPt, ancBegin);
		for (EdgeDiscTreeIterator basePt = baseBegin; 
		     basePt != baseEnd; ++basePt)
		  {
		    m_BD_const(ancPt, basePt) = 
		      p11ToSpec * p11ForIntermediateEdges *
		      m_BD_const(baseEnd, basePt);
		  }
	      }

	    // If we have completed the root we are done
	    if (ancEdge->isRoot())
	      {
		break;
	      }

	    // Update p11 by adding the probability for p11 over the
	    // whole edge, update loss factor, and get next ancestor.
	    p11ForIntermediateEdges *= m_BD_const(ancEnd, ancBegin);
	    loss = m_BD_zero[ancEdge->getSibling()];
	    ancEdge = ancEdge->getParent();
	  }
      }
  }


  void
  EdgeDiscBDProbs::calcPtAndUt(Real t, Real& Pt, Real& ut) const
  {
    if (std::abs(m_birthRate - m_deathRate) < 1e-9)
      {
	Real denom = 1.0 + (m_deathRate * t);
	Pt = 1.0 / denom;
	ut = (m_deathRate * t) / denom;
      }
    else if (m_deathRate < 1e-9) //TODO: Not allowed at the moment 
                                 //(see constructor). Why?
      {
	Pt = 1.0;
	ut = 1.0 - std::exp(-m_birthRate * t);
      }
    else
      {
	Real dbDiff = m_deathRate - m_birthRate;
	Real E = std::exp(dbDiff * t);
	Real denom = m_birthRate - (m_deathRate * E);
	Pt = -dbDiff / denom;
	ut = (m_birthRate * (1.0 - E)) / denom;
      }
  }


} // end namespace beep.

