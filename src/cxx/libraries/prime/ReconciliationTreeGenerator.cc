#include <sstream>

#include "AnError.hh"
#include "LambdaMap.hh" 
#include "ReconciliationTreeGenerator.hh"
#include "ReconciliationTimeSampler.hh" 


//----------------------------------------------------------------------
//Author: Bengt Sennblad
//�the MCMC-club, Stockholm Bioinformatics Center, 2002
//----------------------------------------------------------------------

//----------------------------------------------------------------------
//
// Class ReconciliationTreeGenerator
// This class contains functions for generating a tree, T, most often a 
// species tree, and a reconciliation, gamma, according to the Birth-death 
// model specified by attribute BD.
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Construct/destruct/assign
  // 
  //----------------------------------------------------------------------
  ReconciliationTreeGenerator::
  ReconciliationTreeGenerator(BirthDeathProbs& BDP, const string &prefix)
    : S(BDP.getStree()),      // Again we don't need S as an attribute by itself
      BDP(BDP),     // it can be reached from BDP (we only need its root once)
      R(R),
      G(),
      gs(),
      gamma(S.getNumberOfNodes()),
      leaf_prefix(prefix)
  {
    if(S.getRootNode()->getTime() == 0)
      {
	S.getRootNode()->setTime(S.rootToLeafTime());
      }
  }
  
  ReconciliationTreeGenerator::
  ReconciliationTreeGenerator(ReconciliationTreeGenerator& RTG)
    : S(RTG.S),
      BDP(RTG.BDP),
      R(RTG.R),
      G(RTG.G),
      gs(RTG.gs),
      gamma(RTG.gamma),
      leaf_prefix(RTG.leaf_prefix)
  {
  }
  
  ReconciliationTreeGenerator::~ReconciliationTreeGenerator()
  {
  }

  ReconciliationTreeGenerator& 
  ReconciliationTreeGenerator::operator=(const ReconciliationTreeGenerator& RTG)
  {
    if(this != &RTG)
      {
	S     = RTG.S;
	BDP   = RTG.BDP;
	R     = RTG.R;
	G     = RTG.G;
	gs    = RTG.gs;
	gamma = RTG.gamma;
	leaf_prefix = RTG.leaf_prefix;
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  //public:
  // generateGammaTree(leaves, generateEdgeTimes)
  // Generates a guest tree with a specified size of the top slice
  // typically this is used when you want to generate a "unreconciled"
  // tree, i.e., the host tree has only a single vertex, with a specified
  // number of leaves; or when you want a tree with no top slice (nleaves=1).
  // Note that gamma is not a GammaMap. This is because GammaMap needs a 
  // full tree as input. View gamma as the working sketch
  //----------------------- -----------------------------------------------
  void
  ReconciliationTreeGenerator::generateGammaTree(const unsigned& nleaves, 
						 const bool& generateRootTime)
  {
    if(G.getRootNode()) // Remove any old tree lying around!
      {
	gs.clearMap();
	gamma = std::vector<SetOfNodes>(S.getNumberOfNodes());
	G.clear();
      }
    Node& sr = *S.getRootNode();  //start at root sr of S
    G.setRootNode(generateSlice(nleaves, sr)); //Start recursion
  
    LambdaMap lambda(G, S, gs);
    GammaMap tmpGamma(G, S, lambda);
    createTrueGamma(tmpGamma);
    ReconciliationTimeSampler RTS(G, BDP, tmpGamma);
    RTS.sampleTimes(generateRootTime);
    return;
  }

  // generateGammaTree(generateEdgeTimes)
  // Generates a guest tree where the size of the top slice is drawn from 
  // the birth death process
  //----------------------------------------------------------------------
  void
  ReconciliationTreeGenerator::generateGammaTree(const bool& generateRootTime)
  {
    unsigned nleaves = 0;
    while(nleaves == 0)   // Well to get a tree we need at least one leaf
      {
	nleaves = BDP.sampleNumberOfChildren(*S.getRootNode(),
					     R.genrand_real1());
      }
    return generateGammaTree(nleaves, generateRootTime);
  }

  //export host tree()
  //----------------------------------------------------------------------
  Tree
  ReconciliationTreeGenerator::getStree()
  {
    if(!S.getRootNode())
      {
	throw AnError("No species tree exists to export!!!!!");
      }
    return S; 

  }

  //export guest tree()
  //----------------------------------------------------------------------
  Tree
  ReconciliationTreeGenerator::exportGtree()
  {
    if(!G.getRootNode())
      {
	throw AnError("No gene tree has been generated to return");
      }
    return G;
  }

  Tree&
  ReconciliationTreeGenerator::getGtree()
  {
    if(!G.getRootNode())
      {
	throw AnError("No gene tree has been generated to return");
      }
    return G;
  }

  //exportGamma()
  //----------------------------------------------------------------------
  GammaMap
  ReconciliationTreeGenerator::exportGamma()
  {
    if(gamma.empty())
      {
	throw AnError("No gamma has been generated to return");
      }
    LambdaMap lambda(G, S, gs);
    GammaMap tmpGamma(G, S, lambda);
    createTrueGamma(tmpGamma);
    return tmpGamma;
  }

  //exportGS
  //----------------------------------------------------------------------
  StrStrMap
  ReconciliationTreeGenerator::exportGS() const
  {
    return gs;
  }

  // output of gs
  //----------------------------------------------------------------------
  string 
  ReconciliationTreeGenerator::gs4os()
  {
    ostringstream oss;
    oss << gs;
    return oss.str();
  }


  //---------------------------------------------------------------------
  // I/O
  //---------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, 
	     const ReconciliationTreeGenerator& tsm)
  {
    return o << "ReconciliationTreeGenerator.\n"
	     << "A class for constructing guest trees from host trees.\n"
      //Add indentation here, e.g., <<indent = original_indent + 3
	     << tsm.print()
      ;
  }


  //private:  
  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------
 
  // generateSlice()
  // creates planted subtree G^{p(sn),sn}_{gn}, where p(sn) is the parent
  // of sn and gn is the root of the created subtree starting on p(sn)
  //----------------------------------------------------------------------
  Node*  
  ReconciliationTreeGenerator::generateSlice(unsigned nLeaves, Node& sn)
  {
    SetOfNodes& currentGamma = gamma[sn.getNumber()];  
    unsigned copy = currentGamma.size();  //Combined iterator 
    unsigned last = copy + nLeaves;       //and gene number
    vector<Node*> leaves;
    while(copy != last)
      {
	if(sn.isLeaf())  //Create terminal leaf/gene gl
	  {
	    // Gene name equals the corresponding species name 
	    // followed by the current gene number
	    string sn_name = sn.getName();
	    if(sn_name == "")
	      {
		sn_name = "Species";
	      }
	    ostringstream gl_name;
	    gl_name << leaf_prefix << sn_name << "_" << copy;

	    gs.insert(gl_name.str(), sn.getName());     // associate gl and sn
	    leaves.push_back(G.addNode(0, 0, gl_name.str())); // create gl
	  }
	else            //Create internal 'leaf' gn and pass on recursion
	  {
	    leaves.push_back(generateSubtree(sn));
	  }
	copy++;
      }
    currentGamma.insertVector(leaves); //Add to current gamma
    return growTree(leaves);           //Return Root, gn, of slice
  }

  // simulateSubtree()
  // creates rooted subtree G_{gn} where gn is placed on sn
  //----------------------------------------------------------------------
  Node*  
  ReconciliationTreeGenerator::generateSubtree(Node& sn)
  {
    Node& snL = *sn.getLeftChild();
    Node& snR = *sn.getRightChild();

    // sample the size of the left and right planted subtrees.
    // Since the sampling is conditioned on that gn survives below sn, i.e., 
    // gn has at least one descendant in L(S_sn), we demand that at least 
    // one slice must survive
    unsigned nLeavesLeft= 0;
    unsigned nLeavesRight = 0;
    while(nLeavesLeft + nLeavesRight == 0)  
      {
	nLeavesLeft = BDP.sampleNumberOfChildren(snL, R.genrand_real1()); 
	nLeavesRight = BDP.sampleNumberOfChildren(snR, R.genrand_real1()); 
      }

    // Generate the planted subtrees
    if(nLeavesRight == 0 )     // gene lost in snR, grow left slice 
      {
	return generateSlice(nLeavesLeft, snL);
      }
    else if(nLeavesLeft == 0 ) // gene lost in snL , grow right slice
      {
	return generateSlice(nLeavesRight, snR);
      }
    else                       // gene survives in both, grow both slices
      {
	return G.addNode(generateSlice(nLeavesLeft, snL), 
			 generateSlice(nLeavesRight, snR));
      }
  } 


  // growTree()
  // Creates a subtree T such that L(T) = containt of vector leaves, 
  // following a birth-death (Yule) process.
  //----------------------------------------------------------------------
  Node*
  ReconciliationTreeGenerator::growTree(vector<Node*>& leaves)
  {
    vector<Node*>::iterator nodeIter;
    //Proceed with the births in reverse order
    while(leaves.size()>1)
      {
	//Create parent of random pair of adjacent leaves and substitute these 
	//for the parent in vector leaves
	nodeIter = leaves.begin() + R.genrand_modulo(leaves.size()-1);
	Node* parent = G.addNode(*nodeIter, *(nodeIter+1));
	leaves.insert(leaves.erase(nodeIter, nodeIter+2), parent);
      }
    return leaves[0];
  }
  
  // createTrueGamma
  // reads gamma and converts to a GammaMap stored in tmpGamma
  //---------------------------------------------------------------------
  void
  ReconciliationTreeGenerator::createTrueGamma(GammaMap& tmpGamma) const
  {
    for(unsigned i = 0; i < gamma.size(); i++)
      {
	Node* sc = S.getNode(i);
	for(unsigned j = 0; j < gamma[i].size(); j++)
	  {
	    Node* gc = gamma[i][j];
	    tmpGamma.addToSet(sc, gc);
	  }
      }
    return;
  }

  //----------------------------------------------------------------------
  //I/O
  //----------------------------------------------------------------------

  // print function for parameters used in operator<<
  //--------------------------------------------------------------------
  const std::string 
  ReconciliationTreeGenerator::print() const
  {
    std::ostringstream os;
    os     << "A reconciled guest tree, G, is generated on the following\n"
	   << "host tree, S:\n"
	   << indentString(S.print())
	   << "using a birth-death process with the following settings:\n"
	   << indentString(BDP.print())
      ;
      
    return os.str();
  }

}//end namespace beep
