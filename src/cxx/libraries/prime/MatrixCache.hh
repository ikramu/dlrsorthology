//
// The Matrix cache is a means of avoiding to recompute frequently
// used probability matrices, as used in BaseSubstitutionMatrix.
// Storing every computed P matrix may cause space problems, so we
// also implement some "garbage collection". If it a long time has
// passed since a matrix was used, it gets dumped.
//

#ifndef MATRIXCACHE_HH
#define MATRIXCACHE_HH

#include <map>
#include <utility>

#include "Beep.hh"


namespace beep
{

  // 
  // This is how often we do garbage collection:
  //
  const long eon = 1000;


  template<class MType>
  class MatrixCache
  {
  public:
    MatrixCache<MType>();
    MatrixCache<MType>(const MatrixCache<MType> &C);

    virtual ~MatrixCache();

    // 
    // Assignment
    //
    MatrixCache<MType>& operator=(const MatrixCache<MType> &C);
  
    //
    // Is the sought matrix in the cache?
    //
    bool check(Real pamdistance) const;
  
    //
    // Access a matrix from the cache. An error is thrown if it
    // is not in the cache. You better used the 'check' method first.
    //
    MType find(Real pamDistance);

    //
    // Insert a matrix in the cache
    //  
    void insert(Real pamDistance, MType M);

    //---------------------------------------------------------------
    //
    // Internal methods
    //
    //---------------------------------------------------------------
  protected:
    //
    // Whenever we collect garbage, everything older than one eon is
    // thrown away.
    //
    void garbageCollect();


    //---------------------------------------------------------------
    //
    // Attributes
    //
    //---------------------------------------------------------------
  private:
    //
    // Abbreviate the silly STL type expression:
    //
    typedef typename std::map<Real, std::pair<long,MType> > CacheType;

    //
    // The actual data structure to store the matrices in
    //
    CacheType theCache;
  
    //
    // The garbage collection must keep track of where we are in "time"
    //
    long generation;
  
  };









  //====================================================================================
  //
  // Definitions of the templates class methods.
  //
  //====================================================================================

  template<typename MType>
  MatrixCache<MType>::MatrixCache()
    : theCache(),
      generation(1l)
  {
    // Boring
  }


  template<typename MType>
  MatrixCache<MType>::MatrixCache(const MatrixCache<MType> &C)
    : theCache(C.theCache),
      generation(C.generation)
  {
    // Boring
  }


  template<typename MType>
  MatrixCache<MType>::~MatrixCache()
  {
    // Boring
  }


  template<typename MType>
  MatrixCache<MType>& 
  MatrixCache<MType>::operator=(const MatrixCache<MType> &C)
  {
    if (this != &C)
      {
	theCache = C.theCache;
	generation = C.generation;
      }

    return *this;
  }


  template<typename MType>
  bool
  MatrixCache<MType>::check(Real pamDistance) const
  {

    if (theCache.find(pamDistance) != theCache.end())
      {
	return true;
      }
    else 
      {
	return false;
      }
  }

  template <typename MType>
  MType
  MatrixCache<MType>::find(Real pamDistance)
  {
    //  cerr << "Retrieving at PAM " << pamDistance << endl;
    typename CacheType::iterator item = theCache.find(pamDistance);
    (*item).second.first = generation;	// Update the usage time of this matrix

    // return the second element of the pair which is stored in the second slot in
    // the map item.
    return (*item).second.second;
  }


  template <typename MType>
  void
  MatrixCache<MType>::insert(Real pamDistance, MType M)
  {
    using std::make_pair;
//     std::cerr << "Inserting at " 
// 	      << pamDistance
// 	      << ", generation "
// 	      << generation
// 	      << std::endl;

    theCache.insert(make_pair(pamDistance, make_pair(generation, M)));
    //    theCache[pamDistance] = std::pair<long,MType>(generation, M);


    generation++;			// Increment the time attribute

    // Once in a while, every eon, we wipe out the old matrices
    if (generation % eon == 0)
      {
	garbageCollect();
      }
  }


  template<typename MType>
  void
  MatrixCache<MType>::garbageCollect()
  {
    //  cerr << "Garbage collecting: ";
    long n=0;
    for (typename CacheType::iterator i = theCache.begin();
	 i != theCache.end();
	 )			// Aha, no increment here, but later...
      {
	if ((*i).second.first < generation - eon)
	  {
	    theCache.erase(i++);
	    n++;
	  }
	else
	  {
	    i++;
	  }
      }
    //  cerr << n << " items, " << theCache.size() << " items left\n";
  }

}//end namespace beep

#endif
