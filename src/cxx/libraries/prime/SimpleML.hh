#ifndef SIMPLEML_HH
#define SIMPLEML_HH

#include <fstream>

#include "SimpleMCMC.hh"
#include "MCMCModel.hh"
#include "PRNG.hh"
#include "Probability.hh"

namespace beep
{
  //! Preliminary Maximum A Posteriori version of Simple MCMC
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  class SimpleML : public SimpleMCMC
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //-------------------------------------------------------------    
    SimpleML(MCMCModel &M, unsigned thinning = 1);
    ~SimpleML();
    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    // Run one ML iteration -- overloaded from SimpleMCMC
    //-------------------------------------------------------------    
    void iterate(unsigned n_iters=1, unsigned print_factor = 1);

//     // Settings
//     //-------------------------------------------------------------
//     void setOutputFile(char *filename);//, char *header=NULL);
//     void setThinning(unsigned i);

//     // Output likelihood info on cerr or not 
//     // Old setting returned.
//     //-------------------------------------------------------------
//     bool setShowDiagnostics(bool yes_no);	

//     std::string estimateTimeLeft(unsigned iteration, unsigned when_done);

    Probability getLocalOptimum();
    std::string getBestState();

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
//     friend std::ostream& operator<<(std::ostream &o, 
// 				    const SimpleML& A);
    std::string print() const;

  private:
    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
//     MCMCModel &model;
//     PRNG &R;		       // Source of randomness
//     unsigned iteration;	       // Output current state everytime this 
//                                //counter is zero.
//     unsigned thinning;	       // iteration is incremented modulo thinning 
//                                //(i.e., iteration < thinning)
//     Probability p;	       // Probability of the model's current state
//     std::ofstream os;	       // Stream to write output to.
//     std::streambuf *cout_buf;  // Save old cout buffer here if we write to file
//     bool show_diagnostics;     // ...likelihood to cerr or not
//     unsigned start_time;       // Keep track of how long we've been computing
    Probability localOptimum;  // Best likelihood value 
    std::string bestState;     // String representation for best state 
  };

}//end namespace beep
#endif
