#ifndef RANDOMTREEGENERATOR_HH
#define RANDOMTREEGENERATOR_HH

#include "Node.hh"
#include "PRNG.hh"
#include "Tree.hh"

#include <vector>

namespace beep
{
  class RandomTreeGenerator
  {
  public:
    static Tree generateRandomTree(std::vector<std::string> leaf_names);
    
  protected:
    Node* growTree(std::vector<Node*> leaves);
    std::vector<Node*> addLeaves(std::vector<std::string> leaf_names);

    //--------------------------------------------------------------------
    //
    // Attributes
    //
    //--------------------------------------------------------------------
  protected:
    Tree G;
    PRNG R;
  };
}
#endif
