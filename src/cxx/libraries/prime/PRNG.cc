#include <cmath>
#include <sys/types.h>
#include <climits>
#include <unistd.h>
#include <iostream>
#include <cassert>

#include "AnError.hh"
#include "PRNG.hh"

// Author: LArs Arvestad, � the MCMC-club, SBC, all rights reserved

namespace beep
{
  using namespace std;
  PRNG::Impl PRNG::x = PRNG::Impl();

  //---------------------------------------------------------------------
  //
  // Construct Destruct Assign
  //
  //---------------------------------------------------------------------
  PRNG::PRNG(unsigned long seed)
    : large_percentile(DEFAULT_LARGE_PERCENTILE)
  {
    x.init_genrand(seed);
  }

  // Use the current process id as seed. This is not a cryptographically
  // good thing, but will do for simulations, right?
  //---------------------------------------------------------------------
  PRNG::PRNG()
    : large_percentile(DEFAULT_LARGE_PERCENTILE)
  {
  }


  PRNG::~PRNG()
  {
  }


  unsigned long
  PRNG::getSeed() const
  {
    return x.getSeed();
  }

  //! Setup internal state.
  //! First time a PRNG is instantiated, this constuctor will run and
  //! set the seed to something reasonable.
  // TODO: We should use /dev/random if it is available! /arve
  /*
    Bj�rn Sundman skrev: 

    Ist�llet f�r att anv�nda processID som seed s� �r det en bra ide att
    l�sa fr�n /dev/random eller /dev/urandom. Se 'man 4 random'. H�r f�r
    du en enkel kodsnutt:

    unsigned long int get_seed(){
    unsigned long int ret;
    FILE * f = fopen("/dev/urandom", "r");
    if(f == NULL)
    return 4711;
    fread( &ret, sizeof(unsigned long int), 1, f);
    fflush(f);
    fclose(f);
    return ret;
    }

    MVH
    Bj�rn 
  */
  PRNG::Impl::Impl()
    : mti(N+1) 
  {
    x.seed = getpid();
    x.init_genrand(seed);
  }

  unsigned long
  PRNG::Impl::getSeed()
  {
    return seed;
  }

  // Common constructor code: Initializes mt[N] with a seed
  //---------------------------------------------------------------------
  void
  PRNG::Impl::init_genrand(unsigned long s)
  {
    mt[0]= s & 0xffffffffUL;
    for (mti = 1; mti < N; mti++) 
      {
	mt[mti] = 
	  (1812433253UL * (mt[mti-1] ^ (mt[mti-1] >> 30)) + mti); 
	/* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
	/* In the previous versions, MSBs of the seed affect   */
	/* only MSBs of the array mt[].                        */
	/* 2002/01/09 modified by Makoto Matsumoto             */
	mt[mti] &= 0xffffffffUL;
	/* for >32 bit machines */
      }
  }




  //---------------------------------------------------------------------
  //
  // Interface
  //
  //---------------------------------------------------------------------

  void
  PRNG::setSeed(unsigned long s)
  {
    x.seed = s;
    x.init_genrand(x.seed);
  }


  // Generates a random number on [0,0xffffffff]-interval
  //---------------------------------------------------------------------
  unsigned long			// Shouldn't this be unsigned int, since we want 32 bits?
  PRNG::genrand_int32()
  {
    return x.genrand_int32();
  }

  unsigned long
  PRNG::Impl::genrand_int32()
  {
    unsigned long y;
    static unsigned long mag01[2]={0x0UL, MATRIX_A};
    /* mag01[x] = x * MATRIX_A  for x=0,1 */

    if (mti >= N) { /* generate N words at one time */
      unsigned kk;

      if (mti == N+1)   /* if init_genrand() has not been called, */
	init_genrand(5489UL); /* a default initial seed is used */

      for (kk=0;kk<N-M;kk++) {
	y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
	mt[kk] = mt[kk+M] ^ (y >> 1) ^ mag01[y & 0x1UL];
      }
      for (;kk<N-1;kk++) {
	y = (mt[kk]&UPPER_MASK)|(mt[kk+1]&LOWER_MASK);
	mt[kk] = mt[kk+(M-N)] ^ (y >> 1) ^ mag01[y & 0x1UL];
      }
      y = (mt[N-1]&UPPER_MASK)|(mt[0]&LOWER_MASK);
      mt[N-1] = mt[M-1] ^ (y >> 1) ^ mag01[y & 0x1UL];

      mti = 0;
    }

    y = mt[mti++];

    /* Tempering */
    y ^= (y >> 11);
    y ^= (y << 7) & 0x9d2c5680UL;
    y ^= (y << 15) & 0xefc60000UL;
    y ^= (y >> 18);

    return y;
  }

  /* generates a random number on [0,1]-real-interval */
  double 
  PRNG::genrand_real1()
  {
    return genrand_int32()*(1.0/4294967295.0); 
    /* divided by 2^32-1 */ 
  }

  /* generates a random number on [0,1)-real-interval */
  double 
  PRNG::genrand_real2()
  {
    return genrand_int32()*(1.0/4294967296.0); 
    /* divided by 2^32 */
  }

  /* generates a random number on (0,1)-real-interval */
  double 
  PRNG::genrand_real3()
  {
    return (((double)genrand_int32()) + 0.5)*(1.0/4294967296.0); 
    /* divided by 2^32 */
  }


  // Lasse's additions
  unsigned
  PRNG::genrand_modulo(unsigned modulus)
  {
    // To make sure that numbers are chosen uniformly at random,
    // only "random" numbers up to a*n are accepted.
    unsigned a = UINT_MAX / modulus;
    unsigned r;
    do {
      r = genrand_int32();
    } while (r >= a * modulus);

    r = r % modulus;		// Get it into the right interval
    return r;

  }


  //
  // Sample from the exponential distribution
  // 
  // It is ensured that no very large sample x is returned.
  // This means that if x >= x_max, and 1 - F(x_max) <= large_percentile
  // the sample is discarded. You can adjust large_percentile with
  // PRNG::set_large_percentile below, but by default, it is set to 0.001.
  //
  double 
  PRNG::exponential(double lambda)
  {
    double u;
    do {				// Choose a u = 1 - F(x)
      u = genrand_real3();	// We'll take a log, so avoid zero. 
    }
    while (u < large_percentile);	// Discard if it is unlikely huge.
    return - log(u) / lambda;
  }

  void
  PRNG::set_large_percentile(double p)
  {
    if (p == 0)
      {
	throw AnError("PRNG: Value for large_percentile must be positive! (0 given)", 1);
      }

    large_percentile = p;
  }

}//end namespace beep
