#ifndef RECONCILIATIONSAMPLER_HH
#define RECONCILIATIONSAMPLER_HH

#include "PRNG.hh"
#include "GuestTreeModel.hh"
#include "PRNG.hh"

namespace beep
{

  //!todo{Update notation to fit published paper!}
  //-------------------------------------------------------------------------
  //
  //! This is a subclass of GuestTreeModel that implements sampling
  //! of reconciliations.
  //! \todo{Add description of samplingalgorithm /bens}
  //-------------------------------------------------------------------------
  //class ReconciliationSampler : public GuestTreeModel
  class ReconciliationSampler : public LabeledGuestTreeModel
  {
  public:
    //--------------------------------------------------------------------
    //
    // {Con,De}struct, assignment
    //
    //--------------------------------------------------------------------
    ReconciliationSampler(Tree &G, StrStrMap &gs, 
			  BirthDeathProbs &nee);
    ReconciliationSampler(ReconciliationModel& rm);
    ReconciliationSampler(const ReconciliationSampler &M);
    virtual ~ReconciliationSampler();

    ReconciliationSampler & operator=(const ReconciliationSampler &rs);

    //-------------------------------------------------------------------------
    //
    // Interface
    //
    //--------------------------------------------------------------------

    //--------------------------------------------------------------------
    // Overloading part of the ProbabilityModel interface
    //--------------------------------------------------------------------
    virtual void  update();

    //! Generates and returns a new \f$\gamma\f$, also stores it in 
    //! attribute gamma
    GammaMap sampleReconciliation();

    //! Generates and returns a new \f$ \gamma\f$ and its probability, 
    //! \f$ Pr(\gamma | G, \lambda, \mu)\f$, also stores it in attribute gamma
    std::pair<GammaMap, Probability> sampleReconciliationAndProb();

    //! Generates a new \f$\gamma\f$ and stores it in attribute gamma
    void generateReconciliation();


  protected:
    //--------------------------------------------------------------------
    //
    // Implementation details
    //
    //--------------------------------------------------------------------
    //! Resets probabilities including those of base class
    void setAttributesAndProbabilities();

    //! Computes C_AX and D_AX
    void computePosteriors();

    //! recursive helper function of computePosteriors().
    void computePosteriors(Node *u);

    //! Compute CA and DA for x and u
    void updateC_A(Node *x, Node *u);

    //! Compute CX and CX for x and u and left child of u
    void updateC_X(Node *x, Node *u);
    //! Starts recursion at top of slice for sampling from C_A and 
    //! C_X of G_u^x, calls recurseSlice
    Probability beginSlice(Node *x, Node *u);

    //! Recursive sampling, starting inside a sliced subtree, from C_X 
    //! and C_A, calls beginSlice
    Probability recurseSlice(Node *x, Node *u, unsigned k);

    //!	Samples and returns x\in [L,U] from probability distribution given by v
    unsigned chooseElement(std::vector<Probability> &v, unsigned L, 
			      unsigned U);

  protected:
    //------------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------------
    // Random number generator
    PRNG R;

    //! \name
    //! Structures for saving posterior probabilities
    //! \f$C(x,u,k,l) = Pr[l^\gamma(x,u)\leq l|l^\gamma(x,p_G(u)=k]\f$
    //! C_A and C_X stores values for C(x,u,k,l)
    //! \f[ \frac{dC(x,u,k,l)}{dl} = c(x,u,k,l) = 
    //!   Pr[l^\gamma(x,u)=l|l^\gamma(x,p_G(u)=k]\f]
    //! D_A and D_X stores values for c(x,u,k,l) (D for derivatives of C_.)
    //! Sampling is made from C_., while probabilities of rec comes from D_.
    //! C_X(x,u) and D_X(x,u) are indexed by index k, and k1
    // @{
    //----------------------------------------------------------------------
    //! C_A stores C(x,u,0,l), for \f$u\in\hat\gamma(x)\f$
    NodeNodeMap< std::vector<Probability> > C_A; 

    //! C_X(x,u)[k][l] stores C(x,v,k,l), where v is the left child of u
    //! \f$ k=|L(G_{u,\gamma(x)})|,l=|L(G_{v,\gamma(x)})|\f$
    NodeNodeMap< std::vector<std::vector<Probability> > > C_X; 

    //! D_A stores c(x,u,0,l), for \f$ u\in\hat\gamma(x)\f$
    NodeNodeMap< std::vector<Probability> > D_A;

    //! D_X(x,u)[k][l] stores c(x,v,k,l), where v is the left child of u
    //! \f$ k=|L(G_{u,\gamma(x)})|,l\geq|L(G_{v,\gamma(x)})|\f$
    NodeNodeMap< std::vector<std::vector<Probability> > > D_X;
    // @}

    // A boolean makes sure that we don't sample without proper initialization!
    // A call to calculateDataProbability is done automatically if needed.
    //----------------------------------------------------------------------
    bool posteriorsComputed;
  };

}//end namespace beep

#endif
