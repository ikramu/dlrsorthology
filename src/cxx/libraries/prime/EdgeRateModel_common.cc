#include "EdgeRateModel_common.hh"

#include "AnError.hh"
#include "Density2P.hh"
#include "Node.hh"
#include "Probability.hh"
#include "Tree.hh"

#include <sstream>

//----------------------------------------------------------------------
//
// class EdgeRateModel_common, inherits from RateModel
// Virtual base class defining a common interface for EdgeRateModels
// These classes model rate (average over positions) of each node in the
// Tree T, with the underlying density function rateProbs. 
//
// Subclasses of type VarRateModel provides models for rates that are 
// constant over edges, ConstRateModel, or that vary between edges, 
// VarRateModel.
//
// This class itself also serve as as an "end class" of a chain of 
// nested EdgeRateModel subclasses.
//
//  Authors Martin Linder, adapted by Bengt Sennblad, 
//  copyright the MCMC club, SBC
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  // Constructor/Destructor
  //----------------------------------------------------------------------
  // TODO: Attribute rateProb probably does not need to be a reference, 
  // but could be an object specific for this class. The constructor should 
  // then instead take a Real for the variance instead of a Density2P
  // Contra this is that with an external rateProb, it could be shared among
  // genes and the variance could be handled in a separate class, hmm!
  // Also, we need to templatize the class if we don't give rateProb 
  // as an argument /bens
  EdgeRateModel_common::EdgeRateModel_common(
		  Density2P& rateProb, const Tree& T, EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      rateProb(&rateProb), 
      T(&T),
      edgeRates(0),
      perturbedRootEdges(rwp)
  { 
  };

  EdgeRateModel_common::EdgeRateModel_common(const EdgeRateModel_common& erm)
    : EdgeRateModel(erm),
      rateProb(erm.rateProb), 
      T(erm.T),
      edgeRates(erm.edgeRates),
      perturbedRootEdges(erm.perturbedRootEdges)
  {
  };

  EdgeRateModel_common::~EdgeRateModel_common() {};

  EdgeRateModel_common& 
  EdgeRateModel_common::operator=(const EdgeRateModel_common& erm)
  {
    if(this != &erm)
      {
	EdgeRateModel::operator=(erm);
	rateProb = erm.rateProb;
	T = erm.T;
	edgeRates = erm.edgeRates;
	perturbedRootEdges = erm.perturbedRootEdges;
      }
    return *this;
  };

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // Access to shared members 
  // These return const objects, as a reasonable encapsulation compromise
  //----------------------------------------------------------------------
  const Density2P& 
  EdgeRateModel_common::getDensity() const
  {
    return *rateProb;
  }

  const Tree& 
  EdgeRateModel_common::getTree() const
  {
    return *T;
  }  

  RealVector& 
  EdgeRateModel_common::getRateVector() const
  {
    return edgeRates;
  }  


  // Interface inherited from ProbabilityModel - likelihood default to 1.0
  //----------------------------------------------------------------------
  Probability 
  EdgeRateModel_common::calculateDataProbability() 
  {return 1.0;}

  void 
  EdgeRateModel_common::update() 
  {}

  string
  EdgeRateModel_common::type() const
  {
    return "EdgeRateModel_common";
  }

  // Returns mean of underlying density.
  Real 
  EdgeRateModel_common::getMean() const
  {
    return rateProb->getMean();
  }

  // Returns variance of underlying density. 
  Real 
  EdgeRateModel_common::getVariance() const
  {
#ifdef USE_EMBEDDED_PARAMS
    return rateProb->getBeta();
#else
    return rateProb->getVariance();
#endif
  }
  
  // Returns mean of underlying density. 
  void 
  EdgeRateModel_common::setMean(const Real& newValue)
  {
    rateProb->setMean(newValue);
    return;
  }

  // Returns variance of underlying density.
  void 
  EdgeRateModel_common::setVariance(const Real& newValue)
  {
#ifdef USE_EMBEDDED_PARAMS
    rateProb->setBeta(newValue);
#else
    rateProb->setVariance(newValue);
#endif
    return;
  }
  
  // returns rate for (incoming edge to) Node node - default strict mol.clock
  // TODO: Should these be pure virtual - is EdgeRatemodel ever used in
  // its own right?/bens
  //----------------------------------------------------------------------
  Real
  EdgeRateModel_common::getRate(const Node* n) const
  {
    assert(n != 0);     // Check precondition pointer is no NULL
    // Note! BeepVector ensures saneness of n /bens
    return edgeRates[n];
  }

  Real
  EdgeRateModel_common::getRate(const Node& n) const
  {
    return getRate(&n);
  }
  
  Real 
  EdgeRateModel_common::operator[](const Node& n) const
  {
    return getRate(n);
  }

  Real 
  EdgeRateModel_common::operator[](const Node* n) const
  {
    return getRate(n); // send to pointer version, since n could be NULL
  }

  // Overloading functions inherited from EdgeWeightModel
  //------------------------------------------------------------------
  unsigned 
  EdgeRateModel_common::nWeights() const
  {
    return nRates();
  }
  void 
  EdgeRateModel_common::setWeight(const Real& weight, const Node& u)
  {
    return setRate(weight, u);
  }
  RealVector& 
  EdgeRateModel_common::getWeightVector() const
  {
    return getRateVector();
  }
  Real 
  EdgeRateModel_common::getWeight(const Node& u) const
  {
    return getRate(u);
  }
  void 
  EdgeRateModel_common::getRange(Real& low, Real& high)
  {
    return rateProb->getRange(low, high);
  }

  EdgeWeightModel::RootWeightPerturbation
  EdgeRateModel_common::getRootWeightPerturbation() const
  {
  	return perturbedRootEdges;
  }

  void
  EdgeRateModel_common::setRootWeightPerturbation(RootWeightPerturbation rwp)
  {
	  perturbedRootEdges = rwp;
  }

  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream& o, const EdgeRateModel_common& rm)
  {
    return o << indentString(rm.print());
  };

  std::string  
  EdgeRateModel_common::print() const
  {
    std::ostringstream oss;
    oss << "The rate probabilities are modeled using a \n"
	<< rateProb->print();

    return oss.str();
  }

}//end namespace beep

