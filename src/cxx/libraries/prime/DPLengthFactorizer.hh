#ifndef DPLENGTHFACTORIZER_HH
#define DPLENGTHFACTORIZER_HH

#include <string>

#include "DP_ML.hh"
#include "EdgeRateMCMC.hh"
#include "EdgeWeightHandler.hh"
#include "Probability.hh"
#include "ReconciliationTimeMCMC.hh"

/*********************************************************************

  DPLengthFactorizer

  The purpose of this class is to enable fast separation of the
  lengths into rates and times. We sometimes interrupt the ML-run
  and check whether we can separate better using Dynamic Programming

**********************************************************************/

namespace beep
{
  class DPLengthFactorizer : public StdMCMCModel,
			     public ProbabilityModel
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //-------------------------------------------------------------
    DPLengthFactorizer(MCMCModel& prior, Tree& G, Tree& S, 
		       ReconciliationTimeMCMC& rtm, EdgeRateMCMC& erm,
		       Density2P& df,
		       Real& birthRate, Real& deathRate, 
		       unsigned noOfDiscrIntervals, double DPratio);
    DPLengthFactorizer(const DPLengthFactorizer &dplf); // Copy constuctor
    ~DPLengthFactorizer();
    DPLengthFactorizer& operator=(const DPLengthFactorizer& dplf);


    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    //-----------------------------------------------------------------------
    // StdMCMCModel interface
    //-----------------------------------------------------------------------
    MCMCObject suggestOwnState();
    //    MCMCObject suggestOwnState(unsigned x);
    void commitOwnState();
    //    void commitOwnState(unsigned x);
    void discardOwnState();
    //    void discardOwnState(unsigned x);

    std::string ownStrRep() const;
    std::string ownHeader() const;

    Probability updateDataProbability();


  protected:
    //-----------------------------------------------------------------------
    // ProbabilityModel interface
    //-----------------------------------------------------------------------
    virtual Probability calculateDataProbability();
    virtual void update();

  public:

    //----------------------------------------------------------------------
    // I/O
    // I have not converged on a consensus for this yet, but the 
    // following functions should suffice
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const DPLengthFactorizer& A);
    std::string print() const;
    
  private:
    //-----------------------------------------------------------------------
    //
    // Implementation
    //
    //-----------------------------------------------------------------------

  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //  
    //----------------------------------------------------------------------
    Tree* G;
    Tree* S; 
    ReconciliationTimeMCMC* rtm;
    EdgeRateMCMC* erm;
    unsigned noOfDiscrIntervals;
    double DPratio;
    DP_ML optimizer;
    bool doingLF;
    Tree* oldG;
    std::vector<Real> oldRates;
    std::vector<Real> oldNodeTimes;
  };

}//end namespace beep
#endif
