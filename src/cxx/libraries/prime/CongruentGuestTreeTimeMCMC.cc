#include "CongruentGuestTreeTimeMCMC.hh"

#include "AnError.hh"
#include "MCMCObject.hh"
#include "Node.hh"
#include "Probability.hh"
#include "StrStrMap.hh"
#include "Tree.hh"

#include <sstream>

namespace beep
{
  using namespace std;
  //-----------------------------------------------------------------
  // Construct/destruct/assign
  //-----------------------------------------------------------------
  CongruentGuestTreeTimeMCMC::CongruentGuestTreeTimeMCMC(MCMCModel& prior, 
							 Tree& S_in,
							 Tree& G_in, 
							 StrStrMap& gs)
    : StdMCMCModel(prior, 0, "CongruentGandS"),
      S(&S_in),
      G(&G_in),
      sigma(*G, *S, gs)
  {
    if(G->hasTimes() == false)
      {
	RealVector* rv = new RealVector(*G);
	G->setTimes(*rv, true);
      }
    initG(*G->getRootNode(), sigma);
  }
  
  CongruentGuestTreeTimeMCMC::~CongruentGuestTreeTimeMCMC()
  {}
  CongruentGuestTreeTimeMCMC::
  CongruentGuestTreeTimeMCMC(const CongruentGuestTreeTimeMCMC& m)
    : StdMCMCModel(m) ,
      S(m.S),
      G(m.G),
      sigma(m.sigma)
  {}

  CongruentGuestTreeTimeMCMC& 
  CongruentGuestTreeTimeMCMC::operator=(const CongruentGuestTreeTimeMCMC& m)
  {
    if(&m != this)
      {
	StdMCMCModel::operator=(m);
	S = m.S;
	G = m.G;
	sigma = m.sigma;
      }
    return *this;
  }
  //-----------------------------------------------------------------
  // Interface
  //-----------------------------------------------------------------
  MCMCObject
  CongruentGuestTreeTimeMCMC::suggestOwnState()
  {
    throw AnError("we should never go here", 1);
    return MCMCObject(1.0, 1.0);
  }
  
  void 
  CongruentGuestTreeTimeMCMC::commitOwnState()
  {
    throw AnError("we should never go here", 1);
  }
  
  void 
  CongruentGuestTreeTimeMCMC::discardOwnState()
  {
    throw AnError("we should never go here", 1);
  }
  
  std::string 
  CongruentGuestTreeTimeMCMC::ownStrRep() const
  {
    return "";
  }

  std::string 
  CongruentGuestTreeTimeMCMC::ownHeader() const
  {
    return "";
  } 
  
  void  
  CongruentGuestTreeTimeMCMC::update()
  {
    initG(*G->getRootNode(), sigma);
  }

  Probability 
  CongruentGuestTreeTimeMCMC::updateDataProbability()
  {
    initG(*G->getRootNode(), sigma);

    return 1.0;
  }

  string
  CongruentGuestTreeTimeMCMC::print() const
  {
    ostringstream oss;
    oss << name 
	<< ": The guest tree and its divergence times are\n"
	<< "requested to be congruent with the host tree\n"
	<< StdMCMCModel::print();
    return oss.str();
  }
  //-----------------------------------------------------------------
  // Implementation
  //-----------------------------------------------------------------
  // PRE: for any child v of u, sigma[v] != sigma[u]
  void 
  CongruentGuestTreeTimeMCMC::initG(Node& u, LambdaMap& sigma)
  {
    // Check precondition
    assert(u.isLeaf() || (sigma[u] != sigma[*u.getLeftChild()] 
			  && sigma[u] != sigma[*u.getRightChild()]));
    
    G->setTime(u, S->getTime(*sigma[u]));
    if(u.isLeaf() == false)
      {
	initG(*u.getLeftChild(), sigma);
	initG(*u.getRightChild(), sigma);
      }
    else
      {
	assert(G->getTimes() == 0);
// 	G->setTime(u, 0);
      }
  }

}//end namespace beep
