/* 
 * File:   EdgeDiscGSRObjects.hh
 * Author: peter9
 *
 * Created on November 30, 2009, 12:29 PM
 *
 * Since EdgeDiscGSR works with references internally some objects
 * it uses must be stored somewhere else if a snapshot of an EdgeDiscGSR object
 * is to be stored away.
 * An EdgeDiscGSRObjects object is just a placeholder for these objects.
 */

/**
 * Overview of members:
 * public:
 *  EdgeDiscGSRObjects(const Tree &G, const GammaDensity &gamma, const EdgeDiscBDProbs &bd_probs);
 *  Tree & getGeneTree();
 *  GammaDensity & getGamma();
 *  EdgeDiscBDProbs & getBDProbs();
 *  virtual ~EdgeDiscGSRObjects();
 * private:
 *  Tree m_G;
 *  GammaDensity m_gamma;
 *  EdgeDiscBDProbs m_bd_probs;
 *
 */

#ifndef _PRIMESAMPLE_HH
#define	_PRIMESAMPLE_HH

#include "Tree.hh"
#include "GammaDensity.hh"
#include "EdgeDiscBDProbs.hh"

namespace beep{

    class EdgeDiscGSRObjects {
    public:
        /**
         * EdgeDiscGSRObjects
         *
         * Constructor with arguments.
         *
         * @param G A gene tree.
         * @param gamma A gamma density function.
         * @param bd_probs Birth and death rate information.
         */
        EdgeDiscGSRObjects(const Tree &G, const GammaDensity &gamma, const EdgeDiscBDProbs &bd_probs):
        m_G(G), m_gamma(gamma), m_bd_probs(bd_probs)
        {}

        /**
         * getGeneTree
         *
         * Return a reference to the gene tree stored in this object.
         */
        Tree & getGeneTree(){return m_G;}

        /**
         * getGamma
         *
         * Returns a reference to the gamma density function stored
         * in this object.
         */
        GammaDensity & getGamma(){return m_gamma;}

        /**
         * getBDProbs
         *
         * Returns a reference to the birth and death probabilities
         * stored in this object.
         */
        EdgeDiscBDProbs & getBDProbs(){return m_bd_probs;}


        /**
         * ~EdgeDiscGSRObjects
         *
         * Destructor.
         */
        virtual ~EdgeDiscGSRObjects(){}
    private:
        Tree m_G;                   //Gene tree
        GammaDensity m_gamma;       //Density function
        EdgeDiscBDProbs m_bd_probs; //Birth-death rates holder
    };

};

#endif	/* _PRIMESAMPLE_HH */

