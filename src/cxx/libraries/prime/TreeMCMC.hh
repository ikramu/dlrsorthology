#ifndef TREEMCMC_HH
#define TREEMCMC_HH


#include "Beep.hh"
#include "BranchSwapping.hh"
#include "GuestTreeModel.hh"
#include "InvMRCA.hh"
#include "StdMCMCModel.hh"

#include <string>

namespace beep
{

  // Forward declarations
  class ReconciliationModel;
  class Tree;

  //------------------------------------------------------------------
  //
  // TreeMCMC
  //! This virtual base class implements an MCMC method for finding trees. 
  //! Using standard algorithms to perturb trees, e.g., NNI and 
  //! re-rooting, the tree space is investigated.
  //! Subclasses (UniformTreMCMC and GuestTreeMCMC) also inherits from
  //! some ProbabilityModel class that gives the prior probability of
  //! a tree.
  //!
  //! Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
  //
  //------------------------------------------------------------------
  class TreeMCMC : public StdMCMCModel
  {
  protected:
    //------------------------------------------------------------------
    //
    // Constructor
    // 
    //------------------------------------------------------------------
    TreeMCMC(MCMCModel& prior, Tree& T_in, const Real& suggestRatio = 1.0);
    TreeMCMC(MCMCModel& prior, Tree& T_in, 
	     const std::string& name, const Real& suggestRatio);
    ~TreeMCMC();	       
    TreeMCMC(const TreeMCMC& rtm);
    TreeMCMC&operator=(const TreeMCMC& rtm);

    //---------------------------------------------------------------------
    //
    // Interface 
    //
    //---------------------------------------------------------------------
  public:
    //! Access to tree.
    Tree& getTree() const;
    //! Exchange tree - this allows dynamic binding 
    //! Remember to do an update() before doing calculateDataProbability().
    void setTree(const Tree& T_in);

    virtual void fixRoot();
    virtual void fixTree();
	
    //! Sets flag for sending detailed tree perturbation events to listeners.
    //---------------------------------------------------------------------
    virtual void setDetailedNotification(bool doSendDetails);
	
    //---------------------------------------------------------------------
    // Interface to StdMCMCModel
    //---------------------------------------------------------------------
    MCMCObject  suggestOwnState();
    void        commitOwnState();
    void        discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    std::string getAcceptanceInfo() const;
    void updateToExternalPerturb(const Tree& newT);

    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const TreeMCMC& A);
    std::string print() const;

    //---------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
  protected:
    virtual void init();
    virtual void update_idx_limits();

  protected:
    //---------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
    BranchSwapping mrGardener;       //!< The actual tree-perturber
    Tree* T;                         //!< Current state tree
    Tree old_T;                      //!< Saves value of previous state
    RealVector old_times;            //!< Saves values of external attributes
    RealVector old_rates;            //!< Saves values of external attributes
    RealVector old_lengths;          //!< Saves values of external attributes
    std::vector<Real> idx_limits;    //!< Holds local perturb-probs
    bool detailedNotifInfo;          //!< Flag for notifying tree listeners with detailed info.
    unsigned whichPerturbType;         //!< Flag for curr. perturbation type.
    std::pair<unsigned,unsigned> rerootAccPropCnt;   //!< Reroot acc. and tot. prop. count.
    std::pair<unsigned,unsigned> nniAccPropCnt;   //!< NNI acc. and tot. prop. count.
    std::pair<unsigned,unsigned> sprAccPropCnt;   //!< SPR acc. and tot. prop. count.
  };



  //==============================================================
  //
  // subclass UniformTreeMCMC
  //! This TreeMCMC subclass implements an underlying uniform 
  //! distribution on trees.
  //! Using standard algorithms to perturb trees, e.g., NNI and 
  //! re-rooting, the tree space is investigated.
  //! 
  //! copyright The MCMC club, SBC
  // 
  //==============================================================
  class UniformTreeMCMC : public TreeMCMC
  {
  public:
    UniformTreeMCMC(MCMCModel& prior, Tree& T, Real suggestRatio = 1.0);
    UniformTreeMCMC(MCMCModel& prior, Tree& T, const std::string& name_in, 
		    Real suggestRatio = 1.0);
    ~UniformTreeMCMC();
    UniformTreeMCMC(const UniformTreeMCMC& utm);
    UniformTreeMCMC& operator=(const UniformTreeMCMC& utm);
    void fixRoot();
    std::string print() const;
  protected:
    //! resets probability of a tree with  x number of leaves.
    void init();

    //-------------------------------------------------------------
    //
    // Interface from ProbailityModel
    //
    //-------------------------------------------------------------
    Probability updateDataProbability();
    void update();

    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
  protected:
    Probability p;    //!< The uniform probability of a tree
    unsigned nLeaves; //!< number of leaves used when init() was performed last
    bool isRooted;    //!< We might handle unrooted tree, which affects p
  };

  //==============================================================
  //
  // subclass GuestTreeMCMC
  //! This TreeMCMC subclass implements an underlying distribution
  //! on trees given by the Gene Evolution Model.
  //! Using standard algorithms to perturb trees, e.g., NNI and 
  //! re-rooting, the tree space is investigated.
  //! 
  //! copyright The MCMC club, SBC
  // 
  //==============================================================
  class GuestTreeMCMC : public TreeMCMC, public GuestTreeModel 
  {
  public:
    GuestTreeMCMC(MCMCModel& prior, Tree& G, StrStrMap& gs, 
		  BirthDeathProbs& bdp, Real suggestRatio = 1.0);
    GuestTreeMCMC(MCMCModel& prior, ReconciliationModel& rm, 
		  Real suggestRatio = 1.0);
    GuestTreeMCMC(MCMCModel& prior, ReconciliationModel& rm,
		  const std::string& name_in, Real suggestRatio = 1.0);
    ~GuestTreeMCMC();
    GuestTreeMCMC(const GuestTreeMCMC& utm);
    GuestTreeMCMC& operator=(const GuestTreeMCMC& utm);
    std::string print() const;

    //-------------------------------------------------------------
    //
    // Interface from ProbailityModel
    //
    //-------------------------------------------------------------
    Probability updateDataProbability();
  };

  class OrthologyMCMC : public GuestTreeMCMC
  {
  public:
    OrthologyMCMC(MCMCModel& prior, Tree& G, StrStrMap& gs, 
		  BirthDeathProbs& bdp, Real suggestRatio = 1.0);
    OrthologyMCMC(MCMCModel& prior, ReconciliationModel& rm, 
		  Real suggestRatio = 1.0);
    OrthologyMCMC(MCMCModel& prior, ReconciliationModel& rm,
		  const std::string& name_in, Real suggestRatio = 1.0);
    ~OrthologyMCMC();
    OrthologyMCMC(const OrthologyMCMC& utm);
    OrthologyMCMC& operator=(const OrthologyMCMC& utm);
    //     std::string print() const;

    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    //! sets up orthoVec and orthoProb needed for recording orthology
    void estimateOrthology(bool speciationNotOrthology = false);

    //-------------------------------------------------------------
    // Interface from GuestTreeMCMC
    //-------------------------------------------------------------
    void commitOwnState();
    Probability updateDataProbability();

    //! Overloading base class functions
    //!@{
    std::string ownStrRep() const;
    std::string ownHeader() const;
    //!@}

    //-------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------
    Probability recordOrthology();

  protected:
    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
    std::vector<unsigned> orthoVec;
    std::vector<Probability> orthoProb;
    InvMRCA invMRCA;
    bool specNotOrtho;
  };

}//end namespace beep
#endif

