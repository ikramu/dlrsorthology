#include "EdgeDiscTree.hh"

namespace beep
{

using namespace std;

EdgeDiscTree::EdgeDiscTree(Tree& S, EdgeDiscretizer* discretizer) :
	EdgeDiscPtMap<Real>(S),
	PerturbationObservable(),
	m_S(&S),
	m_discretizer(discretizer),
	m_timesteps(S),
	m_cacheTimesteps(S)
{
	rediscretize();
	// Set m_DS member in subclass to point to this.
	// This is because the original baseline map is in fact itself.
	m_DS = this;
}


EdgeDiscTree::EdgeDiscTree(const EdgeDiscTree& eds) :
	EdgeDiscPtMap<Real>(*eds.m_S),
	PerturbationObservable(),
	m_S(eds.m_S),
	m_discretizer(eds.m_discretizer),
	m_timesteps(eds.m_timesteps),
	m_cacheTimesteps(eds.m_cacheTimesteps)
{
	// Set m_DS member in subclass to point to this.
	// This is because the original baseline map is in fact itself.
	m_DS = this;
}


EdgeDiscTree::~EdgeDiscTree()
{
}

bool
EdgeDiscTree::isSpeciation(const EdgeDiscretizer::Point &p)
{
	return p.second == 0;
}


EdgeDiscTree&
EdgeDiscTree::operator=(const EdgeDiscTree& eds) {
	if (this != &eds)
	{
		// Members inherited from EdgeDiscPtMap.
		m_DS = this;				// See constructor for explanation.
		m_vals = eds.m_vals;
		m_cache = eds.m_cache;
		m_cacheIsValid = eds.m_cacheIsValid;
		// Members from PerturbationObservable.
		m_notifyPertObservers = eds.m_notifyPertObservers;
		m_pertObservers = eds.m_pertObservers;
		// Own members.
		m_S = eds.m_S;
		m_discretizer = eds.m_discretizer;
		m_timesteps = eds.m_timesteps;
		m_cacheTimesteps = eds.m_cacheTimesteps;
	}
	return *this;
}


void
EdgeDiscTree::rediscretize()
{
	// Recompute discretization points.
	m_discretizer->discretize(*m_S, m_vals);
	
	// Recompute edge times.
	for (Tree::const_iterator it = m_S->begin(); it != m_S->end(); ++it)
	{
		if ((*it)->isRoot() && (*it)->getTime() < 1e-8)
		{
			m_timesteps[*it] = 0;
		}	
		else
		{
			// It is assumed that the points are in fact equidistantly spaced (save for end intervals).
			m_timesteps[*it] = (m_vals[*it][2]) - (m_vals[*it][1]);
		}
	}
}


void
EdgeDiscTree::rediscretizeNode(const Node* n)
{
	const Node* lc = n->getLeftChild();
	const Node* rc = n->getRightChild();
	m_discretizer->discretizeEdge(n, m_vals[n]);
	m_discretizer->discretizeEdge(lc, m_vals[lc]);
	m_discretizer->discretizeEdge(rc, m_vals[rc]);
	m_timesteps[n] = (m_vals[n][2]) - (m_vals[n][1]);
	m_timesteps[lc] = (m_vals[lc][2]) - (m_vals[lc][1]);
	m_timesteps[rc] = (m_vals[rc][2]) - (m_vals[rc][1]);
}


bool
EdgeDiscTree::isAncestor(const EdgeDiscretizer::Point &p1, const EdgeDiscretizer::Point &p2)
{
	if (p1.first->getNumber() == p2.first->getNumber())
		return p1.second >= p2.second;
	else
		return p1.first->dominates(*(p2.first));
}


bool
EdgeDiscTree::isProperAncestor(const EdgeDiscretizer::Point &p1, const EdgeDiscretizer::Point &p2)
{
	if (p1.first->getNumber() == p2.first->getNumber())
		return p1.second > p2.second;
	else
		return p1.first->dominates(*(p2.first));
}


Real
EdgeDiscTree::getMinTimestep() const
{
	Real ts = numeric_limits<Real>::max();
	for (Tree::const_iterator it = m_S->begin(); it != m_S->end(); ++it)
	{
		const Node* n = (*it);
		if (n->isRoot() && n->getTime() < 1e-8) { continue; }
		if (m_timesteps[n] < ts) { ts = m_timesteps[n]; }
	}
	return ts;
}


void
EdgeDiscTree::cache()
{
	m_cacheTimesteps = m_timesteps;
	EdgeDiscPtMap<Real>::cache();
}


void
EdgeDiscTree::cachePath(const Node* node)
{
	const Node* n = node;
	while (n != NULL)
	{
		m_cacheTimesteps[n] = m_timesteps[n];
		n = n->getParent();
	}
	EdgeDiscPtMap<Real>::cachePath(node);
}


void
EdgeDiscTree::restoreCache()
{
	if (m_cacheIsValid)
	{
		m_timesteps = m_cacheTimesteps;
		EdgeDiscPtMap<Real>::restoreCache();
	}
}


void
EdgeDiscTree::restoreCachePath(const Node* node)
{
	if (m_cacheIsValid)
	{
		const Node* n = node;
		while (n != NULL)
		{
			m_timesteps[n] = m_cacheTimesteps[n];
			n = n->getParent();
		}
		EdgeDiscPtMap<Real>::restoreCachePath(node);
	}
}

} // end namespace beep.

