#include "FastCacheSubstitutionModel.hh"

#include "Node.hh"
#include "EdgeWeightHandler.hh"
#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "SequenceData.hh"
#include "TransitionHandler.hh"
#include "SiteRateHandler.hh"
#include "Tree.hh"

#include <sstream>
#include <cassert>
#include <memory>


namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------
  FastCacheSubstitutionModel::
  FastCacheSubstitutionModel(const SequenceData& Data, 
			     const Tree& T_in, 
			     SiteRateHandler& siteRates_in, 
			     const TransitionHandler& Q,
			     EdgeWeightHandler& edgeWeights,
			     const vector<string>& partitionsList)
    : SubstitutionModel(Data, T_in, siteRates_in, Q, edgeWeights, partitionsList),
      likes(T_in),
      tmp(Q.getAlphabetSize())
  {
    init();
  }
  
  // Copy Constructor
  //----------------------------------------------------------------------
  FastCacheSubstitutionModel::
  FastCacheSubstitutionModel(const FastCacheSubstitutionModel& sm)
    :SubstitutionModel(sm),
     likes(sm.likes),
     tmp(sm.tmp)
  {
  }
  
  // Destructor
  //----------------------------------------------------------------------
  FastCacheSubstitutionModel::~FastCacheSubstitutionModel()
  {
  }

  // Assignment operator
  //----------------------------------------------------------------------
  FastCacheSubstitutionModel& 
  FastCacheSubstitutionModel::operator=(const FastCacheSubstitutionModel& sm)
  {
    if(this != &sm)
      {
	SubstitutionModel::operator=(sm);
	likes = sm.likes;
	tmp = sm.tmp;
      }
    return *this;
  }
  
  //----------------------------------------------------------------------
  //
  // Public interface
  //
  //----------------------------------------------------------------------

  // Likelihood calculation - interface to MCMCModels
  //----------------------------------------------------------------------
  Probability 
  FastCacheSubstitutionModel::calculateDataProbability()
  {
    //! \todo{Check if this really should be an assert/bens}
    assert(T->getRootNode()->isLeaf() == false);

    Node* left = T->getRootNode()->getLeftChild();
    Node* right = left->getSibling();

    // reset return value
    like = 1.0;
    // Loop over partitions
    for(unsigned i = 0; i < partitions.size(); i++)
      {
	// First recurse down to initiate likes
	if(T->perturbedTree() == true)
	  {
	    initLikelihood(*left, i);
	    initLikelihood(*right, i);
	  }
	else
	  {
	    recursiveLikelihood(*left, i);
	    recursiveLikelihood(*right, i);
	  }
 	like *= rootLikelihood(i);
      }
    T->perturbedTree(false);

    return like;
  }

  // Partial Likelihood caluclation - for use with perturbedNode
  Probability 
  FastCacheSubstitutionModel::calculateDataProbability(Node* n)
  {
    assert(n != 0); // Check precondition

    // reset return value
    like = 1.0;

    // Loop over partitions
    for(unsigned i = 0; i < partitions.size(); i++)
      {
	// Perturbations in some classes (e.g., ReconciliationTimeMCMC) may
	// affect the outgoing edges of n, therefore we start by descending
	// one step
	if(!n->isLeaf())
	  {
	    updateLikelihood(*n->getLeftChild(), i);
	    updateLikelihood(*n->getRightChild(), i);
	  }
	// Then we just ascend up to root
	while(!n->isRoot())
	  {
	    updateLikelihood(*n, i);
	    n = n->getParent();
	    assert(n != 0); //Check for sanity
	  }
 	like *= rootLikelihood(i);
     }
    return like;
  }

  //------------------------------------------------------------------
  //
  // I/O
  // 
  //------------------------------------------------------------------
  // TODO: Shape up these print functions /bens
//   std::string
//   FastCacheSubstitutionModel::print() const
//   {
//     return print(false);
//   }

  std::string
  FastCacheSubstitutionModel::print(bool estimateRates) const
  {
    return "FastCacheSubstitutionModel: " + SubstitutionModel::print(estimateRates);
  }

//   std::string
//   FastCacheSubstitutionModel::print(const bool& estimateRates) const
//   {
//     ostringstream oss;
//     oss
//       << "Substitution likelihood is performed"
//       // Here we should maybe only give the part of data that we are using,
//       // i.e., the partitions used?
//       << " using sequence data:\n"
//       // The funciton below should be replaced by D.print(partition)
//       // that should write a "name" for the Sequence data and the 
//       // user-definedpartition that is used (e.g., either exactly which 
//       // positions are defined or the "name" used for the partition
//       << indentString(D->print(), "  ")
//       << indentString("partitions, any user-defined partitions of the data\n")
//       << "and substitution matrix:\n"
//       << indentString(Q->print());
//     return oss.str();
//   }


  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------

  // Initiates all data structures for storing likelihood.
  void
  FastCacheSubstitutionModel::init()
  {
    // Create a template PartitionLike and initiate its element
    PartitionLike pl;
    for(PartitionVec::const_iterator i = partitions.begin();
	i != partitions.end(); i++)
      {
	pl.push_back(make_pair(PatternTranslator(i->size()), SubPatternLike()));
      }
    // Fill likes with a copy of pl for each node
    likes = BeepVector<PartitionLike>(*T, pl); 

    T->perturbedTree(true);

    //! \todo{ We should maybe only init the PatterTranslator here and 
    //!        don't do the probability computation/bens}
    calculateDataProbability();
    return;
  }


  // Helper functions for Likelihood calculation
  // Recurse down to leaves and then updates Likelihoods for each node on 
  // the way up
  //----------------------------------------------------------------------
  // \f$ \{ Pr[D_{i,\cdot}|T_n, w, r, p], r\in siteRates, p \in partitions[partition]\}\f$
  //----------------------------------------------------------------------
  Probability
  FastCacheSubstitutionModel::rootLikelihood(const unsigned& partition)
  {
    Node&n = *T->getRootNode();
    if(n.isLeaf())
      {
	return 1.0;
      }
    else
      {
	// Initiate return value
	Probability ret(1.0);

	// Set up data and likelihood storage
	PatternVec& pv = partitions[partition];

	// Get nested Likelihoods
	PatternLike& pl_l = likes[*n.getLeftChild()][partition];
	PatternLike& pl_r = likes[*n.getRightChild()][ partition];

	LA_Vector current(Q->getAlphabetSize());
	// Lastly, loop over all unique patterns in pv
	for(unsigned i = 0; i < pv.size(); i++)
	  {
	    Probability patternL = 0.0;

	    // Compute Pr[Dk|T, ew, r(j)] for each siteRateCategory j
	    for(unsigned j = 0; j < siteRates->nCat(); j++)
	      {
		// Now, this looks weird, but pl_l.first is the 
		// PatternTranslator, while pl_L.second holds the 
		// RateLike in its second element. 
		LA_Vector& left = pl_l.second[pl_l.first[i]].second[j];
		LA_Vector& right = pl_r.second[pl_r.first[i]].second[j];

		// left.*right, store result in tmp
		left.ele_mult(right, tmp);
		// pi * tmp, store result in current
		Q->multWithPi(tmp, current);
		patternL += current.sum();
	      } 
	    unsigned exp = pv[i].second; // # of occurrences of pattern
	    //! \todo{The division below is not defined for unsigned - ifx!}
// 	    ret *= pow(patternL, exp); // Pr[rateCat]=1/nCat
 	    ret *= pow(patternL / siteRates->nCat(), exp); // Pr[rateCat]=1/nCat
	  }
	return ret;
      }
  }

  // This function must be improved and speeded up!
  vector<const Node*>
  FastCacheSubstitutionModel::initLikelihood(const Node& n, 
						  const unsigned& partition)
  {
    typedef __gnu_cxx::hash_map<string, unsigned>   PatternMap;
    vector<const Node*> leaves;
    if(!n.isLeaf())
      {
	leaves = initLikelihood(*n.getLeftChild(),partition);
	vector<const Node*> tmp = initLikelihood(*n.getRightChild(),
						      partition);
	leaves.insert(leaves.end(), tmp.begin(), tmp.end());
      }
    else
      {
	leaves.push_back(&n);
      }


    PatternVec& pv = partitions[partition];
    PatternTranslator& pt = likes[n][partition].first;
    SubPatternLike& sl = likes[n][partition].second;
    sl.clear();  

    RateLike templ(siteRates->nCat(), LA_Vector(Q->getAlphabetSize()));    
    PatternMap tmp_map;
    unsigned sli = 0;    // Index for sl's next element
   
    for(unsigned i = 0; i < pv.size(); i++) //  iterate over treePatterns
      {
	// Get pattern (can this be done in a neater way?)
	ostringstream oss;
	for(vector<const Node*>::const_iterator ci = leaves.begin(); 
	    ci != leaves.end(); ci++)
	  {
	    oss << (*D)((*ci)->getName(), pv[i].first); // =D[gene, position]
	  }
	string subPattern = oss.str();

	// Check if this pattern already has been calculated	
	PatternMap::const_iterator hi = tmp_map.find(subPattern);
	if(hi == tmp_map.end())
 	  {
	    // If so, first create a corresponding RateLike...
	    sl.push_back(make_pair(i, templ));

	    // ...then record what pattern it refers to
	    pt[i] = sli;
	    tmp_map[subPattern] = sli;
	    sli++; // Lastly, update sl's index
	  }
	else
	  {
	    pt[i] = hi->second;
	  }
      }
    updateLikelihood(n, partition);
    return leaves;
  }
  
  void
  FastCacheSubstitutionModel::recursiveLikelihood(const Node& n, 
						  const unsigned& partition)
  {
    if(!n.isLeaf())
      {
	recursiveLikelihood(*n.getLeftChild(),partition);
	recursiveLikelihood(*n.getRightChild(),partition);
      }

    return updateLikelihood(n, partition);
  }
  

  void 
  FastCacheSubstitutionModel::updateLikelihood(const Node& n, 
					       const unsigned& partition)
  {
    if(n.isLeaf())
      {
	return leafLikelihood(n, partition);
      }
    else
      {
	// Get likelihood storage
	SubPatternLike& sl = likes[n][partition].second;
	// Get nested Likelihoods
	PatternLike& pl_l = likes[*n.getLeftChild()][partition];
	PatternLike& pl_r = likes[*n.getRightChild()][partition];

	// Compute Pr[Dk|T, ew, r(j)] for each siteRateCategory j
	for(unsigned j = 0; j < siteRates->nCat(); j++)
	  {
	    // Set up (j-specific) P matrix 
	    Q->resetP(edgeWeights->getWeight(n) * siteRates->getRate(j));
	    
	    // Lastly, loop over all unique patterns in hl
	    for(SubPatternLike::iterator si = sl.begin(); si != sl.end(); si++)
	      {
		unsigned i = si->first;
		// Now, this looks weird, but pl_l.first is the 
		// PatternTranslator, while pl_L.second holds the 
		// RateLike in its second element. 
		LA_Vector& left = pl_l.second[pl_l.first[i]].second[j];
		LA_Vector& right = pl_r.second[pl_r.first[i]].second[j];
		
		// left.*right, store result in left
		left.ele_mult(right, tmp);
		Q->mult(tmp, si->second[j]);
	      }
	  }
	return;
      }
  }
  
  void
  FastCacheSubstitutionModel::leafLikelihood(const Node& n, 
					     const unsigned& partition)
  {
    // Set up data and likelihood storage
    PatternVec& pv = partitions[partition];
    SubPatternLike& sl = likes[n][partition].second;

      //Loop over rate categories
      for(unsigned j = 0; j < siteRates->nCat(); j++)
	{
	  // initiate P 
	  Q->resetP(edgeWeights->getWeight(n) * siteRates->getRate(j));
	
	  // Lastly, loop over all unique patterns in hl
	  for(SubPatternLike::iterator si = sl.begin(); si != sl.end(); si++)
	    {
	      // Get position of pattern's first occurrence in partition
	      unsigned pos = pv[si->first].first; 
	      if(!Q->col_mult(si->second[j], (*D)(n.getName(), pos)))
		{
		  Q->mult(D->leafLike(n.getName(),pos), si->second[j]);
		}
	    }
	}

    return;
  }
  
    
}//end namespace beep
