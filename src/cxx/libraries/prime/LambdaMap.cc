#include "LambdaMap.hh"

#include "AnError.hh"
#include "Node.hh"
#include "StrStrMap.hh"
#include "Tree.hh"

#include <string>

// Author: Lars Arvestad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  using namespace std;

  // Constructor
  // The allocations are done by the superclass constructor.
  // The filling in of the map is done here.
  //--------------------------------------------------------------------------
  LambdaMap::LambdaMap(const Tree& G, const Tree& S, const StrStrMap &gs) 
    : NodeVector(G.getNumberOfNodes()),
      description()
  {
    if(G.getRootNode() != NULL)
      {
	try 
	  {
	    recursiveLambda(G.getRootNode(), S, gs);
	  }
	catch (AnError& err) 
	  {
	    err.action();
	  }
      }
    // We do not save G and S as attributes, so to make output nice
    // we make this description string
    ostringstream oss;
    oss << "LambdaMap between guest tree" << G.getName() 
	<< " and host tree " << S.getName();
    description = oss.str();
  }
 
  LambdaMap::LambdaMap(const LambdaMap& l)
    : NodeVector(l),
      description(l.description)
  {
  }

  LambdaMap&
  LambdaMap::operator=(const LambdaMap& l)
  {
    if(&l != this)
      {
	NodeVector::operator=(l);
	description = l.description;
      }
    return *this;
  }
    

  LambdaMap::~LambdaMap()
  {}

  LambdaMap::LambdaMap(const Tree& G, const Tree& S) 
      : NodeVector(G.getNumberOfNodes())
    {};


  void 
  LambdaMap::update(const Tree& G, const Tree& S, StrStrMap* gs)
  {
    if(gs)
      {
	recursiveLambda(G.getRootNode(), S, *gs);
      }
    else
      {
	recursiveLambda(G.getRootNode(), S);

      }
    return;
  }

  std::ostream& 
  operator<<(std::ostream &o, const LambdaMap& l)
  {
    return o << l.print();
  };
  
  std::string 
  LambdaMap::print() const
  {
    std::ostringstream oss;
    oss << description << ":\n";
    for(unsigned i = 0; i < pv.size(); i++)
      {
	oss << "\tLambda[" << i << "] = " << pv[i]->getNumber() << ";\n";
      }
    return oss.str();
  }

  //--------------------------------------------------------------------------
  // Computing the lambda map
  //
  // lambda is defined as follows
  // 1. If g \in leaves(G) then \lambda(g) = s \in leaves(S), in the natural 
  //    way.
  // 2. Otherwise, \lambda(g) = MRCA(lambda(left(g)), lambda(right(g))).
  //
  //--------------------------------------------------------------------------

  // Compute lambda for inner node g and return lambda(g).
  //--------------------------------------------------------------------------
  Node *
  LambdaMap::recursiveLambda(Node *g, const Tree& S, const StrStrMap &gs)
  {
    if (g->isLeaf())
      {
	return compLeafLambda(g, S, gs);
      }
    else
      {
	Node *ls = recursiveLambda(g->getLeftChild(), S, gs);
	Node *rs = recursiveLambda(g->getRightChild(), S, gs);
	Node *s = S.mostRecentCommonAncestor(ls, rs);
	pv[g->getNumber()] = s;
#ifdef SHOW_LAMBDA
	cerr << "Lambda[" << g->getNumber() << "] = " << s->getNumber() << endl;
#endif
	return s;
      }
  }

  // Compute lambda for inner node g and return lambda(g) using existing gs
  //--------------------------------------------------------------------------
  Node *
  LambdaMap::recursiveLambda(Node *g, const Tree& S)
  {
    if (g->isLeaf())
      {
	return pv[g->getNumber()];
      }
    else
      {
	Node *ls = recursiveLambda(g->getLeftChild(), S);
	Node *rs = recursiveLambda(g->getRightChild(), S);
	Node *s = S.mostRecentCommonAncestor(ls, rs);
	pv[g->getNumber()] = s;
#ifdef SHOW_LAMBDA
	cerr << "Lambda[" << g->getNumber() << "] = " << (*s).getNumber() << endl;
#endif
	return s;
      }
  }

  // Set up lambda map for leaf g. 
  // The lambda value is returned
  //--------------------------------------------------------------------------
  Node *
  LambdaMap::compLeafLambda(Node *g, const Tree& S, const StrStrMap& gs)
  {
    string genename = g->getName();
    const string sp_name = gs.find(genename);
    if (sp_name.empty()) 
      {
	throw AnError("Input inconsistency: "
		      "Leaf name missing in gene-to-species data.", 
		      genename, 1);
      }
    
    
    try 
      {
	Node *s = S.findLeaf(sp_name);
	pv[g->getNumber()] = s;
	return s;
      }
    catch (AnError& e) 
      {
	cerr << "An error occured when trying to map genes to species.\n"
	     << "Please verify that gene and species names are correct\n"
	     << "and complete!\n\n";
	e.action();
	return NULL;
      }
  }

  
}// end namespace beep
