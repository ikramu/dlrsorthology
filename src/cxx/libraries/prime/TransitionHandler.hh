#ifndef TRANSITIONHANDLER_HH
#define TRANSITIONHANDLER_HH

#include "LA_Vector.hh"
#include "SequenceData.hh"

namespace beep
{
  //------------------------------------------------------------------  
  //
  // Class TransitionHandler
  //! Base class for handling transition probabilities of a 
  //! Markov process. The transition rate matrix, Q, can be
  //! decomposed into a symmetric matrix R and the vector of 
  //! stationary frequencies, \f$ \pi\f$. The transition probability
  //! matrix, P, over a given (Markov) time interval, w, is given by 
  //! \f$ P=\exp(Qw)\f$. Note that w often is measured in the expected 
  //! number of occurring over the interval.
  //!
  //! Author: Bengt Sennblad, copyright the MCMC-club, SBC
  //   
  //------------------------------------------------------------------
  class TransitionHandler
  {
    //------------------------------------------------------------------  
    //
    // Construct/destruct/assign
    //   
    //------------------------------------------------------------------
  public:
    virtual ~TransitionHandler();
    
  protected:
    //! Constructor used by subclasses. 
    //! sequence_type must conform to the ones used by SequenceDATA/Type
    TransitionHandler(const std::string& model_name,
		      const SequenceType& sequence_type);
    
  public:
    //! Copy constructor used by subclasses. 
    TransitionHandler(const TransitionHandler& th);

    //! assignment operator used by subclasses. 
    TransitionHandler& operator=(const TransitionHandler& th);

    //------------------------------------------------------------------  
    //
    // Interface
    //   
    //------------------------------------------------------------------

    //------------------------------------------------------------------
    // Shared, non-virtual, functions
    //------------------------------------------------------------------
    // TODO: Evaluate which of these functions really are needed /bens

    //! Equality operator. Simple for now
    //-----------------------------------------------------------------
    bool operator==(const TransitionHandler& th) const;
    
    //! Tests if model and the submitted data are compatible
    //! This could be used as an early error-catcher
    //-----------------------------------------------------------------
    bool isCompatible(const SequenceData& sd) const;

    // Access (a copy) of the attributes
    //! Returns the name of the model
    //-----------------------------------------------------------------
    std::string getModel() const;

    //! Returns the (sequence) type that the model handles.
    //-----------------------------------------------------------------
    SequenceType getType() const;

    //! Returns the alphabet size of the Markov process.
    // TODO: Maybe rename to simply size() /bens
    //-----------------------------------------------------------------
    unsigned getAlphabetSize() const;

    //------------------------------------------------------------------
    // Virtual interface to be overloaded in subclasses
    //------------------------------------------------------------------
    // TODO: Access functions to pi and R? set/get

    //! Set up P, the transition probability matrix for the Markov
    //! process acting over 'time' MarkovTime. ('time' is not
    //! necessarily chronological.)
    //------------------------------------------------------------------
    virtual void resetP(const Real& MarkovTime) const = 0;

    //! (Matrix) multiplication \f$ <result>=P<operand>\f$
    //------------------------------------------------------------------
    virtual void mult(const LA_Vector& operand, 
		      LA_Vector& result) const = 0;

    //! (Matrix) multiplication 
    //! \f$ \langle result\rangle=P<operand>_{<column>}\f$
    //------------------------------------------------------------------
    virtual bool col_mult(LA_Vector& result, 
			  const unsigned& column) const = 0;

    //! Element-wise multiplication\f$ <result>=\pi <operand>\f$
    //------------------------------------------------------------------
    virtual void multWithPi(const LA_Vector& operand, 
			    LA_Vector& result) const =0;

    //
    //------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& os, 
				    const TransitionHandler& Q)
    {return os << Q.print();};
    virtual std::string print() const = 0;

    //------------------------------------------------------------------  
    //
    // Attributes
    //   
    //------------------------------------------------------------------
    // TODO: Can we avoid sequence_type? /bens
    std::string model_name;
    SequenceType sequence_type;//!< The sequence type that is handled, 
                               //!< e.g., DNA
    unsigned alphabet_size;    //!< Number of states in model alphabet
  };

} //end namespace beep

#endif
