#include "HybridTreeInputOutput.hh"

#include "AnError.hh"
#include "GammaMap.hh"
#include "HybridTree.hh"
#include "NHXannotation.h"
#include "Node.hh"
#include "StrStrMap.hh"


#include <cassert>		// For early bug detection

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <cmath>
#include <algorithm>


#include <libxml/parser.h>
#include <libxml/tree.h>


namespace beep
{
  using namespace std;

  //--------------------------------------------------------------------
  //
  // Constructors
  // 
  //--------------------------------------------------------------------

  // Only create TreeInputOutput objects through *named constructors* (see C++ FAQ 
  // lite). Actual constructor is protected, and does actually not do 
  // anything!

  HybridTreeInputOutput::HybridTreeInputOutput(const TreeInputOutput& io)
    : TreeInputOutput(io)
  {}

  // Update: Since I sometimes want to output trees without reading 
  // anything, I will now allow instantiating the empty object.
  //--------------------------------------------------------------------
  HybridTreeInputOutput::HybridTreeInputOutput()
    : TreeInputOutput()
  {}

  HybridTreeInputOutput::~HybridTreeInputOutput()
  {}

  HybridTreeInputOutput::HybridTreeInputOutput(const HybridTreeInputOutput& io)
    : TreeInputOutput(io)
  {}

  HybridTreeInputOutput&
  HybridTreeInputOutput::operator=(const HybridTreeInputOutput& io)
  {
    if(&io != this)
      {
	TreeInputOutput::operator=(io);
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
 
  // Named constructors! 
  // Usage: 
  //   TreeInputOutput io = fromFile("Nisse");
  //--------------------------------------------------------------------
//   HybridTreeInputOutput
//   HybridTreeInputOutput::fromFile(const string &f)
//   {
//     return TreeInputOutput::fromFile(f);
//   }


//   HybridTreeInputOutput
//   HybridTreeInputOutput::fromString(const string &s)
//   {
//     return TreeInputOutput::fromString(s);
//   }


  //----------------------------------------------------------------------
  // Reading trees:
  //----------------------------------------------------------------------

 
  HybridTree
  HybridTreeInputOutput::readHybridTree()
  {
    TreeIOTraits traits;
    checkTagsForTrees(traits);	
    if(traits.containsTimeInformation() == false)
      {
	throw AnError("Host tree lacks time information for some of it nodes", 1);
      }

    return readHybridTree(traits, 0, 0);
  }



  vector<HybridTree>
  HybridTreeInputOutput::readAllHybridTrees(TreeIOTraits& traits,
				    std::vector<SetOfNodes> *AC, 
				    std::vector<StrStrMap>* gsV)
  {
    assert(xmlroot);

    // todo check.   assert(AC == 0 && gsV == 0); Should this be? /Erik Sjolund 2010-01-07
    assert(AC == 0 && gsV == 0); // disable uncertain functionality
    vector<HybridTree> GV;	       
    traits.setHY(true);

    for (xmlNodePtr cur = xmlroot; cur; cur = cur->next) {
      if (cur->type == XML_ELEMENT_NODE  &&     xmlStrEqual( cur->name, BAD_CAST "tree" ) ) {
	StrStrMap gsi;
	HybridTree tree;
        readBeepTree(cur,traits, AC, &gsi,tree,   tree.getOPAttribute(), tree.getEXAttribute());
	GV.push_back(tree);
        if(gsV) { 
           gsV->push_back(gsi);
	}
      }
    }
    reverse(GV.begin(), GV.end());
    return GV;
  }


  HybridTree
  HybridTreeInputOutput::readHybridTree(TreeIOTraits& traits,
				    std::vector<SetOfNodes> *AC, 
					 StrStrMap *gs)
 {
    assert(xmlroot);
    // todo check.   assert(AC == 0 && gs == 0); Should this be? /Erik Sjolund 2010-01-07
    assert(AC == 0 && gs == 0); // disable uncertain functionality
    traits.setHY(true);

    for (xmlNodePtr cur = xmlroot; cur; cur = cur->next) {
      if (cur->type == XML_ELEMENT_NODE  &&     xmlStrEqual( cur->name, BAD_CAST "tree" ) ) {
	HybridTree tree;
        readBeepTree(cur,traits, AC, gs,tree,   tree.getOPAttribute(), tree.getEXAttribute());
        return tree;
      }
    }
    std::cerr << "no tree found" << std::endl;
    abort(); // was exit(EXIT_FAILURE);
  }

  // Convenience front to readAllBeepTrees(...)
  // Reads 'NW tags' as edge times and nothing more
  //----------------------------------------------------------------------
  vector<HybridTree>
  HybridTreeInputOutput::readAllHybridTrees(std::vector<StrStrMap>* gs)
  {
    TreeIOTraits traits;

    // todo check.   Why is
    //   traits.setNT(true);
    // set here and not in for instance 
    //  HybridTree
    //  HybridTreeInputOutput::readHybridTree(TreeIOTraits& traits,
    //			    std::vector<SetOfNodes> *AC, 
    //				 StrStrMap *gs)
    // ? /Erik Sjolund 2010-01-07
    traits.setNT(true);
    return readAllHybridTrees(traits, 0, gs);
  }

  //----------------------------------------------------------------------
  // Writing trees
  //----------------------------------------------------------------------

  string 
  HybridTreeInputOutput::writeHybridTree(const HybridTree& T, 
			  const TreeIOTraits traits,
			  const GammaMap* gamma)
  {
    TreeIOTraits localtraits(traits);
    localtraits.setID(false);
    ostringstream name;

    if (localtraits.hasName()) {
      name << "[&&PRIME NAME=" << T.getName();
      if(T.getRootNode() == NULL)
	{
	  name << "] [empty tree]";
	  return name.str();
	}
      else 
	{
	  if(localtraits.hasNT())
	    {
	      name << " TT=" << T.getTopTime();
	    }
	  name << "]";
	}
    }
    map<unsigned, unsigned> id;
//     string least = "";
    map<Node*, string> least;
    return recursivelyWriteBeepTree(*T.getRootNode(), least, localtraits,
				    gamma, T.getOPAttribute(), 
				    T.getEXAttribute(), &id) + name.str();
  }

  // convenience front function for writeHybridTree(...) 
  // writes tree T with edge times
  //----------------------------------------------------------------------
  string 
  HybridTreeInputOutput::writeHybridTree(const HybridTree& S)
  {
    TreeIOTraits traits;
    traits.setID(true);
    traits.setET(true);
    return writeHybridTree(S, traits, 0);
  }


}//end namespace beep
