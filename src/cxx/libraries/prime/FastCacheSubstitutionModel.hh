#ifndef FASTCACHESUBSTITUTIONMODEL_HH
#define FASTCACHESUBSTITUTIONMODEL_HH

#include "BeepVector.hh"
#include "SubstitutionModel.hh"

// #include <map>
#include <ext/hash_map>
#include <string>
#include <utility>
#include <vector>

namespace __gnu_cxx
{
        template<> struct hash< std::string >
        {
                size_t operator()( const std::string& x ) const
                {
                        return hash< const char* >()( x.c_str() );
                }
        };
}

namespace beep
{
  //----------------------------------------------------------------------
  //
  // Class FastCacheSubstitutionModel - subclass of ProbabilityModel
  // 
  //! This class implements the standard (probabilistic) Markov model for 
  //! the substitution process, see eg, Felsenstein 1981, for any type 
  //! of aligned sequence data.
  //! Data are divided into user-defined partitions (domains, codon
  //! positions, etc.), Within each partition sequence positions with 
  //! identical patterns of states over leaves are identified and counted.
  //! Rate variation across sites over discrete classes, e.g., Yang 1993
  //! are modelled
  //!
  //! Author: Bengt Sennblad  copyright: the mcmc-club SBC
  //
  //----------------------------------------------------------------------

  class FastCacheSubstitutionModel: public SubstitutionModel
  {
  public:

    //    using __gnu_cxx::hash_map;
    // Structure to save likelihoods in
    //----------------------------------------------------------------------
    //! Stores pointers into a HiddenPatternLike, which stores the 
    //! subtree likelihoodstree probabilities, for each rate class. 
    //! Each element pointed to by  RateLike corresponds to a column-specific
    //! rate hypotheses, r (cf. SiteRateHandler).
    //! Each elements of a LA_Vector corresponds to a state, i, in the 
    //! Markov substitution model, and stores the probability of the
    //! Markov process starting with i at the root of a (sub)tree of 
    //! interest and given r, produces the state pattern
    //! corresponding of that position at the leaves of the subtree. 
    
    //! The actual storage of subtree likelihoods are stored in 
    //! La_vectors in a HiddenPatternLike, one for each rate class
    typedef std::vector<LA_Vector>                       RateLike;

    //! Each subtree patternLike has a separate RateLike and an associate 
    //! unsigned keeping track of (one of) the corresponding (tree)Patterns
    typedef std::vector<std::pair<unsigned, RateLike> >  SubPatternLike;

    //! We also need a translator from (tree)PatternLikes to subPatternLikes
    typedef std::vector<unsigned>                        PatternTranslator;

    //! Join these two, an we have all info we need for calculating
    //! the likelihood of a collection of (tree)Pattern for a subtree/node.
    //!  Each element of a PatternLike should correspond to an element 
    //! of a SequenceData::PatternVec
    typedef std::pair<PatternTranslator, SubPatternLike> PatternLike;

    //! Finally, we need a PatternLike for each partition.
    //! Each element of a PartitionLike should correspond to an element 
    //! of a SequenceData::PartitionVec
    typedef std::vector<PatternLike> PartitionLike;

    //--------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------------
    //! Sets up the attributes of the class. 
    //! Note that partitionsList should be an existing user-defined 
    //! partition (use 'all' as a default value).
    //-------------------------------------------------------------------
    FastCacheSubstitutionModel(const SequenceData& Data, 
		      const Tree& T, 
		      SiteRateHandler& siteRates, 
		      const TransitionHandler& Q,
		      EdgeWeightHandler& edgeWeights,
		      const std::vector<std::string>& partitionsList);
  
    //! Copy Constructor
    //-------------------------------------------------------------------
    FastCacheSubstitutionModel(const FastCacheSubstitutionModel& sm);
  
    //! Destructor
    //-------------------------------------------------------------------
    virtual ~FastCacheSubstitutionModel();

    //! Assignment operator
    //-------------------------------------------------------------------
    FastCacheSubstitutionModel& operator=(const FastCacheSubstitutionModel& sm);
  
    //-------------------------------------------------------------------
    //
    // Public interface
    //
    //-------------------------------------------------------------------

    // Access to Tree T. Note that a const reference is returned. The 
    // logic is that non-const access to T should be explicit in main()
    //-------------------------------------------------------------------
//     const Tree& getTree();
  
    // Likelihood calculation - interface to MCMCModels
    //! Calculates the probability of the data given the tree.
    //! \f$ Pr[D|T, w, Q, \alpha, ncat]\f$, where \f$ w\f$ is the edgeweights 
    //! of T and \alpha is the parameter of SiteRateHandler and ncat is the 
    //! number of rate classes in SiteRateHandler. If D is partitioned, 
    //! different parameter values may be used for different partitions.
    // This is calculated recursively.
    // TODO: We probably need to add a calculateDataProbability(Node& n),
    // for the sake of a common interface.  This is used in
    // cachedSubstitutionModel, but here would just relay to
    //calculateDataProbability().
    //------------------------------------------------------------------
    Probability calculateDataProbability();
    Probability calculateDataProbability(Node* n);

//     // Pass on update to edgeWeightHandler, etc?
//     void update();

    //------------------------------------------------------------------
    //
    // I/O
    // 
    //------------------------------------------------------------------
//     friend std::ostream& operator<<(std::ostream &o, 
// 				    const FastCacheSubstitutionModel& pm); 
//     std::string print() const;
    std::string print(bool estimateRates = false) const;
//   private:
//     // TODO: Is this really needed /bens
//     std::string print(const bool& estimateRates) const;

  private:
    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------

    // Helper function for Likelihood calculation
    //-------------------------------------------------------------------
    void init(); //!< Initiates all data structures for storing likelihood.

    Probability rootLikelihood(const unsigned& partition);
    std::vector<const Node*> initLikelihood(const Node& n, const unsigned& partition);
    void recursiveLikelihood(const Node& n, const unsigned& partition);
    void updateLikelihood(const Node& n, const unsigned& partition);
    void leafLikelihood(const Node& n, const unsigned& partition);




  protected:
    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
    //! Each ultimate element of likes is a RateVec pointer corresponding
    //! to a pattern in 'partitions' (the sorted data matrix) and a specific
    //! vertex in the tree. The pointers points in a surjective manner into
    //! hiddenLikes, where the actual RateLikes are saved. This means that
    //! if several patterns has the same subpattern for a subtree, the
    //! hiddenLike for this subpPattern only has to be caluculated once.
    //! likes provides an interface to hiddenLikes for the parental vertex.  
    BeepVector<PartitionLike> likes;

    LA_Vector tmp;
  };
}//end namespace beep
#endif


