#ifndef TREEANALYSIS_HH
#define TREEANALYSIS_HH

#include "BeepVector.hh"
#include "Node.hh"
#include "NodeMap.hh"
#include "Tree.hh"

namespace beep
{

  class LambdaMap;		// Forward
  class GammaMap;		// Forward

  //----------------------------------------------------------------
  //
  //! This class contains methods for extracting data from trees.
  //! Author: LArs Arvestad, � the MCMC-club, SBC, all rights reserved
  // 
  //----------------------------------------------------------------
  class TreeAnalysis 
  {
  public:
    //----------------------------------------------------------------
    //
    // Constructor
    // 
    //----------------------------------------------------------------
    TreeAnalysis(Tree &T);

    //----------------------------------------------------------------
    //
    // Interface
    // 
    //----------------------------------------------------------------

    //! Count the number of leaves for every subtree. The vector is indexed
    //! by the id of the root of each subtree.
    NodeMap<unsigned> subtreeSize();

    // Compute lower bounds on slices for various circumstances
    //
    //  NodeNodeMap<unsigned> sliceLowerBounds(Tree &S, GammaMap &gamma_star);

    //! Return a boolean vector telling you whether the node's subtrees are
    //! isomorphic or not. The vector is indexed by the node id. Note that
    //! no consideration is taken regarding reconciliations.
    //! \todo{All functions witih LambdaMap arguments can probably be 
    //! improved a bit, compare with corresponding functions with 
    //! GammaMap arguments /bens}
    NodeMap<bool> isomorphicSubTrees(LambdaMap &l);

    //! Return a Boolean vector iso, such that, for v,w shildren of u, 
    //! iso[u]=true if  $(G_v,\gamma_v)\cong(G_w,\gamma_w)$.
    NodeMap<bool> isomorphicSubTrees(GammaMap &gamma);


  protected:
    //----------------------------------------------------------------
    //
    // Implementation
    // 
    //----------------------------------------------------------------
    
    //! Supports subtreeSize above.
    unsigned recursiveSubtreeSize(NodeMap<unsigned> &sizes, Node *v);


    //! Front-end to recursiveIsomorphicTrees, see isomorphicSubTrees.
    void computeIsomorphicTrees(NodeMap<bool> &iso, LambdaMap &l, Node *r);

    //! Fill NodeMap<bool> iso with the result of
    //! recursiveIsomorphicTrees(beep::GammaMap &gamma,Node *v1,Node *v2)
    void computeIsomorphicTrees(NodeMap<bool>& iso, GammaMap& gamma, Node& r);

    //! Supports isomorphicSubTrees above. Is similar to DupSpecModel::isEqual.
    //! Return true if the subtrees rooted at v1 and v2 are isomorphic, 
    //! otherwise return false.
    bool recursiveIsomorphicTrees(LambdaMap &l, Node *v1, Node *v2);

    //! Isomorphy of reconciled trees: return true if 
    //! $(G_{v1}, \gamma_{v1})\cong{G_{v2},\gamma_{v2})$.
    bool recursiveIsomorphicTrees(GammaMap& gamma, Node& v1, Node& v2);



  private:
    //----------------------------------------------------------------
    //
    // Attributes
    // 
    //----------------------------------------------------------------
    Tree &T; //!< The tree to analyze

  };

}//end namespace beep
#endif
