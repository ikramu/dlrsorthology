#include <iostream>
#include <string>
#include <vector>

#include "DPLengthFactorizer.hh"

namespace beep
{
  using namespace std;

  //----------------------------------------------------------------------
  //
  // Constructor/Destructor
  //
  //----------------------------------------------------------------------

  DPLengthFactorizer::DPLengthFactorizer(MCMCModel& prior, 
					 Tree& G, Tree& S, 
					 ReconciliationTimeMCMC& rtm,
					 EdgeRateMCMC& erm,
					 Density2P& df,
					 Real& birthRate, Real& deathRate,
					 unsigned noOfDiscrIntervals, 
					 double DPratio) 
    : StdMCMCModel(prior, erm.nParams(), "Dplf"),
      ProbabilityModel(),
      G(&G),
      S(&S),
      rtm(&rtm),
      erm(&erm),
      noOfDiscrIntervals(noOfDiscrIntervals),
      DPratio(DPratio),
      optimizer(G, S, &df, birthRate, deathRate, noOfDiscrIntervals),
      doingLF(false),
      oldG(new Tree(G)),
      oldRates(G.getNumberOfNodes(),0.0),
      oldNodeTimes(G.getNumberOfNodes(),0.0)
  {   
  }

  // Copy constuctor
  //----------------------------------------------------------------------
  DPLengthFactorizer::DPLengthFactorizer(const DPLengthFactorizer &dplf) 
    :  StdMCMCModel(dplf),
       ProbabilityModel(dplf),
       G(dplf.G),
       S(dplf.S),
       rtm(dplf.rtm),
       erm(dplf.erm),
       noOfDiscrIntervals(dplf.noOfDiscrIntervals),
       DPratio(dplf.DPratio),
       optimizer(dplf.optimizer),
       doingLF(dplf.doingLF),
       oldG(dplf.oldG),
       oldRates(dplf.oldRates),
       oldNodeTimes(dplf.oldNodeTimes)
  {
  }

  // Death and destruction
  //----------------------------------------------------------------------
  DPLengthFactorizer::~DPLengthFactorizer()
  {
  }

  //-------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------
  //-----------------------------------------------------------------------
  // StdMCMCModel interface
  //-----------------------------------------------------------------------

  MCMCObject
  DPLengthFactorizer::suggestOwnState()
  {
    PRNG rand;
    Real p = rand.genrand_real1();
    if (p < DPratio)
      {
 	cout << "Factorize (i.e. suggestDP)!\n";
 	cerr << "Factorize (i.e. suggestDP)!\n";
	doingLF = true;
  	MCMCObject MOb;

// 	*oldG = *G;
    	vector<double> lengths;
 	for(unsigned nIndex = 0; nIndex <= (G->getNumberOfNodes()-1); nIndex++) 
 	  {
 	    Node* n = G->getNode(nIndex);
	    Real nSubstRate = 0.0;
	    Real nNodeTime = 0.0;
	    if (!n->isRoot())
	      {
		//nSubstRate = erm->getRate(n);
		nSubstRate = G->getRate(*n);
		oldRates[nIndex] = nSubstRate;
		nNodeTime = G->getTime(*n);
		oldNodeTimes[nIndex] = nNodeTime;
	      }
	    Real nTime = G->getEdgeTime(*n);
	    Real nLength = nSubstRate * nTime;
 	    cout << "nIndex: " << nIndex << " nSubstRate: " << nSubstRate << " nTime: " << nTime << " nLength: " << nSubstRate * nTime << "\n";
 	    lengths.push_back(nLength);
 	  }

 	Probability factorizationLike = optimizer.DP(G->getRootNode()->getNumber(),true,false,lengths);

	optimizer.backTrace((G->getRootNode()->getNumber()),noOfDiscrIntervals);
 	for(unsigned nIndex = 0; nIndex <= (G->getNumberOfNodes()-1); nIndex++) 
 	  {
 	    Node* n = G->getNode(nIndex);
	    if (!n->isRoot())
	      {
		if (n->getParent()->isRoot())
		  {
		    Node* nSibl = n->getSibling();		    
		    unsigned nSiblIndex = nSibl->getNumber();		    
		    //erm->setRate(((lengths.at(nIndex) + lengths.at(nSiblIndex))/(n->getTime() + nSibl->getTime())),n); 
		    G->setRate(*n,((lengths.at(nIndex) + lengths.at(nSiblIndex))/(G->getEdgeTime(*n) + G->getEdgeTime(*nSibl)))); 
		  }
		else
		  {
		    //erm->setRate((lengths.at(nIndex)/n->getTime()),n);
		    G->setRate(*n,(lengths.at(nIndex)/G->getEdgeTime(*n))); 
		  }
 		Real nSubstRate = G->getRate(*n);
 		Real nTime = G->getEdgeTime(*n);
 		cout << "nIndex: " << nIndex << " nSubstRate: " << nSubstRate << " nTime: " << nTime << " nLength: " << nSubstRate * nTime << "\n";
	      }
	  }
	G->perturbedNode(G->getRootNode());
	MOb.stateProb = factorizationLike;

	cout << "DP_prob: " << factorizationLike << "\n";
	return MOb;
      }
    else
      {
        doingLF = false;
	return erm->suggestNewState(); 
      }
  }

  void
  DPLengthFactorizer::commitOwnState()
  {
    if (doingLF)
      {
      }
    else
      {
	erm->commitNewState();
      }
  }

  void
  DPLengthFactorizer::discardOwnState()
  {
    if (doingLF)
      {
 	cout << "discard DP !\n";
 	cerr << "discard DP !\n";
// 	*G = *oldG;
	for(unsigned nIndex = 0; nIndex <= (G->getNumberOfNodes()-1); ++nIndex) 
	  {
	    Node* n = G->getNode(nIndex);
	    if (!n->isRoot())
	      {
		//erm->setRate(oldRates.at(nIndex),n);
		G->setRate(*n,oldRates.at(nIndex));
		if (!n->isLeaf())
		  {
		    G->setTime(*n,oldNodeTimes.at(nIndex));
		  }
	      }
	  }

// 	G->updateAllEdgeTimes();
	G->perturbedNode(G->getRootNode());
//   	cerr << "end discard DP !\n";
     }
    else
      {
	erm->discardNewState();
      }
    return;
  }    

  std::string
  DPLengthFactorizer::ownStrRep() const
  {
    return erm->strRepresentation();
  }

  std::string
  DPLengthFactorizer::ownHeader() const
  {
    return erm->strHeader();
  }

  Probability
  DPLengthFactorizer::updateDataProbability()
  {
    update(); 
//     return erm->updateDataProbability();
    return erm->initStateProb();
  }

  Probability
  DPLengthFactorizer::calculateDataProbability()
  {
    return 1.0;
  }

  void
  DPLengthFactorizer::update()
  {
    erm->update();
  }


  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const DPLengthFactorizer& A)
  {
    return o << A.print(); 
  }

  string 
  DPLengthFactorizer::print() const
  {
    ostringstream oss;
    oss << erm->print();
    return oss.str();
  }
    
} //end namespace beep
