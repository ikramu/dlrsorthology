#ifndef DP_ML_HH
#define DP_ML_HH

#include <algorithm>
#include <vector>

#include "BirthDeathProbs.hh"
#include "Density2P.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "Tree.hh"
#include "GenericMatrix.hh"

namespace beep
{
  class DP_ML
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //-------------------------------------------------------------  
    DP_ML(Tree& G, Tree& S, Density2P* df, Real& birthRate, Real& deathRate, const unsigned noOfDiscrIntervals);
    ~DP_ML();
  
    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------

    void setValues(unsigned i, unsigned j, Probability p, unsigned l_val, unsigned r_val);
    void setValue(unsigned i, unsigned j, Probability p);
    Probability getValue(unsigned i, unsigned j);
    unsigned getLeftPointer(unsigned i, unsigned j);
    unsigned getRightPointer(unsigned i, unsigned j);
    void backTrace(unsigned u_index, unsigned u_time_iter);
    Probability oneValue(unsigned u_index, std::vector<double> rates, std::vector<double> nodeTimes);
    Probability DP(unsigned u_index, bool optimize, bool integral); 
    Probability DP(unsigned u_index, bool optimize,bool integral, std::vector<double> lengths);
    Probability DPsampleTreeFromDistr(unsigned u_index);

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------

  private:
    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
    unsigned noOfNodes;
    unsigned noOfDiscrIntervals;
    Tree& G;
    Tree& S;
    GenericMatrix<Probability> M;
    GenericMatrix<unsigned> M_left;
    GenericMatrix<unsigned> M_right;
    Density2P* df;
    BirthDeathProbs bdp;
  };
}//end namespace beep
#endif
