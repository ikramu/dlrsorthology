#ifndef RECONCILEDTREETIMEMODEL_HH
#define RECONCILEDTREETIMEMODEL_HH

#include "ReconciliationModel.hh"
#include "BirthDeathProbs.hh"
#include "Tree.hh"
namespace beep
{

  class ReconciledTreeTimeModel : public ReconciliationModel
  {
  public:
    //------------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //------------------------------------------------------------
    ReconciledTreeTimeModel(Tree& G, StrStrMap& gs, 
			    BirthDeathProbs& bdp_in);
    ~ReconciledTreeTimeModel();
    ReconciledTreeTimeModel(const ReconciledTreeTimeModel& rtt);
    ReconciledTreeTimeModel& operator=(const ReconciledTreeTimeModel& rtt);

    //------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------
    void update();
    Probability calculateDataProbability();

    //------------------------------------------------------------
    // I/O
    //------------------------------------------------------------
    std::string print() const;

  protected:
    //------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------

    // computes \f$ r_V(x,u) \f$
    // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
    //-----------------------------------------------------------------
    Probability computeRV(Node& x, Node& u);

    // computes \f$ r_A(x,u) \f$
    //-----------------------------------------------------------------
    Probability computeRA(Node& x, Node& u);
	      
    // computes \f$ r_X(x,u,k), k\in[|L(G_u)|] \f$
    // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
    //-----------------------------------------------------------------
    Probability computeRX(Node& x, Node& u);

  };
} // end namespace beep
#endif
