#include <cassert>
#include <cmath>
#include <sstream>

#include "BirthDeathInHybridProbs.hh"
#include "AnError.hh"
#include "HybridTree.hh"

using namespace std;

namespace beep
{
  class Tree;

  //---------------------------------------------------------------
  // 
  // Construct/Destruct
  //
  //---------------------------------------------------------------
  BirthDeathInHybridProbs::BirthDeathInHybridProbs(HybridTree &S, 
				   const Real& birth_rate,
				   const Real& death_rate,
				   Real* top_time)
    : BirthDeathProbs(S.getBinaryTree(), birth_rate, death_rate, top_time),
      H(&S)
  {
    if(*topTime == 0)
      {
	H->setTopTime(1.0);
      }
    // If badly chosen rates, then protest!
    if (birth_rate <= 0.0)
      {
	throw AnError("Cannot have birth rate <= 0.0!");
      }
    if (death_rate <= 0.0)
      {
	throw AnError("Cannot have death rate <= 0.0!");
      }
    update();
  }

  BirthDeathInHybridProbs::BirthDeathInHybridProbs(const BirthDeathInHybridProbs &BDP)
    : BirthDeathProbs(BDP),
      H(BDP.H)
  {
  }

  //virtual 
  //---------------------------------------------------------------
  BirthDeathInHybridProbs::~BirthDeathInHybridProbs()
  {
  }


  // Assignment
  //---------------------------------------------------------------
  BirthDeathInHybridProbs&
  BirthDeathInHybridProbs::operator=(const BirthDeathInHybridProbs &BDP)
  {
    if (this != &BDP)
      {
	BirthDeathProbs::operator=(BDP);
	H = BDP.H;
      }

    return *this;
  }



  //---------------------------------------------------------------------
  //
  // Interface
  //
  //---------------------------------------------------------------------
  HybridTree& 
  BirthDeathInHybridProbs::getStree()
  {
    return *H;
  }


  // Force an update of precomputed values
  //---------------------------------------------------------------------
  void
  BirthDeathInHybridProbs::update()
  {
    if(BD_const.size() != H->getNumberOfNodes())
      {
	BD_const = ProbVector(H->getNumberOfNodes());
	BD_var = ProbVector(H->getNumberOfNodes());
	BD_zero = ProbVector(H->getNumberOfNodes()); 
	generalBirthRate = RealVector(H->getNumberOfNodes());
	generalDeathRate = RealVector(H->getNumberOfNodes());
      }
    calcBirthDeathInHybridProbs(*H->getRootNode());
  }


  // The conditional (partial)probability of n gene copies over an arc (x,y)
  // Remember kids: This is not a probability, since we are cancelling a 
  // factor $(1-D)^{c+1}$ also occuring elsewhere! That is why it has *Partial* 
  // in its name. 
  //---------------------------------------------------------------------
  Probability
  BirthDeathInHybridProbs::partialProbOfCopies(const Node &y, unsigned n_kids) const
  {
    if (n_kids == 0) 
      {
	assert(BD_zero[y] > 0.0);
	return BD_zero[y];
      }
    else 
      {
	assert(BD_const[y] > 0.0);
	return BD_const[y] * pow(BD_var[y], (int)n_kids - 1);
      }
  }


  // We don't treat the top slice differently here, but might be doing so 
  // in derived classes.
  // Remember kids: This is not a probability, since we are cancelling a 
  // factor $(1-D)^{c+1}$ also occuring elsewhere! That is why it has *Partial* 
  // in its name. 
  //******TO BEOVERLOADED IN INTEGRALDUPSPECPROBS*****
  //---------------------------------------------------------------------
  Probability
  BirthDeathInHybridProbs::topPartialProbOfCopies(unsigned n_kids) const
  {
    return partialProbOfCopies(*H->getRootNode(), n_kids); 
  }


  //---------------------------------------------------------------------
  // I/O
  //---------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, 
	     const BirthDeathInHybridProbs& tsm)
  {
    return o << "BirthDeathInHybridProbs.\n"
	     << "A class for handling parameters and probabilities\n"
	     << "relating to the birth-death model used in reconciliations.\n"
	     << "Also handles sampling from probability distributions\n"
      //Add indentation here, e.g., <<indent = original_indent + 3
	     << tsm.print();
  };


  // print function for parameters used in operator<<
  //--------------------------------------------------------------------
  std::string 
  BirthDeathInHybridProbs::print() const
  {
    //I must update this! /bens
    std::ostringstream os;
    os     << "Parameters:\n"
	   << H->getName()
	   << " (species tree):\n"
      //here should be added a S.tree4os statement when this becomes available
	   << "\n"
	   << "birth_rate, death rate, db_diff (their negative difference):\n"
	   << birth_rate << "\t" << death_rate << "\t" << db_diff << "\n"
	   << "\n"
	   << "BD_zero, BD_const, BD_var, generalBirthRate, generalDeathRate\n"
	   << "are  variables derived from birth_rate and death_rate specific\n"
	   << "to vertices in the host tree, and are used in the probability\n"
	   << " calculations.\n"
	   << "\n"
	   << "\n";
      
    return os.str();
  }



  //---------------------------------------------------------------------
  //
  // Implementation
  //
  //---------------------------------------------------------------------

  // calcPt_Ut()
  // Used for the calculation the Pt-value and Ut-value of a child Node.
  // Ut is known as u_t in the paper, and Pt is P(t) in the paper.
  //---------------------------------------------------------------------
  void
  BirthDeathInHybridProbs::calcPt_Ut(const Real t, Probability & Pt, Probability & u_t) const
  {
    assert(t >= 0);
    assert(death_rate >= 0);
    assert(birth_rate > 0);
    if (death_rate == birth_rate)
      {
	Probability denominator(1.0 + (death_rate * t)); // I hope birth_rate * t is not too small... - it shouldn't matter as long as it's positive
	Pt = Probability(1.0) / denominator;
	u_t = Probability(death_rate * t) / denominator;
      }
    else if(death_rate == 0)
      {
	Pt = 1.0; 
	u_t = 1.0 - exp(-birth_rate * t);
	assert(u_t != 1.0);
      }
    else
      {
	Probability E = std::exp(db_diff * t);
	Probability denominator = birth_rate - (death_rate * E);
	Pt = -db_diff / denominator; 

	Probability u_t_numerator = birth_rate * (1.0 - E);
	u_t = u_t_numerator / denominator; 
	assert(u_t != 1.0);
      }
    assert(Pt > 0.0);
//     assert(u_t > 0.0); //!\todo{Allowing zero length edges -- problem?}
  }




  // calcBirthDeathInHybridProbs()
  //******TO BEOVERLOADED IN INTEGRALDUPSPECPROBS*****
  //----------------------------------------------------------------------
  void
  BirthDeathInHybridProbs::calcBirthDeathInHybridProbs(Node &root)
  {
    assert(*topTime > 0.0);
    calcBirthDeathInHybridProbs_recursive(root);
  }

  // calcBirthDeathInHybridProbs_recursive()
  // This function is called for nodes below the root.
  //----------------------------------------------------------------------
  void 
  BirthDeathInHybridProbs::calcBirthDeathInHybridProbs_recursive(Node &y)
  {
    Probability Pt; 
    Probability Ut;
    Real t = 0;
    if(y.isRoot())
      {
	t = *topTime;
      }
    else
      {
	t = y.getTime();
      }
    calcPt_Ut(t, Pt, Ut);

    assert(Pt > 0.0);
//     assert(Ut > 0.0); //!\todo{Allowing zero edge times -- OK?}
    assert(Ut != 1.0);

    // This isessentially a ghost that won't reach present time, 
    // thus nothing will survive
    if(H->isExtinct(y))
      {
	BD_const[y] = 0;
	BD_var[y]   = 0;
	BD_zero[y]  = 1;

	// General birth and death rates used when sampling edge times
	// These should never be used so I set them to negative and hopes it 
	// creates an error if they are used
	generalBirthRate[y] = -birth_rate;
	generalDeathRate[y] = -death_rate;

      }
    else if(y.isLeaf() == true)
      {
	BD_const[y] = Pt * (1.0 - Ut);
	BD_var[y]   = Ut;
	BD_zero[y]  = 1.0 - Pt; // Simple expression for leaves

	// General birth and death rates used when sampling edge times
	generalBirthRate[y] = birth_rate;
	generalDeathRate[y] = death_rate;

      }
    else 
      {
	Node &left = *(y.getLeftChild());
	Node &right = *(y.getRightChild());
      
	// Probability of extinction below y is prob of extinction along both 
	// child lineages, given by BD_zeroL and BD_zeroR:
	calcBirthDeathInHybridProbs_recursive(left);
	calcBirthDeathInHybridProbs_recursive(right);
      
	Probability D = BD_zero[left] * BD_zero[right];
	Probability tmp = 1.0 - (Ut * D);
      
	// Compute the probability of extinction over arc (x, y):
	BD_zero[y] = 1.0 - (Pt * (1.0 - D) / tmp);
      
	// The following prepares for computing e_A(x, y, u). 
	//See also Node.hh!
	BD_const[y] = Pt * (1.0 - Ut) / (tmp * tmp);
	BD_var[y]   = Ut / tmp;
      
	// General birth and death rates used when sampling edge times
	generalBirthRate[y] = birth_rate *(1 - D.val());
	generalDeathRate[y] = death_rate - birth_rate * D.val();
      }
  }






}//end namespace beep
