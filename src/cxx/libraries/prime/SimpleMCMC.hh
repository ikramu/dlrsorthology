#ifndef SIMPLEMCMC_HH
#define SIMPLEMCMC_HH

#include <fstream>

#include "MCMCModel.hh"
#include "PRNG.hh"
#include "Probability.hh"

//-------------------------------------------------------------
//
// A very simple version of the Metropolis algorithm.
//
// The model supports thinning, but not by default. If you want to
// output your state every 100 iterations, then you have to
// instantiate your MCMC object by something like 
//    SimpleMCMC metropolis(M, R, 100);
//! Author: Lars Arvestad, SBC, � the MCMC-club, SBC, all rights reserved
//
//-------------------------------------------------------------

namespace beep
{
  class SimpleMCMC
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //-------------------------------------------------------------
    SimpleMCMC(MCMCModel &M, unsigned thinning = 1);
    virtual ~SimpleMCMC();

    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    // Run one or several iterations of Metropolis
    //-------------------------------------------------------------
    virtual void iterate(unsigned n_iters=1, unsigned print_factor = 1);

    // Advance the mcmc one or several iterations.
    // This function is "silent" i.e. no diagnostics
    // is printed on any file (stdout, stderr etc.).
    //
    // n_iters is the number of iterations to perform.
    //
    // Author: peter9 and fmattias (SBC, 'junior MCMC-club')
    virtual void advance(unsigned n_iters=1);

    // Settings
    //-------------------------------------------------------------
    //! Deprecated -- Use const version below
//     virtual void setOutputFile(char *filename)
//     {
//       return setOutputFile(filename);
//     };//, char *header=NULL);
    virtual void setOutputFile(const char *filename);//, char *header=NULL);
    virtual void setThinning(unsigned i);

    // Output likelihood info on cerr or not 
    // Old setting returned.
    //-------------------------------------------------------------
    virtual bool setShowDiagnostics(bool yes_no);	

    virtual std::string estimateTimeLeft(unsigned iteration, unsigned when_done);

    Probability getLocalOptimum();
    std::string getBestState();

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const SimpleMCMC& A);
    virtual std::string print() const;

    void setFirstIterate(bool);
    void setLastIterate(bool);

  protected:
    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
    MCMCModel &model;
    PRNG &R;		       // Source of randomness
    unsigned iteration;	       // Output current state everytime this 
                               //counter is zero.
    unsigned thinning;	       // iteration is incremented modulo thinning 
                               //(i.e., iteration < thinning)
    Probability p;	       // Probability of the model's current state
    std::ofstream os;	       // Stream to write output to.
    std::streambuf *cout_buf;  // Save old cout buffer here if we write to file
    bool localOptimumFound;    // Best likelihood value found in this iteration
    bool show_diagnostics;     // ...likelihood to cerr or not
    unsigned start_time;	// Keep track of how long we've beeen computing
    Probability localOptimum;  // Best likelihood value 
    std::string bestState;     // String representation for best state
    bool m_first_iterate;    //True if the call to 'iterate' is the first of many
    bool m_last_iterate;    //True if the call to 'iterate' is the last of many
  };

}//end namespace beep
#endif
