
#include <assert.h>
#include "Density2P_positive.hh"
#include "Beep.hh"

// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  //----------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //----------------------------------------------------------------------
  Density2P_positive::Density2P_positive(Real mean, Real variance, 
					 const std::string& density)
    : Density2P_common(mean, variance, density)
  {
    range.first = Real_limits::min();
  }

  Density2P_positive::Density2P_positive(const Density2P_positive& df)
    : Density2P_common(df)
  {}

  Density2P_positive::~Density2P_positive()
  {}

  Density2P_positive& 
  Density2P_positive::operator=(const Density2P_positive& df)
  {
    if(&df != this)
      {
	Density2P_common::operator=(df);
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // Sets embedded parameters of density (see doc for subclass).
  // Precondition: alpha and beta is within precision range.
  void 
  Density2P_positive::setEmbeddedParameters(const Real& first, 
					  const Real& second)
  {
    // Assert preconditions; separate tests give more error info
    assert(-Real_limits::max() < first && first < Real_limits::max());   
    assert(-Real_limits::max() < second && second < Real_limits::max()); 

    return Density2P_common::setEmbeddedParameters(first, second);
  }


  // value range - default uses precision range
  //------------------------------------------------------------------

//   bool 
//   Density2P_positive::isInRange(const Real& x) const
//   {
//     return (Real_limits::min() < x && x < Real_limits::max());
//   }

//   void 
//   Density2P_positive::getRange(Real& min, Real& max) const
//   {
//     min = Real_limits::min();
//     max = Real_limits::max(); 
//     return;
//   }

  void
  Density2P_positive::setRange(const Real& min, const Real& max)
  {
    assert(min >= Real_limits::min());
    return Density2P_common::setRange(min, max);
  }


  // parameter ranges default uses precision ranges
  //------------------------------------------------------------------

//   void 
//   Density2P_positive::getMeanRange(Real& min, Real& max) const
//   {
//     min = Real_limits::min();
//     max = Real_limits::max();
//     return;
//   }

//   void 
//   Density2P_positive::getVarianceRange(Real& min, Real& max) const
//   {
//     min = Real_limits::min();
//     max = Real_limits::max();
//     return;
//   }

}//end namespace beep

