//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///* The idea of this class is to provide an easy interface to the posterior
// * MCMC Pr[G,l,\theta | D]. It uses the GSRSampler and further nests it by
// * including sequences and substitutions.
// *
// * File:   PosteriorSampler.hh
// * Author: fmattias
// *
// * Created on January 25, 2010, 7:27 PM
// */
//
//#ifndef _POSTERIORSAMPLER_HH
//#define	_POSTERIORSAMPLER_HH
//
//#include <vector>
//
//#include "Density2P.hh"
//#include "EdgeDiscTree.hh"
//#include "EdgeRateMCMC_common.hh"
//#include "EdgeWeightHandler.hh"
//#include "lsd/LSDProbs.hh"
//#include "MatrixTransitionHandler.hh"
//#include "SequenceData.hh"
//#include "SiteRateHandler.hh"
//#include "StrStrMap.hh"
//#include "SubstitutionMCMC.hh"
//#include "UniformDensity.hh"
//
//#include "GSRSampler.hh"
//
//namespace beep {
//
//class PosteriorSampler : public MCMCSampler<SubstitutionMCMC> {
//public:
//    /**
//     * Creates a sampler for the posterior density Pr[G,l,\theta | D] described
//     * in "Simultaneous bayesian gene tree reconstruction and reconcilliation
//     * analysis". This constructor creates a discretized species tree from the
//     * minIntervals and timeStep parameters.
//     *
//     * sequenceData - The sequences 'D'.
//     * speciesTree - The species tree.
//     * geneSpeciesMap - A map between genes and species.
//     * Q - The transition matrix for substitutions.
//     * edgeRateDensity - The density function for edge rates.
//     * minIntervals -  The minimum number of intervals that an edge in the
//     *                 species tree will be discretized in.
//     * timeStep - The distance between two discretization points. If 0
//     *            minIntervals will be used instead.
//     */
//    PosteriorSampler(SequenceData &sequenceData,
//                     Tree &speciesTree,
//                     StrStrMap &geneSpeciesMap,
//                     MatrixTransitionHandler &Q,
//                     Density2P &edgeRateDensity,
//                     int minIntervals = DEFAULT_MIN_INTERVALS,
//                     float timeStep = DEFAULT_TIME_STEP
//                    );
//
//    /**
//     * Creates a sampler for the posterior density Pr[G,l,\theta | D] described
//     * in "Simultaneous bayesian gene tree reconstruction and reconcilliation
//     * analysis". This constructor uses an already created discretized species
//     * tree.
//     *
//     * sequenceData - The sequences 'D'.
//     * speciesTree - The species tree.
//     * geneSpeciesMap - A map between genes and species.
//     * Q - The transition matrix for substitutions.
//     * edgeRateDensity - The density function for edge rates.
//     * discretizedTree - A discretization of the species tree.
//     * lsd - Large scale duplications in the species tree.
//     */
//    PosteriorSampler(SequenceData &sequenceData,
//                     Tree &speciesTree,
//                     StrStrMap &geneSpeciesMap,
//                     MatrixTransitionHandler &Q,
//                     Density2P &edgeRateDensity,
//                     EdgeDiscTree &discretizedTree,
//                     LSDProbs lsd = LSDProbs()
//                    );
//
//    virtual ~PosteriorSampler();
//
//    /**
//     * Returns the GSR sampler underlying this sampler. Note however that
//     * the GSR sampler is connected to the posterior samples, such that when
//     * you draw a new sample from the GSR sampler it will advance the posterior
//     * sampler too. The difference is only that you get a EdgeDiscGSR * instead
//     * of a SubstitutionMCMC *.
//     */
//    GSRSampler *getGSRSampler();
//
//private:
//    // Hide copy constructor
//    PosteriorSampler(const PosteriorSampler &other);
//
//    /**
//     * setup()
//     *
//     * Sets up the nested GSR MCMC chain.
//     */
//    void setup();
//
//    //
//    SequenceData                m_sequenceData;
//    MatrixTransitionHandler     m_Q;
//    GSRSampler                  m_gsrSampler;
//
//    EdgeWeightHandler           *m_edgeWeightHandler;
//    UniformDensity              *m_siteRateDensity;
//    ConstRateMCMC               *m_mcmcSiteRates;
//    SiteRateHandler             *m_siteRateHandler;
//    std::vector<std::string>    *m_partList;
//    SubstitutionMCMC            *m_substitutionMCMC;
//    SimpleMCMC                  *m_posteriorMCMC;
//
//private:
//    static const double   UNIFORM_SITE_RATE_START = 0.0;
//    static const double   UNIFORM_SITE_RATE_END = 3.0;
//
//    static const float  DEFAULT_TIME_STEP = 0.0f;
//    static const int    DEFAULT_MIN_INTERVALS = 3;
//
//};
//
//}
//
//#endif	/* _POSTERIORSAMPLER_HH */

