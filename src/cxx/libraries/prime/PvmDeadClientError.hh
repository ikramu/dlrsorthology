#ifndef PVMDEADCLIENTERROR_HH
#define PVMDEADCLIENTERROR_HH

#include "PvmError.hh"

//
// For notifications of disappeared clients
//

class PvmDeadClientError : public beep::AnError
{
public:
  PvmDeadClientError(int task_id);
};

#endif
