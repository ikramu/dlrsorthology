#ifndef EPOCHTREE_HH
#define EPOCHTREE_HH

#include <list>
#include <string>
#include <utility>
#include <vector>

#include "BeepVector.hh"
#include "EpochPtSet.hh"
#include "Node.hh"
#include "Tree.hh"

namespace beep
{

/**
 * Encapsulates a host tree where the tip of the top time edge and vertices
 * have given rise to a slicing of the tree into "epochs".
 * Moreover, each epoch has, time-wise, been sliced further into several segments,
 * see EpochPtSet for more info. The top time of the tree is required to be
 * greater than 0 (i.e. there must be a top time edge).
 * 
 * If two (or more) non-leaf vertices have the same time, one of the corresponding borders
 * are moved slightly so that two adjacent but disjoint epochs are created. This implies
 * that, for the sake of consistency, one should strive to access all node times, top time,
 * etc. from this class rather than from the underlying tree or the nodes themselves.
 * 
 * Creating the discretized tree, one can choose to divide each epoch into the
 * same number of sub-divisions, or set an approximate timestep resulting in roughly
 * equidistant spacing.
 * 
 * The order of edges in epochs are conserved thus: comparing epoch number i and epoch
 * i-1 below, the "split edge" at index j in epoch i stems from edges j and j+1 in
 * epoch i-1. An edge k, k<j, in epoch i has index k in epoch i-1. An edge k, k>j, in
 * epoch i has index k+1 in epoch i-1.
 * 
 * @author Joel Sjöstrand.
 */
class EpochTree
{
	typedef std::vector<const Node*> nodevec;
	
public:
	
	typedef std::vector<EpochPtSet>::const_iterator const_iterator;
	typedef std::vector<EpochPtSet>::const_reverse_iterator const_reverse_iterator;
	
	/**
	 * Minimum time span for a epoch. If smaller than this, one of the "time-colliding"
	 * epoch borders is moved slightly. Should work for any number of time-colliding nodes
	 * (leaves not considered, naturally).
	 */
	static const Real MIN_SPLICE_DELTA = 0.0001;
	
	/**
	 * Constructor. The user specifies a minimum number of discretization sub-division
	 * intervals per epoch, and an approximate timestep between points.
	 * Set the timestep to <=0 to create the same number of intervals on each epoch.
	 * @param S the underlying tree.
	 * @param minNoOfIvsPerEp the minimum number of discretization intervals
	 *        per epoch, or exact number if maxTimestep<=0. Must be at least 2.
	 * @param maxTimestep the approximate desired timestep. Never exceeded.
	 *        Set to <=0 for exactly minNoOfIvsPerSlice intervals on each epoch.
	 */
	EpochTree(Tree& S, unsigned minNoOfIvsPerEp, Real maxTimestep=0.0);
	
	/**
	 * Destructor.
	 */
	~EpochTree();
	
	/**
	 * Updates the discretization based on the underlying tree.
	 */
	void update();
	
	/**
	 * Returns an iterator to the leaf epoch.
	 * @return iterator to leaf epoch.
	 */
	const_iterator begin() const;
	
	/**
	 * Returns an iterator beyond the top time epoch.
	 * @return iterator beyond top time epoch.
	 */
	const_iterator end() const;
	
	/**
	 * Returns a reverse iterator to the top time epoch.
	 * @return reverse iterator to top time epoch.
	 */
	const_reverse_iterator rbegin() const;
	
	/**
	 * Returns a reverse iterator beyond the leaf epoch.
	 * @return reverse iterator beyond leaf epoch.
	 */
	const_reverse_iterator rend() const;

	/**
	 * Returns the epoch at a specified index. Index 0 corresponds to epoch at leaves.
	 * @param epochNo the epoch identifier.
	 * @return the epoch discretization.
	 */
	const EpochPtSet& operator[] (unsigned epochNo) const;
	
	/**
	 * Returns the discretized top time. Note: Don't use the tree's own value
	 * directly, since it may not be exactly the same as the discretized value.
	 * @return the discretized top time.
	 */
	Real getTopTime() const;
	
	/**
	 * Returns the discretized top-to-leaf-time. Note: Don't use the tree's own value
	 * directly, since it may not be exactly the same as the discretized value.
	 * @return the discretized top-to-leaf time.
	 */
	Real getTopToLeafTime() const;
	
	/**
	 * Returns the discretized root-to-leaf-time. Note: Don't use the tree's own value
	 * directly, since it may not be exactly the same as the discretized value.
	 * @return the discretized root-to-leaf time.
	 */
	Real getRootToLeafTime() const;
	
	/**
	 * Returns the discretized time of a node. Note: Don't use the node's own time
	 * directly, since it may not be exactly the same as the discretized value.
	 * @param node the node.
	 * @return the discretized time of the node.
	 */
	Real getTime(const Node* node) const;
	
	/**
	 * Returns a discretized time.
	 * @param et the time identifier.
	 * @return the discretized time.
	 */
	Real getTime(const EpochTime& et) const;
	
	/**
	 * Returns the epoch number above a specified node.
	 * @param node the lower end of the epoch.
	 * @return the epoch number.
	 */
	unsigned getEpochAbove(const Node* node) const;

	/**
	 * Returns the epoch number below a specified node.
	 * Undefined for leaves.
	 * @param node the upper end of the epoch.
	 * @return the epoch number.
	 */
	unsigned getEpochBelow(const Node* node) const;
	
	/**
	 * Returns the total number of epochs.
	 * @return the number of epochs. 
	 */
	unsigned getNoOfEpochs() const;
	
	/**
	 * Returns the number of edges of a specified epoch.
	 * @param epochNo the epoch identifier.
	 * @return the number of edges.
	 */
	unsigned getNoOfEdges(unsigned epochNo) const;
	
	/**
	 * Returns the timestep of a certain epoch.
	 * @param epochNo the epoch identifier.
	 * @return the timestep.
	 */
	Real getTimestep(unsigned epochNo) const;
	
	/**
	 * Returns the smallest timestep among the epochs.
	 * @return the smallest timestep.
	 */
	Real getMinTimestep() const;
	
	/**
	 * Returns the number of discretized times of the 
	 * entire tree.
	 * @param unique if false, counts epoch-epoch boundary times
	 *        twice (the time is part of both adjacent epochs).
	 * @return the number of discretized times.
	 */
	unsigned getTotalNoOfTimes(bool unique) const;
	
	/**
	 * Returns the total number of points in the
	 * entire tree, all
	 * endpoints included, even time-coinciding ones.
	 * @return overall number of points.
	 */
	unsigned getTotalNoOfPoints() const;
	
	/**
	 * With respect to an epoch's edge vector, returns
	 * the index of the edge splitting at the epoch's
	 * lower border.
	 * @param epochNo epoch identifier.
	 * @return index of splitting edge.
	 */
	unsigned getSplitIndex(unsigned epochNo) const;
	
	/**
	 * Returns the discretized time identifier of
	 * the very tip of the top time edge.
	 * @return the top time "tip".
	 */
	EpochTime getEpochTimeAtTop() const;
	
	/**
	 * Returns the discretized time identifier
	 * below another. Undefined for input
	 * corresponding to leaf time.
	 * @param et the time identifier.
	 * @return the time below.
	 */
	EpochTime getEpochTimeBelow(const EpochTime& et) const;
	
	/**
	 * Returns the discretized time identifier below another,
	 * with the condition that their time values do not coincide,
	 * i.e. if the input parameter is lowermost of an epoch, the
	 * returned value is the penultimate time of epoch below.
	 * @param et the time identifier.
	 * @return the time strictly below.
	 */
	EpochTime getEpochTimeBelowStrict(const EpochTime& et) const;
	
	/**
	 * Returns the discretized time identifier above another.
	 * @param et the time identifier. Undefined for input
	 * corresponding to tip of top time edge.
	 * @return the time above.
	 */
	EpochTime getEpochTimeAbove(const EpochTime& et) const;
	
	/**
	 * Returns the discretized time identifier above another,
	 * with the condition that their time values do not coincide,
	 * i.e. if the input refers to last time on an epoch, the
	 * returned value is second time of epoch above.
	 * @param et the time identifier.
	 * @return the time strictly above.
	 */
	EpochTime getEpochTimeAboveStrict(const EpochTime& et) const;
	
	/**
	 * Returns the discretized time identifier above another,
	 * with the condition that if the new value would be the last
	 * of the epoch, a move yet another notch up is made.
	 * @param et the time identifier.
	 * @return the time above, albeit not last of epoch.
	 */
	EpochTime getEpochTimeAboveNotLast(const EpochTime& et) const;
	
	/**
	 * Returns true if the discretized time identifier is the
	 * last of its epoch.
	 * @param et the time identifier.
	 * @return true if last time of epoch.
	 */
	bool isLastEpochTime(const EpochTime& et) const;
	
	/**
	 * Returns the underlying tree.
	 * @return the tree.
	 */
	Tree& getOrigTree() const;
	
	/**
	 * Returns the underlying tree's root node.
	 * @return the root node.
	 */
	const Node* getOrigRootNode() const;
	
	/**
	 * Returns the underlying tree's node with the specified index.
	 * @param index the node index.
	 * @return the node.
	 */
	const Node* getOrigNode(unsigned index) const;
	
	/**
	 * Returns info string for debugging.
	 * @param inclNodeInfo if true includes node-based info.
	 * @return the info string.
	 */
	std::string getDebugInfo(bool inclEpochInfo=false, bool inclNodeInfo=false) const;
	
private:
	
	/**
	 * Recursive helper. Returns the number of subintervals to slice
	 * a certain epoch time span into.
	 * @param tLo the epoch's lower time
	 * @param tUp the epoch's upper time.
	 * @return the number of subintervals.
	 */
	unsigned getNoOfIntervals(Real tLo, Real tUp) const;
	
	/**
	 * Recursive helper. Adds leaves of tree rooted at n from
	 * left to right.
	 * @param v vector to add leaves to.
	 * @param n root of subtree.
	 */
	void addLeavesLeftToRight(nodevec& v, const Node* n) const;
	
public:
	
private:
	
	/** The underlying original Tree. */
	Tree& m_S;
	
	/**
	 * The minimum number of sub-division intervals per epoch.
	 * If maxTimestep <= 0, this is the exact number of intervals of each epoch.
	 */
	unsigned m_minNoOfIvs;
	
	/**
	 * The approximate (but never exceeded) timestep between discretization
	 * points. Set to <=0 to divide every epoch in equally
	 * many parts (see minNoOfIvs).
	 */
	Real m_maxTimestep;
	
	/** Epochs. */
	std::vector<EpochPtSet> m_epochs;
	
	/** For each epoch, the index of the edge in the epoch leading to split below. */
	std::vector<unsigned> m_splits;
	
	/** For each node, the index of the epoch which the node is the lower end of. */
	UnsignedVector m_nodeAboves;
};

} // end namespace beep

#endif // EPOCHTREE_HH

