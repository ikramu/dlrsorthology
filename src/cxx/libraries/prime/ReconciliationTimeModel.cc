#include <sstream>

#include "AnError.hh"
#include "ReconciliationTimeModel.hh"


namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Construct / Destruct /assignment
  //
  //----------------------------------------------------------------------
  ReconciliationTimeModel::ReconciliationTimeModel(Tree &G_in, 
						   BirthDeathProbs& bdp_in, 
						   const GammaMap& gamma_in,
						   bool include_root_time)
    : G(&G_in),
      bdp(&bdp_in),
      gamma(&gamma_in),
      table(G->getNumberOfNodes()),
      includeRootTime(include_root_time) 
  {
    if(G->hasTimes() == false)
      {
	G->setTimes(*new RealVector(*G));
      }
    recursiveUpdateTable(*G->getRootNode());
  };

  // Construct using an existing ReconciliationModel (subclass)
  ReconciliationTimeModel::ReconciliationTimeModel(ReconciliationModel& rs,
						   bool include_root_time)
    : G(&rs.getGTree()),
      bdp(&rs.getBirthDeathProbs()),
      gamma(&rs.getGamma()),
      table(G->getNumberOfNodes()),
      includeRootTime(include_root_time)
  {
    if(G->hasTimes() == false)
      {
	G->setTimes(*new RealVector(*G));
      }
    recursiveUpdateTable(*G->getRootNode());
  };

  // Copy constructor
  //----------------------------------------------------------------------  
  ReconciliationTimeModel::ReconciliationTimeModel(const ReconciliationTimeModel& RTS)
    : G(RTS.G),
      bdp(RTS.bdp),
      gamma(RTS.gamma),
      table(RTS.table),
      includeRootTime(RTS.includeRootTime)
  {
  };

  //Destructor
  //----------------------------------------------------------------------  
  ReconciliationTimeModel::~ReconciliationTimeModel()
  {
  }

  // Assignment operator
  //----------------------------------------------------------------------  
  ReconciliationTimeModel& 
  ReconciliationTimeModel::operator=(const ReconciliationTimeModel& RTS)
  {
    if(this != &RTS)
      {
	G = RTS.G;
	bdp = RTS.bdp;
	gamma = RTS.gamma;
	table = RTS.table;
	includeRootTime = RTS.includeRootTime;
      }
    return *this;
  };


  //----------------------------------------------------------------------
  //
  // Public interface
  //
  //----------------------------------------------------------------------

  // Acess shared attributes
  //----------------------------------------------------------------------
  Tree& 
  ReconciliationTimeModel::getGTree()
  {
    return *G;
  }

  // Interface inherited from ProbabilityModel
  //----------------------------------------------------------------------
  Probability
  ReconciliationTimeModel::calculateDataProbability()
  {
    Node* Sroot = bdp->getStree().getRootNode();

    Node* Groot = G->getRootNode();
    if(includeRootTime)
      {
	return recursiveDataProb(Groot, Sroot);//, Sroot->getTime());
      }
    else
      {
// 	Probability old = 
// 	  recursiveDataProb(Groot->getLeftChild(),Sroot,Sroot->getTime())
// 	   * recursiveDataProb(Groot->getRightChild(), Sroot, Sroot->getTime());
// 	Probability novel = 
// 	  recursiveDataProb(Groot->getLeftChild(),Sroot)
// 	   * recursiveDataProb(Groot->getRightChild(), Sroot);

// 	if((std::abs(old.p - novel.p) / (2 * (old.p + novel.p))) > 0.000001)
// 	  {
// 	    ostringstream oss;
// 	    oss << "difference found!!!\n"
// 		 << "old = " << old
// 		<< "     novel = " << novel << endl;
// 	    exit(27);
// 	  }

#ifdef THOMPSON
	// If THOMPSON is defined, this will 
	// yield Thompson's probability Pr[t,G,l|lambda, mu, S], where G is a
	// labeled history, t is the divergence times and l the labels of G, 
	// and S is a single-leaved host tree. 
	// Notice that S must be single-leaved!
	return bdp->getThompsons_p1() 
	  * recursiveDataProb(Groot->getLeftChild(),Sroot)
	  * recursiveDataProb(Groot->getRightChild(), Sroot);
#else
	return recursiveDataProb(Groot->getLeftChild(),Sroot)
	  * recursiveDataProb(Groot->getRightChild(), Sroot);
#endif
      }
  }

  void
  ReconciliationTimeModel::update()
  {
    recursiveUpdateTable(*G->getRootNode());
  }

  //----------------------------------------------------------------------
  //
  //I/O
  //
  //----------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, 
	     const ReconciliationTimeModel& RTS)
  {
    return o << RTS.print();
  };

  
  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------

  // recursively calculates probability of edgeTimes
  // within a slice this is modeled using the birth death process relating 
  // to the slice. Suppose u has children v and w, that t_u is the time from
  // u to the bottom of the slice, and that T = time interval of the slice
  //    \mbox{Pr}(t_u|maxT, G_u^{x,y}) = 
  //    (c-1)\tilde{P(t_u)(1-\tilde{u}_{t_u})/\tilde{u}_{T}
  //    (\mbox{Pr}(t_v|t_u, G_v^{x,y})\mbox{Pr}(t_v|t_u, G_v^{x,y})}
  // gn is the current node of G, sn is the current slice of S maxT is the 
  // time between either gn's parent and the bottom of the slice or the
  // time interval of the slice
  //----------------------------------------------------------------------
  Probability
  ReconciliationTimeModel::recursiveDataProb(Node* gn, Node* sn)
  {
    assert(gn != 0);
    assert(sn != 0);

    // case 1: gn is either a speciation or has a virtual speciation on its
    // incoming edge. In either case it has one or more antichains mapped to it
    //-------------------------------------------------------------------------
    if(gamma->numberOfGammaPaths(*gn) > 0)// if gn has gamma-maps
      {
	// First get to the bottom slice - this is where we can apply the 
	// birth-death model (in other mapped slices the prob of time is 1.0)
	Node* acn = gamma->getLowestGammaPath(*gn); 

	// 1a: it's a speciation
	if(gamma->isSpeciation(*gn)) 
	  {
	    // relative nodeTime (in slice) = 0 with prob=1.0 for speciations
	    if(!gn->isLeaf()) 
	      {
		// set params for passing on recursion on the next slices
		Node* gl = gn->getLeftChild(); 
		Node* gr = gl->getSibling();
		Node* sl = &gamma->getDominatingChild(*acn, *gl); 
		Node* sr = sl->getSibling();
		Probability left = recursiveDataProb(gl, sl);
		Probability right = recursiveDataProb(gr, sr);

		return left * right;
	      }
	    else 
	      {
		return 1.0;
	      }
	  }
	// 1b: it's a duplication with virtual speciations which means 
	// it's edge covers several slices we are only interested in the
	// bottom slice (all other has pro=1.0 for gn's time
	else                  
	  {                   
	    // set params for passing on recursion
	    Node* gl = gn->getLeftChild(); 
	    Node* gr = gl->getSibling();
	    Node* s = gamma->getLineage(acn, *gn);
	    Probability left = recursiveDataProb(gl, s);
	    Probability right = recursiveDataProb(gr, s);

	    Real t = gn->getNodeTime() - s->getNodeTime();
  	    return left * right * bdp->partialEdgeTimeProb(*s, table[gn], t);
	  }
      }

    // case 2: gn is a duplication without virtual speciations (no antichains)
    // We only need to calculate the BD-process prob of gn's time
    //-------------------------------------------------------------------------
    else              
      {                    
	// set params for passing on recursion
	Node* gl = gn->getLeftChild(); 
	Node* gr = gl->getSibling();
	Probability left = recursiveDataProb(gl, sn);
	Probability right = recursiveDataProb(gr, sn);
	Real t = gn->getNodeTime() - sn->getNodeTime();
  	return left * right * bdp->partialEdgeTimeProb(*sn, table[gn], t); 
      }
  };
  

  // Counts for each gene node the number of children in the
  // lowest gamma below or at same level
  //----------------------------------------------------------------------
  unsigned
  ReconciliationTimeModel::recursiveUpdateTable(Node& gn)
  {
    if(!gn.isLeaf()) 
      {
	//Pass on recursion
	unsigned leaves = recursiveUpdateTable(*gn.getLeftChild()) +
	  recursiveUpdateTable(*gn.getRightChild());
      
	if(gamma->isSpeciation(gn)) //speciations always get 1
	  {
	    table[gn] = 1;
	  }
	else // Otherwise use value from recursion
	  {
	    table[gn] = leaves;
	  
	    // pass back current number of leaves

	    if(gamma->numberOfGammaPaths(gn) == 0)
	      {
		return leaves;
	      }
	  }
      }
    else  
      {
	table[gn] = 1;
      }
    // Leaves and speciations are terminal in slices
    return 1;
  };


  //----------------------------------------------------------------------
  //
  //I/O
  //
  //----------------------------------------------------------------------

  // print function for parameters used in operator<<
  //
  // THIS MUST BE UPDATED!!
  //--------------------------------------------------------------------
  std::string 
  ReconciliationTimeModel::print() const
  {
    std::ostringstream os;
    os     << "Parameters:\n"
	   << "G (gene tree):\n"
      //here should be added a G->tree4os statement when this becomes available
	   << "\n"
	   << "gamma (reconciliation betweeen S and G):\n"
      //here should be added a gamma->gamma4os statement when this becomes available
	   << gamma
	   << "\n"
	   << "table (# leaves in G_{u,gamma(y)}, u in V(G), y in V(S)):\n"
	   << table4os()
	   << "\n";
      
    return os.str();
  }

  // helper print function for table
  //--------------------------------------------------------------------
  std::string
  ReconciliationTimeModel::table4os() const
  {
    std::ostringstream os;
    typedef std::map<unsigned, unsigned>::const_iterator MapIter;
    os << "------------------------------------\n";
    for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
      {
	os << "  Node " 
	   << i
	   << "\t"
	   << table[G->getNode(i)]
	   << " leaves\n";
      }
    os << "------------------------------------\n";
    return os.str();
  };


  //old version
  Probability
  ReconciliationTimeModel::recursiveDataProb(Node* gn, Node* sn, Real maxT)
  {

    cerr << "ReconciliationModel::recursiveDataProb(Node* gn, Node* sn, Real maxT)\n"
	 << "Deprecated! Use recursiveDataProb(Node* gn, Node* sn) instead\n";


    assert(gn != 0);
    assert(sn != 0);
    assert(maxT >0); // Uncomment when everythings works and comment out below instead!
//     if(maxT <= 0)
//       {
// 	ostringstream oss;
// 	oss << "maxT >= 0 for gene node " 
// 	    << gn->getNumber()
// 	    << " and species node "
// 	    << sn->getNumber()
// 	    << endl;
// 	throw AnError(oss.str());
//       }

    // Initiate some common variables
    Probability p(1.0);         // return value
    Real time = gn->getTime();  // The part of the edge time that is modeled
    Node* gl = gn;              // left child if it exists
    Node* gr = gn;              // right child if it exists
    Node* sl = sn;              // slice of gl
    Node* sr = sn;              // slice of gr
    Real maxTL = 0;             // maxT of gl and gr
    Real maxTR = 0;             // maxT of gl and gr

    // case 1: gn is either a speciation or has a virtual speciation on its
    // incoming edge. In either case it has one or more antichains mapped to it
    //-------------------------------------------------------------------------
    if(gamma->numberOfGammaPaths(*gn) > 0)// if gn has gamma-maps
      {
	// First get to the bottom slice - this is where we can apply the 
	// birth-death model (in other mapped slices the prob of time is 1.0)
	Node* acn = gamma->getLowestGammaPath(*gn); 

	// 1a: it's a speciation
	if(gamma->isSpeciation(*gn)) 
	  {
	    // maxT-time is always 0 for speciations
	    if(!gn->isLeaf()) 
	      {
		// set params for passing on recursion on the next slices
		// note that for all slices between sn and acn p = 1!
		gl = gn->getLeftChild(); 
		gr = gl->getSibling();
		sl = &gamma->getDominatingChild(*acn, *gl); 
		sr = sl->getSibling();
		maxTL = sl->getTime();
		maxTR = sr->getTime();
	      }
	  }
	// 1b: it's a duplication with virtual speciations which means 
	// it's edge covers several slices we are only interested in the
	// bottom slice (all other has pro=1.0 for gn's time
	else                  
	  {                   
	    // go back to the top and decrease time accordingly
	    // Remember we only want gn's time in the bottom slice!
	    while(!acn->dominates(*sn)) 
	      {    
		time -= acn->getTime();
		acn = acn->getParent();
	      }

	    // Probability for next duplication time from BD-process
	    // This Pr(t_u|T, G_u^{x,y})
  	    p *= bdp->partialEdgeTimeProb(*sl, table[gn], maxT - time);

	    // set params for passing on recursion
	    gl = gn->getLeftChild();
	    gr = gl->getSibling();
	    sl = sr = gamma->getLineage(acn, *gn);
	    maxTL = maxTR = maxT - time;
	  }
      }

    // case 2: gn is a duplication without virtual speciations (no antichains)
    // We only need to calculate the BD-process prob of gn's time
    //-------------------------------------------------------------------------
    else              
      {                    
	// get next duplication time and
	// This Pr(t_u|T, G_u^{x,y})
  	p = bdp->partialEdgeTimeProb(*sn, table[gn], maxT - time); 

	// set params for passing on recursion
	gl = gn->getLeftChild();
	gr = gl->getSibling();
	sl = sr = sn;
	maxTL = maxTR = maxT - time;
      }

    // Pass on recursion. Include time probabilities from nested vertices
    //-------------------------------------------------------------------------
    if(!gn->isLeaf())       
      {
	if(maxTL < 0) // change these to asserts? / bens
	  {
	    ostringstream oss;
	    oss  << "ReconciliationTime Sampler: "
		 << "maxT became less than 0 for gene node \n"
		 << gl->getNumber()
		 << " and species node "
		 << sl->getNumber()
		 << endl;
	    throw AnError(oss.str());
	  }
	if(maxTR < 0)
	  {
	    ostringstream oss;
	    oss << "ReconciliationTime Sampler: "
		<< "maxT became less than 0 for gene node \n"
		<< gr->getNumber()
		<< " and species node "
		<< sr->getNumber()
		<< endl;
	    throw AnError(oss.str());
	  }
	Probability left = recursiveDataProb(gl, sl, maxTL);
	Probability right = recursiveDataProb(gr, sr, maxTL);
	p *= (left * right);
      }	    

    // Finally return the propability product that relates to G_{gn}
    return p;
  };
  






}//end namespace beep
