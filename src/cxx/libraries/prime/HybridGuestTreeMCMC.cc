#include "HybridGuestTreeMCMC.hh"

#include "BirthDeathProbs.hh"
#include "HybridTree.hh"
#include "MCMCObject.hh"

namespace beep
{
  using namespace std;
  //--------------------------------------------------------------
  //
  // Construct/destruct/assign
  // 
  //--------------------------------------------------------------
  HybridGuestTreeMCMC::HybridGuestTreeMCMC(MCMCModel& prior, 
					   Tree& G, HybridTree& S,
					   StrStrMap& gs, BirthDeathProbs& bdp,
					   Real suggestRatio)
    : TreeMCMC(prior, G, G.getName() + "_" + bdp.getStree().getName() 
	       + "_HybridGuestTree", suggestRatio),
      HybridGuestTreeModel(G, S, gs, bdp)
  {}
	
  HybridGuestTreeMCMC::~HybridGuestTreeMCMC()
  {};
    
  HybridGuestTreeMCMC::HybridGuestTreeMCMC(const HybridGuestTreeMCMC& rtm)
    : TreeMCMC(rtm),
      HybridGuestTreeModel(rtm)
  {}
    
  HybridGuestTreeMCMC& 
  HybridGuestTreeMCMC::operator=(const HybridGuestTreeMCMC& rtm)
  {
    if(this != &rtm)
      {
	TreeMCMC::operator=(rtm);
	HybridGuestTreeModel::operator=(rtm);
      }
    return *this;
  }
    
    
  //--------------------------------------------------------------
  //
  // Interface
  // 
  //--------------------------------------------------------------
   string 
  HybridGuestTreeMCMC::print() const
  {
    return HybridGuestTreeModel::print() + TreeMCMC::print();
  }
    
  Probability 
  HybridGuestTreeMCMC::updateDataProbability()
  {
    if(G->perturbedNode() == G->getRootNode() || S->perturbedNode())
      {
	update();
      }
    return calculateDataProbability();
  }
    
}//end namespace beep

