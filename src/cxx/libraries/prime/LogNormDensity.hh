#ifndef LOGNORMDENSITY_HH
#define LOGNORMDENSITY_HH

#include "Density2P_positive.hh"


namespace beep
{
  //----------------------------------------------------------------------
  //
  //! Class LogNormDensity implements the logNormal density distribution
  //
  //! \f[
  //!  f(x) = 
  //!  \begin{cases}
  //!   \frac{e^{-\frac{\left(\ln(x)-\alpha\right)^2}{2\beta}}} 
  //!        {x \sqrt{2 \pi \beta}} & 
  //!        x > 0,\\  0, & \mbox{else},
  //!  \end{cases}
  //! \f]
  //! where \f$ \alpha\f$ = mean and \f$ \beta \f$ = variance of underlying 
  //! normal distribution
  //!
  //! Note! In conventional notation, \f$ \mu, \sigma^2 \f$ is used 
  //! instead of \f$ \alpha, \beta \f$.
  //!
  //! Invariants: variance > 0
  //!             mean     > 0
  //!
  //! Author: Martin Linder, adapted by Bengt Sennblad
  //! copyright the MCMC-club, SBC
  //
  //----------------------------------------------------------------------
  class LogNormDensity : public Density2P_positive
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct
    //
    //----------------------------------------------------------------------
    LogNormDensity(Real mean, Real variance, bool embedded = false);
    LogNormDensity(const LogNormDensity& df);
    ~LogNormDensity();
    LogNormDensity& operator=(const LogNormDensity& df);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    // Density-, distribution-, and sampling functions
    //----------------------------------------------------------------------
    Probability operator()(const Real& x) const;
    Probability operator()(const Real& x, const Real& interval) const;
    Real sampleValue(const Real& p) const;

    // Access parameters
    //----------------------------------------------------------------------
    Real getMean() const;
    Real getVariance() const;

    // Set Parameters
    //----------------------------------------------------------------------
    void setEmbeddedParameters(const Real& first, const Real& second);
    void setParameters(const Real& mean, const Real& variance);    
    void setMean(const Real& mean);    
    void setVariance(const Real& variance);
    
    //------------------------------------------------------------------
    // I/O
    //------------------------------------------------------------------
    virtual std::string print() const;

  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    static const Real pi = 3.14159265358979; //! math. constant \f$ \pi \f$.
    
    //The attribute c is a constant term, given the parameter 
    //values, in the log density function.
    //! \f$ c = \ln(\frac{1}{\sqrt{2\pi\beta}}) \f$
    //---------------------------------------------------------
    Real c;

  };

}//end namespace beep
#endif
