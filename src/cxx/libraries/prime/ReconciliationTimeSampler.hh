#ifndef RECONCILIATIONTIMESAMPLER_HH
#define RECONCILIATIONTIMESAMPLER_HH

#include <vector>
#include <iostream>

#include "Beep.hh"
#include "BeepVector.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "Node.hh"
#include "Tree.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "GammaMap.hh"
#include "ReconciliationModel.hh"

namespace beep
{
  //----------------------------------------------------------------------
  //
  //! Samples vertex times from Gene Evolution model
  //
  //----------------------------------------------------------------------
  class ReconciliationTimeSampler 
  {
    typedef std::map<unsigned, unsigned> Table;
  public:
    //----------------------------------------------------------------------
    //
    // Construct / Destruct /assignment
    //
    //----------------------------------------------------------------------

    // Constructor
    //----------------------------------------------------------------------  
    ReconciliationTimeSampler(Tree &G, BirthDeathProbs& bdp, GammaMap& gamma);

    // Construct from existing ReconciliationMOdel (subclass); ensures that
    // *this has the most current gamma
    ReconciliationTimeSampler(ReconciliationModel& rm);

    // Copy constructor
    //----------------------------------------------------------------------  
    ReconciliationTimeSampler(const ReconciliationTimeSampler& RTS);

    // Destructor
    //----------------------------------------------------------------------  
    ~ReconciliationTimeSampler();

    // Assignment operator
    //----------------------------------------------------------------------  
    ReconciliationTimeSampler& operator=(const ReconciliationTimeSampler& RTS);


    //----------------------------------------------------------------------
    //
    // Public interface
    //
    //----------------------------------------------------------------------
    // Set Shortest allowed edge time -- used in fastGEM
    void setShortestT(Real value);

    // Access for sharedparameter
    //----------------------------------------------------------------------
    Tree& getGTree();

    void update();
    // Main interface - get the times
    //----------------------------------------------------------------------
    Probability
    sampleTimes(bool includeRootEdge = true);

    //----------------------------------------------------------------------
    //
    //I/O
    //
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const ReconciliationTimeSampler& RTS);


  private:
    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------

    // generates times for each gene node 
    //----------------------------------------------------------------------
    Probability // Or vector of LikelihodVector if doing subst probs simultaneously
    recursiveTimeGeneration(Node* gn, Node* sn, Real maxT);
    void // Or vector of LikelihodVector if doing subst probs simultaneously
    old_recursiveTimeGeneration(Node* gn, Node* sn, Real maxT);
  
    //   // The actual calculation of edge time
    //   //----------------------------------------------------------------------
    //   Real
    //   generateEdgeTime(const unsigned& leaves, const Real& maxT);

    // calculates for each gene node the number of children in the
    // lowest gamma below or at same level
    //----------------------------------------------------------------------
    unsigned
    recursiveUpdateTable(Node& gn);


    //----------------------------------------------------------------------
    //
    //I/O
    //
    //----------------------------------------------------------------------

    // print function for parameters used in operator<<
    //--------------------------------------------------------------------
    const std::string print() const;

    // helper print function for table
    //--------------------------------------------------------------------
    const std::string
    table4os() const;
  

    //----------------------------------------------------------------------
    // Attributes 
    //----------------------------------------------------------------------
    Tree* G;
    Tree* S;
    BirthDeathProbs* bdp;
    const GammaMap* gamma;
    PRNG R;
    UnsignedVector table; // Maps #leaf in G_{u,lowest gamma} for each genenode u
    Real shortestT;

  }
    ;  
}//end namespace beep
#endif
