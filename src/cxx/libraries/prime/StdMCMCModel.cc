#include "StdMCMCModel.hh"

#include "Beep.hh"
#include "MCMCObject.hh"
#include "LogNormDensity.hh"
#include "NormalDensity.hh"
#include "UniformDensity.hh"

#include <sstream>
#include <algorithm>
#include <cmath>
#include <assert.h>

//---------------------------------------------------------------
//
// classMCMCModel
// Baseclass that provides common implementation code 
// for the "MCMCModel-class family", which uses 
// MCMC to intergrate over parameters
//
// An instance of the StdMCMCModel class will inherit from a 
// ProbabilityModel class for likelihood calculations. 
// It also contains a MCMCModel class for calculation of any 
// additional prior probs that might exists; give a DummyMCMC 
// in constructor if no such prior occur. These two classes 
// contain the parameters fro StdMCMCModel to perturb.
//
// The main task for this class is to decide on when, on 
// suggestNewState()-calls, to pass this call on to the nested 
// prior classes and when to perturb the parameter of the 
// ProbabilityModel baseclass.
// The suggestRatio is a parameter that decides if a likelihood
// or a prior parameter should be updated:
// suggestRatio = pr{a}/pr{b}, where a = the average likelihood 
// parameter a is perturbed, b = the average prior parameter b 
// is perturbed. The attribute numParams is the number of local 
// parameters. If a likelihood parameter is to be perturbed, the 
// pure virtual' function perturbState() is called.
//
// This class currently inherits from MCMCModel, it is possible
// that it might eventually replace MCMCModel.
// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
//
//---------------------------------------------------------------



namespace beep
{
  using namespace std;

  //---------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  // 
  //---------------------------------------------------------------

  // Constructor
  // suggestRatio = pr{a}/pr{b}, a = a like parameter is perturbed, 
  // b = a prior parameter is pertubed. paramIdxRatio adjusts for 
  // unequal number of like and prior parameters.
  //---------------------------------------------------------------
  StdMCMCModel::StdMCMCModel(MCMCModel& prior, 
			     const unsigned& n_params,
			     const Real& suggestRatio) :
    MCMCModel(),
    prior(&prior),
    n_params(n_params),
    stateProb(0.0), 
    old_stateProb(0.0),
    suggestRatio(suggestRatio),
    suggestRatioDelta(0),
    suggestRatioPendingUpdates(0),
    paramIdxRatio((n_params == 0)? 0:
		  (1.0 / (1.0 + (prior.nParams() * suggestRatio) /
			  (n_params *(1.0 - suggestRatio))))),
    paramIdx(0),
    name()
  {
    updateParamIdx();
    initName("Model"); 
  }

  StdMCMCModel::StdMCMCModel(MCMCModel& prior, 
			     const unsigned& n_params,
			     const std::string& name_in,
			     const Real& suggestRatio) :
    MCMCModel(),
    prior(&prior),
    n_params(n_params),
    stateProb(0.0), 
    old_stateProb(0.0),
    suggestRatio(suggestRatio),
    suggestRatioDelta(0),
    suggestRatioPendingUpdates(0),
    paramIdxRatio((n_params == 0)? 0:
		  (1.0 / (1.0 + (prior.nParams() * suggestRatio) /
			  (n_params *(1.0 - suggestRatio))))),
    paramIdx(0),
    name()
  {
    updateParamIdx();
    name = name_in;   // If a name is given, then assume that this is unique!
    initName(name); 
  }

  StdMCMCModel::StdMCMCModel(const StdMCMCModel& A) :
    MCMCModel(A),
    prior(A.prior),
    n_params(A.n_params),
    stateProb(A.stateProb),
    old_stateProb(A.old_stateProb),
    suggestRatio(A.suggestRatio),
    suggestRatioDelta(A.suggestRatioDelta),
    suggestRatioPendingUpdates(A.suggestRatioPendingUpdates),
    paramIdxRatio(A.paramIdxRatio),
    paramIdx(0),
    name()
  {
    // Copies will be identical - even th names
    // Use A.name w/o unique number as base for initName(base)
    //     initName(A.name.substr(0, A.name.find_first_of("0123456789")));
  }

  StdMCMCModel::~StdMCMCModel()
  {
  }

  StdMCMCModel& 
  StdMCMCModel::operator=(const StdMCMCModel& A)
  { 
    if(this != &A)
      {
	MCMCModel::operator=(A);
	prior = A.prior;
	n_params = A.n_params;
	stateProb = A.stateProb;
	old_stateProb = A.old_stateProb;
	suggestRatio = A.suggestRatio;
	suggestRatioDelta = A.suggestRatioDelta;
	suggestRatioPendingUpdates = A.suggestRatioPendingUpdates;
	paramIdxRatio = A.paramIdxRatio;
	paramIdx = A.paramIdx;
	// Keep old name?
	// Use A.name w/o unique number as base for initName(base)
	name = name;  // Copies will be identical - even the names
	// 	name = A.name.substr(0, A.name.find_first_of("0123456789")) +
	// 	  name.substr(A.name.find_first_of("0123456789"));
      }
    return *this;
  }

  //---------------------------------------------------------------
  // Standardized interface from MCMCModel
  // You should never really need to change this!
  //---------------------------------------------------------------
  MCMCObject
  StdMCMCModel::suggestNewState(unsigned x)
  {
    MCMCObject MOb;
    old_stateProb = stateProb;

    if(x <= n_params) 
      {      
	MOb = suggestOwnState(x);
	MOb.stateProb *= prior->currentStateProb();
      }
    else
      {
	MOb = prior->suggestNewState(x - n_params);
	MOb.stateProb *= updateDataProbability();
      }
    stateProb = MOb.stateProb;
    return MOb; 
  }

  MCMCObject 
  StdMCMCModel::suggestNewState()
  {
    if (name == "EdgeWeights")
      {
	// Old way for MapDP(?) to change suggestion ratio during chain.
	if (suggestRatio < 1.0)
	  {
	    suggestRatio = suggestRatio + 0.0001;
	    updateParamIdx();
	  }
      }
    else if (suggestRatioPendingUpdates > 0)
      {
	// Crude way of enabling changing suggestion ratio during chain.
	suggestRatio += suggestRatioDelta;
	suggestRatioPendingUpdates--;
      }

    MCMCObject MOb;
    old_stateProb = stateProb;

    paramIdx = R.genrand_real3();
    if(paramIdx <= paramIdxRatio) 
      {     
	MOb = suggestOwnState();
	MOb.stateProb *= prior->currentStateProb();
      }
    else
      {
	MOb = prior->suggestNewState();
	MOb.stateProb *= updateDataProbability();
      }
    stateProb = MOb.stateProb;
    return MOb; 
  }

  MCMCObject
  StdMCMCModel::suggestOwnState(unsigned x)
  { 
    return MCMCObject(1.0, 1.0);
  };

  void
  StdMCMCModel::commitOwnState(unsigned x)
  { 
  };

  void
  StdMCMCModel::discardOwnState(unsigned x)
  { 
  };

  // Return probability of current state - called when this is a prior.
  // First perform an update followed by a calculateDataProbability, then
  // ask prior for its likelihood.
  //----------------------------------------------------------------------
  Probability 
  StdMCMCModel::initStateProb()
  {
    stateProb = updateDataProbability() *  prior->initStateProb();
//     Probability priorP = prior->initStateProb();
//     Probability ownProb = updateDataProbability();
//     stateProb = own Prob *  priorP;
//     std::cerr << "Name: " << name << " Mult value: " << ownProb << " Tot value: " << stateProb << " prior: " << priorP << endl;
    return stateProb;
  }

  Probability 
  StdMCMCModel::currentStateProb()
  {
    return stateProb;
  }

  void        
  StdMCMCModel::commitNewState()
  {
    if(paramIdx <= paramIdxRatio) 
      {      
	commitOwnState();
      }
    else
      {
	prior->commitNewState();
      }
    //    old_stateProb = stateProb;
    registerCommit();
  }

  void    
  StdMCMCModel::commitNewState(unsigned x)
  {
    if(x <= n_params) 
      {      
	commitOwnState(x);
      }
    else
      {
	prior->commitNewState(x - n_params);
      }
    old_stateProb = stateProb;
    registerCommit();
  }

  void        
  StdMCMCModel::discardNewState()
  {
    if(paramIdx <= paramIdxRatio) 
      {      
	discardOwnState();
      }
    else
      {
	prior->discardNewState();
      }
    stateProb = old_stateProb;
    registerDiscard();
  }

  void
  StdMCMCModel::discardNewState(unsigned x)
  {
    if(x <= n_params) 
      {      
	discardOwnState(x);
      }
    else
      {
	prior->discardNewState(x - n_params);
      }
    stateProb = old_stateProb;
    registerDiscard();
  }

  // Represent current state as a string - calls PM.strRepresentation()
  // for states of nested priors
  //----------------------------------------------------------------------
  string 
  StdMCMCModel::strRepresentation() const
  {
    std::ostringstream oss;
#ifdef DEBUG_SHOWPROBS
    oss << stateProb << ";" << "\t";
#endif
    oss << ownStrRep();
    oss << prior->strRepresentation();
    return oss.str();
  }

  // Print a header corresponding to this string
  //----------------------------------------------------------------------
  string 
  StdMCMCModel::strHeader() const
  {
    //    using std::string;  //!< \todo{ Is this really needed? Does anyone know why it is here? Did I put it here? /bens}
    std::ostringstream oss;

    // Get own header and insert '<name>.' in front of parameters
    string s = ownHeader(); 
    // Start by finding first parameter name (if any)
    string::size_type pos = s.find_first_not_of(";\t "); 
    while(pos != string::npos)
      {
	s.insert(pos, name + ".");                // insert unique name
	pos = s.find_first_of(";\t ", pos);       // Are there more parameters?
	if(pos != string::npos)         
	  pos = s.find_first_not_of(";\t ", pos); // If so, find them and repeat
      };

#ifdef DEBUG_SHOWPROBS
    oss << name+".stateProb(logfloat);\t";
#endif
    oss << s 
	<< prior->strHeader();
    return oss.str();
  }

  // Return number of states in prior (probably only used first time!) 
  //----------------------------------------------------------------------
  unsigned   
  StdMCMCModel::nParams() const
  {
    return n_params + prior->nParams();
  }

  void 
  StdMCMCModel::setName(const string& name_in)
  {
    name = name_in;
  }

  string
  StdMCMCModel::getName()
  {
    return name;
  }


  void
  StdMCMCModel::setChangingSuggestRatio(Real finalRatio, unsigned noOfSteps)
  {
    suggestRatioPendingUpdates = noOfSteps;
    suggestRatioDelta = (finalRatio - suggestRatio) / noOfSteps;
  }


  //----------------------------------------------------------------------
  //
  // I/O
  //
  //----------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const StdMCMCModel& A)
  {
    return o << A.print()
      ;
  };

  string 
  StdMCMCModel::print() const
  {
    ostringstream oss;
    oss 
      << "suggestRatio is " 
      << paramIdxRatio
      << " for "
      << n_params
      << " parameters\n"
      << "Prior:\n"
      << indentString(prior->print())
      ;
    return oss.str();
  }

  //---------------------------------------------------------------
  //
  // Implementation
  // 
  //---------------------------------------------------------------
  void
  StdMCMCModel::updateParamIdx()
  {
    // Setup probability of perturbing this.
    if(n_params != 0)
      {
	Real weight_nParams = suggestRatio * n_params;
	paramIdxRatio = weight_nParams / (weight_nParams + prior->nParams());
      }
    else
      {
	paramIdxRatio = 0;
      }
  }

  // Assigns a unique name to an obejct of this class.
  // Typically '<base><unique number>'.
  void 
  StdMCMCModel::initName(std::string base)
  {
    std::ostringstream oss;
    oss << base << unique++;
    name = oss.str();
  }


  // Samples X ~ N(value, variance)
  // Precondition: min < old < max 
  // Postcondition: min < return-value < max
  //----------------------------------------------------------------------
  Real   
  StdMCMCModel::perturbNormal(Real value, Real variance, Real Min, 
			      Real Max, Probability& propRatio) const
  {
    static NormalDensity nd(1.0, 1.0);

    assert(Min < value && value < Max); //Check precondition
    assert(Real_limits::min() < variance && variance < Max); //Check precondition

    Real old = value;
    nd.setParameters(value, variance);
    //     do
    //       {
    value = nd.sampleValue(R.genrand_real3());
    //       }
    //     while(Min > value || value > Max);
    if(Min > value || value > Max)
      {
	value = old;
	propRatio = 0.0;
      }
    else
      {
	propRatio = 1.0;
      }

    return value;
  }

  // Samples ln x ~ logN(ln value, variance)
  // strict: 0 -> ~]Min, Max[; 1 -> ]Min, Max]; 2 -> [Min, Max[; 3-> [Min,Max]
  // Precondition: min < old < max 
  // Postcondition: min < return-value < max
  //----------------------------------------------------------------------
  Real   
  StdMCMCModel::perturbLogNormal(Real value, Real variance, Real Min, 
				 Real Max, Probability& propRatio,
				 unsigned strict) const
  {
    // joelgs: Empirical tests seemed to suggest return values exp(X) where
    // X ~ N(ln value, variance), i.e. the fixed input variance referred to the
    // corresponding Normal distribution, implying that the variance w.r.t.
    // returned values will vary with the mean, 'value'.

    static LogNormDensity nd(1.0, 1.0);

    //Check precondition
    assert((strict > 1 && Min <= value) || Min < value);
    assert(((strict == 1 || strict == 3) && value <= Max) || (value < Max));

    Real old = value;
    propRatio = 1.0;
    nd.setEmbeddedParameters(std::log(old)-0.5*variance, variance);
    value = nd.sampleValue(R.genrand_real3());

    if(((strict > 1 && Min <= value) || Min < value) &&
       (((strict == 1 || strict == 3) && value <= Max) || (value < Max)))
      {
	propRatio /= nd(value);
	nd.setEmbeddedParameters(std::log(value)-0.5*variance, variance);
	propRatio *= nd(old);
      }
    else
      {
	value = old;
	propRatio = 0.0;
      }

    return value;
  }

  // Samples ln x ~ U(ln value, variance)
  // Precondition: min < old < max 
  // Postcondition: min < return-value < max
  //----------------------------------------------------------------------
  Real   
  StdMCMCModel::perturbUniform(Real value, Real variance, Real Min,
			       Real Max, Probability& propRatio) const
  {
    // joelgs: Empirical tests seemed to suggest return values X where
    // X ~ U(a, b) where a and b are computed intuitively from mean 'value'
    // and variance. In other words, no logarithm on either input mean or
    // return value. Also, should the proposal ratio be 1.0...?

    static UniformDensity ud(1.0, 1.0);

    assert(Min < value && value < Max); //Check precondition

    ud.setParameters(value, variance);
    //     do
    //       {
    Real old = value;
    value = ud.sampleValue(R.genrand_real3());
    //       }
    //     while(Min > value || value > Max);
    if(Min > value || value > Max)
      {
	propRatio = 0;
	value = old;
      }
    else
      {
	propRatio = value/old;
      }

    return value;
  }


  Real
  StdMCMCModel::perturbTruncatedNormal(Real value, Real width, StdMCMCModel::SuggestionAudacity within,
				       Real min, Real max, Probability& propRatio) const
  {
    Real smpl = -1;
    switch (within)
      {
      case FIVE_PCT:
	smpl = perturbTruncatedNormal(value, &GetSuggVarForFivePct, min, max, propRatio, width);
	break;
      case TWENTYFIVE_PCT:
	smpl = perturbTruncatedNormal(value, &GetSuggVarForTwentyFivePct, min, max, propRatio, width);
	break;
      case FIFTY_PCT:
	smpl = perturbTruncatedNormal(value, &GetSuggVarForFiftyPct, min, max, propRatio, width);
	break;
      case SEVENTYFIVE_PCT:
	smpl = perturbTruncatedNormal(value, &GetSuggVarForSeventyFivePct, min, max, propRatio, width);
	break;
      case NINETYFIVE_PCT:
	smpl = perturbTruncatedNormal(value, &GetSuggVarForNinetyFivePct, min, max, propRatio, width);
	break;
      default:
	break;
      }
    return smpl;
  }



  Real
  StdMCMCModel::perturbTruncatedNormal(Real value, Real (*varFunc)(Real, Real, Real, Real),
				       Real min, Real max, Probability& propRatio, Real hyper) const
  {
    static NormalDensity tnd(1.0, 1.0);

    assert(value > min && value < max);

    // Retrieve current variance by invoking function pointer.
    Real old = value;
    Real var = (*varFunc)(old, min, max, hyper);

    // Use current distribution. Get area when truncated tails excluded.
    tnd.setParameters(old, var);
    Probability nonTails = tnd.cdf(max) - tnd.cdf(min);

    unsigned cntr = 0;
    do
      {
	value = tnd.sampleValue(R.genrand_real3());
	++cntr;

	// Abort after 100 tries.
	if (cntr > 100)
	  {
	    propRatio = 0.0;
	    return old;
	  }
      }
    while (value <= min || value >= max);

    // Must compensate for removed tails to get true PDF.
    propRatio = 1.0 / (tnd(value) / nonTails);

    // Get new distribution.
    var = (*varFunc)(value, min, max, hyper);
    tnd.setParameters(value, var);
    nonTails = tnd.cdf(max) - tnd.cdf(min);

    // Must compensate for removed tails to get true PDF.
    propRatio *= (tnd(old) / nonTails);

    return value;
  }


  string
  StdMCMCModel::getAcceptanceInfo() const
  {
    std::ostringstream oss;
    if (n_params > 0)
      {
	oss << "# Acc. ratio for " << name << ": No info." << std::endl;
      }
    if (prior != NULL)
      {
	oss << prior->getAcceptanceInfo();
      }
    return oss.str();
  }


  //------------------------------------------------------------------
  //
  // Attributes
  //
  //------------------------------------------------------------------
  unsigned StdMCMCModel::unique(0); //!< Assigns unique numbers to name

}//end namespace beep
