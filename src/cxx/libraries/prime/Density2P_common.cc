#include "Density2P_common.hh"

#include "Beep.hh"
#include "Probability.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "GammaDensity.hh"
#include "UniformDensity.hh"

#include <assert.h>

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Class Density2P
  // Implemented base class (body) for probabilistic density functions 
  // that takes 2 parameters.
  //
  // Invariants: variance > 0; 
  //
  // Author: Bengt Sennblad, copyright: the MCMC-club, SBC.
  // All rights reserved.
  //
  //----------------------------------------------------------------------
  //----------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //----------------------------------------------------------------------
  Density2P_common::Density2P_common(Real mean, Real variance, 
				     const std::string& density)
    : alpha(mean),
      beta(variance),
      density(density),
      range(-Real_limits::max(), Real_limits::max()) 

  {}

  Density2P_common::Density2P_common(const Density2P_common& df)
    : alpha(df.alpha),
      beta(df.beta),
      density(df.density),
      range(df.range)
  {}

  Density2P *
  Density2P_common::createDensity(Real mean, Real variance, bool embedded, const std::string &density)
  {
    if(density == "INVG"){
      return new InvGaussDensity(mean, variance, embedded);
    }
    else if(density == "LOGN"){
      return new LogNormDensity(mean, variance, embedded);
    }
    else if(density == "GAMMA"){
      return new GammaDensity(mean, variance, embedded);
    }
    else if(density == "UNIFORM"){
      return new UniformDensity(mean, variance, embedded);
    }
    else{
      return 0;
    }
  }

  Density2P_common::~Density2P_common()
  {}

  Density2P_common& 
  Density2P_common::operator=(const Density2P_common& df)
  {
    if(&df != this)
      {
	alpha = df.alpha;
	beta = df.beta;
	density = df.density;
	range = df.range;
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
  // What density does the class handle?
  std::string 
  Density2P_common::densityName() const
  {
    return density;
  }

  Probability 
  Density2P_common::operator()(const Real& x, const Real& interval) const
  {
    return 1.0;
  }

  Probability 
  Density2P_common::pdf(const Real& x) const
  {
      throw AnError("pdf not yet implemented",1);
      return 0;
  }
  Probability 
  Density2P_common::cdf(const Real& x) const
  {
    throw AnError("cdf not yet implemented",1);
    return 0;
  }

  //! Returns current value of alpha.
  Real 
  Density2P_common::getAlpha() const
  {
    return alpha;
  }
  
  //! Returns current value of beta.
  Real 
  Density2P_common::getBeta() const
  {
    return beta;
  }
  
  // Sets embedded parameters of density (see doc for subclass).
  // Precondition: alpha and beta is within precision range.
  void 
  Density2P_common::setEmbeddedParameters(const Real& first, 
					  const Real& second)
  {
    // Check preconditions separate tests give more error info
    assert(-Real_limits::max() < first && first < Real_limits::max());   
    assert(-Real_limits::max() < second && second < Real_limits::max()); 

    alpha = first;
    beta = second;
    return setParameters(getMean(), getVariance());
  }

  // Sets embedded parameters alpha of density 
  // Postcondition: getMean()==mean and getVariance()==variance
  void 
  Density2P_common::setAlpha(const Real& newAlpha)
  {
    return setEmbeddedParameters(newAlpha, beta);
  }
  
  // Sets embedded parameter beta of density 
  // Postcondition: getMean()==mean and getVariance()==variance
  void 
  Density2P_common::setBeta(const Real& newBeta)
  {
    return setEmbeddedParameters(alpha, newBeta);
  }
  
  // value range - default uses precision range
  //------------------------------------------------------------------

  bool 
  Density2P_common::isInRange(const Real& x) const
  {
    return (range.first < x && x < range.second);
//     return (-Real_limits::max() < x && x < Real_limits::max());
  }

  void 
  Density2P_common::getRange(Real& min, Real& max) const
  {
    min = range.first;
    max = range.second;

//     min = -Real_limits::max();
//     max = Real_limits::max(); 
    return;
  }


    //! set user-defined range in which density has a non-zero probability.
    void 
    Density2P_common::setRange(const Real& min, const Real& max)
    {
      assert(min >= -Real_limits::max());
      assert(max <= Real_limits::max()); 
      range.first = min;
      range.second = max;
    }

  // parameter ranges default uses precision ranges
  //------------------------------------------------------------------

  void 
  Density2P_common::getMeanRange(Real& min, Real& max) const
  {
    min = range.first;
    max = range.second;
//     min = -Real_limits::max();
//     max = Real_limits::max();
    return;
  }

  void 
  Density2P_common::getVarianceRange(Real& min, Real& max) const
  {
    min = Real_limits::min();
    max = range.second;
//     max = Real_limits::max();
    return;
  }
  
}//end namespace beep
