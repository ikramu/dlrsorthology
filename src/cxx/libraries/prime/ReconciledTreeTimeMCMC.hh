#ifndef RECONCILEDTREETIMEMCMC_HH
#define RECONCILEDTREETIMEMCMC_HH

#include "BranchSwapping.hh"
#include "ReconciledTreeTimeModel.hh"
#include "StdMCMCModel.hh"

namespace beep
{

  //! MCMC interface for ReconciledTreeTimeModel. Perturbs the guest tree
  //! (branch swapping) and its divergence times. 
  class ReconciledTreeTimeMCMC 
    : public StdMCMCModel,
      public ReconciledTreeTimeModel
  {
  public:
    //-------------------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //-------------------------------------------------------------------
    ReconciledTreeTimeMCMC(MCMCModel& prior, Tree& G, StrStrMap& gs, 
			   BirthDeathProbs& bdp, Real suggestRatio = 1.0);
    ReconciledTreeTimeMCMC(MCMCModel& prior, Tree& G, StrStrMap& gs, 
			   BirthDeathProbs& bdp, std::string name,
			   Real suggestRatio = 1.0);
    ReconciledTreeTimeMCMC(MCMCModel& prior, Tree& G, StrStrMap& gs, 
			   BirthDeathProbs& bdp, 
			   Real minEdgeTime, 
			   bool fixRoot,
			   std::string name,
			   Real suggestRatio = 1.0);
    ~ReconciledTreeTimeMCMC();
    ReconciledTreeTimeMCMC(const ReconciledTreeTimeMCMC& rtm);
    ReconciledTreeTimeMCMC& operator=(const ReconciledTreeTimeMCMC&);
  
  public:  
    //-------------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------------
    void sampleTimes();
    void fixTimes();

    //-------------------------------------------------------------------
    // Interface inherited from StdMCMCModel
    //-------------------------------------------------------------------
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    //-------------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const ReconciledTreeTimeMCMC& A);
    std::string print() const;

  protected:
    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
    void sampleTimes(Node* u, Real maxT);
    MCMCObject perturbTime(Node& u);
    
  protected: 
    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
    bool estimateTimes;
    bool rootFixed;
    Real minEdgeTime;
    Real oldTime;
    Node* IdxNode;
  };
 
}// end namespace beep
#endif
