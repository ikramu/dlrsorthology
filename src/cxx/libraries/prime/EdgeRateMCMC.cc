#include "EdgeRateMCMC.hh"

#include "AnError.hh"
#include "MCMCObject.hh"


namespace beep
{
  //----------------------------------------------------------------------
  //
  // Class EdgeRateMCMC
  // Interface base class for MCMC-shells of EdgeRateModel subclasses.
  //
  //----------------------------------------------------------------------
  EdgeRateMCMC::EdgeRateMCMC(MCMCModel& prior, unsigned n_params_in,
			     const std::string& name, 
			     const Real& suggestRatio)
    : EdgeRateModel(),
      StdMCMCModel(prior, n_params_in, name, suggestRatio),
      oldValue(0),
      idx_limits(3, 1.0),
      idx_node(0),
      min(0),
      max(0),
      suggestion_variance(0.1), // This is reset in EdgeRateMCMC_common.hh
      accPropCnt(0, 0)
  {
  };
  
  EdgeRateMCMC::EdgeRateMCMC(const EdgeRateMCMC& erm)
    : EdgeRateModel(erm),
      StdMCMCModel(erm), 
      oldValue(erm.oldValue),
      idx_limits(erm.idx_limits),
      idx_node(erm.idx_node),
      min(erm.min),
      max(erm.max),
      suggestion_variance(erm.suggestion_variance),
      accPropCnt(erm.accPropCnt)
  {};

  EdgeRateMCMC& 
  EdgeRateMCMC::operator=(const EdgeRateMCMC& erm)
  {
    if(&erm != this)
      {
	EdgeRateModel::operator=(erm);
	StdMCMCModel::operator=(erm);
	oldValue = erm.oldValue;
	idx_limits = erm.idx_limits;
	idx_node = erm.idx_node;
	min = erm.min;
	max = erm.max;
	suggestion_variance = erm.suggestion_variance;
	accPropCnt = erm.accPropCnt;
      }
    return *this;
  };

  EdgeRateMCMC::~EdgeRateMCMC()
  {};

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // fix parameters
  //----------------------------------------------------------------------
  void  
  EdgeRateMCMC::fixMean()
  {
    if( idx_limits[0] != 0)                      // check if already fixed
      {
	idx_limits[0] = 0.0;                     // set perturb prob = 0
	n_params--;                              // less params to perturb
	update_idx_limits();                     // tell StdMCMCModel
      }
    return;
  };

  void  
  EdgeRateMCMC::fixVariance()
  {
    if( idx_limits[1] != 0)                      // check if already fixed
      {
	idx_limits[1] = 0;                       // set perturb prob = 0
	n_params--;                              // less params to perturb
	update_idx_limits();                     // tell StdMCMCModel
      }
    return;
  };

  void  
  EdgeRateMCMC::setMax(const Real& newMax)
  {
    max = newMax;
  }
    
  //----------------------------------------------------------------------
  // Inherited from StdMCMCModel
  //----------------------------------------------------------------------

  // Perturbs with equal probability either the mean or the variance of 
  // underlying density or one of the edge rates.
  MCMCObject  
  EdgeRateMCMC::suggestOwnState()
  {
	  ++accPropCnt.second;

    // Create return object
    MCMCObject MOb(1.0, 1.0);
    
    // First transform paramIdx to local parameter space
    Real Idx = paramIdx / paramIdxRatio; 

    if(Idx < idx_limits[0])              // mean
      {
	oldValue = getMean();            // gbm treats mean differently
	setMean(perturbLogNormal(oldValue, suggestion_variance,
				 min, max, MOb.propRatio));
// 	// adjust all rates to new context
// 	adjustRates(getMean()/oldValue);
      }
    else if(Idx < idx_limits[1])         // variance
      {
	oldValue = getVariance();        // gbm treats mean differently
	setVariance(perturbLogNormal(oldValue, suggestion_variance,
				  Real_limits::min(), max,
				  MOb.propRatio));
      }
    else                                 // rates
      {
	assert(idx_limits[2] != 0);      // check for sane idx_limits 
  
	MOb.propRatio = perturbRate();   // Const treat rates differently

	//	writeLengthsToTree();
     }
      
    // Since any perturbation might have changed likelihood we must update it
    MOb.stateProb = updateDataProbability();
    return MOb;
  }

  // Perturbs with equal probability either the mean or the variance of
  // underlying density or one of the edge rates.
  MCMCObject  
  EdgeRateMCMC::suggestOwnState(unsigned x)
  {
	  ++accPropCnt.second;

    // Create return object
    MCMCObject MOb(1.0, 1.0);
    
    // First transform paramIdx to local parameter space
    Real Idx = paramIdx / paramIdxRatio; 

    if(Idx < idx_limits[0])              // mean
      {
	oldValue = getMean();            // gbm treats mean differently
	setMean(perturbLogNormal(oldValue, suggestion_variance,
				 min, max, MOb.propRatio));
// 	// adjust all rates to new context
// 	adjustRates(getMean()/oldValue);
      }
    else if(Idx < idx_limits[1])         // variance
      {
	oldValue = getVariance();        // gbm treats mean differently
	setVariance(perturbLogNormal(oldValue, suggestion_variance,
				  Real_limits::min(), max,
				  MOb.propRatio));
      }
    else                                 // rates
      {
	assert(idx_limits[2] != 0);      // check for sane idx_limits   

// 	unsigned x = R.genrand_modulo(n_params - (idx_limits[0] == 0 ? 0 : 1) - (idx_limits[1] == 0 ? 0 : 1));
// 	MOb.propRatio = perturbRate(x);   // Const treat rates differently
	unsigned perturbEdge = (x - (idx_limits[0] == 0 ? 0 : 1) - (idx_limits[1] == 0 ? 0 : 1));
	MOb.propRatio = perturbRate(perturbEdge);   // Const treat rates differently
	writeLengthsToTree();
     }
      
    // Since any perturbation might have changed likelihood we must update it
    MOb.stateProb = updateDataProbability();
    return MOb;
  }

  void  
  EdgeRateMCMC::commitOwnState()
  {
	  ++accPropCnt.first;
  }

  void
  EdgeRateMCMC::commitOwnState(unsigned x)
  {
	  ++accPropCnt.first;
  }

  void  
  EdgeRateMCMC::discardOwnState()
  {
    // First transform paramIdx to local parameter space
    Real Idx = paramIdx / paramIdxRatio;
    if(Idx < idx_limits[0])             // mean
      {
// 	// reset all rates to old context
// 	adjustRates(oldValue/getMean());
	setMean(oldValue);        // gbm trets mean different
      }
    else if(Idx < idx_limits[1])        // variances
      {
 	setVariance(oldValue);    // gbm treats mean different
      }
    else                                // rates
      {
	assert(idx_limits[2] != 0); // check for sanity of idx_limits   
 
	setRate(oldValue, idx_node);
#ifdef PERTURBED_NODE
	resetPerturbedNode();
#endif
      }
    return;
  };  

  void
  EdgeRateMCMC::discardOwnState(unsigned x)
  {
    // First transform paramIdx to local parameter space
    Real Idx = paramIdx / paramIdxRatio;
    if(Idx < idx_limits[0])             // mean
      {
// 	// reset all rates to old context
// 	adjustRates(oldValue/getMean());
	setMean(oldValue);        // gbm trets mean different
      }
    else if(Idx < idx_limits[1])        // variances
      {
 	setVariance(oldValue);    // gbm treats mean different
      }
    else                                // rates
      {
	assert(idx_limits[2] != 0); // check for sanity of idx_limits   
 
	setRate(oldValue, idx_node);
#ifdef PERTURBED_NODE
	resetPerturbedNode();
#endif
      }
    return;
  };  

  std::string  
  EdgeRateMCMC::ownStrRep() const
  {
    std::ostringstream oss;
    if(idx_limits[0] != 0)
      {
	oss << getMean() << ";\t";
      }
    if(idx_limits[1] != 0)
      {
 	oss << getVariance() << ";\t";
      }
    if(idx_limits[2] != 0)
      {
	oss << ratesStr();
      }
    return oss.str();
  };

  std::string  
  EdgeRateMCMC::ownHeader() const
  {
    std::ostringstream oss;
    if(idx_limits[0] != 0)
      {
	oss << "mean(float);\t";
      }
    if(idx_limits[1] != 0)
      {
 	oss << "variance(float);\t";
      }
    if(idx_limits[2] != 0)
      {
	oss << ratesHeader();
      }
    return oss.str();
  };

  std::string
  EdgeRateMCMC::getAcceptanceInfo() const
  {
  	std::ostringstream oss;
  	if (n_params > 0)
  	{
  		oss << "# Acc. ratio for " << name << ": "
  			<< accPropCnt.first << " / " << accPropCnt.second << " = "
  			<< (accPropCnt.first / (Real) accPropCnt.second) << std::endl;
  	}
  	if (prior != NULL)
  	{
  		oss << prior->getAcceptanceInfo();
  	}
  	return oss.str();
  }

  Probability  
  EdgeRateMCMC::updateDataProbability()
  {
    update();
    return calculateDataProbability();
  };

  //----------------------------------------------------------------------
  //
  // I/O
  //
  //----------------------------------------------------------------------
  std::string  
  EdgeRateMCMC::print() const
  {
    std::ostringstream oss;
    oss  << StdMCMCModel::print()
      ;
    return oss.str();
  };

  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------
  void   
  EdgeRateMCMC::update_idx_limits()
  {
    Real par = 0.0;
    if(idx_limits[0] != 0)
      {
	idx_limits[0] = (par+=1.0) / n_params; 
      }
    if(idx_limits[1] != 0)
      {
	idx_limits[1] = (par+=1.0) / n_params;   
      }
    if(idx_limits[2] != 0)
      {
	idx_limits[2] = 1.0;//static_cast<Real>(n_params) / n_params;      
      }
    updateParamIdx();
    return;
  }
    

}//end namespace beep

