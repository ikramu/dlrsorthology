#include "SubstitutionMCMC.hh"

#include "MCMCObject.hh"

namespace beep
{
  using namespace std;

  //---------------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  //
  //---------------------------------------------------------------------
  SubstitutionMCMC::SubstitutionMCMC(MCMCModel& prior,
				     const SequenceData& Data, 
				     const Tree& T,
				     SiteRateHandler& siteRates_in,
				     const TransitionHandler& Q,
				     EdgeWeightHandler& edgeWeights,
				     const vector<string>& partitionsList)
#ifdef PERTURBED_NODE
#ifdef PERTURBED_TREE
    : FastCacheSubstitutionModel(Data, T, siteRates_in, Q, edgeWeights, partitionsList),
#else
    : CacheSubstitutionModel(Data, T, siteRates_in, Q, edgeWeights, partitionsList),
#endif
#else
    : SubstitutionModel(Data, T, siteRates_in, Q, edgeWeights, partitionsList),
#endif
      StdMCMCModel(prior, 0, "SubstModel", 0),
      accPropCnt(0, 0)
  {
  }

  SubstitutionMCMC::SubstitutionMCMC(SubstitutionMCMC& SM)
#ifdef PERTURBED_NODE
#ifdef PERTURBED_TREE
    : FastCacheSubstitutionModel(SM),
#else
    : CacheSubstitutionModel(SM),
#endif
#else
    : SubstitutionModel(SM),
#endif
      StdMCMCModel(SM),
      accPropCnt(SM.accPropCnt)
  {
  }

  SubstitutionMCMC::~SubstitutionMCMC()
  {
  }
  
  SubstitutionMCMC&
  SubstitutionMCMC::operator=(const SubstitutionMCMC& SM)
  {
    if(&SM != this)
      {
#ifdef PERTURBED_NODE
#ifdef PERTURBED_TREE
	FastCacheSubstitutionModel::operator=(SM);
#else
	CacheSubstitutionModel::operator=(SM);
#endif
#else
	SubstitutionModel::operator=(SM);
#endif
	StdMCMCModel::operator=(SM);
	accPropCnt = SM.accPropCnt;
      }
    return *this;
  }

  //---------------------------------------------------------------------
  //
  // Interface from StdMCMCModel
  //
  //---------------------------------------------------------------------
  // We do not have any parameters to perturb
  //---------------------------------------------------------------------
  MCMCObject 
  SubstitutionMCMC::suggestOwnState()
  {
    //    update(); // for nested models, e.g., siteRates and edgeweights
    return MCMCObject(updateDataProbability(), 1.0);
  }
  
  void
  SubstitutionMCMC::commitOwnState()
  {
  }
  
  void
  SubstitutionMCMC::discardOwnState()
  {
  }
  
  string
  SubstitutionMCMC::ownStrRep() const
  {
    ostringstream oss;
    oss << like << ";\t"; //stateProb / prior->currentStateProb() << ";\t"; 
#ifdef SHOWPROBS
    oss << prior->currentStateProb() << ";\t";
#endif
    return oss.str();
  }
  
  string
  SubstitutionMCMC::ownHeader() const
  {
#ifdef SHOWPROBS
      return "substLike(logfloat);\tpriorLike(logfloat);\t";
#else
      return "substLike(logfloat);\t";
#endif
  }
  
  string
  SubstitutionMCMC::getAcceptanceInfo() const
    {
    	std::ostringstream oss;
    	if (n_params > 0)
    	{
    		oss << "# Acc. ratio for " << name << ": "
    			<< accPropCnt.first << " / " << accPropCnt.second << " = "
    			<< (accPropCnt.first / (Real) accPropCnt.second) << endl;
    	}
    	if (prior != NULL)
    	{
    		oss << prior->getAcceptanceInfo();
    	}
    	return oss.str();
    }


  Probability
  SubstitutionMCMC::updateDataProbability()
  {
    update(); 

#ifdef PERTURBED_NODE
    Probability ret;
    if(T->perturbedNode())
      {
	if(T->perturbedNode()->isRoot()) // We must recalculate for whole tree
	  {
	    ret = calculateDataProbability();
	  }
	else  // Recalculate everything between T.perturbedNode and root
	  {
	    ret = calculateDataProbability(T->perturbedNode());
	  }
      }
    else // Nothing to recalculate - return previous value
      {
	ret = like;
      }
    return ret;
#else
    return calculateDataProbability();
#endif
  }

  //----------------------------------------------------------------------
  //
  // I/O
  // I have not converged on a consensus for this yet, but the 
  // following functions should suffice for the final product
  //
  //----------------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const SubstitutionMCMC& A)
  {
    return o << A.print();
  }
  string 
  SubstitutionMCMC::print() const
  {
    ostringstream oss;
    oss << name << ": ";
#ifdef PERTURBED_NODE
#ifdef PERTURBED_TREE
    oss << FastCacheSubstitutionModel::print(true)
#else
    oss << CacheSubstitutionModel::print(true)
#endif
#else
    oss << SubstitutionModel::print(true)
#endif
	<< StdMCMCModel::print()
	<< "\n"
      ;
    return oss.str();
  }
  
}//end namespace beep
