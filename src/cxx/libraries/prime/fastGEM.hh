#ifndef FAST_GEM_HH
#define FAST_GEM_HH

#include <algorithm>
#include <vector>

#include "Beep.hh"
#include "fastGEM_BirthDeathProbs.hh"
#include "Density2P.hh"
#include "EdgeRateModel_common.hh"
#include "GammaMap.hh"
#include "GenericMatrix.hh"
#include "Generic3DMatrix.hh"
#include "LambdaMap.hh"
#include "LengthRateModel.hh"
#include "Node.hh"
#include "NodeMap.hh"
#include "NodeNodeMap.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "StrStrMap.hh"
#include "Tree.hh"
#include "VarRateModel.hh"

namespace beep
{
  //------------------------------------------------------------------------
  //
  //! Implements the algorithm described in "A fast implementation of the
  //! Gene Evolution Model" (text in writing). 
  //! Key features are:
  //! 1. The discretization of the planted species tree. We use 
  //! noOfDiscrIntervals discretization steps between leaves and root and
  //! equally many from root to the top of the planted tree. Discretization 
  //! points are denoted x,y,z,... Speciation points (Snodes) are not 
  //! discretization points...
  //! 2. Matrices Sa, Lb, Lt. Here Sa(x,u) holds the probability the genetree
  //! G^u has evolved from p(x) to x, the event u has occurred on x and that
  //! we then continue correctly; Lb(x,u,i), b for below,  holds the 
  //! probability that G_u starts at p(x)and  u occurrs on i.
  //! The value stored in Lt(x,u,i) is the corresponding time.
  //! 3. The matrix P11dup and vector P11spec (constructed in class
  //! fastGEM_BirthDeathProbs. P11dup(Snode,x) holds the probability for 
  //! evolving from p(x) to x where x is between Snode and its parent. 
  //! P11spec(Snode) is for evolving from a speciation point to its parent.
  //------------------------------------------------------------------------
  class fastGEM : public iidRateModel
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //-------------------------------------------------------------  
    fastGEM(Tree& G, Tree& S, StrStrMap &gs, Density2P* df, fastGEM_BirthDeathProbs& fbdp, std::vector<double>* discrPoints, const unsigned noOfDiscrPoints);
    ~fastGEM();
  
    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
  public:
    Probability calculateDataProbability();

    Probability calcSumProb(unsigned gRootIndex);
    unsigned calcMaxProb(unsigned gRootIndex);
    void backTrace(unsigned uIndex, unsigned uTime);
    Probability getRootLbValue(unsigned xIndex, unsigned gRootIndex, unsigned iIndex);
    void update();

    //---------------------------------------------------------------------
    //
    // Implementation
    //
    //---------------------------------------------------------------------
    
  private:
    void reconcileRecursively(unsigned uIndex);
    void setSaValue(unsigned xIndex, unsigned uIndex, Probability p);
    void setLbValue(unsigned xIndex, unsigned uIndex, unsigned iIndex, Probability p);
    void setLtValue(unsigned xIndex, unsigned uIndex, unsigned iIndex, Real t);
    void setPointers(unsigned xIndex, unsigned uIndex, unsigned lVal, unsigned rVal);
    Probability getSaValue(unsigned xIndex, unsigned uIndex);
    Probability getLbValue(unsigned xIndex, unsigned uIndex, unsigned iIndex);
    Real getLtValue(unsigned xIndex, unsigned uIndex, unsigned iIndex);
    unsigned getLeftPointer(unsigned uIndex, unsigned uTime);
    unsigned getRightPointer(unsigned uIndex, unsigned uTime);
    Probability calcLb(unsigned Sindex, unsigned xIndex, Node* u, unsigned iIndex);
    Real calcLt(unsigned Sindex, unsigned xIndex, Node* u, unsigned iIndex);
    Probability calcSa(unsigned Sindex, unsigned xIndex, Node* u);
    Probability calcSaWithLoss(unsigned Sindex, unsigned xIndex, Node* u);
    void updateSpeciesTreeDependent();
    void updateGeneTreeDependent();
    unsigned getDiscrPtBelowSnode(unsigned Sindex);
    unsigned getDiscrPtAboveSnode(unsigned Sindex);
    unsigned getSpecPtBelowDiscrPt(unsigned xIndex, unsigned uIndex);
    void fillDiscrPtBelowAboveTables();
    void fillSpecPtBelowTable();
    
    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    void printSa();
    void printLb();
    void printLt();

    std::string print() const;
    
  private:
    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
    // External attributes
    Tree& G;
    Tree& S;
    StrStrMap& gs;
    Density2P* df;
    fastGEM_BirthDeathProbs& fbdp;

    // Internal attributes
    Real& birthRate;
    unsigned noOfSNodes;
    unsigned noOfGNodes;
    unsigned noOfDiscrPoints;
    std::vector<double>* discrPoints; 

    GenericMatrix<Probability> Sa; //!< Pr[G^u starts on p(x) & u occurs on x]
    Generic3DMatrix<Probability> Lb;//!<Pr[G_u starts on p(x) & u occurs on x]
    Generic3DMatrix<Real> Lt;       //!< Corresponding time
    GenericMatrix<unsigned> SaLeft;
    GenericMatrix<unsigned> SaRight;

    LambdaMap lambda;
    Real timeStep;
    std::vector<unsigned>* discrPtBelowSnode; 
    std::vector<unsigned>* discrPtAboveSnode; 
    GenericMatrix<unsigned> specPtBelowDiscrPt;
    bool withRates;
    bool longRootEdge;
  };
}//end namespace beep
#endif
