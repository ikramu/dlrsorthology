#include <cassert>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "HybridGuestTreeModel.hh"
#include "Probability.hh"
#include "StrStrMap.hh"
#include "HybridTree.hh"
#include "Node.hh"


namespace beep
{
  using namespace std;

  //------------------------------------------------------------------------
  //
  //
  // class HYBRIDGUESTTREEMODEL
  //
  //
  //------------------------------------------------------------------------

  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

  HybridGuestTreeModel::HybridGuestTreeModel(Tree &G_in, 
					     HybridTree& S_in,
					     StrStrMap &gs_in,
					     BirthDeathProbs &bdp_in)
    : S(&S_in),
      G(&G_in),
      gs(&gs_in),
      bdp(&bdp_in),
      S_A(*S, G_in),
      S_X(*S, G_in),
      doneSA(*S, G_in),
      doneSX(*S, G_in),
      doneSlice(*S),
      slice_U(*G),
      slice_L(*S, *G,0),
      isomorphy(*G, 1)
  {
    update();		   // Compute important helper params
  }

  HybridGuestTreeModel::~HybridGuestTreeModel()
  {
    //  delete gamma;
  }

  HybridGuestTreeModel::HybridGuestTreeModel(const HybridGuestTreeModel &M)
    : S(M.S),
      G(M.G),
      gs(M.gs),
      bdp(M.bdp),
      S_A(M.S_A),
      S_X(M.S_X),
      doneSA(M.doneSA),
      doneSX(M.doneSX),
      doneSlice(M.doneSlice),
      slice_U(M.slice_U),
      slice_L(M.slice_L),
      isomorphy(M.isomorphy)
  {
    update();
  }

   HybridGuestTreeModel &
  HybridGuestTreeModel::operator=(const HybridGuestTreeModel &M)
  {
    if (this != &M)
      {
	S = M.S;
	G = M.G;
	gs = M.gs;
	bdp = M.bdp;
	S_A = M.S_A;
	S_X = M.S_X;
	doneSA = M.doneSA;
	doneSX = M.doneSX;
	doneSlice = M.doneSlice;
	slice_U= M.slice_U;
	slice_L = M.slice_L;
	isomorphy = M.isomorphy;
      }
    update();
    return *this;
  }


  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------

  // Updating stuff when the gene tree has changed /this is cheap so O.K. /bens
  //-------------------------------------------------------------------------
  void
  HybridGuestTreeModel::update()
  {
    // Reset values
    doneSlice = NodeMap<unsigned>(*S, 1); 
    slice_L = NodeNodeMap<unsigned>(*S, *G, 0);
    slice_U = NodeMap<unsigned>(*G);
    computeSlice(*S->getRootNode());
    isomorphy = UnsignedVector(*G, 1);
    computeIsomorphy(*G->getRootNode());
  }


  //------------------------------------------------------------------------
  // Calculations and computations
  //------------------------------------------------------------------------

  // Since calculateDataProbability often is called both in e.g., 
  // ReconciliationSampler and in, e.g., GeneTreeMCMC, we get redundant 
  // calculations done in a single MCMC-iteartions. Thus, maybe we should 
  // include a flag showing if the function needs to be recalculated. 
  // I leave it for now, though. /bens
  //------------------------------------------------------------------------
  Probability
  HybridGuestTreeModel::calculateDataProbability()
  {
    // Clear old values - Stupid way of doing it?
    doneSA = doneSX = NodeNodeMap<unsigned>(*S,*G, 1);
    // Needed since we actually recreate S
    if(S->perturbedNode() == S->getRootNode())
      {
	S_A = NodeNodeMap<Probability>(*S, *G);
	S_X = NodeNodeMap< std::vector<Probability> >(*S, *G);
      }

    Node& rootS = *S->getRootNode();
    Node& rootG = *G->getRootNode();
    
    // Compute the S_A and S_X structures
    computeSA(rootS,rootG); 
    return S_A(rootS, rootG);
  }

  //-------------------------------------------------------------------
  //
  // I/O
  //
  //-------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const HybridGuestTreeModel& pm)
  {
    return o << pm.print();
  };
  
  string 
  HybridGuestTreeModel::print() const
  {
    std::ostringstream oss;
    oss << "HybridGuestTreeModel: Computes the likelihood of a gene.\n"
	<< "tree given a species network, by summing over all \n"
	<< "reconciliations.\n"
	<< indentString(G->getName() + " (guest tree)\n");
    return oss.str();
  }
  
  
  // Debugging support macros
  //-------------------------------------------------------------------
#ifdef DEBUG_DP
#define DEBUG_SX(S,X,U,K) {cerr << S << ":\tS_X(" << X.getNumber() << ", " << U.getNumber() << ", " << K <<  ") = " << S_X(X,U)[K-1].val() << endl;}
#define DEBUG_SA(S,X,U) {cerr << S << ":\tS_A(" << X.getNumber() << ", " << U.getNumber() << ") = " << S_A(X,U).val() << endl;}

#define DEBUG_S_X(S,X,U,K) {cerr << S << ":\tS_X(" << X->getNumber() << ", " << U->getNumber() << ", " << K <<  ") = " << S_X(X,U)[K-1].val() << endl;}
#define DEBUG_S_A(S,X,U) {cerr << S << ":\tS_A(" << X->getNumber() << ", " << U->getNumber() << ") = " << S_A(X,U).val() << endl;}
#else
#define DEBUG_SX(S,X,U,K)
#define DEBUG_SA(S,X,U)

#define DEBUG_S_X(S,X,U,K)
#define DEBUG_S_A(S,X,U)
#endif



  //-----------------------------------------------------------------------
  //
  // Implementation
  //
  //-----------------------------------------------------------------------
  // computes \f$ s_V(x,u) \f$
  // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
  //-----------------------------------------------------------------
  void 
  HybridGuestTreeModel::computeSV(Node& x, Node& u)
  {
    assert(slice_L(x,u) > 0); //check precondition

    if(x.isLeaf())
      {
	assert(u.isLeaf());
	S_X(x,u)[0] = 1.0;
      }
    else
      {
	// First handle case when u maps below x
	Node& y = *x.getLeftChild();
	Node& z = *x.getRightChild();

	computeSA(y,u);
	computeSA(z,u);
	S_X(x,u)[0] = 
	  S_A(y,u) * bdp->partialProbOfCopies(z, 0) + //2 var. of u as dupl.
	  S_A(z,u) * bdp->partialProbOfCopies(y, 0);
	// Then the cases when u is a speciation at x 
	if(u.isLeaf() == false)
	  {
	    Node& v = *u.getLeftChild();
	    Node& w = *u.getRightChild();
	    
 	    computeSA(y,v);
 	    computeSA(z,w);
 	    computeSA(y,w);
 	    computeSA(z,v);
	    S_X(x,u)[0] += 
	      S_A(y,v) * S_A(z,w) +                  // 2 var. of u as spec.
	      S_A(y,w) * S_A(z,v);
	  }
      }
    DEBUG_SX("ComputeSX", x, u, 1);
  }

  // computes \f$ s_A(x,u) \f$
  //-----------------------------------------------------------------
  void 
  HybridGuestTreeModel::computeSA(Node& x, Node& u)
  {
//     cerr << "Entering coputeSA(" << x.getNumber() << ", " << u.getNumber() << ")\n"; 
    // Check if S_A(x,u) is already filled in...
    if(doneSA(x,u) == 0)
      {
	return;
      }
    // ...else fill it in and set flag correspondingly
    doneSA(x,u) = 0;

    Probability p = 0;

    // if u \in S_x, then s_A(x,u) = \sum_k\in[|L(G:U)|}Q_x(k)s_X(x,u,k)   
    if(slice_L(x,u) > 0)
      {
	computeSX(x,u);
	
	for(unsigned k = slice_L(x, u); k <= slice_U[u]; k++) 
	  {
	    if(x.isRoot())
	      {
		p += S_X(x,u)[k-1] * bdp->topPartialProbOfCopies(k);	  
	      }
	    else
	      {
		p += S_X(x,u)[k-1] * bdp->partialProbOfCopies(x, k);	  
	      }
	  }
      }
    // if u \not\in S_x, then s_A(x,u) = X_A(x)
    else
      {
 	p = 0;
      }
    
    S_A(x,u) = p;
    DEBUG_SA("ComputeSA", x, u);
  }
  
  // computes \f$ s_X(x,u,k), k\in[|L(G_u)|] \f$
  // precondition: slice_L(x,u) > 0
  //-----------------------------------------------------------------
  void 
  HybridGuestTreeModel::computeSX(Node& x, Node& u)
  {
    assert(slice_L(x,u) > 0);
    
    // Check if S_A(x,u) is already filled in...
    if(doneSX(x,u) == 0)
      {
	return;
      }
    // ...else fill it in and set flag correspondingly
    doneSX(x,u) = 0;

    unsigned U = slice_U[u];
    unsigned L = slice_L(x, u);
    S_X(x, u).assign(U, 0); // all elements initiated to 0
    
    if(L == 1)
      {
	computeSV(x,u);
      }
    if(&u != orthoNode)
      {
	for(unsigned k = std::max(2u, L); k <= U; k++) 
	  {	
	    Probability sum(0.0);
	    
	    Probability factor(1.0 / (k-1));

	    // Multiply factor with 2 if needed
	    adjustFactor(factor, u);

	    // Loop through all valid solutions to k1 + k2 = k and sum
	    // $S_X(x, left, k1)S_X(x, right, k2)$
	    Node& v = *u.getLeftChild();
	    Node& w = *u.getRightChild();
	    computeSX(x,v);
	    computeSX(x,w);
	    
	    unsigned Lv  = slice_L(&x,&v);
	    unsigned Uv  = slice_U[&v];
	    unsigned Lw = slice_L(&x,&w);
	    unsigned Uw = slice_U[&w];
	    assert(Lv > 0);
	    assert(Lw > 0);
	    for (unsigned k1 = Lv; k1 <= Uv; k1++)
	      {
		unsigned k2 = k - k1;
		if (k2 <= Uw && k2 >= Lw) 
		  {
		    sum +=  S_X(x,v)[k1-1] * S_X(x,w)[k2-1]; 
		  }
	      }
	    S_X(x, u)[k - 1] = factor * sum;
	    DEBUG_SX("ComputeSX", x, u, k);
	  }
      }
    return;
  }

  //! multiplies with two if subtrees are non-isomorphic
  //------------------------------------------------------------------------
  void 
  HybridGuestTreeModel::adjustFactor(Probability& factor, Node& u)
  {
    if(isomorphy[u] == 1)
      {
	factor *= 2;
      }
    return;
  }	      


  // For all x in S and u in G precompute slice_U(u) and slice_L(x,u), 
  // slice_L(x,u)also indicate whether u can pass <p(x),x> during its
  // evolution inside S
  //------------------------------------------------------------------------
  void 
  HybridGuestTreeModel::computeSlice(Node& x)
  {
    // Check if this node is already done
    if(doneSlice[x] == 0)
      {
	return;
      }
    // Otherwise get mark it and do it
    doneSlice[&x] = 0;
    if(x.isLeaf())
      {
	sliceRecurseG(x, *G->getRootNode());
      }
    else
      {
	Node& y = *x.getLeftChild();
	Node& z = *x.getRightChild();
	
	computeSlice(y);
	computeSlice(z);
	sliceRecurseG(x, *G->getRootNode());
      }
  }

  void 
  HybridGuestTreeModel::sliceRecurseG(Node& x, Node& u)
  {
    if(S->isExtinct(x))
      {
	slice_L(x,u) = 0;
      }
    if(u.isLeaf())
      {
	if(x.isLeaf())
	  {
	    if(gs->find(u.getName()) == x.getName())
	      {
		slice_L(x,u) = 1;
	      }
	    else
	      {
		slice_L(x,u) = 0;
	      }	   
	  }
	else
	  {
	    Node& y = *x.getLeftChild();
	    Node& z = *x.getRightChild();
	    slice_L(x,u) = max(slice_L(y, u), slice_L(z, u));
	  }	    
      }
    else
      {
	Node& v = *u.getLeftChild();
	Node& w = *u.getRightChild();
	sliceRecurseG(x, v);
	sliceRecurseG(x, w);
	if(slice_L(x, v) * slice_L(x, w) == 0)
	  {
	    slice_L(x,u) = 0;
	  }
	else if(x.isLeaf() || slice_L(x, v) * slice_L(x, w) > 1)
	  {	    
	    slice_L(x,u) = slice_L(x, v) + slice_L(x, w);
	  }
	else
	  {
	    slice_L(x,u) = 1;
	  }
      }
    //Compute slice_u
    if(x.isRoot())
      {
	if(u.isLeaf())
	  {
	    slice_U[u] = 1;
	  }
	else
	  {
	    slice_U[u]=slice_U[*u.getLeftChild()]+slice_U[*u.getRightChild()]; 
	  }
      }
//     cerr << "slice_L("<<x.getNumber()<<","<<u.getNumber() <<") = "<< slice_L(x,u)<< endl;
  }
  
  
  // Fill isomorphy with the result of 
  // recursiveIsomorphy(Node& v, Node& w)
  void
  HybridGuestTreeModel::computeIsomorphy(Node& u)
  {
    if(u.isLeaf()) 
      {
	isomorphy[&u] = 1;
	return;
      } 
    else 
      {
	Node& v = *u.getLeftChild();
	Node& w = *u.getRightChild();
	if(recursiveIsomorphy(v, w)) 
	  {
	    isomorphy[&u] = 0;
	  }
	computeIsomorphy(v);
	computeIsomorphy(w);
      }
  }
  
  bool
  HybridGuestTreeModel::recursiveIsomorphy(Node& v, Node& w)
  {
    if(v.isLeaf() && w.isLeaf()) 
      {
	if(gs->find(v.getName()) == gs->find(w.getName()))
	  {
	    return true;
	  } 
	else 
	  {
	    return false;
	  }
      } 
    else if(v.isLeaf() || w.isLeaf()) 
      {
	return false;
      } 
    else
      {
	Node& v_left  = *v.getLeftChild();
	Node& v_right = *v.getRightChild();
	Node& w_left  = *w.getLeftChild();
	Node& w_right = *w.getRightChild();
	
	if(recursiveIsomorphy(v_left, w_left)
	    && recursiveIsomorphy(v_right, w_right))
	  {
	    return true;
	  } 
	else if(recursiveIsomorphy(v_left, w_right)
		&& recursiveIsomorphy(v_right, w_left))
	  {
	    return true;
	  }
	else
	  {
	    return false;
	  }
      }
    
  }
  


}// end namespace beep
