#ifndef EDGEDISCTREE_HH
#define EDGEDISCTREE_HH

#include "EdgeDiscPtMaps.hh"
#include "PerturbationObservable.hh"
#include "TreeDiscretizers.hh"

namespace beep
{

/**
 * A point map representing an "edge-discretized" tree.
 * Instances are created using factory methods of subclasses
 * of EdgeDiscretizer. See that class for more info.
 *
 * Every edge contains at least three points (save for the top time edge
 * in case it's empty and will only have the root).
 *
 * Indexing is based on Node instances, but user's should never mix
 * time values of those (or the original Tree) with the discretized
 * values since they may differ slightly.
 *
 * See also the base class EdgeDiscPtMap.
 *
 * @author Joel Sj�strand.
 */
class EdgeDiscTree : public beep::EdgeDiscPtMap<Real>, public PerturbationObservable
{

public:

	/**
	 * @param S the tree on which map is based.
	 * @param discretizer the object dictating the discretization approach.
	 */
	EdgeDiscTree(Tree& S, EdgeDiscretizer* discretizer);


	/**
	 * Destructor.
	 */
	virtual ~EdgeDiscTree();

	/**
	 * Copy-constructor.
	 * @param eds object to be copied.
	 */
	EdgeDiscTree(const EdgeDiscTree& eds);

	/**
	 * Assignment operator.
	 * @param eds object to be copied.
	 * @return new copy.
	 */
	EdgeDiscTree& operator=(const EdgeDiscTree& eds);


	/**
	 * Recreates the discretization. Invoke after a change to S.
	 */
	void rediscretize();
        
        /**
	 * Returns true if the given point p is a speciations, otherwise
	 * false.
	 *
	 * @param p - A discretization point.
	 */
	bool isSpeciation(const EdgeDiscretizer::Point &p);

	/**
	 * Recreates the discretization for edges surrounding an interior node.
	 * Invoke this when an interior node has been moved time-wise.
	 * @param n the node which has been moved time-wise.
	 */
	void rediscretizeNode(const Node* n);

	/**
	 * Overrides the method of the subclass. Invokes parameter-less rediscretize().
	 * @param defaultVal has no effect.
	 */
	virtual void rediscretize(const Real& defaultVal)
	{
		rediscretize();
	}

	/**
	 * Overrides the method of the subclass. Has no effect.
	 * @param defaultVal has no effect.
	 */
	virtual void reset(const Real& defaultVal)
	{
	}

	/**
	 * Returns the timestep used on a certain edge.
	 * Note that the segments closest to the ends are only
	 * half that size.
	 * @param the lower end of the edge.
	 * @return the timestep used on the edge.
	 */
	Real getTimestep(const Node* node) const
	{
		return m_timesteps[node];
	}

	/**
	 * Returns the smallest timestep of any edge in the tree
	 * (an empty top time edge discarded).
	 * @return the smallest timestep.
	 */
	Real getMinTimestep() const;

	/**
	 * Returns the underlying tree.
	 * @return the node.
	 */
// 	inline Tree& getTree() const
  Tree& getTree() const
	{
		return *m_S;
	}

	/**
	 * Returns a specified node.
	 * @param number the node's ID.
	 * @return the node.
	 */
	inline const Node* getNode(unsigned number) const
	{
		return m_S->getNode(number);
	}

	/**
	 * Returns the root node.
	 * @return the root node.
	 */
	inline const Node* getRootNode() const
	{
		return m_S->getRootNode();
	}

	/**
	 * Returns the discretized time value of a node.
	 * @return the discretized time.
	 */
	inline Real getTime(const Node* node) const
	{
		return m_vals[node][0];
	}

	/**
	 * Returns the discretized time value of an edge.
	 * @return the discretized time.
	 */
	inline Real getEdgeTime(const Node* node) const
	{
		return (node->isRoot() ?
				(m_vals[node].back() - m_vals[node][0]) :
				(m_vals[node->getParent()][0] - m_vals[node][0]));
	}

	/**
	 * Returns the time span of the top edge.
	 * @return the top time.
	 */
	inline Real getTopTime() const
	{
		const Node* root = m_S->getRootNode();
		return (m_vals[root].back() - m_vals[root][0]);
	}

	/**
	 * Returns the time span from top to leaves.
	 * @return the top-to-leaves time.
	 */
	inline Real getTopToLeafTime() const
	{
		return getTopmost();
	}

	/**
	 * Returns the time span from root to leaves.
	 * @return the root-to-leaf time.
	 */
	inline Real getRootToLeafTime() const
	{
		return (m_vals[m_S->getRootNode()][0]);
	}

	/**
	 * Returns true if p1 is a proper ancestor of p2 or, p1 is equal to p2.
	 * @param p1 a discretization point.
	 * @param p2 a discretization point.
	 * @return true if p1==p2 or p1 is a proper ancestor of p2.
	 */
	bool isAncestor(const EdgeDiscretizer::Point& p1, const EdgeDiscretizer::Point& p2);

    /**
	 * Returns true if p1 is a proper ancestor of p2.
	 * @param p1 a discretization point.
	 * @param p2 a discretization point.
	 * @return true if p1 is a proper ancestor of p2.
	 */
	bool isProperAncestor(const EdgeDiscretizer::Point& p1, const EdgeDiscretizer::Point& p2);

	/**
	 * Extends base class cache functionality.
	 */
	virtual void cache();

	/**
	 * Extends base class cache functionality.
	 * @param node the lowest point on the path.
	 */
	virtual void cachePath(const Node* node);

	/**
	 * Extends base class cache functionality.
	 */
	virtual void restoreCache();

	/**
	 * Extends base class cache functionality.
	 * @param node the lower node of the path.
	 */
	virtual void restoreCachePath(const Node* node);

protected:

	/** Underlying tree. */
	Tree* m_S;

	/** Discretizer. */
	EdgeDiscretizer* m_discretizer;

	/** Timestep per edge. */
	RealVector m_timesteps;

	/** Cached timesteps. */
	RealVector m_cacheTimesteps;

};

} // end namespace beep.

#endif /* EDGEDISCTREE_HH */

