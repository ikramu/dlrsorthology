#ifndef GENERICMATRIX_HH
#define GENERICMATRIX_HH

#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>

#include "AnError.hh"

namespace beep 
{
  //-------------------------------------------------------------
  //! The purpose of this template is to provide easy matrix 
  //! handling without the numerical sophistication of LA_Matrix. 
  //!
  //! Some code stems from the C++ FAQ, www.parashift.com/c++-faq/
  //-------------------------------------------------------------
  template<typename T>  // See section on templates for more
  class GenericMatrix 
  {
  public:
    //-----------------------------------------------------------
    //
    // Constructors, destructors and assigment operator
    //
    //-----------------------------------------------------------
    GenericMatrix(unsigned nrows, unsigned ncols);
 
    // Based on the Law Of The Big Three:
    ~GenericMatrix();
    GenericMatrix(const GenericMatrix<T>& m);
    GenericMatrix<T>& operator=(const GenericMatrix<T>& m);
    
    //-----------------------------------------------------------
    //
    // Interface
    //
    //-----------------------------------------------------------
    // Access methods to get the (i,j) element:
    T& operator() (unsigned i, unsigned j);
    const T& operator() (unsigned i, unsigned j) const;
    
    // Access dimensions
    unsigned nrows() const;
    unsigned ncols() const;
    
    // Arithmetic
    GenericMatrix<T>& operator+=(const GenericMatrix& m);
    
    // Manipulation
    void reset(T x);
    void scale(T x);

    friend std::ostream& operator<<(std::ostream &o,
				    const GenericMatrix<T>& m)
    {
      return o << m.print();
    }
    
    std::string print() const
    {
      std::ostringstream oss;
      oss << "GenericMatrix: nrows = " 
	  << nrows_ 
	  << " ncols = " 
	  << ncols_ 
	  << "\n";
      return oss.str();
    }

    //------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------
  protected:
    const T& get_element(unsigned i, unsigned j) const;
    T& get_element(unsigned i, unsigned j);
    
    //------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------
  private:
    unsigned nrows_;
    unsigned ncols_;
    std::vector<T> data_;
  };
 


  //--------------------------------------------------------------
  //
  // Constructors, destructors and assigment operator
  //
  //--------------------------------------------------------------
  template<typename T>
  inline GenericMatrix<T>::GenericMatrix(unsigned nrows, 
					 unsigned ncols)
    : nrows_(nrows),
      ncols_(ncols),
      data_ (nrows * ncols)
  {
    if (nrows == 0 || ncols == 0)
      throw AnError("No dimensions on matrix!");
  }

  template<typename T>
  inline GenericMatrix<T>::GenericMatrix(const GenericMatrix<T>& m)
    : nrows_ (m.nrows_),
      ncols_ (m.ncols_),
      data_  (m.data_)
  {
    if (nrows_ == 0 || ncols_ == 0)
      throw AnError("No dimensions on matrix!");
  }
 

 
 template<typename T>
 inline GenericMatrix<T>::~GenericMatrix()
 {
   // Make sure that data_ is not dynamically allocated
 }

  template<typename T>
  GenericMatrix<T>& 
  GenericMatrix<T>::operator=(const GenericMatrix<T>& m)
  {
    if (this != &m)
      {
	nrows_ = m.nrows_;
	ncols_ = m.ncols_;
	data_  = m.data_;
      }
    return *this;
  }


  //--------------------------------------------------------------
  //
  // Interface
  //
  //--------------------------------------------------------------
  template<typename T>
  inline T& GenericMatrix<T>::operator()(unsigned row, 
					 unsigned col)
  {
    return get_element(row, col);
  }

  template<typename T>
  inline const T& GenericMatrix<T>::operator()(unsigned row, 
					       unsigned col) const
  {
    return get_element(row, col);
  }

  template<typename T>
  unsigned GenericMatrix<T>::nrows() const
  {
    return nrows_;
  }

  template<typename T>
  unsigned GenericMatrix<T>::ncols() const
  {
    return ncols_;
  }

  template<typename T>
  GenericMatrix<T>& 
  GenericMatrix<T>::operator+=(const GenericMatrix<T>& m)
  {
    assert(m.nrows() == nrows());
    assert(m.ncols() == ncols());

    int r = nrows();
    int c = ncols();

    for (int i = 0; i < r; i++) 
      {
	for (int j = 0; j < c; j++) 
	  {
	    get_element(i, j) +=  m(i, j);
	  }
      }

    return *this;
  }

  template<typename T>
  void GenericMatrix<T>::reset(T val)
  {
    std::fill(data_.begin(), data_.end(), val);
  }


  template<typename T>
  void GenericMatrix<T>::scale(T x)
  {
     std::transform(data_.begin(), data_.end(), data_.begin(),
 		   std::bind2nd(std::multiplies<T>(), x));
  }

  //--------------------------------------------------------------
  //
  // Implementation
  //
  //--------------------------------------------------------------
  template<typename T>
  inline T& GenericMatrix<T>::get_element(unsigned row, 
					  unsigned col)
  {
    if (row >= nrows_ || col >= ncols_) 
      {
	throw AnError("Out of bounds matrix index");
      }
    return data_[row*ncols_ + col];
  }
 
  template<typename T>
  inline const T& GenericMatrix<T>::get_element(unsigned row, 
						unsigned col) const
  {
    if (row >= nrows_ || col >= ncols_)
      {
	throw  AnError("Out of bounds matrix index");
      }
    return data_[row*ncols_ + col];
  }
 
}
#endif
