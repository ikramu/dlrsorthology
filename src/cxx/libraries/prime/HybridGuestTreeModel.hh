#ifndef HYBRIDGUESTTREEMODEL_HH
#define HYBRIDGUESTTREEMODEL_HH

#include "NodeNodeMap.hh"
#include "NodeMap.hh"
#include "BeepVector.hh"

#include <iostream>
#include <sstream>

namespace beep
{
  // Forward declarations
  class BirthDeathProbs;
  class Node;
  class Probability;
  class StrStrMap;
  class HybridTree;
  class Tree;
  //------------------------------------------------------------------------
  //
  //! Implements methods necessary for computing reconciliation likelihoods
  //! and most likely reconciliation etc, by way of dynamic programming. 
  //! \todo{Improve class description /bens}
  //
  //------------------------------------------------------------------------
  class HybridGuestTreeModel 
  {
  public:
    //------------------------------------------------------------------
    //
    //!\name Construct / Destruct / Assign
    //@{
    //------------------------------------------------------------------
    HybridGuestTreeModel(Tree &G, HybridTree& S,
			 StrStrMap &gs, BirthDeathProbs &bdp);
    HybridGuestTreeModel(const HybridGuestTreeModel &M);
    virtual ~HybridGuestTreeModel();

    HybridGuestTreeModel & operator=(const HybridGuestTreeModel &M);
    //@}
    //------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------

    //------------------------------------------------------------------
    // Interface to MCMCModel
    //------------------------------------------------------------------
    void update();

    //! If orthoNode=NULL, then calculateData Probability() returns Pr[G], 
    //! else if, e.g., orthoNode is the LCA of v and w, calculateDataProbability() 
    //! returns Pr[G and v and w are orthologs].
    //------------------------------------------------------------------------
    virtual Probability calculateDataProbability(); 

    //-------------------------------------------------------------------
    //
    // I/O
    //
    //-------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const HybridGuestTreeModel& pm);
    std::string print() const;

    //------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------
  protected:

    //! computes \f$ s_V(x,u) \f$
    //! precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
    virtual void computeSV(Node& x, Node& u);

    //! computes \f$ s_A(x,u) \f$
    virtual void computeSA(Node& x, Node& u);

    //! computes \f$ s_X(x,u,k), k\in[|L(G_u)|] \f$
    //! precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
    virtual void computeSX(Node& x, Node& u);

    //! multiplies with two if subtrees are non-isomorphic
    virtual void adjustFactor(Probability& factor, Node& u);
 
    //! For all x in S and u in G precompute slice_U(u) and slice_L(x,u), 
    //! slice_Lx,u) also indicate whether u can pass <p(x),x> during its
    //! evolution inside S
    //@{
    void computeSlice(Node& x);
    void sliceRecurseG(Node& x, Node& u);
    //@}

    //! Fills in isomorphy for G
    //@{
    void computeIsomorphy(Node& u);
    bool recursiveIsomorphy(Node& v, Node& w);
    //@}

  protected:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------

    //! Externally handled attributes
    //!@{
    HybridTree* S;
    Tree* G;
    StrStrMap* gs;
    BirthDeathProbs* bdp;
    //!@}

    // The following two structures are indexed by a species node and 
    // a gene node
    //! Holds \f$ S_A(x,u) \f$ for any \f$ x\in V(S), u\in V(G) \f$
    NodeNodeMap<Probability> S_A; 

    //! Holds \f$ S_X(x,u) \f$ for any \f$ x\in V(S), u\in V(G) \f$
    //! Notice that the S_X vectors are zero indexed!
    NodeNodeMap< std::vector<Probability> > S_X; 

    //! These keeps track if the corresponding value in S_X and S_A is computed 
    //!@{
    NodeNodeMap<unsigned> doneSA; 
    NodeNodeMap<unsigned> doneSX; 
    NodeMap<unsigned>     doneSlice;
    //!@}

    //! Element i stores the number of leaves in the subtree rooted at
    //! node with ID = i. This is an upper bound on gene tree slices.
    NodeMap<unsigned> slice_U;

    //! Like slice_U, but for lower bounds. This bound depends your
    //! reconciliation, or rather, where in the species tree you want a
    //! gene node.
    NodeNodeMap<unsigned> slice_L;

    //! Keeps track o isomorphisms in G
    UnsignedVector isomorphy;

    //! The value of this attribute determines what is computed by 
    //! calculateDataProbability(). If orthoNode = v, where v is the
    //! least common ancestor of some leaves l and l', then the prob 
    //! that l and l' are orthologs are computed, else if orthoNode = 0,
    //! Pr[G] is computed exactly as in GuestTreemodel
    const Node* orthoNode; 
  };

  
}//end namespace beep

#endif
