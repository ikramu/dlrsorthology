/* 
 * File:   LSDTimeProbs.cc
 * Author: fmattias
 * 
 * Created on December 7, 2009, 12:13 PM
 */

#include "LSDTimeProbs.hh"

#include <istream>
#include <map>

#include "Probability.hh"
#include "Tree.hh"
#include "Node.hh"
#include "AnError.hh"
#include "io/LSDFileFormat.hxx"

namespace beep {

using namespace std;

LSDTimeProbs::LSDTimeProbs()
{

}

LSDTimeProbs::LSDTimeProbs(const LSDTimeProbs& other) :
                                m_lsdTimes(other.m_lsdTimes),
                                m_lsdProbs(other.m_lsdProbs)
{

}

LSDTimeProbs::~LSDTimeProbs()
{
}

void LSDTimeProbs::addTime(const Node *edge, Real time, Probability lsdProbability)
{
    if(time > edge->getTime()) {
        throw AnError("LSD time can not be larger than time of the edge.");
    }

    m_lsdTimes[edge->getNumber()] = time;
    m_lsdProbs[edge->getNumber()] = lsdProbability;
}

bool LSDTimeProbs::hasTime(const Node *edge)
{
    return m_lsdTimes.count(edge->getNumber()) > 0;
}

Real LSDTimeProbs::getTime(const Node *edge)
{
    if(hasTime(edge)) {
        return m_lsdTimes[edge->getNumber()];
    }
    else {
        return -1.0;
    }
}

Probability LSDTimeProbs::getProbability(const Node *edge)
{
    if(hasTime(edge)) {
        return m_lsdProbs[edge->getNumber()];
    }
    else {
        return -1.0;
    }
}

bool LSDTimeProbs::lsdInInterval(const Node *edge, Real t1, Real t2)
{
    if(hasTime(edge)) {
        // Construct interval (min(t1, t2), max(t1, t2))
        Real lsdTime = m_lsdTimes[edge->getNumber()];
        Real start = std::min(t1, t2);
        Real end = std::max(t1, t2);

        // Return true if there is an LSD between the time points
        return lsdTime > start && lsdTime < end;
    }
    else {
        return false;
    }
}

void LSDTimeProbs::setAll(Probability p)
{
    std::map<int, Real>::iterator lsd_it;
    lsd_it = m_lsdTimes.begin();

    // Go through each LSD and set the probability to p
    for(; lsd_it != m_lsdTimes.end(); ++lsd_it) {
        m_lsdProbs[(*lsd_it).first] = p;
    }
}

LSDTimeProbs::LSDTimePointsIterator LSDTimeProbs::begin()
{
    return m_lsdTimes.begin();
}

LSDTimeProbs::LSDTimePointsIterator LSDTimeProbs::end()
{
    return m_lsdTimes.end();
}

void LSDTimeProbs::readFromFile(istream &input,
                                    const Tree &speciesTree,
                                    LSDTimeProbs &lsdTimeProbs)
{
    try {
        // Parse xml and create an object representation of the file
        auto_ptr<LSDList> lsdXML(LSDList_(input));

        // Add the LSDs in the object representation to the LSDTimeProbs object
        LSDList::LSDElement_const_iterator lsdList_it = lsdXML->LSDElement().begin();
        LSDList::LSDElement_const_iterator lsdList_itEnd = lsdXML->LSDElement().end();
        for(; lsdList_it != lsdList_itEnd; ++lsdList_it) {
            LSDContinuousPoint lsdPoint = *lsdList_it;
            Node *speciesEdge = speciesTree.getNode(lsdPoint.edge());
            double lsdProbability = lsdPoint.lsdProbability();

            /* Add LSD point */
            lsdTimeProbs.addTime(speciesEdge, lsdPoint.time(), lsdProbability);
        }
    }
    catch(const xml_schema::exception& e) {
        cerr << e << endl;
    }
}

}
