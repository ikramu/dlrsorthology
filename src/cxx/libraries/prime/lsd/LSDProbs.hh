/* LSDProbs is an object for storing large scale duplication data. It stores
 * the probability of duplicating on a discretization point in a discretized
 * species tree. 
 *
 * File:   LSDProbs.hh
 * Author: fmattias
 *
 * Created on December 2, 2009, 11:13 AM
 */

#ifndef _LSDPOINTS_HH
#define	_LSDPOINTS_HH

#include <istream>
#include <map>

#include "Probability.hh"
#include "TreeDiscretizers.hh"
#include "EdgeDiscTree.hh"
#include "lsd/LSDTimeProbs.hh"

namespace beep {

class LSDProbs {
public:
    typedef std::pair<unsigned int, unsigned int> LSDPoint;
    /**
     * 
     */
    LSDProbs();

    /**
     * Copy constructor
     *
     * Creates an object identical to other.
     */
    LSDProbs(const LSDProbs& other);

    /**
     * Assignment operator
     *
     * Makes a copy of the given LSDProbs object. No references or pointers
     * are shared between the objects after assignment.
     *
     * other - *This* object will be identical to other after assignment.
     */
    LSDProbs & operator=(const LSDProbs &other);
    virtual ~LSDProbs();

    /**
     * addPoint
     *
     * Associates a discretization point with a duplication probability. The
     * specified probability is the probability of duplication on the given
     * discretization vertex.
     *
     * point - Discretization vertex which will be associated with a probability.
     * duplicationProbability - Probability of duplication on point.
     */
    void addPoint(const EdgeDiscretizer::Point &point, Probability duplicationProbability);

    /**
     * Given a map from edges to continuous LSDs, this function adds all LSDs
     * from the continuous map to this object, by selecting the discretization
     * point closest to each continuous LSD.
     *
     * timeProbs - A map of continuous LSDs.
     * discretizedSpeciesTree - A discretized species tree corresponding to the
     *                          species tree used when adding the LSDs in
     *                          timeProbs.
     */
    void addDiscProbsFromTimes(LSDTimeProbs &timeProbs, const EdgeDiscTree &discretizedSpeciesTree);

    /**
     * contains
     *
     * Returns true if the given point has a probability associated with it.
     * Note, the probability can be 0.
     *
     * point - The discretization vertex to search for.
     */
    bool contains(const EdgeDiscretizer::Point &point) const;

    /**
     * getProbability
     *
     * Returns the probability associated with point. If there is no probability
     * associated with point, 0 will be returned.
     */
    Probability getProbability(const EdgeDiscretizer::Point &point) const;

    /**
     * Sets the probability of all stored LSDs to p.
     *
     * p - Probability that all LSDs will have after function call.
     */
    void setAll(Probability p);


    /**
     * Reads a set of continuous LSDs from input and adds them in the specified
     * LSDProbs object.
     *
     * input - Stream to read the LSDs from.
     * discretizedSpeciesTree - Species tree in which the LSDs will occur.
     * lsd - Object where the LSDs will be stored.
     */
    static void readFromFile(std::istream &input,
                             const EdgeDiscTree &discretizedSpeciesTree,
                             LSDProbs &lsd);

    /**
     * Output operator
     *
     * Prints each stored lsd probability and the id and discretization point
     * number on which the lsd is located.
     *
     * out - output operator
     */
    std::ostream& operator<<(std::ostream &out);


private:
    /**
     * Returns the closest discretization point to a given edge and time.
     *
     * edge - Edge which the closest discretization point will be searched for.
     * time - Continuous time of the LSD.
     * discretizedSpeciesTree - The discretized species tree in which edge is
     *                          located.
     */
    LSDPoint getClosestDiscPoint(const Node *edge, Real time, const EdgeDiscTree &discretizedSpeciesTree) const;

    /**
     * Converts from a discretization point to the internal LSDPoint format.
     *
     * point - Discretization point from to convert from.
     * lsdPoint - Discretization point from to convert to.
     */
    void convertToLSDPoint(const EdgeDiscretizer::Point &point, LSDPoint &lsdPoint) const;

    // Holds duplication points
    std::map<LSDPoint, Probability> m_lsds;
};

}

#endif	/* _LSDPOINTS_HH */

