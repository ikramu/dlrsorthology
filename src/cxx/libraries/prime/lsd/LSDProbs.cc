/* 
 * File:   LSDProbs.cc
 * Author: fmattias
 * 
 * Created on December 2, 2009, 11:13 AM
 */

#include <cmath>
#include <ostream>
#include <istream>

#include "lsd/LSDProbs.hh"

namespace beep {

LSDProbs::LSDProbs()
{
}

LSDProbs::LSDProbs(const LSDProbs& other) : m_lsds(other.m_lsds)
{
}

LSDProbs &
LSDProbs::operator=(const LSDProbs &other)
{
    // Check that the given object is different from this
    if(&other != this) {
        this->m_lsds = other.m_lsds;
    }
    return *this;
}

LSDProbs::~LSDProbs() {
}

void LSDProbs::addPoint(const EdgeDiscretizer::Point &point, Probability duplicationProbability)
{
    LSDPoint lsdPoint;
    convertToLSDPoint(point, lsdPoint);
    m_lsds[lsdPoint] = duplicationProbability;
}

bool LSDProbs::contains(const EdgeDiscretizer::Point &point) const
{
    LSDPoint lsdPoint;
    convertToLSDPoint(point, lsdPoint);
    return m_lsds.count(lsdPoint) > 0;
}

Probability LSDProbs::getProbability(const EdgeDiscretizer::Point &point) const
{
    LSDPoint lsdPoint;
    convertToLSDPoint(point, lsdPoint);
    if(m_lsds.count(lsdPoint) > 0) {
        // The use of an iterator here is required for the const qualifier.
        std::map<LSDPoint, Probability>::const_iterator duplication_it = m_lsds.find(lsdPoint);
        return (*duplication_it).second;
    }
    else {
        return Probability(0.0);
    }
}

void LSDProbs::setAll(Probability p)
{
    std::map<LSDPoint, Probability>::iterator lsd_it;
    lsd_it = m_lsds.begin();

    // Iterate through each LSD and set the probability to p
    for(; lsd_it != m_lsds.end(); ++lsd_it) {
        (*lsd_it).second = p;
    }
}

void LSDProbs::convertToLSDPoint(const EdgeDiscretizer::Point &point, LSDPoint &lsdPoint) const
{
    lsdPoint.first = point.first->getNumber();
    lsdPoint.second = point.second;
}

void LSDProbs::addDiscProbsFromTimes(LSDTimeProbs &timeProbs, const EdgeDiscTree &discretizedSpeciesTree)
{
    LSDTimeProbs::LSDTimePointsIterator lsd_it = timeProbs.begin();

    // For each continuous point find the closest discretization point and
    // add them here.
    for(lsd_it = timeProbs.begin(); lsd_it != timeProbs.end(); ++lsd_it) {
        LSDTimeProbs::TimePoint lsdTimePoint = *lsd_it;
        const Node *edge = discretizedSpeciesTree.getNode(lsdTimePoint.first);
        LSDPoint lsdProb = getClosestDiscPoint(edge, lsdTimePoint.second, discretizedSpeciesTree);
        m_lsds[lsdProb] = timeProbs.getProbability(edge);
    }
}

LSDProbs::LSDPoint LSDProbs::getClosestDiscPoint(const Node *edge, Real time, const EdgeDiscTree &discretizedSpeciesTree) const
{
    EdgeDiscretizer::Point point(edge, 0);
    Real absoluteTime = edge->getNodeTime() + time;
    int closestPoint = 0;
    Real minDistance = time;

    // Try distance to each point
    for(unsigned int discPoint = 1; discPoint < discretizedSpeciesTree.getNoOfPts(edge); discPoint++) {
        point.second = discPoint;

        // See if we got a new minimum
        if(std::abs(discretizedSpeciesTree(point) - absoluteTime) < minDistance) {
            minDistance = std::abs(discretizedSpeciesTree(point) - absoluteTime);
            closestPoint = discPoint;
        }
    }

    return LSDPoint(edge->getNumber(), closestPoint);
}

void LSDProbs::readFromFile(std::istream &input,
                          const EdgeDiscTree &discretizedSpeciesTree,
                          LSDProbs &lsd)
{
    // Read continuous times from file
    LSDTimeProbs lsdTimes;
    LSDTimeProbs::readFromFile(input, discretizedSpeciesTree.getTree(), lsdTimes);

    // Convert to discretized points
    lsd.addDiscProbsFromTimes(lsdTimes, discretizedSpeciesTree);
}

std::ostream & LSDProbs::operator<<(std::ostream &out)
{
    std::map<LSDPoint, Probability>::const_iterator lsd_it, lsd_itEnd;
    lsd_it = m_lsds.begin();
    lsd_itEnd = m_lsds.end();

    out << "LSD proabilities: " << std::endl;
    for(; lsd_it != lsd_itEnd; ++lsd_it) {
        out << "Edge: " << (*lsd_it).first.first 
            << " Disc point: " << (*lsd_it).first.second 
            << " Probability: " << (*lsd_it).second.val() << std::endl;
    }

    return out;
}

}
