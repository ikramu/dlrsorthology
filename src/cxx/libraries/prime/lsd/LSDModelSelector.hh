//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************
//
///*
// * File:   LSDModelSelector.hh
// * Author: fmattias
// *
// * Created on December 1, 2009, 3:53 PM
// */
//
//#ifndef _LSDMODELSELECTOR_HH
//#define	_LSDMODELSELECTOR_HH
//
//#include "SequenceData.hh"
//#include "Density2P.hh"
//#include "mcmc/PosteriorSampler.hh"
//
//namespace beep {
//
//class LSDModelSelector {
//public:
//    LSDModelSelector();
//    virtual ~LSDModelSelector();
//
//    /**
//     * computeModelProbability
//     *
//     * Computes the model likelihood given an MCMC chain over all
//     * free parameters of the model. The computation is performed by
//     * computing the average likelihood obtained when drawing samples from
//     * prior model.
//     *
//     * treeLengthThetaMCMC - An MCMC over the prior parameters of the model.
//     * iterations - The number of iterations to perfrom in the integration.
//     * skip - The sample interval used to sample from the given MCMC chain.
//     */
//    Probability computeModelProbability(PosteriorSampler &treeLengthThetaMCMC, int iterations, int skip);
//
//    /**
//     * computeBayesFactor
//     *
//     * Computes bayes factor given the numerator and denominator,
//     *
//     * B = modelAlternative / modelZero
//     *
//     * modelZero - The denominator of bayes factor.
//     * modelAlternative - The numerator of bayes factor.
//     */
//    Real computeBayesFactor(Probability modelZero, Probability modelAlternative);
//
//private:
//
//};
//}
//
//#endif	/* _LSDMODELSELECTOR_HH */

