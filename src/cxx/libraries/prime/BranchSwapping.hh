#ifndef BRANCHSWAPPING_HH
#define BRANCHSWAPPING_HH

#include "Beep.hh"
#include "BeepVector.hh"
#include "PRNG.hh"

//!\tofo{ To be done:
//!    Reset all node times in the tree to 1 (has to be greater than 0)}


namespace beep
{

// Forward declarations
class Node;
class Tree;
class TreePerturbationEvent;

//----------------------------------------------------------------------
//
//! Class for perturbing the 'topology' of trees, i.e., rerooting 
//! and branch-swaping
//!
//! author: Lars Arvestad, �rjan �kerborg
//! copyright: The mcmc-club, Stockholm Bioinformatics Center
//
//----------------------------------------------------------------------
class BranchSwapping
{
public:
	//----------------------------------------------------------------------
	//
	// Constructors and Destructors
	//
	//----------------------------------------------------------------------
	BranchSwapping();
	~BranchSwapping();
	BranchSwapping(const BranchSwapping& bs);
	BranchSwapping& operator=(const BranchSwapping& bs);

	//----------------------------------------------------------------------
	//
	// Interface
	//
	//----------------------------------------------------------------------
	TreePerturbationEvent* doReRoot(Tree &T, bool withLengths=false, bool withTimes=false, bool returnInfo=false);
	TreePerturbationEvent* doNNI(Tree &T, bool withLengths=false, bool withTimes=false, bool returnInfo=false);
	TreePerturbationEvent* doSPR(Tree &T, bool withLengths=false, bool withTimes=false, bool returnInfo=false);

	//! Reroot tree at incoming arc to Node v. Also updates 
	//! branchLengths and edgeTimes.
	//----------------------------------------------------------------------
        void setRootOn(Node *v);
        void setRootOn(Node *v, bool withLengths, bool withTimes);

	//! Place root such that the leaves in outgroup from a 
	//! monophyletic group
	//----------------------------------------------------------------------
	void rootAtOutgroup(Tree& T, std::vector<std::string> outgroup);
	
private:
	//----------------------------------------------------------------------
	//
	// Implementation
	//
	//----------------------------------------------------------------------

	//! Switch place on subtree T_v and T_W
	//!
	//!             .                         .                     .
	//!            / \                       / \                    .
	//!           /   \                     /   \                   .
	//!          /     \                   /     \                  .
	//!         /       \                 /       \                 .
	//!        /         \               /         \                .
	//!       /vp         \ wp          /vp         \ wp     v   (argument 1)
	//!      / \         / \    to     / \         / \       w   (argument 2)
	//!     /   \       /   \         /   \       /   \      vp = v_parent  
	//!    /w  vs\     /v  ws\       /w  vs\     /v  ws\     wp = w_parent  
	//!   / \   / \   / \   / \     / \   / \   / \   / \    vs = v_sibling  
	//!  /___\ /___\ /___\ /___\   /___\ /___\ /___\ /___\   ws = w_sibling  
	//!
	//! precondition: 
	//! v.isRoot() == false, w.isRoot() == false, v != w, v != 0, w != 0
	//----------------------------------------------------------------------
	void swap(Node *v, Node *w);

	void rotate(Node* v, Node *v_child, bool withLengths, bool withTimes);

	//! Recursively move root to <v,v_child> and update edge lengths.
	//! Notice that the same node will be root node.
	//!
	//! Here is the basic move.
	//!
	//!            .vp            .vp                               .
	//!           / \c        a+b/ \x                               .
	//!         b/   \          /vc \                               . 
	//!         /   /3\        /1\   \                              .
	//!        /v  /___\      /___\   \v                            .
	//!       / \         to         / \                            . 
	//!     a/   \                  /   \c-x                        .
	//!     /vc   \                /     \    v          (argument 1)
	//!    /1\   /2\              /2\   /3\   vc=v_child (argument 2)
	//!   /___\ /___\            /___\ /___\  vp=v_parent  
	//----------------------------------------------------------------------

	void recursiveEdgeTimeScaling(Node* v, Real scaleFactor);
	bool isInSubtree(Node* u_c_new, Node* u);
	
protected:
	
	PRNG R;   //!< Random number generator.
};

}//end namespace beep

#endif
