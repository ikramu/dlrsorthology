#ifndef DENSITY2P_COMMON_HH
#define DENSITY2P_COMMON_HH

#include "Density2P.hh"

namespace beep
{
  //----------------------------------------------------------------------
  //
  // Class Density2P
  //! Implemented base class (body) for probabilistic density functions 
  //! that takes 2 parameters.
  //!
  //! Invariants: variance >0; 
  //!
  //! Author: Bengt Sennblad, copyright: the MCMC-club, SBC.
  //! All rights reserved.
  //
  //----------------------------------------------------------------------
  class Density2P_common : public Density2P
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //----------------------------------------------------------------------
    Density2P_common(Real mean, Real variance, const std::string& density);
    Density2P_common(const Density2P_common& df);
    
    //! Factory method that allocates a Density2P instance from a string
    //! name:
    //! INVG - Inverse Gauss density
    //! LOGN - Log Normal density
    //! GAMMA - Gamma density
    //! UNIFORM - Uniform density
    //! The function returns 0 if name is not one of the above.
    static Density2P *createDensity(Real mean, Real variance, bool embedded, const std::string &density);
    virtual ~Density2P_common();

    Density2P_common& operator=(const Density2P_common& df);

  public:
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    // What density does this class handle?
    std::string densityName() const;

    Probability operator()(const Real& x, const Real& interval) const;
    virtual Probability pdf(const Real& x) const;
    virtual Probability cdf(const Real& x) const;

    // Access Parameters
    //----------------------------------------------------------------------

    //! Returns current value of alpha.
    Real getAlpha() const; 

    //! Returns current value of beta.
    Real getBeta() const; 

    // Set Parameters
    //----------------------------------------------------------------------

    //! Sets embedded parameters (alpha and beta) of density 
    //! Defaults to alpha = mean and beta = variance.
    //! Postcondition: getMean()==mean and getVariance()==variance
    void setEmbeddedParameters(const Real& first, const Real& second);

    //! Sets embedded parameter alpha of density 
    //! Postcondition: alpha and beta is within precision limits.
    void setAlpha(const Real& alpha);

    //! Sets embedded parameters beta of density 
    //! Postcondition: getMean()==mean and getVariance()==variance
    void setBeta(const Real& beta);


    // test and information on valid ranges
    //------------------------------------------------------------------

    //! Check if value is valid (has a non-zero probability).
    //! Defaults to precision limits
    bool isInRange(const Real& rate) const;

    //! Returns range in which density has a non-zero probability.
    //! Defaults to precision limits
    void getRange(Real& min, Real& max) const;
 
    //! set user-defined range in which density has a non-zero probability.
    void setRange(const Real& min, const Real& max);

    //! Get range in which mean is valid.
    //! Defaults to range
    void getMeanRange(Real& min, Real& max) const;

    //! Get range in which variance is valid.
    //! Defaults to [precision of zero,max of range]
    void getVarianceRange(Real& min, Real& max) const;

  protected:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------

    // Attribues relate to the parameters of the density function - 
    // may be chosen for practical issues rather than strict convention
    //----------------------------------------------------------------------
    Real alpha; //!< First parameter, e.g., mean or a function thereof
    Real beta;  //!< Second parameter, e.g., variance or a function thereof
    std::string density;        //! Standard name of density.
    std::pair<Real, Real> range;
  };

}//end namespace beep
#endif
