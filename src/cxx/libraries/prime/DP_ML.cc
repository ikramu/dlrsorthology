#include "DP_ML.hh"

namespace beep
{
  using namespace std;
  
  //-------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  //
  //-------------------------------------------------------------

  DP_ML::DP_ML(Tree& G, Tree& S, Density2P* df, Real& birthRate, Real& deathRate, unsigned noOfDiscrIntervals)
    : noOfNodes(G.getNumberOfNodes()),
      noOfDiscrIntervals(noOfDiscrIntervals),
      G(G),
      S(S),
      M(noOfNodes, noOfDiscrIntervals+1),
      M_left(noOfNodes, noOfDiscrIntervals+1),
      M_right(noOfNodes, noOfDiscrIntervals+1),
      df(df),
      bdp(S,birthRate,deathRate)
  {
     for (unsigned i = 0; i <= (noOfNodes-1); i++)
       {
	 for (unsigned j = 0; j <= noOfDiscrIntervals; j++)
	   {
	     M(i,j) = 1.0;
	     M_left(i,j) = 0;
	     M_right(i,j) = 0;
	   }
       }
  }

  DP_ML::~DP_ML()
  {
  }

//   // Assignment
//   //----------------------------------------------------------------------
//   DP_ML &
//   DP_ML::operator=(const DP_ML &dpml)
//   {
//     if (this != &dpml)
//       {
// 	noOfNodes = dpml.noOfNodes;
// 	noOfDiscrIntervals = dpml.noOfDiscrIntervals;
// 	G = dpml.G;
// 	S = dpml.S;
// 	M = dpml.M;
// 	M_left = dpml.M_left;
// 	M_right = dpml.M_rigth;
// 	df = dpml.df;
// 	bdp = dpml.bdp;
//       }

//     return *this;
//   }

  void 
  DP_ML::setValues(unsigned i, unsigned j, Probability p, unsigned l_val, unsigned r_val)
  {
    M(i,j) = p;
    M_left(i,j) = l_val;
    M_right(i,j) = r_val;
  }

  void 
  DP_ML::setValue(unsigned i, unsigned j, Probability p)
  {
    M(i,j) = p;
  }

  Probability
  DP_ML::getValue(unsigned i, unsigned j)
  {
    Probability p = M(i,j);
    return p;
  }

  unsigned
  DP_ML::getLeftPointer(unsigned i, unsigned j)
  {
    unsigned l = M_left(i,j);
    return l;
  }

  unsigned
  DP_ML::getRightPointer(unsigned i, unsigned j)
  {
    unsigned r = M_right(i,j);
    return r;
  }
  
  void DP_ML::backTrace(unsigned u_index, unsigned u_time_iter)
  {
    Node* u = G.getNode(u_index);
    Node* v = u->getLeftChild();
    Node* w = u->getRightChild();
    unsigned v_time_iter = getLeftPointer(u_index,u_time_iter);
    unsigned w_time_iter = getRightPointer(u_index,u_time_iter);

    Real u_time = u_time_iter*(1.0/noOfDiscrIntervals);

    if (!u->isRoot())
      {
	G.setTime(*u,u_time);
	//u->setNodeTime(u_time);
      }
    cout << "Node: " << u_index << " time: " << u_time << "\n";

    if (!v->isLeaf()) 
      {	
	unsigned v_index = v->getNumber();
	backTrace(v_index,v_time_iter);
      }
    if (!w->isLeaf()) 
      {
	unsigned w_index = w->getNumber();	
	backTrace(w_index,w_time_iter);
      }

    //G.updateAllEdgeTimes();
    //v->updateTime();
    //w->updateTime();
  }

  Probability DP_ML::oneValue(unsigned u_index, vector<double> rates, vector<double> nodeTimes)
  {
    Node* u = G.getNode(u_index);
    Node* s = S.getRootNode();

    Node* v = u->getLeftChild();
    Node* w = u->getRightChild();
    unsigned v_index = v->getNumber();	
    unsigned w_index = w->getNumber();
   
    Probability ret = 1.0;
    Probability v_GammaProb = 1.0;
    Probability w_GammaProb = 1.0;
    Probability BDProb = 1.0;
 
    if (!v->isLeaf())
      {
	ret *= oneValue(v_index,rates,nodeTimes);
      }
    if (!w->isLeaf())
      {
	ret *= oneValue(w_index,rates,nodeTimes);
      }

    Real v_rate = rates.at(v_index);
    if (v_rate > 1e-10)
      {
	v_GammaProb = df->operator()(v_rate);
      }
    Real w_rate = rates.at(w_index);
    if (w_rate > 1e-10)
      {
	w_GammaProb = df->operator()(w_rate);
      }

    if (u->isRoot())
      {
	BDProb = 1;
      }
    else
      {
	BDProb = bdp.partialEdgeTimeProb(*s,u->getNumberOfLeaves(),nodeTimes.at(u_index));
      }
    return ret * v_GammaProb * w_GammaProb * BDProb;
  }

  Probability DP_ML::DP(unsigned u_index, bool optimize, bool integral) 
  {
    Node* u = G.getNode(u_index);
    Node* s = S.getRootNode();

    Node* v = u->getLeftChild();
    Node* w = u->getRightChild();
    unsigned v_index = v->getNumber();
    unsigned w_index = w->getNumber();

    Probability P_max;

    if (!v->isLeaf())
      {
	P_max = DP(v_index,optimize,integral);
      }
    if (!w->isLeaf())
      {
	P_max = DP(w_index,optimize,integral);
      }

    unsigned u_firstRelevantDiscrPoint = G.getHeight(u) - 1;
    unsigned u_lastRelevantDiscrPoint = noOfDiscrIntervals - (G.getHeight() - G.getHeight(u));
		
    if (u->isRoot()) 
      {
	u_firstRelevantDiscrPoint = noOfDiscrIntervals;
      }
	      
    for (unsigned u_time_iter = u_firstRelevantDiscrPoint; u_time_iter <= u_lastRelevantDiscrPoint; ++u_time_iter)
      {
	Real u_time = u_time_iter*(1.0/noOfDiscrIntervals);

	Probability P_utu_save = 0; // max if optimize, sum if integral
	unsigned best_v_time_iter = 0;
	unsigned best_w_time_iter = 0;

	unsigned v_firstRelevantDiscrPoint = G.getHeight(v) - 1;
	unsigned v_lastRelevantDiscrPoint = min(u_time_iter - 1, noOfDiscrIntervals - (G.getHeight() - G.getHeight(v)));
	unsigned w_firstRelevantDiscrPoint = G.getHeight(w) - 1;
	unsigned w_lastRelevantDiscrPoint = min(u_time_iter - 1, noOfDiscrIntervals - (G.getHeight() - G.getHeight(w)));

	Probability BDProb;
	if (u->isRoot())
	  BDProb = 1;
	else
	  BDProb = bdp.partialEdgeTimeProb(*s,u->getNumberOfLeaves(),u_time);

	for (unsigned v_time_iter = v_firstRelevantDiscrPoint; v_time_iter <= v_lastRelevantDiscrPoint; ++v_time_iter)
	  {
	    Real v_time = v_time_iter*(1.0/noOfDiscrIntervals);
	    Real v_timeFromParent = u_time - v_time;

	    Probability v_GammaProb = df->operator()(v->getLength()/v_timeFromParent);

	    for (unsigned w_time_iter = w_firstRelevantDiscrPoint; w_time_iter <= w_lastRelevantDiscrPoint; ++w_time_iter)
	      {			
		Real w_time = w_time_iter*(1.0/noOfDiscrIntervals);
		Real w_timeFromParent = u_time - w_time;

		Probability w_GammaProb = df->operator()(w->getLength()/w_timeFromParent);

		Probability P_utu = BDProb * v_GammaProb * w_GammaProb * getValue(v_index,v_time_iter) * getValue(w_index,w_time_iter);
		if (optimize)
		  {
		    if (P_utu > P_utu_save)
		      {
			best_v_time_iter = v_time_iter; 
			best_w_time_iter = w_time_iter; 
			P_utu_save = P_utu;
		      }
		  }
		else if (integral) 
		  {
		    P_utu_save += P_utu;
		  }

		if (w->isLeaf())
		  {
		    break;
		  }
	      }
	    if (v->isLeaf())
	      {
		break;
	      }
	  }
	if (optimize)
	  {
	    setValues(u_index,u_time_iter,P_utu_save,best_v_time_iter,best_w_time_iter);
	  }
	else if (integral)
	  {
	    setValue(u_index,u_time_iter,P_utu_save);
	  }
      }

    P_max = getValue(G.getRootNode()->getNumber(),noOfDiscrIntervals);
    return P_max;
  }

  Probability 
  DP_ML::DP(unsigned u_index, bool optimize, bool integral, vector<double> lengths) 
  {
    Node* u = G.getNode(u_index);
    Node* s = S.getRootNode();

    Node* v = u->getLeftChild();
    Node* w = u->getRightChild();
    unsigned v_index = v->getNumber();
    unsigned w_index = w->getNumber();

    Probability P_max;

    if (!v->isLeaf())
      {
	P_max = DP(v_index,optimize,integral,lengths);
      }
    if (!w->isLeaf())
      {
	P_max = DP(w_index,optimize,integral,lengths);
      }

    unsigned u_firstRelevantDiscrPoint = G.getHeight(u) - 1;
    unsigned u_lastRelevantDiscrPoint = noOfDiscrIntervals - (G.getHeight() - G.getHeight(u));
		
    if (u->isRoot()) 
      {
	u_firstRelevantDiscrPoint = noOfDiscrIntervals;
      }
	      
    for (unsigned u_time_iter = u_firstRelevantDiscrPoint; u_time_iter <= u_lastRelevantDiscrPoint; ++u_time_iter)
      {
	Real u_time = u_time_iter*(1.0/noOfDiscrIntervals);

	Probability P_utu_save = 0; // max if optimize, sum if integral
	unsigned best_v_time_iter = 0;
	unsigned best_w_time_iter = 0;

	unsigned v_firstRelevantDiscrPoint = G.getHeight(v) - 1;
	unsigned v_lastRelevantDiscrPoint = min(u_time_iter - 1, noOfDiscrIntervals - (G.getHeight() - G.getHeight(v)));
	unsigned w_firstRelevantDiscrPoint = G.getHeight(w) - 1;
	unsigned w_lastRelevantDiscrPoint = min(u_time_iter - 1, noOfDiscrIntervals - (G.getHeight() - G.getHeight(w)));

	Probability BDProb;
      	if (u->isRoot())
	  {
#ifdef THOMPSON
	    BDProb = bdp.getThompsons_p1();
#else
	    BDProb = 1;
#endif
	  }
       	else
	  BDProb = bdp.partialEdgeTimeProb(*s,u->getNumberOfLeaves(),u_time);

	for (unsigned v_time_iter = v_firstRelevantDiscrPoint; v_time_iter <= v_lastRelevantDiscrPoint; ++v_time_iter)
	  {
	    Real v_time = v_time_iter*(1.0/noOfDiscrIntervals);
	    Real v_timeFromParent = u_time - v_time;

	    Probability v_GammaProb = 1.0;
	    Real v_rate = lengths.at(v_index)/v_timeFromParent;
	    if (v_rate > 1e-10)
	      {
		v_GammaProb = df->operator()(v_rate);
	      }

	    for (unsigned w_time_iter = w_firstRelevantDiscrPoint; w_time_iter <= w_lastRelevantDiscrPoint; ++w_time_iter)
	      {			
		Real w_time = w_time_iter*(1.0/noOfDiscrIntervals);
		Real w_timeFromParent = u_time - w_time;

		Probability w_GammaProb = 1.0;
		Real w_rate = lengths.at(w_index)/w_timeFromParent;
		if (w_rate > 1e-10)
		  {
		    w_GammaProb = df->operator()(w_rate);
		  }

		Probability P_utu = BDProb * v_GammaProb * w_GammaProb * getValue(v_index,v_time_iter) * getValue(w_index,w_time_iter);

		if (optimize)
		  {
		    if (P_utu > P_utu_save)
		      {
			best_v_time_iter = v_time_iter; 
			best_w_time_iter = w_time_iter; 
			P_utu_save = P_utu;
		      }
		  }
		else if (integral) 
		  {
		    P_utu_save += P_utu;
		  }

		if (w->isLeaf())
		  {
		    break;
		  }
		//		cout << "u_index: " << u_index << " u_time: " << u_time << " v_index: " << v_index << " v_time: " << v_time << " w_index: " << w_index << " w_time: " << w_time << " BDProb: " << BDProb << " v_GammaProb: " << v_GammaProb << " w_GammaProb: " << w_GammaProb << " P_utu: " << P_utu << "\n";
	      }
	    if (v->isLeaf())
	      {
		break;
	      }
	  }

	if (optimize)
	  {
	    setValues(u_index,u_time_iter,P_utu_save,best_v_time_iter,best_w_time_iter);
	  }
	else if (integral)
	  {
	    setValue(u_index,u_time_iter,P_utu_save);
	  }
      }
    P_max = getValue(G.getRootNode()->getNumber(),noOfDiscrIntervals);
    return P_max;
  }

  Probability 
  DP_ML::DPsampleTreeFromDistr(unsigned u_index)
  {
    Node* u = G.getNode(u_index);
    Node* s = S.getRootNode();

    Node* v = u->getLeftChild();
    Node* w = u->getRightChild();
    unsigned v_index = v->getNumber();
    unsigned w_index = w->getNumber();

    Probability P_max;

    if (!v->isLeaf())
      {
	P_max = DPsampleTreeFromDistr(v_index);
      }
    if (!w->isLeaf())
      {
	P_max = DPsampleTreeFromDistr(w_index);
      }

    unsigned u_firstRelevantDiscrPoint = G.getHeight(u) - 1;
    unsigned u_lastRelevantDiscrPoint = noOfDiscrIntervals - (G.getHeight() - G.getHeight(u));

    if (u->isRoot()) 
      {
	u_firstRelevantDiscrPoint = noOfDiscrIntervals;
      }
	      
    vector<Probability> P_utu_vec;
    for (unsigned u_time_iter = u_firstRelevantDiscrPoint; u_time_iter <= u_lastRelevantDiscrPoint; ++u_time_iter)
      {
	P_utu_vec.clear();
	Probability P_samp = 0;
	Probability P_utu_sum = 0;
	PRNG rand;
	Real p = rand.genrand_real1();

	Real u_time = u_time_iter*(1.0/noOfDiscrIntervals);

	unsigned best_v_time_iter = 0;
	unsigned best_w_time_iter = 0;

	unsigned v_firstRelevantDiscrPoint = G.getHeight(v) - 1;
	unsigned v_lastRelevantDiscrPoint = u_time_iter - 1;
	unsigned w_firstRelevantDiscrPoint = G.getHeight(w) - 1;
	unsigned w_lastRelevantDiscrPoint = u_time_iter - 1;
	unsigned w_intervals = w_lastRelevantDiscrPoint - w_firstRelevantDiscrPoint + 1;

	for (unsigned v_time_iter = v_firstRelevantDiscrPoint; v_time_iter <= v_lastRelevantDiscrPoint; ++v_time_iter)
	  {
	    Real v_time = v_time_iter*(1.0/noOfDiscrIntervals);
	    Real v_timeFromParent = u_time - v_time;

	    Probability v_GammaProb = df->operator()(v->getLength()/v_timeFromParent);

	    Probability BDProb;
	    if (u->isRoot())
	      BDProb = 1;
	    else
	      BDProb = bdp.partialEdgeTimeProb(*s,u->getNumberOfLeaves(),u_time);

	    for (unsigned w_time_iter = w_firstRelevantDiscrPoint; w_time_iter <= w_lastRelevantDiscrPoint; ++w_time_iter)
	      {			
		Real w_time = w_time_iter*(1.0/noOfDiscrIntervals);
		Real w_timeFromParent = u_time - w_time;

		Probability w_GammaProb = df->operator()(w->getLength()/w_timeFromParent);

		Probability P_utu = BDProb * v_GammaProb * w_GammaProb * getValue(v_index,v_time_iter) * getValue(w_index,w_time_iter);
		P_utu_vec.push_back(P_utu);
		P_utu_sum += P_utu;

		if (w->isLeaf())
		  {
		    break;
		  }
	      }
	    if (v->isLeaf())
	      {
		break;
	      }
	  }

	Probability randLike = P_utu_sum * p;
	Probability compLike = 0;
	for (unsigned iter = 0; iter <= P_utu_vec.size()-1; ++iter)
	  {
	    compLike += P_utu_vec.at(iter);
	    //  cout << " iter: " << iter << " p: " << p << " compLike: " << compLike << "\n";

	    if (compLike > randLike)
	      {
		best_v_time_iter = iter/w_intervals + v_firstRelevantDiscrPoint;
		best_w_time_iter = (iter % w_intervals) + w_firstRelevantDiscrPoint;
		P_samp = P_utu_vec.at(iter);
		break;
	      }
	  }
	setValues(u_index,u_time_iter,P_samp,best_v_time_iter,best_w_time_iter);
      }

    P_max = getValue((noOfNodes-1),noOfDiscrIntervals);
    return P_max;
  }
}
