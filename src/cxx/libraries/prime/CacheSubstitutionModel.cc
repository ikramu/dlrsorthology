#include "CacheSubstitutionModel.hh"

#include "Node.hh"
#include "EdgeWeightHandler.hh"
#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "SequenceData.hh"
#include "TransitionHandler.hh"
#include "SiteRateHandler.hh"
#include "Tree.hh"

#include <sstream>
#include <cassert>
#include <memory>

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------
  CacheSubstitutionModel::CacheSubstitutionModel(const SequenceData& Data, 
						 const Tree& T, 
						 SiteRateHandler& siteRates, 
						 const TransitionHandler& Q,
						 EdgeWeightHandler& edgeWeights,
						 const vector<string>& partitionsList)
    : SubstitutionModel(Data, T, siteRates, Q, edgeWeights, partitionsList),
      likes(T),
      tmp(Q.getAlphabetSize())
  {
    //Create a template RateLike and PatternLike
    RateLike templ(siteRates.nCat(), LA_Vector(Q.getAlphabetSize()));
    PartitionLike pl;

    // Initiate element of pl
    for(PartitionVec::const_iterator i = partitions.begin();
	i != partitions.end(); i++)
      {
	pl.push_back(PatternLike(i->size(), templ));
      }
    likes = BeepVector<PartitionLike>(T, pl); // copy template
  }
  
  // Copy Constructor
  //----------------------------------------------------------------------
  CacheSubstitutionModel::CacheSubstitutionModel(const CacheSubstitutionModel& sm)
    :SubstitutionModel(sm),
     likes(sm.likes),
     tmp(sm.tmp)
  {
  }
  
  // Destructor
  //----------------------------------------------------------------------
  CacheSubstitutionModel::~CacheSubstitutionModel()
  {
  }

  // Assignment operator
  //----------------------------------------------------------------------
  CacheSubstitutionModel& 
  CacheSubstitutionModel::operator=(const CacheSubstitutionModel& sm)
  {
    if(this != &sm)
      {
	SubstitutionModel::operator=(sm);
	likes = sm.likes;
	tmp = sm.tmp;
      }
    return *this;
  }
  
  //----------------------------------------------------------------------
  //
  // Public interface
  //
  //----------------------------------------------------------------------

  // Likelihood calculation - interface to MCMCModels
  //----------------------------------------------------------------------
  Probability 
  CacheSubstitutionModel::calculateDataProbability()
  {
      //cout << *T << endl;
    Node* left = T->getRootNode()->getLeftChild();
    Node* right = left->getSibling();

    // reset return value
    like = 1.0;
    // Loop over partitions
    for(unsigned i = 0; i < partitions.size(); i++)
      {
	// First recurse down to initiate likes
	recursiveLikelihood(*left, i);
	recursiveLikelihood(*right, i);

	like *= rootLikelihood(i);
      }
    T->perturbedNode(0);
    return like;
  }

  // Partial Likelihood caluclation - for use with perturbedNode
  Probability 
  CacheSubstitutionModel::calculateDataProbability(Node* n)
  {
    assert(n != 0); // Check precondition

    // reset return value
    like = 1.0;

    // Loop over partitions
    for(unsigned i = 0; i < partitions.size(); i++)
      {
	// Perturbations in some classes (e.g., ReconciliationTimeMCMC) may
	// affect the outgoing edges of n, therefore we start by descending
	// one step
	if(!n->isLeaf())
	  {
	    updateLikelihood(*n->getLeftChild(), i);
	    updateLikelihood(*n->getRightChild(), i);
	  }
	// Then we just ascend up to root
	while(!n->isRoot())
	  {
	    updateLikelihood(*n, i);
	    n = n->getParent();
	    assert(n != 0); //Check for sanity
	  }
 	like *= rootLikelihood(i);
     }
    assert(like > 0);
    return like;
  }

  //------------------------------------------------------------------
  //
  // I/O
  // 
  //------------------------------------------------------------------

//   // TODO: Shape up these print functions /bens
  std::string
  CacheSubstitutionModel::print() const
  {
    return print(false);
  }
  
  std::string
  CacheSubstitutionModel::print(bool estimateRates) const
  {
    return "CacheSubstitutionModel: " + SubstitutionModel::print(estimateRates);
  }

//   std::string
//   CacheSubstitutionModel::print(const bool& estimateRates) const
//   {
//     ostringstream oss;
//     oss
//       << "Substitution likelihood is performed"
//       // Here we should maybe only give the part of data that we are using,
//       // i.e., the partitions used?
//       << " using sequence data:\n"
//       // The funciton below should be replaced by D.print(partition)
//       // that should write a "name" for the Sequence data and the 
//       // user-definedpartition that is used (e.g., either exactly which 
//       // positions are defined or the "name" used for the partition
//       << indentString(D->print(), "  ")
//       << indentString("partitions, any user-defined partitions of the data\n")
//       << "and substitution matrix:\n"
//       << indentString(Q->print());
//     return oss.str();
//   }


  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------
 
  // Helper functions for Likelihood calculation
  // Recurse down to leaves and then updates Likelihoods for each node on 
  // the way up
  //----------------------------------------------------------------------
  // \f$ \{ Pr[D_{i,\cdot}|T_n, w, r, p], r\in siteRates, p \in partitions[partition]\}\f$
  //----------------------------------------------------------------------
  Probability
  CacheSubstitutionModel::rootLikelihood(const unsigned& partition)
  {
    Node&n = *T->getRootNode();
    if(n.isLeaf())
      {
	return 1.0;
      }
    else
      {
	// Initiate return value
	Probability ret(1.0);

	// Set up data and likelihood storage
	PatternVec& pv = partitions[partition];
 	PatternLike& pl = likes[n][partition];

	// Get nested Likelihoods
	PatternLike& pl_l = likes[*n.getLeftChild()][partition];
	PatternLike& pl_r = likes[*n.getRightChild()][ partition];

	// Lastly, loop over all unique patterns in pv
	for(unsigned i = 0; i < pv.size(); i++)
	  {
	    Probability patternL = 0.0;

	    // Compute Pr[Dk|T, ew, r(j)] for each siteRateCategory j
	    for(unsigned j = 0; j < siteRates->nCat(); j++)
	      {
 		LA_Vector& current = pl[i][j];
		LA_Vector& left = pl_l[i][j];
		LA_Vector& right = pl_r[i][j];
		// left.*right, store result in tmp
		left.ele_mult(right, tmp);
		// pi * tmp, store result in current
		Q->multWithPi(tmp, current);
		patternL += current.sum();
	      } 
	    unsigned exp = pv[i].second; // # of occurrences of pattern
	    ret *= pow(patternL/ siteRates->nCat(), exp); // Pr[rateCat]=1/nCat
	  }
	return ret;
      }
  }

  void
  CacheSubstitutionModel::recursiveLikelihood(const Node& n, 
					      const unsigned& partition)
  {

    if(!n.isLeaf())
      {
	recursiveLikelihood(*n.getLeftChild(), partition);
	recursiveLikelihood(*n.getRightChild(), partition);
      }
    return updateLikelihood(n, partition);
  }
  
  void 
  CacheSubstitutionModel::updateLikelihood(const Node& n, 
					   const unsigned& partition)
  {
    if(n.isLeaf())
      return leafLikelihood(n, partition);
    else
      {
	// Get data and likelihood storage
	PatternVec& pv = partitions[partition];
	PatternLike& pl = likes[n][partition];

	// Get nested Likelihoods
	PatternLike& pl_l = likes[*n.getLeftChild()][partition];
	PatternLike& pl_r = likes[*n.getRightChild()][partition];

	// Compute Pr[Dk|T, ew, r(j)] for each siteRateCategory j
	for(unsigned j = 0; j < siteRates->nCat(); j++)
	  {
	    assert(edgeWeights->getWeight(n) > 0);
	    // Set up (j-specific) P matrix 
	    Q->resetP(edgeWeights->getWeight(n) * siteRates->getRate(j));
	
	    // Lastly, loop over all unique patterns in pv
	    for(unsigned i = 0; i < pv.size(); i++)
	      {
		LA_Vector& current = pl[i][j];
		LA_Vector& left = pl_l[i][j];
		LA_Vector& right = pl_r[i][j];
		// left.*right, store result in left
		left.ele_mult(right, tmp);
		Q->mult(tmp, current);
	      } 
	  }
	return;
      }
  }
  
  void
  CacheSubstitutionModel::leafLikelihood(const Node& n, 
					 const unsigned& partition)
  {
    // Set up data and likelihood storage
    PatternVec& pv = partitions[partition];
    PatternLike& pl = likes[n][partition];
    
      //Loop over rate categories
      for(unsigned j = 0; j < siteRates->nCat(); j++)
	{
	  // initiate P 
	  Q->resetP(edgeWeights->getWeight(n) * siteRates->getRate(j));
	
	  // Loop over each unique pattern in pv
	  for(unsigned i = 0; i < pv.size(); i++)
	    {
	      // Get position of pattern's first occurrence in partition
	      unsigned pos = pv[i].first; 
	      if(!Q->col_mult(pl[i][j], (*D)(n.getName(), pos)))
		Q->mult(D->leafLike(n.getName(),pos), pl[i][j]);
	      // 	      pl[i][j] = P * D->leafLike(n.getName(),pos);
	    }
	}

    return;
  }
  
    
}//end namespace beep
