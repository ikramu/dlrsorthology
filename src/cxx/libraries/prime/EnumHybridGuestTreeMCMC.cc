#include "EnumHybridGuestTreeMCMC.hh"

#include "HybridTree.hh"
#include "MCMCObject.hh"

namespace beep
{
  using namespace std;
  //--------------------------------------------------------------
  //
  // Construct/destruct/assign
  // 
  //--------------------------------------------------------------
  EnumHybridGuestTreeMCMC::EnumHybridGuestTreeMCMC(MCMCModel& prior, 
					   Tree& G, HybridTree& S,
					   StrStrMap& gs, BirthDeathProbs& bdp,
					   Real suggestRatio)
    : TreeMCMC(prior, G, G.getName() + "_" + bdp.getStree().getName() 
	       + "_HybridGuestTree", suggestRatio),
      EnumHybridGuestTreeModel(G, S, gs, bdp)
  {}
	
  EnumHybridGuestTreeMCMC::~EnumHybridGuestTreeMCMC()
  {};
    
  EnumHybridGuestTreeMCMC::EnumHybridGuestTreeMCMC(const EnumHybridGuestTreeMCMC& rtm)
    : TreeMCMC(rtm),
      EnumHybridGuestTreeModel(rtm)
  {}
    
  EnumHybridGuestTreeMCMC& 
  EnumHybridGuestTreeMCMC::operator=(const EnumHybridGuestTreeMCMC& rtm)
  {
    if(this != &rtm)
      {
	TreeMCMC::operator=(rtm);
	EnumHybridGuestTreeModel::operator=(rtm);
      }
    return *this;
  }
    
    
  //--------------------------------------------------------------
  //
  // Interface
  // 
  //--------------------------------------------------------------
   string 
  EnumHybridGuestTreeMCMC::print() const
  {
    return EnumHybridGuestTreeModel::print() + TreeMCMC::print();
  }
    
  Probability 
  EnumHybridGuestTreeMCMC::updateDataProbability()
  {
    if(G->perturbedTree() || S->perturbedTree())
      {
	update();
      }
    return calculateDataProbability();
  }
    
}//end namespace beep

