#ifndef EDGEDISCBDPROBS_HH
#define EDGEDISCBDPROBS_HH

#include <vector>

#include "Beep.hh"
#include "EdgeDiscTree.hh"
#include "PerturbationObservable.hh"
#include <sstream>

namespace beep
{

  // Forward declarations.
  class BirthDeathProbs;

  /**
   * Holder of birth and death rates, and related probabilities.
   * 
   * Handles probabilities concerning a birth-death process for an 
   * "edge discretized tree". More specifically, it contains 
   * precalculated probabilities for lineage survival and extinction 
   * in the same vein as BirthDeathProbs, although on (mostly) a 
   * point-to-point basis rather than node-to-node. See also the
   * documentation for BirthDeathProbs.
   * 
   * It will notify registered listeners when any of the two rates 
   * are changed. Includes caching functionality.
   * 
   * If the discretization/topology on which an instance is based
   * changes, the structure must be "rediscretized".
   *  
   * @author Joel Sj�strand.
   */
  class EdgeDiscBDProbs : public PerturbationObservable
  {
	
  public:
	
    /**
     * Constructor.
     * @param DS the discretized tree.
     * @param birthRate the birth rate of the process.
     * @param deathRate the death rate of the process.
     */
    EdgeDiscBDProbs(EdgeDiscTree* DS, Real birthRate, Real deathRate);
	
    /**
     * Copy constructor. Copies cache too.
     * @param probs the object to be copied.
     */
    EdgeDiscBDProbs(const EdgeDiscBDProbs& probs);
	
    /**
     * Destructor.
     */
    virtual ~EdgeDiscBDProbs();
    
    /**
     * Assignment operator. Copies cache too.
     */
    EdgeDiscBDProbs& operator=(const EdgeDiscBDProbs& probs);

    /**
     * Updates the precalculated internal values.
     */
    void update(bool rediscretize=false);
	
    /**
     * Returns the discretized tree on which this instance is based.
     * @return the discretized tree.
     */
    const EdgeDiscTree* getDiscTree() const
    {
      return m_DS;
    }
	
    /**
     * Returns the birth rate.
     * @return the birth rate.
     */
    inline Real getBirthRate() const
    {
      return m_birthRate;
    };
	
    /**
     * Returns the death rate.
     * @return the death rate.
     */
    inline Real getDeathRate() const
    {
      return m_deathRate;
    };
	
    /**
     * Returns the birth and death rates.
     * @param birthRate the birth rate.
     * @param deathRate the death rate.
     */
    void getRates(Real& birthRate, Real& deathRate) const
    {
      birthRate = m_birthRate;
      deathRate = m_deathRate;
    };
	
    /**
     * Sets new birth and death rates.
     * @param newBirthRate the new birth rate.
     * @param newDeathRate the new death rate.
     * @param doUpdate calls update() after setting new rates.
     */
    void setRates(Real newBirthRate, Real newDeathRate, bool doUpdate=true);
	
    /**
     * Returns the max allowed rate. Apparently there is (has been?)
     * a need for this since otherwise "our program will crash when
     * (birth_rate - death_rate) * t is too big".
     * @return the max allowed rate.
     */
    Real getMaxAllowedRate() const;

    /**
     * For an edge <parent(Y),Y> returns the (partial) probability of a single lineage
     * starting at parent(Y) having k descendants at Y of which only one is a
     * mortal (i.e. may survive to the leaves) whereas the remaining ones
     * are destined to degenerate. It is assumed that the lineage starts epsilon
     * below parent(Y) and ends epsilon above Y.
     * @param Y the lower node of the edge.
     * @return the one-to-one value.
     */
    inline Real getOneToOneProb(const Node* Y) const
    {
      EdgeDiscretizer::Point x = Y->isRoot() ?
	m_DS->getTopmostPt() : EdgeDiscretizer::Point(Y->getParent(), 0);
      EdgeDiscretizer::Point y(Y, 0);
      return m_BD_const(x, y);
    };
    
    /**
     * For a path, returns the (partial) probability of a single lineage starting
     * at upper point x having k descendants at lower point y
     * of which only one is a mortal (i.e. may survive to the leaves) whereas
     * the remaining ones are destined to degenerate. Should x or y correspond to
     * a node, it is assumed that the lineage starts epsilon below x and ends epsilon
     * above y.
     * @param x the upper point of the path.
     * @param y the lower point of the path.
     * @return the one-to-one value. Undefined if points are not on a time decreasing path.
     */
    //    inline 
    Real getOneToOneProb(const EdgeDiscretizer::Point& x,
			 const EdgeDiscretizer::Point& y) const
    {
      using namespace beep;
      using namespace std;
      return m_BD_const(x, y);
    };
	
    /**
     * For a planted subtree, returns the probability of a single lineage
     * starting at the top having no descendants at the leaves.
     * @param node the lower node of the edge at which top the lineage starts.
     * @return the extinction value.
     */
    inline Real getExtinctionProb(const Node* node) const
    {
      return m_BD_zero[node];
    }
	
    /**
     * Caches curr
ent values.
     */
    void cache();
	
    /**
     * Replaces current values with stored cache.
     */
    void restoreCache();

  private:
	
    /**
     * Recursive helper. Calculates probablities for pairs of points
     * within one edge. This applies (where P(t)=1-P_0(t) in Kendall's
     * notation):
     *   One-to-one ~ sum_(i=1:inf){ P(t)*(1-u_t)*u_t^(i-1)*D^(i-1)*i }
     * 	 Extinction ~ {1-P(t)} + sum_(i=1:inf){ P(t)*(1-u_t)*u_t^(i-1)*D^i }
     * The expressions simplify to the ones used in the method.
     * @param node the lower node of the edge.
     * @param doRecurse if true processes children too.
     */
    void calcProbsForEdge(const Node* node, bool doRecurse=true);

    /**
     * Computes p11 between inner points of two edges.
     * @param lowerPoint lower point.
     * @param upperPoint upper point.
     * @return p11 between the points.
     */
    Real computeInnerP11(const EdgeDiscretizer::Point& loPt, const EdgeDiscretizer::Point& upPt);
	
    /**
     * Recursive helper. For an edge e, calculates probabilities for points
     * on e to points on edges above. Within-edge probabilities must
     * be up-to-date before invoking, typically via a call to 'calcProbsForEdge(node)'.
     * @param node the lower node of the edge.
     * @param doRecurse if true processes children too.
     */
    void calcProbsForRootPath(const Node* node, bool doRecurse=true);
	
    /**
     * Helper. Computes P(t) and u_t for a given time. P(t) corresponds
     * to 1-P_0(t) in Kendall's notation.
     */
    void calcPtAndUt(Real t, Real& Pt, Real& ut) const;
	
  public:
	
  private:
	
    /** The discretized tree. */
    EdgeDiscTree* m_DS;
	
    /** Birth rate. */
    Real m_birthRate;
    Real m_birthRateOld;
	
    /** Death rate. */
    Real m_deathRate;
    Real m_deathRateOld;
	
    /**
     * One-to-one probabilities between pairs of points (x,y) where x >= y in the PO.
     * If x or y corresponds to a node, the probability refers to a lineage starting epsilon
     * below x and/or ending epsilon above y.
     */
    RealEdgeDiscPtPtMap m_BD_const;
		
    /**
     * Extinction probabilities. Each element refers to the planted subtree
     * rooted at the corresponding node.
     */
    RealVector m_BD_zero;
    RealVector m_BD_zeroOld;

  };

} // End namespace beep.

#endif /* EDGEDISCBDPROBS_HH */

