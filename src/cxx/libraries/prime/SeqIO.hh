//
// SeqIO
//
// Provides sequence reading functions
//

#ifndef SEQIO_HH
#define SEQIO_HH

#include <string>

extern "C" {
#include "sfile/sfile.h"
}

#include "SequenceData.hh"

namespace beep
{
  /// Class that with static methods that can be used to read sequences from file.
  class SeqIO
  {
  public:
    /// 
    static SequenceData readSequences(const std::string &filename);
    static SequenceData readSequences(const std::string &filename, const beep::SequenceType &dt);
    static SequenceData readSequences(const std::string &filename, const std::string &dts);

  private: 
    SeqIO();
    ~SeqIO();
    void importData(const std::string &filename);
    bool importDataFormat2(const std::string &filename);

  private:
    seq *slist;			// Temp storage of input
    std::vector<std::pair<std::string,std::string> > data;
    beep::SequenceType *guessedType; // Predicted type of data, DNA or AminoAcid
    Probability DNA_likelihood;
    Probability AA_likelihood;
  };
}//end namespace beep
#endif
