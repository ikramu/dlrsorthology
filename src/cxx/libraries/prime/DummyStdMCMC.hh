#ifndef DUMMYSTDMCMc_HH
#define DUMMYSTDMCMC_HH

#include <iostream>
#include <string>

#include "StdMCMCModel.hh"

//---------------------------------------------------------------
//
// classMCMCModel
// Baseclass that provides common implementation code 
// for the "MCMCModel-class family", which uses 
// MCMC to intergrate over parameters
//
// An instance of the DummyStdMCMC class will inherit from a 
// ProbabilityModel class for likelihood calculations. 
// It also contains a MCMCModel class for calculation of any 
// additional prior probs that might exists; give a DummyMCMC 
// in constructor if no such prior occur. These two classes 
// contain the parameters fro DummyStdMCMC to perturb.
// Typically, an instance class constructor lools like:
// InstanceMCMC::InstanceMCMC(MCMCModel& prior, ProbabilityModel& like)
//     : DummyStdMCMC(prior.getPRNG(), prior, 3, 0.4),
//       ProbabilityModel(like),
//       ...
//
// The main task for this class is to decide on when, on 
// suggestNewState()-calls, to pass this call on to the nested 
// prior classes and when to perturb the parameter of the 
// ProbabilityModel baseclass.
// The suggestRatio is a parameter that decides if a likelihood
// or a prior parameter should be updated:
// suggestRatio = pr{a}/pr{b}, where a = the average likelihood 
// parameter a is perturbed, b = the average prior parameter b 
// is perturbed. The attribute numParams is the number of local 
// parameters. If a likelihood parameter is to be perturbed, the 
// pure virtual' function perturbState() is called.
//
// This class currently inherits from MCMCModel, it is possible
// that it might eventually replace MCMCModel.
//
//---------------------------------------------------------------



namespace beep
{
  class DummyStdMCMC : public StdMCMCModel
  {
  public:
    //---------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    // 
    //---------------------------------------------------------------

    // TODO: Is this class ever used, and if it is, can we use 
    // DummyMCMC instead? /bens

    // Constructor
    // suggestRatio = pr{a}/pr{b}, a = a like parameter is perturbed, 
    // b = a prior parameter is pertubed. paramIdxRatio adjusts for 
    // unequal number of like and prior parameters.
    //---------------------------------------------------------------
    DummyStdMCMC();
    DummyStdMCMC(const DummyStdMCMC& A);
    virtual ~DummyStdMCMC();
    DummyStdMCMC& operator=(const DummyStdMCMC& A);

//     //---------------------------------------------------------------
//     //
//     // Standardized interface from MCMCModel
//     // You should never really need to change this!
//     // 
//     //---------------------------------------------------------------
//     MCMCObject suggestNewState();
//     Probability initStateProb();
//     Probability currentStateProb();
//     void commitNewState();
//     void discardNewState();

//     // Represent current state as a string - calls PM.strRepresentation()
//     // for states of nested priors
//     //----------------------------------------------------------------------
//     std::string strRepresentation();

//     // Print a header corresponding to this string
//     //----------------------------------------------------------------------
//     std::string strHeader();

//     // Return number of states in prior (probably only used first time!) 
//     //----------------------------------------------------------------------
//     unsigned nParams();

    //----------------------------------------------------------------------
    //
    // Internal interface
    //
    // For example (do = suggest/ commit / discard):
    //     virtual doState()
    //     {
    //       Real Idx = paramIdx / paramIdxRatio;
    //       if(Idx < x)
    // 	       do x();
    //       else if(Idx < y)
    // 	       do y();
    //       else
    // 	       do z();
    //     }
    //
    //     virtual ownStrRep()
    //     {
    //       std::ostringstream oss;
    //       oss << x << " "
    // 	         << y << " "
    // 	         << z << " "
    // 	         ;
    //       return oss.str();
    //     }
    //
    //----------------------------------------------------------------------
    virtual MCMCObject suggestOwnState();
    virtual void commitOwnState();
    virtual void discardOwnState();

    virtual std::string ownStrRep() const;
    virtual std::string ownHeader() const;

    // I have lumped calls to p.update() and p.calculateDataProbability(),
    // where p is the ProbabilityModel "baseclass", because I think that they
    // will almost always be called together
    //----------------------------------------------------------------------
    virtual Probability updateDataProbability();
    
    //----------------------------------------------------------------------
    //
    // I/O
    // I have not converged on a consensus for this yet, but the 
    // following functions should suffice
    //
    //-----------------------------------------------------, dm, 0, 0.5);-----------------
    friend std::ostream& operator<<(std::ostream &o, const DummyStdMCMC& A);
    virtual std::string print() const;
    
  protected:
    //---------------------------------------------------------------
    //
    // Attributes
    // 
    //---------------------------------------------------------------
  };
}//end namespace beep

#endif
