/* 
 * File: MpiMultiGSR.cc Author: bens
 *
 * Created on den 4 mars 2010, 18:56
 */
#include "MpiMultiGSR.hh"

#include "MCMCObject.hh"
#include "Probability.hh"
#include "TreeIO.hh"
#include "TreeIOTraits.hh"
#include "mpicxx.h"
#include "TreePerturbationEvent.hh"
#include <iostream>
#include <sstream>
#include <fstream>


namespace beep
{
  using namespace std;

  SeriGSRvars::SeriGSRvars() 
    : geneFam(-1), 
      G(""),
      lambda(-1),
      mu(-1),
      m(-1),
      v(-1)
  {}

  SeriGSRvars::SeriGSRvars(unsigned geneFam, std::string G, 
			   double lambda, double mu,
			   double m, double v)
    : geneFam(geneFam), 
      G(G),
      lambda(lambda),
      mu(mu),
      m(m),
      v(v)
  {
  }

  SeriGSRvars::~SeriGSRvars()
  {}

  SeriGSRvars::SeriGSRvars(const SeriGSRvars& orig)
    : geneFam(orig.geneFam), 
      G(orig.G),
      lambda(orig.lambda),
      mu(orig.mu),
      m(orig.m),
      v(orig.v)
  {
  }
  
  SeriGSRvars&
  SeriGSRvars::operator=(const SeriGSRvars& orig)
  {
    if(&orig != this)
      {
	geneFam = orig.geneFam;
	G=orig.G;
	lambda = orig.lambda;
	mu = orig.mu;
	m = orig.m;
	v = orig.v;
      }
    return *this;
  }

  template<class Archive> 
  void 
  SeriGSRvars::serialize(Archive & ar, const unsigned int version) 
  {
    ar & geneFam;
    ar & G;
    ar & lambda;
    ar & mu;
    ar & m;
    ar & v;
  }
  
  SeriMultiGSRvars::SeriMultiGSRvars()
    : S(""),
      Gvars()
  {}
  
  SeriMultiGSRvars::~SeriMultiGSRvars()
  {}
  
  SeriMultiGSRvars::SeriMultiGSRvars(const SeriMultiGSRvars& orig)
    : S(orig.S),
      Gvars(orig.Gvars)
  {}

  SeriMultiGSRvars&
  SeriMultiGSRvars::operator=(const SeriMultiGSRvars& orig)
  {     
    if(&orig != this)
      {
	S = orig.S;
	Gvars = orig.Gvars;
      }
    return *this;
  }

  void 
  SeriMultiGSRvars::clear()
  {
    S = "";
    Gvars.clear();
  }
  
  template<class Archive> 
  void 
  SeriMultiGSRvars::serialize(Archive & ar, const unsigned int version) 
  {
    ar & S;
    ar & Gvars;
  }
  
  
  // Construct/destruct/assign
  //---------------------------
  MpiMultiGSR::MpiMultiGSR(MCMCModel& prior, EdgeDiscTree& DS,
			   mpi::communicator& world, 
			   const Real& suggestRatio)
    : StdMCMCModel (prior, 0, "multiGSR", suggestRatio),
      DS(&DS),
      geneFams(),
      trees(),
      bds(),
      rateDensities(),
      vars(),
      subIdx(0),
      world(world)
  {
    ostringstream oss;
    oss << "MULTIGSR_" << world.rank();
    name = oss.str();
  }
  
  MpiMultiGSR::~MpiMultiGSR()
  {}
  
  // Interface
  //--------------------------
  void
  MpiMultiGSR::addGeneFamily(SubstitutionMCMC& like, TreeMCMC& genetree,
			     EdgeDiscBDMCMC& bdm, Density2PMCMC& dens,
			     bool isMaster)
  {
    geneFams.push_back(&like);
    trees.push_back(&genetree);
    bds.push_back(&bdm);
    rateDensities.push_back(&dens);
    // Update StdMCMCModel
    n_params += like.nParams();
    updateParamIdx();

    if(isMaster)
      {
	geneFams.back()->initStateProb();
	updateGvars(geneFams.size()-1);
	update();
      }
    else
      {
	updateSlave();
      }
  }
  
  
  MCMCObject 
  MpiMultiGSR::suggestOwnState()
  {
    subIdx = R.genrand_modulo(geneFams.size());
    MCMCObject MOb = geneFams[subIdx]->suggestNewState ();
#ifdef DEBUGMPIMULTIGSR
    //    MOb.stateProb ==  geneFams[subIdx]->initStateProb(); // XXX CHeck if needed
    if(MOb.stateProb != geneFams[subIdx]->initStateProb())
      {
	cerr << "Houston, we've got a problem" << endl;
	cerr << MOb.stateProb <<" != "<<geneFams[subIdx]->updateDataProbability()<< endl;
	throw AnError("Houston, we've got a problem", 1);
      }
#endif
    MOb.stateProb += calcDataProbability(subIdx);
#ifdef DEBUGMPIMULTIGSR
    Probability ret2 = 1.0;
    for (unsigned i = 0; i < geneFams.size (); i++)
      {
	ret2 *= geneFams[i]->initStateProb();
      }    
    if(((MOb.stateProb - ret2)/MOb.stateProb) > Probability(0.001))
      {
	ostringstream oss;
	oss << "ret computd using slaves, " << MOb.stateProb << " != " << ret2 << ", ret2 computed using master only, (ret - ret2)/ret = " << ((ret2-MOb.stateProb)/ret2).val();
	cerr << oss.str();
	throw AnError(oss.str(), 1);
      }
#endif
    return MOb;
  }
  
  void 
  MpiMultiGSR::updateGvars(unsigned geneFamIdx)
  {
    if(world.size() > 0)
      {
	if(geneFamIdx <0)
	  {
	    cerr << world.rank() 
		 << " updateGvars,  genefamIdx = " 
		 << geneFamIdx 
		 << endl;
	    throw AnError("shit", 1);
	  }
	TreeIO io;
	vector<SeriGSRvars>& gvars = vars.Gvars;
	gvars.push_back(SeriGSRvars(geneFamIdx, 
				    io.writeGuestTree(trees[geneFamIdx]->getTree()),
				    bds[geneFamIdx]->getModel().getBirthRate(), 
				    bds[geneFamIdx]->getModel().getDeathRate(),
				    rateDensities[geneFamIdx]->getModel().getMean(), 
				    rateDensities[geneFamIdx]->getModel().getVariance()
				    )
			);
      }
  }

  Probability 
  MpiMultiGSR::updateDataProbability()
  {
    update();
    // All gene families should be treated, set excludeGF to 
    // an index > maxIndex
    return calcDataProbability(geneFams.size());
  }
  
  void 
  MpiMultiGSR::commitOwnState ()
  {
    updateGvars(subIdx);
    update();
  }
  
  void 
  MpiMultiGSR::discardOwnState ()
  {
    geneFams[subIdx]->discardNewState();
    //DEBUG Needed?
//     geneFams[subIdx]->initStateProb();
  }
  
  std::string 
  MpiMultiGSR::ownStrRep() const
  {
    ostringstream oss;
    for(unsigned i = 0; i < geneFams.size(); i++)
      {
  	oss << geneFams[i]->ownStrRep();
 	oss << trees[i]->ownStrRep();
 	oss << bds[i]->ownStrRep();
 	oss << rateDensities[i]->ownStrRep();
      }
    TreeIO tio;
    oss << tio.writeHostTree(DS->getTree()) << "\t";
    return oss.str();
  }
  
  std::string 
  MpiMultiGSR::ownHeader () const
  {
    ostringstream oss;
    for(unsigned i = 0; i < geneFams.size(); i++)
      {
  	oss << geneFams[i]->ownHeader();
 	oss << trees[i]->ownHeader();
 	oss << bds[i]->ownHeader();
 	oss << rateDensities[i]->ownHeader();
      }
    oss << "S(Tree)\t";
    return oss.str();
  }
  
  Probability 
  MpiMultiGSR::calcDataProbability(unsigned excludeGF)
  {
    std::vector<Probability> p(geneFams.size(), 1.0);
    mpi::request reqs[2 * geneFams.size()];
    int process = 1;
    unsigned firstjob = 0;
    unsigned lastjob = 0;
    if(world.size() == 1)
      {
	for (unsigned i = 0; i < geneFams.size (); i++)
	  {
	    p[i] = geneFams[i]->initStateProb();//updateDataProbability();
	  }
      }
    else 
      {
	for (unsigned i = 0; i < geneFams.size (); i++)
	  {
	    if(i != excludeGF)
	      {
		reqs[lastjob++] = world.isend(process, SCALC, i);
		reqs[lastjob++] = world.irecv(process, RCALC1, p[i]);
#ifdef DEBUGMPIMULTIGSR
		process = 1;
		cerr << endl << "slave " << process << " computing p["<<i<<"]"<< endl;
		mpi::request reqs;
		reqs = world.isend(process, SCALC, i);
		reqs.wait();
		mpi::request reqr;
		reqr = world.irecv(process, RCALC1, p[i]);
		reqr.wait();
		cerr << endl;
		cerr << endl << "master computing p["<<i<<"]"<< endl;
		Probability ret2 = geneFams[i]->initStateProb();
		cerr << endl;
		if(((p[i] - ret2)/p[i]) > 0.001)
		  {
		    ostringstream oss;
		    oss << "p[" << i << "] computed using slaves, " << p[i] << " != " << ret2 << ", ret2 computed using master only, (p["<<i<<"] - ret2)/p["<<i<<"] = " << ((p[i] - ret2)/p[i]).val();
		    cerr << oss.str();
		    throw AnError(oss.str(), 1);
		  }
#endif
		process++;
		if(process >= world.size())
		  {
		    if(i < geneFams.size()-1)
		      {
			p[++i] = geneFams[i]->initStateProb();//updateDataProbability();
		      }
		    mpi::wait_all(reqs+firstjob, reqs+lastjob); 
		    firstjob = lastjob;
		    process = 1;
		  }
	      }
	  }
	mpi::wait_all(reqs+firstjob, reqs+lastjob);
      }
    Probability ret = 1.0;
#ifdef DEBUGMPIMULTIGSR
    Probability ret2 = 1.0;
    Probability p2;
#endif
    for (unsigned i = 0; i < geneFams.size(); i++)
      {
	if(i != excludeGF)
	  {
	    ret *= p[i];
#ifdef DEBUGMPIMULTIGSR
	    p2 = geneFams[i]->initStateProb();
	    ret2 *= p2;
	    cerr << i << " p with slaves, " << p[i] << "  vs. " << p2 <<  " p2 with master, " << endl;
	    cerr << i << " ret with slaves, " << ret << "  vs. " << ret2 <<  " ret2 with master, " << endl;
#endif
	  }
      } 
    return ret;
  }
  
  
  void 
  MpiMultiGSR::update()
  {
    if(world.size() > 1)
      {
	TreeIO io;
	TreeIOTraits traits;
	vars.S = io.writeHostTree(DS->getTree());
	mpi::request reqs[world.size()]; 
	
#ifdef DEBUGMPIMULTIGSR
	for(unsigned i = 0; i < vars.Gvars.size(); i++)
	  {
	    SeriGSRvars& s = vars.Gvars[i];
	  }
#endif
	
	for(int i = 1; i < world.size(); i++)
	  {
	    reqs[i] = world.isend(i, UPDATE, vars);
	  }
	mpi::wait_all(reqs+1, reqs+world.size());
	vars.clear();
      }
  }
  
  void 
  MpiMultiGSR::stopSlaves()
  {
    mpi::request reqs[world.size()]; 

    for(int i = 1; i < world.size(); i++)
    {
      reqs[i] = world.isend(i, STOP, 0);
    }    
    mpi::wait_all(reqs+1, reqs+world.size());
  }

  void 
  MpiMultiGSR::waitingSlaves()
  {
//     std::ofstream os;
//     std::streambuf* cout_buf = NULL;
//     if (cout_buf)  // Already have a file open. Close and reassign.
//       {
// 	os.close();
// 	cout.rdbuf(cout_buf);
//       }

//     os.open("MpiMultiGSR_slave_out file.txt");
//     cout_buf = cout.rdbuf();
//     cout.rdbuf(os.rdbuf());

    while(true)
      {
	mpi::status msg = world.probe();
	if(msg.tag() == SCALC)
	  {
 	    unsigned myIdx;
  	    mpi::request req;
  	    req = world.irecv(0, SCALC, myIdx);
  	    req.wait();
#ifdef DEBUGMPIMULTIGSR
  	    Probability p = geneFams[myIdx]->initStateProb();
#else
   	    Probability p = geneFams[myIdx]->updateDataProbability();
#endif
	    sleep(1);
     	    req = world.isend(0, RCALC1, p);
   	    req.wait();
	  }
	else if(msg.tag() == STOP)
	  {
	    int sig;
	    mpi::request reqs = world.irecv(0, STOP, sig);
	    break;
	  }
	else if(msg.tag() == UPDATE)
	  {
	    updateSlave();
	  }
      }
    return;
  }

  void 
  MpiMultiGSR::updateSlave()
  {
    // Receive the data
    mpi::request req = world.irecv(0, UPDATE, vars);
    req.wait();

    // extract the data
    if(vars.S != "")
      {
	// Turn off notifications just to be sure.
	Tree& S = DS->getTree();
	bool notifStat = S.setPertNotificationStatus(false);
	TreeIO tio = TreeIO::fromString(vars.S);
	Real topTime = S.getTopTime();
	S = tio.readHostTree();
	S.setTopTime(topTime);
	// Notify listeners.
	S.setPertNotificationStatus(notifStat);
	PerturbationEvent pe(PerturbationEvent::PERTURBATION);
	S.notifyPertObservers(&pe);
      }
    for(unsigned i = 0; i < vars.Gvars.size(); i++)
      {
	SeriGSRvars& s = vars.Gvars[i];
	// G
	TreeIO tio = TreeIO::fromString(s.G);
	trees[s.geneFam]->updateToExternalPerturb(tio.readGuestTree());
	// Birth-death
	bds[s.geneFam]->updateToExternalPerturb(s.lambda, s.mu);
	// rate vars
	rateDensities[s.geneFam]->updateToExternalPerturb(s.m, s.v);
	geneFams[s.geneFam]->initStateProb();
      }
    vars.clear();
  }
  
  string 
  MpiMultiGSR::print() const
  {
    ostringstream oss;
    oss << "Parallelized, multi-gene version of GSR, the nested"
	<< "GSR classes are.";
    for (unsigned i = 0; i < geneFams.size (); i++)
      {
	oss << indentString(geneFams[i]->print());
      }
    oss << StdMCMCModel::print();
    return oss.str();
  }



}// end namespace beep

