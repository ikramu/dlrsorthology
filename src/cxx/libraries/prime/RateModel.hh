#ifndef RATEMODEL_HH
#define RATEMODEL_HH

#include <iostream>
#include <sstream>

#include "Beep.hh"
#include "ProbabilityModel.hh"

//----------------------------------------------------------------------
//
// class RateModel
// Virtual base class defining a common interface for RateModels
//
// There are then two types of subclasses (defined in separate files):
// 1) SiteRateModels, that models rate variation over sites, and
// 2) EdgeRateModels (of which there are further subtypes), that
//    models rate variation over edges in a tree.
// SiteRateModels can contain an EdgeRateModel to allow simultaneous
// modeling of rate variation over sites and edges. EdgeRateModels can 
// contain further EdgeRateModels to allow nested modeling of edge 
// rates, e.g., to model lineage-specific rate components over several 
// gene trees.
//
//  Author Bengt Sennblad, copyright the MCMC club, SBC
//
//----------------------------------------------------------------------


namespace beep
{
  // Forward declarations 
  class DensityFunction2P;
  class Node;
  class Probability;

  class RateModel : public ProbabilityModel
  {
  public:
    RateModel(){};
    RateModel(const RateModel& rm){};
    virtual ~RateModel() {};          //Destructor must be virtual
    RateModel& operator=(const RateModel& rm){};

  public:
    //----------------------------------------------------------------------
    //
    // Interface
    // Provides default return values for subclasses
    //
    //----------------------------------------------------------------------

    // Interface inherited from ProbabilityModel - likelihood default to 1.0
    //----------------------------------------------------------------------
    virtual Probability calculateDataProbability() {return 1.0;};
    virtual void update() {};

    // returns number of site-rate categories - default is set to one
    //----------------------------------------------------------------------
    virtual unsigned nCat() const {return 1;};

    // returns rate for category rate_cat, Node node - default strict mol.clock
    //----------------------------------------------------------------------
    virtual Real operator()(const Node& node, const unsigned& rate_cat) const
    {return 1.0;};

    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& o, const RateModel& rm)
    {
      return o << rm.print();
    };
    
    virtual std::string print() const
    {
      std::ostringstream oss;
      oss << "None\n";
      return oss.str();
    };

  };

}//end namespace beep

#endif
