#ifndef PROBABILITYMODEL_HH
#define PROBABILITYMODEL_HH

#include <string>

#include "Beep.hh"
#include "Probability.hh"

namespace beep
{
  //------------------------------------------------------------------
  //
  // class ProbabilityModel
  //! Abstract base-class defining an interface for probability
  //! calculatuions in MCMC-implementations. 
  //!
  //! The expected use of this interface is as follows (in pseudo code)
  //! -# Set up a model (subclass to ProbabilityModel)
  //! -# Connect this to either a Generator or a MCMCModel subclass
  //!
  //! Attributes of this model will often be shared by an MCMCModel, 
  //! which is responsible for perturbing these attributes
  //! Author: Lars Arvestad, Bengt Sennblad, SBC, � the MCMC-club, SBC, 
  //! all rights reserved
  //
  //------------------------------------------------------------------

  class ProbabilityModel
  {
  public:
    //------------------------------------------------------------------
    //
    // Construct/Destruct
    //
    //------------------------------------------------------------------
    ProbabilityModel();
    ProbabilityModel(std::string name);
    virtual ~ProbabilityModel();
    ProbabilityModel(const ProbabilityModel& pm);
    ProbabilityModel& operator=(const ProbabilityModel& pm);    

    //------------------------------------------------------------------
    //
    // Interface to MCMCModel
    //
    //------------------------------------------------------------------

    //! Calculates Pr(Data | Model) for current values of Data and 
    //! Model parameters. 
    //------------------------------------------------------------------
    virtual Probability calculateDataProbability() = 0;

    //! Stores / Resets current state 
    //------------------------------------------------------------------

    //
    //! Implement update() to ensure that internal help structures
    //! are up-to-date when an input parameter changes.
    //------------------------------------------------------------------
    virtual void update() = 0;

    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------

    // Always define an ostream operator!
    // This should provide a neat output describing the model and its 
    // current settings for output to the user. It should list current 
    // parameter values by linking to their respective ostream operator
    //------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const ProbabilityModel& pm);
    virtual std::string print() const;
    
    // Added so that e.g. an MCMC class may output parameters of
    // its model when iterating.
    virtual bool hasOwnStatus() { return false; };
    virtual std::string ownStatusHeader() { return ""; };
    virtual std::string ownStatusStrRep() {return ""; };

  protected:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------
    Probability like;       //!< Latest calculated likelihood
  };
}//end namespace beep

#endif
