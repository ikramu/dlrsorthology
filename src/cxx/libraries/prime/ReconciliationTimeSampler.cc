#include <sstream>

#include "AnError.hh"
#include "ReconciliationTimeSampler.hh"


namespace beep
{
  using namespace std;
/**
 * Create a time sampler. The parameter gs is not used.
 */
   ReconciliationTimeSampler::
   ReconciliationTimeSampler(Tree &G_in, BirthDeathProbs& bdp_in, 
			     GammaMap& gamma_in)
    : G(&G_in),
      S(&bdp_in.getStree()),
      bdp(&bdp_in),
      gamma(&gamma_in),
      R(),
      table(G->getNumberOfNodes()),
      shortestT(-1)
  {
    if(G->hasTimes() == false)
      {
	G->setTimes(*new RealVector(*G));
      }
    recursiveUpdateTable(*G->getRootNode());
  };

  // Construct using an existing RecipnciliationModel (subclass)
  ReconciliationTimeSampler::ReconciliationTimeSampler(ReconciliationModel& rs)
    : G(&rs.getGTree()),
      S(&rs.getSTree()),
      bdp(&rs.getBirthDeathProbs()),
      gamma(&rs.getGamma()),
      R(),
      table(G->getNumberOfNodes())   
//  ReconciliationSampler(G, gs, bdp, R),
  {
    if(G->hasTimes() == false)
      {
	G->setTimes(*new RealVector(*G));
      }
    recursiveUpdateTable(*G->getRootNode());
  };

  // Copy constructor
  //----------------------------------------------------------------------  
  ReconciliationTimeSampler
  ::ReconciliationTimeSampler(const ReconciliationTimeSampler& RTS)
    : G(RTS.G),
      S(RTS.S),
      bdp(RTS.bdp),
      gamma(RTS.gamma),
      R(),
      table(RTS.table)
  {
  };

  //Destructor
  //----------------------------------------------------------------------  
  ReconciliationTimeSampler::~ReconciliationTimeSampler()
  {
  }

  // Assignment operator
  //----------------------------------------------------------------------  
  ReconciliationTimeSampler& 
  ReconciliationTimeSampler::operator=(const ReconciliationTimeSampler& RTS)
  {
    if(this != &RTS)
      {
	G = RTS.G;
	S = RTS.S;
	bdp = RTS.bdp;
	gamma = RTS.gamma;
	//	R = RTS.R;
	table = RTS.table;
      }
    return *this;
  };


  //----------------------------------------------------------------------
  //
  // Public interface
  //
  //----------------------------------------------------------------------
  // Set Shortestg allowed edge time -- used in fastGEM
  void
  ReconciliationTimeSampler::setShortestT(Real value)
  {
    shortestT = value;
  }  
  
  // Acess shared attributes
  //----------------------------------------------------------------------
  Tree& 
  ReconciliationTimeSampler::getGTree()
  {
    return *G;
  }

  // Update if gamma has changed
  void 
  ReconciliationTimeSampler::update()
  {
    recursiveUpdateTable(*G->getRootNode());
  }

  // Main interface - get the times
  //----------------------------------------------------------------------
  Probability 
  ReconciliationTimeSampler::sampleTimes(bool includeRootEdge)
  {
    Node* Sroot = bdp->getStree().getRootNode();
    Probability P = 0;
    unsigned trial = 0;
    while(P == 0)
      {
	if(trial > 1000000)
	  {
	    throw AnError("ReconciliationTimeSampler::sampleTimes\n"
			  "Failure to sample times with the given shortestT", 1);
	  }
	P = 1.0;
	if(includeRootEdge)
	  {
	    P *= recursiveTimeGeneration(G->getRootNode(), Sroot, 
					 S->getTopTime());
	  }
	else
	  {
	    // This is the proper way of doing it
	    Node* Groot = G->getRootNode();
	    Groot->setNodeTime(S->getTopTime()+ S->getTime(*Sroot));
	    
	    P *= recursiveTimeGeneration(Groot->getLeftChild(), Sroot,S->getTopTime());
	    P *= recursiveTimeGeneration(Groot->getRightChild(),Sroot,S->getTopTime());
	  }
#ifdef PERTURBED_TREE
	// We have changed all edges => all SubstLike needs to be recalculated
	G->perturbedTree(true);
#elif PERTURBED_NODE
	// We have changed all edges => all SubstLike needs to be recalculated
	G->perturbedNode(G->getRootNode());
#endif
	trial++;
      }
    return P;
  }

  //----------------------------------------------------------------------
  //
  //I/O
  //
  //----------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, 
	     const ReconciliationTimeSampler& RTS)
  {
    return o << "ReconciliationTimeSampler.\n"
	     << "A class for intergrating substitution rate probabilities\n"
	     << "over underlying arc-times, by sampling from a prior of the"
	     << "arc-times\n"
      //Add indentation here, e.g., <<indent = original_indent + 3
	     << RTS.print();
  };

  
  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------

  // generates times for each gene node conditioned on that the time 
  // available for slice sn is maxT
  // use of shortestTime is only tested for Species trees with single edge
  //----------------------------------------------------------------------

  Probability
  ReconciliationTimeSampler::recursiveTimeGeneration(Node* gn, Node* sn, 
						     Real maxT)
  {
    assert(gn != 0 && sn != 0 && maxT >0);

    // Initiate variables for passing recursion on
    Node* gl = gn;
    Node* gr = gn;
    Node* sl = sn;
    Node* sr = sn;
    Real maxTL = 0;
    Real maxTR = 0;
    Probability P = 1.0;

    // if gn has gamma-maps gn is either a speciation, whose edge may cover 
    // one or several slices, or a duplication whose edge stretches cover 
    // at least two slices
    if(gamma->numberOfGammaPaths(*gn) > 0)          
      {
	Node* acn = gamma->getLowestGammaPath(*gn); // record the lowest slice
	
	// If gn is a speciation, then we reached the bottom of the 
	// lowest slice. The time of gn is at least maxT
	if(gamma->isSpeciation(*gn))                
	  {
	    G->setTime(*gn, S->getTime(*acn)); // set new time
	    if(!gn->isLeaf()) 
	      {   
		//set params for passing on recursion
		gl = gn->getLeftChild(); 
		gr = gl->getSibling();
		sl = &gamma->getDominatingChild(*acn, *gl);
		sr = sl->getSibling();
		maxTL = S->getEdgeTime(*sl);
		maxTR = S->getEdgeTime(*sr);
		assert(maxTL > 0);
		assert(maxTR > 0);
	      }
	  }
	// If gn is a duplication, its time is at least maxT plus a 
	// sampled time in the lowest slice
	else                      
	  {                       
	     // set params for passing on recursion
	     if(!gn->isLeaf()) 
	     {   
		gl = gn->getLeftChild();
		gr = gl->getSibling();
	     }
	     sl = sr = gamma->getLineage(acn, *gn);
	     if(sl != sn)
	     {
		maxT = S->getEdgeTime(*sl);	    
	     }

	    // Generate next duplication time
	    Real p = R.genrand_real3();
	    unsigned trials = 0;
	    Real nexT = -1.0;
	    Real parentT = S->getTopTime();
	    if(gn->isRoot() == false)
	      {
		parentT = G->getTime(*gn->getParent());
	      }
	    do
	      {
		if(trials > 1000000)
		  {
		    nexT = parentT - shortestT - S->getTime(*sl);
		    if(nexT <=0)
		      {
			return 0;
		      }
		    break;
		  }
		nexT = bdp->generateEdgeTime(*sl, table[gn], p, maxT);
		trials++;
	      }
	    while(parentT - (S->getTime(*sl) + nexT) < shortestT);
	    assert(nexT > 0);
	    G->setTime(*gn, S->getTime(*sl) + nexT); // set new time
	    P = bdp->partialEdgeTimeProb(*sl, table[gn], nexT);
	    // set further params for passing on recursion
	    maxTL = maxTR = nexT;
	    assert(maxTL > 0);
	    assert(maxTR > 0);
	  }
      }
    // If there is no gamma maps, gn is simply a duplication whose edge only
    // covers the sn slice. gn's edge time can be sampled.
    else                       
      {                        
	// sample edge time set params for passing on recursion
	Real p = R.genrand_real3();
	unsigned trials = 0;
	Real nexT = -1.0;
	Real parentT = S->getTopTime();
	if(gn->isRoot() == false)
	  {
	    parentT = G->getTime(*gn->getParent());
	  }
	do
	  {
	    if(trials > 1000000)
	      {
		nexT = parentT - shortestT - S->getTime(*sl);
		if(nexT <=0)
		  {
		    return 0;
		  }
		break;
	      }
	    nexT = bdp->generateEdgeTime(*sn, table[gn], p, maxT);
	    trials++;
	  }
	while(parentT - (S->getTime(*sn) + nexT) < shortestT);
	assert(nexT > 0);
	G->setTime(*gn, S->getTime(*sn) + nexT); // set new time
	P = bdp->partialEdgeTimeProb(*sn, table[gn], nexT);

	if(!gn->isLeaf()) 
	{   
	   gl = gn->getLeftChild();
	   gr = gl->getSibling();
	}
	
	sl = sr = sn;
	maxTL = maxTR = nexT;
	assert(maxTL > 0);
	assert(maxTR > 0);
      }

    // It now remains to set top Time of root
    if(gn->isRoot())
      {
	G->setTopTime(S->getTime(*S->getRootNode()) + 
		      S->getTopTime() - G->getTime(*gn));
      }

    // Pass on recursion in the appropriate way
    if(!gn->isLeaf())          
      {
	if(maxTL < 0)
	  {
	    cerr << "maxT (= " << maxTL << ") became less than 0 for guest node \n"
		 << gl
		 << " and host node"
		 << sl;
	    throw PROGRAMMING_ERROR("Bad programming");
	  }
	if(maxTR < 0)
	  {
	    cerr << "maxT (= " << maxTR << ") became less than 0 for nodes \n"
		 << gr
		 << " and "
		 << sr;
	    throw PROGRAMMING_ERROR("Bad programming");
	  }
	P *= recursiveTimeGeneration(gl, sl, maxTL);
	P *= recursiveTimeGeneration(gr, sr, maxTR);
      }	  
    return P;
  };
  
  // Counts for each gene node the number of children in the
  // lowest gamma below or at same level
  //----------------------------------------------------------------------
  unsigned
  ReconciliationTimeSampler::recursiveUpdateTable(Node& gn)
  {
    if(!gn.isLeaf()) 
      {
	//Pass on recursion
	unsigned leaves = recursiveUpdateTable(*gn.getLeftChild()) +
	  recursiveUpdateTable(*gn.getRightChild());
      
	if(gamma->isSpeciation(gn)) //speciations always get 1
	  {
	    table[gn] = 1;
	  }
	else // Otherwise use value from recursion
	  {
	    table[gn] = leaves;
	  
	    // pass back current number of leaves
	    if(gamma->numberOfGammaPaths(gn) == 0)
	      {
		return leaves;
	      }
	  }
      }
    else  
      {
	table[gn] = 1;
      }
    // Leaves and speciations are terminal in slices
    return 1;
  };


  //----------------------------------------------------------------------
  //
  //I/O
  //
  //----------------------------------------------------------------------

  // print function for parameters used in operator<<
  //
  // THIS MUST BE UPDATED!!
  //--------------------------------------------------------------------
  const std::string 
  ReconciliationTimeSampler::print() const
  {
    std::ostringstream os;
    os     << "Parameters:\n"
	   << "G (gene tree):\n"
      //here should be added a G->tree4os statement when this becomes available
	   << "\n"
	   << "gamma (reconciliation betweeen S and G):\n"
      //here should be added a gamma->gamma4os statement when this becomes available
	   << gamma
	   << "\n"
	   << "table (# leaves in G_{u,gamma(y)}, u in V(G), y in V(S)):\n"
	   << table4os()
	   << "\n"
	   << "R (a random sampler)"
	   << "\n"
	   << "\n";
      
    return os.str();
  }

  // helper print function for table
  //--------------------------------------------------------------------
  const std::string
  ReconciliationTimeSampler::table4os() const
  {
    std::ostringstream os;
    typedef std::map<unsigned, unsigned>::const_iterator MapIter;
    os << "------------------------------------\n";
    for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
      {
	os << "  Node " 
	   << i
	   << "\t"
	   << table[G->getNode(i)]
	   << " leaves\n";
      }
    os << "------------------------------------\n";
    return os.str();
  };

}//end namespace beep
