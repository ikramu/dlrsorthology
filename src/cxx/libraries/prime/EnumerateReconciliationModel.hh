#ifndef ENUMERATERECONCILIATIONMODEL_HH
#define ENUMERATERECONCILIATIONMODEL_HH

#include "ReconciledTreeModel.hh"

namespace beep
{
  //------------------------------------------------------------------------
  //
  //! Class EnumerateReconciliationModel allows counting of number of 
  //! possible reconciliations between the given gene tree and species
  //! tree. It can also assign unique numbers to these reconciliations
  //! and thereby allows enumerating them and, as it inherits from
  //! ReconciledTreeModel, to compute their probability.
  //!
  //! Sizes are computed recursively using two functions
  //! \f[ \begin{eqnarray*}
  //!  n_V(x,u)&=&
  //!  \begin{cases}
  //!     1 & u\in L(G)\\ %
  //!     0 &\sigma(u)>_S x \text{ or }\left(\sigma(u) = 
  //!         x \wedge u\not\in\gamma^*(x)\right)\\ %
  //!     n_X(y,v)n_X(z,w)& \sigma(u) = x\\ %
  //!     n_X(y,u)& \text{otherwise}
  //!   \end{cases}\\ %
  //!   n_X(x,u)&=&
  //!   \begin{cases}
  //!     1 & u\inL(G)\\ %
  //!     0 &\sigma(u)>_S x\\ %
  //!     \frac{n_X(x,v)\left(n_X(x,v)-1\right)}{2} + n_V(x,u)& G_v\cong G_w\\ %
  //!     n_X(x,v)n_X(x,w) +n_V(x,u)& \text{otherwise}
  //!   \end{cases}
  //! \end{eqnarray*}
  //! \f]
  //!
  //! author: Bengt Sennblad 
  //! copyright: The MCMC-club, SBC
  // 
  //------------------------------------------------------------------------
  class EnumerateReconciliationModel : public ReconciledTreeModel
  {
  public:
    //------------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //------------------------------------------------------------------
    EnumerateReconciliationModel(Tree &G_in, StrStrMap &gs_in,
				 BirthDeathProbs &bdp_in);
    EnumerateReconciliationModel(Tree &G_in, StrStrMap &gs_in, 
			    BirthDeathProbs &bdp_in, 
				 std::vector<SetOfNodes>& AC);
    EnumerateReconciliationModel(const EnumerateReconciliationModel &M);
    virtual ~EnumerateReconciliationModel();
    EnumerateReconciliationModel & operator=(const EnumerateReconciliationModel &M);

    //------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------
    //! Returns number of possible reconciliations for a given gene and 
    //! species tree and a given \f$ \sigma |f$-labeling (i.e.LambdaMap)
    unsigned getNumberOfReconciliations();

    //! Returns the unique ID for the present reconciliation, gamma.
    unsigned computeGammaID();
    
    //! Sets gamma to the reconciliation represented by the unique ID 
    //! given by argument 'unique'
    void setGamma(unsigned unique);

    void setGamma(const GammaMap& newGamma)
    {
      ReconciliationModel::setGamma(newGamma);
    }
    //-------------------------------------------------------------------
    //
    // I/O
    //
    //-------------------------------------------------------------------
    std::string print() const;
    
    //! \name Debugging
    //! For debugging of reconciliation counting
    //! @{
    std::string printh(Node* x) const;
    std::string printu(Node* x, Node* u) const;
    std::string printx(Node* x, Node* u) const;
    //! @}
      
  protected:
    //------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------
    //! Initiate helper structures for counting reconciliations and ofr
    //! computing probabilities (base class)
    void inits(); 

    //! Fill in helper structure for computing number of reconc and IDs
    void compute_N(Node* x, Node* u);

    //! Recursive helper function for computing reconciliation IDs
    unsigned compute_u(Node* x,Node* u);

    // Recursive helper function for setting reconciliation iven an ID	 
    void setGamma(Node* x,Node* u, unsigned unique);

  protected:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------
    //! Structure that holds n_V 
    NodeNodeMap<unsigned> N_V; 
    //! Structure that holds n_X
    NodeNodeMap<unsigned> N_X;
  };



  //------------------------------------------------------------------------
  //
  //! Class EnumerateLabeledReconciliationModel allows counting of number of 
  //! possible reconciliations between the given gene tree and species
  //! tree. It can also assign unique numbers to these reconciliations
  //! and thereby allows enumerating them and, as it inherits from
  //! ReconciledTreeModel, to compute their probability.
  //!
  //! Sizes are computed recursively using two functions
  //! \f[ \begin{eqnarray*}
  //!  n_V(x,u)&=&
  //!  \begin{cases}
  //!     1 & u\in L(G)\\ %
  //!     0 &\sigma(u)>_S x \text{ or }\left(\sigma(u) = 
  //!         x \wedge u\not\in\gamma^*(x)\right)\\ %
  //!     n_X(y,v)n_X(z,w)& \sigma(u) = x\\ %
  //!     n_X(y,u)& \text{otherwise}
  //!   \end{cases}\\ %
  //!   n_X(x,u)&=&
  //!   \begin{cases}
  //!     1 & u\inL(G)\\ %
  //!     0 &\sigma(u)>_S x\\ %
  //!     \frac{n_X(x,v)\left(n_X(x,v)-1\right)}{2} + n_V(x,u)& G_v\cong G_w\\ %
  //!     n_X(x,v)n_X(x,w) +n_V(x,u)& \text{otherwise}
  //!   \end{cases}
  //! \end{eqnarray*}
  //! \f]
  //!
  //! author: Bengt Sennblad 
  //! copyright: The MCMC-club, SBC
  // 
  //------------------------------------------------------------------------
  class EnumerateLabeledReconciliationModel : public LabeledReconciledTreeModel
  {
  public:
    //------------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //------------------------------------------------------------------
    EnumerateLabeledReconciliationModel(Tree &G_in, StrStrMap &gs_in,
				 BirthDeathProbs &bdp_in);
    EnumerateLabeledReconciliationModel(Tree &G_in, StrStrMap &gs_in, 
			    BirthDeathProbs &bdp_in, 
				 std::vector<SetOfNodes>& AC);
    EnumerateLabeledReconciliationModel(const EnumerateLabeledReconciliationModel &M);
    virtual ~EnumerateLabeledReconciliationModel();
    EnumerateLabeledReconciliationModel & operator=(const EnumerateLabeledReconciliationModel &M);

    //------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------
    //! Returns number of possible reconciliations for a given gene and 
    //! species tree and a given \f$ \sigma |f$-labeling (i.e.LambdaMap)
    unsigned getNumberOfReconciliations();

    //! Returns the unique ID for the present reconciliation, gamma.
    unsigned computeGammaID();
    
    //! Sets gamma to the reconciliation represented by the unique ID 
    //! given by argument 'unique'
    void setGamma(unsigned unique);


    //-------------------------------------------------------------------
    //
    // I/O
    //
    //-------------------------------------------------------------------
    std::string print() const;
    
    //! \name Debugging
    //! For debugging of reconciliation counting
    //! @{
    std::string printh(Node* x) const;
    std::string printu(Node* x, Node* u) const;
    std::string printx(Node* x, Node* u) const;
    //! @}
      
  protected:
    //------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------
    //! Initiate helper structures for counting reconciliations and ofr
    //! computing probabilities (base class)
    void inits(); 

    //! Fill in helper structure for computing number of reconc and IDs
    void compute_N(Node* x, Node* u);

    //! Recursive helper function for computing reconciliation IDs
    unsigned compute_u(Node* x,Node* u);

    // Recursive helper function for setting reconciliation iven an ID	 
    void setGamma(Node* x,Node* u, unsigned unique);

  protected:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------
    //! Structure that holds n_V 
    NodeNodeMap<unsigned> N_V; 
    //! Structure that holds n_X
    NodeNodeMap<unsigned> N_X;
  };

}//end namespace beep

#endif
