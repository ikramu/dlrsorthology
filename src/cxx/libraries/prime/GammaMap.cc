#include <algorithm>
#include <cassert>

#include "AnError.hh"
#include "GammaMap.hh"
#include "PRNG.hh"
#include "Tree.hh"

// Author: Lars Arvestad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //----------------------------------------------------------------------


  // Make it possible to instantiate a Gamma from annotations in a gene tree. 
  // (See TreeIO!)
  //----------------------------------------------------------------------
  GammaMap::GammaMap(Tree& G, Tree& S, const LambdaMap& L, 
		     vector<SetOfNodes>& AC_info)
    : Gtree(&G), 
      Stree(&S), 
      lambda(L),
      gamma(S.getNumberOfNodes()), // Allocate a vector of the right size.
      chainsOnNode(G.getNumberOfNodes())
  {
    // This must currently be a recursion to get chainsOnNode correctly 
    // set when S has odd node ID's - make chainsOnNode a set and the 
    // problem is gone
    readGamma(S.getRootNode(), AC_info);
    checkGamma(G.getRootNode());
  }

  // Constructor setting up an empty gamma.
  //----------------------------------------------------------------------
  GammaMap::GammaMap(Tree &G, Tree &S, const LambdaMap &L)
    : Gtree(&G), 
      Stree(&S), 
      lambda(L), 
      gamma(S.getNumberOfNodes()),	// Allocate a vector of the right size.
      chainsOnNode(G.getNumberOfNodes())
  {
  }

  //----------------------------------------------------------------------
  // Most of the time we actually want to create gamma from a leaf-map, gs,
  // rather than from a lambda. Thus, here follows a gs-version of the two 
  // constructors, above
  //----------------------------------------------------------------------

  // Make it possible to instantiate a Gamma from annotations in a 
  // gene tree. (See TreeIO!) - gs-version
  //----------------------------------------------------------------------
  GammaMap::GammaMap(Tree &G, Tree &S, const StrStrMap& gs,
		     vector<SetOfNodes> &AC_info)
    : Gtree(&G), 
      Stree(&S), 
      lambda(G, S, gs),
      gamma(S.getNumberOfNodes()), // Allocate a vector of the right size. 
      chainsOnNode(G.getNumberOfNodes())
  {
    // This must currently be a recursion to get chainsOnNode correctly 
    // set when S has odd node ID's - make chainsOnNode a set and the 
    // problem is gone
    readGamma(S.getRootNode(), AC_info);
    checkGamma(G.getRootNode());
  }

  // Constructor setting up an empty gamma, using gs.
  //----------------------------------------------------------------------
  GammaMap::GammaMap(Tree &G, Tree &S, const StrStrMap& gs)
    : Gtree(&G), 
      Stree(&S), 
      lambda(G, S, gs), 
      gamma(S.getNumberOfNodes()),	// Allocate a vector of the right size.
      chainsOnNode(G.getNumberOfNodes())
  {
  }



  // Named constructor for initing the reconciliation with least number
  // of duplications, or the gamma bound.
  //
  // The upper bound for gamma (gamma*) is also used as an initial set 
  // of anti-chains.
  //----------------------------------------------------------------------
  GammaMap
  GammaMap::MostParsimonious(Tree &G, Tree &S, LambdaMap &L)
  {
    GammaMap gamma_star(G, S, L);
    gamma_star.computeGammaBound(G.getRootNode());
    return gamma_star;
  }

  GammaMap
  GammaMap::MostParsimonious(Tree &G, Tree &S, StrStrMap& gs)
  {
    GammaMap gamma_star(G, S, gs);
    gamma_star.computeGammaBound(G.getRootNode());
    return gamma_star;
  }





  // Copy constructor. In particular, it copies the gamma bound!
  //----------------------------------------------------------------------
  GammaMap::GammaMap(const GammaMap &original)
    : Gtree(original.Gtree),
      Stree(original.Stree),
      lambda(original.lambda),
      gamma(original.gamma),
      chainsOnNode(original.chainsOnNode)
  {
  }

  GammaMap::~GammaMap()
  {
  }

  // Assignment
  // reference attribute safe!
  //----------------------------------------------------------------------
  GammaMap& 
  GammaMap::operator=(const GammaMap &gm)
  {
    if (this != &gm)
      {
	// We don't want to change the actual objects that these attributes
	// reference to, so we test if the new ones are identical!
	if(Gtree != gm.Gtree || Stree != gm.Stree)
	  {
	    throw AnError("GammaMap::operator=: "
			  "referenced trees do not match", 1);
	  }
 	Gtree = gm.Gtree;
 	Stree = gm.Stree;
	lambda = gm.lambda;
	gamma = gm.gamma;
	chainsOnNode = gm.chainsOnNode;
      }
    return *this;
  }

  // Resets gamma to and empty gamma
  //----------------------------------------------------------------------
  void 
  GammaMap::reset()
  {
    lambda.update(*Gtree, *Stree);
    
    gamma = vector<SetOfNodes>(Stree->getNumberOfNodes());
    chainsOnNode = vector<deque<Node*> >(Gtree->getNumberOfNodes());

    return;
  }

  // Check if there is a gamma to work with
  //----------------------------------------------------------------------
  bool 
  GammaMap::empty() const
  {
    return gamma.empty(); //!\todo{ this does not work - gamma is never empty!}
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  // Access
  //----------------------------------------------------------------------

  // Get references to the trees
  //----------------------------------------------------------------------
  Tree& 
  GammaMap::getS()
  {
    return *Stree;
  }

  Tree& 
  GammaMap::getG()
  {
    return *Gtree;
  }

  //Get the size of gamma(x). i.e., the number of Nodes mapped to x 
  //----------------------------------------------------------------------
  unsigned
  GammaMap::getSize(Node& x) const   //version with reference
  {
    return gamma[x.getNumber()].size();
  }
  unsigned
  GammaMap::getSize(Node* x) const   //version with pointer
  {
    return gamma[x->getNumber()].size();
  }

  // Return the number of gene tree leaves in the species leaf
  // with the most genes.
  unsigned
  GammaMap::sizeOfWidestSpeciesLeaf() const
  {
    return sizeOfWidestSpeciesLeaf(Stree->getRootNode(), 0);
  }

  unsigned 
  GammaMap::sizeOfWidestSpeciesLeaf(Node *x, unsigned current_max) const
  {
    if (x->isLeaf()) {
      unsigned w = getSize(x);
      if (w > current_max) {
	return w;
      } else {
	return current_max;
      }
    } else {
      current_max = sizeOfWidestSpeciesLeaf(x->getLeftChild(), current_max);
      current_max = sizeOfWidestSpeciesLeaf(x->getRightChild(), current_max);
      return current_max;
    }
  }

  // Verify validity of gamma
  //
  // This is not a perfect test, but it is simple and catches many
  // problems. If gamma(x) is non-empty, then also gamma(parent(x)) is
  // non-empty. Return value
  //
  bool
  GammaMap::valid() const
  {
    try {
      valid(Stree->getRootNode());
    } 
    catch (int i) {
      return false;
    }
    return true;
  }

  // Notice that the return value here means someting else:
  bool
  GammaMap::valid(Node *x) const
  {
    if (x->isLeaf()) {
      if (getSize(x) > 0) {
	return true;
      } else {
	return false;
      }
    } else {
      bool l = valid(x->getLeftChild());
      bool r = valid(x->getRightChild());
      if (l || r) {
	if (getSize(x) == 0) {
	  throw 1;
	} else {
	  return true;
	}
      } else {
	return false;
      }
    }
  }
    
    
  // Count the anti-chains on a gene node
  //----------------------------------------------------------------------
  unsigned 
  GammaMap::numberOfGammaPaths(Node &u) const
  {
    assert(chainsOnNode.size() > u.getNumber());
    const deque<Node*> &anti_chains = chainsOnNode[u.getNumber()];
    return anti_chains.size();
  }


  // Methods for the retrieval the highest and lowest anti-chains, 
  // represented by their species tree nodes, of a gene node u. Null is 
  // returned if there is no anti-chain on u.
  //----------------------------------------------------------------------
  Node*
  GammaMap::getHighestGammaPath(Node &u) const // Dominates all others on u
  {
    const deque<Node*>& anti_chains = chainsOnNode[u.getNumber()];
    if (anti_chains.empty())
      {
	return NULL;
      }
    else
      {
	return anti_chains.back();
      }
  }

  Node* 
  GammaMap::getLowestGammaPath(Node &u) const // Dominated by others
  {
    const deque<Node*> &anti_chains = chainsOnNode[u.getNumber()];
    if (anti_chains.empty())
      {
	return NULL;
      }
    else
      {
	return anti_chains.front();
      }
  }


  // Test if a given Node u is in gamma(Nodex) 
  //----------------------------------------------------------------------
  bool
  GammaMap::isInGamma(Node *u, Node *x) const
  {
    const SetOfNodes &target_set = gamma[x->getNumber()];
    return target_set.member(u);
  }

  // Test if a given Node u is a speciation that occurs in gamma(Nodex) 
  //----------------------------------------------------------------------
  bool
  GammaMap::isSpeciationInGamma(Node *u, Node *x) const
  {
    const SetOfNodes &target_set = gamma[x->getNumber()];
    return target_set.member(u) && lambda[u] ==x;
  }



  // We determine whether a node u is a speciation or not by looking if
  // there is an anti-chain for x on u, and if lambda(u) == x.
  //
  // Let x be MRCA of the species of the leaves of G_u. This means that x is 
  // lambda(u). If u is represents the speciation x (in the species tree), 
  // then we require that x is _on_ u. It must also be the lowest antichain
  // on u, because, if z < x < y, if z is lowest, the species tree is violated,
  // and if y is lowest, then the x speciation occurs below u.
  //----------------------------------------------------------------------
  bool
  GammaMap::isSpeciation(Node& u) const
  {
    if (lambda[u] == getLowestGammaPath(u))
      {
	return true;
      }
    else
      {
	return false;
      }
  }


  SetOfNodes
  GammaMap::getGamma(Node *x) const
  {
    assert(x != NULL);
    assert(x->getNumber() < gamma.size());
    return gamma[x->getNumber()];
  }


  //----------------------------------------------------------------------
  multimap<int, int>
  GammaMap::getOrthology() const
  {
    multimap<int, int> orthology;
    Node* r = Gtree->getRootNode();
    getOrthology(r, orthology);
    return orthology;
  }

  list<Node*>
  GammaMap::getOrthology(Node* v, multimap<int, int> &orthology) const
  {
    if (v->isLeaf())
      {
	list<Node*> single;
	single.push_back(v);
	return single;
      }
    else
      {
	Node *left = v->getLeftChild();
	Node *right = v->getRightChild();
	list<Node*> left_nodes = getOrthology(left, orthology);
	list<Node*> right_nodes = getOrthology(right, orthology);

	if (isSpeciation(*v))
	  {
	    for (list<Node*>::const_iterator i =left_nodes.begin();
		 i != left_nodes.end();
		 i++)
	      {
		for (list<Node*>::const_iterator j =right_nodes.begin();
		     j != right_nodes.end();
		     j++)
		  {
		    orthology.insert(pair<int,int>((*i)->getNumber(), 
						   (*j)->getNumber()));
		  }
	      }
	  }

	left_nodes.splice(left_nodes.end(), right_nodes);
	return left_nodes;
      }      
  }


  // Given that x is on u, what lineage is u in? 
  // (1) u could be a speciation corresponding to u. In this case, x
  //     is returned.
  // (2) u is a duplication, and is found in found in a lineage y,
  //     where y is a child of x. Return y!
  //----------------------------------------------------------------------
  Node* 
  GammaMap::getLineage(Node* x, Node &u) const
  {
    Node *z = lambda[u];
    Node *y = z;

    while (*z < *x)
      {
	y = z;
	z = z->getParent();
      }

    return y;
  }

  //----------------------------------------------------------------------
  //
  // Manipulation
  //
  //----------------------------------------------------------------------

  // Pertubation of gamma
  // UpperBound is typically the most parsimonious reconciliation
  //----------------------------------------------------------------------
  void
  GammaMap::perturbation(GammaMap &UpperBound)
  {
    PRNG R;  // Grab random numbers from R, thank you.
    Node *u;
    Node *x;

    // u points to a root node, for subtree containing anti-chain for x.
    getRandomSubtree(UpperBound, &u, &x); 

    // Store N_v (from counting algorithm) here. (-1 means "not used".)
    vector<int> N(u->getNumber() + 1, -1); 
				
#ifdef DEBUGGING_GAMMA
    cerr << "\tGamma: " << *x << "\tTop at gene node: " << *u << endl;
#endif

    // How many options do we have for choosing an anti-chain?
    unsigned N_top = countAntiChainsUpper(*u, x, N); 

    if (N_top == 1)
      {
	return ;			// Only one choice: We are done!
      }

    // We can enumerate anti-chain positions, so a random number will 
    // help us choose a new position:
    unsigned new_position = R.genrand_modulo(N_top); 

#ifdef DEBUGGING_GAMMA
    cerr << "\tNew anti-chain position: #" << new_position << " (out of " << N_top << " choices)" << endl;
#endif

    // Recurse down the subtree and make the necessary changes to gamma,
    // et.c. 
    makeGammaChangeAbove(*u, x, N, new_position);
  }

  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------

  // Since chainsOnNode is a deque, we need to set gamma from the leafs up
  // Thus, we need this recursive function. If chainsOnNode is changed to a
  // set, this function is superfluous.
  //----------------------------------------------------------------------
  void
  GammaMap::readGamma(Node* sn, vector<SetOfNodes>& AC_info)
  {
    if(sn->isLeaf() == false) // send recursion on if necessary
      {
	readGamma(sn->getLeftChild(), AC_info);
	readGamma(sn->getRightChild(), AC_info);
      }
    SetOfNodes son = AC_info[sn->getNumber()];
    for (unsigned j = 0; j < son.size(); j++)
      {
	addToSet(sn, *son[j]);
      }
    return;
  }

  // Checks that current GammaMap is valid and reduces it
  // Below I will use some notation from Arvestad et al. 2006.
  // Assume that we are considering a node gn in the slice G_{u,\gamma(x)}.
  // Notice that the attribute 'gamma' does not precisely correspond to
  // the 'full' \gamma as described in Arvestad et al. 2006.
  // The 'full' \gamma_u(x) comprise all nodes in the slice G_{u,\gamma(x)}
  // The 'attribute' gamma_u(x) corresponds to \gammamin_u(x) in Arvestad
  // et al.2006, i.e., the leaves of G_{u,\gamma(x)}. Notice also that
  // \gammamax_u(x) is the root of G_{u,\gamma(x)}.
  // This function checks that gamma, i.e., \gammamin, fills the 
  // requirements for a reconciliation. If a full \gamma has been given 
  // in the constructor, it is first reduced  to \gammamin.
  //
  // WHAT DOES THIS RETURN??? 
  //----------------------------------------------------------------------
  Node*
  GammaMap::checkGamma(Node *gn)
  {
    // Get the lowest node in S that are mapped to gn.  This is called 
    // \beta(gn) in Arvestad et al. 2006 - can be NULL if there no mapped node
    Node* sn = getLowestGammaPath(*gn); 


    if(gn->isLeaf()) 
      {
	// Lowest antichain of a leaf in G should always be a leaf in S
	if(sn == 0)
	  {
	    ostringstream oss;
#ifndef NDEBUG
	    oss << "GammaMap::checkGamma\n";
#endif
	    oss << "Reconciliation error:\nGuest tree leaf '" 
		<< gn->getNumber() 
		<< "' with label '" 
		<< gn->getName()
		<< "' is not mapped to a species node.";
	    throw AnError(oss.str(), 1);
	  }
	if(!sn->isLeaf()) 
	  {
	    ostringstream oss;
#ifndef NDEBUG
	    oss << "GammaMap::checkGamma\n";
#endif
	    oss << "Reconciliation error:\nGuest tree leaf '" 
		<< gn->getNumber() 
		<< "' with label '" 
		<< gn->getName()
		<< "' is not mapped to a species tree leaf.\n"
		<< "The current mapping is to '" 
		<< sn 
		<< "', curiously!\n";
	    throw AnError(oss.str(), 1);
	  }
      }
    else
      {
	// pass recursion on
	Node* gl = gn->getLeftChild();
	Node* gr = gn->getRightChild();
	Node* sl = checkGamma(gl);
	Node* sr = checkGamma(gr);

	if(sl == sr) // i.e., gn is a duplication
	  {
	    sn = checkGammaForDuplication(gn, sn, sl, sr);
	  } 
	else 
	  {
	    sn = checkGammaForSpeciation(gn, sn, sl, sr);
	  }
      }

    sn = checkGammaMembership(gn, sn);
    return sn;
  }


  // We have established that gn is a duplication
  Node *
  GammaMap::checkGammaForDuplication(Node *gn, Node *sn, Node *sl, Node *sr)
  {
    while(sn == sl) // This means that we are reading a 'full' gamma
      {
	removeFromSet(sn, gn);
	// Now renew sn to get the lowest reduced \gamma^{-1)
	sn = getLowestGammaPath(*gn); 
      }
    
    if(sn != 0) // This means that sn == \gammamax_u(x)
      {
	if(*sn < *sl)
	  {
	    ostringstream oss;
#ifndef NDEBUG
	    oss << "GammaMap::checkGammaForDuplication\n";
#endif 
	    oss << "Reconciliation error:\nThe host nodes that the "
		<< "children of guest node '"
		<< gn->getNumber() 
		<< "' are ancestral\nto the host node that guest node '"
		<< gn->getNumber()
		<< "' itself is mapped to\n";
	    throw AnError(oss.str(), 1);
	  }
	else if(sn != sl->getParent()) 
	  {
	    ostringstream oss;
#ifndef NDEBUG
	    oss << "GammaMap::checkGammaForDuplication\n";
#endif
	    oss << "Reconcilation error:\nThe subtree rooted at guest node '"
		<< gn->getNumber() 
		<< "' is missing from gamma("
		<< sl->getParent()->getNumber()
		<< ")\n";
	    throw AnError(oss.str(), 1);
	  }

	return sn;
      }
    else 
      {
	// gn is a duplication and sn != \gammamax_u(x).
	// This is the only time sn  is allowed to 0
	// return the highest antichain of gn children
	return sl;
      }
  }

  Node *
  GammaMap::checkGammaForSpeciation(Node *gn, Node *sn, Node *sl, Node *sr)   // gn is a speciation 
  {
    Node* sm = Stree->mostRecentCommonAncestor(sl, sr); // "lambda"
    while(sn == sl) // This means that we are reading a 'full' gamma
      {
	removeFromSet(sn, gn);
	// Now renew sn to get the lowest reduced \gamma^{-1)
	sn = getLowestGammaPath(*gn); 
      }
    if(sn == 0 || sn != sm) // then sn is not mapped as a speciation!
      {
	ostringstream oss;
#ifndef NDEBUG
	oss << "GammaMap::checkGammaForSpeciation\n";
#endif
	oss << "Reconcilation error:\nGuest node '"
	    << gn->getNumber() 
	    << "' should be a speciation and map to host node '"
	    << sm->getNumber() << "'\n";
	throw AnError(oss.str(), 1);
      }
		
    // Check consistency of children's mappings
    else if(sl->getParent() != sm || sr->getParent() != sm)
      {
	Node* gl = gn->getLeftChild();
	Node* gr = gn->getRightChild();
	ostringstream oss;

#ifndef NDEBUG
	oss << "GammaMap::checkGammaForSpeciation\n";
#endif
	oss << "Reconciliation error:\nSubtrees rooted at guest nodes "
	    << gl->getNumber()
	    << " and/or "  
	    << gr->getNumber()
	    << " must map to\na child of host node "
	    << sm->getNumber()
	    << ", but not to any of their ancestors\n";
	throw AnError(oss.str(), 1);		
      }

    return sn;
  }

    // Now we just need to check that gn is a member of all relevant gamma
  Node *
  GammaMap::checkGammaMembership(Node *gn, Node *sn)
  {
    for(unsigned i = 1; i < chainsOnNode[gn->getNumber()].size(); i++)
      {
	if(sn->getParent() != chainsOnNode[gn->getNumber()][i])
	  {
	    ostringstream oss;
#ifndef NDEBUG
	    oss << "GammaMap::checkGammaMembership\n";
#endif 
	    oss << "Reconciliation error:\nThe host nodes to which guest node "
 		<< gn->getNumber() 
 		<< " is mapped must form a path.\nIn particular, host node "
		<< chainsOnNode[gn->getNumber()][i]->getNumber() 	
		<< " is not the parent of host node "
		<< sn->getNumber()
 		<< "\n";
	    throw AnError(oss.str(), 1);
	  }
	sn = sn->getParent();
      }

    return sn;
  }


  // Get the root of the gene-tree slice we want to work with. Randomly.
  //
  // *u_ret points to the root of a subtree whose dominating anti-chain 
  // is from *x_ret.
  //
  // Basic algorithm:
  // 1. Choose an anti-chain gamma(x). Avoid the leaves!
  // 2. In order to get a tree-slice *including* gamma(x), trace back up
  //    through parents until either a dominating anti-chain or
  //    gamma_star limits you.
  //
  // If the parental anti-chain is really including u as the speciation event,
  // the anti-chain below cannot be moved up onto parent(x). In this case, we 
  // return the lower subtree.
  //
  // The inner nodes of S are sampled inefficiently! I iterate until I have 
  // found a non-leaf. 
  // There should be a more direct way of doing this!
  //----------------------------------------------------------------------
  void
  GammaMap::getRandomSubtree(GammaMap &UpperBound, 
			     Node **u_ret, 
			     Node **x_ret)
  {
    PRNG R;
    unsigned n_species_nodes = Stree->getNumberOfNodes();
    unsigned n_gene_nodes;
    unsigned spec_node_n;
    Node *x;		// We want a node from x's anti-chain

    // We don't want the leaves in the species tree, and we want to
    // avoid those species nodes for which there is no mapping (this
    // can happen when there are species not represented in the species tree.
    do 
      {
	spec_node_n = R.genrand_modulo(n_species_nodes);
	x = Stree->getNode(spec_node_n);
	n_gene_nodes = gamma[spec_node_n].size();
      } 
    while (x->isLeaf() || n_gene_nodes == 0);

    // Grab a gene node from x's anti-chain
    SetOfNodes &node_set = gamma[spec_node_n]; // The anti-chain
    // Choose a member of the anti-chain...
    unsigned gene_node_n = R.genrand_modulo(n_gene_nodes); 
    // ... to be root of subtree
    Node *u = node_set[gene_node_n];                  

    // Get dominating anti-chain so that we have an upper limit of where
    // we put the anti-chain.
    Node *y = x->getParent();

    // Trace up through u's parent-path to find the root of the sub tree
    // containing this anti-chain and no other. Make sure to not cross
    // the UpperBound given by GammaStar!
    if (y == NULL)    // We found root! Better avoid testing against parent
      {
	while (UpperBound.isInGamma(u, x) == false)
	  {
	    u = u->getParent();
	  }
      }
    else 
      // x is not root, so y's anti-chain will put a limit on how high we can get
      {				
	while (UpperBound.isInGamma(u, x) == false
	       && isInGamma(u, y) == false)
	  {
	    u = u->getParent();
	  }
      }
  
    // If u is the speciation for y, then we have to reuse the lower
    // node, which is either left or right...
    if (lambda[u] == y) 
      {
	if (x == y->getLeftChild())
	  {
	    u = u->getLeftChild();
	  }
	else
	  {
	    u = u->getRightChild();
	  }
      }

    *u_ret = u;
    *x_ret = x;
    return;
  }



  // Count possible anti-chains positions
  //
  // Algorithm:
  // 1. For a leaf u of a slice / subtree, the number of choices is one: N_u=1
  // 2. Otherwise, if u has children v and w, there are N_v and N_w possible
  //    positions in each of the subtrees for an anti-chain, as well as the 
  //    option of putting the anti-chain right on u. Therefore, 
  //    N_u = 1 + N_v * N_w. 
  //
  // First count down from the top of the slice to where the anti-chain x 
  // presently is. Then continue by counting anti-chain positions below x.
  //----------------------------------------------------------------------
  unsigned
  GammaMap::countAntiChainsUpper(Node &u, Node *x, vector<int> &N)
  {
    if(numberOfGammaPaths(u) == 0      // In the middle of a slice, or
       || getLowestGammaPath(u)->strictlyDominates(*x))// at very top of slice. 
//        || *x < *getLowestGammaPath(u)) // at very top of slice. 
      //"<" is "dominates strictly"
      {
	// Must find x in subtrees
	unsigned N_left = countAntiChainsUpper(*u.getLeftChild(), x, N);
	unsigned N_right = countAntiChainsUpper(*u.getRightChild(), x, N);
	unsigned N_here = 1 + N_left * N_right;
	N[u.getNumber()] = N_here;
	return N_here;
      }
    else if(x->dominates(*getLowestGammaPath(u))) // Stumbled on targeted anti-chain
      {
	return countAntiChainsLower(u, x, N);
      }
    else 
      {			// \todo This third option should never occur. But why is it here?
      PROGRAMMING_ERROR("Check the code please...");
      return 0;
      }
  }

  // Continue counting below the anti-chain in question.
  //----------------------------------------------------------------------
  unsigned
  GammaMap::countAntiChainsLower(Node &u, Node *x, vector<int> &N)
  {
    if (numberOfGammaPaths(u) == 0 || x == getLowestGammaPath(u)) 
      {				// We continue downward
	unsigned N_left = countAntiChainsLower(*u.getLeftChild(), x, N);
	unsigned N_right = countAntiChainsLower(*u.getRightChild(), x, N);
	unsigned N_here = 1 + N_left * N_right;
	N[u.getNumber()] = N_here;
	return N_here;
      }
    else				// We have come to the bottom
      {
	N[u.getNumber()] = 1;
	return 1;			// and encountered a sub-tree leaf
      }
  }
      


  // Rotate nodes in G so that left children are found in the left branch of an S node, etc.
  //
  // Does not really belong here, does it?
  void
  GammaMap::twistAndTurn()
  {
    twistAndTurn(Gtree->getRootNode(), Stree->getRootNode());
  }

  void
  GammaMap::twistAndTurn(Node *v, Node *x)
  {
    if (v->isLeaf() || x->isLeaf())
      {
	// Done
      }
    else
      {
	Node *vl = v->getLeftChild();
	Node *vr = v->getRightChild();

	Node* xl = x->getLeftChild();
	Node* xr = x->getRightChild();

	Node* vll = lambda[vl];
	Node* vrl = lambda[vr];

	// If v is (or could be) a speciation, make sure the two children
	// don't cross unnecessarily.
	if (vll != lambda[v]
	    && vrl != lambda[v])
	  {
	    if (vll == xr && vrl == xl) {
	      v->setChildren(vr, vl);
	    }
	  }
	else if (vll != lambda[v])
	  {
	    Node *rep = x->getDominatingChild(vll);
	    if (rep == xr)
	      {
		v->setChildren(vr, vl);
	      }
	  }
	else if (vrl != lambda[v])
	  {
	    Node *rep = x->getDominatingChild(vrl);
	    if (rep == xl)
	      {
		v->setChildren(vr, vl);
	      }
	  }
	twistAndTurn(vl, vll);
	twistAndTurn(vr, vrl);
      }
  }

  // computeGammaBound
  // 
  // From work notes:
  // The upper limits of our gamma anti-chains are given by the function
  // computeGammaBound below. If a bound is mapped to an edge, the bound is
  // assigned to the vertex ``below''. This could make for some confusion,
  // since in some cases the bound is realy \emph{on} the actual
  // vertex. However, there is a rule involving the lambda mapping. Consider
  // an arc (u, v) in G. If there can be a speciation corresponding to
  // vertex x in the species tree above v, then lambda(v) $<$ x. On the other
  // hand, if the speciation is cannot reside higher than v, then 
  // lambda(v) = x.
  //----------------------------------------------------------------------
  void
  GammaMap::computeGammaBound(Node *v)
  {
    computeGammaBoundBelow(v);
    // Now make sure that the root of the species tree got mapped.
    Node *sroot = Stree->getRootNode();
    if (getSize(sroot) == 0)
      {
	assignGammaBound(v, sroot);
      }
  }
      

  void
  GammaMap::computeGammaBoundBelow(Node *v)
  {
    assert(v != NULL);

    if (v->isLeaf())
      {
	addToSet(lambda[v], *v);
      }
    else
      {
	Node *left = v->getLeftChild();
	Node *right = v->getRightChild();

	// First ensure everythings alright down below...
	computeGammaBoundBelow(left);
	computeGammaBoundBelow(right);

	// Then take care of the current node.
	Node *x = lambda[v];
	Node *xl = lambda[left];
	Node *xr = lambda[right];
	if (x != xl && x != xr)
	  {
	    addToSet(x, *v);
	    assignGammaBound(left, x->getDominatingChild(xl));
	    assignGammaBound(right, x->getDominatingChild(xr));
	  }
	else if (x != xl)
	  {
	    assignGammaBound(left, x); // Include x!
	  }
	else if (x != xr)
	  {
	    assignGammaBound(right, x); // Include x!
	  }
      }
  }

  // Given that there is one anti-chain y mapped to gene node v, but there are 
  // other anti-chains, at least x that dominates all other candidates.
  // Iterate up from y (or rather its parent) until x has been reached
  // and map all anti-chains (including x) to v.
  // Old explanation:
  //   When we know that there are a number of gamma-chains that has to be
  //   mapped onto an edge, we use this function. It adds gene node v to 
  //   all gamma chains below and including species node x.
  //----------------------------------------------------------------------
  void
  GammaMap::assignGammaBound(Node *v, Node *x)
  {
    assert(x != NULL);
    assert(v != NULL);

    Node *y = lambda[v]->getParent();
    // While x dominated (<=) by y, iterate 'til reaching the root of the subtree
    while (x->dominates(*y))	
      {
	addToSet(y, *v);
	y = y->getParent();
	if (!y)      // We reached the NULL pointer, i.e., we reached the root,
	  break;     // and x cannot dominate the root.
      }
  }


  //----------------------------------------------------------------------
  // Recurse down the subtree tree, rooted at u, containing an anti-chain for
  // x. Remove nodes from old position for x, and insert those that are
  // new. 
  //----------------------------------------------------------------------

  // First we assume that we have not yet reached the old position of
  // the anti-chain.
  //----------------------------------------------------------------------
  void 
  GammaMap::makeGammaChangeAbove(Node &u, Node *x, vector<int> &N, 
				 unsigned new_position)
  {
#ifdef DEBUGGING_GAMMA
    cerr << "\tnew_position = " << new_position << endl;
#endif

    unsigned id = u.getNumber();

    if (N[id] - 1 == static_cast<int>(new_position))     // The place of anti-chain

      {
	// This may be the old position for gamma(x). If it isn't, x is
	// either higher or lower than every other anti-chain on this edge.
	if (isInGamma(&u, x) == false)
	  {
	    if (numberOfGammaPaths(u) == 0
		|| x->dominates(*getHighestGammaPath(u)) == true)
	      {
		chainsOnNode[id].push_back(x); // Above any previous anti-chain on g
	      } 
	    else
	      {
		chainsOnNode[id].push_front(x); // Below any previous anti-chain on g
	      }

	    gamma[x->getNumber()].insert(&u);
	    removeOldAntiChain(u.getLeftChild(), x);
	    removeOldAntiChain(u.getRightChild(), x);
	  } 
	// else no change is needed, the anti-chain is already in the 
	// right position!
      }
    else
      {
	// Prepare for downward recursion:
	Node *l = u.getLeftChild();
	Node *r = u.getRightChild();

	unsigned modulus = N[l->getNumber()];
	unsigned left_position = new_position % modulus;
	unsigned right_position = new_position / modulus;

	if (isInGamma(&u, x) == false)
	  {
	    // Recurse downward looking for the right edge/node while
	    // looking for the old anti-chain.
	    makeGammaChangeAbove(*l, x, N, left_position);
	    makeGammaChangeAbove(*r, x, N, right_position);
	  }
	else
	  {
	    // We found the old anti-chain! Remove it, and recurse down
	    // without worrying about the old anti-chain.
	    SetOfNodes &anti_chain = gamma[x->getNumber()];
	    anti_chain.erase(&u);
	
	    deque<Node*> &coinciding_chains = chainsOnNode[id];
	    if (x == coinciding_chains.front())
	      {
		coinciding_chains.pop_front();
	      }
	    else 
	      {
		coinciding_chains.pop_back();
	      }
	    makeGammaChangeBelow(*l, x, N, left_position);
	    makeGammaChangeBelow(*r, x, N, right_position);
	  }
      }
  }


  //
  // This function is much like makeGammaChangeAbove, except we know
  // that the old anti-chain has already been taken care of, so it can
  // be disregarded.
  //----------------------------------------------------------------------
  void
  GammaMap::makeGammaChangeBelow(Node &u, Node *x, vector<int> &N, unsigned new_position)
  {
    unsigned id = u.getNumber();
    if (N[id] - 1 == static_cast<int>(new_position)) // This is the place for the anti-chain
      {
	chainsOnNode[id].push_back(x); // Above any previous anti-chain on g
	gamma[x->getNumber()].insert(&u);
      }
    else
      {
	// Continue to recurse downward!
	Node *l = u.getLeftChild();
	Node *r = u.getRightChild();
	unsigned modulus = N[l->getNumber()];
	unsigned left_position = new_position % modulus;
	unsigned right_position = new_position / modulus;
	makeGammaChangeBelow(*l, x, N, left_position);
	makeGammaChangeBelow(*r, x, N, right_position);
      }
  }

  //
  // Remove anti-chain gamma(x) from nodes at or below u. Stop when
  // another anti-chain is found. 
  //----------------------------------------------------------------------
  void
  GammaMap::removeOldAntiChain(Node *u, Node *x)
  {
    if (isInGamma(u, x) == true)	// We found the old anti-chain!
      {
	// Grab the set of nodes in this anti-chain.
	SetOfNodes &anti_chain = gamma[x->getNumber()]; 
	// Remove u from that set.		      
	anti_chain.erase(u);

	deque<Node*> &coinciding_chains = chainsOnNode[u->getNumber()];
	coinciding_chains.pop_back(); // We know that gamma(x) must be above all
	// other chains on this node, otherwise the
	// datastructure us inconsistent!
	// No further recursion needed; We found what we needed in this subtree.
      }
    else
      {
	// Still looking for that elusive anti-chain! Framåt marsch!
	removeOldAntiChain(u->getLeftChild(), x);
	removeOldAntiChain(u->getRightChild(), x);
      }
  }

  //
  // addToSet is assumed to be called from the leaves up with respect to both
  // arguments x and v.  If addToSet is called first with x1 and v1, and then
  //  x2 and v2, and x1 < x2 and v1 < v2 is not guaranteed, an inconsistent
  // datastructure has been created.
  //
  // Please notice the assymetry in the argument types: Node* vs. GeneNode&. 
  // The reason is that the Nodes are put into a STL deque, which requires
  // certain operators/constructors to be defined. I failed to defined these 
  // satisfactorily, gave up, and decided to store pointers instead of 
  // references
  //----------------------------------------------------------------------
  void
  GammaMap::addToSet(Node *x,  Node *v) 
  {
    assert(x != NULL);

    gamma[x->getNumber()].insert(v); // Insert v into the right anti-chain
    // Register an anti-chain with species x to be on node v.
    // x is assumed to dominate earlier species node registered at v.
    chainsOnNode[v->getNumber()].push_back(x); 
				

#ifdef DEBUGGING_GAMMA
    cout << "Gamma["
	 << *x
	 << "] includes "
	 << *v 
	 << endl;

    cout << "Registering species node "
	 << *x
	 << " on gene node "
	 << *v
	 << endl;
#endif
  }

  void
  GammaMap::addToSet(Node *x, Node &v)
  {
    addToSet(x, &v);
  }

  // remove a node association
  void
  GammaMap::removeFromSet(Node *x, Node *v)
  {
    assert(x != NULL);
    if(v == 0) // Nothing to be done but we allow this for practical reasons
      {
	return;
      }

    // Find iterator pos of x on v and remove x
    deque<Node*>& dref = chainsOnNode[v->getNumber()];
    deque<Node*>::iterator i = find(dref.begin(), dref.end(), x);
    if(i != dref.end())
      {
	dref.erase(i);
	gamma[x->getNumber()].erase(v); 
      }
    return;			       
  }

  //----------------------------------------------------------------------
  //
  // Debugging
  //
  //----------------------------------------------------------------------
  std::ostream& 
  operator<< (std::ostream& o, const GammaMap &gamma)
  {
    o << gamma.Stree->getName() 
      <<"\tgamma(" 
      << gamma.Stree->getName() 
      << ")\n----------------\n";
    return o << gamma.print(false);
  }

  string
  GammaMap::print(const bool& full) const
  {
    if(empty())
      {
	return "no gamma defined\n";
      }
    ostringstream oss;
    
    SetOfNodes gammaset;
    for (unsigned i = 0; i < gamma.size(); i++) 
      {
	if(full)
	  {
	    gammaset = getFullGamma(*Stree->getNode(i));
	  }
	else
	  {
	    gammaset = gamma[i];
	  }
	if (!gammaset.empty())
	  {
	    oss << i << "\t";  // This is the species node number
	    for (unsigned j = 0; j < gammaset.size(); j++) 
	      {
		// 		if(j % 8 == 0 && j > 0) //fix decent line breaks
		// 		  oss << "\n\t\t";
		if(j != 0)
		  {	
		    oss << ", " ;
		  }
		oss << gammaset[j]->getNumber(); 
	      }
	    oss << "\n";
	  }
	else
	  {
	    oss << i << "\n"; //"\tnot mapped\n";
	  }
      }
    return oss.str();
  }

  //! \todo{fix this for HybridTrees}
  SetOfNodes
  GammaMap::getFullGamma(const Node& x) const
  {
    const SetOfNodes& reduced = gamma[x.getNumber()];
    SetOfNodes full(reduced);
    Node* u;
    if(x.isRoot())  // Then include subtree induced by reduced
      {
	for(unsigned i = 0; i < reduced.size(); i++) 
	  {
	    u = reduced[i];

	    while(u->isRoot() == false)
	      {
		u = u->getParent();
		full.insert(u);
	      }
 	  }
      }
    else            // Include V(G_{u,\gamma(x)}):p_G(u)\not\in\gamma(x)
      {
	Node* p_x = x.getParent();
	
	for(unsigned i = 0; i < reduced.size(); i++)
	  {
	    u = reduced[i]; // get the current node

	    while(isInGamma(u, p_x) == false) // else we've reached top of slice
	      {
		u = u->getParent();
		if(x.dominates(*lambda[u])) // Don't include speciations
		  {
		    full.insert(u);
		  }
	      }	    
	  }
      }
    return full;
  }



}// end namespace beep
