#include <cassert>
#include <map>
#include <string>
#include <sstream>
#include <utility>
#include <vector>

#include "AnError.hh"
#include "SequenceData.hh"
#include <boost/algorithm/string.hpp>

namespace beep {
    // TODO: A short test in file test_SequenceData seems to indicate that 
    // SequenceData distinguishes capitol and small letters in data - this
    // is not good and need to be fixed!! /bens


    using namespace std;

    //----------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //----------------------------------------------------------------------

    // Constructor
    //----------------------------------------------------------------------

    SequenceData::SequenceData(const beep::SequenceType& dt)
    : seqType(dt),
    data() {
        //    cerr << "Constructor, type = " << dt << endl;
    }

    // Copy Constructor
    //----------------------------------------------------------------------

    SequenceData::SequenceData(const SequenceData& D)
    : seqType(D.seqType),
    data(D.data) {
        //    cerr << "Copy constructor, from:" << D << endl;
    }

    // Destructor 
    //----------------------------------------------------------------------

    SequenceData::~SequenceData() {
        //    cerr << "Destructor." << endl;
    }

    // Assignment operator
    //----------------------------------------------------------------------

    SequenceData&
            SequenceData::operator=(const SequenceData& D) {
        if (this == &D) {
            return *this;
        } else {
            seqType = D.getSequenceType();
            data = D.data;
            return *this;
        }
    }

    //public:
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------


    //----------------------------------------------------------------------
    //
    // Accessors
    //
    //----------------------------------------------------------------------

    // Access sequence type
    //----------------------------------------------------------------------

    beep::SequenceType
    SequenceData::getSequenceType() const {
        return seqType;
    }

    // Get the number of positions
    //----------------------------------------------------------------------

    unsigned
    SequenceData::getNumberOfPositions() const {
        return (*data.begin()).second.size();
    }

    unsigned
    SequenceData::getNumberOfSequences() const {
        return data.size();
    }

    unsigned
    SequenceData::getNameMaxSize() const {
        unsigned len = 0;
        for (map<string, string >::const_iterator i = data.begin();
                i != data.end();
                i++) {
            unsigned l = (*i).first.length();
            if (l > len) {
                len = l;
            }
        }
        return len;
    }

    // Access a sequence position
    //----------------------------------------------------------------------

    unsigned
    SequenceData::operator()(const std::string& name, unsigned pos) const {
        assert(data.find(name) != data.end());
        //! \todo{Add an assert for that name exists in data! \bens}
        return seqType((*data.find(name)).second[pos]);
    }

    const beep::LA_Vector
    SequenceData::leafLike(const std::string& name, unsigned pos) const {
        return seqType.getLeafLike((*data.find(name)).second[pos]);
    }

    const std::string
    SequenceData::operator[](const std::string& name) const {
        return (*data.find(name)).second;
    }

    string
    SequenceData::getSequenceName(unsigned idx) const {
        for (map<string, string >::const_iterator i = data.begin();
                i != data.end();
                i++) {
            if (idx == 0) {
                return (*i).first;
            } else {
                idx--;
            }
        }
        throw PROGRAMMING_ERROR("Out of bounds!");
        return "";
    }


    // Sort data into a table with character patterns present in the 
    // given partition, ordered according to nameOrder, and their 
    // associated number of occurrences in the interval. If no interval
    // is given all data are sorted
    // Should probably be completely replaced by getSortedData(), below, 
    // but might be interesting when outputting statistic of data.
    //----------------------------------------------------------------------

    PatternMap
    SequenceData::sortData() const {
        string s = "all";
        return sortData(s);

    }

    PatternMap
    SequenceData::sortData(const std::string& partition) const {
        //This does not handle partitions yet
        using namespace std;

        // Sort the data
        map<string, vector<unsigned> > sorted;

        unsigned nchar = data.begin()->second.size();
        for (unsigned j = 0; j < nchar; j++) {
            ostringstream oss;
            for (map<string, string>::const_iterator i = data.begin();
                    i != data.end(); i++) {
                oss << (*i).second[j];
            }
            // TODO: This 'ugly hack' should not be needed anymore as Sequencetype
            // handles ambiguities. Remove when thouroughly tested. /bens

            // 	// This is no longer such a terribly ugly hack to remove columns 
            // 	// containing gaps, '-'. This should now work for all data types.
            // 	// However, this may still be done in a better way
            // 	if(oss.str().find('-') == std::string::npos)
            // 	  {
            sorted[oss.str()].push_back(j);
            // 	  }
        }
        return sorted;
    }



    // SubstitutionModle actually never uses PatternMap as a map. Thus, 
    // it is better to return a PatternVec instead.

    PatternVec
    SequenceData::getSortedData() const {
        string s = "all";
        return getSortedData(s);
    }

    PatternVec
    SequenceData::getSortedData(const std::string& partition) const {
        //This does not handle partitions yet
        using namespace std;

        // Sort the data
        map<string, vector<unsigned> > sorted;

        // TODO: If we keep sortData() this could be called here! / bens
        unsigned nchar = data.begin()->second.size();
        for (unsigned j = 0; j < nchar; j++) {
            // Read the current column's pattern
            ostringstream oss;
            for (map<string, string>::const_iterator i = data.begin();
                    i != data.end(); i++) {
                oss << (*i).second[j];
            }
            // Increase the number of occurrences of pattern
            sorted[oss.str()].push_back(j);
        }
        // Convert each element in sorted to a pair consisting of first
        // occurrence and the number of occurrences of pattern (in partition)
        // and store in a vector.
        PatternVec ret;
        for (map<string, vector<unsigned> >::const_iterator i = sorted.begin();
                i != sorted.end(); i++)
            ret.push_back(make_pair(i->second[0], i->second.size()));
        return ret;
    }



    //----------------------------------------------------------------------
    //
    // Manipulators
    //
    //----------------------------------------------------------------------

    void
    SequenceData::changeType(beep::SequenceType newtype) {
        seqType = newtype;
    }


    // Adding data read from file
    //----------------------------------------------------------------------

    void
    SequenceData::addData(const string& name, const string& sequence) {
        if (seqType == myCodon) {
            string c;
            c.reserve(sequence.length() / 3);
            for (unsigned i = 0; i + 2 < sequence.length(); i += 3) // I can never remember the iterator idioms! /arve
            {
                c += myCodon.uint2char(myCodon.str2uint(sequence.substr(i, 3)));
            }
            data[name] = c;
            if (c.length() * 3 != sequence.length()) {
                throw AnError("Sequence does not contain an even reading frame: Length is not a multiple of 3.");
            }
        } else {
            data[name] = sequence;
        }
    }


    //----------------------------------------------------------------------
    //
    // I/O
    //
    //----------------------------------------------------------------------

    ostream& operator<<(ostream& os, const SequenceData& D) {
        return os << D.print()
                ;
    }

    std::map<char, double> SequenceData::getBaseFrequencies() {

        std::vector<double> baseFreq;
        std::map<char, double> bFreq;
        // currently only for DNA
        if (seqType.getType() == "DNA") {
            for (int i = 0; i < seqType.alphabetSize(); i++)
                baseFreq.push_back(0.0);
            
            bFreq['A'] = 0.0;
            bFreq['C'] = 0.0;
            bFreq['G'] = 0.0;
            bFreq['T'] = 0.0;

            for (map<string, string>::const_iterator i = data.begin();
                    i != data.end(); i++) {
                //cout << (*i).first << "\t" << (*i).second << endl;
                string seq = (*i).second;
                boost::to_upper(seq);
                //cout << seq << endl;
                
                for (int j = 0; j < seq.length(); j++) {
                    if (seq[j] == 'A')
                        //baseFreq[0] += 1.0;
                        bFreq['A'] += 1.0;
                    else if (seq[j] == 'G')
                        bFreq['G'] += 1.0;
                        //baseFreq[1] += 1.0;
                    else if (seq[j] == 'C')
                        bFreq['C'] += 1.0;
                        //baseFreq[2] += 1.0;
                    else if (seq[j] == 'T')
                        bFreq['T'] += 1.0;
                    else if (seq[j] == '-')
                        continue;
                        //baseFreq[3] += 1.0;
                    else
                        cerr << "Sequence contains non-conformant characters" << endl;
                }
            }

            // normalize the frequencies
            //double total = baseFreq[0] + baseFreq[1] + baseFreq[2] + baseFreq[3];
            double total = bFreq['A'] + bFreq['G'] + bFreq['C'] + bFreq['T'];
            for (unsigned int k = 0; k < baseFreq.size(); k++)
                baseFreq[k] = baseFreq[k] / total;
            for (map<char, double>::iterator it = bFreq.begin(); it != bFreq.end(); ++it) {
                //cout << (*it).first << ": " << (*ii).second << endl;
                bFreq[(*it).first] = bFreq[(*it).first]/total;
            }
        }

        return bFreq;
    }

    std::string
    SequenceData::print() const {
        ostringstream oss;
        if (data.size() == 0) {
            oss << "<no data>";
        } else {
            oss
                    << seqType
                    << "; Size: "
                    << data.size()
                    << " sequence, "
                    << data.begin()->second.size()
                    << " characters\n"
#ifdef DEBUG_PARAMS
                    << "data:\n"
                    << data4os()
#endif
                    ;
        }
        return oss.str();
    }


    // Helper function of operator<<
    //----------------------------------------------------------------------

    const string SequenceData::data4os() const {
        ostringstream oss;

        for (map<string, string>::const_iterator i = data.begin();
                i != data.end(); i++) {
            oss << (*i).first
                    << "\t";
            if (seqType == myCodon) {
                for (string::const_iterator j = (*i).second.begin();
                        j != (*i).second.end(); j++) {
                    oss << myCodon.uint2str(myCodon.char2uint(*j));
                }
            } else {
                oss << (*i).second;
            }
            oss << "\n";
        }
        //    oss << "\n";
        return oss.str();
    }

    // Helper function of operator<<
    //----------------------------------------------------------------------

    const string SequenceData::data4fasta() const {
        ostringstream oss;
        for (map<string, string >::const_iterator i = data.begin();
                i != data.end(); i++) {
            oss << ">"
                    << (*i).first
                    << "\n";
            if (seqType == myCodon) {
                for (string::const_iterator j = (*i).second.begin();
                        j != (*i).second.end(); j++) {
                    oss << myCodon.uint2str(myCodon.char2uint(*j));
                }
            } else {
                oss << (*i).second;
            }
            oss << "\n";
        }
        oss << "\n";
        return oss.str();
    }

}//end namespace beep

