#include "Density2PMCMC.hh"

#include "MCMCObject.hh"

#include <assert.h>
#include <cmath>
#include <sstream>

namespace beep
{
  using namespace std;

  //----------------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //----------------------------------------------------------------
  Density2PMCMC::Density2PMCMC(MCMCModel& prior, Density2P& d, 
			       bool doPerturbCV)
    : StdMCMCModel(prior, 2u, "Density"),
      density(&d),
      perturbCV(doPerturbCV),
      oldValue(0),
      idx_limits(0.5),
      //suggestion_variance(0.1 * d.getMean()),    
      // joelgs: When the lognormal proposal is used, 
      // its var scales roughly with
      suggestion_variance(0.1),                    
      // d.getMean(). Hence seems dangerous to multiply 
      // with mean explicitly too.
      whichParam(0),
      p1AccPropCnt(0, 0),
      p2AccPropCnt(0, 0)
  {
    if (density->densityName() == "Uniform")
      {
	fixMean();
	fixVariance();
      }
  }

  Density2PMCMC::Density2PMCMC(const Density2PMCMC& dm)
    : StdMCMCModel(dm),
      density(dm.density),
      perturbCV(dm.perturbCV),
      oldValue(dm.oldValue),
      idx_limits(dm.idx_limits),
      suggestion_variance(dm.suggestion_variance),
      whichParam(dm.whichParam),
      p1AccPropCnt(dm.p1AccPropCnt),
      p2AccPropCnt(dm.p2AccPropCnt)
  {
  }

  Density2PMCMC::~Density2PMCMC()
  {}

  Density2PMCMC&
  Density2PMCMC::operator=(const Density2PMCMC& dm)
  {
    if(this != &dm)
      {
	StdMCMCModel::operator=(dm);
	density = dm.density;
	perturbCV = dm.perturbCV;
	oldValue = dm.oldValue;
	idx_limits = dm.idx_limits;
	suggestion_variance = dm.suggestion_variance;
	whichParam = dm.whichParam;
	p1AccPropCnt = dm.p1AccPropCnt;
	p2AccPropCnt = dm.p2AccPropCnt;
      }
    return *this;
  }

  //----------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------
  // Fix parameters -- note that when building a chain of
  // MCMC-classes, these functions must be called before
  // constructing the next MCMC class
  //----------------------------------------------------------------
  void
  Density2PMCMC::fixMean()
  {
    if(idx_limits != 0 && n_params != 0)
      {
	n_params--;
	idx_limits = 0;
	updateParamIdx();
      }
  }

  void
  Density2PMCMC::fixVariance()
  {
    if(idx_limits != 1.0 && n_params != 0)
      {
	n_params--;
	idx_limits = 1.0;
	updateParamIdx();
      }
  }

  Density2P& 
  Density2PMCMC::getModel()
  {
    return *density;
  }


  //----------------------------------------------------------------------
  // Inherited from StdMCMCModel
  //----------------------------------------------------------------------

  // Perturbs with equal probability either the mean or the variance of
  // underlying density or one of the edge rates.
  // If perturbCV==true, then the following model is used:
  // variance=stddev^2, stddev=CV*mean, and CV is perturbed in MCMC.
  // This means that variance is changed both when mean and CV is perturbed.
  // Notice that CV=sqrt(variance)/mean.
  MCMCObject
  Density2PMCMC::suggestOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = density->setPertNotificationStatus(false);

    assert(n_params > 0);
    // Create return object
    MCMCObject MOb(1.0, 1.0);

    // First transform paramIdx to local parameter space
    Real Idx = paramIdx / paramIdxRatio;
    Real Min;
    Real Max;

    if (Idx < idx_limits)
      {
	// Perturb mean (first parameter).
	whichParam = 0;
	++p1AccPropCnt.second;
	oldValue = density->getMean();
	density->getMeanRange(Min, Max); 
	if (perturbCV)
	  {
	    Real sd = 0;
	    do
	      {
		// TODO: joelgs: Note that negative values will not be suggested
		// when using lognormal proposal.
		//Real newValue = perturbLogNormal(oldValue, suggestion_variance, Min, Max, MOb.propRatio);
		Real newValue = perturbTruncatedNormal(oldValue, 0.4, StdMCMCModel::FIFTY_PCT, Min, Max, MOb.propRatio);
		density->setMean(newValue);
		// TODO: When updating the variance with respect to the update 
		// in mean, the resulting variance may lie outside the allowed
		// range. The variance should scale with the new mean
	        sd = newValue / oldValue * std::sqrt(density->getVariance());
	      }
	    while(density->isInRange(sd*sd) == false);// When this is false the suggestion
		                             // should be rejected 
	    density->setVariance(sd * sd);
	  }
	else
	  {
	    // TODO: joelgs: Note that negative values will not be suggested
	    // when using lognormal proposal.
	    //Real newValue = perturbLogNormal(oldValue, suggestion_variance, Min, Max, MOb.propRatio);
	    Real newValue = perturbTruncatedNormal(oldValue, 0.4, StdMCMCModel::FIFTY_PCT, Min, Max, MOb.propRatio);
	    density->setMean(newValue);
	  }
      }
    else
      {
	// Perturb variance (second parameter) or CV.
	whichParam = 1;
	++p2AccPropCnt.second;
	oldValue = density->getVariance();
	density->getVarianceRange(Min, Max); // TODO: joelgs: Can negative Min arise?
	if (perturbCV)
	  {
	    Real mn = density->getMean();
	    Real cv = std::sqrt(oldValue) / mn;
	    //Real stddev = mn * perturbLogNormal(cv, suggestion_variance, std::sqrt(Min)/mn, std::sqrt(Max)/mn, MOb.propRatio);
	    Real stddev = mn * perturbTruncatedNormal(cv, 0.14, StdMCMCModel::FIFTY_PCT, std::sqrt(Min)/mn, std::sqrt(Max)/mn, MOb.propRatio);
	    density->setVariance(stddev * stddev);
	  }
	else
	  {
	    //density->setVariance(perturbLogNormal(oldValue, suggestion_variance, Min, Max, MOb.propRatio));
	    density->setVariance(perturbTruncatedNormal(oldValue, 0.4, StdMCMCModel::FIFTY_PCT, Min, Max, MOb.propRatio));
	  }
      }

    // Since any perturbation might have changed likelihood we must 
    // update it -- NO we have a uniform prior on parameters
    //     MOb.stateProb = updateDataProbability();

    // Notify listeners.
    density->setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::PERTURBATION);
    density->notifyPertObservers(&pe);

    return MOb;
  }

  void
  Density2PMCMC::commitOwnState()
  {
    if (whichParam == 0) { ++p1AccPropCnt.first; }
    else                 { ++p2AccPropCnt.first; }
  }

  void
  Density2PMCMC::discardOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = density->setPertNotificationStatus(false);

    // First transform paramIdx to local parameter space
    Real Idx = paramIdx / paramIdxRatio;

    if(Idx < idx_limits)
      {
	// Restore mean.
	if (perturbCV)
	  {
	    // Must restore variance too, since mean has changed.
	    Real sd = oldValue / density->getMean() * 
	      std::sqrt(density->getVariance());
	    density->setVariance(sd * sd);
	  }
	density->setMean(oldValue);      // Note: gbm treats mean different.
      }
    else
      {
	// Restore variance.
	density->setVariance(oldValue);  // Note: gbm treats mean different.
      }

    // Notify listeners.
    density->setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::RESTORATION);
    density->notifyPertObservers(&pe);
  };

  std::string
  Density2PMCMC::ownStrRep() const
  {
    std::ostringstream oss;
    if(n_params > 0)
      {
	if(idx_limits != 1.0)
	  {
	    oss << density->getMean() << ";\t";
	  }
	if(idx_limits != 0)
	  {
	    oss << density->getVariance() << ";\t";
	  }
      }
    return oss.str();
  };

  std::string
  Density2PMCMC::ownHeader() const
  {
    std::ostringstream oss;
    if(n_params > 0)
      {
	if(idx_limits != 1.0)
	  {
	    oss << "mean(float);\t";
	  }
	if(idx_limits != 0)
	  {
	    oss << "variance(float);\t";
	  }
      }
    return oss.str();
  };


  std::string
  Density2PMCMC::getAcceptanceInfo() const
  {
    std::ostringstream oss;
    if (n_params > 0)
      {
	unsigned totAcc = p1AccPropCnt.first + p2AccPropCnt.first;
	unsigned totProp = p1AccPropCnt.second + p2AccPropCnt.second;
	string p2 = perturbCV ? "CV:        " : "variance:  ";
	oss << "# Acc. ratio for " << name << ": "
	    << totAcc << " / " << totProp << " = "
	    << (totAcc / (Real) totProp) << endl;
	oss << "#    of which mean: "
	    << p1AccPropCnt.first << " / " << p1AccPropCnt.second << " = "
	    << (p1AccPropCnt.first / (Real) p1AccPropCnt.second) << endl
	    << "#    and " << p2
	    << p2AccPropCnt.first << " / " << p2AccPropCnt.second << " = "
	    << (p2AccPropCnt.first / (Real) p2AccPropCnt.second) << endl;
      }
    if (prior != NULL)
      {
	oss << prior->getAcceptanceInfo();
      }
    return oss.str();
  }


  Probability
  Density2PMCMC::updateDataProbability()
  {
    return 1.0;
  }

  void 
  Density2PMCMC::updateToExternalPerturb(Real newMean, Real newVariance)
  {
    if(newMean != density->getMean() || newVariance != density->getVariance())
      {
	// Turn off notifications just to be sure.
	bool notifStat = density->setPertNotificationStatus(false);

	density->setParameters(newMean, newVariance);

	// Notify listeners.
	density->setPertNotificationStatus(notifStat);
	PerturbationEvent pe(PerturbationEvent::PERTURBATION);
	density->notifyPertObservers(&pe);
      }

  }


  //----------------------------------------------------------------------
  //
  // I/O
  //
  //----------------------------------------------------------------------
  std::string
  Density2PMCMC::print() const
  {
    std::ostringstream oss;
    oss << name << ": ";
    oss  << density->print();
    string p2 = perturbCV ? "CV" : "variance";
    if(idx_limits != 1.0)
      {
	if (idx_limits != 0)
	  {
	    oss << "Mean and " 
		<< p2 
		<< " is perturbed during MCMC.\n";
	  }
	else
	  {
	    oss << "Mean is perturbed during MCMC, but " 
		<< p2 
		<< " is fixed.\n";
	  }
      }
    else
      {
	if (idx_limits != 0)
	  {
	    oss << "Mean is fixed, but " 
		<< p2 
		<< " is perturbed during MCMC.\n";
	  }
	else
	  {
	    oss << "Mean and " 
		<< p2 
		<< " are fixed.\n";
	  }
      }
    oss << StdMCMCModel::print();
    return oss.str();
  };

}//end namespace beep
