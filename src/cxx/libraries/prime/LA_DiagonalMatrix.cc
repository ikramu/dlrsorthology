#include "LA_DiagonalMatrix.hh" 

#include "AnError.hh"
#include "beep2blas.hh" 
#include "LA_Vector.hh" 

#include <cassert>
#include <string>
#include <sstream>

namespace beep {
  //public:
  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

  LA_DiagonalMatrix::LA_DiagonalMatrix(const unsigned& dim, 
				       const Real in_data[])
    : dim(dim),
      data(new Real[dim])
  {
    // Using blas strided vector copy
    ACC_PREFIX(copy_)(dim, in_data, 1, data, 1);
  };

  //Create a unit matrix
  //------------------------------------------------------------------------
  LA_DiagonalMatrix::LA_DiagonalMatrix(const unsigned& dim)
    : dim(dim),
      data(new Real[dim])
  {
    for(unsigned i = 0; i < dim; i++)
      {
	data[i] = 1;  
      }
  };

  // copy constructor
  //------------------------------------------------------------------------
  LA_DiagonalMatrix::LA_DiagonalMatrix(const LA_DiagonalMatrix& B)
    : dim(B.dim),
      data(new Real[dim])
  {
    // Using blas vector copy
    ACC_PREFIX(copy_)(dim, B.data, 1, data, 1);
  };

  LA_DiagonalMatrix::~LA_DiagonalMatrix()
  {
    delete[] data;
  };

  // Assignment operator
  //------------------------------------------------------------------------
  LA_DiagonalMatrix& 
  LA_DiagonalMatrix::operator=(const LA_DiagonalMatrix& B)
  {
    if(this != &B)
      {
	assert(dim == B.dim);
	// Using blas vector copy
	ACC_PREFIX(copy_)(dim, B.data, 1, data, 1);
      }
    return *this;
  };


  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------
  
  // Dimension of matrix
  //------------------------------------------------------------------------
  const unsigned& 
  LA_DiagonalMatrix::getDim() const
  {
    return dim;
  };
  // Access to individual elements
  //------------------------------------------------------------------------
  Real& 
  LA_DiagonalMatrix::operator()(const unsigned& row, 
				const unsigned& column)
  {
    if(row == column)
      {
	return data[row];
      }
    else
      {
	throw AnError("LA_DiagonalMatrix::operator():"
		      "Can not assign to off-diagonal elements");
      }
  };

  Real LA_DiagonalMatrix::operator()(const unsigned& row, 
		    const unsigned& column) const
  {
    if(row == column)
      {
	return data[row];
      }
    else
      {
	return 0.0;
      }
  };

  // Transpose of diagonal matrix  -- rather meaningless!
  //------------------------------------------------------------------------
  LA_DiagonalMatrix 
  LA_DiagonalMatrix::transpose() const
  {
    return *this;
  };

  // Inverse of Diagonal Matrix 
  //------------------------------------------------------------------------
  LA_DiagonalMatrix 
  LA_DiagonalMatrix::inverse() const
  {
    LA_DiagonalMatrix A(*this);
    for(unsigned i = 0; i < dim; i++)
      {
	A.data[i] = 1.0 /A.data[i]; //right?
      }
    return A;
  }

  // Trace (sum of diagonal elements of Matrix A using BLAS function
  //   void dasum_(const int& n, const double x, const int& stridex)
  // which performs d = x_1 + x_2 + ... + x_dim,
  // where d is a scalar that is returned by the function,
  // Vector x (this) has dimension n (=dim), stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  Real 
  LA_DiagonalMatrix::trace() const
  {
    return beep::ACC_PREFIX(asum_)(dim, data, 1);
  };

  // Multiplication of diagonal matrix A and a scalar 
  //------------------------------------------------------------------------
  LA_DiagonalMatrix 
  LA_DiagonalMatrix::operator*(const Real& alpha) const
  {
    LA_DiagonalMatrix D(*this);
    beep::ACC_PREFIX(scal_)(dim, alpha, D.data, 1);
    return D;
  };

  LA_DiagonalMatrix 
  operator*(const Real& alpha, const LA_DiagonalMatrix& D)
  {
    LA_DiagonalMatrix E(D);
    beep::ACC_PREFIX(scal_)(E.getDim(), alpha, E.data, 1);
    return E;
  };


  // Multiplication of diagonal matrix A and vector x 
  //------------------------------------------------------------------------
  LA_Vector 
  LA_DiagonalMatrix::operator*(const LA_Vector& x) const
  {
    assert(x.getDim() == dim);
    LA_Vector y(dim);
    for(unsigned i = 0; i < dim; i++)
      {
	y.data[i] = data[i] * x.data[i];
      }
    return y;
  };

  void
  LA_DiagonalMatrix::mult(const LA_Vector& x, LA_Vector& result) const
  {
    assert(x.getDim() == dim && result.getDim() == dim);
    for(unsigned i = 0; i < dim; i++)
      {
	result.data[i] = x.data[i] * data[i];
      }
    return;
  };

  // Multiplication of diagonal matrix A and diagonal matrix B 
  //------------------------------------------------------------------------
  LA_DiagonalMatrix 
  LA_DiagonalMatrix::operator*(const LA_DiagonalMatrix& B) const
  {
    assert(B.getDim() == dim);
      
    LA_DiagonalMatrix C(B);
    for(unsigned i = 0; i < dim; i++)
      {
	C.data[i] = data[i] * B.data[i];
      }
    return C;
  };

  // Multiplication of diagonal matrix A and dense matrix B 
  // Since the multiplication to be performed, in effect is to multiply 
  // row i of B with A[i,i], we can make use of BLAS function
  //   void dcal_(const int& m, const double& alpha,  
  //              const double dx[],  const int& stridex)
  // which performs x = alpha * x,
  // where m is the dimension of x (here always dim), alpha is a scalar, 
  // dx is the storage array of x and stridex is the stride between 
  // elements in x (here always 1)
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_DiagonalMatrix::operator*(const LA_Matrix& B) const
  {
    assert(B.getDim() == dim);
    LA_Matrix C(B);
    for(unsigned i = 0; i < dim; i++)
      {
	beep::ACC_PREFIX(scal_)(dim, data[i], &C.data[i], dim);
      }
    return C;
  };

  void
  LA_DiagonalMatrix::mult(const LA_Matrix& B, LA_Matrix& result) const
  {
    assert(B.getDim() == dim && result.getDim() == dim);
    result = B;
    for(unsigned i = 0; i < dim; i++)
      {
	beep::ACC_PREFIX(scal_)(dim, data[i], &result.data[i], dim);
      }
    return;
  };

  //------------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------------
  std::ostream& operator<<(std::ostream& os, 
				  const beep::LA_DiagonalMatrix& A)
  {
    std::ostringstream oss;
    unsigned dim = A.getDim();
    oss << "dimension: " << dim << "\n";
    for(unsigned i = 0; i < dim; i++)
      {
	for(unsigned j = 0; j < dim; j++)
	  {
	    oss << "\t" << (i==j?A(i,j):0) << ",";
	  }
	oss << "\n";
      }
    return os << oss.str();
  };
 
} /* namespace beep */





