#ifndef GENERIC3DMATRIX_HH
#define GENERIC3DMATRIX_HH

#include <sstream>
#include <vector>
#include <algorithm>
#include <functional>

#include "AnError.hh"

namespace beep 
{
  //------------------------------------------------------------------
  //! The purpose of this template is to provide easy matrix handling
  //! without the numerical sophistication of LA_Matrix. 
  //!
  //! Some code stems from the C++ FAQ, www.parashift.com/c++-faq/
  //------------------------------------------------------------------
  template<typename T>  // See section on templates for more
  class Generic3DMatrix 
  {
  public:
    //-------------------------------------------------------------------
    //
    // Constructors, destructors and assigment operator
    //
    //-------------------------------------------------------------------
    Generic3DMatrix(unsigned nrows, unsigned ncols, unsigned nfloors);
 
    // Based on the Law Of The Big Three:
    ~Generic3DMatrix();
    Generic3DMatrix(const Generic3DMatrix<T>& m);
    Generic3DMatrix<T>& operator= (const Generic3DMatrix<T>& m);
    
    //-------------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------------
    // Access methods to get the (i,j,k) element:
    T&       operator() (unsigned i, unsigned j, unsigned k);
    const T& operator() (unsigned i, unsigned j, unsigned k) const;
    
    // Access dimensions
    unsigned nrows() const;
    unsigned ncols() const;
    unsigned nfloors() const;
    
    // Arithmetic
    Generic3DMatrix<T>& operator+= (const Generic3DMatrix& m);
    
    //    // Manipulation
    //void reset(T x);
    //void scale(T x);

    friend std::ostream& operator<<(std::ostream &o,
				    const Generic3DMatrix<T>& m)
    {
      return o << m.print();
    }
    
    std::string print() const
    {
      std::ostringstream oss;
      oss << "Generic3DMatrix: nrows = " 
	  << nrows_ 
	  << " nfloors = " 
	  << nfloors_ 
	  << " ncols = " 
	  << ncols_ 
	  << "\n";
      return oss.str();
    }

    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
  protected:
    const T& get_element(unsigned i, unsigned j, unsigned k) const;
    T& get_element(unsigned i, unsigned j, unsigned k);
    
    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
  private:
    unsigned nrows_;
    unsigned ncols_;
    unsigned nfloors_;
    std::vector<T> data_;
  };
 


  //-------------------------------------------------------------------
  //
  // Constructors, destructors and assigment operator
  //
  //-------------------------------------------------------------------
  template<typename T>
  inline Generic3DMatrix<T>::Generic3DMatrix(unsigned nrows, unsigned ncols, unsigned nfloors)
    : nrows_ (nrows),
      ncols_ (ncols),
      nfloors_ (nfloors),
      data_  (nrows * ncols * nfloors)
  {
    if (nrows == 0 || ncols == 0 || nfloors == 0)
      throw AnError("No dimensions on matrix!");
  }

  template<typename T>
  inline Generic3DMatrix<T>::Generic3DMatrix(const Generic3DMatrix<T>& m)
    : nrows_ (m.nrows_),
      ncols_ (m.ncols_),
      nfloors_ (nfloors),
      data_  (m.data_)
  {
    if (nrows_ == 0 || ncols_ == 0 || nfloors == 0)
      throw AnError("No dimensions on matrix!");
  }
 

 
 template<typename T>
 inline Generic3DMatrix<T>::~Generic3DMatrix()
 {
   // Make sure that data_ is not dynamically allocated
 }

  template<typename T>
  Generic3DMatrix<T>& Generic3DMatrix<T>::operator= (const Generic3DMatrix<T>& m)
  {
    if (this != &m)
      {
	nrows_ = m.nrows_;
	ncols_ = m.ncols_;
	nfloors_ = m.nfloors_;
	data_  = m.data_;
      }
    return *this;
  }


  //-------------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------------
  template<typename T>
  inline T& Generic3DMatrix<T>::operator() (unsigned row, unsigned col, unsigned floor)
  {
    return get_element(row, col, floor);
  }

  template<typename T>
  inline const T& Generic3DMatrix<T>::operator() (unsigned row, unsigned col, unsigned floor) const
  {
    return get_element(row, col, floor);
  }

  template<typename T>
  unsigned Generic3DMatrix<T>::nrows() const
  {
    return nrows_;
  }

  template<typename T>
  unsigned Generic3DMatrix<T>::ncols() const
  {
    return ncols_;
  }

  template<typename T>
  unsigned Generic3DMatrix<T>::nfloors() const
  {
    return nfloors_;
  }

  template<typename T>
  Generic3DMatrix<T>& Generic3DMatrix<T>::operator+=(const Generic3DMatrix<T>& m)
  {
    assert(m.nrows() == nrows());
    assert(m.ncols() == ncols());
    assert(m.nfloors() == nfloors());

    int r = nrows();
    int c = ncols();
    int f = nfloors();

    for (int i = 0; i < r; i++) 
      {
	for (int j = 0; j < c; j++) 
	  {
	    for (int k = 0; k < f; k++) 
	      {
		get_element(i, j, k) +=  m(i, j, k);
	      }
	  }
      }

    return *this;
  }
  //-------------------------------------------------------------------
  //
  // Implementation
  //
  //------------------------------------------------------------------- 
  template<typename T>
  inline T& Generic3DMatrix<T>::get_element(unsigned row, unsigned col, unsigned floor)
  {
    if (row >= nrows_ || col >= ncols_ || floor >= nfloors_ ) 
      throw AnError("Out of bounds matrix index");
    return data_[row*ncols_*nfloors_ + col*nfloors_ + floor];
  }

  template<typename T>
  inline const T& Generic3DMatrix<T>::get_element(unsigned row, unsigned col, unsigned floor) const
  {
    if (row >= nrows_ || col >= ncols_ || floor >= nfloors_ )
      throw  AnError("Out of bounds matrix index");
    return data_[row*ncols_*nfloors_ + col*nfloors_ + floor];
  }
 
}
#endif
