#ifndef RECONCILEDTREEMODEL_HH
#define RECONCILEDTREEMODEL_HH

#include <iostream>
#include <sstream>

#include "ReconciliationModel.hh"


namespace beep
{
  //------------------------------------------------------------------------
  //
  //! Implements methods necessary for computing the probability
  //! of a reconciled tree by way of dynamic programming. 
  //! \todo{Improve class description /bens}
  //
  //------------------------------------------------------------------------
  class ReconciledTreeModel : public ReconciliationModel
  {
  public:
    //------------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //------------------------------------------------------------------
    ReconciledTreeModel(Tree& G, StrStrMap& gs, BirthDeathProbs& bdp);
    ReconciledTreeModel(Tree& G, StrStrMap& gs, BirthDeathProbs& bdp,
			std::vector<SetOfNodes>& AC);
    ReconciledTreeModel(const ReconciliationModel& rs);
    ReconciledTreeModel(const ReconciledTreeModel& M);
    virtual ~ReconciledTreeModel();

    ReconciledTreeModel & operator=(const ReconciledTreeModel& M);

    //------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------

    //------------------------------------------------------------------
    // Interface to MCMCModel
    //------------------------------------------------------------------
    virtual Probability calculateDataProbability(); 

    //-------------------------------------------------------------------
    //
    //! \name I/O
    //
    //-------------------------------------------------------------------
    //@{
    friend std::ostream& 
    operator<<(std::ostream &o, const ReconciledTreeModel& pm);
    std::string print() const;
    //@}

    //------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------
  protected:
    //! Initialize some basic structures.
    void inits();

    //! Computes e_V(x,u) by recursing through $\gamma^{-1}(u)$
    //! Notice that the argument x is not really needed, but is used as
    //! an error-check with the asserts.
    //! Precondition: u != 0; x == gamma.getHighestGammaPath()
    //-----------------------------------------------------------------------
    Probability computeE_V(Node* x, Node* u);

    //! Computes e_A(x,u)
    //! Precondition: u!=0, x!=0, u.isLeaf()==false, x-isLeaf()==false,
    //!               gamma.isInGamma(u,x)||gamma.isInGamma(u,x.getParent())
    //----------------------------------------------------------------------
    Probability computeE_A(Node* x, Node *u);

    //! Computes e_X(x,u)
    //! precondition: x!=0, u!=0
    //----------------------------------------------------------------------
    Probability computeE_X(Node* x, Node* u, unsigned& leaves);

    //! multiplies with two if subtrees are non-isomorphic respecting gamma
    //----------------------------------------------------------------------
    virtual void adjustFactor(Probability& factor, Node& u);

    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------
    //! Indexed by node id. Elem is true if the reconciled subtrees of
    //! the node are isomorphic.
    //! \todo{move this to base class? /bens}
    NodeMap<bool> isomorphy_rec;	


  };
  //------------------------------------------------------------------------
  //
  //! Implements methods necessary for computing the probability
  //! of a reconciled tree by way of dynamic programming. 
  //! \todo{Improve class description /bens}
  //
  //------------------------------------------------------------------------
  class LabeledReconciledTreeModel : public ReconciledTreeModel
  {
  public:
    //------------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //------------------------------------------------------------------
    LabeledReconciledTreeModel(Tree& G, StrStrMap& gs, BirthDeathProbs& bdp);
    LabeledReconciledTreeModel(Tree& G, StrStrMap& gs, BirthDeathProbs& bdp,
			std::vector<SetOfNodes>& AC);
    LabeledReconciledTreeModel(const ReconciliationModel& rs);
    LabeledReconciledTreeModel(const ReconciledTreeModel& M);
    virtual ~LabeledReconciledTreeModel();

    LabeledReconciledTreeModel & operator=(const ReconciledTreeModel& M);

    //! Always multplies with two
    //----------------------------------------------------------------------
    void adjustFactor(Probability& factor, Node& u);
  };

}//end namespace beep

#endif
