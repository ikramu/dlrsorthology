#include <cassert>
#include <cmath>
#include <limits>

#include "AnError.hh"
#include "TreeDiscretizer.hh"

namespace beep
{

using namespace std;


TreeDiscretizerOld::TreeDiscretizerOld(Tree& S, Real maxTimestep, unsigned minNoOfPtsPerEdge) :
	m_S(S),
	m_equidivision(false),
	m_maxTimestep(maxTimestep),
	m_minNoOfPts(minNoOfPtsPerEdge),
	m_timesteps(S),
	m_pts(S)
{
	if (maxTimestep <= 0)
	{
		throw AnError("Cannot create discretized tree with non-positive target time step.");
	}
	if (minNoOfPtsPerEdge < 1)
	{
		throw AnError("Cannot create discretized tree with no points on edge.");
	}
	for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
	{
		const Node* n = *it;
		Real et = (n->isRoot()) ? m_S.getTopTime() : m_S.getEdgeTime(*n);
		m_pts[n] = new vector<Real>();
		m_pts[n]->reserve(max(minNoOfPtsPerEdge, static_cast<unsigned>(ceil(et/m_maxTimestep))));
	}
	update();
}


TreeDiscretizerOld::TreeDiscretizerOld(Tree& S, unsigned noOfPtsPerEdge) :
	m_S(S),
	m_equidivision(true),
	m_maxTimestep(0),
	m_minNoOfPts(noOfPtsPerEdge),
	m_timesteps(S),
	m_pts(S)
{
	if (noOfPtsPerEdge < 1)
	{
		throw AnError("Cannot create discretized tree with no points on edge.");
	}
	for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
	{
		const Node* n = *it;
		m_pts[n] = new vector<Real>();
		m_pts[n]->reserve(noOfPtsPerEdge);
	}
	update();
}


TreeDiscretizerOld::~TreeDiscretizerOld()
{
	for (unsigned i=m_pts.size(); i>0; --i)
	{
		delete (m_pts[i-1]);
	}
}


void TreeDiscretizerOld::update()
{
	if (m_equidivision)
	{
		// Create m_minNoOfPts points on each edge (except for empty top time edge).
		for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
		{
			const Node* n = *it;
			m_pts[n]->clear();
			Real t = m_S.getTime(*n);
			if (n->isRoot() && m_S.getTopTime() <= 0)
			{
				m_pts[n]->push_back(t); // Only the node itself.
				m_timesteps[n] = 0;
				continue;
			}
			m_timesteps[n] = n->isRoot() ?
					m_S.getTopTime() / m_minNoOfPts : m_S.getEdgeTime(*n) / m_minNoOfPts;
			assert(m_timesteps[n] > 0);
			for (unsigned i=0; i<m_minNoOfPts; ++i)
			{
				m_pts[n]->push_back(t + i * m_timesteps[n]);
			}
		}
	}
	else
	{
		// Create points for each edge with time step as close to 'target' as possible,
		// but never exceeding it. Never create fewer than m_minNoOfPts (except for
		// empty top time edge).
		for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
		{
			const Node* n = *it;
			m_pts[n]->clear();
			Real t = m_S.getTime(*n);
			if (n->isRoot() && m_S.getTopTime() <= 0)
			{
				m_pts[n]->push_back(t); // Only the node itself.
				m_timesteps[n] = 0;
				continue;
			}
			Real noOfPtsFloat = n->isRoot() ?
					m_S.getTopTime() / m_maxTimestep : m_S.getEdgeTime(*n) / m_maxTimestep;
			unsigned noOfPts = max(m_minNoOfPts, static_cast<unsigned>(ceil(noOfPtsFloat)));
			m_timesteps[n] = n->isRoot() ?
					m_S.getTopTime() / noOfPts : m_S.getEdgeTime(*n) / noOfPts;
			assert(m_timesteps[n] > 0);
			for (unsigned i=0; i<noOfPts; ++i)
			{
				m_pts[n]->push_back(t + i * m_timesteps[n]);
			}
		}
	}
}


Tree& TreeDiscretizerOld::getOrigTree() const
{
	return m_S;
}


const Node* TreeDiscretizerOld::getOrigRootNode() const
{
	return m_S.getRootNode();
}


const Node* TreeDiscretizerOld::getOrigNode(unsigned index) const
{
	return m_S.getNode(index);
}


unsigned TreeDiscretizerOld::getTotalNoOfPts() const
{
	unsigned noOfPts = 0;
	unsigned sz = m_pts.size();
	for (unsigned i=0; i<sz; ++i)
	{
		noOfPts += m_pts[i]->size();
	}
	return noOfPts; 
}


unsigned TreeDiscretizerOld::getNoOfPts(const Node* node) const
{
	return (m_pts[node]->size());
}


Real TreeDiscretizerOld::getTimestep(const Node* node) const
{
	return (m_timesteps[node]);
}


Real TreeDiscretizerOld::getTopTime() const
{
	return (m_S.getTopTime());
}


Real TreeDiscretizerOld::getTopToLeafTime() const
{
	return (m_S.getTime(*m_S.getRootNode()) + m_S.getTopTime());
}


Real TreeDiscretizerOld::getEdgeTime(const Node* node) const
{
	return m_S.getEdgeTime(*node);
}


const vector<Real>* TreeDiscretizerOld::getPts(const Node* node) const
{
	return (m_pts[node]);
}


Real TreeDiscretizerOld::getPtTime(TreeDiscretizerOld::Point pt) const
{
	return ((*m_pts[pt.first])[pt.second]);
}


Real TreeDiscretizerOld::getPtTime(const Node* node) const
{
	return ((*m_pts[node])[0]);
}


Real TreeDiscretizerOld::getPtTimeDiff(TreeDiscretizerOld::Point ptOne, TreeDiscretizerOld::Point ptTwo) const
{
	return ((*m_pts[ptOne.first])[ptOne.second] - (*m_pts[ptTwo.first])[ptTwo.second]);
}


TreeDiscretizerOld::Point TreeDiscretizerOld::getParentPt(TreeDiscretizerOld::Point pt) const
{
	if (pt.second == m_pts[pt.first]->size() - 1)
	{
		// First point on edge above.
		return TreeDiscretizerOld::Point(pt.first->getParent(), 0);
	}
	return TreeDiscretizerOld::Point(pt.first, pt.second + 1);
}


TreeDiscretizerOld::Point TreeDiscretizerOld::getLeftChildPt(const Node* node) const
{
	Node* lc = node->getLeftChild();
	return TreeDiscretizerOld::Point(lc, m_pts[lc]->size()-1);
}


TreeDiscretizerOld::Point TreeDiscretizerOld::getRightChildPt(const Node* node) const
{
	Node* rc = node->getRightChild();
	return TreeDiscretizerOld::Point(rc, m_pts[rc]->size()-1);
}


TreeDiscretizerOld::Point TreeDiscretizerOld::getTopmostPt() const
{
	return getTopmostPt(m_S.getRootNode());
}


TreeDiscretizerOld::Point TreeDiscretizerOld::getTopmostPt(const Node* node) const
{
	return TreeDiscretizerOld::Point(node, m_pts[node]->size()-1);
}


unsigned TreeDiscretizerOld::getNoOfStepsBetweenPts(TreeDiscretizerOld::Point xPt, TreeDiscretizerOld::Point yPt) const
{
	unsigned steps = m_pts[yPt.first]->size() - yPt.second;
	while (yPt.first != xPt.first)
	{
		yPt.first = yPt.first->getParent();
		steps += m_pts[yPt.first]->size();
	}
	steps += -m_pts[xPt.first]->size() + xPt.second;
	return steps;
}


void TreeDiscretizerOld::getMinMaxEdgeTime(Real& min, Real& max, Real& topTime) const
{
	min = numeric_limits<Real>::max();
	max = numeric_limits<Real>::min();
	for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
	{
		const Node* n = *it;
		if (n->isRoot()) { continue; }
		Real et = m_S.getEdgeTime(*n); 
		if (et < min) { min = et; }
		if (et > max) { max = et; }
	}
	topTime = m_S.getTopTime();
}


void TreeDiscretizerOld::getMinMaxTimestep(Real& min, Real& max, Real& topTimeVal) const
{
	min = numeric_limits<Real>::max();
	max = numeric_limits<Real>::min();
	for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
	{
		const Node* n = *it;
		if (n->isRoot()) { continue; }
		if (m_timesteps[n] < min) { min = m_timesteps[n]; }
		if (m_timesteps[n] > max) { max = m_timesteps[n]; }
	}
	topTimeVal = m_timesteps[m_S.getRootNode()];
}


void TreeDiscretizerOld::getMinMaxNoOfPts(unsigned& min, unsigned& max, unsigned& topTimeVal) const
{
	// min should always be equal to m_minNoOfPts, but we still search as a precaution.
	min = numeric_limits<unsigned>::max();
	max = numeric_limits<unsigned>::min();
	for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
	{
		const Node* n = *it;
		unsigned noOfPts = m_pts[n]->size();
		if (n->isRoot()) { continue; }
		if (noOfPts < min) { min = noOfPts; }
		if (noOfPts > max) { max = noOfPts; }
	}
	topTimeVal =  m_pts[m_S.getRootNode()]->size();
}

void TreeDiscretizerOld::debugInfo(bool printNodeInfo) const
{
	Real minET, maxET, ttET;
	getMinMaxEdgeTime(minET, maxET, ttET);
	Real minStep, maxStep, ttStep;
	getMinMaxTimestep(minStep, maxStep, ttStep);
	unsigned minPts, maxPts, ttPts;
	getMinMaxNoOfPts(minPts, maxPts, ttPts);
	cerr
		<< "# ================================ TreeDiscretizerOld ====================================" << endl
		<< "# Discretization type: ";
	if (m_equidivision)
	{
		cerr << "Every edge comprises " << m_minNoOfPts << " pts" << endl;
	}
	else
	{
		cerr << "Time step roof is " << m_maxTimestep
			<< ", min no of pts per edge is " << m_minNoOfPts << endl;
	}
	cerr	
		<< "# Absolute root time: " << getPtTime(m_S.getRootNode()) << ", Absolute toptime time: " << getTopToLeafTime() << endl
		<< "# Number of pts: " << getTotalNoOfPts() << ", of which " << m_S.getNumberOfNodes() << " are nodes" << endl
		<< "# Min / max / toptime edge times: " << minET << " / " << maxET << " / " << ttET << endl
		<< "# Min / max / toptime timesteps: " << minStep << " / " << maxStep << " / " << ttStep << endl
		<< "# Min / max / toptime no of pts: " << minPts << " / " << maxPts << " / " << ttPts << endl;
	
	if (printNodeInfo)
	{
		cerr << "# Node:\tName:\tPt time span:\t\tET:\tNo of pts:\tTimestep:" << endl;
		for (Tree::iterator it=m_S.begin(); it!=m_S.end(); ++it)
		{
			const Node* n = *it;
			cerr << "# "
				<< n->getNumber() << '\t'
				<< (n->isLeaf() ? n->getName() : (n->isRoot() ? "Root   " : "       ")) << '\t'
				<< getPtTime(n) << "..." << getPts(n)->back() << '\t'
				<< getEdgeTime(n) << '\t'
				<< getNoOfPts(n) << '\t'
				<< getTimestep(n)<< '\t'
				<< endl;
		}
	}
	cerr << "# =====================================================================================" << endl;
}


} //end namespace beep

