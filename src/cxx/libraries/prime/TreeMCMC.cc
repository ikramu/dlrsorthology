#include "TreeMCMC.hh"

#include "BirthDeathProbs.hh"
#include "MCMCObject.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "TreePerturbationEvent.hh"

// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  using namespace std;

  //-------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //-------------------------------------------------------------
  //! \todo{Should constructor really take a Tree? but how initiate oldT? /bens}
  TreeMCMC::TreeMCMC(MCMCModel& prior, Tree& T_in, const Real& suggestRatio) :
    StdMCMCModel(prior, 3, T_in.getName()+"_Model", suggestRatio),
    //!< \todo{ONE tree and one root to perturb or n vertices to perturb?}
    mrGardener(),
    T(&T_in),
    old_T(),
    old_times(NULL),
    old_rates(NULL),
    old_lengths(NULL),
    idx_limits(3, 1.0),
    detailedNotifInfo(false),
    whichPerturbType(0),
    rerootAccPropCnt(0, 0),
    nniAccPropCnt(0, 0),
    sprAccPropCnt(0, 0)
  {
    init();
  }

  TreeMCMC::TreeMCMC(MCMCModel& prior, Tree& T_in,
		     const string& name, const Real& suggestRatio) :
    StdMCMCModel(prior, 3, name, suggestRatio),
    //!< \todo{ONE tree and one root to perturb or n vertices to perturb?
    mrGardener(),
    T(&T_in),
    old_T(),
    old_times(0),
    old_rates(0),
    old_lengths(0),
    idx_limits(3, 1.0),
    detailedNotifInfo(false),
    whichPerturbType(0),
    rerootAccPropCnt(0, 0),
    nniAccPropCnt(0, 0),
    sprAccPropCnt(0, 0)
  {
    init();
  }

  TreeMCMC::~TreeMCMC()
  {
  }

  TreeMCMC::TreeMCMC(const TreeMCMC& rtm) :
    StdMCMCModel(rtm),
    mrGardener(rtm.mrGardener),
    T(rtm.T),
    old_T(rtm.old_T),
    old_times(rtm.old_times),
    old_rates(rtm.old_rates),
    old_lengths(rtm.old_lengths),
    idx_limits(rtm.idx_limits),
    detailedNotifInfo(rtm.detailedNotifInfo),
    whichPerturbType(rtm.whichPerturbType),
    rerootAccPropCnt(rtm.rerootAccPropCnt),
    nniAccPropCnt(rtm.nniAccPropCnt),
    sprAccPropCnt(rtm.sprAccPropCnt)
  {
  }

  TreeMCMC&
  TreeMCMC::operator=(const TreeMCMC& rtm)
  {
    if(this != &rtm)
      {
	StdMCMCModel::operator=(rtm);
	mrGardener = rtm.mrGardener;
	T = rtm.T;
	old_T = rtm.old_T;
	old_times = rtm.old_times;
	old_rates = rtm.old_rates;
	old_lengths = rtm.old_lengths;
	idx_limits = rtm.idx_limits;
	detailedNotifInfo = rtm.detailedNotifInfo;
	whichPerturbType = rtm.whichPerturbType;
	rerootAccPropCnt = rtm.rerootAccPropCnt;
	nniAccPropCnt = rtm.nniAccPropCnt;
	sprAccPropCnt = rtm.sprAccPropCnt;
      }
    return *this;
  }

  //-------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------
  Tree&
  TreeMCMC::getTree() const
  {
    return *T;
  }

  void
  TreeMCMC::setTree(const Tree& T_in)
  {
    T->partialCopy(T_in);
    //! \todo{This should always be followed by a backup - user's
    //! responsibility or should it be done here? (overhead)/bens}
    return;
  }


  void
  TreeMCMC::fixTree()             // In this case only reroot is possible
  {
    if( idx_limits[1] != 0)       // check if already fixed
      {
	idx_limits[1] = 0.0;      // set perturb prob = 0
	n_params--;               // less params to perturb
	update_idx_limits();      // tell StdMCMCModel
      }
    if( idx_limits[2] != 0)       // check if already fixed
      {
	idx_limits[2] = 0.0;      // set perturb prob = 0
	n_params--;               // less params to perturb
	update_idx_limits();      // tell StdMCMCModel
      }
    return;
  };


  void
  TreeMCMC::fixRoot()                // In this case NNI and SPR are possible
  {
    if( idx_limits[0] != 0)          // check if already fixed
      {
	idx_limits[0] = 0.0;         // set perturb prob = 0
	n_params--;                  // less params to perturb
	update_idx_limits();         // tell StdMCMCModel
      }
    return;
  };


  void
  TreeMCMC::setDetailedNotification(bool doSendDetails)
  {
    detailedNotifInfo = doSendDetails;
  }


  //-------------------------------------------------------------
  // StdMCMCModel Interface
  //-------------------------------------------------------------
  MCMCObject
  TreeMCMC::suggestOwnState()
  {
    // First transform paramIdx to local parameter space
    Real Idx = paramIdx / paramIdxRatio;

    // Create a default MCMCObject (proposal_ratio = 1.0)
    MCMCObject MOb;
    Tree& tmpT = getTree();

    // Turn off notifications just to be sure.
    bool notifStat = tmpT.setPertNotificationStatus(false);

    // Note that some parameters, esp. edgeTimes may have  been perturbed
    // since we last updated old_T, we therefore need to update old_T here
    // and NOT in commitState!! Notice that external attributes are not
    // included
    old_T.partialCopy(getTree());  // use getTree to allow dynamic binding

    // We need to save all old external attributes
    if (tmpT.hasTimes())
      old_times = tmpT.getTimes();
    if (tmpT.hasRates())
      old_rates = tmpT.getRates();
    if (tmpT.hasLengths())
      old_lengths = tmpT.getLengths();

    TreePerturbationEvent* info = NULL;

    if (Idx < (idx_limits[0]))
      {
	assert(idx_limits[0] != 0);    //check for sane limits!
	// Re-balance (or unbalance) the tree.
	whichPerturbType = 0;
	++rerootAccPropCnt.second;
	info = mrGardener.doReRoot(tmpT, tmpT.hasLengths(), tmpT.hasTimes(), 
				   detailedNotifInfo);
      }
    else if (Idx < idx_limits[1])
      {
	// Perturb the gene tree using NNI.
	if (tmpT.getNumberOfLeaves() != 4 || 
	    (tmpT.getRootNode()->getLeftChild()->isLeaf()
	     || tmpT.getRootNode()->getRightChild()->isLeaf()))
	  {
	    whichPerturbType = 1;
	    ++nniAccPropCnt.second;
	    info = mrGardener.doNNI(tmpT, tmpT.hasLengths(), tmpT.hasTimes(), 
				    detailedNotifInfo);
	  }
	else
	  {
	    // Use another method than NNI in case of symmetric 4-leaf tree.
	    if (Idx-(idx_limits[0]) < (idx_limits[1])-Idx)
	      {
		whichPerturbType = 0;
		++rerootAccPropCnt.second;
		info = mrGardener.doReRoot(tmpT, tmpT.hasLengths(), 
					   tmpT.hasTimes(), detailedNotifInfo);
	      }
	    else
	      {
		whichPerturbType = 2;
		++sprAccPropCnt.second;
		info = mrGardener.doSPR(tmpT, tmpT.hasLengths(), tmpT.hasTimes(), 
					detailedNotifInfo);
	      }
	  }
      }
    else if (Idx < idx_limits[2])
      {
	// Perturb the gene tree using SPR.
	whichPerturbType = 2;
	++sprAccPropCnt.second;
	info = mrGardener.doSPR(tmpT, tmpT.hasLengths(), tmpT.hasTimes(), 
				detailedNotifInfo);
      }

    MOb.stateProb = updateDataProbability();

#ifdef PERTURBED_TREE
    tmpT.perturbedTree(true);
#elif PERTURBED_NODE
    tmpT.perturbedNode(tmpT.getRootNode());
#endif

    // Notify listeners.
    tmpT.setPertNotificationStatus(notifStat);
    PerturbationEvent* pe = (info == NULL) ?
      new PerturbationEvent(PerturbationEvent::PERTURBATION) : info;
    tmpT.notifyPertObservers(pe);
    if (pe != NULL) { delete pe; }

    return MOb;
  }

  void
  TreeMCMC::commitOwnState()
  {
    switch (whichPerturbType)
      {
      case 0:	++rerootAccPropCnt.first; break;
      case 1: ++nniAccPropCnt.first;    break;
      case 2: ++sprAccPropCnt.first;    break;
      default: break;
      }
  }

  void
  TreeMCMC::discardOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = T->setPertNotificationStatus(false);

    setTree(old_T);     // use setTree to allow dynamic binding

    // reset old attributes
    Tree& tmpT = getTree();

    if (tmpT.hasTimes())   { tmpT.getTimes()   = old_times;   }
    if (tmpT.hasRates())   { tmpT.getRates()   = old_rates;   }
    if (tmpT.hasLengths()) { tmpT.getLengths() = old_lengths; }

#ifdef PERTURBED_TREE
    getTree().perturbedTree(true);
#elif PERTURBED_NODE
    tmpT.perturbedNode(tmpT.getRootNode());
#endif

    // Notify listeners.
    tmpT.setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::RESTORATION);
    tmpT.notifyPertObservers(&pe);
  }


  string
  TreeMCMC::ownStrRep() const
  {
    string s;
    if(n_params > 0)
      {
	TreeIO io;
	TreeIOTraits traits;
	s += io.writeBeepTree(getTree(), traits, 0) + ";\t";
	//s += io.writeBeepTree(getTree(), true, false, false, true, false, 0) + "; ";
      }
    return s;
  }

  string
  TreeMCMC::ownHeader() const
  {
    string s;
    if(n_params > 0)
      {
	string tree_name = getTree().getName();
	if (tree_name.length() > 0)
	  {
	    s += tree_name;
	    s += "(tree); ";
	  }
	else
	  {
	    s += "T(tree); ";
	  }
      }
    return s;
  }

  void 
  TreeMCMC::updateToExternalPerturb(const Tree& newT)
  {
    Tree& tmpT = getTree();

    if(newT != tmpT)
      {
	// Turn off notifications just to be sure.
	bool notifStat = tmpT.setPertNotificationStatus(false);

	// Do the actual perturbation
	setTree(newT);
	if (tmpT.hasTimes())   { tmpT.getTimes()   = newT.getTimes();   }
	if (tmpT.hasRates())   { tmpT.getRates()   = newT.getRates();   }
	if (tmpT.hasLengths()) { tmpT.getLengths() = newT.getLengths();   }


	// Notify listeners.
	tmpT.setPertNotificationStatus(notifStat);
	TreePerturbationEvent* info = NULL; // For now
	PerturbationEvent* pe = (info == NULL) ?
	  new PerturbationEvent(PerturbationEvent::PERTURBATION) : info;
	tmpT.notifyPertObservers(pe);
	if (pe != NULL) { delete pe; }
      }

  }

  string
  TreeMCMC::getAcceptanceInfo() const
  {
    std::ostringstream oss;
    if (n_params > 0)
      {
	unsigned totAcc = rerootAccPropCnt.first + nniAccPropCnt.first + 
	  sprAccPropCnt.first;
	unsigned totProp = rerootAccPropCnt.second + nniAccPropCnt.second + 
	  sprAccPropCnt.second;
	oss << "# Acc. ratio for " << name << ": "
	    << totAcc << " / " << totProp << " = "
	    << (totAcc / (Real) totProp) << endl;
	oss << "#    of which rerooting: "
	    << rerootAccPropCnt.first << " / " 
	    << rerootAccPropCnt.second << " = "
	    << (rerootAccPropCnt.first / (Real) rerootAccPropCnt.second) 
	    << endl
	    << "#    and NNI:            "
	    << nniAccPropCnt.first << " / " << nniAccPropCnt.second << " = "
	    << (nniAccPropCnt.first / (Real) nniAccPropCnt.second) << endl
	    << "#    and SPR:            "
	    << sprAccPropCnt.first << " / " << sprAccPropCnt.second << " = "
	    << (sprAccPropCnt.first / (Real) sprAccPropCnt.second) << endl;

      }
    if (prior != NULL)
      {
	oss << prior->getAcceptanceInfo();
      }
    return oss.str();
  }


  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  ostream&
  operator<<(ostream &o, const TreeMCMC& A)
  {
    return o << A.print();
  }

  string
  TreeMCMC::print() const
  {
    ostringstream oss;
    if(idx_limits[0] == 0)
      {
	oss << "The gene tree ";
	if(idx_limits[1] == 0)
	  oss << "and its root is fixed to the following tree:\n"
	      << getTree().print();
	else
	  oss << "is fixed to the following tree:\n"
	      << getTree().print()
	      << "while its root is perturbed during MCMC-analysis.\n";
      }
    else
      {
	oss << "The gene tree ";
	if(idx_limits[1] == 0)
	  oss << "The root of the tree is fixed, but the subtrees of the\n"
	      << "root is perturbed (NNI) suring MCMC-analysis.\n";
	else
	  oss << " and its root is perturbed (NNI) during MCMC-analysis.\n";
      }
    oss << StdMCMCModel::print();
    ;
    return oss.str();
  }

  //----------------------------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------------------------
  void
  TreeMCMC::init()
  {
    old_T.partialCopy(*T);
    if(T->hasTimes())
      {
	old_times = T->getTimes();
      }
    if(T->hasRates())
      {
	old_rates = T->getRates();
      }
    if(T->hasLengths())
      {
	old_lengths = T->getLengths();
      }
    //! \todo{What T->getNumberOfLeaves change during MCMC - currently no
    //! check for sanity (below) if this happens}
    if(T->getNumberOfLeaves() < 4)
      {
	cerr << "Warning! TreeMCMC::constructor: Branch-swapping is \n"
	     << "         meaningless on trees with less than four leaves,\n"
	     << "         and will not be performed in the MCMC\n";
	fixRoot();
	fixTree();
      }
    else
      {
	update_idx_limits();                     // tell StdMCMCModel
      }
  }

  void
  TreeMCMC::update_idx_limits()
  {
    Real par = 0.0;
    if(idx_limits[0] != 0)
      {
	idx_limits[0] = (par+=1.0) / n_params;
      }
    if(idx_limits[1] != 0)
      {
	idx_limits[1] = (par+=1.0) / n_params;
      }
    if(idx_limits[2] != 0)
      {
	idx_limits[2] = (par+=1.0) / n_params;
      }
    updateParamIdx();
    return;
  }





  //==============================================================
  //
  // subclass UniformTreeMCMC
  //
  //==============================================================
  UniformTreeMCMC::UniformTreeMCMC(MCMCModel& prior, Tree& T,
				   Real suggestRatio)
    : TreeMCMC(prior, T, suggestRatio),
      p(1.0),
      nLeaves(T.getNumberOfLeaves()),
      isRooted(true)
  {
    init();
  }

  UniformTreeMCMC::UniformTreeMCMC(MCMCModel& prior, Tree& T,
				   const string& name_in,
				   Real suggestRatio)
    : TreeMCMC(prior, T, suggestRatio),
      p(1.0),
      nLeaves(T.getNumberOfLeaves()),
      isRooted(true)
  {
    name = name_in;
    init();
  }

  UniformTreeMCMC::~UniformTreeMCMC()
  {};

  UniformTreeMCMC::UniformTreeMCMC(const UniformTreeMCMC& utm)
    : TreeMCMC(utm),
      p(utm.p),
      nLeaves(utm.nLeaves),
      isRooted(utm.isRooted)
  {}

  UniformTreeMCMC&
  UniformTreeMCMC::operator=(const UniformTreeMCMC& utm)
  {
    if(this != &utm)
      {
	TreeMCMC::operator=(utm);
	p = utm.p;
	nLeaves = utm.nLeaves;
	isRooted = utm.isRooted;
      }
    return *this;
  }

  void UniformTreeMCMC::fixRoot()
  {
    isRooted = true;
    TreeMCMC::fixRoot();
  }

  //-------------------------------------------------------------
  //
  // Interface from ProbabilityModel
  //
  //-------------------------------------------------------------
  Probability
  UniformTreeMCMC::updateDataProbability()
  {
    update();
    return p;
  }

  void
  UniformTreeMCMC::update()
  {
    if(nLeaves != T->getNumberOfLeaves()) // <=> p will have changed!
      {
	nLeaves = T->getNumberOfLeaves();
	init();
      }
    return;
  }

  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  string
  UniformTreeMCMC::print() const
  {
    ostringstream oss;
    oss << name << ": ";
    oss << "Probability of guest tree "
	<< T->getName()
	<< " is uniform over all rooted trees with "
	<< T->getNumberOfLeaves()
	<< " leaves,\nor perhaps modeled elsewhere\n"
	<< TreeMCMC::print()
	<< "\n";
    return oss.str();
  }

  //-------------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------------
  void
  UniformTreeMCMC::init()
  {
    //     unsigned n = nLeaves;
    //     if(isRooted)
    //       {
    // 	n = n-1;
    //       }
    //     else
    //       {
    // 	n = n-2;
    //       }

    //     unsigned denom = 1;
    //     for(unsigned x = 2 * n; x > n; x--)
    //       {
    // 	cerr << "x: " << x << " denom: " << denom << "\n";
    // 	denom *= x;
    //       }
    //     p = pow(2, n) / Real(denom);
    //     return;

    p = 1.0; // REMOVE
    return;  // REMOVE
  }

  //==============================================================
  //
  // subclass GuestTreeMCMC
  //
  //==============================================================
  GuestTreeMCMC::GuestTreeMCMC(MCMCModel& prior,
			       Tree& G, StrStrMap& gs, BirthDeathProbs& bdp,
			       Real suggestRatio)
    : TreeMCMC(prior, G,
	       G.getName() + "_" + bdp.getStree().getName() + "_GuestTree",
	       suggestRatio),
      GuestTreeModel(G, gs, bdp)
  {}

  GuestTreeMCMC::GuestTreeMCMC(MCMCModel& prior,
			       ReconciliationModel& rm,
			       Real suggestRatio)
    : TreeMCMC(prior, rm.getGTree(),
	       rm.getGTree().getName() + "_"
	       + rm.getSTree().getName() + "_Model",
	       suggestRatio),
      GuestTreeModel(rm)
  {}

  GuestTreeMCMC::GuestTreeMCMC(MCMCModel& prior,
			       ReconciliationModel& rm,
			       const string& name_in,
			       Real suggestRatio)
    : TreeMCMC(prior, rm.getGTree(), name_in, suggestRatio),
      GuestTreeModel(rm)
  {
  }

  GuestTreeMCMC::~GuestTreeMCMC()
  {};

  GuestTreeMCMC::GuestTreeMCMC(const GuestTreeMCMC& rtm)
    : TreeMCMC(rtm),
      GuestTreeModel(rtm)
  {}

  GuestTreeMCMC&
  GuestTreeMCMC::operator=(const GuestTreeMCMC& rtm)
  {
    if(this != &rtm)
      {
	TreeMCMC::operator=(rtm);
	GuestTreeModel::operator=(rtm);
      }
    return *this;
  }


  string GuestTreeMCMC::print() const
  {
    return GuestTreeModel::print() + TreeMCMC::print();
  }

  Probability
  GuestTreeMCMC::updateDataProbability()
  {
    update();
    return calculateDataProbability();
  }

  //==============================================================
  //
  // subclass OrthologyMCMC
  //
  //==============================================================
  OrthologyMCMC::OrthologyMCMC(MCMCModel& prior,
			       Tree& G, StrStrMap& gs,
			       BirthDeathProbs& bdp,
			       Real suggestRatio)
    : GuestTreeMCMC(prior, G, gs, bdp, suggestRatio),
      orthoVec(),
      orthoProb(),
      invMRCA(G),
      specNotOrtho(false)
  {
  }

  OrthologyMCMC::OrthologyMCMC(MCMCModel& prior,
			       ReconciliationModel& rm,
			       Real suggestRatio)
    : GuestTreeMCMC(prior, rm, suggestRatio),
      orthoVec(),
      orthoProb(),
      invMRCA(rm.getGTree()),
      specNotOrtho(false)
  {
  }

  OrthologyMCMC::OrthologyMCMC(MCMCModel& prior,
			       ReconciliationModel& rm,
			       const string& name_in,
			       Real suggestRatio)
    : GuestTreeMCMC(prior, rm, suggestRatio),
      orthoVec(),
      orthoProb(),
      invMRCA(rm.getGTree()),
      specNotOrtho(false)
  {
    name = name_in;
  }

  OrthologyMCMC::~OrthologyMCMC()
  {};

  OrthologyMCMC::OrthologyMCMC(const OrthologyMCMC& rtm)
    : GuestTreeMCMC(rtm),
      orthoVec(rtm.orthoVec),
      orthoProb(rtm.orthoProb),
      invMRCA(rtm.invMRCA),
      specNotOrtho(rtm.specNotOrtho)
  {}

  OrthologyMCMC&
  OrthologyMCMC::operator=(const OrthologyMCMC& rtm)
  {
    if(this != &rtm)
      {
	TreeMCMC::operator=(rtm);
	orthoVec = rtm.orthoVec;
	orthoProb = rtm.orthoProb;
	invMRCA = rtm.invMRCA;
	specNotOrtho = rtm.specNotOrtho;
      }
    return *this;
  }


  void
  OrthologyMCMC::estimateOrthology(bool speciationsNotOrthology)
  {
    for(unsigned i = 0; i < T->getNumberOfNodes(); i++)
      {
	if(T->getNode(i)->isLeaf() == false)// && gamma_star.isSpeciation(*u))
	  {
	    orthoVec.push_back(i);
	    orthoProb.push_back(0);
	  }
      }
    if(speciationsNotOrthology)
      {
	specNotOrtho = true;
      }
  }

  void
  OrthologyMCMC::commitOwnState()
  {
    //    invMRCA.update();
  }

  Probability
  OrthologyMCMC::updateDataProbability()
  {
    update();
    if(orthoVec.empty()) // No orthology requested
      {
	return calculateDataProbability();
      }

    return recordOrthology();
  }

  Probability
  OrthologyMCMC::recordOrthology()
  {
    Probability ret = calculateDataProbability();
    for(unsigned i = 0; i < orthoVec.size(); i++)
      {
	Node* u = T->getNode(orthoVec[i]);        
	if(gamma_star.isSpeciation(*u))  // only MPR spec yield orthology
	  {
	    setOrthoNode(u);
	    orthoProb[i] = calculateDataProbability() / ret;
	  }
      }
    setOrthoNode(0);

    return ret;
  }

  string
  OrthologyMCMC::ownStrRep() const
  {
    ostringstream oss;
    oss << TreeMCMC::ownStrRep();
    if(orthoVec.empty() == false)
      {
	if(specNotOrtho)
	  {
	    for(unsigned i = 0; i < orthoVec.size(); i++)
	      {
		Node* u = T->getNode(orthoVec[i]);
		if(gamma_star.isSpeciation(*u))  // only MPR spec yield orthology
		  {
		    oss << orthoProb[i].val() << ";\t";
		  }
	      }
	  }
	else
	  {
	    oss <<  "[";
	    for(unsigned i = 0; i < orthoVec.size(); i++)
	      {
		Node* u = T->getNode(orthoVec[i]);
		if(gamma_star.isSpeciation(*u))  // only MPR spec yield orthology
		  {
		    oss <<  invMRCA.getStrRep(*u, orthoProb[i]);
		  }
	      }
	    oss << "];\t";
	  }
      }
    return oss.str();

  }

  string
  OrthologyMCMC::ownHeader() const
  {
    ostringstream oss;
    oss << TreeMCMC::ownHeader();
    if(orthoVec.empty() == false)
      {
	string tree_name = getTree().getName();
	if(specNotOrtho)
	  {
	    for(unsigned i=0; i < orthoVec.size(); i++)
	      {
		Node* u = T->getNode(orthoVec[i]);
		if(gamma_star.isSpeciation(*u))  // only MPR spec yield orthology
		  {
		    oss << "speciation[" << u->getNumber() << "](logfloat);\t";
		  }
	      }
	  }
	else
	  {
	    oss << "orthology(orthologypairs);\t";
	  }
      }
    return oss.str();
  }


}//end namespace beep
