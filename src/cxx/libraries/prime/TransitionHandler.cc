#include "TransitionHandler.hh"
#include "MatrixTransitionHandler.hh"

namespace beep
{
  //------------------------------------------------------------------  
  //
  // Class TransitionHandler
  //
  //------------------------------------------------------------------
  //------------------------------------------------------------------  
  //
  // Construct/destruct/assign
  //   
  //------------------------------------------------------------------
  TransitionHandler::~TransitionHandler()
  {}
    
  // Constructor used by subclasses. 
  // sequence_type must conform to the ones used by SequenceDATA/Type
  TransitionHandler::TransitionHandler(const std::string& modelName,
				       const SequenceType& sequenceType) 
    : model_name(modelName),
      sequence_type(sequenceType),
      alphabet_size(sequenceType.alphabetSize())
  {}
    
  // Copy constructor used by subclasses. 
  TransitionHandler::TransitionHandler(const TransitionHandler& th)
    : model_name(th.model_name),
      sequence_type(th.sequence_type),
      alphabet_size(th.alphabet_size)
  {}

  // assignment operator used by subclasses. 
  TransitionHandler&
  TransitionHandler::operator=(const TransitionHandler& th)
  {
    if(&th != this)
      {
	model_name = th.model_name;
	sequence_type = th.sequence_type;
	alphabet_size = th.alphabet_size;
      }
    return *this;
  }

  //------------------------------------------------------------------  
  //
  // Interface
  //   
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  // Shared, non-virtual, functions
  //------------------------------------------------------------------
  // TODO: Evaluate which of these functions really are needed /bens

  // Equality operator. Simple for now
  //-----------------------------------------------------------------
  bool TransitionHandler::operator==(const TransitionHandler& th) const
  {
    return model_name == th.model_name;  
  }
    
  // Tests if model and the submitted data are compatible
  // This could be used as an early error-catcher
  //-----------------------------------------------------------------
  bool 
  TransitionHandler::isCompatible(const SequenceData& sd) const
  {
    return sequence_type == sd.getSequenceType();
  }

  // Access (a copy) of the attributes
  // Returns the name of the model
  //-----------------------------------------------------------------
  std::string TransitionHandler::getModel() const
  {
    return model_name;
  }

  // Returns the name of (sequence) type that the model handles.
  //-----------------------------------------------------------------
  SequenceType TransitionHandler::getType() const
  {
    return sequence_type;
  }

  // Returns the alphabet size of the Markov process.
  // TODO: Maybe rename to simply size() /bens
  //-----------------------------------------------------------------
  unsigned 
  TransitionHandler::getAlphabetSize() const
  {
    return alphabet_size;
  }


} //end namespace beep

