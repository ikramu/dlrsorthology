#ifndef EDGEWEIGHTMODEL_HH
#define EDGEWEIGHTMODEL_HH

#include "ProbabilityModel.hh"
#include "BeepVector.hh"

namespace beep
{
  // Forward declarations
  class Node;
  class Tree;

  //--------------------------------------------------------
  //
  //! virtual base class for any type of weight (length, rate, etc)
  //! that is associated to an edge in a tree.
  //
  //--------------------------------------------------------
  class EdgeWeightModel : public ProbabilityModel
  {

  public:

    //----------------------------------------------------------------------
    //
    // Controls how the child edges of the root are perturbed.
    //
    //----------------------------------------------------------------------
    enum RootWeightPerturbation
      {
	BOTH,
	RIGHT_ONLY,
	NONE
      };

  public:
    //--------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //--------------------------------------------------------
    ~EdgeWeightModel()
    {};

    //--------------------------------------------------------
    //
    // Interface
    //
    //--------------------------------------------------------
    // In addition to the standard ProbabilityModel interface, we have:

    // Access parameters
    //------------------------------------------------------------------
    //! Returns the tree on which edge weights are modeled
    virtual const Tree& getTree() const = 0;

    //! number of weights that are modeled
    virtual unsigned nWeights() const = 0;

    //! Returns reference to vector of weight for (incoming edge to) Node u.
    virtual RealVector& getWeightVector() const = 0;

    //! Returns weight for (incoming edge to) Node u.
    virtual Real getWeight(const Node& node) const = 0;

    //! set the weight for (incoming edge to) Node u to 'weight'.
    virtual void setWeight(const Real& weight, const Node& u) = 0;

    //! set the weight for (incoming edge to) Node u to 'weight'.
    //     virtual void setWeightVector(RealVector& weights) = 0;

    //! get the behaviour of how root edges are handled.
    virtual RootWeightPerturbation getRootWeightPerturbation() const = 0;

    //! set the behaviour of how root edges are handled.
    virtual void setRootWeightPerturbation(RootWeightPerturbation rwp) = 0;

    //! get legitimate range of weights
    virtual void getRange(Real& low, Real& high) = 0;

    //-------------------------------------------------------------------
    //
    // IO
    //
    //-------------------------------------------------------------------
    virtual std::string print() const = 0;

  };
}//end namespace beep

#endif
