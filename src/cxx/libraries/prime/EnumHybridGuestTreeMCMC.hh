#ifndef ENUMHYBRIDGUESTTREEMCMC_HH
#define ENUMHYBRIDGUESTTREEMCMC_HH

#include "EnumHybridGuestTreeModel.hh"
#include "TreeMCMC.hh"

namespace beep
{
  //--------------------------------------------------------------
  //
  //! Subclass of TreeMCMC to handle guest tree evolution 
  //! in hybrid networks
  // 
  //--------------------------------------------------------------
  class EnumHybridGuestTreeMCMC : public TreeMCMC, 
			      public EnumHybridGuestTreeModel
  {
  public:
    //--------------------------------------------------------------
    //
    // Construct/destruct/assign
    // 
    //--------------------------------------------------------------
    EnumHybridGuestTreeMCMC(MCMCModel& prior, 
			Tree& G, HybridTree& S,
			StrStrMap& gs, BirthDeathProbs& bdp,
			Real suggestRatio = 1.0);
    ~EnumHybridGuestTreeMCMC();    
    EnumHybridGuestTreeMCMC(const EnumHybridGuestTreeMCMC& rtm);
    EnumHybridGuestTreeMCMC& operator=(const EnumHybridGuestTreeMCMC& rtm);
    
    //--------------------------------------------------------------
    //
    // Interface
    // 
    //--------------------------------------------------------------
    std::string print() const;    
    Probability updateDataProbability();

  };
}//end namespace beep

#endif
