#ifndef TREEPERTURBATIONEVENT_HH
#define TREEPERTURBATIONEVENT_HH

#include <set>

#include "Node.hh"
#include "PerturbationObservable.hh"

namespace beep
{

  /**
   * Extends PerturbationEvent. Contains detailed information
   * about what in a tree that has changed, e.g. topology or edge weight.
   * This class only represents perturbations (not "restorations") and is
   * highly coupled to the available tree perturbation implementations.
   * For this reason, the constructor is private, but there are a number of
   * factory methods available. The object invoking these has ownership of
   * the created event object.
   * 
   * Currently, the details are specified in the following (non-formalized)
   * way:
   * 
   * - Each perturbation will result in one or two "root paths", where edges
   *   along the path and (please note) "child edges" connected to the paths
   *   may have changed, either topologically or weight/time/length-wise.
   * 
   * - Consider the unchanged rooted subtrees connected to the paths via the
   *   "child edges". A subset of these are stored, namely those which have
   *   ancestors which may have been subject to topology changes. (The 
   *   remaining ones are not stored, but implicitly defined.)
   * 
   * To conclude, the root paths contain information on the parts of the tree 
   * which may have explicitly changed (the paths and child edges). The 
   * subtrees contain information on parts which have not changed, but may 
   * still be of importance since the topology above may have changed.
   * 
   * @author: Joel Sjöstrand.
   */
  class TreePerturbationEvent : public PerturbationEvent
  {
	
  public:
	
    /**
     * Tree perturbation types.
     */
    enum TreePerturbationType
      {
	REROOT,
	NNI,
	SPR,
	EDGE_WEIGHT
      };
	
    /**
     * Factory method. Creates an event object detailing a reroot
     * perturbation. Must be called prior to the actual perturbation.
     * Only verified for edge lengths.
     * @param v the node which will be moved just beneath the root.
     * @return the event object detailing the tree changes.
     */
    static TreePerturbationEvent* createReRootInfo(const Node* v);
	
    /**
     * Factory method. Creates an event object detailing an NNI
     * perturbation. Must be called prior to the actual perturbation.
     * NOTE:Only verified for edge lengths.
     * @param v the node which will become a child of its grandparent.
     * @return the event object detailing the tree changes.
     */
    static TreePerturbationEvent* createNNIInfo(const Node* v);
	
    /**
     * Factory method. Creates an event object detailing an SPR
     * perturbation. Must be called prior to the actual perturbation.
     * NOTE: Only verified for edge lengths.
     * @param u_c the node which will be moved to another place in the tree.
     * @param u_c_new the node which will become a sibling of u_c.
     * @return the event object detailing the tree changes.
     */
    static TreePerturbationEvent* createSPRInfo(const Node* u_c, const Node* u_c_new);

    /**
     * Factory method. Creates an event object detailing an edge weight
     * perturbation (for a single edge). Must be called prior to the actual
     * perturbation. NOTE: Only verified for edge lengths.
     * @param v the lower node of the edge which will be pertubed.
     * @return the event object detailing the tree changes.
     */
    static TreePerturbationEvent* createEdgeWeightInfo(const Node* v);
	
    /**
     * Destructor.
     */
    virtual ~TreePerturbationEvent();
	
    /**
     * Returns the tree perturbation type.
     * @return the tree perturbation type.
     */
    TreePerturbationType getTreePerturbationType() const;
	
    /**
     * Returns a set of unchanged rooted subtrees whose ancestors may
     * have changed topologically. They should all be connected
     * to one of the tree root paths by the edge above (i.e. their planted
     * equivalents should have their tip on the paths).
     * @return the set of subtrees.
     */
    const std::set<const Node*>& getSubtrees() const;

    /**
     * Returns the paths to the tree root which have been subject to change,
     * topologically and/or weight/time/length-wise. Please note that "child edges"
     * of these paths may also have changed. The path nodes should be different, but
     * the paths may by all means overlap.
     * @param path1 lower end of the first tree root path.
     * @param path2 lower end of the second tree root path. Often null.
     */
    void getRootPaths(const Node*& path1, const Node*& path2) const;
	
    /**
     * Returns string information about the object.
     * @return an info string.
     */
    virtual std::string print() const;

    /**
     * Prints debugging info about member variables to stderr.
     */
    void debugInfo() const;
	
  private:
	
    /**
     * Constructor.
     * @param treePertType the tree perturbation type.
     * @param rootPath the first tree root path.
     * @param rootPath2 the second tree root path.
     */
    TreePerturbationEvent(TreePerturbationType treePertType,
			  const Node* rootPath, const Node* rootPath2 = NULL);
	
    /**
     * Helper. Adds a subtree to the list.
     * @param subroot the root of the subtree to add.
     */
    void insertSubtree(const Node* subroot);
	
  public:
	
  private:
	
    /** Tree perturbation type. */
    TreePerturbationType m_treePertType;
	
    /**
     * Roots of unchanged subtrees whose ancestors may have changed
     * topologically.
     */
    std::set<const Node*> m_subtrees;
	
    /**
     * Lower node of the first path to the tree root which may have changed,
     * topologically and/or weight-wise.
     */
    const Node* m_rootPath;
	
    /**
     * Lower node of the second path to the tree root which may have changed,
     * topologically and/or weight-wise. Often null.
     */
    const Node* m_rootPath2;
  };

} // end namespace beep

#endif /*TREEPERTURBATIONEVENT_HH*/

