#ifndef HYBRIDGUESTTREEMCMC_HH
#define HYBRIDGUESTTREEMCMC_HH

#include "HybridGuestTreeModel.hh"
#include "TreeMCMC.hh"

namespace beep
{
  //--------------------------------------------------------------
  //
  //! Subclass of TreeMCMC to handle guest tree evolution 
  //! in hybrid networks
  // 
  //--------------------------------------------------------------
  class HybridGuestTreeMCMC : public TreeMCMC, 
			      public HybridGuestTreeModel
  {
  public:
    //--------------------------------------------------------------
    //
    // Construct/destruct/assign
    // 
    //--------------------------------------------------------------
    HybridGuestTreeMCMC(MCMCModel& prior, 
			Tree& G, HybridTree& S,
			StrStrMap& gs, BirthDeathProbs& bdp,
			Real suggestRatio = 1.0);
    ~HybridGuestTreeMCMC();    
    HybridGuestTreeMCMC(const HybridGuestTreeMCMC& rtm);
    HybridGuestTreeMCMC& operator=(const HybridGuestTreeMCMC& rtm);
    
    //--------------------------------------------------------------
    //
    // Interface
    // 
    //--------------------------------------------------------------
    std::string print() const;    
    Probability updateDataProbability();

  };
}//end namespace beep

#endif
