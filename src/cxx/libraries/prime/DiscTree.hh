#ifndef DISCTREE_HH
#define DISCTREE_HH

#include <string>
#include <utility>
#include <vector>

#include "BeepVector.hh"
#include "Node.hh"
#include "Tree.hh"

namespace beep
{

/**
 * Encapsulation of a Tree that discretizes the nodes and creates 
 * discretization points along its edges. Consider a discretization of the
 * time interval spanned by the Tree into equidistant steps. The separators
 * between timesteps are referred to as "grid lines" (albeit one-dimensional),
 * where grid index 0 corresponds to the leaves, and the highest grid index
 * the top of the tree.
 * 
 * The root-to-leaf time defines the timestep (not the top-to-leaf time). 
 * A (non-null) top time is after that rounded off using this step.
 *  
 * A DiscTree consists of a uni- and bifurcating tree where nodes (coined 
 * "points") are created for each position where a grid line intersects with
 * an edge in the original Tree. Moreover, for each node of the original Tree,
 * there is a correponding point, although placed at the closest grid line 
 * (implying that nodes are also discretized). Thus, only the latter have 
 * two children.
 * 
 * To access points in the DiscTree, one uses the nodes of the original 
 * Tree along with a grid index. For each edge in the original Tree, there 
 * is a vector with the points corresponding to the edge. It is indexed so
 * that the point corresponding to the lower node of the original Tree has 
 * index zero, followed by the pure discretization points.
 * 
 * Note: the main difference between this class and TreeDiscretizer is that 
 * the time values of nodes are discretized too. Therefore it is highly 
 * advisable not to use time values of original nodes, but always strive to
 * use the discretized values, and the same applies to the top time. Please
 * keep this in mind.
 * 
 * Note: unlike some other tree discretizations, here there is a point on the 
 * very tip of the top time edge.
 */
class DiscTree
{

public:
	
	/**
	 * Convenience notation for referring to discretization points as 
	 * index-node pairs. The index refers to the grid index, while the
	 *  node refers to the edge on which the point is located. 
	 * Note: The offset index of the point with respect to its edge
	 * can be retrieved with 'getRelativeIndex()'.
	 */
	typedef std::pair<unsigned, const Node*> Point;
	
	/**
	 * Constructor. Creates a discretized version of a Tree using the
	 *  specified number of intervals from root to leaves. The top time 
	 * is rounded off to the closes discretization grid line.
	 * @param S the underlying tree.
	 * @param noOfRootToLeafIvs the number of intervals of the
	 *        discretization grid from root to leaves.
	 */
	DiscTree(Tree& S, unsigned noOfRootToLeafIvs);
	
	/**
	 * Destructor.
	 */
	virtual ~DiscTree();
	
	/**
	 * Makes a clean update of the discretized tree based on the underlying
	 * Tree and the specified top time.
	 */
	void update();
	
	/**
	 * Returns the total number of points in the discretized tree.
	 * @return the number of points.
	 */
	unsigned getNoOfPtsInTree() const;
	
	/**
	 * Returns the number of points for an edge, as usual excluding the 
	 * point of the upper node. Remember, however, that the top time edge
	 *  (unless null) includes the point on its upper tip.
	 * @param node the end node of the edge.
	 * @return the number of points.
	 */
	unsigned getNoOfPtsOnEdge(const Node* node) const;
	
	/**
	 * Returns the total number of discretization intervals spanning the
	 * tree.
	 * @return the number of intervals.
	 */
	unsigned getNoOfIvs() const;
	
	/**
	 * Returns the number of discretization intervals from root to leaves.
	 * @return the number of intervals.
	 */
	unsigned getNoOfRootToLeafIvs() const;
	
	/**
	 * Returns the number of discretization intervals spanning (and 
	 * defining) the top time edge.
	 * @return the number of intervals.
	 */
	unsigned getNoOfTopTimeIvs() const;
	
	/**
	 * Returns the underlying Tree.
	 * @return the Tree.
	 */
	Tree& getOrigTree() const;
	
	/**
	 * Returns the underlying Tree's root node.
	 * @return the root.
	 */
	const Node* getOrigRootNode() const;
	
	/**
	 * Returns the underlying Tree's node for a certain index.
	 * @param index the node's index.
	 * @return the node.
	 */
	const Node* getOrigNode(unsigned index) const;
	
	/**
	 * Returns the time step between discretization grid lines.
	 * @return the time step.
	 */
	Real getTimestep() const;
	
	/**
	 * Returns the root-to-leaf time in the discretized tree.
	 */
	Real getRootToLeafTime() const;
	
	/**
	 * Returns the time length of the top time edge.
	 * @return the time.
	 */
	Real getTopTime() const;
	
	/**
	 * Returns the topmost time value, i.e. root time + top time edge 
	 * time. 
	 * @return the time value.
	 */
	Real getTopToLeafTime() const;
	
	/**
	 * Returns the topmost point in the tree, i.e. where the toptime 
	 * edge ends.
	 * @return the topmost point in the tree.
	 */
	Point getTopPt() const;
	
	/**
	 * Returns the index of the point relative to the lower end of its 
	 * edge, i.e. 0 for the point corresponding to the lower node, 1 
	 * for its parent point, etc. Note: for an invalid point, the 
	 * behaviour is undefined.
	 * @param pt the discretization point.
	 * @return the relative index.
	 */
	unsigned getRelativeIndex(Point pt) const;
	
	/**
	 * Returns the grid index on which node is placed.
	 * @param node the node.
	 * @return the grid index.
	 */
	unsigned getGridIndex(const Node* node) const;
	
	/**
	 * Returns the grid index span of an edge. The grid index which the
	 *  parent node occupies is not included.
	 * @param node the lower node of the edge.
	 * @return the grid index span where 'first' <= 'second'.
	 */
	std::pair<unsigned,unsigned> getEdgeGridIndices(const Node* node) const;
	
	/**
	 * Returns true if the specified grid index is covered by the
	 * specified edge. As usual, the point corresponding to the upper
	 * node of an edge is not considered part of it.
	 * @param gridIndex the grid index.
	 * @param node the lower node of the edge.
	 * @return true if the grid index is covered by the edge.
	 */
	bool isWithinEdge(unsigned gridIndex, const Node* node) const;
	
	/**
	 * Returns true if the specified grid index is strictly above the
	 * specified edge. As usual, the point corresponding to the upper
	 * node of an edge is not considered part of it.
	 * @param gridIndex the grid index.
	 * @param node the lower node of the edge.
	 * @return true if the grid index is above the edge.
	 */
	bool isAboveEdge(unsigned gridIndex, const Node* node) const;
	
	/**
	 * Returns true if the specified grid index is strictly below the
	 * specified edge.
	 * @param gridIndex the grid index.
	 * @param node the lower node of the edge.
	 * @return true if the grid index is below the edge.
	 */
	bool isBelowEdge(unsigned gridIndex, const Node* node) const;
	
	/**
	 * Returns the point on a specified grid index located on the path
	 * from a node to the root's top. It is required that the grid index
	 * is on or above the node.
	 * @param gridIndex the grid index of the point.
	 * @param the lower end of the path on which the point is located.
	 * @return the point.
	 */
	Point getPt(unsigned gridIndex, const Node* node) const;
	
	/**
	 * Returns the time corresponding to the specified grid index.
	 * @param gridIndex the grid index.
	 * @return the time.
	 */
	Real getPtTime(unsigned gridIndex) const;

	/**
	 * Returns the discretized time corresponding to the specified node.
	 * @param node the node.
	 * @return the discretized time of the node.
	 */
	Real getPtTime(const Node* node) const;

	/**
	 * Returns iterator to time values for all points covered by
	 * an edge. As usual, the point corresponding to the upper
	 * node of an edge is not considered part of it.
	 * @param node the lower node of the edge.
	 * @param itBegin iterator to the time value of the first point of the
	 * edge, i.e. the one corresponding to the node.
	 * @param itEnd iterator to the end of the time values.
	 */
	void getPtTimes(const Node* node,
			std::vector<Real>::const_iterator& itBegin,
			std::vector<Real>::const_iterator& itEnd) const;
	
	/**
	 * Returns the discretized time length of an edge.
	 * @param node the lower node of the edge.
	 * @return the discretized time length of the edge.
	 */
	Real getEdgeTime(const Node* node) const;
	
	/**
	 * For the tree, returns the maximum found discrepancy between the 
	 * original time value of a node and its corresponding discretized 
	 * point. Can be seen as a simple measure of the accuracy of the
	 * discretization. 
	 * @return the maximum node time discrepancy.
	 */
	Real getMaxNodeTimeDiff() const;
	
	/**
	 * For the tree, returns the maximum found discrepancy between the original time value of
	 * an edge and its corresponding discretized edge. Can be seen as a simple measure of
	 * the accuracy of the discretization.
	 * @return the maximum edge time discrepancy.
	 */
	Real getMaxEdgeTimeDiff() const;
	
	/**
	 * Returns the smallest time distance between two nodes in the original Tree.
	 * @param includeTopTime indicates whether the top time edge should be taken into account.
	 * @return the smallest node-to-node time.
	 */
	Real getMinOrigEdgeTime(bool includeTopTime = true) const;
		
	/**
	 * Returns true if there is an edge in the original Tree which has
	 * not been divided by at least one pure discrete point in the discretized tree,
	 * i.e. the discretized nodes are adjacent.
	 * A null top time edge is discarded.
	 * @return true if an edge has not been divided.
	 */
	bool containsNonDividedEdge() const;
	
	/**
	 * Prints miscellaneous information about the DiscTree to cerr.
	 * @param printNodeInfo prints node and edge info if true.
	 */
	void debugInfo(bool printNodeInfo=true) const;
	
private:
	
	void createGridTimes();
	
	void createGridIndices(const Node* node, unsigned parentGridIndex);
	
public:
	
private:
	
	/** The underlying original Tree. */
	Tree& m_S;
	
	/** No of intervals of discretization grid from root to leaves. */
	unsigned m_noOfRootToLeafIvs;
	
	/** No of intervals of top time edge, thus defining its time length. */
	unsigned m_noOfTopTimeIvs;
	
	/** Time step in discretization grid. */
	Real m_timestep;
	
	/** Times for each grid index in ascending order. */
	std::vector<Real> m_gridTimes;
	
	/** For each edge with lower node X, contains the lowermost grid index of the edge. */
	BeepVector<unsigned> m_loGridIndices;
	
	/** For each edge with lower node X, contains the uppermost grid index of the edge. */
	BeepVector<unsigned> m_upGridIndices;
};

} // end namespace beep

#endif /* DISCTREE_HH*/

