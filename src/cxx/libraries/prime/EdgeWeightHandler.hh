#ifndef EDGEWEIGHTHANDLER
#define EDGEWEIGHTHANDLER

#include <iostream>
#include <sstream>

#include "Beep.hh"
#include "EdgeRateModel.hh"
#include "Node.hh"
#include "BeepVector.hh"
#include "Tree.hh"


namespace beep
{
  //-------------------------------------------------------------------
  //
  // Class EdgeWeightHandler
  //! Handles edge lengths (branch lengths) for Tree Markov models.
  //! This base version equates edge lengths with edge rates 
  //! as modeled by EdgeRateModel
  //!
  //! author Bengt Sennblad copyright the mcmc-club, SBC
  //
  //-------------------------------------------------------------------
  class EdgeWeightHandler
  {
    //-------------------------------------------------------------------
    //
    // Constructor, Destructor, Assignment
    //
    //-------------------------------------------------------------------
  public:
    EdgeWeightHandler(EdgeWeightModel& erm);
    EdgeWeightHandler(const EdgeWeightHandler& ewh);
    virtual ~EdgeWeightHandler();
    EdgeWeightHandler& operator=(const EdgeWeightHandler& ewh);

    //-------------------------------------------------------------------
    //
    // Constructor, Destructor, Assignment
    //
    //-------------------------------------------------------------------
    const Tree& getTree();

    //-------------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------------
    //! Returns the edge rate * edge time 
    //! Returns the rate of incomimg edge to n 
    virtual Real getWeight(const Node& n) const;
    virtual Real getWeight(const Node* n) const;
    virtual Real operator[](const Node& n) const;
    virtual Real operator[](const Node* n) const;

    //! Updates edgelengths. Base class has nothing to update.
    virtual void update();

    //-------------------------------------------------------------------
    //
    // IO
    //
    //-------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const EdgeWeightHandler& ewh);
    virtual std::string print() const;

  protected:
    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
    virtual void init(EdgeWeightModel& erm);

    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
  protected:
    const Tree* T; //!< access to Tree
    RealVector* lengths;

  };






  //-------------------------------------------------------------------
  //
  // Class EdgeTimeRateHandler
  //! Handles time-dependent edge lengths (branch lengths) for
  //! Tree Markov models.
  //! Edge lengths are equated with edge rate * edge time 
  //!
  //! author Bengt Sennblad copyright the mcmc-club, SBC
  //
  //-------------------------------------------------------------------
  class EdgeTimeRateHandler : public EdgeWeightHandler
  {
    //-------------------------------------------------------------------
    //
    // Constructor, Destructor, Assignment
    //
    //-------------------------------------------------------------------
  public:
    EdgeTimeRateHandler(EdgeRateModel& erm);
    EdgeTimeRateHandler(const EdgeTimeRateHandler& erh);
    ~EdgeTimeRateHandler();
    EdgeTimeRateHandler& operator=(const EdgeTimeRateHandler& ewh);


    //! Updates edgelengths. Checks if either rates or times of any edge is 
    //! perturbed and update the corresponding edge weight
    // TODO: Two alternatives: 
    // Either, update always update all edgelengths (possibly only if any 
    // perturbation is indicated), or 
    // we could skip update() and edgelengths, and let operator[x] always  
    // return the multiplication edgeRate[x] * x.getTime(). 
    void update();

    //-------------------------------------------------------------------
    //
    // IO
    //
    //-------------------------------------------------------------------
    virtual std::string print() const;
 
  protected:
    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
    virtual void init(EdgeRateModel& erm);

    };

}//end namespace beep

#endif
