#include <errno.h>
#include <limits>
#include <sstream>

#include "BeepOption.hh"

using namespace std;

namespace beep
{
  namespace option
  {

    BeepOptionMap::BeepOptionMap(string helpIds, string unknownOptionErrMsg) 
      : m_helpIds(),
	m_unknownOptionErrMsg(unknownOptionErrMsg),
	m_options(),
	m_optionsById(),
	m_optionsInOrderOfIns()
    {
      // Store valid help IDs in set.
      string token;
      istringstream iss(helpIds);
      while (getline(iss, token, ','))
	{
	  m_helpIds.insert(token);
	}
    }


    BeepOptionMap::~BeepOptionMap()
    {
      map<string, BeepOption*>::iterator it = m_options.begin(); 
      for (; it != m_options.end(); ++it)
	{
	  BeepOption* bo = (*it).second;
	  delete bo;
	}
      m_options.clear();
      m_optionsById.clear();
      m_optionsInOrderOfIns.clear();
    }


    bool BeepOptionMap::parseOptions(int& argIndex, int argc, char **argv)
    {
      while (argIndex < argc && argv[argIndex][0] == '-') 
	{
	  string dashId = string(argv[argIndex]);
	  string id = string(dashId).erase(0, 1);
		
	  // Help option encountered, abort without exception 
	  // by returning false.
	  if (m_helpIds.find(id) != m_helpIds.end())	
	    { 
	      return false; 
	    }
		
	  // When option not recognized.
	  if (id == "" || m_optionsById.find(id) == m_optionsById.end())
	    {
	      throw AnError(m_unknownOptionErrMsg + ' ' 
			    + dashId + '.');
	    } 
		
		// Parse option depending on type. Individual methods throw errors if faulty data.
		BeepOption* bo = m_optionsById[id];
		switch (bo->getType())
		{
			case EMPTY:
				argIndex++;
				break;
			case BOOL:
				parseBool(static_cast<BoolOption*>(bo), argIndex, argc, argv);
				break;
			case UNSIGNED:
				parseUnsigned(static_cast<UnsignedOption*>(bo), argIndex, argc, argv);
				break;
			case INT:
				parseInt(static_cast<IntOption*>(bo), argIndex, argc, argv);
				break;
			case DOUBLE:
				parseDouble(static_cast<DoubleOption*>(bo), argIndex, argc, argv);
				break;
			case STRING:
				parseString(static_cast<StringOption*>(bo), argIndex, argc, argv);
				break;
			case INT_X2:
				parseIntX2(static_cast<IntX2Option*>(bo), argIndex, argc, argv);
				break;
			case DOUBLE_X2:
				parseDoubleX2(static_cast<DoubleX2Option*>(bo), argIndex, argc, argv);
				break;
			case DOUBLE_X3:
				parseDoubleX3(static_cast<DoubleX3Option*>(bo), argIndex, argc, argv);
				break;
			case STRING_ALT:
				parseStringAlt(static_cast<StringAltOption*>(bo), argIndex, argc, argv);
				break;
			case USER_SUBST_MODEL:
				parseUserSubstModel(static_cast<UserSubstModelOption*>(bo), argIndex, argc, argv);
			default:
				throw AnError("Unknown Beep Option Type!");
				break;
		}
		argIndex++;
	}
      return true;
    }


    void BeepOptionMap::parseBool(BoolOption* bo, int& argIndex,
				  int argc, char **argv)
    {
      if (!bo->hasBeenParsed)
	{
	  bo->val = !(bo->val);		// Invert default.
	  bo->hasBeenParsed = true;
	}
    }


    void BeepOptionMap::parseUnsigned(UnsignedOption* bo, int& argIndex, int argc, char **argv)
    {
      try
	{
	  if (!toUnsigned(argv[++argIndex], bo->val)) 
	    { 
	      throw "Dummy"; 
	    }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


    void BeepOptionMap::parseInt(IntOption* bo, int& argIndex, 
				 int argc, char **argv)
    {
      try
	{
	  if (!toInt(argv[++argIndex], bo->val)) 
	    { 
	      throw "Dummy"; 
	    }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


    void BeepOptionMap::parseDouble(DoubleOption* bo, int& argIndex,
				    int argc, char **argv)
    {
      try
	{
	  if (!toDouble(argv[++argIndex], bo->val)) 
	    { 
	      throw "Dummy"; 
	    }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


    void BeepOptionMap::parseString(StringOption* bo, int& argIndex, 
				    int argc, char **argv)
    {
      try
	{
	  if ((++argIndex) >= argc) 
	    {
	      throw "Dummy"; 
	    }
	  bo->val = string(argv[argIndex]);
	  if (bo->valCase == UPPER)
	    { 
	      transform(bo->val.begin(), bo->val.end(), bo->val.begin(), 
			(int(*)(int))toupper); 
	    }
	  else if (bo->valCase == LOWER)
	    { 
	      transform(bo->val.begin(), bo->val.end(), bo->val.begin(), 
			(int(*)(int))tolower); 
	    }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


void BeepOptionMap::parseIntX2(IntX2Option* bo, int& argIndex, int argc, char **argv)
{
	try
	{
	  if (!toInt(argv[++argIndex], bo->val.first)) { throw "Dummy"; }
	  if (!toInt(argv[++argIndex], bo->val.second)) { throw "Dummy"; }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


void BeepOptionMap::parseDoubleX2(DoubleX2Option* bo, int& argIndex, int argc, char **argv)
{
	try
	{
	  if (!toDouble(argv[++argIndex], bo->val.first)) 
	    { 
	      throw "Dummy"; 
	    }
	  if (!toDouble(argv[++argIndex], bo->val.second)) 
	    { 
	      throw "Dummy"; 
	    }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


void BeepOptionMap::parseDoubleX3(DoubleX3Option* bo, int& argIndex, int argc, char **argv)
{
	try
	{
		if (!toDouble(argv[++argIndex], bo->val))  { throw "Dummy"; }
		if (!toDouble(argv[++argIndex], bo->val2)) { throw "Dummy"; }
		if (!toDouble(argv[++argIndex], bo->val3)) { throw "Dummy"; }
	}
	catch (...)
	{
		throw AnError(bo->parseErrMsg);
	}
	bo->hasBeenParsed = true;
}


void BeepOptionMap::parseStringAlt(StringAltOption* bo, int& argIndex, int argc, char **argv)
{
	try
	{
	  if ((++argIndex) >= argc) 
	    {
	      throw "Dummy"; 
	    }
	  bo->val = string(argv[argIndex]);
	  if (bo->valCase == UPPER)
	    { 
	      transform(bo->val.begin(), bo->val.end(), bo->val.begin(), 
			(int(*)(int))toupper); 
	    }
	  else if (bo->valCase == LOWER)
	    { 
	      transform(bo->val.begin(), bo->val.end(), bo->val.begin(), 
			(int(*)(int))tolower); 
	    }
		
	  string valTmp = string(bo->val);
	  if (bo->ignoreCase) 
	    { 
	      transform(valTmp.begin(), valTmp.end(), valTmp.begin(), 
			(int(*)(int))toupper); 
	    }
	  bool found = false;
	  for (set<string>::iterator it = bo->validVals.begin(); 
	       it != bo->validVals.end(); ++it)
	    {
	      string alt = string(*it);
	      if (bo->ignoreCase) 
		{ 
		  transform(alt.begin(), alt.end(), alt.begin(), 
			    (int(*)(int))toupper); 
		}
	      if (valTmp == alt) 
		{
		  found = true; break; 
		}
	    }
	  if (!found) 
	    {
	      throw "Dummy"; 
	    }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


    void BeepOptionMap::parseUserSubstModel(UserSubstModelOption* bo,
					    int& argIndex, int argc, 
					    char **argv)
    {
      // Read sequence type.
      int dim = 0;
      try
	{
	  bo->type = argv[++argIndex];
	  if (bo->ignoreCase)
	    {
	      transform(bo->type.begin(), bo->type.end(), bo->type.begin(), 
			(int(*)(int))toupper);
	    }
	  if (bo->type == "DNA")            
	    {
	      dim = 4; 
	    }
	  else if (bo->type == "AMINOACID")
	    {
	      dim = 20; 
	    }
	  else if (bo->type == "CODON")     
	    {
	      dim = 61; 
	    }
	  else                            
	    {
	      throw "Dummy"; 
	    }
	}
      catch (...)
	{
	  throw AnError(bo->parseErrMsg);
	}
	
      // Check that indata contains enough values.
      if (argIndex + (dim * (dim + 1) / 2) >= argc)
	{
	  throw AnError(bo->sizeParseErrMsg);
	}
	
      // Parse Pi.
      try
	{
	  for (int i = 0; i < dim; ++i)
	    {
	      double piVal;
	      toDouble(argv[++argIndex], piVal);
	      bo->pi.push_back(piVal);
	    }
	}
      catch (...)
	{
	  throw AnError(bo->piParseErrMsg);
	}
	
      // Parse R.
      try
	{
	  for (int i= 0; i < dim * (dim - 1) / 2; ++i)
	    {
	      double rVal;
	      toDouble(argv[++argIndex], rVal);
	      bo->r.push_back(rVal);
	    }
	}
      catch (...)
	{
	  throw AnError(bo->rParseErrMsg);
	}
      bo->hasBeenParsed = true;
    }


    void BeepOptionMap::addOption(string name, BeepOption* option)
    {
      m_options[name] = option;
      m_optionsById[option->id] = option;
      m_optionsInOrderOfIns.push_back(option);
    }


    BeepOption* BeepOptionMap::getOption(string name)
    {
      if (m_options.find(name) == m_options.end())
	{ 
	  throw AnError("No such option: '" + name + "'"); 
	}
      return (m_options[name]);
    }


    BeepOption* BeepOptionMap::getOptionById(string id)
    {
      if (m_optionsById.find(id) == m_optionsById.end())
	{ 
	  throw AnError("No such option.");
	}
      return (m_optionsById[id]);
    }


    bool BeepOptionMap::hasBeenParsed(string name)
    {
      return (m_options[name]->hasBeenParsed);
    }


    void BeepOptionMap::addBoolOption(string name, string id, 
				      bool defaultVal, string helpMsg)
    {
      addOption(name, new BoolOption(id, defaultVal, helpMsg));	
    }


    bool BeepOptionMap::getBool(string name)
    {
      BeepOption* bo = getOption(name);
      if (bo->getType() != BOOL)
	{ 
	  throw AnError("Wrong option type."); 
	}
      return ((static_cast<BoolOption*>(bo))->val);
    }


    BoolOption* BeepOptionMap::getBoolOption(string name)
    {
      if (m_options.find(name) == m_options.end())
	{ 
	  throw AnError("No such option: '" + name + "'"); 
	}
      return (static_cast<BoolOption*>(m_options[name]));
    }


    void BeepOptionMap::addUnsignedOption(string name, string id,
					  unsigned defaultVal, string helpMsg)
    {
      addOption(name, new UnsignedOption(id, defaultVal, helpMsg));
    }


    unsigned BeepOptionMap::getUnsigned(string name)
    {
      BeepOption* bo = getOption(name);
      if (bo->getType() != UNSIGNED)
	{ 
	  throw AnError("Wrong option type."); 
	}
      return ((static_cast<UnsignedOption*>(bo))->val);
    }


    UnsignedOption* BeepOptionMap::getUnsignedOption(string name)
    {
      if (m_options.find(name) == m_options.end())
	{ 
	  throw AnError("No such option: '" + name + "'"); 
	}
      return (static_cast<UnsignedOption*>(m_options[name]));
    }


    void BeepOptionMap::addIntOption(string name, string id, int defaultVal,
				     string helpMsg)
    {
      addOption(name, new IntOption(id, defaultVal, helpMsg));
    }


    int BeepOptionMap::getInt(string name)
    {
      BeepOption* bo = getOption(name);
      if (bo->getType() != INT)
	{ 
	  throw AnError("Wrong option type."); 
	}
      return ((static_cast<IntOption*>(bo))->val);
    }


    IntOption* BeepOptionMap::getIntOption(string name)
    {
      if (m_options.find(name) == m_options.end())
	{ 
	  throw AnError("No such option: '" + name + "'"); 
	}
      return (static_cast<IntOption*>(m_options[name]));
    }


    void BeepOptionMap::addDoubleOption(string name, string id, 
					double defaultVal, string helpMsg)
    {
      addOption(name, new DoubleOption(id, defaultVal, helpMsg));
    }


    double BeepOptionMap::getDouble(string name)
    {
      BeepOption* bo = getOption(name);
      if (bo->getType() != DOUBLE)
	{ 
	  throw AnError("Wrong option type."); 
	}
      return ((static_cast<DoubleOption*>(bo))->val);
    }


    DoubleOption* BeepOptionMap::getDoubleOption(string name)
    {
      if (m_options.find(name) == m_options.end())
	{ 
	  throw AnError("No such option: '" + name + "'"); 
	}
      return (static_cast<DoubleOption*>(m_options[name]));
    }


    void BeepOptionMap::addStringOption(string name, string id, 
					string defaultVal, string helpMsg, 
					StringCase valCase)
    {
      addOption(name, new StringOption(id, defaultVal, helpMsg, valCase));
    }


    string BeepOptionMap::getString(string name)
    {
      BeepOption* bo = getOption(name);
      if (bo->getType() != STRING)
	{ 
	  throw AnError("Wrong option type."); 
	}
      return ((static_cast<StringOption*>(bo))->val);
    }


    StringOption* BeepOptionMap::getStringOption(string name)
    {
      if (m_options.find(name) == m_options.end())
	{ 
	  throw AnError("No such option: '" + name + "'"); 
	}
      return (static_cast<StringOption*>(m_options[name]));
    }


void BeepOptionMap::addIntX2Option(string name, string id, pair<int,int> defaultVal,
		string helpMsg)
{
	addOption(name, new IntX2Option(id, defaultVal, helpMsg));
}


pair<int,int> BeepOptionMap::getIntX2(string name)
{
	BeepOption* bo = getOption(name);
	if (bo->getType() != INT_X2)
	{ throw AnError("Wrong option type."); }
	return ((static_cast<IntX2Option*>(bo))->val);
}


IntX2Option* BeepOptionMap::getIntX2Option(string name)
{
	if (m_options.find(name) == m_options.end())
	{ throw AnError("No such option: '" + name + "'"); }
	return (static_cast<IntX2Option*>(m_options[name]));
}


void BeepOptionMap::addDoubleX2Option(string name, string id,
		pair<double,double> defaultVal, string helpMsg)
{
	addOption(name, new DoubleX2Option(id, defaultVal, helpMsg));
}


pair<double,double> BeepOptionMap::getDoubleX2(string name)
{
	BeepOption* bo = getOption(name);
	if (bo->getType() != DOUBLE_X2)
	{ throw AnError("Wrong option type."); }
	return ((static_cast<DoubleX2Option*>(bo))->val);
}


DoubleX2Option* BeepOptionMap::getDoubleX2Option(string name)
{
	if (m_options.find(name) == m_options.end())
	{ throw AnError("No such option: '" + name + "'"); }
	return (static_cast<DoubleX2Option*>(m_options[name]));
}


void BeepOptionMap::addDoubleX3Option(string name, string id,
	double defaultVal, double defaultVal2, double defaultVal3, string helpMsg)
{
	addOption(name, new DoubleX3Option(id, defaultVal,
			defaultVal2, defaultVal3, helpMsg));
}


vector<double> BeepOptionMap::getDoubleX3(string name)
{
	BeepOption* bo = getOption(name);
	if (bo->getType() != DOUBLE_X3)
	{ throw AnError("Wrong option type."); }
	DoubleX3Option* d3bo = static_cast<DoubleX3Option*>(bo); 
	vector<double> v;
	v.push_back(d3bo->val);
	v.push_back(d3bo->val2);
	v.push_back(d3bo->val3);
	return v;
}


DoubleX3Option* BeepOptionMap::getDoubleX3Option(string name)
{
	if (m_options.find(name) == m_options.end())
	{ throw AnError("No such option: '" + name + "'"); }
	return (static_cast<DoubleX3Option*>(m_options[name]));
}


void BeepOptionMap::addStringAltOption(string name, string id, string defaultVal,
		string validVals, string helpMsg, StringCase valCase, bool ignoreCase)
{
	addOption(name, new StringAltOption(id, defaultVal, validVals, helpMsg, valCase, ignoreCase));
}


    string BeepOptionMap::getStringAlt(string name)
    {
      BeepOption* bo = getOption(name);
      if (bo->getType() != STRING_ALT)
	{ 
	  throw AnError("Wrong option type."); 
	}
      return ((static_cast<StringAltOption*>(bo))->val);
    }


    void BeepOptionMap::addUserSubstModelOption(string name, string id, 
						string helpMsg,
						bool ignoreCase)
    {
      addOption(name, new UserSubstModelOption(id, helpMsg, ignoreCase));
    }


    StringAltOption* BeepOptionMap::getStringAltOption(string name)
    {
      if (m_options.find(name) == m_options.end())
	{ 
	  throw AnError("No such option: '" + name + "'"); 
	}
      return (static_cast<StringAltOption*>(m_options[name]));
    }


    UserSubstModelOption* BeepOptionMap::getUserSubstModelOption(string name)
    {
      BeepOption* bo = getOption(name);
      if (bo->getType() != USER_SUBST_MODEL)
	{ 
	  throw AnError("Wrong option type."); 
	}
      return (static_cast<UserSubstModelOption*>(bo));
    }


    bool BeepOptionMap::toInt(char* str, int& result)
    {
      char *endPtr;
      errno = 0;
      long val = strtol(str, &endPtr, 10);
      if (errno || endPtr == str || *endPtr || 
	  val < numeric_limits<int>::min() || val > numeric_limits<int>::max())
	{
	  return false;
	}
      result = static_cast<int>(val);
      return true;
    }


    bool BeepOptionMap::toUnsigned(char* str, unsigned& result)
    {
      char *endPtr;
      errno = 0;
      unsigned long val = strtoul(str, &endPtr, 10);
      if (errno || endPtr == str || *endPtr ||
	  val > numeric_limits<unsigned>::max())
	{
	  return false;
	}
      result = static_cast<unsigned>(val);
      return true;
    }


    bool BeepOptionMap::toDouble(char* str, double& result)
    {
      char *endPtr;
      errno = 0;
      double val = strtod(str, &endPtr);
      if (errno || endPtr == str || *endPtr)
	{
	  return false;
	}
      result = val;
      return true;
    }


    ostream& operator<<(ostream& o, const BeepOption &bo)
    {
      ostringstream oss;
      oss << bo.helpMsg;
      return o << oss.str();    
    }


    ostream& operator<<(ostream& o, const BeepOption *bo)
    {
      return operator<<(o, *bo);
    }


    ostream& operator<<(ostream& o, const BeepOptionMap &bom)
    {
      ostringstream oss;
      vector<BeepOption*>::const_iterator it;
      for (it = bom.m_optionsInOrderOfIns.begin(); 
	   it != bom.m_optionsInOrderOfIns.end(); ++it)
	{
	  oss << (*it);
	}
      return o << oss.str();    
    }


    ostream& operator<<(ostream& o, const BeepOptionMap *bom)
    {
      return operator<<(o, *bom);
    }


    StringAltOption::StringAltOption(string id_, string defaultVal, 
				     string validVals_, string helpMsg_, 
				     StringCase valCase_, bool ignoreCase_) 
      : BeepOption(id_, helpMsg_, ""), 
	val(defaultVal),
	validVals(), 
	valCase(valCase_),
	ignoreCase(ignoreCase_)
    {
      if (valCase == UPPER)
	{ 
	  transform(val.begin(), val.end(), val.begin(), (int(*)(int))toupper);
	}
      else if (valCase == LOWER)
	{ 
	  transform(val.begin(), val.end(), val.begin(), (int(*)(int))tolower);
	}
	
      // Store alternatives from comma separated list in vector.
      string token;
      istringstream iss(validVals_);
      while (getline(iss, token, ','))
	{
	  validVals.insert(token);
	}
	
      // Create default error message.
      parseErrMsg = "Expected ";
      set<string>::iterator it;
      for (it = validVals.begin(); it != validVals.end(); ++it)
	{
	  parseErrMsg += "'" + (*it) + "'/";
	}
      parseErrMsg.erase(parseErrMsg.length()-1, parseErrMsg.length());
      parseErrMsg += " after option -" + id + '.';
	
      // Make sure default value is valid.
      string valTmp = string(val);
      if (ignoreCase) { transform(valTmp.begin(), valTmp.end(), 
				  valTmp.begin(), (int(*)(int))toupper); }
      bool found = false;
      for (it = validVals.begin(); it != validVals.end(); ++it)
	{
	  string alt = string(*it);
	  if (ignoreCase) 
	    {
	      transform(alt.begin(), alt.end(), alt.begin(), 
			(int(*)(int))toupper); 
	    }
	  if (valTmp == alt)
	    {
	      found = true;
	      break;
	    }
	}
      if (!found) 
	{
	  throw AnError("Invalid default value in StringAltOption."); 
	}
    };

  } // end namespace option.
} // end namespace beep.

