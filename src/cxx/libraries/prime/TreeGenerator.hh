#ifndef TREEGENERATOR_HH
#define TREEGENERATOR_HH

#include "GammaMap.hh"

#include <vector>

namespace beep
{
  //--------------------------------------------------------------------
  //
  //! base class with interface for BDTreeGenerators
  // This is also the former name of RandomTreeGenerator
  //
  //--------------------------------------------------------------------
  class TreeGenerator
  {
  public:
    //--------------------------------------------------------------------
    //
    // Construct/ destruct/assign
    //
    //--------------------------------------------------------------------
    TreeGenerator(){};
    virtual ~TreeGenerator(){};
    
    //--------------------------------------------------------------------
    //
    // Attributes
    //
    //--------------------------------------------------------------------
    virtual void setTopTime(Real time) = 0;
    virtual Real getTopTime() = 0;
    virtual bool generateTree(Tree& G, bool noTopTime) = 0;
    virtual StrStrMap exportGS() = 0;
    virtual GammaMap exportGamma() = 0;
  };
}
#endif
