#ifndef HYBRIDTREEINPUTOUTPUT_HH
#define HYBRIDTREEINPUTOUTPUT_HH

#include "TreeInputOutput.hh"

namespace beep
{
  // Forward declarations
  class GammaMap;
  class HybridTree;
  class Node;
  class SetOfNodes;
  class StrStrMap;
  class Tree;
  
  //--------------------------------------------------------------------
  //
  // HybridTreeInputOutput
  //
  //! extends TreeInputOutput to HybridTrees
  //
  //--------------------------------------------------------------------
  class HybridTreeInputOutput : public TreeInputOutput
  {
  public:
    //--------------------------------------------------------------------
    //
    // Constructors
    // 
    //--------------------------------------------------------------------




  public:
    HybridTreeInputOutput();	  //! "Empty" constructor, allows reading from STDIN.
    HybridTreeInputOutput(const TreeInputOutput& io); //! Convert from a TreeInputOutput
    HybridTreeInputOutput(const HybridTreeInputOutput& io);
    HybridTreeInputOutput& operator=(const HybridTreeInputOutput& io);
    virtual ~HybridTreeInputOutput();


    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
  public:

    /// \name Named constructors! 
    //! Usage: 
    //--------------------------------------------------------------------
    //@{
//     static HybridTreeInputOutput fromFile(const std::string &filename);
//     static HybridTreeInputOutput fromString(const std::string &treeString);
    //@}

    //----------------------------------------------------------------------
    //! \name Reading trees
    //----------------------------------------------------------------------
    //@{
    HybridTree readHybridTree(); 



    
    HybridTree readHybridTree(TreeIOTraits& traits,
			      std::vector<SetOfNodes> *AC, StrStrMap *gs);

    // Convenience front to readAllBeepTrees(...)
    // Reads 'NW tags' as edge times and nothing more
    //----------------------------------------------------------------------
    std::vector<HybridTree> readAllHybridTrees(std::vector<StrStrMap>* gs = 0);

    //! Basic function for reading multiple trees in PRIME format
    //! ID and name of nodes are always read, optionally edge times
    //! (useET=true), node times (useNT=true), edge lengths (useBL=true)
    //! antichains (AC!=NULL) and gene species maps (gs!=NULL) may be
    //! read. NWIsET detemines whether the 'Newick Weight' tag, i.e., what
    //! is given after ':' in the newick tree, should be interpreted as edge
    //! times or edge lengths.
    //! precondition: (useET && useNT) != true
    //----------------------------------------------------------------------







    std::vector<HybridTree> readAllHybridTrees( TreeIOTraits& traits,
					       std::vector<SetOfNodes> *AC,
					       std::vector<StrStrMap>* gs);
   
    //@}

    //----------------------------------------------------------------------
    //! \name Writing trees
    //----------------------------------------------------------------------
    //@{
    static std::string writeHybridTree(const HybridTree& T, 
				       TreeIOTraits traits,
				       const GammaMap* gamma);

    //! convenience front function for writeHybridTree(...) 
    //! writes tree S with edge times
    //----------------------------------------------------------------------
    static std::string writeHybridTree(const HybridTree& S);

  };
}//end namespace beep
#endif
