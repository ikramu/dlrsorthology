#include "GammaDensity.hh"

#include <assert.h>
#include "AnError.hh"
#include "DiscreteGamma.hh"
#include "Probability.hh"

#include <cmath>
#include <sstream>

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // class GammaDensity implements the Gamma density distribution
  //
  // Author: Martin Linder, adapted by Bengt Sennblad
  // copyright the MCMC-club, SBC
  //
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------
  GammaDensity::GammaDensity(Real mean, Real variance, bool embedded)
    : Density2P_positive(mean, variance, "Gamma"),
      c()
  {
    if(embedded)
      {
	setEmbeddedParameters(mean, variance);
      }
    else
      {
	setParameters(mean, variance);
      }
  }

  GammaDensity::GammaDensity(const GammaDensity& df)
    : Density2P_positive(df),
      c(df.c)
  {}

  GammaDensity::~GammaDensity() 
  {}

  GammaDensity& 
  GammaDensity::operator=(const GammaDensity& df)
  {
    if(&df != this)
      {
	Density2P_positive::operator=(df);
	c = df.c;
      }
    return *this;
  }


  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // Density-, distribution-, and sampling functions
  //----------------------------------------------------------------------
  Probability 
  GammaDensity::operator()(const Real& x) const
  {
    return pdf(x);
  }

  Probability 
  GammaDensity::pdf(const Real& x) const
  {
    if( x <= 0)
      {
	return 0.0;
      }
    else
      {
	return Probability::setLogProb(c + (alpha - 1.0) * 
				       std::log(x) - x * beta, 1);
      }
  }

  Probability 
  GammaDensity::cdf(const Real& x) const
  {
    Probability ret = 0;
    if(x > 0)
      {
    	Real lg = std::log(gamma_in(x * beta, alpha));
    	if (isinf(lg))
    	{
    		if (lg < 0) { return Probability(0.0); }
    		return Probability(1.0);
    	}
        ret =  Probability::setLogProb(lg, 1);
      }
    if (ret.val() > 1.0) {
        return Probability(1.0);
    }
    return ret;
  }

   

  Real
  GammaDensity::sampleValue(const Real& p) const
  {
    assert(0 < p && p < 1.0); // Check precondition
    return ppGamma(p, alpha, beta); 
  }
  
  // Access parameters
  //----------------------------------------------------------------------
  Real
  GammaDensity::getMean() const
  {
    return alpha / beta;
  }
    
  Real 
  GammaDensity::getVariance() const
  {
    return alpha / std::pow(beta, 2);
  }


  // Set Parameters
  //----------------------------------------------------------------------
  void
  GammaDensity::setParameters(const Real& mean, const Real& variance)
  {
      //bool meanInRange = isInRange(mean);
      //bool varInRange = isInRange(variance);
    assert(isInRange(mean) && isInRange(variance)); // check precondition

    beta = mean / variance;
    alpha = mean * beta;
    c = alpha * std::log(beta) - lgamma(alpha);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void 
  GammaDensity::setMean(const Real& mean)
  {
#ifndef NDEBUG
    Real variance = getVariance();
#endif
    assert(isInRange(mean)); // cheak precondition

    beta = mean * std::pow(beta, 2) /alpha;
    alpha = mean * beta;
    c = alpha * std::log(beta) - lgamma(alpha);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void
  GammaDensity::setVariance(const Real& variance)
  {
    assert(isInRange(variance)); // cheak precondition

    Real mean = getMean();
    beta = mean / variance;
    alpha = mean * beta;
    c = alpha * std::log(beta) - lgamma(alpha);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------

  std::string 
  GammaDensity::print() const
  {
    std::ostringstream oss;
    oss << "Gamma distribution, G("
	<< alpha
	<< ", "
	<< beta
	<< ")\n" 
      ;
    return oss.str();
  }
    
}//end namespace beep
