#ifndef _ABSTRACTMCMC_HH
#define	_ABSTRACTMCMC_HH

#include "AnError.hh"
#include "Probability.hh"
#include "IterationObserver.hh"

namespace beep {
  class AbstractMCMC {
  public:
    AbstractMCMC(){};
virtual    ~AbstractMCMC(){};
    virtual bool iterate( IterationObserver & iterObs, unsigned int * iterations_done ) = 0; 
  };
}

#endif	/* _ABSTRACTMCMC_HH */


