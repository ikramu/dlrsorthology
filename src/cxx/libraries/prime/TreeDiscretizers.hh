#ifndef TREEDISCRETIZERS_HH
#define TREEDISCRETIZERS_HH

#include <string>
#include <utility>
#include <vector>

#include "BeepVector.hh"
#include "Node.hh"
#include "Tree.hh"

namespace beep
{

  /**
   * Abstract base class for discretization of a tree by means of
   * augmenting it with points. The purpose of this class
   * (or rather subclasses) is to define a discretization approach,
   * not to actually hold discretization points, etc. Instead, a
   * discretization map of the tree should be obtainable through
   *  factory methods.
   *
   * How each edge is split may vary between implemented
   * discretizers. In particular note that:
   * 
   *  1) There may be overlapping points when a discretizer includes 
   *     endpoints for two adjacent segments (or there may not be...)
   *  2) The value of points corresponding directly to original
   *     tree nodes may or may not be exactly identical to the
   *     values of the nodes (it depends on the method used).
   *     This implies that one should never use discretization
   *     values and edge/node values interchangeably.
   * 
   * @author Joel Sj�strand.
   */


  /** Output operator for 'Point' typedef in EdgeDiscretizer
   *  need sto be placed outside class t work
   */
  std::ostream& operator<<(std::ostream& o, 
			   const  std::pair<const Node*, unsigned>&p);

  class TreeDiscretizer
  {
  public:
	
    /**
     * Destructor.
     */
    virtual ~TreeDiscretizer() {};
	
    /**
     * Returns a string representation.
     * @return a string representation.
     */
    virtual std::string print() const = 0;
  };


  /**
   * Abstract base class for discretizations where a tree is
   * sliced on a per-edge basis. Subclasses implement factory
   * methods which fill a BeepVector of points according to a
   * tree S.
   *
   * Subclasses MUST adhere to the following:
   * 1) For each edge, there is a point corresponding to the
   *    lower node of the edge (at internal index 0), but not
   *    corresponding to the upper node.
   * 2) There is an additional point added to the tip of the
   *    "top time edge". This point is often used similarly to
   *    a node.
   * 3) Each edge is subdivided into equidistant intervals.
   *    Points are then assigned to the ends and to interval
   *    midpoints. This implies that point spacing is NOT
   *    equidistant:
   *    pt0  <-dt/2->  pt1  <-dt->  pt2  <-dt-> ... ptk <-dt/2->
   *    (where for the top edge, an additional point, pt(k+1),
   *    would follow.)
   * 4) Each edge must be subdivided into at least two intervals.
   *    This implies that it has least three points, or four for
   *    the top time edge.
   * 
   * @author Joel Sj�strand.
   */
  class EdgeDiscretizer : public TreeDiscretizer
  {
  public:
	
    /** Point representation. */
    typedef std::pair<const Node*, unsigned> Point;

    /**
     * Destructor.
     */
    virtual ~EdgeDiscretizer() {};
		
    /**
     * Returns a string representation.
     * @return a string representation.
     */
    virtual std::string print() const = 0;
	
  public:
	
    /**
     * Subclasses implement this to create a discretization.
     * @param S the tree to discretize.
     * @param pts the points to fill with the discretization.
     */
    virtual void discretize(Tree& S, BeepVector< std::vector<Real> >& pts) const = 0;

    /**
     * Subclasses implement this to create a discretization of
     * a single edge.
     * @param n lower node of the edge to discretize.
     * @param pts the points to fill with the discretization.
     */
    virtual void discretizeEdge(const Node* n, std::vector<Real>& pts) const = 0;
  };



  /**
   * Discretizes a tree by splitting each edge in C intervals,
   * where C is invariant (with one exception: one may define
   * a separate value for the top time edge if desired). E.g.
   * with C=4, every edge will be split into 4 equidistant
   * intervals. Point spacing is a different matter though; 
   * see superclass EdgeDiscretizer.
   *
   * @author Joel Sj�strand.
   */
  class EquiSplitEdgeDiscretizer : public EdgeDiscretizer
  {
  public:
	
    /**
     * Contructor.
     * @param noOfIvs number of intervals to slice each edge into, minimum 2.
     * @param noOfTopEdgeIvs optional, number of intervals to slice top time edge into.
     *     If not specified, will equal the previous parameter. An empty top time edge
     *     will consist only of a point corresponding to the root.
     */
    EquiSplitEdgeDiscretizer(unsigned noOfIvs, unsigned noOfTopEdgeIvs = 0);
	
    /**
     * Returns the number of slices of edges.
     * @param noOfIvs number of intervals on each edge.
     * @param noOfTopEdgeIvs number of intervals on the top time edge. 
     */
    inline void getNumberOfIvs(unsigned& noOfIvs, unsigned& noOfTopEdgeIvs)
    {
      noOfIvs = m_noOfIvs;
      noOfTopEdgeIvs = m_noOfTopEdgeIvs;
    }
	
    /**
     * Returns a string representation.
     * @return a string representation.
     */
    virtual std::string print() const
    { return "EquiSplitEdgeDiscretizer"; }

    /**
     * Creates a discretization.
     * @param S the tree to discretize.
     * @param pts the points to fill with the discretization.
     */
    virtual void discretize(Tree& S, BeepVector< std::vector<Real> >& pts) const;
	
    /**
     * Creates a discretization for a single edge.
     * @param n lower node of the edge to discretize.
     * @param pts the points to fill with the discretization.
     */
    virtual void discretizeEdge(const Node* n, std::vector<Real>& pts) const;

  private:
	
    /** The number of intervals edges are split into. */
    unsigned m_noOfIvs;
	
    /** The number of intervals the top time edge is split into. */
    unsigned m_noOfTopEdgeIvs;
  };



  /**
   * Discretizes a tree by splitting each edge using a (never exceeded) approximate
   * segment length (or "step size"). For instance if the desired step size is set to 0.11,
   * an edge of length 1.0 will be discretized into 10 segments of length 0.1.
   * One may define a separate value for the top time edge if desired.
   * Note however, that particularly for short edges, the discrepancy between
   * desired and actual step size may be significant.
   * 
   * Once the the interval length has been determined for an edge, points are added in
   * a slightly non-intuitive manner, see EdgeDiscretizer.
   *
   * One may specify the discretization of the top time edge separately, which may
   * be useful if it needs to be long but is not required to be of high resolution.
   *
   * @author Joel Sj�strand.
   */
  class StepSizeEdgeDiscretizer : public EdgeDiscretizer
  {
  public:
	
    /**
     * Contructor.
     * @param targetStepSz the desired (and never exceeded) step size.
     * @param minNoOfIvs the minimum number of intervals to slice
     *        an edge into, minimum 2. Defaults to 2.
     * @param noOfTopEdgeIvs optional, number of intervals to slice top time edge into.
     *     If not specified or 0, will be set in same fashion as other edges. An empty top
     *     time edge will consist only of a point corresponding to the root.
     */
    StepSizeEdgeDiscretizer(Real targetStepSz, unsigned minNoOfIvs = 2, unsigned noOfTopEdgeIvs = 0);
	
    /**
     * Returns the desired step size.
     * @return the step size. 
     */
    inline Real getTargetStepSz()
    {
      return m_targetStepSz;
    }
	
    /**
     * Returns a string representation.
     * @return a string representation.
     */
    virtual std::string print() const
    { return "StepSizeEdgeDiscretizer"; }
	
    /**
     * Creates the discretization.
     * @param S the tree to discretize.
     * @param pts the points to fill with the discretization.
     */
    virtual void discretize(Tree& S, BeepVector< std::vector<Real> >& pts) const;
	
    /**
     * Creates a discretization for a single edge.
     * @param n lower node of the edge to discretize.
     * @param pts the points to fill with the discretization.
     */
    virtual void discretizeEdge(const Node* n, std::vector<Real>& pts) const;

  private:
	
    /** The number of intervals edges are split into. */
    Real m_targetStepSz;
	
    /** The minimum number of intervals per edge. */
    unsigned m_minNoOfIvs;
	
    /** The number of intervals the top time edge is split into. */
    unsigned m_noOfTopEdgeIvs;
  };

}

#endif /*TREEDISCRETIZERS_HH*/
