/*
 * File:   TimeEstimatorPlusPlus.cc
 * Author: fmattias
 *
 * Created on January 15, 2010, 3:20 PM
 */

#include <time.h>
#include <sstream>
#include <string>

#include "TimeEstimator.hh"

using namespace std;

TimeEstimator::TimeEstimator(int totalNumberOfIterations) :
                                                        m_output(&std::cout)
{
    start();
    m_numberOfIterationsLeft = totalNumberOfIterations;
}

TimeEstimator::TimeEstimator(const TimeEstimator& other) :
                                             m_numberOfIterationsLeft(other.m_numberOfIterationsLeft),
                                             m_performedIterations(other.m_performedIterations),
                                             m_totalTime(other.m_totalTime),
                                             m_lastUpdate(other.m_lastUpdate),
                                             m_output(other.m_output)

{
}

TimeEstimator::~TimeEstimator()
{
}

void
TimeEstimator::start()
{
    m_totalTime = 0.0;
    m_performedIterations = 0;
    m_lastUpdate = clock();
}

void
TimeEstimator::reset(int totalNumberOfIterations)
{
    start();
    m_numberOfIterationsLeft = totalNumberOfIterations;
}

void
TimeEstimator::update(int numberOfIterations)
{
    m_performedIterations += numberOfIterations;
    m_numberOfIterationsLeft -= numberOfIterations;
    clock_t timeDiff = clock() - m_lastUpdate;

    /* Ignore time update if timer overflows */
    if(timeDiff > 0) {
        m_totalTime += timeDiff / (double) CLOCKS_PER_SEC;
    }
    m_lastUpdate = clock();
}

double
TimeEstimator::getEstimatedTimeLeft()
{
    double averageTimePerIteration = m_totalTime / m_performedIterations;
    return averageTimePerIteration * m_numberOfIterationsLeft;
}

void
TimeEstimator::printEstimatedTimeLeft()
{
    *m_output << getPrintableEstimatedTimeLeft() << endl;
}

string
TimeEstimator::getPrintableEstimatedTimeLeft()
{
    double timeLeft = getEstimatedTimeLeft();
    int hours = timeLeft / 3600.0;
    int minutes = timeLeft / 60.0 - hours * 60.0;
    int seconds = timeLeft - minutes * 60.0 - hours * 3600.0;
    stringstream ss;
    ss << "Estimated time left: " << hours << " hours " << minutes <<
            " minutes " << seconds << " seconds.";
    return ss.str();
}

void
TimeEstimator::setOutputStream(ostream &output)
{
    m_output = &output;
}

TimeEstimator *
TimeEstimator::instance(int totalNumberOfIterations)
{
    // Singleton instance
    static TimeEstimator *singletonInstance = 0;

    if(singletonInstance == 0) {
        singletonInstance = new TimeEstimator(totalNumberOfIterations);
    }

    return singletonInstance;

}