#ifndef DENSITY2P_POSITIVE_HH
#define DENSITY2P_POSITIVE_HH

#include "Density2P_common.hh"

#include <string>



namespace beep
{
  //----------------------------------------------------------------------
  //
  // Class Density2P_positive
  //! Implemented base class (body) for probabilistic density functions 
  //! that takes 2 parameters and has Pr(x) = 0 for all x < 0.
  //!
  //! Invariants: variance > 0; 
  //!             mean     > 0
  //!
  //! Author: Martin Linder, Bengt Sennblad, copyright: the MCMC-club, SBC.
  //! All rights reserved.
  //
  //----------------------------------------------------------------------
  class Density2P_positive : public Density2P_common
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //----------------------------------------------------------------------
    Density2P_positive(Real mean, Real variance, const std::string& density);
    Density2P_positive(const Density2P_positive& df);
    virtual ~Density2P_positive();

    Density2P_positive& operator=(const Density2P_positive& df);

  public:
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    // Set Parameters
    //----------------------------------------------------------------------

    //! Sets embedded parameters (alpha and beta) of density 
    //! (see doc for subclass for exact meaning of these).
    //! Defaults to alpha = mean and beta = variance.
    //! Postcondition: alpha and beta is within precision limits.
    virtual void setEmbeddedParameters(const Real& alpha, const Real& beta);

    // test and infotmation on valid ranges
    //------------------------------------------------------------------

//     //! Check if value is valid (has a non-zero probability).
//     virtual bool isInRange(const Real& rate) const;

//     //! Returns range in which density has a non-zero probability.
//     //! Defaults to (0, precision_max)
//     virtual void getRange(Real& min, Real& max) const;

    void setRange(const Real& min, const Real& max);

//     //! Get range in which mean is valid.
//     //! Defaults to (0, precision_max)
//     virtual void getMeanRange(Real& min, Real& max) const;

//     //! Get range in which variance is valid.
//     //! Defaults to (0, precision_max)
//     virtual void getVarianceRange(Real& min, Real& max) const;
  };

}//end namespace beep
#endif
