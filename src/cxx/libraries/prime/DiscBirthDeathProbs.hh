#ifndef DISCBIRTHDEATHPROBS_HH
#define DISCBIRTHDEATHPROBS_HH

#include <vector>

#include "Beep.hh"
#include "DiscTree.hh"
#include "PerturbationObservable.hh"

namespace beep
{

// Forward declarations.
class BirthDeathProbs;

/**
 * Handles probabilities concerning a birth-death process for a DiscTree.
 * More specifically, it contains precalculated probabilities for constant
 * lineage and deaths over edges in the same vein as BirthDeathProbs, although
 * on a point-to-node basis rather than node-to-node. See also the documentation
 * for BirthDeathProbs. Although nodes refer to the original tree, all
 * computations are carried out with regard to their discretized time values.
 */
class DiscBirthDeathProbs : public PerturbationObservable
{

	friend class DiscBirthDeathMCMC;
	
public:
	
	/**
	 * Constructor.
	 * @param DS the discretized tree.
	 * @param birthRate the birth rate of the process.
	 * @param deathRate the death rate of the process.
	 */
	DiscBirthDeathProbs(const DiscTree& DS, Real birthRate, Real deathRate);
	
	/**
	 * Copy constructor. Deep-copies important elements, leaves out
	 * members only used for optimization.
	 * @param dbdp the object to be copied.
	 */
	DiscBirthDeathProbs(const DiscBirthDeathProbs& dbdp);
	
	/**
	 * Destructor.
	 */
	virtual ~DiscBirthDeathProbs();
    
	/**
	 * Updates the precalculated internal values.
	 */
	void update();
	
	/**
	 * Returns the birth rate.
	 * @return the birth rate.
	 */
	Real getBirthRate() const;
	
	/**
	 * Returns the death rate.
	 * @return the death rate.
	 */
	Real getDeathRate() const;
	
	/**
	 * Returns the birth and death rates.
	 * @param birthRate the birth rate.
	 * @param deathRate the death rate.
	 */
	void getRates(Real& birthRate, Real& deathRate) const;
	
	/**
	 * Sets new birth and death rates.
	 * @param newBirthRate the new birth rate.
	 * @param newDeathRate the new death rate.
	 * @param doUpdate calls update() after setting new rates.
	 */
	void setRates(Real newBirthRate, Real newDeathRate, bool doUpdate=true);
	
	/**
	 * Returns the underlying (non-discretized) tree.
	 * @return the tree.
	 */
	const Tree& getTree() const;
	
	/**
	 * Returns the tree name.
	 * @return the name.
	 */
	std::string getTreeName() const;
	
	/**
	 * Returns the root-to-leaf time in the discretized tree.
	 * @return the time from root to leaves.
	 */
	Real getRootToLeafTime() const;
	
	/**
	 * Returns the time of the edge above the root in the discretized tree.
	 * @return the top time.
	 */
	Real getTopTime() const;
	
	/**
	 * Returns the root-to-leaf time plus the top time in the discretized tree.
	 * @return the top time time.
	 */
	Real getTopToLeafTime() const;
	
	/**
	 * For node Y, returns probability of a constant lineage ("P11") over <parentNode(Y),Y>.
	 * @param Y the node.
	 * @return the P11 value.
	 */
	Probability getConstLinValForEdge(const Node* Y) const;
	
	/**
	 * For a point y, returns the probability of a constant lineage ("P11") over
	 * <parentPoint(y),y>. Note: undefined behaviour if y is invalid.
	 * @param y the point.
	 * @return the P11 value.
	 */
	Probability getConstLinValForSeg(DiscTree::Point y) const;
    
	/**
	 * For a node Y, returns the probability of constant lineage ("P11")
	 * over <parentPoint(Y),Y>.
	 * @param Y the point.
	 * @return the P11 value.
	 */
	Probability getConstLinValForSeg(const Node* Y) const;
	
	/**
	 * For a point y with point ancestor x, returns the probabilities of
	 * constant lineage ("P11") over the path from y up to x, the path from parentPt(y)
	 * up to x, and so forth until the last element corresponds to the single
	 * discretization segment beneath x. Note: undefined behaviour if x or y are
	 * invalid points.
	 * @param lins the P11 values corresponding to lineages of descending lengths.
	 * @param x the ancestral point.
	 * @param y the descendant point.
	 * @param singleLin if true, creates only the longest lineage.
	 * @return the highest node on the path *strictly* beneath point x.
	 */
	const Node* getConstLinValsForPath(std::vector<Probability>& lins,
			DiscTree::Point x, DiscTree::Point y, bool singleLin) const;
	
	/**
	 * For a node Y, returns the loss value for extinction
	 * over (parentNode(Y),Y) or below.
	 * @param Y the node.
	 * @return the loss value.
	 */
	Probability getLossVal(const Node* Y) const;
	
	/**
	 * Prints info for debugging purposes to cerr.
	 * @param printNodeInfo if true, lists node related info.
	 */
	void debugInfo(bool printNodeInfo=true) const;
	
private:
	
	/**
	 * Recursive helper. Calculates BD_const for point segments for the
	 * edge above a specified node, having first recursed over its children.
	 * Old values are removed.
	 *   BD_const ~ sum_(i=1:inf){ P(t)*(1-u_t)*u_t^(i-1)*D^(i-1)*i }
	 * 	 BD_zero ~ {1-P(t)} + sum_(i=1:inf){ P(t)*(1-u_t)*u_t^(i-1)*D^i }
	 * The expressions simplify to the ones used in the class.
	 * @param Y the lower node of the edge.
	 */
	void calcBDProbs(const Node* Y);
	
	/**
	 * Helper. Copies elements from the base leaf probability vector to another.
	 * Grows the base vector if too short before copying.
	 * @param leafProbs the vector to be copied to.
	 * @param noOfPts the number of points to copy.
	 */
	void copyLeafBProbs(std::vector<Probability>& leafProbs, unsigned noOfPts);
	
	/**
	 * Helper. Calculates P(t) and u_t for a given time. 
	 */
	void calcPtAndUt(Real t, Probability& Pt, Probability& ut) const;
	
public:
	
private:
	
	/** The discretized tree. */
	const DiscTree& m_DS;
	
	/** Birth rate. */
	Real m_birthRate;
	
	/** Death rate. */
	Real m_deathRate;
	
	/**
	 * Corresponds to BD_const of BirthDeathProbs, but for parts of edges instead of
	 * whole edges. Vector element i refers to segment <x,Y>, where
	 * x is the i-th point of the edge and Y is the lower node of the edge.
	 * By definition, the element at index 0 (corresponding to <Y,Y>) has
	 * probability 1, and an additional element at the end has probability corresponding
	 * to the entire edge (i.e. BD_const[Y] of BirthDeathProbs, albeit discretized).
	 *
	 * Roughly states "the probability of a single lineage starting at x having only one
	 * lineage at Y reaching the leaves (while the remaining ones go extinct)".
	 */
	BeepVector< std::vector<Probability>* > m_BD_const;
	
	/**
	 * Corresponds to BD_zero of BirthDeathProbs, but for discretized nodes, i.e.
	 * the probability of extinction over edge <parent(Y),Y> or below.
	 */
	BeepVector<Probability> m_BD_zero;
	
	/**
	 * Precomputed P(t) for the discretization time step in DS. Equals
	 * Pr[not going extinct over time t within edge], i.e. 1-P_0(t) in
	 * Kendall's notation.
	 */
	Probability m_Pt;
	
	/**
	 * Precomputed u_t for the discretization time step in DS.
	 */
	Probability m_ut;
	
	/**
	 * Contains precalculated values for a lineage ending at a leaf. Used to speed up
	 * calculations of leaf edges in m_BD_const. Grows dynamically if too small.
	 */
	std::vector<Probability> m_base_BD_const;
	
	/**
	 * Death probability for last point in m_base_BD_const.
	 * Updated as m_base_BD_const grows.
	 */
	Probability m_base_BD_zero;
};

} // End namespace beep.

#endif /* DISCBIRTHDEATHPROBS_HH */

