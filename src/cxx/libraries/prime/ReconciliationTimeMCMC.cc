#include "ReconciliationTimeMCMC.hh"

#include "AnError.hh"
#include "MCMCObject.hh"
#include "ReconciliationTimeSampler.hh"

#include <algorithm>
#include <sstream>

namespace beep
{

  using namespace std;

  //-------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //-------------------------------------------------------------

  // number of parameters equal number of internal nodes
  //-------------------------------------------------------------
  ReconciliationTimeMCMC::ReconciliationTimeMCMC(MCMCModel& prior, 
						 Tree &G_in, 
						 BirthDeathProbs& bdp_in, 
						 GammaMap& gamma_in,
						 Real suggestRatio)
    : StdMCMCModel(prior, G_in.getNumberOfLeaves() - 2, 
		   "EdgeTimes", suggestRatio),
      ReconciliationTimeModel(G_in, bdp_in, gamma_in, false),
      Idx(),
      estimateTimes(true),
      suggestion_variance(1.0 * G->rootToLeafTime() / 
			  G->getRootNode()->getMaxPathToLeaf())
  {
  }
  ReconciliationTimeMCMC::ReconciliationTimeMCMC(MCMCModel& prior, 
						 Tree &G_in, 
						 BirthDeathProbs& bdp_in, 
						 GammaMap& gamma_in,
						 bool include_root_time,
						 Real suggestRatio)
    : StdMCMCModel(prior, G_in.getNumberOfLeaves() - 2, 
		   "EdgeTimes", suggestRatio),
      ReconciliationTimeModel(G_in, bdp_in, gamma_in, include_root_time),
      Idx(),
      estimateTimes(true),
      suggestion_variance(1.0 * G->rootToLeafTime() / 
			  G->getRootNode()->getMaxPathToLeaf())
  {
  }

  // Construct using an existing ReconciliationModel (subclass)
  ReconciliationTimeMCMC::ReconciliationTimeMCMC(MCMCModel& prior, 
						 ReconciliationModel& rs,
						 Real suggestRatio)
    : StdMCMCModel(prior, rs.getGTree().getNumberOfLeaves() - 2, 
		   "EdgeTimes", suggestRatio),
      ReconciliationTimeModel(rs, false),
      Idx(),
      estimateTimes(true),
      suggestion_variance(1.0 * G->rootToLeafTime() / 
			  G->getRootNode()->getMaxPathToLeaf())
  {
  }

  // Construct using an existing ReconciliationModel (subclass)
  ReconciliationTimeMCMC::ReconciliationTimeMCMC(MCMCModel& prior, 
						 ReconciliationModel& rs,
						 bool include_root_time,
						 Real suggestRatio)
    : StdMCMCModel(prior, rs.getGTree().getNumberOfLeaves() - 2, 
		   "EdgeTimes", suggestRatio),
      ReconciliationTimeModel(rs, include_root_time),
      Idx(),
      estimateTimes(true),
      suggestion_variance(1.0 * G->rootToLeafTime() / 
			  G->getRootNode()->getMaxPathToLeaf())
  {
  }

  ReconciliationTimeMCMC::ReconciliationTimeMCMC(MCMCModel& prior, 
						 Tree &G_in, 
						 BirthDeathProbs& bdp_in, 
						 GammaMap& gamma_in,
						 const string& name_in,
						 Real suggestRatio)
    : StdMCMCModel(prior, G_in.getNumberOfLeaves() - 2, 
		   "EdgeTimes", suggestRatio),
      ReconciliationTimeModel(G_in, bdp_in, gamma_in, false),
      Idx(),
      estimateTimes(true),
      suggestion_variance(G->rootToLeafTime()?0.1 * G->rootToLeafTime() / 
			  G->getRootNode()->getMaxPathToLeaf():0.1)
  {
    name = name_in;
  }

  // Construct using an existing ReconciliationModel (subclass)
  ReconciliationTimeMCMC::ReconciliationTimeMCMC(MCMCModel& prior, 
						 ReconciliationModel& rs,
						 const string& name_in,
						 Real suggestRatio)
    : StdMCMCModel(prior, rs.getGTree().getNumberOfLeaves() - 2, 
		   "EdgeTimes", suggestRatio),
      ReconciliationTimeModel(rs, false),
      Idx(),
      estimateTimes(true),
      suggestion_variance(1.0 * G->rootToLeafTime() / 
			  G->getRootNode()->getMaxPathToLeaf())
  {
    name = name_in;
  }

  ReconciliationTimeMCMC::ReconciliationTimeMCMC(const ReconciliationTimeMCMC& rts)
    : StdMCMCModel(rts),
      ReconciliationTimeModel(rts),
      Idx(rts.Idx),
      estimateTimes(rts.estimateTimes),
      suggestion_variance(rts.suggestion_variance)
  {
  }

  ReconciliationTimeMCMC::~ReconciliationTimeMCMC()
  {
  }

  ReconciliationTimeMCMC&
  ReconciliationTimeMCMC::operator=(const ReconciliationTimeMCMC& rts)
  {
    if(&rts != this)
      {
	StdMCMCModel::operator=(rts);
	ReconciliationTimeModel::operator=(rts);
	Idx = rts.Idx;
	estimateTimes = rts.estimateTimes;
	suggestion_variance = rts.suggestion_variance;
      }
    return *this;
  }

  //-------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------
  const GammaMap&
  ReconciliationTimeMCMC::getGamma()
  {
    return *gamma;
  }

  void 
  ReconciliationTimeMCMC::fixTimes()
  {
    cerr << "fixing times\n";
    estimateTimes = false;
    n_params = 0;
    updateParamIdx();
  }

  MCMCObject
  ReconciliationTimeMCMC::suggestOwnState()
  {
    Node* gn;
    do
      {
	//TODO: change Idx to idx_node! see EdgeRateMCMC /bens
	Idx = R.genrand_modulo(G->getNumberOfNodes());
	gn = G->getNode(Idx);
      }
    while(gn->isLeaf() || gamma->isSpeciation(*gn) || (includeRootTime == false && gn->isRoot()) ); 
    // times for leaves and speciations are fixed

#ifdef PERTURBED_NODE
    if(!G->perturbedNode()) 
      {
	G->perturbedNode(gn); // Shortcut for SubstLike
      }
    else
      {
	G->perturbedNode(G->getRootNode()); // recalculate SubstLike!
      }
#endif
    return perturbTime(*gn);
  }

  MCMCObject
  ReconciliationTimeMCMC::suggestOwnState(unsigned x)
  {
    Node* gn;
    do
      {
	unsigned perturbTime = x + G->getNumberOfLeaves() - 1;
	gn = G->getNode(perturbTime);
      }
    while(gn->isLeaf() || gamma->isSpeciation(*gn) || gn->isRoot()); 
    // times for leaves and speciations are fixed

#ifdef PERTURBED_NODE
    if(!G->perturbedNode()) 
      {
	G->perturbedNode(gn); // Shortcut for SubstLike
      }
    else
      {
	G->perturbedNode(G->getRootNode()); // recalculate SubstLike!
      }
#endif
    return perturbTime(*gn);
  }

  void 
  ReconciliationTimeMCMC::commitOwnState()
  {
  }

  void
  ReconciliationTimeMCMC::commitOwnState(unsigned x)
  {
  }

  void
  ReconciliationTimeMCMC::discardOwnState()
  {
    cerr << "discarding\n"; 
    Node* gn = G->getNode(Idx);

    if(gn->isRoot()) // this is still needed, because changeNodeTime cannot
                    // update root's time in relation to an unknown host tree
      {
	Tree& S = bdp->getStree();
	// This could use Tree::setTime
	gn->changeTime(S.rootToLeafTime() + 
		      S.getRootNode()->getTime() - oldValue);
      }
    else if(gn->changeNodeTime(oldValue) == false)
      {
	cout << "changeNodeTime called from ReconciliationTimeMCMC.cc discardOwnState()\n";
	cerr << "changeNodeTime called from ReconciliationTimeMCMC.cc discardOwnState()\n";
	abort(); // was exit(1);
      }
//     gn->changeNodeTime(oldValue);
    like = oldLike;
#ifdef PERTURBED_NODE
    G->perturbedNode(gn); // recalculate SubstLike!
#endif
  }

  void
  ReconciliationTimeMCMC::discardOwnState(unsigned x)
  {
    unsigned perturbTime = x + G->getNumberOfLeaves() - 1;
    Node* gn = G->getNode(perturbTime);

    if(gn->changeNodeTime(oldValue) == false)
      {
	cout << "changeNodeTime called from ReconciliationTimeMCMC.cc discardOwnState()\n";
	cerr << "changeNodeTime called from ReconciliationTimeMCMC.cc discardOwnState()\n";
      }
//     gn->changeNodeTime(oldValue);
    like = oldLike;
#ifdef PERTURBED_NODE
    G->perturbedNode(gn); // recalculate SubstLike!
#endif
  }

  string
  ReconciliationTimeMCMC::ownStrRep() const
  {
    ostringstream oss;
    if(estimateTimes)
      {
	for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	  {
	    Node& n = *G->getNode(i);
	    if(!(n.isLeaf()||n.isRoot()))
	      {
		oss << n.getNodeTime() << ";\t";
	      }
	  }
      }
    return oss.str();
  }

  string
  ReconciliationTimeMCMC::ownHeader() const
  {
    ostringstream oss;
    if(estimateTimes)
      {
	for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	  {
	    Node& n = *G->getNode(i);
	    if(!(n.isLeaf()||n.isRoot()))
	      {
		oss << "nodeTime[" << i << "](float);\t";
	      }
	  }
      }
    return oss.str();
  }

  Probability 
  ReconciliationTimeMCMC::updateDataProbability()
  {
// #ifdef PERTURBED_TREE
//     if(G->perturbedTree())
//       {
// 	GammaMap g(*gamma);
// 	ReconciliationTimeSampler rts(*G, *bdp, g);
// 	rts.sampleTimes(true);
//       }
// #endif
     update();
     like = calculateDataProbability();
    return like;
  }

  //-------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const ReconciliationTimeMCMC& A)
  {
    return o << A.print();
  }

  string 
  ReconciliationTimeMCMC::print() const
  {
    ostringstream oss;
    oss << "Internal node times of the guest tree is ";
    if(estimateTimes)
      {    
	oss <<"perturbed during MCMC.\n";
      }
    else
      {
	oss << "is fixed to start values.\n";
      }
    oss << StdMCMCModel::print();
    return oss.str();
  }
  


  //-------------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------------

  // The function perturbs the node time at 'currentNode' and returns the
  // proposal ratio. The use of changeTime() and changeNodeTime
  // the proposal ratio q_before / q_after is taken from Martin Linder
  //---------------------------------------------------------------------------
  MCMCObject
  ReconciliationTimeMCMC::perturbTime(Node& gn)
  {
    MCMCObject MOb(1.0,1.0);
    assert(!gn.isLeaf());

    oldValue = gn.getNodeTime();
    oldLike = like;

    Tree& S = bdp->getStree();
    Node& gl = *gn.getLeftChild();
    Node& gr = *gn.getRightChild();

    Node* slice = S.getRootNode(); //default if gn is above Sroot

    // We will first determine the allowed limits of the new time:
    //------------------------------------------------------------
    // default maxT: Sroot.time if gn.isRoot and is above Sroot else parentTime
    Real maxT = gn.isRoot()?slice->getTime():gn.getParent()->getNodeTime(); 
    // default minT: nodeTime of closest child
    Real minT = max(gl.getNodeTime(), gr.getNodeTime());
    

    // Find the surrounding antichains and possibly update maxT and minT
    Node* gp = &gn;
    while(gamma->numberOfGammaPaths(*gp) == 0 && !gp->isRoot())
      {
	gp = gp->getParent();
      }
    if(gamma->numberOfGammaPaths(*gp) > 0) // double call but how to avoid it?
      {
	// find closest antichain ABOVE gn
	Node& parentSlice = *gamma->getLowestGammaPath(*gp);
	if(gp == &gn) // there is an antichain on gn that limits maxT
	  {
	    maxT = parentSlice.getNodeTime();	    
	  }

	// find closest antichain BELOW gn
	slice = &gamma->getDominatingChild(parentSlice, gn); 
	// if slice is closer than children update minT
	minT = max(slice->getNodeTime(), minT);      
      }

    updateDataProbability();
    // Now we are ready to suggest a new time
    //-------------------------------------------------------------  
    assert(minT < oldValue);
    assert(oldValue < maxT);
    Real newTime = perturbLogNormal(oldValue, suggestion_variance, 
				    minT, maxT, MOb.propRatio);

    //! \todo{Currently we do not allow gn to be root, if we do, I guess
    //! we should need a similar if clause s below in discrdOwnState() \bens}
    if(gn.isRoot()) // this is still needed, because changeNodeTime cannot
                    // update root's time in relation to an unknown host tree
      {
	// This could use Tree::setTime
	gn.changeTime(S.rootToLeafTime() + 
		      S.getRootNode()->getTime() - newTime);
      }
    else
      {
	if(gn.changeNodeTime(newTime) == false)
	  {
	    cerr << "changeNodeTime called from ReconciliationTimeMCMC.cc perturbLogNormal\n";
	    cout << "changeNodeTime called from ReconciliationTimeMCMC.cc perturbLogNormal\n";
	    abort(); // was exit(1);
	  }
      }
    // get likelihood
    //!\todo  This does not seem to work currently (I thought it did before
    //! though. Check up
//     like *= bdp->partialEdgeTimeProbRatio(*slice, oldValue, newTime);
    cerr << " We're perturbing time at node " << gn.getNumber() << " old like = " <<like; 
    updateDataProbability();
    cerr << "  and new liek = " << like <<     "   oldstateProb = " << old_stateProb << "  new statProb = " << stateProb <<"\n";
    
    MOb.stateProb = like;
    return MOb;
  }

}//end namespace beep
