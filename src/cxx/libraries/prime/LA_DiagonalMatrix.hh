#ifndef LA_DIAGONALMATRIX_H
#define LA_DIAGONALMATRIX_H

#include <iostream>

#include "LA_Matrix.hh" 

namespace beep 
{
  class LA_Vector;

  class LA_DiagonalMatrix
  {
    friend LA_Matrix LA_Matrix::operator*(const LA_DiagonalMatrix& D) const;
    friend void LA_Matrix::eigen(LA_DiagonalMatrix& E, LA_Matrix& V, 
				 LA_Matrix& iV);

    public:
    //------------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //------------------------------------------------------------------------

    LA_DiagonalMatrix(const unsigned& dim, const Real in_data[]);

    //Create a unit matrix
    //------------------------------------------------------------------------
    LA_DiagonalMatrix(const unsigned& dim); 

    // copy constructor
    //------------------------------------------------------------------------
    LA_DiagonalMatrix(const LA_DiagonalMatrix& B);

    // Destructor
    //------------------------------------------------------------------------
    ~LA_DiagonalMatrix();

    // Assignment operator
    //------------------------------------------------------------------------
    LA_DiagonalMatrix& operator=(const LA_DiagonalMatrix& B);


    //------------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------------
  
    // Dimension of matrix
    //------------------------------------------------------------------------
    const unsigned& getDim() const;

    // Access to individual elements
    //------------------------------------------------------------------------
    Real& operator()(const unsigned& row, 
		       const unsigned& column);

    Real operator()(const unsigned& row, 
		      const unsigned& column) const;

    // Transpose of diagonal matrix  -- rather meaningless!
    //------------------------------------------------------------------------
    LA_DiagonalMatrix transpose() const;

    // Inverse of Diagonal Matrix 
    //------------------------------------------------------------------------
    LA_DiagonalMatrix inverse() const;

    // Trace (sum of diagonal elements of Matrix A
    //------------------------------------------------------------------------
    Real trace() const;

    // Multiplication of diagonal matrix A and a scalar 
    //------------------------------------------------------------------------
    LA_DiagonalMatrix operator*(const Real& alpha) const;
    friend LA_DiagonalMatrix operator*(const Real& alpha, 
				       const LA_DiagonalMatrix& D);

    // Multiplication of diagonal matrix A and vector x 
    //------------------------------------------------------------------------
    LA_Vector operator*(const LA_Vector& x) const;
    void mult(const LA_Vector& x, LA_Vector& result) const;

    // Multiplication of diagonal matrix A and diagonal matrix B 
    //------------------------------------------------------------------------
    LA_DiagonalMatrix operator*(const LA_DiagonalMatrix& B) const;

    // Multiplication of diagonal matrix A and dense matrix B 
    //------------------------------------------------------------------------
    LA_Matrix operator*(const LA_Matrix& B) const;
    void mult(const LA_Matrix& B, LA_Matrix& result) const;

    
    //------------------------------------------------------------------------
    // I/O
    //------------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& os, 
				    const beep::LA_DiagonalMatrix& A);

  private:
    //------------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------------
  
    unsigned dim;
    Real* data; //This is an array
  };
 
} /* namespace beep */

#endif




