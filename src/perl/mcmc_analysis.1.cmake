.\" Comment: Man page for mcmc_analysis 
.pc
.TH mcmc_analysis 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
mcmc_analysis \- Analysis of the MCMC
.SH SYNOPSIS
.B mcmc_analysis
[\fIOPTIONS\fR]\fI \fIMCMC-data\fR ...\fR

.SH DESCRIPTION

This program reads MCMC output, from one or several files, from
programs in the BEEP package. It is important that the same columns
are present in each input file. 

Input format:
  Output from MCMC iterations on the format
        <likelihood> <tab> <iter> <tab> <params>
     where 
        <likelihood> is the logarithm of the likelihood in float format
        <tab>        is tab whitespace separating the fields
        <iter>       is an integer for the ordinal of the iteration
        <params>     is a list of fields separated by semicolons containing
                     the parameters of the MCMC.

     The MCMC params are typed and given names by the first line in the file, 
     which is on the format
        # L <tab> N <tab> [<name>(<type>)]+
     The names ought to be unique, but do not have to be. The <type> is one 
     of
        float
        logfloat
        integer
        tree
        orthologypairs
     and used to infer how to parse and analyze the rest of the lines.

Output format:
  A report on the MCMC run, with posterior estimates of the parameters.

.PP

.SH OPTIONS
              

.TP
.BR \-b " " [\fIFLOAT|\fIINT]\fR
The percentage (0 <= x <1), or the number of (x is integer >= 1) of the input to be discarded as burnin. Default: 0.1.
.TP
.BR \-p " " \fISTRING\fR
Plot the parameter named <string>. Output is two columns, the iteration number and the parameter's value in the iteration.
.TP
.BR \-i " " \fISTRING\fR
Ignore the named parameter. See header line in MCMC output for column names. You can name several columns at once using a comma-delimited format, (e.g. \-i Length,Name). No spaces allowed between column names.
.TP
.BR \-t
Output LaTex for the analysis.
.TP
.BR \-l
Count and report the number of samples in the input file.
.TP
.BR \-mp " " \fINT\fR
The maximum number of points to plot.
.TP
.BR \-sp
Explicitly indicate points in plots.
.TP
.BR \-coda
Output file for the CODA package in R
.TP
.BR \-codatrees
Same as \-coda, but includes tree parameters. Each tree is output as an integer ID (order of visitation in chain, 1,2,...). Make sure to use \-i to hide any trees containing lengths/times/rates.
.TP
.BR \-P
Parallel chains. This implies that several files of (parallel) samples are listed.

.SH URL
.TP
The prime-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeGSRf (1),
.BR primeGEM (1),
.BR chainsaw (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR treesize (1)
