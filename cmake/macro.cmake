# The macro conf_value_from_cmake is used to have one location where
# we set a default value. This can then be used in c++ code and in 
# man pages
#
# Execute the macro like this
# conf_value_from_cmake(primeGSRf Iterations 1000000)
# It will create preprocessor defines in
# ${CMAKE_CURRENT_BINARY_DIR}/conf_value_from_cmake_primeGSRf.hh
#
# 
macro(conf_value_from_cmake progname varname value) 
 file(APPEND ${CMAKE_CURRENT_BINARY_DIR}/conf_value_from_cmake_${progname}.hh "#define conf_value_from_cmake_${varname} ${value}
#define conf_value_from_cmake_${varname}_string \"${value}\"
")
endmacro()
