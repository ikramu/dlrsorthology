#!/bin/sh

# Run this script like this:
#
# mkdir ~/installdir
# sh install_ports_and_prime.sh ~/installdir

set -e
macports_version=2.0.4
installdir="$1"


if [ -d "$installdir" ]; then
  cd "$installdir"
  export PATH="$installdir/macports/bin:$PATH"
#  curl -O http://codesynthesis.com/download/xsd/3.3/macosx/i686/xsd-3.3.0-i686-macosx.tar.bz2
#  tar xfj xsd-3.3.0-i686-macosx.tar.bz2
  curl -O https://distfiles.macports.org/MacPorts/MacPorts-${macports_version}.tar.gz
  tar xfz MacPorts-${macports_version}.tar.gz
  cd MacPorts-${macports_version}
  ./configure --prefix="$installdir/macports" --with-no-root-privileges
  make
  make install
  port -v selfupdate
# maybe we need to install fortran like this:
# port install gcc44 +gfortran
  port install openmpi libxml2 bison flex gengetopt
  port install boost +openmpi
  port install cmake
  port install subversion

  cd "$installdir"

  svn co --trust-server-cert --non-interactive https://svn.sbc.su.se/repos/PrIME/trunk prime-trunk

  tmpdir=`mktemp -d /tmp/prime_build.XXXX`
  cd $tmpdir && cmake "-DCMAKE_PREFIX_PATH=$installdir;$installdir/xsd-3.3.0-i686-macosx/" -DCMAKE_INSTALL_PREFIX=$installdir/primeinstall "-DCMAKE_Fortran_COMPILER=$installdir/macports/bin/gfortran-mp-4.4" "$installdir/prime-trunk/" 
  make -j 8 
  make install
#  make test
  echo export "PATH=$installdir/primeinstall/bin:$installdir/macports/bin:$PATH"
else
  echo "The directory $installdir doesn't exist"
fi
