Vagrant::Config.run do |config|
  config.vm.box = "@vagrant_box@"
  config.vm.box_url = "http://files.vagrantup.com/@vagrant_box@.box"
  config.vm.share_folder "primesource", "/deb_build_dir", "@CMAKE_BINARY_DIR@/deb_build_dir"
  config.vm.customize ["modifyvm", :id, "--memory", 2000, "--cpus", @CPU_COUNT_FOR_BUILD_IN_VAGRANT@]
  config.vm.provision :shell, :path => "provision.sh"

end
