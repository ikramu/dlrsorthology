#!/bin/sh

# This sets up pbuilder to build Debian packages for Squeeze.
# In real life, you would probably use multiple Vagrant boxes
# to build packages for different distros w/o pbuilder...



echo $1 > /tmp/distribution

sudo sed -i "s/us.archive/se.archive/g" /etc/apt/sources.list
sudo apt-get update --fix-missing
sudo apt-get install -y pbuilder debootstrap devscripts cdbs debian-archive-keyring
sudo apt-get install -y build-essential cmake gfortran gengetopt libblas-dev liblapack-dev bison flex libopenmpi-dev libxml2-dev libplot-dev libboost-dev libboost-mpi-dev libboost-serialization-dev subversion rpm libpng12-dev 

sudo apt-get install -y fakeroot

echo "unset CCACHEDIR" >> /etc/pbuilderrc

#  ftp://ftp.se.debian.org/debian/


case "@BUILD_FOR_DEBIAN_OR_UBUNTU@" in
ubuntu)  
 sudo pbuilder create --distribution "@BUILD_FOR_DEBIAN_OR_UBUNTU_RELEASE@" --mirror http://ftp.funet.fi/pub/linux/mirrors/ubuntu/archive/                    --debootstrapopts "--keyring=/usr/share/keyrings/ubuntu-archive-keyring.gpg"  --othermirror "deb http://ftp.funet.fi/pub/linux/mirrors/ubuntu/archive/ @BUILD_FOR_DEBIAN_OR_UBUNTU_RELEASE@ universe multiverse"
    ;;
debian)
 sudo pbuilder create --distribution "@BUILD_FOR_DEBIAN_OR_UBUNTU_RELEASE@" --mirror http://ftp.funet.fi/pub/linux/mirrors/debian/                      --debootstrapopts "--keyring=/usr/share/keyrings/debian-archive-keyring.gpg"
    ;;
*) echo error: variable dist_type is neither "ubuntu" nor "debian". 
   exit 1
   ;;
esac

cd "/deb_build_dir/prime-phylo-@PACKAGE_VERSION@/debian"

#su -c "pdebuild --buildresult /vagrant --debbuildopts -j${num_threads}" vagrant

num_threads=`cat /vagrant/num_threads`

pdebuild --buildresult /vagrant --debbuildopts -j${num_threads}




#sudo apt-get install -y git-buildpackage
