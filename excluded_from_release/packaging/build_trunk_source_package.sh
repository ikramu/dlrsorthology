#!/bin/sh

set -e

if [ $# -ne 0 ]
then
  echo "error: wrong number of arguments. Usage: `basename $0`"
  exit 1
fi

srcdir=`mktemp -d` 
cd $srcdir
svn co https://svn.sbc.su.se/repos/PrIME/trunk

builddir=`mktemp -d` 
cd $builddir

cmake $srcdir/trunk && make -j 8 package_source
