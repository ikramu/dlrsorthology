#!/bin/sh

# this script needs to be run on a MacOS i686 computer

set -e


rmd160=`openssl dgst -rmd160 "@CMAKE_BINARY_DIR@/@CPACK_SOURCE_PACKAGE_FILE_NAME@.tar.gz" | awk '{print $2;}'`
sha256=`openssl dgst -sha256 "@CMAKE_BINARY_DIR@/@CPACK_SOURCE_PACKAGE_FILE_NAME@.tar.gz" | awk '{print $2;}'`

cat "@CMAKE_BINARY_DIR@/packaging/macports/Portfile.without_checksums" | sed "s/REPLACE_WITH_RMD160_CHECKSUM/$rmd160/" |
sed "s/REPLACE_WITH_SHA256_CHECKSUM/$sha256/" > "@CMAKE_BINARY_DIR@/packaging/macports/ports/science/@PROJECT_NAME@/Portfile"

cd "@CMAKE_BINARY_DIR@/packaging/macports/ports/"
portindex

echo "To try the Portfile out temporarily add this line to macports/etc/macports/sources.conf"
echo "file://@CMAKE_BINARY_DIR@/packaging/macports/ports [nosync]"

scp -P 50011 "@CMAKE_BINARY_DIR@/@CPACK_SOURCE_PACKAGE_FILE_NAME@.tar.gz"  www.sbc.su.se:/var/www/prime.sbc.su.se/docroot/download/

# We will try to release PrIME as an official MacPorts package instead of running our own rsync server
# rsync -ae "ssh -p 50011" --delete "@CMAKE_BINARY_DIR@/packaging/macports/ports/" www.sbc.su.se:/var/www/prime.sbc.su.se/docroot/download/ports



