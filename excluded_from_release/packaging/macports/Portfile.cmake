# -*- coding: utf-8; mode: tcl; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- vim:fenc=utf-8:ft=tcl:et:sw=4:ts=4:sts=4
# $Id$

PortSystem          1.0
PortGroup           cmake 1.0
name                @PROJECT_NAME@
version             @PACKAGE_VERSION@
revision            1
categories          science
platforms           darwin
license             GPL-3
maintainers         erik.sjolund@sbc.su.se
description         Bayesian estimation of gene trees taking the species tree into account
long_description    PrIME (Probabilistic Integrated Models of Evolution) is a package supporting inference of evolutionary parameters in a Bayesian framework using MCMC. A distinguishing feature of PrIME is that the species tree is taken into account when analyzing gene trees.
homepage            http://prime.sbc.su.se/
master_sites        http://prime.sbc.su.se:/download:tag1 \
                    http://codesynthesis.com:/download/xsd/3.3/macosx/i686/:tag2

set xsdversion 3.3.0

distfiles           ${distname}.tar.gz:tag1
checksums           ${distname}.tar.gz \
                        rmd160 REPLACE_WITH_RMD160_CHECKSUM \
                        sha256 REPLACE_WITH_SHA256_CHECKSUM

extract.only        ${distname}.tar.gz

if { ${os.platform} == "darwin" } {
  if { ${os.arch} == "i386" || ${os.arch} == "x86_64" } {
    set xsddistname xsd-${xsdversion}-i686-macosx
    distfiles-append   ${xsddistname}.tar.bz2:tag2
    checksums-append   ${xsddistname}.tar.bz2 rmd160 f2626166178249a4c17f144c74a08d5a7351c95b \
    sha256 6e0b084e68c4787612454e1adb34cc53a58a3c76a987650b55b636f24f1be01d
  }  
  if { ${os.arch} == "powerpc" } {
    set xsddistname xsd-${xsdversion}-powerpc-macosx
    distfiles-append ${xsddistname}.tar.bz2:tag2
    checksums-append  ${xsddistname}.tar.bz2 rmd160 f58e256ff2b1efaef4a29b91e8f40a068f827127 \
    sha256 8f035804c6d4b0526343b134b4477831c2d0c92f78e84607beaff4069d4e65bd
  } 
}

post-extract {
  system "tar xfj ${distpath}/${xsddistname}.tar.bz2 -C ${workpath}"
}

depends_build       port:gengetopt          \
                    port:bison              \
                    port:flex 

depends_lib         port:openmpi            \
                    port:xercesc3           \
                    port:atlas              \
                    port:g95                \
                    port:libxml2

build.type      gnu

configure.args-append  '-DCMAKE_PREFIX_PATH=${workpath}/${xsddistname}\;${prefix}'
configure.post_args-append ${worksrcpath}

test.run            yes

livecheck.type      regex
livecheck.url       ${homepage}/download/
livecheck.regex     ${name}-(\[.\\d\]+)${extract.suffix}
