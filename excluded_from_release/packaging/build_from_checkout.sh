#!/bin/sh

set -e

if [ $# -ne 2 ]
then
  echo "error: wrong number of arguments. Usage: `basename $0` checkout-dir nr_in_parallell"
  exit 1
fi

checkout_dir=$1

nr_in_parallell=$2



builddir=`mktemp -d /tmp/prime_build_XXXXX` 
cd $builddir

installdir=`mktemp -d /tmp/prime_install_XXXXX` 

cmake -DCMAKE_INSTALL_PREFIX=$installdir -Dprime_dynamic=on $checkout_dir && make -j${nr_in_parallell} && make test ARGS=-j${nr_in_parallell} && make install

echo "build directory was $builddir"
echo "install directory was $installdir"
