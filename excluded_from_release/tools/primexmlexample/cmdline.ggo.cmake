package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "Show an example of how Codesynthesis XSD could be used in PrIME"
    
args "--unamed-opts"

option "talk" t "let the MCMC talk. This shows an example of polymorphism in Codesynthesis (virtual methods)" flag off

usage "primexmlexample [OPTIONS] [<primexmlfile>]"

description "<primexmlfile> is the filename of the file containing trees in PRIME format."


