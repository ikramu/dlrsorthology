#include <iostream>
#include <stdlib.h>
#include <cassert>
#include "primexml.hh"
#include "cmdline.h"
#include "config.h"
#include <boost/foreach.hpp>

using namespace std;
//using namespace beep;

void talkRecursively(const primexmlNs::McmcType &mcmc) {
  std::cout << mcmc.talk() << std::endl;
  BOOST_FOREACH(const primexmlNs::McmcType &iter, mcmc.mcmc()) {
    talkRecursively(iter);
  }
}

int main (int argc, char **argv) {
  struct gengetopt_args_info args_info;
  if (cmdline_parser(argc, argv, &args_info) != 0) {
    exit(EXIT_FAILURE);
  }
  if (args_info.inputs_num != 1) {
    cerr << "Error: No primexml filename given" << endl;
     exit(EXIT_FAILURE);
  }

  //  args_info.inputs[0];


  primexmlNs::EdgeRateMcmcType edge_rate_mcmc(2.3, 100, 10, 2.8);
  primexmlNs::BirthDeathMcmcType birth_death_mcmc(4, 200, 10, 2.5);

  edge_rate_mcmc.mcmc().push_back(birth_death_mcmc);

  int id = 2;
  primexmlNs::NodeType node(id);
  primexmlNs::PrimeType pri(node, edge_rate_mcmc);

  xml_schema::namespace_infomap map;
  map[""].schema = "primexml.xsd";
  map[""].name = PRIMEXML_NAMESPACE;

  primexmlNs::prime(std::cout, pri, map);
  talkRecursively(pri.mcmc());

  if (args_info.talk_flag) {
  }
  return EXIT_SUCCESS;
}
