#ifndef PRIMEXML_CUSTOM_HH
#define PRIMEXML_CUSTOM_HH

#include "primexml-fwd.hh"

#include <memory>   
#include <algorithm>
#include <string>

typedef ::xml_schema::int_ id_type;
typedef ::xml_schema::float_ probability_type;
typedef ::xml_schema::unsigned_long iteration_type;
typedef ::xml_schema::unsigned_long nAcceptedStates_type;
typedef ::xml_schema::float_ birthDeathParam_type;
typedef ::xml_schema::float_ suggestionVariance_type;

template <typename base>
class NodeType_impl: public base {
public:
  NodeType_impl(const id_type &);
  NodeType_impl(const xercesc::DOMElement &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  NodeType_impl(const NodeType_impl &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  virtual NodeType_impl *_clone(xml_schema::flags = 0,
                                 xml_schema::container * = 0) const;
  virtual ~NodeType_impl() {};
};

template <typename base>
class McmcType_impl: public base {
public:
  McmcType_impl(const probability_type&,
                const iteration_type&,
                const nAcceptedStates_type&);
  McmcType_impl(const xercesc::DOMElement &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  McmcType_impl(const McmcType_impl &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  virtual McmcType_impl *_clone(xml_schema::flags = 0,
                                 xml_schema::container * = 0) const;
  virtual ~McmcType_impl() {};
  virtual std::string talk() const { return std::string("I am Mcmc"); };
};

template <typename base>
class EdgeRateMcmcType_impl: public base {
public:
  EdgeRateMcmcType_impl(const probability_type&,
                        const iteration_type&,
                        const nAcceptedStates_type&,
                        const suggestionVariance_type &);
  EdgeRateMcmcType_impl(const xercesc::DOMElement &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  EdgeRateMcmcType_impl(const EdgeRateMcmcType_impl &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  virtual EdgeRateMcmcType_impl *_clone(xml_schema::flags = 0,
                                 xml_schema::container * = 0) const;
  virtual ~EdgeRateMcmcType_impl() {};
  virtual std::string talk() const { return std::string("I am EdgeRate"); };
};

template <typename base>
class BirthDeathMcmcType_impl: public base {
public:
  BirthDeathMcmcType_impl(const probability_type&,
                          const iteration_type&,
                          const nAcceptedStates_type&,
                          const birthDeathParam_type &);
  BirthDeathMcmcType_impl(const xercesc::DOMElement &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  BirthDeathMcmcType_impl(const BirthDeathMcmcType_impl &,
                xml_schema::flags = 0,
                xml_schema::container * = 0);
  virtual BirthDeathMcmcType_impl *_clone(xml_schema::flags = 0,
                                 xml_schema::container * = 0) const;
  virtual  ~BirthDeathMcmcType_impl() {};
  virtual std::string talk() const { return std::string("I am BirthDeathRate"); };
};

#endif
