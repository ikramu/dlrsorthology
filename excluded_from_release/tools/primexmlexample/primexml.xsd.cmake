<?xml version="1.0"?>
<xsd:schema xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns="@primexml_namespace@" elementFormDefault="unqualified" targetNamespace="@primexml_namespace@">
  <xsd:element name="prime" type="PrimeType"/>
  <xsd:complexType name="PrimeType">
    <xsd:sequence>
      <xsd:element minOccurs="1" maxOccurs="1" ref="node"/>
      <xsd:element minOccurs="1" maxOccurs="1" ref="mcmc"/>
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="NodeType">
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" ref="node"/>
    </xsd:sequence>
    <xsd:attribute name="nt" type="xsd:float"/>
    <xsd:attribute name="id" type="xsd:int"  use="required"/>
    <xsd:attribute name="name" type="xsd:string"/>
  </xsd:complexType>
  <xsd:element name="node" type="NodeType"/>

  <xsd:complexType name="McmcType">
    <xsd:sequence>
      <xsd:element minOccurs="0" maxOccurs="unbounded" ref="mcmc"/>
    </xsd:sequence>
    <xsd:attribute name="IIIprobability" type="xsd:float" use="required"/>
    <xsd:attribute name="IIIiteration" type="xsd:unsignedLong" use="required"/>
    <xsd:attribute name="IIInAcceptedStates" type="xsd:unsignedLong" use="required"/>
  </xsd:complexType>
  <xsd:element name="mcmc" type="McmcType"/>

  <xsd:complexType name="StdMcmcType">
    <xsd:complexContent>
      <xsd:extension base="McmcType">
        <xsd:attribute name="IIIn_params" type="xsd:unsignedInt" use="required"/>
        <xsd:attribute name="IIIsuggestRatio" type="xsd:float" use="required"/>
        <xsd:attribute name="IIIsuggestRatioDelta" type="xsd:float" default="0.0"/>
        <xsd:attribute name="IIIsuggestRatioPendingUpdates" type="xsd:unsignedInt" default="0"/>
        <xsd:attribute name="IIIparamIdxRatio" type="xsd:float" use="required"/>
        <xsd:attribute name="IIIparamIdx" type="xsd:float" default="0.0"/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:element name="stdMcmc" type="StdMcmcType" substitutionGroup="mcmc"/>

  <xsd:complexType name="EdgeRateMcmcType">
    <xsd:complexContent>
      <xsd:extension base="McmcType">
        <xsd:attribute name="suggestionVariance" type="xsd:float" use="required"/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:element name="edgeRateMcmc" type="EdgeRateMcmcType" substitutionGroup="mcmc"/>

  <xsd:complexType name="BirthDeathMcmcType">
    <xsd:complexContent>
      <xsd:extension base="McmcType">
        <xsd:attribute name="birthDeathParam" type="xsd:float" use="required"/>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>
  <xsd:element name="birthDeathMcmc" type="BirthDeathMcmcType" substitutionGroup="mcmc"/>

</xsd:schema>

