#!/bin/sh

if test $# -eq 1; then
installdir=$1
else
installdir=`mktemp -d /tmp/prime_install.XXXX`
chmod 700 $installdir
fi

set -e
this_source_dir=`pwd`


export MODULEPATH="/afs/pdc.kth.se/projects/sbc/modules/system:/afs/pdc.kth.se/projects/sbc/modules/i386_ubuntu8.10:/afs/pdc.kth.se/projects/sbc/modules/i386_fc3:/afs/pdc.kth.se/projects/sbc/modules:/afs/pdc.kth.se/home/a/arnee/modules/noarch:/afs/pdc.kth.se/home/a/arnee/modules/i686:/pdc/modules/system/base:/pdc/modules/i386_co4/base:/pdc/modules/i386_fc3/base:/pdc/modules/i386_rh9:$MODULEPATH"

. source-this-gcc.sh

tmpdir=`mktemp -d /tmp/prime_build.XXXX`
chmod 700 $tmpdir
cd $tmpdir 

export "PATH=/afs/pdc.kth.se/home/e/esjolund/vol01/Public/PrIME-build-dependencies-on-Ferlin/cmake-2.8.7-Linux-i386/bin:/afs/pdc.kth.se/home/e/esjolund/vol01/Public/PrIME-build-dependencies-on-Ferlin/xsd-3.3.0-x86_64-linux-gnu:/afs/pdc.kth.se/home/i/ikramu/proj-dir/essential_scripts/plotutils-2.6:$PATH"

cmake -DCMAKE_INSTALL_PREFIX=$installdir '-DCMAKE_PREFIX_PATH=/afs/pdc.kth.se/home/e/esjolund/vol01/Public/PrIME-build-dependencies-on-Ferlin/xsd-3.3.0-x86_64-linux-gnu;/afs/pdc.kth.se/home/e/esjolund/Public/software/libxml2/2.7.6/install/system/' "$this_source_dir/.."  && make -j 8 && make install

