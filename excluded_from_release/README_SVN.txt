Some reminders about using SVN:

To list the available branches:
   svn list https://svn.sbc.su.se/repos/PrIME/branches

To list the available tags:
   svn list https://svn.sbc.su.se/repos/PrIME/tags

