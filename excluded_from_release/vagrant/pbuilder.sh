#!/bin/sh
sudo sed -i "s/us.archive/se.archive/g" /etc/apt/sources.list
sudo apt-get update --fix-missing
sudo apt-get install -y pbuilder debootstrap devscripts cdbs debian-archive-keyring
sudo apt-get install -y build-essential cmake gfortran gengetopt libblas-dev liblapack-dev bison flex libopenmpi-dev libxml2-dev libplot-dev libboost-dev libboost-mpi-dev libboost-serialization-dev subversion rpm libpng12-dev
sudo pbuilder create --distribution squeeze --mirror http://ftp.funet.fi/pub/linux/mirrors/debian/  \
                     --debootstrapopts "--keyring=/usr/share/keyrings/debian-archive-keyring.gpg"

