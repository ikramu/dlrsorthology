I detta exempel har vi tv� parallella mcmc-k�rningar i filerna myRun_1.mcmc
och myRun_2.mcmc.

F�rst anv�nder vi mcmc_analysis f�r att g�ra om filerna till r�tt
format f�r R-coda:

>for i in `ls myRun_?.mcmc`; do j=`echo $i|sed s/mcmc/R/`;
mcmc_analysis -coda $i > $j; done

Sedan k�r vi R:

>module add R
>R

L�s in coda-biblioteket:

$library(coda)

Anv�nd 'read.table' och coda-funktionen 'mcmc' f�r att l�sa in filerna
till r�tt format, (t1, t2, m1 och m2 �r variabler i R). H�r brukar jag
klippa och klistra, men egentligen borde man definiera funktioner f�r
detta i R:

$t1=read.table("myRun_1.R");t2=read.table("myRun_2.R");
$m1=mcmc(t1); m2=mcmc(t2); 

Sedan l�gger vi ihop m1 och m2 (de tv� mcmc-kedjorna) i en lista:

$ml=mcmc.list(m1,m2)

�ppna coda-menu:

$codamenu()

Nu f�r vi en lista med saker vi kan g�ra. F�r att l�sa in v�r ml-fil
v�l '2' och skriv sedan 'ml'. Det dr�jer nu ett tag, sedan kommer en
ny meny.
-F�r att testa konvergens v�lj '2' och sedan '2' igen f�r
Gelman-Rubin-testet som jag brukar anv�nda. Teststorheten
tabelleras f�r varje variabel och f�r alla tillsammans.
-F�r att plotta v�lj '1' och sedan '1' igen. Trace och utj�mnad
posterior plottas f�r varje variabel.
-F�r att �ndra inst�llningar v�ljs '3' och sedan f�ljer man
instruktionerna. Man kan t.ex. ibland vilja plotta bara n�gra av
kedjorna eller bara n�gra av variablerna.
F�r att �terv�nda till senaste menyn v�ljs 'Return to Main Menu'.
F�r att avsluta v�lj '0'.
R avslutas med q(). Det kan ibland vara bra att spara 'workspace
image', men filen kan ta stor plats.

