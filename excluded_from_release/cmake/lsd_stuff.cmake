 ${IO_SOURCES}


set(IO_SOURCES
  ${CMAKE_CURRENT_BINARY_DIR}/io/LSDFileFormat.cxx 
  ${CMAKE_CURRENT_BINARY_DIR}/io/LSDEstimatesFormat.cxx
)


add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/io/LSDFileFormat.cxx  ${CMAKE_CURRENT_BINARY_DIR}/io/LSDFileFormat.hxx
  COMMAND ${XSD_EXECUTABLE}
  ARGS ${XSD_ARGS} --output-dir ${CMAKE_CURRENT_BINARY_DIR}/io/ ${CMAKE_CURRENT_SOURCE_DIR}/io/xsd/LSDFileFormat.xsd
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/io/xsd/LSDFileFormat.xsd
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)
#message(FATAL_ERROR "${XSD_INCLUDE_DIR}")

add_custom_command(
  OUTPUT ${CMAKE_CURRENT_BINARY_DIR}/io/LSDEstimatesFormat.cxx  ${CMAKE_CURRENT_BINARY_DIR}/io/LSDEstimatesFormat.hxx
  COMMAND ${XSD_EXECUTABLE}
  ARGS ${XSD_ARGS} --output-dir ${CMAKE_CURRENT_BINARY_DIR}/io/ ${CMAKE_CURRENT_SOURCE_DIR}/io/xsd/LSDEstimatesFormat.xsd
  DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/io/xsd/LSDEstimatesFormat.xsd
  WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)

set_source_files_properties(  ${CMAKE_CURRENT_BINARY_DIR}/io/LSDFileFormat.cxx  PROPERTIES GENERATED true)
set_source_files_properties(  ${CMAKE_CURRENT_BINARY_DIR}/io/LSDFileFormat.hxx  PROPERTIES GENERATED true)
set_source_files_properties(   ${CMAKE_CURRENT_BINARY_DIR}/io/LSDEstimatesFormat.cxx  PROPERTIES GENERATED true)
set_source_files_properties(   ${CMAKE_CURRENT_BINARY_DIR}/io/LSDEstimatesFormat.hxx  PROPERTIES GENERATED true)

