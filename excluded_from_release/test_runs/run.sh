#!/bin/bash

homePath=$PWD'/../../'
buildPath=$homePath'primebuild'
installPath=$homePath'primeinstall'

rm -rf $installPath
mkdir $installPath
rm -rf $buildPath
mkdir $buildPath

cd $buildPath  && 
cmake -DCMAKE_INSTALL_PREFIX=$installPath  -DCMAKE_PREFIX_PATH=/afs/pdc.kth.se/home/e/esjolund/Public/software/libxml2/2.7.6/install/system/ -DEXCLUDE_LSDCOMBINE=on -DEXCLUDE_PRIMEGSRORTHO=on -DEXCLUDE_PRIMEBDT=ON -DEXCLUDE_PRIMECT=ON $homePath  && 
make -j 8 && 
make install

