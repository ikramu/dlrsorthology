#!/bin/bash

# Command line arguments are in following order
# $1 = sample number

# Check for proper number of command line args.
EXPECTED_ARGS=1
E_BADARGS=65

if [ $# -ne $EXPECTED_ARGS ]
then
  echo ""
  echo "Error: wrong number of arguments"
  echo "Usage: `basename $0` start_counter"
  echo ""
  exit $E_BADARGS
fi

echo ../../../executables/orthoProject -g gsr_spec_mhc_all_$1_fixed.output -v mhc_all_$1_fixed.vv.probs -C mhc_all_$1_fixed.fig -o mhc_all_$1_fixed.tree i3e7_t100_all_$1_small.txt  ../mhc_species.tree  ../mhc_gsmap

#../../../executables/orthoProject -g gsr_spec_mhc_all_$1_fixed.output -v mhc_all_$1_fixed.vv.probs -C mhc_all_$1_fixed.fig -o mhc_all_$1_fixed.tree i3e7_t100_all_$1_small.txt  ../mhc_species.tree  ../mhc_gsmap
