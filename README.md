# DLRSOrthology #
DLRSOrthology is a program for probabilistic orthology analysis using an integrated gene evolution and sequence evolution models. It is based on DLRS (pronounced Delirius and previously known as primeGSR). DLRS is an acronym for Duplication, Loss, Rate and Sequence evolution.

The software is written in C++ and is licensed under the GPL version 3. For details about licensing see the file COPYING.