#!/bin/bash

# This is a script for testing primeDLRS output. Here we want to generate posterior using 
# 20000 MCMC cycles, every 50th of which is saved to test_dlrs_output.mcmc file. We are using
# JTT model of amino acid sequence evolution for the data. The gene sequences are in apr.seq 
# file; the species tree is in apr.stree file; while the gene to species mapping is in apr.gsmap file

# we use a common prefix for all output files 
outprefix='Test'
outdir='dlrsorthology_output'

rm -rf dlrsorthology_output
mkdir dlrsorthology_output

../primeinstall/bin/DLRSOrthology -b 160 -o $outdir/$outprefix.tree -g $outdir/$outprefix.output -v $outdir/$outprefix.vv.probs -c $outdir/$outprefix.svg  test_dlrs_output.mcmc apr.stree apr.gsmap
