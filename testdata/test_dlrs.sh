#!/bin/bash

# This is a script for testing primeDLRS output. Here we want to generate posterior using 
# 20000 MCMC cycles, every 50th of which is saved to test_dlrs_output.mcmc file. We are using
# JTT model of amino acid sequence evolution for the data. The gene sequences are in apr.seq 
# file; the species tree is in apr.stree file; while the gene to species mapping is in apr.gsmap file

rm -rf test_dlrs_output.mcmc;

../primeinstall/bin/primeDLRS  -i 20000 -t 100 -Sm JTT -r -o test_dlrs_output.mcmc apr.seq apr.stree apr.gsmap
