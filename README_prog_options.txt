Options to programs (beep_xx) is preceded by an hyphen '-' and
comprise an option code of one or more alphabetical letters (currently
a maximum of two letters are used). If the option then takes values,
a space is needed betweenoption code and  values and between
subsequent values. The option letters now follow the following form:

General options comprise a single lowercase letter, examples:
-u/h	usage/help text (specific for program
-i <n>	number of MCMC-iterations to run
-t <n>	Thinning in MCMC. Write to mcmc-otput every <n> iteration.
-w <n>	Write to STDERR n times less often than to mcmc-output.
-l	Calculate likelihood only, don't do MCMC
-s	seed for random number generator
-o	output filename
-q	quiet mode
-d	debug info

Model-specific options copmprise two letters, an upper-case
letter coding for the model (or other option family) in question, and
a lower-case letter coding for the option relating to this
model. 
Model-specific letter are (so far):
S      Substitution model including site rate models
E      Edge rate model
B      Birth-death model including reconciliations
G      Guest tree (gene  tree)
H      Host tree (species tree)
(O     gene order?)
T      'true'. Used in simulation/generation programs. options
       relating to output of parameter values used in data generation

Option-specific letters vary with model, but some examples are:
o      outgroup (in combination with T, truefile name)
p      numeric parameter values (e.g., mean, variance or intensities)
f      Force numeric parameter values.
m      the specific model to use
d      Density or distribution to use
t      times (usually times fixed)
g      tree topology (usually tree topology fixed)
r      root (usually root fixed)


Lastly some examples of options:
-Sm JC69    Use substitution model descibed by Jukes-Cantor (1969)
-Ed Gamma   Use a Gamma density as the underlying density of the
	    edgerate model
-Bp 0.3 0.4 Set starting value of birth rate to 0.3 and of death rate
	    to 0.4 in birth death model
-Bf 0.3 0.4 Fix the value of birth rate to 0.3 and of death rate to
	    0.4 in MCMC ananlysis.
-Gg	    Fix tree topology of guest tree in MCMC analysis.

Last changed:
/bens 2006-02-06
